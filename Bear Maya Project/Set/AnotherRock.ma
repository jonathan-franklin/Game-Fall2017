//Maya ASCII 2017ff05 scene
//Name: AnotherRock.ma
//Last modified: Tue, Nov 14, 2017 11:59:43 AM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "0B9A8F2D-43FA-E9D9-D756-BAA1DE1ED8CC";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 2.1158860666826866 3.0480581638243649 0.76394942292802726 ;
	setAttr ".r" -type "double3" -60.938352730021087 -1730.2000000004759 -1.8422075125883868e-014 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "63C83D2B-4D05-725D-F9D7-428CB48D61D4";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 3.6631916938552491;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "2D9B3A06-42BD-A09D-001C-8DA047D7164D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.81206117812013989 1000.1 -0.17698769266720979 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "496FD368-444D-D6B4-35A3-6D8C8F5D497F";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 2.7901589196948398;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "20161A6C-49C0-2BCB-B474-4F982C714BA8";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.1243924004740395 0.062466244470779864 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "E6B56661-4264-33AC-5871-AFA60CDFB309";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 2.7901589196948398;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "4069CB9A-4017-E016-F5C3-09A3C1A3945C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "83A871C8-467A-0DEC-4BF9-FABA9FD95749";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "anotherRock:Mesh";
	rename -uid "29F6BFE3-40E0-9007-6E17-319E32C2BFF7";
	setAttr ".s" -type "double3" 1.2000347354578098 0.91127597028534646 1 ;
createNode mesh -n "anotherRock:MeshShape" -p "anotherRock:Mesh";
	rename -uid "5718808F-4D26-291D-206D-D5BEE872CB55";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
createNode mesh -n "anotherRock:polySurfaceShape1" -p "anotherRock:Mesh";
	rename -uid "BEAF523F-4AC6-B3CA-BC77-76AAE920CA33";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 68 ".vt[0:67]"  0.78622299 -0.68245602 -0.63331401 0.85661101 -0.67205298 0.837143
		 -1 -0.51128799 0.53560901 -0.73781401 -0.53454298 -0.839737 0.64143598 0.47440499 -0.58848298
		 0.71672797 0.46494299 0.66003102 -1 0.29964399 0.53560901 -0.59161699 0.32627201 -0.61635298
		 0.153255 -0.69470501 -1 -0.111679 -0.67792398 -1 0.153255 -0.77438498 0.99356699
		 -0.111679 -0.77438599 0.99356699 0.153255 0.67858499 -0.81520599 -0.111679 0.66216898 -0.81869
		 0.153254 0.80177301 0.81196302 -0.111679 0.79498303 0.85299999 0.91556197 -0.78950399 -0.17859299
		 -1 -0.58091497 0.023011001 0.91556197 0.702981 -0.17859299 -0.85380298 0.35715801 -0.070509002
		 0.153255 1.055130959 -0.26579699 -0.111679 1.055130959 -0.26579699 0.153255 -0.966443 -0.26579699
		 -0.111679 -0.966443 -0.26579699 1 -0.815418 0.33983499 0.88384801 0.80303901 0.33983499
		 0.153254 1.084969044 0.50577098 -0.111679 1.084969044 0.50577003 -1 -0.57987702 0.33546501
		 -1 0.33082801 0.33546501 0.153255 -0.88642502 0.51220298 -0.111679 -0.88642502 0.51220298
		 0.75426298 0.047440998 -0.64159799 0.84217501 0.19089501 0.79115498 -1.16228294 -0.065274999 0.57982999
		 -0.74061698 0.142184 -0.63721699 0.18426099 0.085390002 -1.031805992 -0.125076 0.083749004 -1.032155037
		 0.237812 0.14048301 1.13409603 -0.24261101 0.13792901 1.17924201 -1.016085982 0.00056499999 -0.070639998
		 1.074332952 0.074533001 -0.196215 -1.16228294 -0.078988999 0.36235601 1.17292297 0.074732997 0.36710399
		 0.22054 -0.39865401 1.27671099 -0.26040301 -0.39190099 1.27707303 -1.25623298 -0.292274 0.023979001
		 1.16442299 -0.39320499 -0.23289201 -1.25569403 -0.286948 0.42310801 1.27231395 -0.37851301 0.428693
		 0.82055402 -0.26593599 -0.764678 0.919016 -0.20711 0.84261 -1.254004 -0.234413 0.67546499
		 -0.92265701 -0.18537199 -0.74971998 0.199349 -0.37600899 -1.28190696 -0.135469 -0.36171699 -1.28205299
		 -0.44906101 -0.54217601 -0.91364503 -0.59033501 -0.63261998 0.746804 -0.370285 0.580199 -0.78383899
		 -0.59033501 0.58922499 0.76433599 -0.51155901 0.67904103 -0.16057 -0.59033501 -0.758708 -0.110178
		 -0.59033501 0.67861402 0.414004 -0.59033501 -0.72124702 0.416971 -0.440229 0.115235 -0.91424298
		 -0.68395698 -0.000124 0.93972802 -0.73739302 -0.30704099 0.95290703 -0.55963099 -0.26669699 -1.17699897;
	setAttr -s 132 ".ed[0:131]"  63 57 0 57 2 0 2 28 0 28 63 0 62 29 0 29 6 0
		 6 59 0 59 62 0 43 25 0 25 5 0 5 33 0 33 43 0 65 59 0 6 34 0 34 65 0 40 19 0 19 7 0
		 7 35 0 35 40 0 67 56 0 56 3 0 3 53 0 53 67 0 50 0 0 0 8 0 8 54 0 54 50 0 8 9 0 9 55 0
		 55 54 0 5 14 0 14 38 0 38 33 0 14 15 0 15 39 0 39 38 0 25 26 0 26 14 0 26 27 0 27 15 0
		 24 1 0 1 10 0 10 30 0 30 24 0 10 11 0 11 31 0 31 30 0 8 22 0 22 23 0 23 9 0 0 16 0
		 16 22 0 12 13 0 13 21 0 21 20 0 20 12 0 4 12 0 20 18 0 18 4 0 42 29 0 29 19 0 40 42 0
		 32 4 0 18 41 0 41 32 0 58 7 0 19 60 0 60 58 0 56 61 0 61 17 0 17 3 0 42 34 0 22 30 0
		 31 23 0 16 24 0 21 27 0 26 20 0 25 18 0 43 41 0 62 60 0 61 63 0 28 17 0 47 41 0 43 49 0
		 49 47 0 52 34 0 42 48 0 48 52 0 50 32 0 47 50 0 40 46 0 46 48 0 44 38 0 39 45 0 45 44 0
		 51 33 0 44 51 0 12 36 0 36 37 0 37 13 0 32 36 0 58 64 0 64 35 0 35 53 0 53 46 0 66 65 0
		 52 66 0 51 49 0 24 49 0 51 1 0 57 66 0 52 2 0 17 46 0 44 10 0 45 11 0 28 48 0 47 16 0
		 36 54 0 55 37 0 64 67 0 55 67 0 64 37 0 45 66 0 57 11 0 39 65 0 58 13 0 31 63 0 61 23 0
		 21 60 0 62 27 0 56 9 0 15 59 0;
	setAttr -s 264 ".n";
	setAttr ".n[0:165]" -type "float3"  -0.3786 -0.88739997 0.26320001 -0.3786
		 -0.88739997 0.26320001 -0.3786 -0.88739997 0.26320001 -0.3786 -0.88739997 0.26320001
		 -0.6433 0.74790001 0.1638 -0.6433 0.74790001 0.1638 -0.6433 0.74790001 0.1638 -0.6433
		 0.74790001 0.1638 0.70920002 0.3865 0.58969998 0.70920002 0.3865 0.58969998 0.70920002
		 0.3865 0.58969998 0.70920002 0.3865 0.58969998 -0.60960001 0.3294 0.72109997 -0.60960001
		 0.3294 0.72109997 -0.60960001 0.3294 0.72109997 -0.60960001 0.3294 0.72109997 -0.8082
		 0.4786 -0.34299999 -0.8082 0.4786 -0.34299999 -0.8082 0.4786 -0.34299999 -0.8082
		 0.4786 -0.34299999 -0.57969999 -0.46070001 -0.67210001 -0.57969999 -0.46070001 -0.67210001
		 -0.57969999 -0.46070001 -0.67210001 -0.57969999 -0.46070001 -0.67210001 0.542 -0.45609999
		 -0.70590001 0.542 -0.45609999 -0.70590001 0.542 -0.45609999 -0.70590001 0.542 -0.45609999
		 -0.70590001 -0.034200002 -0.66299999 -0.74779999 -0.034200002 -0.66299999 -0.74779999
		 -0.034200002 -0.66299999 -0.74779999 -0.034200002 -0.66299999 -0.74779999 0.43959999
		 0.46970001 0.76560003 0.43959999 0.46970001 0.76560003 0.43959999 0.46970001 0.76560003
		 0.43959999 0.46970001 0.76560003 0.097900003 0.4364 0.8944 0.097900003 0.4364 0.8944
		 0.097900003 0.4364 0.8944 0.097900003 0.4364 0.8944 0.4436 0.57429999 0.68800002
		 0.4436 0.57429999 0.68800002 0.4436 0.57429999 0.68800002 0.4436 0.57429999 0.68800002
		 0.0414 0.7511 0.65890002 0.0414 0.7511 0.65890002 0.0414 0.7511 0.65890002 0.0414
		 0.7511 0.65890002 0.1636 -0.94840002 0.27149999 0.1636 -0.94840002 0.27149999 0.1636
		 -0.94840002 0.27149999 0.1636 -0.94840002 0.27149999 0 -0.97399998 0.22669999 0 -0.97399998
		 0.22669999 0 -0.97399998 0.22669999 0 -0.97399998 0.22669999 -0.0296 -0.9339 -0.3563
		 -0.0296 -0.9339 -0.3563 -0.0296 -0.9339 -0.3563 -0.0296 -0.9339 -0.3563 0.2282 -0.92019999
		 -0.31799999 0.2282 -0.92019999 -0.31799999 0.2282 -0.92019999 -0.31799999 0.2282
		 -0.92019999 -0.31799999 -0.021600001 0.81980002 -0.57230002 -0.021600001 0.81980002
		 -0.57230002 -0.021600001 0.81980002 -0.57230002 -0.021600001 0.81980002 -0.57230002
		 0.44960001 0.69190001 -0.56489998 0.44960001 0.69190001 -0.56489998 0.44960001 0.69190001
		 -0.56489998 0.44960001 0.69190001 -0.56489998 -0.89139998 0.36829999 -0.2642 -0.89139998
		 0.36829999 -0.2642 -0.89139998 0.36829999 -0.2642 -0.89139998 0.36829999 -0.2642
		 0.7633 0.2367 -0.60110003 0.7633 0.2367 -0.60110003 0.7633 0.2367 -0.60110003 0.7633
		 0.2367 -0.60110003 -0.7446 0.5848 -0.32179999 -0.7446 0.5848 -0.32179999 -0.7446
		 0.5848 -0.32179999 -0.7446 0.5848 -0.32179999 -0.31099999 -0.92430001 -0.2211 -0.31099999
		 -0.92430001 -0.2211 -0.31099999 -0.92430001 -0.2211 -0.31099999 -0.92430001 -0.2211
		 -0.9217 0.3876 0.0162 -0.9217 0.3876 0.0162 -0.9217 0.3876 0.0162 -0.9217 0.3876
		 0.0162 0 -0.99479997 0.1023 0 -0.99479997 0.1023 0 -0.99479997 0.1023 0 -0.99479997
		 0.1023 0.1538 -0.98760003 0.031199999 0.1538 -0.98760003 0.031199999 0.1538 -0.98760003
		 0.031199999 0.1538 -0.98760003 0.031199999 0 0.9993 -0.038600001 0 0.9993 -0.038600001
		 0 0.9993 -0.038600001 0 0.9993 -0.038600001 0.38589999 0.9188 -0.082999997 0.38589999
		 0.9188 -0.082999997 0.38589999 0.9188 -0.082999997 0.38589999 0.9188 -0.082999997
		 0.94620001 0.31169999 -0.087399997 0.94620001 0.31169999 -0.087399997 0.94620001
		 0.31169999 -0.087399997 0.94620001 0.31169999 -0.087399997 -0.66039997 0.73930001
		 -0.1314 -0.66039997 0.73930001 -0.1314 -0.66039997 0.73930001 -0.1314 -0.66039997
		 0.73930001 -0.1314 -0.3603 -0.93190002 0.0427 -0.3603 -0.93190002 0.0427 -0.3603
		 -0.93190002 0.0427 -0.3603 -0.93190002 0.0427 0.96689999 0.1944 -0.1653 0.96689999
		 0.1944 -0.1653 0.96689999 0.1944 -0.1653 0.96689999 0.1944 -0.1653 -0.90530002 0.42109999
		 -0.0561 -0.90530002 0.42109999 -0.0561 -0.90530002 0.42109999 -0.0561 -0.90530002
		 0.42109999 -0.0561 0.80930001 0.26879999 -0.5223 0.80930001 0.26879999 -0.5223 0.80930001
		 0.26879999 -0.5223 0.80930001 0.26879999 -0.5223 -0.84219998 0.52990001 -0.1002 -0.84219998
		 0.52990001 -0.1002 -0.84219998 0.52990001 -0.1002 -0.84219998 0.52990001 -0.1002
		 0.0471 0.2177 0.97490001 0.0471 0.2177 0.97490001 0.0471 0.2177 0.97490001 0.0471
		 0.2177 0.97490001 0.47240001 0.20739999 0.85659999 0.47240001 0.20739999 0.85659999
		 0.47240001 0.20739999 0.85659999 0.47240001 0.20739999 0.85659999 -0.0046000001 0.34450001
		 -0.93879998 -0.0046000001 0.34450001 -0.93879998 -0.0046000001 0.34450001 -0.93879998
		 -0.0046000001 0.34450001 -0.93879998 0.53060001 0.28580001 -0.79799998 0.53060001
		 0.28580001 -0.79799998 0.53060001 0.28580001 -0.79799998 0.53060001 0.28580001 -0.79799998
		 -0.69019997 0.3768 -0.61769998 -0.69019997 0.3768 -0.61769998 -0.69019997 0.3768
		 -0.61769998 -0.69019997 0.3768 -0.61769998 -0.7949 0.54839998 -0.2595 -0.7949 0.54839998
		 -0.2595 -0.7949 0.54839998 -0.2595 -0.7949 0.54839998 -0.2595 -0.50629997 0.33590001
		 0.79430002 -0.50629997 0.33590001 0.79430002 -0.50629997 0.33590001 0.79430002 -0.50629997
		 0.33590001 0.79430002 0.79079998 0.2386 0.56370002 0.79079998 0.2386 0.56370002;
	setAttr ".n[166:263]" -type "float3"  0.79079998 0.2386 0.56370002 0.79079998
		 0.2386 0.56370002 0.77029997 -0.34220001 0.5381 0.77029997 -0.34220001 0.5381 0.77029997
		 -0.34220001 0.5381 0.77029997 -0.34220001 0.5381 -0.45609999 -0.65170002 0.60600001
		 -0.45609999 -0.65170002 0.60600001 -0.45609999 -0.65170002 0.60600001 -0.45609999
		 -0.65170002 0.60600001 -0.79619998 -0.5025 -0.3369 -0.79619998 -0.5025 -0.3369 -0.79619998
		 -0.5025 -0.3369 -0.79619998 -0.5025 -0.3369 0.4251 -0.35190001 0.83399999 0.4251
		 -0.35190001 0.83399999 0.4251 -0.35190001 0.83399999 0.4251 -0.35190001 0.83399999
		 -0.0049999999 -0.59899998 0.80070001 -0.0049999999 -0.59899998 0.80070001 -0.0049999999
		 -0.59899998 0.80070001 -0.0049999999 -0.59899998 0.80070001 -0.75010002 -0.6613 0.0065000001
		 -0.75010002 -0.6613 0.0065000001 -0.75010002 -0.6613 0.0065000001 -0.75010002 -0.6613
		 0.0065000001 0.79350001 -0.38429999 -0.47189999 0.79350001 -0.38429999 -0.47189999
		 0.79350001 -0.38429999 -0.47189999 0.79350001 -0.38429999 -0.47189999 -0.69330001
		 -0.69550002 0.18880001 -0.69330001 -0.69550002 0.18880001 -0.69330001 -0.69550002
		 0.18880001 -0.69330001 -0.69550002 0.18880001 0.84179997 -0.52069998 -0.14219999
		 0.84179997 -0.52069998 -0.14219999 0.84179997 -0.52069998 -0.14219999 0.84179997
		 -0.52069998 -0.14219999 0.0102 0.48280001 -0.87559998 0.0102 0.48280001 -0.87559998
		 0.0102 0.48280001 -0.87559998 0.0102 0.48280001 -0.87559998 0.53649998 0.4113 -0.73689997
		 0.53649998 0.4113 -0.73689997 0.53649998 0.4113 -0.73689997 0.53649998 0.4113 -0.73689997
		 -0.55800003 0.5607 -0.61180001 -0.55800003 0.5607 -0.61180001 -0.55800003 0.5607
		 -0.61180001 -0.55800003 0.5607 -0.61180001 -0.1577 0.53789997 -0.82810003 -0.1577
		 0.53789997 -0.82810003 -0.1577 0.53789997 -0.82810003 -0.1577 0.53789997 -0.82810003
		 -0.50459999 -0.62360001 0.59710002 -0.50459999 -0.62360001 0.59710002 -0.50459999
		 -0.62360001 0.59710002 -0.50459999 -0.62360001 0.59710002 -0.52329999 0.1557 0.83780003
		 -0.52329999 0.1557 0.83780003 -0.52329999 0.1557 0.83780003 -0.52329999 0.1557 0.83780003
		 -0.26989999 0.32080001 -0.90789998 -0.26989999 0.32080001 -0.90789998 -0.26989999
		 0.32080001 -0.90789998 -0.26989999 0.32080001 -0.90789998 -0.35710001 -0.9303 0.083700001
		 -0.35710001 -0.9303 0.083700001 -0.35710001 -0.9303 0.083700001 -0.35710001 -0.9303
		 0.083700001 -0.6645 0.74519998 -0.055199999 -0.6645 0.74519998 -0.055199999 -0.6645
		 0.74519998 -0.055199999 -0.6645 0.74519998 -0.055199999 -0.4461 -0.8373 -0.31600001
		 -0.4461 -0.8373 -0.31600001 -0.4461 -0.8373 -0.31600001 -0.4461 -0.8373 -0.31600001
		 -0.58209997 0.72289997 -0.37220001 -0.58209997 0.72289997 -0.37220001 -0.58209997
		 0.72289997 -0.37220001 -0.58209997 0.72289997 -0.37220001 -0.36559999 -0.67439997
		 -0.6415 -0.36559999 -0.67439997 -0.6415 -0.36559999 -0.67439997 -0.6415 -0.36559999
		 -0.67439997 -0.6415 -0.4373 0.4025 0.80419999 -0.4373 0.4025 0.80419999 -0.4373 0.4025
		 0.80419999 -0.4373 0.4025 0.80419999 -0.546 0.736 0.4003 -0.546 0.736 0.4003 -0.546
		 0.736 0.4003 -0.546 0.736 0.4003 -0.36899999 -0.90219998 0.22319999 -0.36899999 -0.90219998
		 0.22319999 -0.36899999 -0.90219998 0.22319999 -0.36899999 -0.90219998 0.22319999;
	setAttr -s 66 -ch 264 ".fc[0:65]" -type "polyFaces" 
		f 4 0 1 2 3
		f 4 4 5 6 7
		f 4 8 9 10 11
		f 4 12 -7 13 14
		f 4 15 16 17 18
		f 4 19 20 21 22
		f 4 23 24 25 26
		f 4 -26 27 28 29
		f 4 -11 30 31 32
		f 4 -32 33 34 35
		f 4 36 37 -31 -10
		f 4 38 39 -34 -38
		f 4 40 41 42 43
		f 4 -43 44 45 46
		f 4 47 48 49 -28
		f 4 50 51 -48 -25
		f 4 52 53 54 55
		f 4 56 -56 57 58
		f 4 59 60 -16 61
		f 4 62 -59 63 64
		f 4 65 -17 66 67
		f 4 68 69 70 -21
		f 4 -14 -6 -60 71
		f 4 72 -47 73 -49
		f 4 74 -44 -73 -52
		f 4 -55 75 -39 76
		f 4 -58 -77 -37 77
		f 4 -64 -78 -9 78
		f 4 -67 -61 -5 79
		f 4 80 -4 81 -70
		f 4 82 -79 83 84
		f 4 85 -72 86 87
		f 4 88 -65 -83 89
		f 4 -87 -62 90 91
		f 4 92 -36 93 94
		f 4 95 -33 -93 96
		f 4 97 98 99 -53
		f 4 -63 100 -98 -57
		f 4 101 102 -18 -66
		f 4 -91 -19 103 104
		f 4 105 -15 -86 106
		f 4 -84 -12 -96 107
		f 4 108 -108 109 -41
		f 4 110 -107 111 -2
		f 4 112 -105 -22 -71
		f 4 -110 -97 113 -42
		f 4 -114 -95 114 -45
		f 4 115 -92 -113 -82
		f 4 -24 -90 116 -51
		f 4 -112 -88 -116 -3
		f 4 -117 -85 -109 -75
		f 4 117 -30 118 -99
		f 4 -89 -27 -118 -101
		f 4 119 -23 -104 -103
		f 4 -119 120 -120 121
		f 4 -115 122 -111 123
		f 4 -94 124 -106 -123
		f 4 -100 -122 -102 125
		f 4 -74 126 -81 127
		f 4 128 -80 129 -76
		f 4 -50 -128 -69 130
		f 4 -126 -68 -129 -54
		f 4 -29 -131 -20 -121
		f 4 -35 131 -13 -125
		f 4 -130 -8 -132 -40
		f 4 -46 -124 -1 -127;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "9035157D-4249-40E8-4987-67A95FA8544A";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "CCA2AAF0-47D4-13DC-BCD2-54B18B4D1C74";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "2BC37AFA-457F-9CF6-EDE5-73959BDB4454";
createNode displayLayerManager -n "layerManager";
	rename -uid "5C8BBBAF-47C7-687C-2A7D-088EA6F2F81E";
createNode displayLayer -n "defaultLayer";
	rename -uid "7A6DDED3-48CA-E408-EB6F-B2AAD5EFD568";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "53E1535A-4D44-01E0-3741-279BFD4E40EA";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "DA0B35F8-41FC-8C85-186B-0F87AC6D02C6";
	setAttr ".g" yes;
createNode shadingEngine -n "anotherRock:Material";
	rename -uid "C916504C-4AB2-6B93-147A-1AAD7728FA08";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "anotherRock:materialInfo1";
	rename -uid "0B097ABE-4318-F032-8AB1-6B9895377441";
createNode phong -n "anotherRock:Material1";
	rename -uid "461D196D-4CA6-B672-FECC-5B8246A8731C";
	setAttr ".c" -type "float3" 0.63999999 0.63999999 0.63999999 ;
	setAttr ".ambc" -type "float3" 1 1 1 ;
	setAttr ".cp" 98.07843017578125;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "83583A89-4FB7-2568-C799-4594203BBBAA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 1.2000347354578098 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".a" 180;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "4551F352-47A4-5270-D37C-66BB31F6215A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 11 "e[24]" "e[26]" "e[30]" "e[32]" "e[36]" "e[41]" "e[43]" "e[51]" "e[56:57]" "e[96]" "e[100]";
	setAttr ".ix" -type "matrix" 1.2000347354578098 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".wt" 0.52217966318130493;
	setAttr ".re" 36;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "anotherRock:polyTweak1";
	rename -uid "F9A7D49E-4F07-9BE3-6E7D-949B7A079DDF";
	setAttr ".uopa" yes;
	setAttr -s 23 ".tk";
	setAttr ".tk[3]" -type "float3" 0.0019610338 0.044467434 0.031778745 ;
	setAttr ".tk[6]" -type "float3" 0.022112397 0.052241243 0.077786818 ;
	setAttr ".tk[16]" -type "float3" 0.072967969 0 0 ;
	setAttr ".tk[17]" -type "float3" -0.072408497 0 0 ;
	setAttr ".tk[18]" -type "float3" 0.072967969 0 0 ;
	setAttr ".tk[19]" -type "float3" -0.061313268 0 0 ;
	setAttr ".tk[20]" -type "float3" 0.015114716 0 0 ;
	setAttr ".tk[21]" -type "float3" -0.0049917428 0 0 ;
	setAttr ".tk[22]" -type "float3" 0.015114716 0 0 ;
	setAttr ".tk[23]" -type "float3" -0.0049917428 0 0 ;
	setAttr ".tk[34]" -type "float3" 0.066556543 0.071761407 0.096659228 ;
	setAttr ".tk[40]" -type "float3" -0.11718315 0 0 ;
	setAttr ".tk[41]" -type "float3" 0.085017473 0 0 ;
	setAttr ".tk[42]" -type "float3" -0.027522685 0.032122187 0 ;
	setAttr ".tk[46]" -type "float3" -0.09155295 0.045400739 -0.055261455 ;
	setAttr ".tk[47]" -type "float3" 0.091854617 0 0 ;
	setAttr ".tk[48]" -type "float3" -0.018390853 0 0 ;
	setAttr ".tk[52]" -type "float3" 0.035375707 0.020483891 0.072790578 ;
	setAttr ".tk[53]" -type "float3" -0.028993053 -0.055555157 0.0016614245 ;
	setAttr ".tk[56]" -type "float3" -0.035225485 -0.023700671 0.033801418 ;
	setAttr ".tk[60]" -type "float3" -0.035339568 0 0 ;
	setAttr ".tk[61]" -type "float3" -0.041318063 0 0 ;
	setAttr ".tk[66]" -type "float3" 0 0 0.025463065 ;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "D663992F-4EFF-0F3E-CE22-6E9C50FB3D29";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 268\n            -height 384\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 268\n            -height 383\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 268\n            -height 383\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 268\n            -height 384\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n"
		+ "            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n"
		+ "            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n"
		+ "            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n"
		+ "            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 0\n                -outliner \"graphEditor1OutlineEd\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n"
		+ "                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n"
		+ "                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 268\\n    -height 384\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 268\\n    -height 384\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 268\\n    -height 384\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 268\\n    -height 384\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 268\\n    -height 383\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 268\\n    -height 383\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 268\\n    -height 383\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 268\\n    -height 383\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "7C4AFD2F-499C-D924-4ADF-5D8626CD1D0A";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyPlanarProj -n "polyPlanarProj1";
	rename -uid "250439C3-4B32-C73A-AA06-F88B8D86027D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:77]";
	setAttr ".ix" -type "matrix" 1.2000347354578098 0 0 0 0 0.91127597028534646 0 0 0 0 1 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.045284509658813477 0.054004967212677002 -0.0024899840354919434 ;
	setAttr ".ps" -type "double2" 3.1442108154296875 3.1710479648146421 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweak -n "anotherRock:polyTweak2";
	rename -uid "9AE71ABD-4E71-5E1C-9F70-14B4147BD7DA";
	setAttr ".uopa" yes;
	setAttr -s 15 ".tk";
	setAttr ".tk[0]" -type "float3" -0.041728575 -0.027852055 -0.010731823 ;
	setAttr ".tk[1]" -type "float3" -0.03134298 0.016459379 -0.0095300572 ;
	setAttr ".tk[8]" -type "float3" 0.006995515 -0.022560637 0.0036950819 ;
	setAttr ".tk[68]" -type "float3" 0.066053212 0.059562884 0.023452921 ;
	setAttr ".tk[69]" -type "float3" 0.066053212 0.039444014 0.041384615 ;
	setAttr ".tk[70]" -type "float3" 0.066053212 0.0084779747 0.05461574 ;
	setAttr ".tk[71]" -type "float3" 0.066053212 -0.022191796 0.060292803 ;
	setAttr ".tk[72]" -type "float3" 0.066053212 -0.049393233 0.051669531 ;
	setAttr ".tk[73]" -type "float3" 0.066053212 -0.057651214 0.023645408 ;
	setAttr ".tk[74]" -type "float3" 0.066053212 -0.059562884 -0.013834402 ;
	setAttr ".tk[75]" -type "float3" 0.066427514 -0.019884208 -0.050034069 ;
	setAttr ".tk[76]" -type "float3" 0.066053212 -0.023250537 -0.060292803 ;
	setAttr ".tk[77]" -type "float3" 0.066053212 0.00215069 -0.049437635 ;
	setAttr ".tk[78]" -type "float3" 0.066053212 0.035555515 -0.041500989 ;
	setAttr ".tk[79]" -type "float3" 0.066053212 0.055441264 -0.013834402 ;
createNode polyMapCut -n "anotherRock:polyMapCut1";
	rename -uid "B7DDCD61-4257-A0D8-2D1C-5CA3D25A5B91";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[3]" "e[43]" "e[46]" "e[108]" "e[115]" "e[126]" "e[141]";
createNode polyTweakUV -n "anotherRock:polyTweakUV1";
	rename -uid "1B7E6A4B-4FE9-2144-C476-A9BE191885C4";
	setAttr ".uopa" yes;
	setAttr -s 86 ".uvtk[0:85]" -type "float2" 0.48691425 -0.27370983 0.12580824
		 0.60278773 0.4069775 0.51691288 0.45470265 0.5761373 0.09074463 0.020309977 0.31987587
		 0.12029338 0.3015658 0.14262785 0.088472649 0.078350067 -0.81594604 0.15004818 -0.62531084
		 -0.04879145 -0.56825715 0.086684287 -0.64421928 0.17836629 0.15175478 0.30609629
		 0.39068505 0.26526242 0.40680823 0.16055016 0.26800847 0.069296494 0.087684691 0.027909834
		 0.16552684 0.072290756 0.057585187 0.13490298 0.0335669 0.18940288 0.16429436 0.19121593
		 0.27389506 0.1537827 -0.61315358 0.12882607 -0.6045478 0.20840101 -0.4797366 0.2080766
		 -0.47937265 0.13930686 -0.3056905 0.14569883 -0.29689613 0.20778997 -0.15998277 0.20147589
		 -0.14737719 0.14592806 -0.4482711 0.032332316 -0.51716274 0.21475655 -0.33949417
		 0.24774206 -0.28313944 0.0032376091 -0.15371203 0.012141974 -0.09298563 0.26178476
		 -0.47446877 -0.094294161 -0.27871552 -0.10514311 -0.1542446 -0.10199916 -0.89187998
		 0.5310272 -0.70578873 0.5063445 -0.55643463 0.58381057 -0.73800594 0.068869561 -0.40204826
		 -0.13664363 -0.33261698 0.62536854 -0.17903058 0.63749152 -0.18216822 0.76977396
		 -0.32964444 0.16153742 -0.15568373 0.16364242 -0.77341396 0.23616597 -0.59376699
		 0.20738009 -0.27648994 -0.080611989 -0.15254399 -0.072969913 -0.14985168 -0.14387199
		 -0.28296277 -0.14741853 -0.51043004 -0.02616594 -0.41660741 -0.065075994 -0.49676502
		 -0.12149792 -0.66951299 -0.058205295 0.46974948 0.23919266 -0.57643491 0.068393014
		 -0.78065968 0.093872078 -0.030010387 -0.047489464 0.066539064 -0.028279938 0.17662346
		 0.21468785 0.49177018 0.23898451 -0.85326493 0.19159487 -0.93907714 0.27210674 0.47354689
		 0.34864298 0.57769197 0.32974297 0.54243392 0.21812844 -0.34595528 0.45644519 -0.091551401
		 0.46720853 -0.71087867 0.3158198 -0.55493522 0.40268326 -0.29554868 0.049387995 -0.14792636
		 0.054260027 -0.45832154 0.05452146 0.0068730656 0.057903908 0.18820406 0.43328547
		 -0.67872947 0.67649502 -0.89451844 0.23434813 -0.13859709 -0.22449267 0.16117515
		 0.70169222 0.63689536 0.20280303 -0.3723076 0.7504698;
createNode polyMapDel -n "anotherRock:polyMapDel1";
	rename -uid "77AE8BC5-436C-9F8D-B2C1-5F8F38FA4507";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:77]";
createNode polyPlanarProj -n "anotherRock:polyPlanarProj1";
	rename -uid "97840052-48EB-A196-AE21-FB80130259D8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "f[0]" "f[11:16]" "f[21]" "f[23:26]" "f[29]" "f[58:61]" "f[64:66]" "f[70:72]" "f[76:77]";
	setAttr ".ix" -type "matrix" 1.2000347354578098 0 0 0 0 0.91127597028534646 0 0 0 0 1 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.04344630241394043 0.054004967212677002 -0.0032165050506591797 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 2.6065511492615503 2.6497032819962398 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "anotherRock:polyTweakUV2";
	rename -uid "8211C90F-462C-A79D-D4A9-198189B9D0AE";
	setAttr ".uopa" yes;
	setAttr -s 42 ".uvtk[0:41]" -type "float2" 0.18093947 0.25105992 0.27674025
		 0.37553915 0.4130739 0.44796774 0.35494164 0.37243327 0.021895938 0.36371306 0.14973441
		 0.46209863 0.048880637 0.59314352 -0.067038238 0.47927013 -0.60884976 -0.3686403
		 -0.38286003 -0.1196638 -0.21113367 0.034483317 -0.39435959 -0.18607883 -0.15020493
		 0.010859931 -0.010391656 0.19252703 0.11744701 0.29091325 -0.022366257 0.10924618
		 -0.59173006 -0.56104988 -0.38347015 -0.28837085 -0.24592943 -0.1825178 -0.46158913
		 -0.46146032 -0.77133119 -0.64507282 -0.75389373 -0.56003588 -0.59635693 -0.44792861
		 -0.73106241 -0.64959013 0.40557629 -0.13482496 0.53442693 -0.037753552 0.37624651
		 0.17276242 0.2387058 0.06690938 -0.24689499 -0.27774116 0.047765087 0.067457505 0.29912791
		 0.28140262 -0.10462727 -0.15718952 -0.18237497 -0.20475565 0.0096504027 -0.092648394
		 -0.16220725 0.18847187 -0.28244141 0.029774277 0.55328113 0.3722451 0.40735415 0.6052205
		 0.64908957 0.071435533 0.30559936 0.73743606 -0.21976164 0.34297618 0.24173573 -0.22075698;
createNode polyPlanarProj -n "anotherRock:polyPlanarProj2";
	rename -uid "9EB8B444-461C-3FD8-D333-B881CB99B67B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "f[3]" "f[8:9]" "f[34:37]" "f[40]" "f[51:52]" "f[54]" "f[56:57]" "f[63]" "f[67:68]" "f[74:75]";
	setAttr ".ix" -type "matrix" 1.2000347354578098 0 0 0 0 0.91127597028534646 0 0 0 0 1 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.17977255582809448 0.18367631733417511 -0.0024899840354919434 ;
	setAttr ".ps" -type "double2" 2.5652474164962769 2.4327544860719712 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "anotherRock:polyTweakUV3";
	rename -uid "4EA984C2-442C-7B40-2935-DC9C884D855A";
	setAttr ".uopa" yes;
	setAttr -s 75 ".uvtk[0:74]" -type "float2" 0.18963858 -0.2142379 0.16097254
		 -0.2142379 0.17668475 -0.26395962 0.19305225 -0.26532343 -0.12570308 0.037514482
		 -0.12785658 -3.6732374e-005 -0.11303736 0.018231809 -0.12086073 0.042132758 0.20977513
		 -6.0976632e-005 0.17005657 -0.035842773 0.14943086 -0.067181095 0.19580381 -0.050776374
		 0.18694769 -0.11651345 0.14382064 -0.12103207 0.14037095 -0.15606619 0.18388236 -0.15324751
		 0.32754499 -0.095949985 0.26312867 -0.10623965 0.26116782 -0.14791535 0.32541451
		 -0.13199215 0.29487425 -0.024425674 0.25740787 0.0038972171 0.26347628 -0.041132536
		 0.31671217 -0.049470279 -0.22037037 0.044972852 -0.23120128 0.015527216 -0.20229422
		 -0.00079156732 -0.19266053 0.039194196 0.30824685 -0.1802624 0.23898304 -0.21687156
		 0.22071281 -0.27304101 0.29857588 -0.21318515 -0.15625492 0.12134974 -0.17285669
		 0.093314812 -0.12484079 0.085690759 -0.12242054 0.11148459 -0.19382307 -0.034986962
		 -0.14931189 -0.039773222 -0.2353414 -0.011858017 -0.12439869 -0.030553043 -0.13110504
		 0.073544122 -0.19737193 0.075729042 -0.12933891 0.1308752 -0.018431349 -0.12057842
		 0.12126674 0.10941993 0.088720471 0.27946842 -0.77292114 -0.45754111 -0.63391232
		 -0.52496535 -0.49839157 -0.51245534 -0.68673462 -0.3543531 -0.54148638 -0.22857812
		 -0.32015824 -0.44715312 -0.20436965 -0.35870889 -0.32467723 -0.077218428 -0.66982108
		 -0.025204433 -0.44627026 0.12795931 -0.9054808 -0.34384385 -0.82386231 -0.18384856
		 0.34895772 -0.27400175 0.17873909 -0.061963681 0.33681145 -0.16056259 0.4801718 -0.35173339
		 0.076587029 -0.025037169 -0.080373317 0.16734272 0.017055085 0.059524626 0.18978448
		 -0.15493302 -0.18005991 0.26732728 0.085764386 0.40014189 0.041890118 0.096144177
		 0.21623698 -0.013645703 -0.19515517 0.29255825 -0.11603501 0.20967096 0.46488386
		 -0.17887042 0.51940721 -0.27220905 0.59849256 -0.40570617;
createNode polyPlanarProj -n "anotherRock:polyPlanarProj3";
	rename -uid "85C726A8-4C40-B050-0389-BDBF41B7AAAD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "f[1]" "f[4]" "f[18]" "f[20]" "f[22]" "f[28]" "f[31]" "f[33]" "f[38:39]" "f[53]";
	setAttr ".ix" -type "matrix" 1.2000347354578098 0 0 0 0 0.91127597028534646 0 0 0 0 1 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -1.0308723896741867 0.17865248024463654 -0.20633149147033691 ;
	setAttr ".ro" -type "double3" -89.999999999999986 -3.180554681463516e-015 45.07087324513185 ;
	setAttr ".ps" -type "double2" 1.8361862771036757 1.9413349628448486 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "anotherRock:polyTweakUV4";
	rename -uid "49FC62D5-4B42-547E-C402-C1B3CEDEA6AB";
	setAttr ".uopa" yes;
	setAttr -s 93 ".uvtk[0:92]" -type "float2" 0.22452474 0.42472419 0.20857263
		 0.42472419 0.21725988 0.40038952 0.22718382 0.40071353 -0.081246309 0.10519107 -0.082128353
		 0.090735897 -0.061685264 0.08639501 -0.06126488 0.10409377 0.23342957 0.51416516
		 0.20828371 0.507249 0.20190386 0.49088076 0.22692703 0.49302852 0.22250324 0.46711844
		 0.19952926 0.46819216 0.19895521 0.45313874 0.22183798 0.45246902 0.29324287 0.4628495
		 0.25817531 0.46601099 0.2571418 0.45076153 0.29257369 0.44741839 0.27976793 0.49740854
		 0.25783068 0.51221257 0.25858074 0.49094981 0.28933644 0.48373768 -0.14992021 0.10341895
		 -0.14898075 0.087037638 -0.11771742 0.090474792 -0.11850709 0.10612582 0.28640023
		 0.42600888 0.24896406 0.42170402 0.24179462 0.39615807 0.2824136 0.41163331 -0.11682473
		 0.15897615 -0.11888285 0.13367522 -0.08312083 0.13037659 -0.089635231 0.15208304
		 -0.11473117 0.060636424 -0.085881479 0.057940848 -0.14695209 0.070725195 -0.067622826
		 0.055749953 -0.059220966 0.12621647 -0.14919795 0.1225228 0.54400247 -0.13177331
		 0.50724244 -0.12448052 0.51712739 -0.15800649 0.53795958 -0.16766956 0.52728641 -0.0089589618
		 0.50820023 -0.017330207 0.5013625 -0.039468642 0.53454465 -0.031889509 0.53947347
		 -0.058876891 0.49628192 -0.06579981 0.49821806 -0.08621113 0.54135895 -0.094614878
		 0.57188755 -0.059214424 0.57222891 -0.096209474 0.55024314 -0.00054980756 0.56404942
		 -0.027689273 -0.77122623 0.4311209 -0.73422635 0.43353611 -0.7374016 0.45711073 -0.7729553
		 0.45105216 -0.76077187 0.39013675 -0.73601735 0.38242733 -0.73342478 0.40609127 -0.76667553
		 0.40834734 0.56199437 -0.13687851 0.5523234 -0.17672479 -0.70338619 0.43703517 -0.70759606
		 0.46185079 -0.7161122 0.38056698 -0.70619726 0.40744764 -0.71952951 0.49265313 -0.74575746
		 0.48083708 -0.77232409 0.47117284 -0.27728808 -0.040887773 -0.20332293 -0.050500758
		 -0.20750608 -0.016482688 -0.26982516 0.0019920322 -0.16113374 -0.10020702 -0.21483062
		 -0.10019099 -0.2477309 -0.16700093 -0.21602045 -0.16955467 -0.15097322 -0.047209363
		 -0.29320493 -0.18750082 -0.2820875 -0.11121425 -0.1657455 -0.0087602176 -0.13386434
		 2.3808701e-005 -0.121686 -0.039773464 -0.11694883 -0.095389761 -0.2467148 -0.20346195
		 -0.16089043 -0.18312141 -0.20173265 -0.23562266;
createNode polyPlanarProj -n "anotherRock:polyPlanarProj4";
	rename -uid "88D5FFE9-4D8E-C45B-6C99-DEB70FAD533F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[17]" "f[19]" "f[32]";
	setAttr ".ix" -type "matrix" 1.2000347354578098 0 0 0 0 0.91127597028534646 0 0 0 0 1 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 1.0253388285636902 0.25019210577011108 -0.47163549810647964 ;
	setAttr ".ro" -type "double3" 29.467697451747263 -43.226547701913098 0 ;
	setAttr ".ps" -type "double2" 1.2651751745281077 1.2170207500457764 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "anotherRock:polyTweakUV5";
	rename -uid "8B0D4889-4FA3-2E9E-B236-B1BE12805C1D";
	setAttr ".uopa" yes;
	setAttr -s 101 ".uvtk[0:100]" -type "float2" -0.079698026 -0.58513135
		 -0.094813019 -0.58513135 -0.086581647 -0.60818863 -0.077178642 -0.60788155 -0.21545391
		 0.071896054 -0.21628965 0.058199558 -0.19691962 0.054086525 -0.19652128 0.070856266
		 -0.071260735 -0.50038499 -0.095086642 -0.50693822 -0.1011316 -0.52244723 -0.077421874
		 -0.52041221 -0.081613474 -0.54496229 -0.10338166 -0.54394495 -0.1039255 -0.55820817
		 -0.082243875 -0.55884278 -0.014586974 -0.54900706 -0.047813863 -0.54601163 -0.048793137
		 -0.56046069 -0.015221052 -0.56362826 -0.02735463 -0.51626205 -0.048140451 -0.50223511
		 -0.047429718 -0.52238172 -0.018288312 -0.5292154 -0.28052312 0.070216984 -0.27963296
		 0.054695487 -0.25001061 0.057952203 -0.25075886 0.072781682 -0.021070492 -0.58391398
		 -0.056541659 -0.58799297 -0.063334741 -0.61219805 -0.024847835 -0.59753495 -0.2491648
		 0.12285793 -0.2511149 0.098885074 -0.21723005 0.095759585 -0.22340252 0.11632662
		 -0.24718113 0.029679999 -0.2198458 0.027125897 -0.2777108 0.039239209 -0.20254549
		 0.025050053 -0.19458465 0.091817819 -0.27983877 0.088317983 -0.18842737 0.37042537
		 -0.22325787 0.37733534 -0.21389176 0.34556898 -0.19415316 0.33641317 -0.204266 0.48679319
		 -0.2223504 0.4788613 -0.22882916 0.45788491 -0.1973888 0.46506619 -0.19271867 0.43949538
		 -0.23364316 0.43293583 -0.23180863 0.41359594 -0.19093217 0.4056333 -0.16200596 0.43917558
		 -0.1616825 0.40412241 -0.18251431 0.4947609 -0.16943265 0.46904597 -0.064236633 -0.093529649
		 -0.029178826 -0.091241203 -0.032187402 -0.068903983 -0.065874927 -0.074644499 -0.054330926
		 -0.13236256 -0.030875769 -0.13966739 -0.028419316 -0.11724553 -0.059924733 -0.11510777
		 -0.17137989 0.3655881 -0.18054326 0.32783329 4.259534e-005 -0.087925762 -0.0039463434
		 -0.064412713 -0.012015452 -0.14142993 -0.0026210158 -0.11596025 -0.015253452 -0.035227165
		 -0.040104713 -0.046423011 -0.065276876 -0.055579975 0.38135087 0.20826516 0.45143378
		 0.19915673 0.44652587 0.22886786 0.39360866 0.2430461 0.49552718 0.16180648 0.44216594
		 0.15471554 0.4168399 0.11496081 0.44733906 0.11296317 0.50095868 0.20684637 0.37599936
		 0.090454839 0.37667483 0.1477751 0.48077041 0.23652537 0.51067388 0.24691178 0.52532554
		 0.21340115 0.53833389 0.16823201 0.42007637 0.11336315 0.50303006 0.11009599 0.46637428
		 0.097583354 -0.057271201 -0.50972122 0.076815739 -0.76563221 0.026674993 -1.077929854
		 -0.19363999 -0.55532509 -0.21785375 0.022685824 -0.44303879 0.22196163 -0.31607369
		 0.41696015 -0.61032653 0.78362828;
createNode polyPlanarProj -n "anotherRock:polyPlanarProj5";
	rename -uid "27B11595-44FE-2A4F-42B4-7E8771CF37AC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[2]" "f[10]" "f[41]";
	setAttr ".ix" -type "matrix" 1.2000347354578098 0 0 0 0 0.91127597028534646 0 0 0 0 1 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 1.0565468370914459 0.28764745593070984 0.59122249484062195 ;
	setAttr ".ro" -type "double3" -27.179709296990222 45.298254434384219 -1.0992174535028203 ;
	setAttr ".ps" -type "double2" 1.2142582255897389 1.2651545405387878 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "anotherRock:polyTweakUV6";
	rename -uid "248E18ED-4DE7-A23B-4229-3F855798F15B";
	setAttr ".uopa" yes;
	setAttr -s 109 ".uvtk[0:108]" -type "float2" -0.64401728 0.27180454 -0.65354586
		 0.27180454 -0.64835674 0.25726897 -0.64242899 0.25746253 0.58408207 -0.27183673 0.58355522
		 -0.28047115 0.59576625 -0.28306404 0.59601736 -0.27249223 -0.63869828 0.32522959
		 -0.65371841 0.32109836 -0.65752918 0.31132129 -0.64258236 0.31260419 -0.64522475
		 0.29712757 -0.65894765 0.29776889 -0.65929049 0.2887772 -0.64562213 0.28837711 -0.60297048
		 0.29457766 -0.6239171 0.29646605 -0.62453443 0.28735721 -0.60337025 0.28536031 -0.61101937
		 0.31522048 -0.62412298 0.32406321 -0.62367493 0.31136253 -0.60530388 0.30705455 0.54306167
		 -0.27289528 0.54362285 -0.28268015 0.56229711 -0.28062713 0.56182545 -0.27127841
		 -0.60705781 0.27257195 -0.62941921 0.27000055 -0.63370162 0.25474137 -0.60943907
		 0.26398513 0.56283033 -0.23970981 0.56160092 -0.25482258 0.58296239 -0.25679293 0.57907116
		 -0.24382721 0.56408089 -0.2984502 0.58131337 -0.30006036 0.54483467 -0.29242396 0.59221965
		 -0.30136901 0.59723824 -0.25927785 0.54349309 -0.26148418 0.41946426 -0.28761506
		 0.39750671 -0.28325891 0.40341121 -0.3032847 0.41585463 -0.30905667 0.40947941 -0.21425548
		 0.3980788 -0.21925579 0.39399451 -0.23247956 0.41381487 -0.22795238 0.41675895 -0.24407251
		 0.39095974 -0.24820772 0.39211622 -0.26039982 0.41788521 -0.26541954 0.43612063 -0.24427411
		 0.43632451 -0.26637203 0.42319193 -0.20923251 0.43143874 -0.2254435 -0.040458731
		 -0.48425159 -0.01835792 -0.48280889 -0.020254558 -0.46872729 -0.041491531 -0.47234619
		 -0.034214068 -0.50873226 -0.019427689 -0.51333731 -0.017879115 -0.49920231 -0.037740465
		 -0.49785468 0.43021116 -0.29066446 0.42443442 -0.31446555 6.3587664e-005 -0.48071882
		 -0.0024510825 -0.46589595 -0.0075379382 -0.51444846 -0.0016155813 -0.49839208 -0.0095792096
		 -0.44749707 -0.025245715 -0.45455503 -0.041114517 -0.46032771 -0.20633717 0.27187181
		 -0.16215621 0.26612973 -0.16440544 0.28711557 -0.20324956 0.2990298 -0.13804384 0.23386416
		 -0.16946216 0.23575106 -0.19065897 0.18962289 -0.17183737 0.18798597 -0.1308663 0.26688817
		 -0.21841514 0.17872046 -0.20916998 0.22824088 -0.13805443 0.29163843 -0.11893074
		 0.29634017 -0.11247868 0.27145946 -0.11189988 0.23624992 -0.1906527 0.15913914 -0.13981976
		 0.17724372 -0.1647553 0.13604724 0.44773188 -0.040926676 0.46965909 -0.0019393392
		 0.37818682 4.4837087e-008 0.36176351 -0.069675848 0.47715092 -0.094011925 0.39898157
		 -0.1504261 0.50859731 -0.12880725 0.43072423 -0.20689353 -0.30575615 -0.054325089
		 -0.24899958 -0.18468192 -0.17179984 -0.12528041 -0.17913561 -0.071161106 -0.17905381
		 -0.23135 -0.11104003 -0.16829324 -0.31096691 0.023016568 -0.18179315 -0.0052086264;
createNode polyPlanarProj -n "anotherRock:polyPlanarProj6";
	rename -uid "983038EA-406F-78C7-6720-4F994BD79E57";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "f[27]" "f[30]";
	setAttr ".ix" -type "matrix" 1.2000347354578098 0 0 0 0 0.91127597028534646 0 0 0 0 1 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 1.293734610080719 0.18673595786094666 0.097900494933128357 ;
	setAttr ".ro" -type "double3" -20.100763279254878 90 0 ;
	setAttr ".ps" -type "double2" 1.0334775923357975 1.0901084542274475 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "anotherRock:polyTweakUV7";
	rename -uid "4A2DE454-4E29-E583-4D8D-BFA87A38B1CE";
	setAttr ".uopa" yes;
	setAttr -s 115 ".uvtk[0:114]" -type "float2" 0.33190161 0.42521444 0.32580301
		 0.42521444 0.32912421 0.41591117 0.33291817 0.41603503 -0.24155439 -0.51344889 -0.24189161
		 -0.5189752 -0.23407611 -0.52063471 -0.23391537 -0.51386845 0.33530593 0.45940816
		 0.32569256 0.45676401 0.32325354 0.45050639 0.33282 0.45132747 0.33112881 0.44142199
		 0.32234567 0.44183242 0.32212624 0.43607748 0.33087441 0.43582135 0.35817286 0.43978992
		 0.34476638 0.44099852 0.34437126 0.43516865 0.35791704 0.43389052 0.35302132 0.45300207
		 0.34463459 0.45866162 0.34492138 0.45053279 0.35667944 0.44777554 -0.26780874 -0.51412636
		 -0.26744956 -0.52038902 -0.25549746 -0.51907504 -0.25579935 -0.51309156 0.35555688
		 0.42570555 0.34124485 0.42405981 0.33850396 0.4142935 0.35403278 0.42020971 -0.25515616
		 -0.49288663 -0.255943 -0.5025593 -0.24227105 -0.50382036 -0.24476151 -0.4955219 -0.25435582
		 -0.53048235 -0.24332646 -0.53151292 -0.26667401 -0.52662539 -0.23634607 -0.53235048
		 -0.23313399 -0.50541079 -0.26753262 -0.50682294 -0.35863096 -0.13636972 -0.37268451
		 -0.13358165 -0.36890548 -0.14639881 -0.36094123 -0.15009306 -0.36502159 -0.089417234
		 -0.37231836 -0.092617594 -0.37493247 -0.10108122 -0.36224681 -0.098183684 -0.3603625
		 -0.10850109 -0.37687483 -0.11114775 -0.37613457 -0.11895109 -0.35964164 -0.12216389
		 -0.34797043 -0.10863013 -0.34783986 -0.12277351 -0.35624519 -0.086202368 -0.35096693
		 -0.09657792 0.88019329 0.21424618 0.89433855 0.21516953 0.89312458 0.22418222 0.87953228
		 0.22186601 0.88419002 0.19857772 0.89365381 0.19563039 0.89464498 0.20467722 0.88193303
		 0.20553972 -0.35175267 -0.13832146 -0.35544991 -0.15355492 0.90612888 0.21650723
		 0.90451938 0.22599436 0.90126365 0.19491918 0.90505415 0.20519577 0.89995718 0.23777024
		 0.88993007 0.23325291 0.8797735 0.22955824 -0.026834251 0.078294754 0.0014429975
		 0.074619666 3.4182758e-006 0.088051304 -0.024858067 0.095676795 0.016875692 0.053968634
		 -0.003233053 0.055176321 -0.016799685 0.025652859 -0.0047532525 0.024605205 0.021469546
		 0.075105086 -0.034564536 0.018674932 -0.028647333 0.050369561 0.016868912 0.090946048
		 0.02910869 0.093955331 0.033238217 0.078030907 0.033608697 0.055495657 -0.016795672
		 0.0061422619 0.015739048 0.017729787 -0.00022051352 -0.0086372914 0.13358769 0.51499408
		 0.14762183 0.53994721 0.089076638 0.54118854 0.078565113 0.49659368 0.15241675 0.4810178
		 0.1023859 0.44491097 0.17254353 0.45874766 0.12270221 0.40876994 0.34810737 -0.058652647
		 0.37926278 -0.15763593 0.43049899 -0.11368051 0.42914852 -0.06942825 0.4288643 -0.19228692
		 0.46972144 -0.14396915 0.34065983 0.001023908 0.42348137 -0.017426861 -0.31736532
		 -0.035825644 -0.31329978 -0.14065315 -0.20195504 -0.15777011 -0.1921095 -0.03468173
		 -0.32493728 0.038936466 -0.17793091 0.038396016;
createNode polyPlanarProj -n "anotherRock:polyPlanarProj7";
	rename -uid "77F6AEC0-4F1E-80C5-604E-0C88F937F757";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "f[43]" "f[45:46]" "f[55]" "f[69]";
	setAttr ".ix" -type "matrix" 1.2000347354578098 0 0 0 0 0.91127597028534646 0 0 0 0 1 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.17977255582809448 -0.44743282347917557 0.90634101629257202 ;
	setAttr ".ro" -type "double3" -133.69051639365094 0 0 ;
	setAttr ".ps" -type "double2" 2.7828743705691208 2.8443503153992338 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "anotherRock:polyTweakUV8";
	rename -uid "E1663DAE-418E-9CFC-2B02-D7BDCB592C0E";
	setAttr ".uopa" yes;
	setAttr -s 127 ".uvtk[0:126]" -type "float2" 0.28128451 -0.79124731 0.2773568
		 -0.79124731 0.27949578 -0.79723889 0.28193924 -0.79715914 0.087416761 0.61139959
		 0.087199599 0.60784042 0.092233047 0.60677165 0.092336573 0.6111294 0.28347701 -0.7692253
		 0.27728564 -0.7709282 0.27571487 -0.77495831 0.281876 -0.7744295 0.28078678 -0.78080904
		 0.27513015 -0.7805447 0.27498886 -0.78425109 0.28062299 -0.78441602 0.29820412 -0.78186011
		 0.28956988 -0.78108174 0.2893154 -0.78483641 0.29803938 -0.78565955 0.29488638 -0.77335107
		 0.28948504 -0.76970607 0.28966975 -0.77494133 0.29724234 -0.77671707 0.070508026
		 0.61096328 0.070739336 0.6069299 0.078436933 0.60777617 0.078242488 0.61162972 0.2965194
		 -0.79093099 0.28730193 -0.79199094 0.28553668 -0.79828084 0.29553777 -0.79447049
		 0.078656726 0.62464243 0.078149982 0.61841291 0.08695522 0.61760068 0.085351266 0.62294525
		 0.079172187 0.60042942 0.086275488 0.59976572 0.071238823 0.60291344 0.090771116
		 0.5992263 0.092839807 0.61657637 0.070685834 0.61566699 0.06830968 -0.17086138 0.059258692
		 -0.16906576 0.061692562 -0.17732047 0.066821784 -0.17969969 0.064193889 -0.14062227
		 0.059494518 -0.14268343 0.057810955 -0.14813431 0.065980986 -0.14626819 0.067194551
		 -0.15291297 0.056560002 -0.15461752 0.057036724 -0.15964316 0.067658782 -0.16171232
		 0.075175494 -0.15299608 0.075259544 -0.16210492 0.06984622 -0.13855179 0.073245592
		 -0.14523402 0.0011574369 0.12983879 0.010267429 0.13043353 0.0094856871 0.13623801
		 0.00073165889 0.13474631 0.003731515 0.11974781 0.0098264776 0.11784961 0.010464851
		 0.12367606 0.0022778742 0.12423161 0.072739616 -0.17211837 0.07035844 -0.18192926
		 0.01786086 0.13129507 0.016824352 0.13740513 0.014727472 0.11739153 0.017168693 0.12401004
		 0.013886105 0.14498918 0.0074283122 0.14207986 0.00088707986 0.13970032 -0.096423283
		 -0.57572842 -0.078211702 -0.57809532 -0.079138845 -0.56944489 -0.095150545 -0.56453383
		 -0.068272486 -0.59139532 -0.081223249 -0.59061754 -0.089960657 -0.60963178 -0.082202323
		 -0.6103065 -0.065313876 -0.57778269 -0.10140186 -0.61412579 -0.097590975 -0.59371328
		 -0.068276852 -0.56758052 -0.060393997 -0.56564248 -0.05773443 -0.57589841 -0.057495847
		 -0.5904119 -0.089958064 -0.62219727 -0.069004521 -0.61473447 -0.079283059 -0.63171589
		 -0.37404728 0.052462343 -0.3650088 0.068533093 -0.40271404 0.069332495 -0.40948379
		 0.040611822 -0.36192065 0.0305804 -0.39414233 0.007326189 -0.34895828 0.016237631
		 -0.38105792 -0.015949918 0.0985649 0.20107611 0.11863016 0.13732728 0.15162815 0.1656362
		 0.15075842 0.19413628 0.15057534 0.11501078 0.17688885 0.14612922 0.093768433 0.23951001
		 0.14710854 0.22762704 -0.089396067 -0.059854262 -0.086900815 -0.146929 -0.011905039
		 -0.16634347 -0.0087267766 -0.059117533 -0.094760388 0.0033831499 1.0372637e-010 0.0024365508
		 0.51999015 -0.34444043 0.53217548 -0.31928343 0.87054348 -0.47693288 0.80781466 -0.47851112
		 -0.42932659 0.052922472 -0.58776569 0.1347765 -0.3839511 0.070220605 -0.24284482
		 -0.014955071 0.01298232 -0.11783729 -0.12805744 -0.023856035 0.18071608 -0.15014176
		 0.18368912 -0.18777007;
createNode polyPlanarProj -n "anotherRock:polyPlanarProj8";
	rename -uid "2893DC1C-45C7-233D-69D5-FBA066925AEB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[6:7]" "f[62]" "f[73]";
	setAttr ".ix" -type "matrix" 1.2000347354578098 0 0 0 0 0.91127597028534646 0 0 0 0 1 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0.15655842423439026 -0.44798402488231659 -0.96304941177368164 ;
	setAttr ".ro" -type "double3" -36.089003761745694 0 0 ;
	setAttr ".ps" -type "double2" 1.6562700867652893 1.6161531163004192 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "anotherRock:polyTweakUV9";
	rename -uid "C5F2D80D-4F5F-3589-AFEB-E082B1F765F9";
	setAttr ".uopa" yes;
	setAttr -s 137 ".uvtk[0:136]" -type "float2" 0.19150318 0.045499947 0.18774889
		 0.045499947 0.18979342 0.039772961 0.19212891 0.039849225 0.32364124 -0.65327448
		 0.32343376 -0.65667641 0.32824481 -0.65769798 0.32834384 -0.65353274 0.19359888 0.066549316
		 0.18768099 0.064921625 0.18617955 0.061069481 0.19206847 0.061574947 0.19102748 0.055477168
		 0.18562058 0.055729847 0.1854855 0.052187141 0.19087084 0.052029513 0.20767549 0.054472499
		 0.19942266 0.055216536 0.19917932 0.051627658 0.20751809 0.050840892 0.20450425 0.062605731
		 0.19934148 0.066089779 0.19951805 0.061085731 0.20675622 0.059388395 0.30747938 -0.65369153
		 0.30770051 -0.65754676 0.31505808 -0.65673786 0.31487226 -0.65305448 0.20606509 0.045802303
		 0.19725479 0.044789162 0.19556756 0.038777083 0.20512697 0.042419124 0.31526816 -0.64061648
		 0.31478381 -0.64657092 0.3232002 -0.64734721 0.32166699 -0.64223874 0.31576085 -0.66376013
		 0.32255048 -0.6643945 0.30817789 -0.66138577 0.32684743 -0.66491014 0.32882488 -0.64832628
		 0.30764928 -0.64919555 -0.2759918 0.0087797157 -0.28464302 0.010496031 -0.28231665
		 0.0026059144 -0.27741396 0.00033175579 -0.27992582 0.037683245 -0.28441763 0.035713147
		 -0.28602684 0.030503005 -0.27821764 0.032286696 -0.27705768 0.025935385 -0.28722253
		 0.024306132 -0.28676686 0.019502476 -0.27661395 0.017524693 -0.26942924 0.02585597
		 -0.26934892 0.01714943 -0.27452314 0.039662287 -0.27127388 0.033275187 -0.68970138
		 -0.47769922 -0.68099374 -0.47713077 -0.681741 -0.47158265 -0.6901083 -0.47300848
		 -0.68724102 -0.48734456 -0.6814152 -0.48915893 -0.68080509 -0.48358974 -0.6886304
		 -0.48305881 -0.27175751 0.0075782365 -0.27403355 -0.0017993329 -0.67373568 -0.4763073
		 -0.67472643 -0.47046712 -0.67673063 -0.48959669 -0.67439723 -0.48327053 -0.67753494
		 -0.46321797 -0.68370748 -0.46599883 -0.68995976 -0.46827322 0.40290114 0.096626557
		 0.42030841 0.094364211 0.41942218 0.10263257 0.40411761 0.10732675 0.42980865 0.081651613
		 0.41742983 0.082395054 0.4090783 0.06422063 0.41649401 0.063575707 0.43263656 0.094663031
		 0.39814252 0.059925131 0.40178505 0.079436056 0.42980441 0.10441456 0.4373391 0.10626706
		 0.43988127 0.096464105 0.44010931 0.082591593 0.40908083 0.052210078 0.42910892 0.059343304
		 0.41928437 0.043111932 -0.027655656 -0.54114866 -0.019016365 -0.52578771 -0.055056259
		 -0.52502358 -0.06152704 -0.55247575 -0.016064625 -0.56206411 -0.046863168 -0.58429122
		 -0.0036747626 -0.57577342 -0.034356631 -0.60653925 -0.72142941 0.28467616 -0.70225036
		 0.22374293 -0.67070979 0.2508015 -0.67154109 0.27804282 -0.67171609 0.20241213 -0.64656478
		 0.23215608 -0.72601402 0.3214125 -0.67502975 0.31005439 0.44429237 -0.062341727 0.44667745
		 -0.14557058 0.51836085 -0.16412757 0.52139878 -0.061637543 0.43916503 -0.0018973154
		 0.5297401 -0.0028021056 0.082089767 0.3053284 0.093736894 0.32937425 0.063987784
		 0.35660148 0.056815125 0.32970822 0.15913522 0.21850088 0.18596597 0.22332332 0.16891009
		 0.24843736 0.143197 0.23618916 0.12445856 0.2609385 0.1499279 0.27484134 0.12687859
		 0.30391362 0.11162172 0.27683792 -0.47280994 0.1990913 -0.60811746 0.48811695 -0.47760341
		 0.24698181 -0.34711662 -0.082349636 -0.17080571 -0.33306599 -0.30704489 0.024870396
		 -0.18884122 -0.11991327 -0.034227975 -0.48687929 -0.052598119 -0.29426539 0.12568924
		 -0.67852348;
createNode polyPlanarProj -n "anotherRock:polyPlanarProj9";
	rename -uid "E402497D-45F1-1968-1B8C-18A2A9586A5E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[42]" "f[48]" "f[50]";
	setAttr ".ix" -type "matrix" 1.2000347354578098 0 0 0 0 0.91127597028534646 0 0 0 0 1 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 1.2101200819015503 -0.46590260416269302 0.038966000080108643 ;
	setAttr ".ro" -type "double3" 42.989620061230049 89.999999999999957 0 ;
	setAttr ".ps" -type "double2" 1.6072880029678345 1.6983920492924258 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "anotherRock:polyTweakUV10";
	rename -uid "21EE0C17-466E-8F74-B7C3-AE9E3B8D9105";
	setAttr ".uopa" yes;
	setAttr -s 145 ".uvtk[0:144]" -type "float2" -0.62093967 0.031787768 -0.62302935
		 0.031787768 -0.62189138 0.028600004 -0.62059134 0.028642455 0.0084857503 0.061535336
		 0.0083701415 0.059641734 0.011048145 0.059073087 0.011103205 0.061391581 -0.61977321
		 0.043504294 -0.62306726 0.042598289 -0.62390298 0.040454105 -0.62062502 0.04073545
		 -0.6212045 0.037341308 -0.62421405 0.037481941 -0.62428921 0.035510004 -0.62129164
		 0.035422266 -0.61193782 0.036782086 -0.61653155 0.037196223 -0.61666697 0.035198584
		 -0.6120255 0.034760658 -0.61370301 0.041309215 -0.61657673 0.043248516 -0.6164785
		 0.040463153 -0.61244953 0.039518379 -0.00051033642 0.061303183 -0.0003873294 0.059157278
		 0.0037080972 0.059607524 0.0036046337 0.061657768 -0.61283416 0.031956069 -0.61773825
		 0.031392131 -0.61867738 0.028045677 -0.61335641 0.030072901 0.0038250729 0.068581022
		 0.0035554704 0.065266661 0.0082401242 0.06483455 0.0073867799 0.067678034 0.0040993225
		 0.05569879 0.0078785401 0.055345658 -0.00012157343 0.057020385 0.010270343 0.055058669
		 0.011370952 0.064289585 -0.00041577747 0.063805722 0.59997928 0.033247203 0.59516376
		 0.034202542 0.59645873 0.029810719 0.59918761 0.028544893 0.59778947 0.049335543
		 0.59528923 0.048238937 0.59439349 0.045338847 0.59874034 0.046331707 0.59938598 0.042796433
		 0.59372795 0.041889541 0.59398156 0.039215725 0.59963292 0.038114857 0.60363215 0.042752218
		 0.60367686 0.037905969 0.6007967 0.050437111 0.60260534 0.046881925 0.33101952 0.060602974
		 0.33586642 0.060919367 0.33545047 0.064007573 0.33079302 0.063213922 0.33238903 0.05523416
		 0.33563182 0.054224253 0.33597142 0.057324171 0.33161569 0.057619706 0.60233617 0.032578431
		 0.60106927 0.02735867 0.33990642 0.061377734 0.3393549 0.064628512 0.33823931 0.053980567
		 0.33953813 0.057501864 0.33779165 0.068663538 0.33435586 0.067115664 0.33087572 0.065849677
		 -0.081607483 -0.099472225 -0.071918257 -0.10073151 -0.072411515 -0.096129157 -0.08093033
		 -0.093516268 -0.066630214 -0.10780761 -0.073520504 -0.10739379 -0.078169174 -0.11751007
		 -0.074041449 -0.11786905 -0.065056123 -0.10056517 -0.084256336 -0.11990104 -0.08222875
		 -0.10904083 -0.066632532 -0.095137253 -0.062438548 -0.094106115 -0.061023574 -0.099562652
		 -0.060896643 -0.1072844 -0.078167744 -0.1241954 -0.067019679 -0.1202249 -0.072488248
		 -0.12925963 0.27181605 0.00100811 0.27662486 0.009558348 0.25656426 0.0099836634
		 0.25296247 -0.0052968147 0.27826783 -0.01063391 0.26112473 -0.02300597 0.28516433
		 -0.01826478 0.26808611 -0.03538974 0.48786935 0.034575086 0.49854481 0.00065835664
		 0.516101 0.015719697 0.51563823 0.030882793 0.51554084 -0.01121491 0.5295406 0.0053412528
		 0.48531744 0.055023331 0.51369637 0.048701145 0.1613175 0.23826182 0.16264507 0.19193482
		 0.20254563 0.18160559 0.20423657 0.23865379 0.15846348 0.2719065 0.20887955 0.27140287
		 -0.38239798 0.21061143 -0.3759149 0.22399586 -0.39247391 0.23915112 -0.39646634 0.22418179
		 -0.33951277 0.16228129 -0.32457817 0.16496556 -0.33407184 0.17894466 -0.34838432
		 0.17212701 -0.35881454 0.185903 -0.34463775 0.19364165 -0.3574675 0.20982397 -0.36595979
		 0.19475299 0.23443907 -0.11863755 0.26562697 -0.12561151 0.27063888 -0.10001027 0.24450807
		 -0.086238354 0.25869352 -0.05413818 0.28564778 -0.070246436 0.29855767 -0.04968923
		 0.27605695 -0.03002068 0.32046041 -0.022321684 0.30020025 0.00054831192 -0.24776097
		 0.23654988 0.060621165 -0.019892419 0.24452081 0.37615865 0.037511438 0.5289402 -0.92295474
		 -0.18420394 -0.82335281 -0.5038051 -0.36425889 -0.32777706 -0.56260955 -0.017333109;
createNode polyPlanarProj -n "anotherRock:polyPlanarProj10";
	rename -uid "E25118DF-4931-D8AF-42E9-6DBA0BA8D0B8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "f[5]" "f[44]" "f[47]" "f[49]";
	setAttr ".ix" -type "matrix" 1.2000347354578098 0 0 0 0 0.91127597028534646 0 0 0 0 1 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -1.0992752611637115 -0.36216116696596146 -0.21437171101570129 ;
	setAttr ".ro" -type "double3" 8.1831361659698246e-015 67.128116688647523 56.813141489122735 ;
	setAttr ".ps" -type "double2" 1.9252545237541199 2.050531845965105 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "anotherRock:polyTweakUV11";
	rename -uid "20DA4389-45D3-9975-6947-97BE7AB94D27";
	setAttr ".uopa" yes;
	setAttr -s 155 ".uvtk[0:154]" -type "float2" 0.15129237 0.11609385 0.19682933
		 0.11609385 0.17203087 0.18555962 0.14370155 0.18463463 -0.18100056 0.40995556 -0.17863309
		 0.44874701 -0.23349342 0.4603962 -0.234621 0.41290039 0.125873 -0.13922614 0.19765402
		 -0.11948285 0.215866 -0.072758064 0.14443459 -0.078889109 0.15706319 -0.0049256822
		 0.22264463 -0.0079905149 0.22428313 0.034981027 0.15896234 0.036892984 -0.044871878
		 0.0072606253 0.055233303 -0.0017642896 0.058183108 0.041767243 -0.04296102 0.051310424
		 -0.0064061256 -0.09139213 0.056216791 -0.133652 0.054075886 -0.072955094 -0.033719946
		 -0.052367121 0.0032900146 0.41471136 0.00076886237 0.45867151 -0.083128378 0.44944781
		 -0.081008695 0.40744743 -0.025338547 0.1124263 0.081527539 0.12471529 0.10199367
		 0.1976393 -0.013957619 0.15346274 -0.085523248 0.26562068 -0.080000542 0.33351722
		 -0.17596956 0.34236929 -0.1584885 0.2841188 -0.091141537 0.52952051 -0.16856083 0.53675461
		 -0.0046754694 0.50244689 -0.21755952 0.54263371 -0.24010605 0.35353315 0.0013515648
		 0.36344537 -0.34331262 0.49901277 -0.26472467 0.47036779 -0.27507305 0.54769146 -0.31771982
		 0.57612246 -0.34840423 0.22241659 -0.30344677 0.23435953 -0.2808013 0.280853 -0.35657847
		 0.27547312 -0.35822916 0.33668172 -0.26059914 0.3371942 -0.25789559 0.38287586 -0.35017765
		 0.41615126 -0.42960811 0.34850091 -0.41772115 0.43021545 -0.40191168 0.21171236 -0.42309105
		 0.27628967 -0.41643009 0.44320187 -0.49090198 0.43834049 -0.48451108 0.39089066 -0.41295007
		 0.40308493 -0.4374724 0.52569288 -0.48729721 0.54120982 -0.49251539 0.49358016 -0.42558986
		 0.4890393 -0.38125217 0.51641989 -0.34630758 0.60100263 -0.55297583 0.43129775 -0.54450196
		 0.38134995 -0.52736121 0.54495406 -0.5473175 0.49085 -0.52048296 0.31935254 -0.4676927
		 0.3431353 -0.41422048 0.36258706 -0.13611716 0.61074084 -0.12331164 0.70902359 -0.16999918
		 0.7040351 -0.19653195 0.61762941 -0.051514864 0.76264203 -0.055735171 0.69274879
		 0.046868622 0.64556074 0.050523579 0.68743068 -0.12497646 0.77863324 0.071102589
		 0.58380508 -0.039055943 0.60440761 -0.18004221 0.76266009 -0.19048834 0.80520678
		 -0.13513279 0.81954235 -0.056803524 0.8208046 0.11468428 0.64555252 0.074444056 0.75865078
		 0.16607386 0.70314848 -0.32778078 0.58640659 -0.32933235 0.58364803 -0.32286015 0.58351082
		 -0.32169822 0.58844078 -0.32986242 0.59016269 -0.32433158 0.59415424 -0.33208752
		 0.5926246 -0.32657748 0.59814966 0.23756655 0.013007706 0.25286758 -0.03560501 0.27803072
		 -0.014017609 0.27736747 0.0077155097 0.27722785 -0.052622706 0.29729357 -0.028892869
		 0.23390897 0.042315986 0.27458423 0.033254445 -0.20310935 0.33530635 -0.19950628
		 0.20957364 -0.091215201 0.18153986 -0.086625919 0.33637008 -0.21085532 0.42661875
		 -0.074024796 0.42525193 -0.025449237 -0.26806024 -0.022805899 -0.26260316 -0.029557247
		 -0.25642422 -0.031185051 -0.26252741 -0.0079643354 -0.28776503 -0.0018752672 -0.28667054
		 -0.0057460382 -0.2809712 -0.01158137 -0.2837508 -0.015833898 -0.27813411 -0.010053854
		 -0.274979 -0.015284725 -0.26838121 -0.018747076 -0.27452585 -0.17460014 0.21721612
		 -0.086453341 0.19750547 -0.072288118 0.26986262 -0.14614202 0.30878642 -0.10604948
		 0.39951172 -0.029868275 0.35398471 0.006619018 0.41208583 -0.056974918 0.4676753
		 0.068523146 0.48943511 0.011261548 0.55407286 0.50102288 0.01547176 0.33875814 -0.014166716
		 0.38596055 -0.20208301 0.51269531 -0.15822819 0.54144937 0.33769605 0.42947537 0.40729001
		 0.34726214 0.19187289 0.49608648 0.17526226 -0.4075332 0.021840002 -0.2915501 0.093193032
		 -0.19790858 -0.088701814 -0.12622759 -0.28393555 0.24660015 -0.38956657 0.28092045
		 -0.62340933 0.35740572 -0.40118629 0.45343959 -0.64512253 0.43689176 -0.45303509
		 0.56907266 -0.68921375;
createNode polyMapSewMove -n "anotherRock:polyMapSewMove1";
	rename -uid "25A5205D-4101-0958-94B6-18BF6BFC699D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[6]" "e[33]" "e[131]" "e[133]";
createNode polyTweakUV -n "anotherRock:polyTweakUV12";
	rename -uid "B0F8ACF2-4A9E-52A5-F36D-3DB2C06C58AC";
	setAttr ".uopa" yes;
	setAttr -s 149 ".uvtk[0:148]" -type "float2" 0.31609279 0.0079372032 0.31748328
		 0.0079372032 0.31672603 0.010058314 0.31586105 0.010030042 -0.48835453 -0.36822644
		 -0.48832923 -0.36978215 -0.48626643 -0.37005255 -0.48629469 -0.36815211 0.31531668
		 0.00014117385 0.31750846 0.00074402225 0.31806457 0.002170732 0.31588346 0.0019835262
		 0.31626904 0.0042419503 0.31827152 0.0041483692 0.31832156 0.0054604872 0.31632698
		 0.0055188606 0.31010306 0.0046140659 0.31315973 0.0043384857 0.3132498 0.0056677093
		 0.31016141 0.0059590847 0.3112776 0.0016017539 0.31318974 0.00031137597 0.31312442
		 0.0021647133 0.31044358 0.0027933514 -0.49569604 -0.36898521 -0.49545968 -0.37073216
		 -0.49213928 -0.370105 -0.49235356 -0.36843503 0.31069955 0.0078252312 0.31396258
		 0.0082004676 0.31458753 0.010427147 0.31104705 0.0090782512 -0.49261123 -0.36275992
		 -0.49262205 -0.36548713 -0.48876402 -0.36554417 -0.48964167 -0.36327299 -0.4915722
		 -0.37327641 -0.48845956 -0.37332612 -0.4951072 -0.37246269 -0.48617324 -0.37357089
		 -0.48589873 -0.3657738 -0.49577695 -0.3669329 -0.48271057 -0.37472585 -0.48547354
		 -0.37684825 -0.48366025 -0.37796649 -0.48290578 -0.36322907 -0.48475567 -0.36379129
		 -0.48248821 -0.36542091 -0.48232967 -0.36796099 -0.48254725 -0.37127388 -0.47934732
		 -0.36834702 -0.47972092 -0.37175876 -0.48069891 -0.36270577 -0.4797242 -0.36535704
		 0.083671905 0.10988127 0.081216946 0.10972101 0.081427626 0.10815683 0.083786614
		 0.10855885 0.082978241 0.11260055 0.081335798 0.11311203 0.081163779 0.11154196 0.083369941
		 0.11139228 -0.48110902 -0.37539318 -0.48243621 -0.3789579 0.079170711 0.10948887
		 0.079450049 0.10784233 0.080015093 0.11323546 0.079357237 0.11145201 0.080241829
		 0.10579862 0.081982046 0.10658261 0.083744735 0.10722382 -0.48806405 -0.37440532
		 -0.48688912 -0.37776116 -0.48832482 -0.38063729 -0.48931697 -0.37832966 -0.49336588
		 -0.3784726 -0.49280757 -0.37987313 -0.48571298 -0.3799648 -0.49513847 -0.37688461
		 -0.49127632 -0.37576798 -0.48420614 -0.37856477 -0.48318514 -0.37975958 -0.48472768
		 -0.3811118 -0.48721722 -0.38241571 -0.49553895 -0.37956616 -0.49242535 -0.38254106
		 -0.49625668 -0.38224059 0.021415079 -0.074255928 0.020769622 -0.075403623 0.023462243
		 -0.075460717 0.023945656 -0.073409721 0.020549085 -0.072693288 0.022850081 -0.071032651
		 0.019623406 -0.071669027 0.021915715 -0.069370478 -0.85826051 -0.82520932 -0.70378631
		 -0.87595809 -0.71294618 -0.77057934 -0.77051759 -0.7318778 -0.61436498 -0.8444609
		 -0.6385591 -0.74831104 -0.94103777 -0.78018117 -0.84189737 -0.69159389 -0.21285343
		 0.076725185 -0.21293192 0.079464369 -0.21529117 0.08007507 -0.21539117 0.076701947
		 -0.21268474 0.07473579 -0.2156657 0.074765541 0.26194939 0.063589253 0.26119629 0.062034506
		 0.26311982 0.060274068 0.26358354 0.062012937 0.25696781 0.069203325 0.25523299 0.06889154
		 0.25633582 0.067267664 0.25799835 0.068059608 0.2592099 0.06645938 0.25756317 0.065560475
		 0.25905347 0.063680746 0.26003993 0.065431364 0.34197357 -0.010843631 0.3402161 -0.010450609
		 0.33993369 -0.011893261 0.34140623 -0.012669321 0.34060678 -0.014478262 0.33908787
		 -0.013570519 0.33836037 -0.014728962 0.33962831 -0.015837332 0.33712617 -0.016271168
		 0.33826789 -0.017559944 -0.54099721 0.039659653 -0.53875136 0.040069859 -0.53940469
		 0.04267076 -0.5411588 0.04206381 -0.54155672 0.035199799 -0.54000694 0.03423658 -0.53886902
		 0.037218116 -0.5409289 0.037448004 0.28403825 0.27186903 0.28249785 0.27092138 0.28342631
		 0.26992628 0.28473079 0.26921085 0.28313485 0.26601651 0.28499472 0.26557854 0.28241864
		 0.26485857 0.28397259 0.26390812 0.28227684 0.26398909 0.28344405 0.26258552;
createNode polyMapSewMove -n "anotherRock:polyMapSewMove2";
	rename -uid "1DBADC55-4D2A-3BF1-822A-30981E8EA59F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[10]" "e[30]" "e[95]" "e[134]";
createNode polyMapSewMove -n "anotherRock:polyMapSewMove3";
	rename -uid "4CAE3E1E-4041-AD4D-18F8-6EBA03CB821F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[7]" "e[67]" "e[79]";
createNode polyTweakUV -n "anotherRock:polyTweakUV13";
	rename -uid "C595A63F-4302-8C6B-EC8B-0E932C96005A";
	setAttr ".uopa" yes;
	setAttr -s 141 ".uvtk[0:140]" -type "float2" 0.027967492 -0.0090391422
		 0.02635528 -0.0090391422 0.027233297 -0.011498491 0.028236208 -0.011465732 -0.019300517
		 0.10648843 -0.017833196 0.10502283 -0.015433356 0.10305045 -0.015718766 0.11223502
		 0.028867368 -4.0090731e-010 0.026326133 -0.00069897762 0.02568136 -0.0023531886 0.028210271
		 -0.0021361238 0.027763164 -0.0047546751 0.025441347 -0.004646162 0.02538335 -0.0061675003
		 0.0276959 -0.0062351897 0.034912247 -0.0051861107 0.031368215 -0.0048665944 0.031263798
		 -0.0064077587 0.034844633 -0.0067456025 0.033550426 -0.0016934827 0.031333428 -0.00019734268
		 0.031409193 -0.0023462127 0.034517456 -0.0030750898 -0.0096727619 0.11165796 -0.01318141
		 0.115741 -0.017344471 0.10859422 -0.014893509 0.10671829 0.034220718 -0.0089093028
		 0.03043736 -0.0093443729 0.029712809 -0.011926156 0.033817831 -0.010362181 -0.017927127
		 0.094441094 -0.015328795 0.1011869 -0.016982401 0.11554044 -0.024745561 0.098347001
		 -0.020410214 0.071362346 -0.019104455 0.085090928 -0.018636562 0.052145861 -0.013137402
		 0.089485504 -0.01333179 0.11670043 -0.0078602647 0.10631847 0.0065208818 0.0957582
		 0.012264808 0.079803705 0.019185942 0.086738043 0.0015493591 0.11759069 -0.002227373
		 0.11965735 -0.00039921963 0.11488223 -0.0036744303 0.11039662 -0.0027333617 0.10326227
		 -2.8709821e-005 0.10951621 0.0025094333 0.10692958 0.0043688924 0.11514221 0.0019043904
		 0.11021279 0.51271731 0.032264885 0.51556325 0.032450553 0.51531899 0.034263868 0.51258433
		 0.033797868 0.51352143 0.029112481 0.51542556 0.028519517 0.51562488 0.030339679
		 0.51306742 0.03051319 0.010895647 0.10017727 0.024675185 0.090776362 0.51793534 0.032719713
		 0.5176115 0.034628469 0.51695651 0.028376417 0.51771909 0.030444024 0.51669365 0.036997676
		 0.51467627 0.036088791 0.51263291 0.035345446 0.017396048 0.066010572 0.040837247
		 0.045288295 0.015697593 0.042392742 0.0043476764 0.00058541447 0.020646971 0.0021750608
		 0.040514085 0.071787596 0.030314971 0.088555135 0.043595266 0.096056134 0.053549875
		 0.078864567 0.061163586 0.051831875 0.0098868972 -0.026093548 0.049247455 -0.0010937911
		 0.036162779 -0.040563919 0.010230085 0.14577122 0.010978735 0.14710243 0.0078554498
		 0.14716862 0.0072947028 0.1447895 0.011234549 0.14395854 0.0085654939 0.14203237
		 0.012308283 0.14277048 0.009649355 0.14010438 0.001109166 0.11913142 -0.0030065707
		 0.12341207 0.0015081541 0.11850417 0.038040981 -0.0094090262 0.038132008 -0.012586613
		 0.040868863 -0.013295119 0.040984809 -0.009382125 0.037845243 -0.0071013016 0.041303299
		 -0.0071359128 -0.021674354 -0.0022672438 -0.02080084 -0.00046381276 -0.023031991
		 0.0015782779 -0.023569997 -0.00043875561 -0.015895898 -0.008779455 -0.013883522 -0.0084177302
		 -0.015162735 -0.0065341312 -0.017091284 -0.0074527874 -0.018496666 -0.0055965278
		 -0.01658643 -0.0045537967 -0.018315138 -0.0023733552 -0.019459438 -0.004404081 -0.61155587
		 0.075616278 -0.60951704 0.075160459 -0.60918939 0.076833971 -0.61089766 0.077734306
		 -0.60997033 0.07983283 -0.6082083 0.078779697 -0.60736442 0.080123648 -0.60883522
		 0.081409387 -0.60593253 0.081912644 -0.60725701 0.08340773 -0.00067782751 0.050621271
		 -0.0032831493 0.050145388 -0.0025252695 0.047128174 -0.00049041311 0.04783231 -2.8739601e-005
		 0.055794898 -0.0018265909 0.056912318 -0.0031466126 0.053453576 -0.00075708184 0.053186849
		 -0.013273794 0.078450181 -0.011486757 0.079549558 -0.012563863 0.080703944 -0.014077181
		 0.081533894 -0.012225858 0.085239559 -0.014383273 0.085747622 -0.011394979 0.086582839
		 -0.013197551 0.087685443 -0.011230415 0.087591492 -0.012584357 0.089219734;
createNode polyMapSewMove -n "anotherRock:polyMapSewMove4";
	rename -uid "0C58BAF7-48E9-098D-13E3-3183CA485E8F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[13]" "e[85]";
createNode polyTweakUV -n "anotherRock:polyTweakUV14";
	rename -uid "183D5FA6-4F3E-023B-DD67-A48B19597A2D";
	setAttr ".uopa" yes;
	setAttr -s 139 ".uvtk[0:138]" -type "float2" 0.0035095157 0.0028886865
		 0.0040246877 0.0028886865 0.0037441456 0.0036746084 0.0034235858 0.0036641501 0.0039959126
		 0.0016897969 0.0044119721 0.002076338 0.0044544809 0.0031946208 0.0031616404 0.0029428198
		 0.0032218792 2.4331134e-010 0.0040339916 0.00022337314 0.0042401226 0.00075201364
		 0.0034319183 0.00068265031 0.0035748198 0.0015194629 0.0043167518 0.0014848016 0.0043353364
		 0.0019709838 0.0035962542 0.0019926128 0.0012901698 0.0016573487 0.0024227265 0.0015552422
		 0.0024560899 0.002047752 0.0013117896 0.0021557331 0.0017253414 0.00054119003 0.0024338709
		 6.3065731e-005 0.0024096211 0.00074978557 0.0014163046 0.00098270818 0.0075033 0.003583522
		 0.0065416065 0.0047790767 0.0050896774 0.0025236078 0.005604344 0.0017737144 0.0015111332
		 0.0028471889 0.0027201506 0.002986229 0.0029517703 0.0038112844 0.0016398974 0.0033114797
		 0.0064582359 -0.002071054 0.0062251692 5.5072422e-005 0.0031721145 7.0270573e-005
		 0.0040726485 -0.0016739593 0.0034401978 0.0026646662 0.0051203636 0.00098084006 0.0051476341
		 0.0059017269 0.0070183687 0.0018578883 0.0012803171 0.0023223027 0.0082793413 0.002109227
		 0.012135902 0.0066076652 0.010204089 -0.0063707372 0.010851521 -0.009599586 -0.0032777654
		 0.002559426 -0.00094377523 0.0013862799 -0.0013867668 0.0049332492 0.001359067 0.006808586
		 0.0057253786 0.0075644027 1.1244965e-005 0.01119331 0.0056821364 0.012729091 -0.0062163277
		 0.0042534806 -0.003894154 0.0082218433 -0.27107555 -0.4345468 -0.095474005 -0.34971943
		 -0.15749842 -0.24106553 -0.31885672 -0.34079751 -0.13919204 -0.61369789 -0.0032957196
		 -0.60240436 -0.037379205 -0.48192707 -0.20392689 -0.53657931 0.014345554 0.01127176
		 0.0088495333 -0.0082988497 0.047959864 -0.27177101 -0.021559954 -0.15911032 0.09741199
		 -0.57217222 0.092673063 -0.42155683 -0.14054984 -0.032514453 -0.24507779 -0.14190042
		 -0.35550609 -0.24146575 0.0028891754 -0.0043340628 -0.0061841076 -0.0001930588 -1.5898566e-005
		 0.0005285691 0.0020491991 0.0063802996 -0.00028419468 0.0071265264 -0.0072452785
		 -0.0093875118 -0.014048253 -0.011143613 -0.012198248 -0.0002787947 0.0035238867 0.01012527
		 -0.0049458076 0.0099657504 0.0007844817 0.014907739 -0.0012231932 -0.0018110347 -0.0014624585
		 -0.002236428 -0.00046429393 -0.0022576207 -0.00028509015 -0.0014973483 -0.0015442057
		 -0.0012317548 -0.00069123687 -0.00061620527 -0.0018873418 -0.00085209217 -0.0010375803
		 -1.1303634e-008 -0.0063285758 -0.00150871 0.00036117533 -0.0023208656 -0.010242797
		 0.00012156248 0.0019839799 0.0059703472 0.0019548966 0.0069859289 0.0010801987 0.0072123618
		 0.0010431324 0.0059617744 0.0020465748 0.0052328426 0.00094135164 0.0052438318 0.0033284645
		 0.0049204268 0.0030493459 0.0043440652 0.003762339 0.0036914747 0.0039342702 0.0043360945
		 0.0014818433 0.007001569 0.0008387721 0.0068859328 0.0012475775 0.0062840455 0.0018638808
		 0.0065775486 0.0023129934 0.0059843822 0.0017025408 0.0056511522 0.002254989 0.0049543283
		 0.0026207066 0.0056033093 0.0024867402 -0.010131875 0.0018351437 -0.0099861491 0.0017304287
		 -0.010521067 0.0022763852 -0.010808825 0.0019799927 -0.011479512 0.0014168459 -0.011142963
		 0.0011470964 -0.011572436 0.0016172171 -0.011983367 0.00068948331 -0.012144214 0.0011127742
		 -0.012622057 0.00021868398 -0.0099014286 0.0010513068 -0.0097493343 0.00080910709
		 -0.0087851277 0.00015878893 -0.009010125 1.1244965e-005 -0.011554853 0.00058581313
		 -0.01191197 0.0010076716 -0.010806586 0.00024401413 -0.010721373 0.0052791568 0.0048607262
		 0.0047081308 0.0045094546 0.0050523328 0.0041405568 0.0055358545 0.0038753492 0.004944283
		 0.0026911411 0.0056337412 0.0025288258 0.0046787774 0.0022619143 0.0052548046 0.0019095665
		 0.0046261786 0.0019395835 0.0050588604 0.0014192372;
createNode polyMapSewMove -n "anotherRock:polyMapSewMove5";
	rename -uid "11CB2652-4696-CDF9-BC53-75B9814F34BA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[101]" "e[119]";
createNode polyMapSewMove -n "anotherRock:polyMapSewMove6";
	rename -uid "DAD47066-4B26-DDBF-E262-0A9644B98CA9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[52]" "e[125]" "e[151]";
createNode polyTweakUV -n "anotherRock:polyTweakUV15";
	rename -uid "FCCF4EC7-4610-3097-9D6D-EFA0A712A7E3";
	setAttr ".uopa" yes;
	setAttr -s 133 ".uvtk[0:132]" -type "float2" 0.10323554 0.047919925 0.10643783
		 0.047919925 0.10469395 0.052804988 0.10270174 0.052739941 0.027822919 0.16681181
		 0.074493214 0.11750115 0.1441467 0.1718376 0.093969651 0.22301155 0.10144798 0.02996503
		 0.10649583 0.03135344 0.10777658 0.034639273 0.10275328 0.034208111 0.10364142 0.039409455
		 0.10825324 0.039193925 0.10836846 0.042215813 0.10377491 0.042350259 0.089440718
		 0.040266421 0.096480407 0.03963178 0.096687883 0.042693038 0.089575134 0.043364152
		 0.092145741 0.033328872 0.096549615 0.030357022 0.096399024 0.034625411 0.090224959
		 0.036073226 -0.19239588 -0.11227632 -0.11544992 -0.14783198 -0.029847156 -0.016905755
		 -0.091895513 0.024477514 0.090814359 0.047662001 0.09832944 0.048526209 0.099768743
		 0.053654466 0.091614716 0.050547823 -0.29695702 0.20162405 -0.20025267 0.1088272
		 -0.067587122 0.24450098 -0.17823748 0.28988281 0.092340097 -0.090101227 0.18264538
		 0.01249794 -0.040133338 -0.18560517 0.23734809 0.073642306 0.03974171 0.30467752
		 -0.28647247 -0.073439009 0.3703303 0.10891609 0.31448257 -0.020818016 0.40061271
		 -0.036626656 0.035848834 0.46287498 -0.00089666224 0.38640648 0.1194504 0.41293553
		 0.20364213 0.34191963 0.29052955 0.23167305 0.31110385 0.41937509 0.39943403 0.29208249
		 0.080340214 0.55470854 0.20256755 0.50534058 -0.2658442 -0.29789436 -0.15016955 -0.31925419
		 -0.43098131 -0.095246233 -0.51063591 -0.20388551 -0.39930007 -0.25593564 0.44578412
		 0.12240444 0.47202063 -0.051427033 -0.29637754 -0.4514991 -0.16935723 -0.46729994
		 -0.55241656 -0.29861799 -0.43666974 -0.39104989 -0.0066720252 -0.44842419 -0.027731806
		 -0.31237987 0.28730109 -0.08046823 0.29772377 -0.21859445 0.21081823 -0.15010774
		 0.053033203 -0.22842711 0.10038106 -0.27732623 0.38282725 -0.130179 0.45135677 -0.14872451
		 0.38289803 -0.2655693 0.16919741 -0.38391587 -0.19034551 -0.61314827 -0.10304904
		 -0.56389427 -0.27609867 -0.45395559 -0.38848394 -0.56841421 -0.1956104 -0.74904621
		 -0.41092843 -0.7663874 -0.17575046 -0.8522467 -0.41566828 -0.91146535 -0.093275718
		 0.55793214 -0.13023971 0.35248893 -0.046284992 0.67008132 0.042949788 -0.034992885
		 0.042768918 -0.028681057 0.037332669 -0.027273821 0.037102286 -0.035046287 0.043338604
		 -0.039576802 0.036469679 -0.03950816 -0.16032265 0.132855 -0.16205779 0.12927261
		 -0.15762581 0.12521631 -0.1565572 0.12922287 -0.17180091 0.14579056 -0.17579813 0.14507213
		 -0.17325717 0.14133066 -0.16942641 0.14315538 -0.16663475 0.13946825 -0.1704292 0.13739698
		 -0.16699532 0.13306576 -0.16472231 0.13709946 -0.10969682 0.081843548 -0.11374656
		 0.082749121 -0.11439734 0.079424746 -0.11100426 0.07763648 -0.11284625 0.07346832
		 -0.11634626 0.075560004 -0.1180226 0.072890624 -0.1151009 0.070336692 -0.12086666
		 0.069336981 -0.11823589 0.066367351 0.87691158 -0.02181261 0.88208663 -0.0208674
		 0.8805812 -0.014874176 0.87653929 -0.016272817 0.87562215 -0.032089286 0.87919337
		 -0.03430884 0.88181537 -0.027438564 0.87706894 -0.026908815 -0.16621274 0.094920583
		 -0.16976227 0.092736967 -0.16762283 0.090444058 -0.16461696 0.088795498 -0.16829439
		 0.081434876 -0.16400902 0.080425724 -0.16994464 0.078766666 -0.16636418 0.076576583
		 -0.17027159 0.07676322 -0.16758217 0.073528975;
createNode polyMapSewMove -n "anotherRock:polyMapSewMove7";
	rename -uid "1279A2DC-4DF5-0EE1-12BB-40BC578D250D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[154]";
createNode polyMapSewMove -n "anotherRock:polyMapSewMove8";
	rename -uid "84E5EB91-4AAA-5DEA-6C55-769772DE19E6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[56]" "e[62]" "e[88]";
createNode polyTweakUV -n "anotherRock:polyTweakUV16";
	rename -uid "40BB4C03-449F-439B-5E3E-A8B0E45120E6";
	setAttr ".uopa" yes;
	setAttr -s 117 ".uvtk";
	setAttr ".uvtk[5]" -type "float2" 0.00012415648 -0.00023832917 ;
	setAttr ".uvtk[6]" -type "float2" 0.00033396482 -0.00013840199 ;
	setAttr ".uvtk[7]" -type "float2" 2.0295382e-005 -3.4451485e-005 ;
	setAttr ".uvtk[24]" -type "float2" -0.00017651916 0.0044341385 ;
	setAttr ".uvtk[25]" -type "float2" -0.0020130575 0.0017485917 ;
	setAttr ".uvtk[26]" -type "float2" -0.00041511655 -0.00014418364 ;
	setAttr ".uvtk[32]" -type "float2" 1.3023615e-005 -0.0033554435 ;
	setAttr ".uvtk[33]" -type "float2" 0.0010807663 -0.0025011301 ;
	setAttr ".uvtk[34]" -type "float2" -0.00043700635 -0.000780195 ;
	setAttr ".uvtk[35]" -type "float2" -0.00091139972 -0.0020774007 ;
	setAttr ".uvtk[36]" -type "float2" -0.00050339103 -0.0015057325 ;
	setAttr ".uvtk[37]" -type "float2" 0.00062894821 -0.0011457503 ;
	setAttr ".uvtk[38]" -type "float2" -0.0026667118 -0.00040328503 ;
	setAttr ".uvtk[39]" -type "float2" 0.0010999739 -0.00059750676 ;
	setAttr ".uvtk[40]" -type "float2" -0.00031611323 -0.00018000603 ;
	setAttr ".uvtk[41]" -type "float2" 0.0055086911 0.00864923 ;
	setAttr ".uvtk[42]" -type "float2" 0.0021128356 0.00012934208 ;
	setAttr ".uvtk[43]" -type "float2" 0.0020543337 -0.0015572309 ;
	setAttr ".uvtk[44]" -type "float2" 0.0032418966 -0.001536727 ;
	setAttr ".uvtk[45]" -type "float2" -0.00069968402 -2.5212765e-005 ;
	setAttr ".uvtk[46]" -type "float2" -0.00056393445 -0.00018489361 ;
	setAttr ".uvtk[47]" -type "float2" -0.00054050982 0.00031268597 ;
	setAttr ".uvtk[48]" -type "float2" -5.6117773e-005 0.00062161684 ;
	setAttr ".uvtk[49]" -type "float2" 0.00085353851 0.00058537722 ;
	setAttr ".uvtk[50]" -type "float2" -0.00022247434 0.0014563799 ;
	setAttr ".uvtk[51]" -type "float2" 0.0010793507 0.001681447 ;
	setAttr ".uvtk[52]" -type "float2" -0.0010095164 -5.0425529e-005 ;
	setAttr ".uvtk[53]" -type "float2" -0.00098274648 0.00078219175 ;
	setAttr ".uvtk[54]" -type "float2" -0.010168642 0.0062449351 ;
	setAttr ".uvtk[55]" -type "float2" -0.0085261762 0.00073572993 ;
	setAttr ".uvtk[56]" -type "float2" 0.0096135736 0.012715802 ;
	setAttr ".uvtk[57]" -type "float2" 0.0089160949 0.012108184 ;
	setAttr ".uvtk[58]" -type "float2" -0.011065215 0.016635329 ;
	setAttr ".uvtk[59]" -type "float2" 0.0027643442 0.0007750392 ;
	setAttr ".uvtk[60]" -type "float2" 0.0043973923 -0.0013561845 ;
	setAttr ".uvtk[61]" -type "float2" -0.019228458 0.0021006688 ;
	setAttr ".uvtk[62]" -type "float2" -0.01433906 -0.0033909231 ;
	setAttr ".uvtk[63]" -type "float2" 0.0091522783 0.0093319491 ;
	setAttr ".uvtk[64]" -type "float2" -0.023270577 0.012859858 ;
	setAttr ".uvtk[65]" -type "float2" -0.0080626011 -0.0068836063 ;
	setAttr ".uvtk[66]" -type "float2" -0.005648613 -0.0026681721 ;
	setAttr ".uvtk[67]" -type "float2" 0.0017354488 -0.0023435056 ;
	setAttr ".uvtk[68]" -type "float2" 0.0016741157 -0.0046562254 ;
	setAttr ".uvtk[69]" -type "float2" 0.00054889917 -0.0030503273 ;
	setAttr ".uvtk[70]" -type "float2" -0.0024504662 -0.0026773512 ;
	setAttr ".uvtk[71]" -type "float2" -0.0023243427 -0.0041776747 ;
	setAttr ".uvtk[72]" -type "float2" 0.0032930374 -0.0031107366 ;
	setAttr ".uvtk[73]" -type "float2" 0.0046460032 -0.0032925606 ;
	setAttr ".uvtk[74]" -type "float2" 0.003284812 -0.0059143007 ;
	setAttr ".uvtk[75]" -type "float2" -0.0020050406 -0.0074938387 ;
	setAttr ".uvtk[76]" -type "float2" 0.0031705052 -0.016733527 ;
	setAttr ".uvtk[77]" -type "float2" 0.020695042 -0.029172644 ;
	setAttr ".uvtk[78]" -type "float2" 0.035176065 -0.03788041 ;
	setAttr ".uvtk[79]" -type "float2" -0.00063548982 -0.00014126301 ;
	setAttr ".uvtk[80]" -type "float2" -0.00067184865 -0.00042432547 ;
	setAttr ".uvtk[81]" -type "float2" -0.00058527547 -0.00045645237 ;
	setAttr ".uvtk[82]" -type "float2" -0.59600163 -0.6858604 ;
	setAttr ".uvtk[83]" -type "float2" -0.54317343 -0.68975198 ;
	setAttr ".uvtk[84]" -type "float2" -0.52677757 -0.64558953 ;
	setAttr ".uvtk[85]" -type "float2" -0.59144372 -0.63701606 ;
	setAttr ".uvtk[86]" -type "float2" -0.63458818 -0.68518305 ;
	setAttr ".uvtk[87]" -type "float2" -0.62813789 -0.62791908 ;
createNode polyMapSewMove -n "anotherRock:polyMapSewMove9";
	rename -uid "F430C921-42D1-83DD-E2BE-269AB15CE9D6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[63]" "e[82]";
createNode polyMapSewMove -n "anotherRock:polyMapSewMove10";
	rename -uid "359C15AD-412A-2682-85F8-CAAC7803B0F6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[77]";
createNode polyTweakUV -n "anotherRock:polyTweakUV17";
	rename -uid "BD89E67E-48BE-26E1-225E-0689264655E3";
	setAttr ".uopa" yes;
	setAttr -s 114 ".uvtk";
	setAttr ".uvtk[5]" -type "float2" -0.00063183904 -0.0023238361 ;
	setAttr ".uvtk[6]" -type "float2" 0.00090515614 -0.0032220483 ;
	setAttr ".uvtk[7]" -type "float2" 0.00055652857 -0.0018126369 ;
	setAttr ".uvtk[24]" -type "float2" -0.012981653 0.0083810389 ;
	setAttr ".uvtk[25]" -type "float2" -0.014596164 0.0020552576 ;
	setAttr ".uvtk[26]" -type "float2" -0.0039629042 -0.0017541051 ;
	setAttr ".uvtk[32]" -type "float2" 0.039054491 0.016009182 ;
	setAttr ".uvtk[33]" -type "float2" 0.01493293 0.0086840689 ;
	setAttr ".uvtk[34]" -type "float2" -0.00019045174 -0.0051343143 ;
	setAttr ".uvtk[35]" -type "float2" 0.053569004 0.019827276 ;
	setAttr ".uvtk[36]" -type "float2" -0.0068235993 -0.0073332191 ;
	setAttr ".uvtk[37]" -type "float2" -0.0016804338 -0.0075364411 ;
	setAttr ".uvtk[38]" -type "float2" -0.015261233 -0.0033148378 ;
	setAttr ".uvtk[39]" -type "float2" 0.0010192394 -0.007322371 ;
	setAttr ".uvtk[40]" -type "float2" -0.0002758801 -0.0018336773 ;
	setAttr ".uvtk[41]" -type "float2" -0.0093142986 0.017853364 ;
	setAttr ".uvtk[42]" -type "float2" 0.0054539144 -0.0087733269 ;
	setAttr ".uvtk[43]" -type "float2" 0.0010975003 -0.011982322 ;
	setAttr ".uvtk[44]" -type "float2" 0.0037310123 -0.014505267 ;
	setAttr ".uvtk[45]" -type "float2" -0.0023047775 0.0016182065 ;
	setAttr ".uvtk[46]" -type "float2" -0.0021131337 -0.00091201067 ;
	setAttr ".uvtk[47]" -type "float2" 0.00060504675 0.0010938644 ;
	setAttr ".uvtk[48]" -type "float2" 0.0030274987 -0.0004324317 ;
	setAttr ".uvtk[49]" -type "float2" 0.0047224164 -0.0038691163 ;
	setAttr ".uvtk[50]" -type "float2" 0.0063898265 0.0016593933 ;
	setAttr ".uvtk[51]" -type "float2" 0.0086368918 -0.0030599833 ;
	setAttr ".uvtk[52]" -type "float2" -0.002128616 0.0051413178 ;
	setAttr ".uvtk[53]" -type "float2" 0.0026130676 0.004414916 ;
	setAttr ".uvtk[54]" -type "float2" -0.030167311 0.011846356 ;
	setAttr ".uvtk[55]" -type "float2" -0.028421938 0.00074602664 ;
	setAttr ".uvtk[56]" -type "float2" -0.0047691017 0.028377727 ;
	setAttr ".uvtk[57]" -type "float2" -0.020938396 0.046658941 ;
	setAttr ".uvtk[58]" -type "float2" -0.028286025 0.028360069 ;
	setAttr ".uvtk[59]" -type "float2" 0.0082113743 -0.0094848275 ;
	setAttr ".uvtk[60]" -type "float2" 0.0061942339 -0.016503394 ;
	setAttr ".uvtk[61]" -type "float2" -0.04807511 0.0085814372 ;
	setAttr ".uvtk[62]" -type "float2" -0.04203257 -0.0044966973 ;
	setAttr ".uvtk[63]" -type "float2" -0.040577367 0.054117225 ;
	setAttr ".uvtk[64]" -type "float2" -0.049019665 0.029033024 ;
	setAttr ".uvtk[65]" -type "float2" -0.031263769 -0.015735313 ;
	setAttr ".uvtk[66]" -type "float2" -0.023263365 -0.0078413337 ;
	setAttr ".uvtk[67]" -type "float2" -0.0013560057 -0.013335437 ;
	setAttr ".uvtk[68]" -type "float2" -0.0052890778 -0.019120604 ;
	setAttr ".uvtk[69]" -type "float2" -0.0060966611 -0.0133771 ;
	setAttr ".uvtk[70]" -type "float2" -0.015006006 -0.0094854534 ;
	setAttr ".uvtk[71]" -type "float2" -0.015717864 -0.01359795 ;
	setAttr ".uvtk[72]" -type "float2" 0.00082135201 -0.017859101 ;
	setAttr ".uvtk[73]" -type "float2" 0.0033420324 -0.020349979 ;
	setAttr ".uvtk[74]" -type "float2" -0.0030674934 -0.024144232 ;
	setAttr ".uvtk[75]" -type "float2" -0.017857373 -0.021900922 ;
	setAttr ".uvtk[76]" -type "float2" 0.0072597302 0.085073963 ;
	setAttr ".uvtk[77]" -type "float2" -0.020619433 0.10931247 ;
	setAttr ".uvtk[78]" -type "float2" -0.0079020485 0.002048552 ;
	setAttr ".uvtk[79]" -type "float2" -0.0045030788 -0.0043620467 ;
	setAttr ".uvtk[80]" -type "float2" -0.008679173 0.0067435503 ;
	setAttr ".uvtk[81]" -type "float2" 0.046566337 0.1223464 ;
	setAttr ".uvtk[82]" -type "float2" 0.021411769 0.15314178 ;
	setAttr ".uvtk[113]" -type "float2" -0.14155683 -0.36271551 ;
	setAttr ".uvtk[114]" -type "float2" -0.023611695 -0.37803879 ;
	setAttr ".uvtk[115]" -type "float2" -0.0298765 -0.28875628 ;
	setAttr ".uvtk[116]" -type "float2" -0.066932291 -0.19820313 ;
	setAttr ".uvtk[117]" -type "float2" 0.14683828 -0.10100339 ;
	setAttr ".uvtk[118]" -type "float2" 0.06991753 -0.0016484708 ;
	setAttr ".uvtk[119]" -type "float2" 0.23136476 -0.071467176 ;
	setAttr ".uvtk[120]" -type "float2" 0.19128945 0.041418985 ;
	setAttr ".uvtk[121]" -type "float2" 0.27460024 -0.032903805 ;
	setAttr ".uvtk[122]" -type "float2" 0.27304295 0.087134615 ;
createNode polyMapSewMove -n "anotherRock:polyMapSewMove11";
	rename -uid "3C775021-4A7C-0CB1-5465-CE847FE7C938";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[2]" "e[20]" "e[70]" "e[81]";
createNode polyTweakUV -n "anotherRock:polyTweakUV18";
	rename -uid "33FD357D-43C1-601D-31F3-3094EAC7DDFA";
	setAttr ".uopa" yes;
	setAttr -s 9 ".uvtk";
	setAttr ".uvtk[105]" -type "float2" -0.032735318 -0.65520912 ;
	setAttr ".uvtk[106]" -type "float2" -0.094744951 -0.75864512 ;
	setAttr ".uvtk[107]" -type "float2" 0.031256407 -0.82338291 ;
	setAttr ".uvtk[108]" -type "float2" 0.068314046 -0.73260504 ;
	setAttr ".uvtk[109]" -type "float2" -0.19023541 -0.47753263 ;
	setAttr ".uvtk[110]" -type "float2" -0.28249112 -0.50546443 ;
	setAttr ".uvtk[111]" -type "float2" -0.20382431 -0.65456223 ;
	setAttr ".uvtk[112]" -type "float2" -0.12289384 -0.58082372 ;
createNode polyMapSewMove -n "anotherRock:polyMapSewMove12";
	rename -uid "AD993284-471B-E091-F9A4-1A803FAF133C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[40]" "e[50]" "e[74]";
createNode polyTweakUV -n "anotherRock:polyTweakUV19";
	rename -uid "00DBA2AF-42F9-DF2C-B6C6-FFA326908070";
	setAttr ".uopa" yes;
	setAttr -s 13 ".uvtk";
	setAttr ".uvtk[83]" -type "float2" 0.37427101 -0.30774033 ;
	setAttr ".uvtk[84]" -type "float2" 0.46863499 -0.19592215 ;
	setAttr ".uvtk[85]" -type "float2" 0.34765348 -0.011164948 ;
	setAttr ".uvtk[86]" -type "float2" 0.27317569 -0.14415078 ;
	setAttr ".uvtk[87]" -type "float2" 0.66550255 -0.87279272 ;
	setAttr ".uvtk[88]" -type "float2" 0.81440008 -0.88353866 ;
	setAttr ".uvtk[89]" -type "float2" 0.75790906 -0.72718298 ;
	setAttr ".uvtk[90]" -type "float2" 0.6048857 -0.75735259 ;
	setAttr ".uvtk[91]" -type "float2" 0.5389725 -0.60065448 ;
	setAttr ".uvtk[92]" -type "float2" 0.6929431 -0.56137449 ;
	setAttr ".uvtk[93]" -type "float2" 0.61001039 -0.37590021 ;
	setAttr ".uvtk[94]" -type "float2" 0.49238971 -0.49890614 ;
createNode polyMapSewMove -n "anotherRock:polyMapSewMove13";
	rename -uid "501D3023-4B2B-5A6F-4CF1-E9A750F121A4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[1]" "e[41]" "e[44]" "e[123]" "e[139]";
createNode polyTweakUV -n "anotherRock:polyTweakUV20";
	rename -uid "096E4005-49E2-4FC4-2BA5-D080E4648863";
	setAttr ".uopa" yes;
	setAttr -s 11 ".uvtk";
	setAttr ".uvtk[89]" -type "float2" 0.40923941 -0.91659498 ;
	setAttr ".uvtk[90]" -type "float2" 0.55805379 -0.8947075 ;
	setAttr ".uvtk[91]" -type "float2" 0.53695381 -0.77375054 ;
	setAttr ".uvtk[92]" -type "float2" 0.39891326 -0.75724345 ;
	setAttr ".uvtk[93]" -type "float2" 0.40718251 -0.59227157 ;
	setAttr ".uvtk[94]" -type "float2" 0.55277777 -0.61765933 ;
	setAttr ".uvtk[95]" -type "float2" 0.57488561 -0.50556624 ;
	setAttr ".uvtk[96]" -type "float2" 0.44286555 -0.45702893 ;
	setAttr ".uvtk[97]" -type "float2" 0.62504303 -0.34839353 ;
	setAttr ".uvtk[98]" -type "float2" 0.49747711 -0.28201205 ;
createNode polyMapSewMove -n "anotherRock:polyMapSewMove14";
	rename -uid "883254D8-4A41-00CE-B127-71B4A660D965";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[24]" "e[27]" "e[130]" "e[145]";
createNode polyTweakUV -n "anotherRock:polyTweakUV21";
	rename -uid "D0BAB19B-4CE2-16D4-EA31-589193FD8B30";
	setAttr ".uopa" yes;
	setAttr -s 103 ".uvtk[0:102]" -type "float2" -0.066581272 0.46062192 -0.068414003
		 0.46049184 -0.08425682 0.44681633 -0.074313931 0.45084861 0.082998477 -0.10309015
		 0.03046516 -0.13874552 0.072489575 -0.20975377 0.12777011 -0.16858664 -0.035963804
		 0.50543195 -0.066908248 0.51785254 -0.062923275 0.50444436 -0.045612261 0.49589908
		 -0.055349931 0.48142165 -0.062867194 0.48843923 -0.06433326 0.47817194 -0.059561793
		 0.47368047 -0.020256557 0.4548583 -0.036023229 0.46996671 -0.042143296 0.46228015
		 -0.025385126 0.44361943 -0.0072213509 0.49022448 -0.018536309 0.49654222 -0.026749415
		 0.48410538 -0.012778827 0.4718805 -0.13756627 0.10081606 -0.17973624 0.052553076
		 -0.083169527 -0.025839521 -0.043546896 0.026398329 -0.029307857 0.4316026 -0.056300167
		 0.45143154 -0.069181457 0.445591 -0.043627717 0.43573502 0.093935758 0.18409212 0.035438061
		 0.10613344 0.16196676 -0.010949628 0.19512975 0.10786089 -0.16184981 -0.1238241 -0.082301758
		 -0.22236395 -0.22152026 0.0030556833 -0.033665054 -0.28297624 0.21171904 -0.12355593
		 -0.085177541 0.15490162 -0.021466635 -0.41503343 -0.13601567 -0.33933848 -0.16695088
		 -0.41823849 0.36789423 -0.13744196 0.29638568 -0.092284195 0.30851132 -0.2141722
		 0.22838081 -0.28803226 0.10936935 -0.35722342 0.28960237 -0.4037171 0.15200518 -0.47195864
		 0.45377988 -0.19258991 0.38927487 -0.30793607 -0.22552587 0.19370696 -0.28440964
		 0.1212483 -0.038434613 0.19916801 -0.049928769 0.2947681 -0.13920751 0.25919634 -0.02099237
		 -0.49032879 -0.19479865 -0.48440331 -0.31964642 0.2833842 -0.3886655 0.19375971 -0.076575898
		 0.37373146 -0.20599721 0.35295254 -0.43866363 0.054201782 -0.31918094 0.026075128
		 -0.18709947 -0.30270976 -0.31810078 -0.28324899 -0.23676752 -0.2186915 -0.27311215
		 -0.063097097 -0.32567099 -0.091836922 -0.25301298 -0.3825219 -0.28538865 -0.44429168
		 -0.38187996 -0.35166121 -0.43856019 -0.12365932 0.10978077 0.31874242 0.11271696
		 0.43044794 0.47742322 -0.019935915 0.27683052 0.040115442 0.58379453 -0.079796106
		 0.24858564 0.29290828 0.2824358 0.40077171 -0.084136918 0.46430749 -0.10812353 0.45127574
		 -0.095921814 0.51055104 -0.079110816 0.50014758 -0.07012476 0.48947522 -0.071662627
		 0.47941867 0.015987258 0.48485151 0.0032319725 0.4646987 -0.0065893652 0.4477517
		 -0.011978436 0.4366456 -0.012942317 0.42088893 -0.024345692 0.51821375 -0.042112648
		 0.53735745 -0.0033708052 0.49717891 -0.012868185 0.50356638 -0.028000755 0.42214286
		 -0.052349631 0.42903543 -0.075756133 0.43841186 -0.079777971 0.44505358 -0.081523925
		 0.44744882;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "7CC79EE1-43C5-F545-9826-DC84C382C6EF";
	setAttr ".pee" yes;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -805.95234892671215 -1749.4046923896649 ;
	setAttr ".tgi[0].vh" -type "double2" 1229.7618558955589 1764.880882250887 ;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "anotherRock:polyTweakUV21.out" "anotherRock:MeshShape.i";
connectAttr "anotherRock:polyTweakUV21.uvtk[0]" "anotherRock:MeshShape.uvst[0].uvtw"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "anotherRock:Material.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "anotherRock:Material.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "anotherRock:Material1.oc" "anotherRock:Material.ss";
connectAttr "anotherRock:Material.msg" "anotherRock:materialInfo1.sg";
connectAttr "anotherRock:Material1.msg" "anotherRock:materialInfo1.m";
connectAttr "anotherRock:polySurfaceShape1.o" "polySoftEdge1.ip";
connectAttr "anotherRock:MeshShape.wm" "polySoftEdge1.mp";
connectAttr "anotherRock:polyTweak1.out" "polySplitRing1.ip";
connectAttr "anotherRock:MeshShape.wm" "polySplitRing1.mp";
connectAttr "polySoftEdge1.out" "anotherRock:polyTweak1.ip";
connectAttr "anotherRock:polyTweak2.out" "polyPlanarProj1.ip";
connectAttr "anotherRock:MeshShape.wm" "polyPlanarProj1.mp";
connectAttr "polySplitRing1.out" "anotherRock:polyTweak2.ip";
connectAttr "polyPlanarProj1.out" "anotherRock:polyMapCut1.ip";
connectAttr "anotherRock:polyMapCut1.out" "anotherRock:polyTweakUV1.ip";
connectAttr "anotherRock:polyTweakUV1.out" "anotherRock:polyMapDel1.ip";
connectAttr "anotherRock:polyMapDel1.out" "anotherRock:polyPlanarProj1.ip";
connectAttr "anotherRock:MeshShape.wm" "anotherRock:polyPlanarProj1.mp";
connectAttr "anotherRock:polyPlanarProj1.out" "anotherRock:polyTweakUV2.ip";
connectAttr "anotherRock:polyTweakUV2.out" "anotherRock:polyPlanarProj2.ip";
connectAttr "anotherRock:MeshShape.wm" "anotherRock:polyPlanarProj2.mp";
connectAttr "anotherRock:polyPlanarProj2.out" "anotherRock:polyTweakUV3.ip";
connectAttr "anotherRock:polyTweakUV3.out" "anotherRock:polyPlanarProj3.ip";
connectAttr "anotherRock:MeshShape.wm" "anotherRock:polyPlanarProj3.mp";
connectAttr "anotherRock:polyPlanarProj3.out" "anotherRock:polyTweakUV4.ip";
connectAttr "anotherRock:polyTweakUV4.out" "anotherRock:polyPlanarProj4.ip";
connectAttr "anotherRock:MeshShape.wm" "anotherRock:polyPlanarProj4.mp";
connectAttr "anotherRock:polyPlanarProj4.out" "anotherRock:polyTweakUV5.ip";
connectAttr "anotherRock:polyTweakUV5.out" "anotherRock:polyPlanarProj5.ip";
connectAttr "anotherRock:MeshShape.wm" "anotherRock:polyPlanarProj5.mp";
connectAttr "anotherRock:polyPlanarProj5.out" "anotherRock:polyTweakUV6.ip";
connectAttr "anotherRock:polyTweakUV6.out" "anotherRock:polyPlanarProj6.ip";
connectAttr "anotherRock:MeshShape.wm" "anotherRock:polyPlanarProj6.mp";
connectAttr "anotherRock:polyPlanarProj6.out" "anotherRock:polyTweakUV7.ip";
connectAttr "anotherRock:polyTweakUV7.out" "anotherRock:polyPlanarProj7.ip";
connectAttr "anotherRock:MeshShape.wm" "anotherRock:polyPlanarProj7.mp";
connectAttr "anotherRock:polyPlanarProj7.out" "anotherRock:polyTweakUV8.ip";
connectAttr "anotherRock:polyTweakUV8.out" "anotherRock:polyPlanarProj8.ip";
connectAttr "anotherRock:MeshShape.wm" "anotherRock:polyPlanarProj8.mp";
connectAttr "anotherRock:polyPlanarProj8.out" "anotherRock:polyTweakUV9.ip";
connectAttr "anotherRock:polyTweakUV9.out" "anotherRock:polyPlanarProj9.ip";
connectAttr "anotherRock:MeshShape.wm" "anotherRock:polyPlanarProj9.mp";
connectAttr "anotherRock:polyPlanarProj9.out" "anotherRock:polyTweakUV10.ip";
connectAttr "anotherRock:polyTweakUV10.out" "anotherRock:polyPlanarProj10.ip";
connectAttr "anotherRock:MeshShape.wm" "anotherRock:polyPlanarProj10.mp";
connectAttr "anotherRock:polyPlanarProj10.out" "anotherRock:polyTweakUV11.ip";
connectAttr "anotherRock:polyTweakUV11.out" "anotherRock:polyMapSewMove1.ip";
connectAttr "anotherRock:polyMapSewMove1.out" "anotherRock:polyTweakUV12.ip";
connectAttr "anotherRock:polyTweakUV12.out" "anotherRock:polyMapSewMove2.ip";
connectAttr "anotherRock:polyMapSewMove2.out" "anotherRock:polyMapSewMove3.ip";
connectAttr "anotherRock:polyMapSewMove3.out" "anotherRock:polyTweakUV13.ip";
connectAttr "anotherRock:polyTweakUV13.out" "anotherRock:polyMapSewMove4.ip";
connectAttr "anotherRock:polyMapSewMove4.out" "anotherRock:polyTweakUV14.ip";
connectAttr "anotherRock:polyTweakUV14.out" "anotherRock:polyMapSewMove5.ip";
connectAttr "anotherRock:polyMapSewMove5.out" "anotherRock:polyMapSewMove6.ip";
connectAttr "anotherRock:polyMapSewMove6.out" "anotherRock:polyTweakUV15.ip";
connectAttr "anotherRock:polyTweakUV15.out" "anotherRock:polyMapSewMove7.ip";
connectAttr "anotherRock:polyMapSewMove7.out" "anotherRock:polyMapSewMove8.ip";
connectAttr "anotherRock:polyMapSewMove8.out" "anotherRock:polyTweakUV16.ip";
connectAttr "anotherRock:polyTweakUV16.out" "anotherRock:polyMapSewMove9.ip";
connectAttr "anotherRock:polyMapSewMove9.out" "anotherRock:polyMapSewMove10.ip";
connectAttr "anotherRock:polyMapSewMove10.out" "anotherRock:polyTweakUV17.ip";
connectAttr "anotherRock:polyTweakUV17.out" "anotherRock:polyMapSewMove11.ip";
connectAttr "anotherRock:polyMapSewMove11.out" "anotherRock:polyTweakUV18.ip";
connectAttr "anotherRock:polyTweakUV18.out" "anotherRock:polyMapSewMove12.ip";
connectAttr "anotherRock:polyMapSewMove12.out" "anotherRock:polyTweakUV19.ip";
connectAttr "anotherRock:polyTweakUV19.out" "anotherRock:polyMapSewMove13.ip";
connectAttr "anotherRock:polyMapSewMove13.out" "anotherRock:polyTweakUV20.ip";
connectAttr "anotherRock:polyTweakUV20.out" "anotherRock:polyMapSewMove14.ip";
connectAttr "anotherRock:polyMapSewMove14.out" "anotherRock:polyTweakUV21.ip";
connectAttr "anotherRock:Material.pa" ":renderPartition.st" -na;
connectAttr "anotherRock:Material1.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "anotherRock:MeshShape.iog" ":initialShadingGroup.dsm" -na;
// End of AnotherRock.ma
