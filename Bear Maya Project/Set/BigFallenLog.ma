//Maya ASCII 2017ff05 scene
//Name: BigFallenLog.ma
//Last modified: Tue, Nov 28, 2017 11:49:52 AM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "9EAA3962-4C7C-4D90-7827-FEA753C0F441";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 22.432105800986406 8.0030820958034763 5.7541143541992534 ;
	setAttr ".r" -type "double3" -19.538352729636358 77.799999999983712 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "2DA62028-43FD-7331-B682-8D86D1143C94";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 25.517058133081864;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -1.3291564460543364 0.87674472136614878 -4.9424110745121315 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "2758BEAB-4CC2-3740-4B05-23BAAB39A501";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "77267438-4999-C8B6-9DD1-C1AC90F97E7E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "B03FE496-468D-A6AE-FFB7-EE8B5B672FA2";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "1C722EA4-4B27-3408-68F6-1AB197AF08AF";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "631B0623-46D5-E39D-D74C-4B970FBCC622";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "FAD7925C-412F-812C-6B5E-E0A4D21ECC9D";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pPipe1";
	rename -uid "FED10089-4938-1F35-11E5-81A6F8FFA48B";
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 1.7606708240068933 1.7606708240068933 1.7606708240068933 ;
createNode mesh -n "pPipeShape1" -p "pPipe1";
	rename -uid "10471EDD-4BA1-EF48-5616-CC89CC67B35A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75001966953277588 0.26351851224899292 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "A015ED90-4961-5F56-06CE-40B4E50F1026";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "BF12B212-4F89-ED39-205A-24AC048ED462";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "A8341229-46E4-B68D-50EF-7092D7FEAFD3";
createNode displayLayerManager -n "layerManager";
	rename -uid "BF2C6B2C-4BF7-E459-AE91-8EB09DA29607";
createNode displayLayer -n "defaultLayer";
	rename -uid "FB2CCB33-4CC3-06E5-3E38-59BA0891C965";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "5ED51B61-4B7B-67AB-2EF9-8ABE048E7148";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "D3A3DA6E-4E1A-A032-0CC3-18894AA5715D";
	setAttr ".g" yes;
createNode polyPipe -n "polyPipe1";
	rename -uid "6ADFAC65-4148-ED51-2D09-2C9DDDA4974C";
	setAttr ".t" 0.15;
	setAttr ".sa" 10;
	setAttr ".sc" 0;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "4D0386C4-4D88-D9EC-4205-3EAD68226B20";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[60:69]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".wt" 0.70057952404022217;
	setAttr ".dr" no;
	setAttr ".re" 61;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak1";
	rename -uid "D98E04B5-476B-83E9-F874-67B49528B0FC";
	setAttr ".uopa" yes;
	setAttr -s 40 ".tk[0:39]" -type "float3"  0 -2.81079674 -6.2412225e-016
		 0 -2.81079674 0 0 -2.81079674 0 0 -2.81079674 0 0 -2.81079674 0 0 -2.81079674 -6.2412246e-016
		 0 -2.81079674 0 0 -2.81079674 0 0 -2.81079674 0 0 -2.81079674 0 0 2.81079674 6.2412225e-016
		 0 2.81079674 0 0 2.81079674 0 0 2.81079674 0 0 2.81079674 0 0 2.81079674 6.2412209e-016
		 0 2.81079674 0 0 2.81079674 0 0 2.81079674 0 0 2.81079674 0 0 2.81079674 6.2412225e-016
		 0 2.81079674 0 0 2.81079674 0 0 2.81079674 0 0 2.81079674 0 0 2.81079674 6.2412209e-016
		 0 2.81079674 0 0 2.81079674 0 0 2.81079674 0 0 2.81079674 0 0 -2.81079674 -6.2412225e-016
		 0 -2.81079674 0 0 -2.81079674 0 0 -2.81079674 0 0 -2.81079674 0 0 -2.81079674 -6.2412246e-016
		 0 -2.81079674 0 0 -2.81079674 0 0 -2.81079674 0 0 -2.81079674 0;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "16D04C06-4EBC-473F-6353-9E91A02F5AB1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[60:69]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".wt" 0.41902244091033936;
	setAttr ".re" 61;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "721750DA-4458-68CF-C94D-259534C7508E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[100:101]" "e[103]" "e[105]" "e[107]" "e[109]" "e[111]" "e[113]" "e[115]" "e[117]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".wt" 0.44869592785835266;
	setAttr ".re" 100;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "8CE1A2E5-49D0-253E-66DF-09A1F34F29EC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[80:81]" "e[83]" "e[85]" "e[87]" "e[89]" "e[91]" "e[93]" "e[95]" "e[97]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".wt" 0.6874271035194397;
	setAttr ".dr" no;
	setAttr ".re" 80;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "ACB1D14C-4C4E-A524-8ED0-4FB92D72EBF1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[60:69]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".wt" 0.42388832569122314;
	setAttr ".re" 61;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "70B70FEB-4E89-9ECE-4C42-81BEB6F7331B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:49]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".wt" 0.8590506911277771;
	setAttr ".dr" no;
	setAttr ".re" 42;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak2";
	rename -uid "B3286E9D-498B-1465-E935-76B0CD6FC8DA";
	setAttr ".uopa" yes;
	setAttr -s 50 ".tk[40:89]" -type "float3"  0.070491634 0.037797283 0.06129013
		 0.070491634 0.037797283 0.06129013 0.070491634 0.037797283 0.06129013 0.070491634
		 0.037797283 0.06129013 0.070491634 0.037797283 0.06129013 0.070491634 0.037797283
		 0.06129013 0.070491634 0.037797283 0.06129013 0.070491634 0.037797283 0.06129013
		 0.070491634 0.037797283 0.06129013 0.070491634 0.037797283 0.06129013 0.063970678
		 0.01210802 0.047584407 0.063970678 0.01210802 0.047584407 0.063970678 0.01210802
		 0.047584407 0.063970678 0.01210802 0.047584407 0.063970678 0.01210802 0.047584407
		 0.063970678 0.01210802 0.047584407 0.063970678 0.01210802 0.047584407 0.063970678
		 0.01210802 0.047584407 0.063970678 0.01210802 0.047584407 0.063970678 0.01210802
		 0.047584407 0.0048683109 -0.042381048 -0.012058496 0.0048683109 -0.042381048 -0.012058496
		 0.0048683109 -0.042381048 -0.012058496 0.0048683109 -0.042381048 -0.012058496 0.0048683109
		 -0.042381048 -0.012058496 0.0048683109 -0.042381048 -0.012058496 0.0048683109 -0.042381048
		 -0.012058496 0.0048683109 -0.042381048 -0.012058496 0.0048683109 -0.042381048 -0.012058496
		 0.0048683109 -0.042381048 -0.012058496 0.028617658 0.015344655 0.024882101 0.028617658
		 0.015344655 0.024882101 0.028617658 0.015344655 0.024882101 0.028617658 0.015344655
		 0.024882101 0.028617658 0.015344655 0.024882101 0.028617658 0.015344655 0.024882101
		 0.028617658 0.015344655 0.024882101 0.028617658 0.015344655 0.024882101 0.028617658
		 0.015344655 0.024882101 0.028617658 0.015344655 0.024882101 -0.040527314 -0.021730557
		 -0.035237152 -0.040527314 -0.021730557 -0.035237152 -0.040527314 -0.021730557 -0.035237152
		 -0.040527314 -0.021730557 -0.035237152 -0.040527314 -0.021730557 -0.035237152 -0.040527314
		 -0.021730557 -0.035237152 -0.040527314 -0.021730557 -0.035237152 -0.040527314 -0.021730557
		 -0.035237152 -0.040527314 -0.021730557 -0.035237152 -0.040527314 -0.021730557 -0.035237152;
createNode polySplitEdge -n "polySplitEdge1";
	rename -uid "4EB01B70-4F02-1BCF-E6B6-808F9B263888";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[52]" "e[62]" "e[180]";
createNode polyBridgeEdge -n "polyBridgeEdge1";
	rename -uid "607251E6-4B6C-A78F-8859-A1AE31F24EDF";
	setAttr ".ics" -type "componentList" 2 "e[62]" "e[180]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 81;
	setAttr ".sv2" 12;
	setAttr ".d" 1;
	setAttr ".sd" 1;
createNode polyTweak -n "polyTweak3";
	rename -uid "5B1DD5D9-43FE-24C2-E739-13AC092CF663";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[12]" -type "float3" -0.17748001 0.019365048 -0.067256123 ;
	setAttr ".tk[22]" -type "float3" -0.17748001 0.019365048 -0.067256123 ;
	setAttr ".tk[100]" -type "float3" 0.061204635 0.0079817912 0.031435043 ;
	setAttr ".tk[101]" -type "float3" 0.061204635 0.0079817912 0.031435043 ;
createNode polyBridgeEdge -n "polyBridgeEdge2";
	rename -uid "8A054C6B-460E-E9CC-C4C9-84AEDDA4B281";
	setAttr ".ics" -type "componentList" 1 "e[201:202]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 90;
	setAttr ".sv2" 101;
	setAttr ".d" 1;
	setAttr ".sd" 1;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "718A6384-4A1A-5528-B31F-76BBFF2184EF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".a" 180;
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "2B5853BF-45B5-69A8-E8E6-7988149723AE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[0:39]" "e[42]" "e[52]" "e[200]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".a" 0;
createNode polySoftEdge -n "polySoftEdge3";
	rename -uid "A32FEB38-4A92-6E10-6CD7-77A48F466E25";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[62]" "e[180]" "e[201:202]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".a" 0;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "3837ACF0-4644-1FE6-CD73-09AC7FB14017";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 809\n            -height 811\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n"
		+ "            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n"
		+ "            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n"
		+ "            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n"
		+ "            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n"
		+ "                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n"
		+ "                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 809\\n    -height 811\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 809\\n    -height 811\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "195446D8-4DA6-B40B-CFDD-9799528F40D0";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "ADCBF463-4DA3-5C39-5404-748133F49F00";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[40:49]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".wt" 0.12118729203939438;
	setAttr ".re" 46;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "4E6270F7-45FE-6AC0-B00B-A19E90AB53C5";
	setAttr ".uopa" yes;
	setAttr -s 30 ".tk[60:89]" -type "float3"  -0.013135122 0 0.0095432233
		 -0.0050171711 0 0.015441263 0.0050171674 0 0.015441263 0.013135119 0 0.009543227
		 0.016235901 0 1.9205855e-009 0.01313512 0 -0.0095432233 0.0050171707 0 -0.015441261
		 -0.0050171702 0 -0.015441263 -0.013135122 0 -0.009543227 -0.016235901 0 9.261471e-010
		 -0.026072782 0 0.018942982 -0.0099589182 0 0.030650388 0.0099589126 0 0.030650388
		 0.026072778 0 0.018942988 0.032227725 0 3.2508516e-009 0.026072782 0 -0.018942975
		 0.0099589182 0 -0.030650387 -0.0099589135 0 -0.030650388 -0.026072782 0 -0.018942986
		 -0.032227725 0 1.3215218e-009 0.019796751 0 -0.014383186 0.0075616883 0 -0.023272481
		 -0.0075616837 0 -0.023272481 -0.019796751 0 -0.014383188 -0.024470128 0 -4.8224105e-009
		 -0.019796751 0 0.014383179 -0.0075616878 0 0.023272473 0.0075616874 0 0.023272481
		 0.019796751 0 0.014383184 0.024470132 0 -3.3756142e-009;
createNode polySplitEdge -n "polySplitEdge2";
	rename -uid "BEDC6824-4C23-DB65-CD1D-3E8B109E445B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[44]" "e[74]" "e[145]";
createNode polyBridgeEdge -n "polyBridgeEdge3";
	rename -uid "D540A823-4091-5261-4C65-CCA86A6E1129";
	setAttr ".ics" -type "componentList" 2 "e[44]" "e[145]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 110;
	setAttr ".sv2" 34;
	setAttr ".d" 1;
	setAttr ".sd" 1;
createNode polyTweak -n "polyTweak5";
	rename -uid "AE02074D-43B2-42B2-B01E-3995C53FA9A9";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[4]" -type "float3" -0.064496957 0.0025313077 0.14865723 ;
	setAttr ".tk[34]" -type "float3" -0.064496957 0.0025313077 0.14865723 ;
	setAttr ".tk[112]" -type "float3" 0.051348489 -0.0076897312 -0.05717586 ;
	setAttr ".tk[113]" -type "float3" 0.051348489 -0.0076897312 -0.05717586 ;
createNode polyBridgeEdge -n "polyBridgeEdge4";
	rename -uid "7AF0E29D-4308-3E1E-59CA-509C1302BF71";
	setAttr ".ics" -type "componentList" 2 "e[224]" "e[226]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 112;
	setAttr ".sv2" 73;
	setAttr ".d" 1;
	setAttr ".td" 1;
createNode polyMapDel -n "polyMapDel1";
	rename -uid "2560C1AA-4216-9FEE-4963-7C9690743551";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyTweak -n "polyTweak6";
	rename -uid "B802CCAB-458C-201B-02B7-9E887CB9E5FE";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[71]" -type "float3" 0.049654868 0.022465074 -0.020669997 ;
	setAttr ".tk[72]" -type "float3" -0.01077572 0.051726349 -0.064004041 ;
	setAttr ".tk[73]" -type "float3" 0 0.32569799 1.110223e-016 ;
	setAttr ".tk[74]" -type "float3" -0.011203777 0.12535408 0.05762776 ;
	setAttr ".tk[75]" -type "float3" -0.00059473375 0.080889545 0.064357899 ;
	setAttr ".tk[110]" -type "float3" 0 0.32569799 5.5511151e-017 ;
createNode polyCylProj -n "polyCylProj1";
	rename -uid "C3BE5A3C-4BFC-93CB-6342-8EB6A335B184";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:113]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0.004836738109588623 0.0052519440650939941 0.010278224945068359 ;
	setAttr ".ro" -type "double3" 89.072074195535322 0 0 ;
	setAttr ".ps" -type "double2" 180 5.5336639825312171 ;
	setAttr ".r" 11.706079483032227;
createNode polyMapDel -n "polyMapDel2";
	rename -uid "3C6928E0-4CC0-DAD8-ABCD-1A83F69CCEB3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "f[10:19]" "f[30:39]" "f[100:101]" "f[112:113]";
createNode polyPlanarProj -n "polyPlanarProj1";
	rename -uid "E5D1D224-4345-4FCA-E1B6-2D8640FA8124";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[10:19]" "f[30:39]" "f[101]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -3.5762786865234375e-007 0.05920785665512085 0.010278224945068359 ;
	setAttr ".ps" -type "double2" 3.5213413238525391 3.5809118184641036 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyMapDel -n "polyMapDel3";
	rename -uid "9CA347CD-4749-1DF7-73E3-FBBF5CB4B77B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[100:101]";
createNode polyPlanarProj -n "polyPlanarProj2";
	rename -uid "A7A7DFF0-4D73-79AA-FDC3-F2827E6AD6C3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[100:101]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0.40091004222631454 1.5804447531700134 5.0246455669403076 ;
	setAttr ".ro" -type "double3" 19.170182783183339 89.999999999999972 0 ;
	setAttr ".ps" -type "double2" 1.677344799041748 1.6686003350943202 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj3";
	rename -uid "2F05324E-4642-316B-F614-C7A82D005921";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[112:113]";
	setAttr ".ix" -type "matrix" 1.7606708240068933 0 0 0 0 3.9094745751963996e-016 1.7606708240068933 0
		 0 -1.7606708240068933 3.9094745751963996e-016 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -1.3291564583778381 0.87674471735954285 -4.9424111843109131 ;
	setAttr ".ro" -type "double3" -89.999999999999943 3.1805546814635176e-015 -34.650324487463038 ;
	setAttr ".ps" -type "double2" 1.6524610757416631 1.8007006645202637 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "9D5C76A6-4631-8993-BA62-71B1F255AC25";
	setAttr ".uopa" yes;
	setAttr -s 181 ".uvtk[0:180]" -type "float2" -0.62534785 1.0297364 -0.38116562
		 1.031689525 -0.37596428 0.86563993 -0.6213783 0.8636868 -0.86281824 1.028521419 1.38806462
		 1.028509021 1.38662481 0.86245906 1.18595159 1.031333685 1.1456604 0.78519964 0.90883327
		 1.031648755 1.078679681 1.029672027 0.90367275 0.86559892 0.65738326 1.03360188 0.65290254
		 0.86755204 0.3973878 1.034816742 0.39555871 0.86876702 0.13332221 1.034829378 0.13505498
		 0.86877966 -0.12792295 1.033635139 -0.12346295 0.86758518 -0.5845542 -0.56526875
		 -0.33116311 -0.56331575 -0.64008015 -0.56602126 -0.7941668 -0.56832045 -0.81284189
		 -0.56828272 -0.93535739 -0.57155043 1.37264705 -0.56649649 -0.93905473 -0.57141507
		 0.40188074 -0.56976563 1.11112666 -0.56530213 0.24366088 -0.56835949 0.85915035 -0.56335688
		 0.090300612 -0.56606966 0.61701304 -0.56140375 -0.057939135 -0.56377059 0.38158512
		 -0.56018877 -0.20262066 -0.56234032 0.14827845 -0.56017625 -0.3461661 -0.56232542
		 -0.087814301 -0.56137085 -0.49120358 -0.56373137 -0.65186322 -0.36233085 -0.80992001
		 -0.36468619 -0.96602082 -0.36615127 0.39679378 -0.36616665 0.24820457 -0.36472613
		 0.10216821 -0.36238033 -0.043282777 -0.36002475 -0.19025201 -0.35855955 -0.34045205
		 -0.35854441 -0.49459931 -0.35998482 -0.81528437 1.027608871 -0.66598576 1.02990818
		 -0.96104157 1.026178837 0.40984353 1.026163816 0.28252324 1.029201269 0.11606539
		 1.0298599 0.22674829 1.027538538 -0.037008137 1.03215909 -0.19444267 1.033589244
		 -0.35392597 1.033604264 -0.51199102 1.032198191 -0.93767506 0.53945577 -0.78774714
		 0.54088598 -0.80463773 0.87471747 0.41999635 0.53944093 0.41583872 0.86060184 0.25662568
		 0.54084712 0.26130396 0.79618031 0.092695691 0.54313695 0.09273649 0.84690791 -0.066159897
		 0.54543602 -0.061245076 0.85987628 -0.21747954 0.5468663 -0.20413789 0.88050503 -0.36224893
		 0.54688114 -0.35800889 0.88051939 -0.5032748 0.54547507 -0.50902379 0.87915868 -0.64395571
		 0.54318511 -0.65750355 0.87694252 -0.93627328 -0.1039829 -0.78255218 -0.10255275
		 -0.80636472 0.20184505 0.41639292 -0.10399792 0.40795648 0.20042354 0.24931929 -0.10259167
		 0.25713673 0.20180696 0.085213564 -0.10030178 0.1056736 0.20405957 -0.070615642 -0.09800265
		 -0.046478003 0.20632145 -0.2176833 -0.096572436 -0.19909586 0.20772839 -0.35863632
		 -0.096557595 -0.35174912 0.20774308 -0.49717858 -0.097963549 -0.50397879 0.20635983
		 -0.63720983 -0.10025362 -0.65551096 0.20410714 1.37505412 -0.34168172 -0.84953237
		 -0.34166896 1.11717677 -0.34048748 0.86614352 -0.3385421 0.62234932 -0.33658874 0.38359368
		 -0.33537376 0.14637911 -0.335361 -0.093106657 -0.33655548 -0.33819193 -0.33850086
		 -0.59067088 -0.34045422 -0.86134815 0.86247182 -1.099042535 0.86245906 -1.11332905
		 -0.56976563 -1.00032997131 -0.57468504 -1.11841607 -0.36616665 -1.097602606 1.028509021
		 -0.94254261 0.8678394 -1.099370956 0.86060184 -0.95681274 0.2004382 -1.10725331 0.20042354
		 -1.095213294 0.53944093 -1.10536623 1.026163816 -1.098816872 -0.10399792 -1.11302042
		 -0.56649649 -1.11061287 -0.34168172 0.043315984 -0.64263815 -0.14545408 -0.43253878
		 -0.22045386 -0.46539059 0.0016285721 -0.71256649 0.28494561 -0.69932842 0.2924934
		 -0.77962279 0.58498269 -0.5863263 0.4255428 -0.69557834 0.43309087 -0.77587265 0.63888252
		 -0.64631724 0.73098028 -0.34142405 0.81064528 -0.35819679 0.70454603 -0.060217664
		 0.77954614 -0.02736561 0.5157761 0.14988187 0.55746388 0.21981022 0.23677441 0.2086235
		 0.22922617 0.28891823 -0.025890384 0.093569964 -0.079790831 0.15356095 -0.17188826
		 -0.15133208 -0.25155327 -0.13455933 -0.50654209 -0.29700291 -0.64980167 -0.0087347627
		 -0.57093692 -0.019854099 -0.44916609 -0.26488191 -0.21717805 -0.5034771 -0.20320678
		 -0.44038516 0.10776338 -0.54929161 0.072993502 -0.47932738 0.32133579 -0.43980378
		 0.25110584 -0.38969135 0.40173054 -0.15699345 0.36723909 -0.35240227 0.29700872 -0.30228984
		 0.32286549 -0.14587399 0.25847104 0.13127458 0.20109472 0.099153668 -0.030893505
		 0.33774894 -0.044864893 0.27465689 -0.35583481 0.38356346 -0.32106483 0.31359929
		 -0.59223676 0.25121856 -0.52200651 0.20110607 -0.78379357 -0.28099942 0.12043664
		 -0.15612657 0.10588205 0.0021172643 -0.89462709 -0.084322169 0.092493445 0.016677488
		 0.107048 -0.14156605 -0.5028773 0.32514074 -0.53992695 -0.56075358 -0.36334589 -0.56075358
		 -0.40861467 0.27764523 -0.51433802 -0.56969649 -0.33775687 -0.56969649;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "8DB11DAE-4BD1-352A-7E96-65AF9BE16347";
	setAttr ".pee" yes;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -171.42856461661233 -286.90475050419138 ;
	setAttr ".tgi[0].vh" -type "double2" 167.85713618709957 298.80951193590062 ;
createNode polyMapSewMove -n "polyMapSewMove1";
	rename -uid "DB0EE491-4D04-8A5F-B6DD-6FB1F7BB860C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[30]";
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "7BA7EF30-4739-9189-994D-F6AE6414FC1F";
	setAttr ".uopa" yes;
	setAttr -s 23 ".uvtk";
	setAttr ".uvtk[125]" -type "float2" -0.51694566 0.93457729 ;
	setAttr ".uvtk[126]" -type "float2" -0.44149011 0.94550842 ;
	setAttr ".uvtk[127]" -type "float2" -0.43763632 0.96727234 ;
	setAttr ".uvtk[128]" -type "float2" -0.52640772 0.95441222 ;
	setAttr ".uvtk[129]" -type "float2" -0.56586385 0.88880026 ;
	setAttr ".uvtk[130]" -type "float2" -0.58502722 0.89912987 ;
	setAttr ".uvtk[131]" -type "float2" -0.58541822 0.80449295 ;
	setAttr ".uvtk[132]" -type "float2" -0.58607721 0.85666239 ;
	setAttr ".uvtk[133]" -type "float2" -0.6052407 0.86699188 ;
	setAttr ".uvtk[134]" -type "float2" -0.60696304 0.80137157 ;
	setAttr ".uvtk[135]" -type "float2" -0.55228043 0.73502725 ;
	setAttr ".uvtk[136]" -type "float2" -0.56797785 0.71964735 ;
	setAttr ".uvtk[137]" -type "float2" -0.48516625 0.69885105 ;
	setAttr ".uvtk[138]" -type "float2" -0.48902005 0.67708713 ;
	setAttr ".uvtk[139]" -type "float2" -0.40971068 0.70978218 ;
	setAttr ".uvtk[140]" -type "float2" -0.40024883 0.68994731 ;
	setAttr ".uvtk[141]" -type "float2" -0.35473523 0.76364535 ;
	setAttr ".uvtk[142]" -type "float2" -0.33557159 0.75331563 ;
	setAttr ".uvtk[143]" -type "float2" -0.34123826 0.8398667 ;
	setAttr ".uvtk[144]" -type "float2" -0.31969312 0.84298784 ;
	setAttr ".uvtk[145]" -type "float2" -0.37437597 0.90933216 ;
	setAttr ".uvtk[146]" -type "float2" -0.35867852 0.92471206 ;
createNode polyMapSewMove -n "polyMapSewMove2";
	rename -uid "51043389-439D-C34E-F0B0-AC92D75DF21D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[29]";
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "3D6CD8EC-4848-8A23-7F7A-E2930E5493A2";
	setAttr ".uopa" yes;
	setAttr -s 177 ".uvtk[0:176]" -type "float2" 0.0084283473 -0.47547242
		 0.00918628 -0.47548687 0.0092024328 -0.47425753 0.0084406855 -0.47424304 0.0076912763
		 -0.47546342 0.014677834 -0.47546333 0.014673363 -0.47423398 0.014050496 -0.47548425
		 0.013925445 -0.47366199 0.013190341 -0.47548658 0.013717544 -0.47547194 0.013174308
		 -0.47425723 0.012409878 -0.47550103 0.01239599 -0.47427168 0.011602831 -0.47551003
		 0.011597169 -0.47428069 0.010783208 -0.47551012 0.010788572 -0.47428077 0.009972346
		 -0.47550127 0.0099861743 -0.47427192 0.0085550072 -0.46366376 0.0093414905 -0.46367824
		 0.0050905347 -0.31050998 0.006228596 -0.31049299 0.0078464272 -0.46364146 0.007271409
		 -0.31046915 0.014629971 -0.4636547 0.0074546458 -0.46361828 -0.0026052422 -0.31048238
		 0.013818217 -0.46366352 -0.0014366508 -0.31049275 0.013036144 -0.46367794 -0.00030395389
		 -0.31050962 0.012284589 -0.46369237 0.00079092383 -0.31052661 0.011553776 -0.46370137
		 0.0018595159 -0.31053722 0.01082964 -0.46370149 0.0029197335 -0.31053728 0.010096801
		 -0.46369267 0.0039909482 -0.31052691 0.0051775575 -0.31201446 0.0063449442 -0.31199706
		 0.0074978769 -0.31198621 -0.0025676708 -0.31198609 -0.0014702119 -0.31199676 -0.00039160997
		 -0.3120141 0.0006826669 -0.31203145 0.0017681718 -0.3120423 0.0028775185 -0.31204242
		 0.0040160418 -0.31203175 0.0063845813 -0.32228035 0.0052818656 -0.3222973 0.0074611008
		 -0.32226977 -0.0026640547 -0.32226968 -0.0017236844 -0.32229209 -0.00049424917 -0.32229698
		 -0.0013117418 -0.32227981 0.00063632429 -0.32231393 0.0017991215 -0.32232451 0.0029770434
		 -0.3223246 0.0041444898 -0.3223142 0.0072885156 -0.31867489 0.0061811805 -0.31868547
		 0.006305933 -0.32115108 -0.002739042 -0.3186748 -0.0027083359 -0.32104683 -0.0015324131
		 -0.31868517 -0.0015669614 -0.32057104 -0.00032164156 -0.3187021 -0.00032194704 -0.32094568
		 0.00085164607 -0.31871909 0.00081533194 -0.32104146 0.001969263 -0.31872964 0.0018707216
		 -0.32119384 0.0030385256 -0.31872976 0.0030072033 -0.32119393 0.0040801167 -0.31871936
		 0.0041225553 -0.32118392 0.0051191747 -0.31870246 0.0052192211 -0.32116753 0.0072781742
		 -0.31392258 0.0061428249 -0.31393313 0.0063186884 -0.31618136 -0.0027124253 -0.31392246
		 -0.002650117 -0.31617087 -0.0014784448 -0.31393284 -0.0015361831 -0.31618106 -0.00026638806
		 -0.31394976 -0.00041750073 -0.31619772 0.00088454783 -0.31396675 0.00070627034 -0.31621441
		 0.001970768 -0.3139773 0.0018334836 -0.31622481 0.0030118227 -0.31397742 0.0029609501
		 -0.31622493 0.0040350854 -0.31396699 0.0040853024 -0.31621471 0.0050693452 -0.31395012
		 0.0052044988 -0.31619805 0.014637422 -0.4653191 0.0077325227 -0.46531919 0.013836992
		 -0.46532795 0.01305784 -0.46534237 0.012301159 -0.4653568 0.011560095 -0.4653658
		 0.010823739 -0.46536589 0.010080409 -0.46535707 0.0093196752 -0.46534267 0.0085359933
		 -0.46532819 0.0076958062 -0.47423407 0.0069580199 -0.47423398 0.0085858703 -0.31048238
		 0.007751286 -0.31044602 0.0086234212 -0.31198609 0.0069624903 -0.47546333 0.007324487
		 -0.32110029 0.008482784 -0.32104683 0.007429868 -0.31617096 0.0085410178 -0.31617087
		 0.0084520876 -0.3186748 0.0085270703 -0.32226968 0.0084787011 -0.31392246 0.0069146873
		 -0.4636547 0.0069221379 -0.4653191 0.0057436824 -0.30968565 0.0050015748 -0.31025404
		 0.0059635937 -0.30984133 0.0059933066 -0.30890316 0.0062601864 -0.30889904 0.0056982338
		 -0.30788392 0.0060173869 -0.3084383 0.0062842369 -0.30843419 0.0059101284 -0.30772167
		 0.0049280226 -0.30733877 0.0050040185 -0.30708033 0.003993392 -0.30735314 0.0039044321
		 -0.3070972 0.0032512844 -0.30792153 0.003031373 -0.3077659 0.0029852092 -0.30882686
		 0.0027183145 -0.30883092 0.0032967329 -0.30972326 0.0030848384 -0.30988556 0.0040669441
		 -0.3102684 0.0053710938 -0.32255399 0.0063083768 -0.32253957 0.0072932541 -0.32292348
		 0.0070807636 -0.32308623 0.0076608658 -0.32398108 0.0073932409 -0.32398519 0.0074126422
		 -0.32492787 0.0071921051 -0.32477176 0.0064714551 -0.32571977 0.0071351826 -0.32524648
		 0.0069146156 -0.32509041 0.0063822269 -0.32546309 0.0053687692 -0.32573676 0.0054449439
		 -0.32547754 0.0044600666 -0.32509363 0.0046725571 -0.32493085 0.0040924549 -0.324036
		 0.0043600798 -0.32403189 0.0044063628 -0.32296792 0.0046269 -0.32312402 -0.084875211
		 -0.33865404 -0.084963016 -0.3367261 -0.085299 -0.3367261 -0.085310757 -0.33884931
		 -0.085332245 -0.33675146 -0.084996268 -0.33675146 -0.05145774 -0.32514063 -0.051539123
		 -0.32273728 -0.051151242 -0.32273728 -0.051250681 -0.32501176 -0.051482916 -0.32271302
		 -0.051095031 -0.32271302;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polyTweakUV3.out" "pPipeShape1.i";
connectAttr "polyTweakUV3.uvtk[0]" "pPipeShape1.uvst[0].uvtw";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyTweak1.out" "polySplitRing1.ip";
connectAttr "pPipeShape1.wm" "polySplitRing1.mp";
connectAttr "polyPipe1.out" "polyTweak1.ip";
connectAttr "polySplitRing1.out" "polySplitRing2.ip";
connectAttr "pPipeShape1.wm" "polySplitRing2.mp";
connectAttr "polySplitRing2.out" "polySplitRing3.ip";
connectAttr "pPipeShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing3.out" "polySplitRing4.ip";
connectAttr "pPipeShape1.wm" "polySplitRing4.mp";
connectAttr "polySplitRing4.out" "polySplitRing5.ip";
connectAttr "pPipeShape1.wm" "polySplitRing5.mp";
connectAttr "polyTweak2.out" "polySplitRing6.ip";
connectAttr "pPipeShape1.wm" "polySplitRing6.mp";
connectAttr "polySplitRing5.out" "polyTweak2.ip";
connectAttr "polySplitRing6.out" "polySplitEdge1.ip";
connectAttr "polyTweak3.out" "polyBridgeEdge1.ip";
connectAttr "pPipeShape1.wm" "polyBridgeEdge1.mp";
connectAttr "polySplitEdge1.out" "polyTweak3.ip";
connectAttr "polyBridgeEdge1.out" "polyBridgeEdge2.ip";
connectAttr "pPipeShape1.wm" "polyBridgeEdge2.mp";
connectAttr "polyBridgeEdge2.out" "polySoftEdge1.ip";
connectAttr "pPipeShape1.wm" "polySoftEdge1.mp";
connectAttr "polySoftEdge1.out" "polySoftEdge2.ip";
connectAttr "pPipeShape1.wm" "polySoftEdge2.mp";
connectAttr "polySoftEdge2.out" "polySoftEdge3.ip";
connectAttr "pPipeShape1.wm" "polySoftEdge3.mp";
connectAttr "polyTweak4.out" "polySplitRing7.ip";
connectAttr "pPipeShape1.wm" "polySplitRing7.mp";
connectAttr "polySoftEdge3.out" "polyTweak4.ip";
connectAttr "polySplitRing7.out" "polySplitEdge2.ip";
connectAttr "polyTweak5.out" "polyBridgeEdge3.ip";
connectAttr "pPipeShape1.wm" "polyBridgeEdge3.mp";
connectAttr "polySplitEdge2.out" "polyTweak5.ip";
connectAttr "polyBridgeEdge3.out" "polyBridgeEdge4.ip";
connectAttr "pPipeShape1.wm" "polyBridgeEdge4.mp";
connectAttr "polyTweak6.out" "polyMapDel1.ip";
connectAttr "polyBridgeEdge4.out" "polyTweak6.ip";
connectAttr "polyMapDel1.out" "polyCylProj1.ip";
connectAttr "pPipeShape1.wm" "polyCylProj1.mp";
connectAttr "polyCylProj1.out" "polyMapDel2.ip";
connectAttr "polyMapDel2.out" "polyPlanarProj1.ip";
connectAttr "pPipeShape1.wm" "polyPlanarProj1.mp";
connectAttr "polyPlanarProj1.out" "polyMapDel3.ip";
connectAttr "polyMapDel3.out" "polyPlanarProj2.ip";
connectAttr "pPipeShape1.wm" "polyPlanarProj2.mp";
connectAttr "polyPlanarProj2.out" "polyPlanarProj3.ip";
connectAttr "pPipeShape1.wm" "polyPlanarProj3.mp";
connectAttr "polyPlanarProj3.out" "polyTweakUV1.ip";
connectAttr "polyTweakUV1.out" "polyMapSewMove1.ip";
connectAttr "polyMapSewMove1.out" "polyTweakUV2.ip";
connectAttr "polyTweakUV2.out" "polyMapSewMove2.ip";
connectAttr "polyMapSewMove2.out" "polyTweakUV3.ip";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pPipeShape1.iog" ":initialShadingGroup.dsm" -na;
// End of BigFallenLog.ma
