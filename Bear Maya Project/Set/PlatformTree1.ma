//Maya ASCII 2017ff05 scene
//Name: PlatformTree1.ma
//Last modified: Wed, Nov 08, 2017 07:27:27 PM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "5695FB89-4506-52D8-095F-4AB4055D956D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -7.5736411799973311 9.6145753104317073 -25.826546443321124 ;
	setAttr ".r" -type "double3" -16.538352729600756 173.3999999999061 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "5B42ED32-4C83-62E4-AEB8-6EB06814D3CB";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 26.16208091814638;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "6F793389-4912-F08F-DA48-10A812D9AD99";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "3CBF10C4-47E5-4D1A-9355-AEB4FBA7CD4A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "0B661F04-419C-C769-F203-18BCCC18099D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "E4437BA5-4E85-C9D0-85B8-6C8D2EA47E13";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "52026FCC-464C-61BD-22FE-85B5FD08CDBF";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "AD1A2424-496D-4FC0-F645-B0923CAA1545";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCube1";
	rename -uid "93902335-4096-E2C6-1796-5FBB532983F0";
	setAttr ".t" -type "double3" -3.0947909202365347 0 0 ;
	setAttr ".s" -type "double3" 2.2543915282992639 1 2.2543915282992639 ;
createNode transform -n "transform2" -p "pCube1";
	rename -uid "3FC2310C-4114-1B67-4138-8291F16FF50A";
	setAttr ".v" no;
createNode mesh -n "pCubeShape1" -p "transform2";
	rename -uid "A36D08BE-4EE6-C37B-BC62-69AD92250DC8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 41 ".pt";
	setAttr ".pt[12]" -type "float3" 0.052610185 -0.037793092 -0.054271385 ;
	setAttr ".pt[13]" -type "float3" -0.05567297 -0.037793092 -0.054271385 ;
	setAttr ".pt[14]" -type "float3" -0.05567297 -0.037793092 0.05401177 ;
	setAttr ".pt[15]" -type "float3" 0.052610185 -0.037793092 0.05401177 ;
	setAttr ".pt[16]" -type "float3" 0.03274449 0.017443279 -0.03440569 ;
	setAttr ".pt[17]" -type "float3" -0.035807274 0.017443279 -0.03440569 ;
	setAttr ".pt[18]" -type "float3" -0.035807274 0.017443279 0.034146074 ;
	setAttr ".pt[19]" -type "float3" 0.03274449 0.017443279 0.034146074 ;
	setAttr ".pt[28]" -type "float3" 0.012871571 0.017443279 -0.028388999 ;
	setAttr ".pt[29]" -type "float3" 0.012871571 0.017443279 0.027918737 ;
	setAttr ".pt[30]" -type "float3" 0.021219276 -0.037793092 0.044236224 ;
	setAttr ".pt[31]" -type "float3" 0 0 -0.021959662 ;
	setAttr ".pt[32]" -type "float3" 0 0 -0.02669517 ;
	setAttr ".pt[33]" -type "float3" 0 0 -0.02914699 ;
	setAttr ".pt[34]" -type "float3" 0 0 -0.026790552 ;
	setAttr ".pt[35]" -type "float3" 0 0 -0.020372329 ;
	setAttr ".pt[36]" -type "float3" 0 0 0.020372329 ;
	setAttr ".pt[37]" -type "float3" 0 0 0.026790552 ;
	setAttr ".pt[38]" -type "float3" 0 0 0.02914699 ;
	setAttr ".pt[39]" -type "float3" 0 0 0.02669517 ;
	setAttr ".pt[40]" -type "float3" 0 0 0.021959662 ;
	setAttr ".pt[41]" -type "float3" 0.021219276 -0.037793092 -0.044706482 ;
	setAttr ".pt[42]" -type "float3" -0.014817134 0.017443279 -0.041821089 ;
	setAttr ".pt[43]" -type "float3" -0.014817134 0.017443279 0.041821085 ;
	setAttr ".pt[44]" -type "float3" -0.022517314 -0.037793092 0.066059858 ;
	setAttr ".pt[55]" -type "float3" -0.022517314 -0.037793092 -0.066059858 ;
	setAttr ".pt[56]" -type "float3" -0.045836814 0.017443279 0.01607644 ;
	setAttr ".pt[57]" -type "float3" -0.017766898 0.037793092 0.019773751 ;
	setAttr ".pt[58]" -type "float3" 0.019260949 0.037793092 0.01307652 ;
	setAttr ".pt[59]" -type "float3" 0.045836814 0.017443279 0.01607644 ;
	setAttr ".pt[60]" -type "float3" 0.072403029 -0.037793092 0.025469292 ;
	setAttr ".pt[66]" -type "float3" 0 0 -0.0096323993 ;
	setAttr ".pt[73]" -type "float3" -0.072403029 -0.037793092 0.025469292 ;
	setAttr ".pt[74]" -type "float3" -0.041714914 0.017443279 -0.014971061 ;
	setAttr ".pt[75]" -type "float3" -0.016554616 0.037793092 -0.018108288 ;
	setAttr ".pt[76]" -type "float3" 0.01663507 0.037793092 -0.012425588 ;
	setAttr ".pt[77]" -type "float3" 0.040456183 0.017443279 -0.014971061 ;
	setAttr ".pt[78]" -type "float3" 0.064268649 -0.037793092 -0.02357279 ;
	setAttr ".pt[84]" -type "float3" 0 0 0.0088210981 ;
	setAttr ".pt[91]" -type "float3" -0.06552738 -0.037793092 -0.02357279 ;
createNode transform -n "pCylinder1";
	rename -uid "21324372-4AEB-32E6-FD3D-F0BFB7BB0E68";
	setAttr ".t" -type "double3" 0 1.1271374902885161 0 ;
	setAttr ".r" -type "double3" 0 0 78.70007444864234 ;
	setAttr ".s" -type "double3" 0.4218251925589297 4.2329116345406765 0.44882996496509325 ;
createNode transform -n "transform1" -p "pCylinder1";
	rename -uid "945CE852-474D-3D5B-57A0-A3B5DDEB7384";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape1" -p "transform1";
	rename -uid "062219A2-430A-6C0C-D2D5-0F963A8681CF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube2";
	rename -uid "1BE53FDC-43E4-BF14-E47E-EBA598E82DBE";
	setAttr ".r" -type "double3" 0 180 0 ;
	setAttr ".rp" -type "double3" -0.25355663597036848 0.93510144252463845 0 ;
	setAttr ".sp" -type "double3" -0.25355663597036848 0.93510144252463845 0 ;
createNode mesh -n "pCube2Shape" -p "pCube2";
	rename -uid "E1FB9748-4B3E-E353-34A8-B5A401ADF310";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50773680210113525 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 16 ".pt[92:107]" -type "float3"  0.69975889 -0.14294723 0 
		0.69975889 -0.14294723 0 0.69975889 -0.14294723 0 0.69975889 -0.14294723 0 0.69975889 
		-0.14294723 0 0.69975889 -0.14294723 0 0.69975889 -0.14294723 -8.5695772e-017 0.69975889 
		-0.14294723 0 0.69975889 -0.14294723 0 0.69975889 -0.14294723 0 0.69975889 -0.14294723 
		0 0.69975889 -0.14294723 0 0.69975889 -0.14294723 0 0.69975889 -0.14294723 -8.5695772e-017 
		0.69975889 -0.14294723 -8.5695772e-017 0.69975889 -0.14294723 -8.5695772e-017;
createNode transform -n "pCube3";
	rename -uid "293DF08E-4278-72E7-DD75-62AA403E1051";
	setAttr ".t" -type "double3" -6.5365172565105585 1.7882602153242098 -1.6525257499838282 ;
	setAttr ".s" -type "double3" 0.2544701306256108 5.9027083604400188 0.2544701306256108 ;
createNode transform -n "transform3" -p "pCube3";
	rename -uid "64085835-4AE8-C8FF-A99F-93BFC54CCFD9";
	setAttr ".v" no;
createNode mesh -n "pCubeShape2" -p "transform3";
	rename -uid "F0A8A9D4-4B8B-1E93-4550-AD9750017CFE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube4";
	rename -uid "ED8E9847-4F4F-91D9-DAAD-64830DD58E6D";
	setAttr ".t" -type "double3" -6.5365172565105585 1.7882602153242098 1.7035698907494496 ;
	setAttr ".s" -type "double3" 0.2544701306256108 5.9027083604400188 0.2544701306256108 ;
createNode transform -n "transform6" -p "pCube4";
	rename -uid "11A2E87F-40CC-14C4-3FDA-13B175502B28";
	setAttr ".v" no;
createNode mesh -n "pCubeShape4" -p "transform6";
	rename -uid "5DE564AE-42E2-09A8-8741-A48643BDE42D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube5";
	rename -uid "3A4CAEFE-459E-067A-2E61-9C897852544E";
	setAttr ".t" -type "double3" -9.7660503068886744 1.7882602153242098 1.7035698907494496 ;
	setAttr ".s" -type "double3" 0.2544701306256108 5.9027083604400188 0.2544701306256108 ;
createNode transform -n "transform7" -p "pCube5";
	rename -uid "DB74226D-4227-7DD9-F531-098B7FA07F3E";
	setAttr ".v" no;
createNode mesh -n "pCubeShape5" -p "transform7";
	rename -uid "B3035E86-4350-6338-08F0-0A976AA2D4D8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube6";
	rename -uid "D6ED8EE0-45B4-1D53-17BE-DAB55486EE28";
	setAttr ".t" -type "double3" -9.7660503068886744 1.7882602153242098 -1.7410636847985668 ;
	setAttr ".s" -type "double3" 0.2544701306256108 5.9027083604400188 0.2544701306256108 ;
createNode transform -n "transform4" -p "pCube6";
	rename -uid "4FC62930-495E-E985-3577-B39E622E2D2B";
	setAttr ".v" no;
createNode mesh -n "pCubeShape6" -p "transform4";
	rename -uid "284CDEA0-4EE3-1BEA-404B-51B0ACE701F7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube7";
	rename -uid "6C347B13-4BF8-59A1-5F05-4195D9D2CB75";
	setAttr ".t" -type "double3" -8.1088668721604975 4.0660989922070661 -0.0052822789962116978 ;
	setAttr ".s" -type "double3" 4.4649952789917773 0.2162563357125129 4.4649952789917773 ;
createNode mesh -n "polySurfaceShape1" -p "pCube7";
	rename -uid "7934095C-447F-EB56-887C-5F92E81E90DA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform5" -p "pCube7";
	rename -uid "71B9D81E-445C-8FBD-3928-DE9CD6C2D27D";
	setAttr ".v" no;
createNode mesh -n "pCubeShape7" -p "transform5";
	rename -uid "52ED7E69-4F98-6CE0-A38F-7FA5BC90521F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube8";
	rename -uid "E09348F2-4E3E-80C4-8B48-66AA545622F5";
	setAttr ".t" -type "double3" -8.1088668721604975 1.5401229972657178 -0.0052822789962116978 ;
	setAttr ".s" -type "double3" 4.4649952789917773 0.2162563357125129 4.4649952789917773 ;
createNode mesh -n "polySurfaceShape1" -p "pCube8";
	rename -uid "818A1DBE-4CE0-8A58-7E50-A7B169BA5332";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform8" -p "pCube8";
	rename -uid "353AEF8F-42BD-C221-9E7B-FC832A09CCE0";
	setAttr ".v" no;
createNode mesh -n "pCubeShape8" -p "transform8";
	rename -uid "667676D0-4BD4-7CAE-AFDB-548E2A9DBCAA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:31]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 48 ".uvst[0].uvsp[0:47]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.44722307 0.5 0.44722307 0.75 0.44722307 0 0.44722307
		 1 0.44722307 0.25 0.54231429 0.5 0.54231429 0.75 0.54231429 0 0.54231429 1 0.54231429
		 0.25 0.625 0.92706203 0.69793791 0 0.54231429 0.92706203 0.44722307 0.92706203 0.30206209
		 0 0.375 0.92706203 0.30206209 0.25 0.375 0.32293794 0.44722307 0.32293794 0.54231429
		 0.32293794 0.625 0.32293794 0.69793791 0.25 0.625 0.42248735 0.79748738 0.25 0.54231429
		 0.42248735 0.44722307 0.42248735 0.20251267 0.25 0.375 0.42248735 0.20251267 0 0.375
		 0.82751262 0.44722307 0.82751262 0.54231429 0.82751262 0.625 0.82751262 0.79748738
		 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 32 ".vt[0:31]"  -0.5 -0.49999905 0.50000024 0.5 -0.49999905 0.50000024
		 -0.5 0.49999905 0.50000024 0.5 0.49999905 0.50000024 -0.5 0.49999905 -0.5 0.5 0.49999905 -0.5
		 -0.5 -0.49999905 -0.5 0.5 -0.49999905 -0.5 -0.21110797 0.49999905 -0.5 -0.21110797 -0.49999905 -0.5
		 -0.21110797 -0.49999905 0.50000024 -0.21110797 0.49999905 0.50000024 0.16925716 0.49999905 -0.5
		 0.16925716 -0.49999905 -0.5 0.16925716 -0.49999905 0.50000024 0.16925716 0.49999905 0.50000024
		 0.5 -0.49999905 0.20824832 0.16925716 -0.49999905 0.20824832 -0.21110797 -0.49999905 0.20824832
		 -0.5 -0.49999905 0.20824832 -0.5 0.49999905 0.20824832 -0.21110797 0.49999905 0.20824832
		 0.16925716 0.49999905 0.20824832 0.5 0.49999905 0.20824832 0.5 0.49999905 -0.18994944
		 0.16925716 0.49999905 -0.18994944 -0.21110797 0.49999905 -0.18994944 -0.5 0.49999905 -0.18994944
		 -0.5 -0.49999905 -0.18994944 -0.21110797 -0.49999905 -0.18994944 0.16925716 -0.49999905 -0.18994944
		 0.5 -0.49999905 -0.18994944;
	setAttr -s 64 ".ed[0:63]"  0 10 0 2 11 0 4 8 0 6 9 0 0 2 0 1 3 0 2 20 0
		 3 23 0 4 6 0 5 7 0 6 28 0 7 31 0 8 12 0 9 13 0 8 9 1 10 14 0 9 29 1 11 15 0 10 11 1
		 11 21 1 12 5 0 13 7 0 12 13 1 14 1 0 13 30 1 15 3 0 14 15 1 15 22 1 16 1 0 17 14 1
		 16 17 1 18 10 1 17 18 0 19 0 0 18 19 1 20 27 0 19 20 1 21 26 0 20 21 1 22 25 0 21 22 0
		 23 24 0 22 23 1 23 16 1 24 5 0 25 12 1 24 25 1 26 8 1 25 26 0 27 4 0 26 27 1 28 19 0
		 27 28 1 29 18 0 28 29 1 30 17 0 29 30 0 31 16 0 30 31 1 31 24 1 18 21 0 17 22 0 25 30 0
		 26 29 0;
	setAttr -s 32 -ch 128 ".fc[0:31]" -type "polyFaces" 
		f 4 0 18 -2 -5
		mu 0 4 0 16 18 2
		f 4 38 37 50 -36
		mu 0 4 31 32 39 41
		f 4 2 14 -4 -9
		mu 0 4 4 14 15 6
		f 4 54 53 34 -52
		mu 0 4 43 44 27 29
		f 4 43 -58 59 -42
		mu 0 4 35 25 47 37
		f 4 51 36 35 52
		mu 0 4 42 28 30 40
		f 4 12 22 -14 -15
		mu 0 4 14 19 20 15
		f 4 -19 15 26 -18
		mu 0 4 18 16 21 23
		f 4 20 9 -22 -23
		mu 0 4 19 5 7 20
		f 4 -56 58 57 30
		mu 0 4 26 45 46 24
		f 4 -27 23 5 -26
		mu 0 4 23 21 1 3
		f 4 -40 42 41 46
		mu 0 4 38 33 34 36
		f 4 -30 -31 28 -24
		mu 0 4 22 26 24 9
		f 4 -32 -33 29 -16
		mu 0 4 17 27 26 22
		f 4 -35 31 -1 -34
		mu 0 4 29 27 17 8
		f 4 -37 33 4 6
		mu 0 4 30 28 0 2
		f 4 1 19 -39 -7
		mu 0 4 2 18 32 31
		f 4 -41 -20 17 27
		mu 0 4 33 32 18 23
		f 4 -43 -28 25 7
		mu 0 4 34 33 23 3
		f 4 -29 -44 -8 -6
		mu 0 4 1 25 35 3
		f 4 -46 -47 44 -21
		mu 0 4 19 38 36 5
		f 4 -48 -49 45 -13
		mu 0 4 14 39 38 19
		f 4 -51 47 -3 -50
		mu 0 4 41 39 14 4
		f 4 10 -53 49 8
		mu 0 4 12 42 40 13
		f 4 3 16 -55 -11
		mu 0 4 6 15 44 43
		f 4 -57 -17 13 24
		mu 0 4 45 44 15 20
		f 4 -59 -25 21 11
		mu 0 4 46 45 20 7
		f 4 -60 -12 -10 -45
		mu 0 4 37 47 10 11
		f 4 32 60 40 -62
		mu 0 4 26 27 32 33
		f 4 39 62 55 61
		mu 0 4 33 38 45 26
		f 4 48 63 56 -63
		mu 0 4 38 39 44 45
		f 4 -38 -61 -54 -64
		mu 0 4 39 32 27 44;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube9";
	rename -uid "BB3AEE78-451D-685C-FBAE-68A3E7D43362";
	setAttr ".r" -type "double3" 0 0 14.802708639278139 ;
	setAttr ".rp" -type "double3" -8.1088668721604975 1.7882602153242098 -0.0052817467272965324 ;
	setAttr ".sp" -type "double3" -8.1088668721604975 1.7882602153242098 -0.0052817467272965324 ;
createNode transform -n "polySurface1" -p "pCube9";
	rename -uid "C3FAC29A-4BA0-C3FA-0E69-75A89C324E58";
createNode mesh -n "polySurfaceShape2" -p "polySurface1";
	rename -uid "D2F1772A-4525-0684-3EA0-DF8D9F11BA9A";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface2" -p "pCube9";
	rename -uid "0DC6B66B-4245-FFA2-C6B8-2B97D8AB8F5A";
createNode mesh -n "polySurfaceShape3" -p "polySurface2";
	rename -uid "DA399C32-4EF2-B224-191F-559FB229300A";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface3" -p "pCube9";
	rename -uid "EEC8F313-45EC-36DA-B281-F69B2867B21C";
createNode mesh -n "polySurfaceShape4" -p "polySurface3";
	rename -uid "94B7516F-433A-4915-8A5E-16AE09433DC2";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface4" -p "pCube9";
	rename -uid "87DBEA34-4A17-C6AE-0B24-8F890AFEDDB0";
createNode mesh -n "polySurfaceShape5" -p "polySurface4";
	rename -uid "CF64758C-498C-A74B-BA50-ECB415205F38";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface5" -p "pCube9";
	rename -uid "21745E98-4B85-DA90-2E66-73A694FB2549";
createNode mesh -n "polySurfaceShape6" -p "polySurface5";
	rename -uid "24AD9EEF-4E43-4EB6-F135-318E1BE810EB";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface6" -p "pCube9";
	rename -uid "D1FEA405-4FBC-6A25-465B-4089B24F5201";
createNode mesh -n "polySurfaceShape7" -p "polySurface6";
	rename -uid "2792F1CF-4826-A393-5CA8-419D443DA4E0";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "transform9" -p "pCube9";
	rename -uid "03482B49-4D46-FF8B-B4FF-90AA018A3F3B";
	setAttr ".v" no;
createNode mesh -n "pCube9Shape" -p "transform9";
	rename -uid "6A64EDA1-4F81-5291-4FFB-28AA5D1BE037";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "polySurface7" -p "pCube9";
	rename -uid "FD158B7D-4494-1F81-C35B-428B2CF7AC74";
	setAttr ".t" -type "double3" -4.9726381240306541 -2.2381841418986936 1.3934810571245677 ;
	setAttr ".r" -type "double3" 37.678552877278932 -31.798519742223494 -10.998190238549 ;
	setAttr ".rp" -type "double3" -8.1088664531707764 4.0660991668701172 -0.0052816867828369141 ;
	setAttr ".sp" -type "double3" -8.1088664531707764 4.0660991668701172 -0.0052816867828369141 ;
createNode mesh -n "polySurfaceShape7" -p "polySurface7";
	rename -uid "805F791B-4989-EC87-D590-45B3FB9835F1";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 32 "f[0]" "f[1]" "f[2]" "f[3]" "f[4]" "f[5]" "f[6]" "f[7]" "f[8]" "f[9]" "f[10]" "f[11]" "f[12]" "f[13]" "f[14]" "f[15]" "f[16]" "f[17]" "f[18]" "f[19]" "f[20]" "f[21]" "f[22]" "f[23]" "f[24]" "f[25]" "f[26]" "f[27]" "f[28]" "f[29]" "f[30]" "f[31]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 48 ".uvst[0].uvsp[0:47]" -type "float2" 0.375 0 0.44722307
		 0 0.44722307 0.25 0.375 0.25 0.375 0.32293794 0.44722307 0.32293794 0.44722307 0.42248735
		 0.375 0.42248735 0.375 0.5 0.44722307 0.5 0.44722307 0.75 0.375 0.75 0.375 0.82751262
		 0.44722307 0.82751262 0.44722307 0.92706203 0.375 0.92706203 0.69793791 0.25 0.69793791
		 0 0.79748738 0 0.79748738 0.25 0.20251267 0 0.30206209 0 0.30206209 0.25 0.20251267
		 0.25 0.54231429 0.5 0.54231429 0.75 0.54231429 0 0.54231429 0.25 0.625 0.5 0.625
		 0.75 0.54231429 0.92706203 0.54231429 0.82751262 0.625 0.82751262 0.625 0.92706203
		 0.625 0 0.625 0.25 0.54231429 0.42248735 0.54231429 0.32293794 0.625 0.32293794 0.625
		 0.42248735 0.54231429 1 0.625 1 0.44722307 1 0.375 1 0.125 0 0.125 0.25 0.875 0 0.875
		 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 32 ".vt[0:31]"  -10.34136391 3.9579711 2.22721648 -5.876369 3.9579711 2.22721648
		 -10.34136391 4.17422724 2.22721648 -5.876369 4.17422724 2.22721648 -10.34136391 4.17422724 -2.23777986
		 -5.876369 4.17422724 -2.23777986 -10.34136391 3.9579711 -2.23777986 -5.876369 3.9579711 -2.23777986
		 -9.051463127 4.17422724 -2.23777986 -9.051463127 3.9579711 -2.23777986 -9.051463127 3.9579711 2.22721648
		 -9.051463127 4.17422724 2.22721648 -7.35313416 4.17422724 -2.23777986 -7.35313416 3.9579711 -2.23777986
		 -7.35313416 3.9579711 2.22721648 -7.35313416 4.17422724 2.22721648 -5.876369 3.9579711 0.92454547
		 -7.35313416 3.9579711 0.92454547 -9.051463127 3.9579711 0.92454547 -10.34136391 3.9579711 0.92454547
		 -10.34136391 4.17422724 0.92454547 -9.051463127 4.17422724 0.92454547 -7.35313416 4.17422724 0.92454547
		 -5.876369 4.17422724 0.92454547 -5.876369 4.17422724 -0.85340565 -7.35313416 4.17422724 -0.85340565
		 -9.051463127 4.17422724 -0.85340565 -10.34136391 4.17422724 -0.85340565 -10.34136391 3.9579711 -0.85340565
		 -9.051463127 3.9579711 -0.85340565 -7.35313416 3.9579711 -0.85340565 -5.876369 3.9579711 -0.85340565;
	setAttr -s 64 ".ed[0:63]"  0 10 0 2 11 0 4 8 0 6 9 0 0 2 0 1 3 0 2 20 0
		 3 23 0 4 6 0 5 7 0 6 28 0 7 31 0 8 12 0 9 13 0 8 9 1 10 14 0 9 29 1 11 15 0 10 11 1
		 11 21 1 12 5 0 13 7 0 12 13 1 14 1 0 13 30 1 15 3 0 14 15 1 15 22 1 16 1 0 17 14 1
		 16 17 1 18 10 1 17 18 0 19 0 0 18 19 1 20 27 0 19 20 1 21 26 0 20 21 1 22 25 0 21 22 0
		 23 24 0 22 23 1 23 16 1 24 5 0 25 12 1 24 25 1 26 8 1 25 26 0 27 4 0 26 27 1 28 19 0
		 27 28 1 29 18 0 28 29 1 30 17 0 29 30 0 31 16 0 30 31 1 31 24 1 18 21 0 17 22 0 25 30 0
		 26 29 0;
	setAttr -s 32 -ch 128 ".fc[0:31]" -type "polyFaces" 
		f 4 0 18 -2 -5
		mu 0 4 0 1 2 3
		f 4 38 37 50 -36
		mu 0 4 4 5 6 7
		f 4 2 14 -4 -9
		mu 0 4 8 9 10 11
		f 4 54 53 34 -52
		mu 0 4 12 13 14 15
		f 4 43 -58 59 -42
		mu 0 4 16 17 18 19
		f 4 51 36 35 52
		mu 0 4 20 21 22 23
		f 4 12 22 -14 -15
		mu 0 4 9 24 25 10
		f 4 -19 15 26 -18
		mu 0 4 2 1 26 27
		f 4 20 9 -22 -23
		mu 0 4 24 28 29 25
		f 4 -56 58 57 30
		mu 0 4 30 31 32 33
		f 4 -27 23 5 -26
		mu 0 4 27 26 34 35
		f 4 -40 42 41 46
		mu 0 4 36 37 38 39
		f 4 -30 -31 28 -24
		mu 0 4 40 30 33 41
		f 4 -32 -33 29 -16
		mu 0 4 42 14 30 40
		f 4 -35 31 -1 -34
		mu 0 4 15 14 42 43
		f 4 -37 33 4 6
		mu 0 4 22 21 0 3
		f 4 1 19 -39 -7
		mu 0 4 3 2 5 4
		f 4 -41 -20 17 27
		mu 0 4 37 5 2 27
		f 4 -43 -28 25 7
		mu 0 4 38 37 27 35
		f 4 -29 -44 -8 -6
		mu 0 4 34 17 16 35
		f 4 -46 -47 44 -21
		mu 0 4 24 36 39 28
		f 4 -48 -49 45 -13
		mu 0 4 9 6 36 24
		f 4 -51 47 -3 -50
		mu 0 4 7 6 9 8
		f 4 10 -53 49 8
		mu 0 4 44 20 23 45
		f 4 3 16 -55 -11
		mu 0 4 11 10 13 12
		f 4 -57 -17 13 24
		mu 0 4 31 13 10 25
		f 4 -59 -25 21 11
		mu 0 4 32 31 25 29
		f 4 -60 -12 -10 -45
		mu 0 4 19 18 46 47
		f 4 32 60 40 -62
		mu 0 4 30 14 5 37
		f 4 39 62 55 61
		mu 0 4 37 36 31 30
		f 4 48 63 56 -63
		mu 0 4 36 6 13 31
		f 4 -38 -61 -54 -64
		mu 0 4 6 5 14 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube10";
	rename -uid "59D89A19-416D-760A-3294-DEA743F468F1";
	setAttr ".t" -type "double3" -14.324288867996453 0 -1.1843827689078246 ;
	setAttr ".r" -type "double3" 9.0204253560974177 -3.9756933518293945e-016 -26.698511200479164 ;
	setAttr ".s" -type "double3" 0.18467053375259954 3.8754458449263756 0.18467053375259954 ;
createNode mesh -n "pCubeShape9" -p "pCube10";
	rename -uid "5D4999FF-4ABB-8533-4379-E5BCD8BC92A0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube11";
	rename -uid "AF1A31EE-4370-0103-8462-AE97AB210EA0";
	setAttr ".t" -type "double3" -11.669182809471039 -0.21696884542553851 1.5580019027558321 ;
	setAttr ".r" -type "double3" -17.793420282603385 41.890743311416145 -37.12471171755486 ;
	setAttr ".s" -type "double3" 0.18467053375259954 3.8754458449263756 0.18467053375259954 ;
createNode mesh -n "pCubeShape11" -p "pCube11";
	rename -uid "1C8B7D5E-43E8-893F-6E45-A6B76EA1D526";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "BEAD3EA4-4A8B-5092-B306-64BDB3AB591A";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "BC43FF8A-41D3-E4C6-FA79-6D802C2DEF83";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "F16A9A69-48E9-BA7D-F587-F9A5DF8B006A";
createNode displayLayerManager -n "layerManager";
	rename -uid "6734B3AB-43FF-3594-5086-CA9E7013AD84";
createNode displayLayer -n "defaultLayer";
	rename -uid "F07D10EA-408C-255B-7002-5DB7A3DC1F95";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "72494461-4FB5-9DB6-83DE-C1A8B4CC694F";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "8210B0FC-4DC2-B1E9-8FE3-23AE6D5DBA9D";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "917A6DB4-4F0D-4013-92F4-D3A3D4B772B8";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 665\n            -height 347\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 664\n            -height 317\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 665\n            -height 317\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1336\n            -height 708\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n"
		+ "            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n"
		+ "            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n"
		+ "            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n"
		+ "            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n"
		+ "                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n"
		+ "                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1336\\n    -height 708\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1336\\n    -height 708\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "052F647A-40D3-94F5-84EA-B88F8AA01C82";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCube -n "polyCube1";
	rename -uid "B429559A-43E2-9284-E3A6-0298EB3821F9";
	setAttr ".cuv" 4;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "3B0D6527-464D-3C09-28D8-95A30B675CC4";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 2.2543915282992639 0 0 0 0 1 0 0 0 0 2.2543915282992639 0
		 -3.0947909202365347 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -3.0947909 0.5 0 ;
	setAttr ".rs" 37865;
	setAttr ".lt" -type "double3" -4.4408920985006262e-016 6.1629758220391547e-032 0.28771625947687762 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -4.2219866843861666 0.5 -1.127195764149632 ;
	setAttr ".cbx" -type "double3" -1.9675951560869027 0.5 1.127195764149632 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "15FEBA26-4B0D-15AE-0BDD-AA84FAACFAC7";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 2.2543915282992639 0 0 0 0 1 0 0 0 0 2.2543915282992639 0
		 -3.0947909202365347 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -3.0947907 0.78771627 0 ;
	setAttr ".rs" 52663;
	setAttr ".lt" -type "double3" 4.4408920985006262e-016 -3.0814879110195774e-032 0.24412565945283404 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -4.0220308307881583 0.78771626949310303 -0.92724011210993296 ;
	setAttr ".cbx" -type "double3" -2.1675506065682923 0.78771626949310303 0.92724011210993296 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "B238C1B2-4463-95F6-511B-91B32BA6E451";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[8:11]" -type "float3"  0.08869604 0 -0.08869604 -0.08869604
		 0 -0.08869604 -0.08869604 0 0.08869604 0.08869604 0 0.08869604;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "898454A0-4092-4B54-863C-639D7DE3C664";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 2.2543915282992639 0 0 0 0 1 0 0 0 0 2.2543915282992639 0
		 -3.0947909202365347 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -3.1587317 1.031842 -0.0083045382 ;
	setAttr ".rs" 44714;
	setAttr ".lt" -type "double3" 0 -4.1459894609539003e-019 0.2012578123408566 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.9212134472237903 1.0318419933319092 -0.77078625248699328 ;
	setAttr ".cbx" -type "double3" -2.3962498844285243 1.0318419933319092 0.75417717593606659 ;
createNode polyTweak -n "polyTweak2";
	rename -uid "03A5F0F3-40D2-6B33-E148-DDACD2EBC083";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[12:15]" -type "float3"  0.044720348 1.7763568e-015
		 -0.076767005 -0.10144622 1.7763568e-015 -0.076767005 -0.10144622 1.7763568e-015 0.069399573
		 0.044720348 1.7763568e-015 0.069399573;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "7FF80A66-4B3C-A27B-D7B9-ED8719EC9CA5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[4:5]" "e[8:9]";
	setAttr ".ix" -type "matrix" 2.2543915282992639 0 0 0 0 1 0 0 0 0 2.2543915282992639 0
		 -3.0947909202365347 0 0 1;
	setAttr ".wt" 0.63390916585922241;
	setAttr ".dr" no;
	setAttr ".re" 5;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak3";
	rename -uid "B039399F-403E-75F4-37CB-F799724E29E6";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[16:19]" -type "float3"  0.12410032 0 -0.12410032 -0.12410032
		 0 -0.12410032 -0.12410032 0 0.12410032 0.12410032 0 0.12410032;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "A56F5BFF-44AE-DA3A-A315-4F879150FA6D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[4:5]" "e[39]" "e[41]";
	setAttr ".ix" -type "matrix" 2.2543915282992639 0 0 0 0 1 0 0 0 0 2.2543915282992639 0
		 -3.0947909202365347 0 0 1;
	setAttr ".wt" 0.52027946710586548;
	setAttr ".dr" no;
	setAttr ".re" 4;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "A6CF4EE6-4E55-9B06-BD12-EA9976080AEA";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[20:23]" -type "float3"  0.045922462 0 0.045922462
		 -0.045922462 0 0.045922462 -0.045922462 0 -0.045922462 0.045922462 0 -0.045922462;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "F210DB39-443C-0E25-7FB5-DF93C8B98B8D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 11 "e[0:3]" "e[14]" "e[18]" "e[22]" "e[26]" "e[30]" "e[34]" "e[38]" "e[42]" "e[48]" "e[51]";
	setAttr ".ix" -type "matrix" 2.2543915282992639 0 0 0 0 1 0 0 0 0 2.2543915282992639 0
		 -3.0947909202365347 0 0 1;
	setAttr ".wt" 0.28989651799201965;
	setAttr ".re" 30;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak5";
	rename -uid "DCEEDA54-486B-1C9F-5F5F-479BD314D6FA";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk";
	setAttr ".tk[0]" -type "float3" 0.11842668 0 -0.11842668 ;
	setAttr ".tk[1]" -type "float3" -0.11842668 0 -0.11842668 ;
	setAttr ".tk[6]" -type "float3" 0.11842668 0 0.11842668 ;
	setAttr ".tk[7]" -type "float3" -0.11842668 0 0.11842668 ;
	setAttr ".tk[24]" -type "float3" 0.022106074 0 -0.022106078 ;
	setAttr ".tk[25]" -type "float3" 0.022106074 0 0.022106078 ;
	setAttr ".tk[26]" -type "float3" -0.022106074 0 0.022106078 ;
	setAttr ".tk[27]" -type "float3" -0.022106074 0 -0.022106078 ;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "A8FDBC60-4BFD-5B51-4A85-1C907CF6AD8B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 12 "e[38]" "e[51:53]" "e[55]" "e[57]" "e[59]" "e[61]" "e[63]" "e[65]" "e[67]" "e[73]" "e[75]" "e[77]";
	setAttr ".ix" -type "matrix" 2.2543915282992639 0 0 0 0 1 0 0 0 0 2.2543915282992639 0
		 -3.0947909202365347 0 0 1;
	setAttr ".wt" 0.56880366802215576;
	setAttr ".dr" no;
	setAttr ".re" 52;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak6";
	rename -uid "827A5DCC-41F1-5EDD-6A65-C28C0E1CF391";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk[28:41]" -type "float3"  0 0 0.02136508 0 0 -0.022113077
		 0 0 -0.034712661 0 0 -0.041758627 0 0 -0.050763693 0 0 -0.055426095 0 0 -0.050945077
		 0 0 -0.038740143 0 0 0.038740143 0 0 0.050945077 0 0 0.055426095 0 0 0.050763693
		 0 0 0.041758627 0 0 0.033964667;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "7F669956-4B59-4C83-A60A-15BE74E182DA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 16 "e[6:7]" "e[10:11]" "e[16]" "e[19]" "e[24]" "e[27]" "e[32]" "e[35]" "e[40]" "e[43]" "e[46]" "e[50]" "e[54]" "e[68]" "e[82]" "e[96]";
	setAttr ".ix" -type "matrix" 2.2543915282992639 0 0 0 0 1 0 0 0 0 2.2543915282992639 0
		 -3.0947909202365347 0 0 1;
	setAttr ".wt" 0.73640888929367065;
	setAttr ".dr" no;
	setAttr ".re" 32;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak7";
	rename -uid "D5AAFBC7-4E71-CECA-7FEF-CFAF3D6F8F50";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk[42:55]" -type "float3"  0 0 0.037111215 0 0 -0.038410477
		 0 0 -0.060295995 0 0 -0.072534859 0 0 -0.08817669 0 0 -0.096275277 0 0 -0.08849176
		 0 0 -0.067291759 0 0 0.067291759 0 0 0.08849176 0 0 0.096275277 0 0 0.08817669 0
		 0 0.072534859 0 0 0.05899673;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "D6B91706-4FBA-AE31-6078-6DBEA808FFDE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 17 "e[6:7]" "e[16]" "e[19]" "e[24]" "e[27]" "e[32]" "e[35]" "e[40]" "e[46]" "e[54]" "e[82]" "e[125]" "e[127]" "e[129]" "e[131]" "e[133]" "e[135]";
	setAttr ".ix" -type "matrix" 2.2543915282992639 0 0 0 0 1 0 0 0 0 2.2543915282992639 0
		 -3.0947909202365347 0 0 1;
	setAttr ".wt" 0.38498038053512573;
	setAttr ".re" 32;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "580FF94E-4C55-594D-9D91-CDB89A409EA2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 2.2543915282992639 0 0 0 0 1 0 0 0 0 2.2543915282992639 0
		 -3.0947909202365347 0 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak8";
	rename -uid "45D4C4C1-44D6-5A12-E892-04B179BEB4EA";
	setAttr ".uopa" yes;
	setAttr -s 36 ".tk[56:91]" -type "float3"  0.062654205 0 0 0.018427072
		 0.07414598 0 -0.039914217 0.07414598 0 -0.081787325 0 0 -0.12364521 0 0 -0.13872899
		 0 0 -0.16864528 0 0 -0.1841345 0 0 -0.16924778 0 0 -0.12870108 0 0 -0.054081075 0
		 0 0.049886048 0 0 0.12870108 0 0 0.16924778 0 0 0.1841345 0 0 0.16864528 0 0 0.1387289
		 0 0 0.1045121 0 0 0.036904842 0 0 0.010853989 0.07414598 0 -0.023510443 0.07414598
		 0 -0.048174705 0 0 -0.072830014 0 0 -0.081714734 0 0 -0.09933614 0 0 -0.10845967
		 0 0 -0.099691041 0 0 -0.075808033 0 0 -0.031855062 0 0 0.029384101 0 0 0.075808033
		 0 0 0.099691041 0 0 0.10845967 0 0 0.09933614 0 0 0.081714697 0 0 0.061560132 0 0;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "132E5447-4FA5-296B-58B4-ACA1154252EA";
	setAttr ".sa" 7;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "7DF17A16-4491-2DFB-A784-F8AD5E99E73B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[14:20]";
	setAttr ".ix" -type "matrix" 0.082654482542824259 0.4136480745669634 0 0 -4.1508562748898408 0.82941732019379744 0 0
		 0 0 0.44882996496509325 0 0 1.1271374902885161 0 1;
	setAttr ".a" 180;
createNode polyUnite -n "polyUnite1";
	rename -uid "B2C06433-4DEC-D3BE-3ED1-C9B72FD86232";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId1";
	rename -uid "B27BCCAA-4A1B-FF75-20D1-77BD09FC6CAF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "990B32E6-4B68-8DCC-241B-71A46DD6903A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:89]";
createNode groupId -n "groupId2";
	rename -uid "F1FA6B36-4702-1A86-4C8F-99A61FB29F8B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "3000184C-47E4-C7FF-3216-F3B9D96CF67E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "3CEBE8A9-440E-65D7-3F32-4887E973DB90";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:20]";
createNode groupId -n "groupId4";
	rename -uid "13ECCADD-441B-4F2A-1241-789B13E25FE5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "ED767D9B-4B26-D98C-533B-F98B8928FDE1";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "A3135804-49B6-D95F-C6BB-4CB5B738680D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:110]";
createNode polyCube -n "polyCube2";
	rename -uid "317EA4E3-4D26-AC38-0F4D-09BBA514FFAB";
	setAttr ".cuv" 4;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "750E2550-4803-856C-8344-65A2841AF45F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:3]";
	setAttr ".ix" -type "matrix" 4.4649952789917773 0 0 0 0 0.25478388206260649 0 0 0 0 4.4649952789917773 0
		 -8.1088668721604975 4.0660989922070661 -0.0052822789962116978 1;
	setAttr ".wt" 0.28889214992523193;
	setAttr ".re" 2;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing8";
	rename -uid "346969BB-4850-DB43-E7B6-4FAD815849B1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[12:13]" "e[15]" "e[17]";
	setAttr ".ix" -type "matrix" 4.4649952789917773 0 0 0 0 0.25478388206260649 0 0 0 0 4.4649952789917773 0
		 -8.1088668721604975 4.0660989922070661 -0.0052822789962116978 1;
	setAttr ".wt" 0.53489077091217041;
	setAttr ".re" 12;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing9";
	rename -uid "5EEC616E-4CFD-BC32-DA44-9B957107A6DC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[6:7]" "e[10:11]" "e[16]" "e[19]" "e[24]" "e[27]";
	setAttr ".ix" -type "matrix" 4.4649952789917773 0 0 0 0 0.25478388206260649 0 0 0 0 4.4649952789917773 0
		 -8.1088668721604975 4.0660989922070661 -0.0052822789962116978 1;
	setAttr ".wt" 0.70824825763702393;
	setAttr ".dr" no;
	setAttr ".re" 11;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing10";
	rename -uid "06766C55-45D0-22D6-ECD1-D38B484B80FB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[10:11]" "e[16]" "e[24]" "e[35]" "e[37]" "e[39]" "e[41]";
	setAttr ".ix" -type "matrix" 4.4649952789917773 0 0 0 0 0.25478388206260649 0 0 0 0 4.4649952789917773 0
		 -8.1088668721604975 4.0660989922070661 -0.0052822789962116978 1;
	setAttr ".wt" 0.56222891807556152;
	setAttr ".re" 41;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "AB7B4B81-46AF-58EC-2C8D-A1A1C131B92B";
	setAttr ".dc" -type "componentList" 2 "f[7]" "f[9]";
createNode polyBridgeEdge -n "polyBridgeEdge1";
	rename -uid "1C4D99DD-42DD-7CFE-E44E-9DBBE7671BED";
	setAttr ".ics" -type "componentList" 2 "e[32]" "e[40]";
	setAttr ".ix" -type "matrix" 4.4649952789917773 0 0 0 0 0.25478388206260649 0 0 0 0 4.4649952789917773 0
		 -8.1088668721604975 4.0660989922070661 -0.0052822789962116978 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 17;
	setAttr ".sv2" 21;
	setAttr ".d" 1;
createNode polyBridgeEdge -n "polyBridgeEdge2";
	rename -uid "DF122445-4BBE-A507-3975-0B8BE408C9F3";
	setAttr ".ics" -type "componentList" 2 "e[39]" "e[55]";
	setAttr ".ix" -type "matrix" 4.4649952789917773 0 0 0 0 0.25478388206260649 0 0 0 0 4.4649952789917773 0
		 -8.1088668721604975 4.0660989922070661 -0.0052822789962116978 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 22;
	setAttr ".sv2" 30;
	setAttr ".d" 1;
createNode polyBridgeEdge -n "polyBridgeEdge3";
	rename -uid "49607DFE-44D0-D7CF-3ED9-1E86D227272B";
	setAttr ".ics" -type "componentList" 2 "e[48]" "e[56]";
	setAttr ".ix" -type "matrix" 4.4649952789917773 0 0 0 0 0.25478388206260649 0 0 0 0 4.4649952789917773 0
		 -8.1088668721604975 4.0660989922070661 -0.0052822789962116978 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 25;
	setAttr ".sv2" 29;
	setAttr ".d" 1;
createNode polyBridgeEdge -n "polyBridgeEdge4";
	rename -uid "97AA54EF-4E40-DB6C-6273-6284C66B36CF";
	setAttr ".ics" -type "componentList" 2 "e[37]" "e[53]";
	setAttr ".ix" -type "matrix" 4.4649952789917773 0 0 0 0 0.25478388206260649 0 0 0 0 4.4649952789917773 0
		 -8.1088668721604975 4.0660989922070661 -0.0052822789962116978 1;
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 0;
	setAttr ".sv1" 26;
	setAttr ".sv2" 18;
	setAttr ".d" 1;
createNode polyUnite -n "polyUnite2";
	rename -uid "A42E01E9-494D-8355-1908-09A5722594C9";
	setAttr -s 6 ".ip";
	setAttr -s 6 ".im";
createNode groupId -n "groupId6";
	rename -uid "B91CA755-49C5-ABC1-F328-33BDC4B9E993";
	setAttr ".ihi" 0;
createNode groupId -n "groupId7";
	rename -uid "0FBB9949-4C8B-A17E-2243-BAB709BF598D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId8";
	rename -uid "972D5564-488B-4E60-0F4A-C7AE37BD3C71";
	setAttr ".ihi" 0;
createNode groupId -n "groupId9";
	rename -uid "9496B65D-441E-A553-02F7-DC87FA1AB0BA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId10";
	rename -uid "1250CC48-47EB-6C91-2A9D-F0AAE9BD6D8A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId11";
	rename -uid "E71270EA-4DE2-8CA0-1265-DF9AA84C4B71";
	setAttr ".ihi" 0;
createNode groupId -n "groupId12";
	rename -uid "A0D757BA-42FC-A0F0-6BC7-D1A27F426EFA";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "56753DBE-4E6C-7249-DD5F-28B78E337751";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:31]";
createNode groupId -n "groupId13";
	rename -uid "2AAA0C59-44B5-4BA7-2959-B6ABFFF76AEA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId14";
	rename -uid "B6A8850C-42E9-ACDC-4FDB-F3B5FDD315DC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId15";
	rename -uid "CF5F59CB-4F04-32A5-BBD7-94AC03D7AF00";
	setAttr ".ihi" 0;
createNode groupId -n "groupId16";
	rename -uid "991F7B6D-4F13-7D5B-FBB2-94B69AB6295A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "5988FB3B-47E4-D794-BB86-2C96D255EF70";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId17";
	rename -uid "2E4404B1-4E5F-1911-47D1-6BA31245BF19";
	setAttr ".ihi" 0;
createNode groupId -n "groupId18";
	rename -uid "49EF374F-4CB4-33ED-18C9-FD960DF6571E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "D190BE3F-4558-766A-0B71-15930970C843";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:87]";
createNode polyCube -n "polyCube3";
	rename -uid "CCD7621E-470E-5815-1F02-2789C1F1C6FF";
	setAttr ".cuv" 4;
createNode polySeparate -n "polySeparate1";
	rename -uid "41758392-4690-24AE-C24F-30A285635072";
	setAttr ".ic" 6;
	setAttr -s 6 ".out";
createNode groupId -n "groupId19";
	rename -uid "6BFA0020-43D6-31A9-9DA1-B48435B8C0B0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts7";
	rename -uid "22621D4F-468A-C939-629F-F1A4CBECA618";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 32 "f[0]" "f[1]" "f[2]" "f[3]" "f[4]" "f[5]" "f[6]" "f[7]" "f[8]" "f[9]" "f[10]" "f[11]" "f[12]" "f[13]" "f[14]" "f[15]" "f[16]" "f[17]" "f[18]" "f[19]" "f[20]" "f[21]" "f[22]" "f[23]" "f[24]" "f[25]" "f[26]" "f[27]" "f[28]" "f[29]" "f[30]" "f[31]";
createNode groupId -n "groupId20";
	rename -uid "09CAEF21-44DA-7CEC-427E-52A08D2B3DF0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts8";
	rename -uid "9CBE687B-4B4D-5BA4-EE7A-E1BD6E29E524";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 6 "f[0]" "f[1]" "f[2]" "f[3]" "f[4]" "f[5]";
createNode groupId -n "groupId21";
	rename -uid "48CEB186-48E1-5D31-D6D9-26B3DC0A104A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts9";
	rename -uid "CF6C24E7-4A21-56A7-B84D-42B2A5C7334F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 6 "f[0]" "f[1]" "f[2]" "f[3]" "f[4]" "f[5]";
createNode groupId -n "groupId22";
	rename -uid "C856EC02-4046-D81E-672D-43B71102EE44";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts10";
	rename -uid "CB21CE7C-4667-C252-2595-3B8EA9FF1CAC";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 32 "f[0]" "f[1]" "f[2]" "f[3]" "f[4]" "f[5]" "f[6]" "f[7]" "f[8]" "f[9]" "f[10]" "f[11]" "f[12]" "f[13]" "f[14]" "f[15]" "f[16]" "f[17]" "f[18]" "f[19]" "f[20]" "f[21]" "f[22]" "f[23]" "f[24]" "f[25]" "f[26]" "f[27]" "f[28]" "f[29]" "f[30]" "f[31]";
createNode groupId -n "groupId23";
	rename -uid "957B2D69-4867-F3F1-0B3B-56B5E3ADEE12";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts11";
	rename -uid "4AAAD268-47A6-163A-A4E0-4C92DBEC050D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 6 "f[0]" "f[1]" "f[2]" "f[3]" "f[4]" "f[5]";
createNode groupId -n "groupId24";
	rename -uid "11AFE383-4D64-94DF-44C3-2B94A3BCFC26";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts12";
	rename -uid "1BE86E55-4494-C891-C308-FE807C509639";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 6 "f[0]" "f[1]" "f[2]" "f[3]" "f[4]" "f[5]";
createNode groupId -n "groupId25";
	rename -uid "5E589591-42A4-EEF7-E7B0-3BA33971B09C";
	setAttr ".ihi" 0;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 27 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 25 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "groupId1.id" "pCubeShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape1.iog.og[0].gco";
connectAttr "groupParts1.og" "pCubeShape1.i";
connectAttr "groupId2.id" "pCubeShape1.ciog.cog[0].cgid";
connectAttr "groupId3.id" "pCylinderShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCylinderShape1.iog.og[0].gco";
connectAttr "groupParts2.og" "pCylinderShape1.i";
connectAttr "groupId4.id" "pCylinderShape1.ciog.cog[0].cgid";
connectAttr "groupParts3.og" "pCube2Shape.i";
connectAttr "groupId5.id" "pCube2Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCube2Shape.iog.og[0].gco";
connectAttr "groupId16.id" "pCubeShape2.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape2.iog.og[0].gco";
connectAttr "groupParts5.og" "pCubeShape2.i";
connectAttr "groupId17.id" "pCubeShape2.ciog.cog[0].cgid";
connectAttr "groupId10.id" "pCubeShape4.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape4.iog.og[0].gco";
connectAttr "groupId11.id" "pCubeShape4.ciog.cog[0].cgid";
connectAttr "groupId8.id" "pCubeShape5.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape5.iog.og[0].gco";
connectAttr "groupId9.id" "pCubeShape5.ciog.cog[0].cgid";
connectAttr "groupId14.id" "pCubeShape6.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape6.iog.og[0].gco";
connectAttr "groupId15.id" "pCubeShape6.ciog.cog[0].cgid";
connectAttr "groupId12.id" "pCubeShape7.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape7.iog.og[0].gco";
connectAttr "groupParts4.og" "pCubeShape7.i";
connectAttr "groupId13.id" "pCubeShape7.ciog.cog[0].cgid";
connectAttr "groupId6.id" "pCubeShape8.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape8.iog.og[0].gco";
connectAttr "groupId7.id" "pCubeShape8.ciog.cog[0].cgid";
connectAttr "groupParts7.og" "polySurfaceShape2.i";
connectAttr "groupId19.id" "polySurfaceShape2.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape2.iog.og[0].gco";
connectAttr "groupParts8.og" "polySurfaceShape3.i";
connectAttr "groupId20.id" "polySurfaceShape3.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape3.iog.og[0].gco";
connectAttr "groupParts9.og" "polySurfaceShape4.i";
connectAttr "groupId21.id" "polySurfaceShape4.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape4.iog.og[0].gco";
connectAttr "groupParts10.og" "polySurfaceShape5.i";
connectAttr "groupId22.id" "polySurfaceShape5.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape5.iog.og[0].gco";
connectAttr "groupParts11.og" "polySurfaceShape6.i";
connectAttr "groupId23.id" "polySurfaceShape6.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "polySurfaceShape6.iog.og[0].gco";
connectAttr "groupParts12.og" "|pCube9|polySurface6|polySurfaceShape7.i";
connectAttr "groupId24.id" "|pCube9|polySurface6|polySurfaceShape7.iog.og[0].gid"
		;
connectAttr ":initialShadingGroup.mwc" "|pCube9|polySurface6|polySurfaceShape7.iog.og[0].gco"
		;
connectAttr "groupParts6.og" "pCube9Shape.i";
connectAttr "groupId18.id" "pCube9Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCube9Shape.iog.og[0].gco";
connectAttr "groupId25.id" "|pCube9|polySurface7|polySurfaceShape7.iog.og[0].gid"
		;
connectAttr ":initialShadingGroup.mwc" "|pCube9|polySurface7|polySurfaceShape7.iog.og[0].gco"
		;
connectAttr "polyCube3.out" "pCubeShape9.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube1.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyTweak1.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak1.ip";
connectAttr "polyTweak2.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polyExtrudeFace2.out" "polyTweak2.ip";
connectAttr "polyTweak3.out" "polySplitRing1.ip";
connectAttr "pCubeShape1.wm" "polySplitRing1.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polySplitRing2.ip";
connectAttr "pCubeShape1.wm" "polySplitRing2.mp";
connectAttr "polySplitRing1.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polySplitRing3.ip";
connectAttr "pCubeShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing2.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polySplitRing4.ip";
connectAttr "pCubeShape1.wm" "polySplitRing4.mp";
connectAttr "polySplitRing3.out" "polyTweak6.ip";
connectAttr "polyTweak7.out" "polySplitRing5.ip";
connectAttr "pCubeShape1.wm" "polySplitRing5.mp";
connectAttr "polySplitRing4.out" "polyTweak7.ip";
connectAttr "polySplitRing5.out" "polySplitRing6.ip";
connectAttr "pCubeShape1.wm" "polySplitRing6.mp";
connectAttr "polyTweak8.out" "polySoftEdge1.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge1.mp";
connectAttr "polySplitRing6.out" "polyTweak8.ip";
connectAttr "polyCylinder1.out" "polySoftEdge2.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge2.mp";
connectAttr "pCubeShape1.o" "polyUnite1.ip[0]";
connectAttr "pCylinderShape1.o" "polyUnite1.ip[1]";
connectAttr "pCubeShape1.wm" "polyUnite1.im[0]";
connectAttr "pCylinderShape1.wm" "polyUnite1.im[1]";
connectAttr "polySoftEdge1.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "polySoftEdge2.out" "groupParts2.ig";
connectAttr "groupId3.id" "groupParts2.gi";
connectAttr "polyUnite1.out" "groupParts3.ig";
connectAttr "groupId5.id" "groupParts3.gi";
connectAttr "|pCube7|polySurfaceShape1.o" "polySplitRing7.ip";
connectAttr "pCubeShape7.wm" "polySplitRing7.mp";
connectAttr "polySplitRing7.out" "polySplitRing8.ip";
connectAttr "pCubeShape7.wm" "polySplitRing8.mp";
connectAttr "polySplitRing8.out" "polySplitRing9.ip";
connectAttr "pCubeShape7.wm" "polySplitRing9.mp";
connectAttr "polySplitRing9.out" "polySplitRing10.ip";
connectAttr "pCubeShape7.wm" "polySplitRing10.mp";
connectAttr "polySplitRing10.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "polyBridgeEdge1.ip";
connectAttr "pCubeShape7.wm" "polyBridgeEdge1.mp";
connectAttr "polyBridgeEdge1.out" "polyBridgeEdge2.ip";
connectAttr "pCubeShape7.wm" "polyBridgeEdge2.mp";
connectAttr "polyBridgeEdge2.out" "polyBridgeEdge3.ip";
connectAttr "pCubeShape7.wm" "polyBridgeEdge3.mp";
connectAttr "polyBridgeEdge3.out" "polyBridgeEdge4.ip";
connectAttr "pCubeShape7.wm" "polyBridgeEdge4.mp";
connectAttr "pCubeShape8.o" "polyUnite2.ip[0]";
connectAttr "pCubeShape5.o" "polyUnite2.ip[1]";
connectAttr "pCubeShape4.o" "polyUnite2.ip[2]";
connectAttr "pCubeShape7.o" "polyUnite2.ip[3]";
connectAttr "pCubeShape6.o" "polyUnite2.ip[4]";
connectAttr "pCubeShape2.o" "polyUnite2.ip[5]";
connectAttr "pCubeShape8.wm" "polyUnite2.im[0]";
connectAttr "pCubeShape5.wm" "polyUnite2.im[1]";
connectAttr "pCubeShape4.wm" "polyUnite2.im[2]";
connectAttr "pCubeShape7.wm" "polyUnite2.im[3]";
connectAttr "pCubeShape6.wm" "polyUnite2.im[4]";
connectAttr "pCubeShape2.wm" "polyUnite2.im[5]";
connectAttr "polyBridgeEdge4.out" "groupParts4.ig";
connectAttr "groupId12.id" "groupParts4.gi";
connectAttr "polyCube2.out" "groupParts5.ig";
connectAttr "groupId16.id" "groupParts5.gi";
connectAttr "polyUnite2.out" "groupParts6.ig";
connectAttr "groupId18.id" "groupParts6.gi";
connectAttr "pCube9Shape.o" "polySeparate1.ip";
connectAttr "polySeparate1.out[0]" "groupParts7.ig";
connectAttr "groupId19.id" "groupParts7.gi";
connectAttr "polySeparate1.out[1]" "groupParts8.ig";
connectAttr "groupId20.id" "groupParts8.gi";
connectAttr "polySeparate1.out[2]" "groupParts9.ig";
connectAttr "groupId21.id" "groupParts9.gi";
connectAttr "polySeparate1.out[3]" "groupParts10.ig";
connectAttr "groupId22.id" "groupParts10.gi";
connectAttr "polySeparate1.out[4]" "groupParts11.ig";
connectAttr "groupId23.id" "groupParts11.gi";
connectAttr "polySeparate1.out[5]" "groupParts12.ig";
connectAttr "groupId24.id" "groupParts12.gi";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCube2Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape8.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape8.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape5.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape5.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape4.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape4.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape7.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape7.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape6.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape6.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCube9Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape9.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape11.iog" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape2.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape3.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape4.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape5.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "polySurfaceShape6.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "|pCube9|polySurface6|polySurfaceShape7.iog.og[0]" ":initialShadingGroup.dsm"
		 -na;
connectAttr "|pCube9|polySurface7|polySurfaceShape7.iog.og[0]" ":initialShadingGroup.dsm"
		 -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId2.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId3.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId4.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId5.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId6.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId7.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId8.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId9.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId10.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId11.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId12.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId13.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId14.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId15.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId16.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId17.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId18.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId19.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId20.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId21.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId22.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId23.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId24.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId25.msg" ":initialShadingGroup.gn" -na;
// End of PlatformTree1.ma
