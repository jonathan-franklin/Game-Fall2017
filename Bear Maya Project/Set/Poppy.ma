//Maya ASCII 2017ff05 scene
//Name: Poppy.ma
//Last modified: Sat, Dec 02, 2017 04:21:35 PM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "E7A41A59-40C7-19DB-CA80-7FA308630063";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -8.8513136297567492 9.2007257431303149 -4.352960009469232 ;
	setAttr ".r" -type "double3" -41.738352729603413 -115.39999999998946 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "2C028DEF-48DD-F2AD-355F-5683BFF8127F";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 15.570607230974376;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0 2.1153492293630523 0 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "9816233A-4906-EF60-CD45-55B2C02FAC00";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "20537578-4E33-62F3-D279-A2B49E0B968B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "7154B1FD-451F-A238-7239-EBA64A3DDB51";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "F37D11B5-4064-BA57-8863-079BA6DEF247";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "B48B83DC-4662-826D-646D-BDA6F4F0EBE4";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "4A1B0A7A-414C-9333-F47A-0EA0054BABFD";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCylinder1";
	rename -uid "DF728DBF-444A-A10F-1C8A-B59CD9966461";
	setAttr ".t" -type "double3" 0 -0.9660732008477898 0 ;
	setAttr ".s" -type "double3" 0.023829963468087335 2.985100085073205 0.023829963468087335 ;
createNode mesh -n "pCylinderShape1" -p "pCylinder1";
	rename -uid "E6C4F1EB-405E-6031-EF23-0F8A1791E751";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.008258087513794421 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube1";
	rename -uid "8A87098B-4545-4057-9331-E08712DD73BA";
	setAttr ".t" -type "double3" 0 2.0474123388854939 0.31791489366613795 ;
	setAttr ".r" -type "double3" -13.306057638452355 0 0 ;
	setAttr ".s" -type "double3" 0.25469879216096702 0.010588080556456533 0.66307714702454479 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "599A80E2-4D34-365F-B596-3B91A9491487";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere1";
	rename -uid "7BEB2027-4AD7-0A03-FA78-C68732E020A1";
	setAttr ".t" -type "double3" 0 2.0336845956301675 0 ;
	setAttr ".s" -type "double3" 0.11485416037514896 0.09567607042722906 0.12215182463898212 ;
createNode mesh -n "pSphereShape1" -p "pSphere1";
	rename -uid "8762F636-4152-24E0-37EB-6D88C833EBB7";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.54035117405432276 0.7626380896942262 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube2";
	rename -uid "A1FA68FC-47EE-F8F7-D8EC-69A9CAABFA6A";
	setAttr ".t" -type "double3" 0.25689184388747011 2.0474123388854948 -0.08812947138459265 ;
	setAttr ".r" -type "double3" -13.306057638452428 124.6635579865427 0 ;
	setAttr ".s" -type "double3" 0.25469879216096702 0.010588080556456533 0.66307714702454479 ;
createNode mesh -n "pCubeShape2" -p "pCube2";
	rename -uid "DE66E01A-455E-58BC-4B87-09818A0893E5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape2" -p "pCube2";
	rename -uid "7537C188-489E-97AE-49BB-9294F2C26067";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 60 ".uvst[0].uvsp[0:59]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.17297955 0.25 0.375 0.45202047 0.17297955 0 0.375
		 0.79797953 0.625 0.79797953 0.82702047 0 0.625 0.45202047 0.82702047 0.25 0.625 0.88556194
		 0.73943812 0 0.26056191 0 0.375 0.88556194 0.26056191 0.25 0.375 0.36443812 0.625
		 0.36443812 0.73943812 0.25 0.625 0.28844512 0.66344512 0.25 0.33655491 0.25 0.375
		 0.28844512 0.33655491 0 0.375 0.96155488 0.625 0.96155488 0.66344512 0 0.43813407
		 0.25 0.43813407 0.28844512 0.43813407 0.36443812 0.43813407 0.45202047 0.43813407
		 0.5 0.43813407 0.75 0.43813407 0.79797953 0.43813407 0.88556194 0.43813407 0.96155488
		 0.43813407 0 0.43813407 1 0.54861069 0.25 0.54861069 0.28844512 0.54861069 0.36443812
		 0.54861069 0.45202047 0.54861069 0.5 0.54861069 0.75 0.54861069 0.79797953 0.54861069
		 0.88556194 0.54861069 0.96155488 0.54861069 0 0.54861069 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 40 ".vt[0:39]"  -0.5 14.70538425 0.5 0.5 14.70538425 0.5
		 -0.5 15.70538425 0.5 0.5 15.70538425 0.5 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5
		 0.5 -0.5 -0.5 -0.82508075 0.5 -0.30808181 -0.82508075 -0.5 -0.30808181 0.82508075 -0.5 -0.30808181
		 0.82508075 0.5 -0.30808181 1.32084489 3.34202957 0.042247623 -1.32084489 3.34202957 0.042247623
		 -1.32084489 4.34202957 0.042247608 1.32084489 4.34202957 0.042247608 1.17804277 10.48441696 0.3462196
		 -1.17804277 10.48441696 0.3462196 -1.17804265 9.48441696 0.3462196 1.17804265 9.48441696 0.3462196
		 -0.24746382 15.37636948 0.53024513 -0.58304596 7.68387222 0.3462196 -0.65372264 1.54148483 0.042247608
		 -0.40835524 -2.30054474 -0.30808181 -0.24746382 -2.30054474 -0.5 -0.24746382 -3.30054474 -0.5
		 -0.40835524 -3.30054474 -0.30808181 -0.65372264 0.54148483 0.042247623 -0.58304584 6.68387222 0.3462196
		 -0.24746382 14.37637043 0.53024513 0.19444257 14.11623383 0.53024513 0.45812333 6.42373753 0.3462196
		 0.51365697 0.28134918 0.042247608 0.32086167 -3.56068015 -0.30808181 0.19444257 -3.56068015 -0.5
		 0.19444257 -4.56068039 -0.5 0.32086167 -4.56068039 -0.30808181 0.51365697 -0.71865058 0.042247623
		 0.45812333 5.42373657 0.3462196 0.19444257 13.11623478 0.53024513;
	setAttr -s 76 ".ed[0:75]"  0 29 1 2 20 1 4 24 1 6 25 1 0 2 1 1 3 1 2 17 1
		 3 16 1 4 6 1 5 7 1 6 9 1 7 10 1 8 4 1 9 13 1 8 9 1 10 12 1 9 26 1 11 5 1 10 11 1
		 11 33 1 12 19 1 13 18 1 12 37 1 14 8 1 13 14 1 15 11 1 14 22 1 15 12 1 16 15 1 17 14 1
		 16 31 1 18 0 1 17 18 1 19 1 1 18 28 1 19 16 1 20 30 1 21 17 1 20 21 1 22 32 1 21 22 1
		 23 8 1 22 23 1 24 34 1 23 24 1 25 35 1 24 25 1 26 36 1 25 26 1 27 13 1 26 27 1 28 38 1
		 27 28 1 29 39 1 28 29 1 29 20 1 30 3 1 31 21 1 30 31 1 32 15 1 31 32 1 33 23 1 32 33 1
		 34 5 1 33 34 1 35 7 1 34 35 1 36 10 1 35 36 1 37 27 1 36 37 1 38 19 1 37 38 1 39 1 1
		 38 39 1 39 30 1;
	setAttr -s 38 -ch 152 ".fc[0:37]" -type "polyFaces" 
		f 4 0 55 -2 -5
		mu 0 4 0 47 38 2
		f 4 26 42 41 -24
		mu 0 4 27 40 41 15
		f 4 2 46 -4 -9
		mu 0 4 4 42 43 6
		f 4 16 50 49 -14
		mu 0 4 17 44 45 25
		f 4 27 -16 18 -26
		mu 0 4 29 23 19 21
		f 4 13 24 23 14
		mu 0 4 16 24 26 14
		f 4 10 -15 12 8
		mu 0 4 12 16 14 13
		f 4 3 48 -17 -11
		mu 0 4 6 43 44 17
		f 4 -19 -12 -10 -18
		mu 0 4 21 19 10 11
		f 4 -42 44 -3 -13
		mu 0 4 15 41 42 4
		f 4 34 54 -1 -32
		mu 0 4 35 46 48 8
		f 4 32 31 4 6
		mu 0 4 32 34 0 2
		f 4 1 38 37 -7
		mu 0 4 2 38 39 33
		f 4 -34 35 -8 -6
		mu 0 4 1 37 31 3
		f 4 -38 40 -27 -30
		mu 0 4 33 39 40 27
		f 4 -25 21 -33 29
		mu 0 4 26 24 34 32
		f 4 -50 52 -35 -22
		mu 0 4 25 45 46 35
		f 4 -36 -21 -28 -29
		mu 0 4 31 37 23 29
		f 4 36 58 57 -39
		mu 0 4 38 49 50 39
		f 4 -41 -58 60 -40
		mu 0 4 40 39 50 51
		f 4 -43 39 62 61
		mu 0 4 41 40 51 52
		f 4 -45 -62 64 -44
		mu 0 4 42 41 52 53
		f 4 -47 43 66 -46
		mu 0 4 43 42 53 54
		f 4 -49 45 68 -48
		mu 0 4 44 43 54 55
		f 4 -51 47 70 69
		mu 0 4 45 44 55 56
		f 4 -53 -70 72 -52
		mu 0 4 46 45 56 57
		f 4 -55 51 74 -54
		mu 0 4 48 46 57 59
		f 4 -56 53 75 -37
		mu 0 4 38 47 58 49
		f 4 56 7 30 -59
		mu 0 4 49 3 30 50
		f 4 -61 -31 28 -60
		mu 0 4 51 50 30 28
		f 4 -63 59 25 19
		mu 0 4 52 51 28 20
		f 4 -65 -20 17 -64
		mu 0 4 53 52 20 5
		f 4 -67 63 9 -66
		mu 0 4 54 53 5 7
		f 4 -69 65 11 -68
		mu 0 4 55 54 7 18
		f 4 -71 67 15 22
		mu 0 4 56 55 18 22
		f 4 -73 -23 20 -72
		mu 0 4 57 56 22 36
		f 4 -75 71 33 -74
		mu 0 4 59 57 36 9
		f 4 -76 73 5 -57
		mu 0 4 49 58 1 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube3";
	rename -uid "8BAE4490-4281-F4E5-6337-6A9AEF6F9A6F";
	setAttr ".t" -type "double3" -0.29089250619706664 2.0474123388854948 -0.080917510413193006 ;
	setAttr ".r" -type "double3" -13.306057638452428 250.41582024782551 0 ;
	setAttr ".s" -type "double3" 0.25469879216096702 0.010588080556456533 0.66307714702454479 ;
createNode mesh -n "pCubeShape3" -p "pCube3";
	rename -uid "8ACC7A23-42F4-BAFF-6D85-56B5AB5D850A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape3" -p "pCube3";
	rename -uid "9CF27778-4B2F-9D2C-7D95-1BB38DD5B5EF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 60 ".uvst[0].uvsp[0:59]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.17297955 0.25 0.375 0.45202047 0.17297955 0 0.375
		 0.79797953 0.625 0.79797953 0.82702047 0 0.625 0.45202047 0.82702047 0.25 0.625 0.88556194
		 0.73943812 0 0.26056191 0 0.375 0.88556194 0.26056191 0.25 0.375 0.36443812 0.625
		 0.36443812 0.73943812 0.25 0.625 0.28844512 0.66344512 0.25 0.33655491 0.25 0.375
		 0.28844512 0.33655491 0 0.375 0.96155488 0.625 0.96155488 0.66344512 0 0.43813407
		 0.25 0.43813407 0.28844512 0.43813407 0.36443812 0.43813407 0.45202047 0.43813407
		 0.5 0.43813407 0.75 0.43813407 0.79797953 0.43813407 0.88556194 0.43813407 0.96155488
		 0.43813407 0 0.43813407 1 0.54861069 0.25 0.54861069 0.28844512 0.54861069 0.36443812
		 0.54861069 0.45202047 0.54861069 0.5 0.54861069 0.75 0.54861069 0.79797953 0.54861069
		 0.88556194 0.54861069 0.96155488 0.54861069 0 0.54861069 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 40 ".vt[0:39]"  -0.5 14.70538425 0.5 0.5 14.70538425 0.5
		 -0.5 15.70538425 0.5 0.5 15.70538425 0.5 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5
		 0.5 -0.5 -0.5 -0.82508075 0.5 -0.30808181 -0.82508075 -0.5 -0.30808181 0.82508075 -0.5 -0.30808181
		 0.82508075 0.5 -0.30808181 1.32084489 3.34202957 0.042247623 -1.32084489 3.34202957 0.042247623
		 -1.32084489 4.34202957 0.042247608 1.32084489 4.34202957 0.042247608 1.17804277 10.48441696 0.3462196
		 -1.17804277 10.48441696 0.3462196 -1.17804265 9.48441696 0.3462196 1.17804265 9.48441696 0.3462196
		 -0.24746382 15.37636948 0.53024513 -0.58304596 7.68387222 0.3462196 -0.65372264 1.54148483 0.042247608
		 -0.40835524 -2.30054474 -0.30808181 -0.24746382 -2.30054474 -0.5 -0.24746382 -3.30054474 -0.5
		 -0.40835524 -3.30054474 -0.30808181 -0.65372264 0.54148483 0.042247623 -0.58304584 6.68387222 0.3462196
		 -0.24746382 14.37637043 0.53024513 0.19444257 14.11623383 0.53024513 0.45812333 6.42373753 0.3462196
		 0.51365697 0.28134918 0.042247608 0.32086167 -3.56068015 -0.30808181 0.19444257 -3.56068015 -0.5
		 0.19444257 -4.56068039 -0.5 0.32086167 -4.56068039 -0.30808181 0.51365697 -0.71865058 0.042247623
		 0.45812333 5.42373657 0.3462196 0.19444257 13.11623478 0.53024513;
	setAttr -s 76 ".ed[0:75]"  0 29 1 2 20 1 4 24 1 6 25 1 0 2 1 1 3 1 2 17 1
		 3 16 1 4 6 1 5 7 1 6 9 1 7 10 1 8 4 1 9 13 1 8 9 1 10 12 1 9 26 1 11 5 1 10 11 1
		 11 33 1 12 19 1 13 18 1 12 37 1 14 8 1 13 14 1 15 11 1 14 22 1 15 12 1 16 15 1 17 14 1
		 16 31 1 18 0 1 17 18 1 19 1 1 18 28 1 19 16 1 20 30 1 21 17 1 20 21 1 22 32 1 21 22 1
		 23 8 1 22 23 1 24 34 1 23 24 1 25 35 1 24 25 1 26 36 1 25 26 1 27 13 1 26 27 1 28 38 1
		 27 28 1 29 39 1 28 29 1 29 20 1 30 3 1 31 21 1 30 31 1 32 15 1 31 32 1 33 23 1 32 33 1
		 34 5 1 33 34 1 35 7 1 34 35 1 36 10 1 35 36 1 37 27 1 36 37 1 38 19 1 37 38 1 39 1 1
		 38 39 1 39 30 1;
	setAttr -s 38 -ch 152 ".fc[0:37]" -type "polyFaces" 
		f 4 0 55 -2 -5
		mu 0 4 0 47 38 2
		f 4 26 42 41 -24
		mu 0 4 27 40 41 15
		f 4 2 46 -4 -9
		mu 0 4 4 42 43 6
		f 4 16 50 49 -14
		mu 0 4 17 44 45 25
		f 4 27 -16 18 -26
		mu 0 4 29 23 19 21
		f 4 13 24 23 14
		mu 0 4 16 24 26 14
		f 4 10 -15 12 8
		mu 0 4 12 16 14 13
		f 4 3 48 -17 -11
		mu 0 4 6 43 44 17
		f 4 -19 -12 -10 -18
		mu 0 4 21 19 10 11
		f 4 -42 44 -3 -13
		mu 0 4 15 41 42 4
		f 4 34 54 -1 -32
		mu 0 4 35 46 48 8
		f 4 32 31 4 6
		mu 0 4 32 34 0 2
		f 4 1 38 37 -7
		mu 0 4 2 38 39 33
		f 4 -34 35 -8 -6
		mu 0 4 1 37 31 3
		f 4 -38 40 -27 -30
		mu 0 4 33 39 40 27
		f 4 -25 21 -33 29
		mu 0 4 26 24 34 32
		f 4 -50 52 -35 -22
		mu 0 4 25 45 46 35
		f 4 -36 -21 -28 -29
		mu 0 4 31 37 23 29
		f 4 36 58 57 -39
		mu 0 4 38 49 50 39
		f 4 -41 -58 60 -40
		mu 0 4 40 39 50 51
		f 4 -43 39 62 61
		mu 0 4 41 40 51 52
		f 4 -45 -62 64 -44
		mu 0 4 42 41 52 53
		f 4 -47 43 66 -46
		mu 0 4 43 42 53 54
		f 4 -49 45 68 -48
		mu 0 4 44 43 54 55
		f 4 -51 47 70 69
		mu 0 4 45 44 55 56
		f 4 -53 -70 72 -52
		mu 0 4 46 45 56 57
		f 4 -55 51 74 -54
		mu 0 4 48 46 57 59
		f 4 -56 53 75 -37
		mu 0 4 38 47 58 49
		f 4 56 7 30 -59
		mu 0 4 49 3 30 50
		f 4 -61 -31 28 -60
		mu 0 4 51 50 30 28
		f 4 -63 59 25 19
		mu 0 4 52 51 28 20
		f 4 -65 -20 17 -64
		mu 0 4 53 52 20 5
		f 4 -67 63 9 -66
		mu 0 4 54 53 5 7
		f 4 -69 65 11 -68
		mu 0 4 55 54 7 18
		f 4 -71 67 15 22
		mu 0 4 56 55 18 22
		f 4 -73 -23 20 -72
		mu 0 4 57 56 22 36
		f 4 -75 71 33 -74
		mu 0 4 59 57 36 9
		f 4 -76 73 5 -57
		mu 0 4 49 58 1 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube4";
	rename -uid "2EDA44D9-4355-2122-0856-ECBD475FFDA3";
	setAttr ".t" -type "double3" 0 1.7193677656661643 0.31791489366613795 ;
	setAttr ".r" -type "double3" 180 0 180 ;
	setAttr ".s" -type "double3" 0.52640860228129127 0.010588080556456533 0.66307714702454479 ;
createNode mesh -n "pCubeShape4" -p "pCube4";
	rename -uid "F6B233E2-40F9-86E7-607A-89A7F480CC0F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape1" -p "pCube4";
	rename -uid "5B70C311-4EB3-2D85-5E12-AD816AB0FFA6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 60 ".uvst[0].uvsp[0:59]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.17297955 0.25 0.375 0.45202047 0.17297955 0 0.375
		 0.79797953 0.625 0.79797953 0.82702047 0 0.625 0.45202047 0.82702047 0.25 0.625 0.88556194
		 0.73943812 0 0.26056191 0 0.375 0.88556194 0.26056191 0.25 0.375 0.36443812 0.625
		 0.36443812 0.73943812 0.25 0.625 0.28844512 0.66344512 0.25 0.33655491 0.25 0.375
		 0.28844512 0.33655491 0 0.375 0.96155488 0.625 0.96155488 0.66344512 0 0.43813407
		 0.25 0.43813407 0.28844512 0.43813407 0.36443812 0.43813407 0.45202047 0.43813407
		 0.5 0.43813407 0.75 0.43813407 0.79797953 0.43813407 0.88556194 0.43813407 0.96155488
		 0.43813407 0 0.43813407 1 0.54861069 0.25 0.54861069 0.28844512 0.54861069 0.36443812
		 0.54861069 0.45202047 0.54861069 0.5 0.54861069 0.75 0.54861069 0.79797953 0.54861069
		 0.88556194 0.54861069 0.96155488 0.54861069 0 0.54861069 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 32 ".pt";
	setAttr ".pt[4]" -type "float3" 6.6613381e-016 2.5134904 -0.00047636032 ;
	setAttr ".pt[5]" -type "float3" -6.6613381e-016 2.5134904 -0.00047636032 ;
	setAttr ".pt[6]" -type "float3" 6.6613381e-016 2.5135 -0.0005449059 ;
	setAttr ".pt[7]" -type "float3" -6.6613381e-016 2.5135 -0.0005449059 ;
	setAttr ".pt[8]" -type "float3" -0.057139985 0.021354668 -0.00047807407 ;
	setAttr ".pt[9]" -type "float3" -0.057139985 0.021364219 -0.00054661906 ;
	setAttr ".pt[10]" -type "float3" 0.057139978 0.021364219 -0.00054661906 ;
	setAttr ".pt[11]" -type "float3" 0.057139978 0.021354668 -0.00047807407 ;
	setAttr ".pt[12]" -type "float3" -0.080365255 -0.072840214 -0.00028645992 ;
	setAttr ".pt[13]" -type "float3" 0.080365255 -0.072840214 -0.00028645992 ;
	setAttr ".pt[14]" -type "float3" 0.080365255 -0.072849751 -0.0002179743 ;
	setAttr ".pt[15]" -type "float3" -0.080365255 -0.072849751 -0.0002179743 ;
	setAttr ".pt[20]" -type "float3" 0 8.9423056 0 ;
	setAttr ".pt[21]" -type "float3" 0 8.9423056 0 ;
	setAttr ".pt[22]" -type "float3" 0.039774988 13.857395 0.00054484629 ;
	setAttr ".pt[23]" -type "float3" -0.028280158 13.069137 0.00022426253 ;
	setAttr ".pt[24]" -type "float3" 3.3306691e-016 9.0151939 -0.11720279 ;
	setAttr ".pt[25]" -type "float3" 3.3306691e-016 9.0152035 -0.11727133 ;
	setAttr ".pt[26]" -type "float3" -0.028280158 13.069152 0.00015571713 ;
	setAttr ".pt[27]" -type "float3" 0.039774988 13.857406 0.00047630095 ;
	setAttr ".pt[28]" -type "float3" 0 8.9423056 0 ;
	setAttr ".pt[29]" -type "float3" 0 8.9423056 0 ;
	setAttr ".pt[30]" -type "float3" 0 8.9423056 0 ;
	setAttr ".pt[31]" -type "float3" 0 8.9423056 0 ;
	setAttr ".pt[32]" -type "float3" -0.031252854 13.857407 0.00045847881 ;
	setAttr ".pt[33]" -type "float3" 0.022220887 13.06915 0.00013789529 ;
	setAttr ".pt[34]" -type "float3" -3.3306691e-016 9.0152025 -0.11728916 ;
	setAttr ".pt[35]" -type "float3" -3.3306691e-016 9.015214 -0.1173577 ;
	setAttr ".pt[36]" -type "float3" 0.022220887 13.069162 6.9350033e-005 ;
	setAttr ".pt[37]" -type "float3" -0.031252854 13.857415 0.00038993335 ;
	setAttr ".pt[38]" -type "float3" 0 8.9423056 0 ;
	setAttr ".pt[39]" -type "float3" 0 8.9423056 0 ;
	setAttr -s 40 ".vt[0:39]"  -0.5 14.70538425 0.5 0.5 14.70538425 0.5
		 -0.5 15.70538425 0.5 0.5 15.70538425 0.5 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5
		 0.5 -0.5 -0.5 -0.82508075 0.5 -0.30808181 -0.82508075 -0.5 -0.30808181 0.82508075 -0.5 -0.30808181
		 0.82508075 0.5 -0.30808181 1.32084489 3.34202957 0.042247623 -1.32084489 3.34202957 0.042247623
		 -1.32084489 4.34202957 0.042247608 1.32084489 4.34202957 0.042247608 1.17804277 10.48441696 0.3462196
		 -1.17804277 10.48441696 0.3462196 -1.17804265 9.48441696 0.3462196 1.17804265 9.48441696 0.3462196
		 -0.24746382 15.37636948 0.53024513 -0.58304596 7.68387222 0.3462196 -0.65372264 1.54148483 0.042247608
		 -0.40835524 -2.30054474 -0.30808181 -0.24746382 -2.30054474 -0.5 -0.24746382 -3.30054474 -0.5
		 -0.40835524 -3.30054474 -0.30808181 -0.65372264 0.54148483 0.042247623 -0.58304584 6.68387222 0.3462196
		 -0.24746382 14.37637043 0.53024513 0.19444257 14.11623383 0.53024513 0.45812333 6.42373753 0.3462196
		 0.51365697 0.28134918 0.042247608 0.32086167 -3.56068015 -0.30808181 0.19444257 -3.56068015 -0.5
		 0.19444257 -4.56068039 -0.5 0.32086167 -4.56068039 -0.30808181 0.51365697 -0.71865058 0.042247623
		 0.45812333 5.42373657 0.3462196 0.19444257 13.11623478 0.53024513;
	setAttr -s 76 ".ed[0:75]"  0 29 1 2 20 1 4 24 1 6 25 1 0 2 1 1 3 1 2 17 1
		 3 16 1 4 6 1 5 7 1 6 9 1 7 10 1 8 4 1 9 13 1 8 9 1 10 12 1 9 26 1 11 5 1 10 11 1
		 11 33 1 12 19 1 13 18 1 12 37 1 14 8 1 13 14 1 15 11 1 14 22 1 15 12 1 16 15 1 17 14 1
		 16 31 1 18 0 1 17 18 1 19 1 1 18 28 1 19 16 1 20 30 1 21 17 1 20 21 1 22 32 1 21 22 1
		 23 8 1 22 23 1 24 34 1 23 24 1 25 35 1 24 25 1 26 36 1 25 26 1 27 13 1 26 27 1 28 38 1
		 27 28 1 29 39 1 28 29 1 29 20 1 30 3 1 31 21 1 30 31 1 32 15 1 31 32 1 33 23 1 32 33 1
		 34 5 1 33 34 1 35 7 1 34 35 1 36 10 1 35 36 1 37 27 1 36 37 1 38 19 1 37 38 1 39 1 1
		 38 39 1 39 30 1;
	setAttr -s 38 -ch 152 ".fc[0:37]" -type "polyFaces" 
		f 4 0 55 -2 -5
		mu 0 4 0 47 38 2
		f 4 26 42 41 -24
		mu 0 4 27 40 41 15
		f 4 2 46 -4 -9
		mu 0 4 4 42 43 6
		f 4 16 50 49 -14
		mu 0 4 17 44 45 25
		f 4 27 -16 18 -26
		mu 0 4 29 23 19 21
		f 4 13 24 23 14
		mu 0 4 16 24 26 14
		f 4 10 -15 12 8
		mu 0 4 12 16 14 13
		f 4 3 48 -17 -11
		mu 0 4 6 43 44 17
		f 4 -19 -12 -10 -18
		mu 0 4 21 19 10 11
		f 4 -42 44 -3 -13
		mu 0 4 15 41 42 4
		f 4 34 54 -1 -32
		mu 0 4 35 46 48 8
		f 4 32 31 4 6
		mu 0 4 32 34 0 2
		f 4 1 38 37 -7
		mu 0 4 2 38 39 33
		f 4 -34 35 -8 -6
		mu 0 4 1 37 31 3
		f 4 -38 40 -27 -30
		mu 0 4 33 39 40 27
		f 4 -25 21 -33 29
		mu 0 4 26 24 34 32
		f 4 -50 52 -35 -22
		mu 0 4 25 45 46 35
		f 4 -36 -21 -28 -29
		mu 0 4 31 37 23 29
		f 4 36 58 57 -39
		mu 0 4 38 49 50 39
		f 4 -41 -58 60 -40
		mu 0 4 40 39 50 51
		f 4 -43 39 62 61
		mu 0 4 41 40 51 52
		f 4 -45 -62 64 -44
		mu 0 4 42 41 52 53
		f 4 -47 43 66 -46
		mu 0 4 43 42 53 54
		f 4 -49 45 68 -48
		mu 0 4 44 43 54 55
		f 4 -51 47 70 69
		mu 0 4 45 44 55 56
		f 4 -53 -70 72 -52
		mu 0 4 46 45 56 57
		f 4 -55 51 74 -54
		mu 0 4 48 46 57 59
		f 4 -56 53 75 -37
		mu 0 4 38 47 58 49
		f 4 56 7 30 -59
		mu 0 4 49 3 30 50
		f 4 -61 -31 28 -60
		mu 0 4 51 50 30 28
		f 4 -63 59 25 19
		mu 0 4 52 51 28 20
		f 4 -65 -20 17 -64
		mu 0 4 53 52 20 5
		f 4 -67 63 9 -66
		mu 0 4 54 53 5 7
		f 4 -69 65 11 -68
		mu 0 4 55 54 7 18
		f 4 -71 67 15 22
		mu 0 4 56 55 18 22
		f 4 -73 -23 20 -72
		mu 0 4 57 56 22 36
		f 4 -75 71 33 -74
		mu 0 4 59 57 36 9
		f 4 -76 73 5 -57
		mu 0 4 49 58 1 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube5";
	rename -uid "ECDEF691-4419-6CB4-6DFC-399A1A3D7D9B";
	setAttr ".t" -type "double3" 0 1.7193677656661643 -0.34667617040607779 ;
	setAttr ".r" -type "double3" 179.99999999999994 180 180 ;
	setAttr ".s" -type "double3" 0.52640860228129127 0.010588080556456533 0.66307714702454479 ;
createNode mesh -n "pCubeShape5" -p "pCube5";
	rename -uid "FA6B7AEB-432C-92B6-94F6-8FB8B68EBCD6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape4" -p "pCube5";
	rename -uid "B8C679A3-4690-B972-9B86-EE889EE44DF4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 60 ".uvst[0].uvsp[0:59]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.17297955 0.25 0.375 0.45202047 0.17297955 0 0.375
		 0.79797953 0.625 0.79797953 0.82702047 0 0.625 0.45202047 0.82702047 0.25 0.625 0.88556194
		 0.73943812 0 0.26056191 0 0.375 0.88556194 0.26056191 0.25 0.375 0.36443812 0.625
		 0.36443812 0.73943812 0.25 0.625 0.28844512 0.66344512 0.25 0.33655491 0.25 0.375
		 0.28844512 0.33655491 0 0.375 0.96155488 0.625 0.96155488 0.66344512 0 0.43813407
		 0.25 0.43813407 0.28844512 0.43813407 0.36443812 0.43813407 0.45202047 0.43813407
		 0.5 0.43813407 0.75 0.43813407 0.79797953 0.43813407 0.88556194 0.43813407 0.96155488
		 0.43813407 0 0.43813407 1 0.54861069 0.25 0.54861069 0.28844512 0.54861069 0.36443812
		 0.54861069 0.45202047 0.54861069 0.5 0.54861069 0.75 0.54861069 0.79797953 0.54861069
		 0.88556194 0.54861069 0.96155488 0.54861069 0 0.54861069 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 32 ".pt";
	setAttr ".pt[4]" -type "float3" 6.6613381e-016 2.5134904 -0.00047636032 ;
	setAttr ".pt[5]" -type "float3" -6.6613381e-016 2.5134904 -0.00047636032 ;
	setAttr ".pt[6]" -type "float3" 6.6613381e-016 2.5135 -0.0005449059 ;
	setAttr ".pt[7]" -type "float3" -6.6613381e-016 2.5135 -0.0005449059 ;
	setAttr ".pt[8]" -type "float3" -0.057139985 0.021354668 -0.00047807407 ;
	setAttr ".pt[9]" -type "float3" -0.057139985 0.021364219 -0.00054661906 ;
	setAttr ".pt[10]" -type "float3" 0.057139978 0.021364219 -0.00054661906 ;
	setAttr ".pt[11]" -type "float3" 0.057139978 0.021354668 -0.00047807407 ;
	setAttr ".pt[12]" -type "float3" -0.080365255 -0.072840214 -0.00028645992 ;
	setAttr ".pt[13]" -type "float3" 0.080365255 -0.072840214 -0.00028645992 ;
	setAttr ".pt[14]" -type "float3" 0.080365255 -0.072849751 -0.0002179743 ;
	setAttr ".pt[15]" -type "float3" -0.080365255 -0.072849751 -0.0002179743 ;
	setAttr ".pt[20]" -type "float3" 0 8.9423056 0 ;
	setAttr ".pt[21]" -type "float3" 0 8.9423056 0 ;
	setAttr ".pt[22]" -type "float3" 0.039774988 13.857395 0.00054484629 ;
	setAttr ".pt[23]" -type "float3" -0.028280158 13.069137 0.00022426253 ;
	setAttr ".pt[24]" -type "float3" 3.3306691e-016 9.0151939 -0.11720279 ;
	setAttr ".pt[25]" -type "float3" 3.3306691e-016 9.0152035 -0.11727133 ;
	setAttr ".pt[26]" -type "float3" -0.028280158 13.069152 0.00015571713 ;
	setAttr ".pt[27]" -type "float3" 0.039774988 13.857406 0.00047630095 ;
	setAttr ".pt[28]" -type "float3" 0 8.9423056 0 ;
	setAttr ".pt[29]" -type "float3" 0 8.9423056 0 ;
	setAttr ".pt[30]" -type "float3" 0 8.9423056 0 ;
	setAttr ".pt[31]" -type "float3" 0 8.9423056 0 ;
	setAttr ".pt[32]" -type "float3" -0.031252854 13.857407 0.00045847881 ;
	setAttr ".pt[33]" -type "float3" 0.022220887 13.06915 0.00013789529 ;
	setAttr ".pt[34]" -type "float3" -3.3306691e-016 9.0152025 -0.11728916 ;
	setAttr ".pt[35]" -type "float3" -3.3306691e-016 9.015214 -0.1173577 ;
	setAttr ".pt[36]" -type "float3" 0.022220887 13.069162 6.9350033e-005 ;
	setAttr ".pt[37]" -type "float3" -0.031252854 13.857415 0.00038993335 ;
	setAttr ".pt[38]" -type "float3" 0 8.9423056 0 ;
	setAttr ".pt[39]" -type "float3" 0 8.9423056 0 ;
	setAttr -s 40 ".vt[0:39]"  -0.5 14.70538425 0.5 0.5 14.70538425 0.5
		 -0.5 15.70538425 0.5 0.5 15.70538425 0.5 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5
		 0.5 -0.5 -0.5 -0.82508075 0.5 -0.30808181 -0.82508075 -0.5 -0.30808181 0.82508075 -0.5 -0.30808181
		 0.82508075 0.5 -0.30808181 1.32084489 3.34202957 0.042247623 -1.32084489 3.34202957 0.042247623
		 -1.32084489 4.34202957 0.042247608 1.32084489 4.34202957 0.042247608 1.17804277 10.48441696 0.3462196
		 -1.17804277 10.48441696 0.3462196 -1.17804265 9.48441696 0.3462196 1.17804265 9.48441696 0.3462196
		 -0.24746382 15.37636948 0.53024513 -0.58304596 7.68387222 0.3462196 -0.65372264 1.54148483 0.042247608
		 -0.40835524 -2.30054474 -0.30808181 -0.24746382 -2.30054474 -0.5 -0.24746382 -3.30054474 -0.5
		 -0.40835524 -3.30054474 -0.30808181 -0.65372264 0.54148483 0.042247623 -0.58304584 6.68387222 0.3462196
		 -0.24746382 14.37637043 0.53024513 0.19444257 14.11623383 0.53024513 0.45812333 6.42373753 0.3462196
		 0.51365697 0.28134918 0.042247608 0.32086167 -3.56068015 -0.30808181 0.19444257 -3.56068015 -0.5
		 0.19444257 -4.56068039 -0.5 0.32086167 -4.56068039 -0.30808181 0.51365697 -0.71865058 0.042247623
		 0.45812333 5.42373657 0.3462196 0.19444257 13.11623478 0.53024513;
	setAttr -s 76 ".ed[0:75]"  0 29 1 2 20 1 4 24 1 6 25 1 0 2 1 1 3 1 2 17 1
		 3 16 1 4 6 1 5 7 1 6 9 1 7 10 1 8 4 1 9 13 1 8 9 1 10 12 1 9 26 1 11 5 1 10 11 1
		 11 33 1 12 19 1 13 18 1 12 37 1 14 8 1 13 14 1 15 11 1 14 22 1 15 12 1 16 15 1 17 14 1
		 16 31 1 18 0 1 17 18 1 19 1 1 18 28 1 19 16 1 20 30 1 21 17 1 20 21 1 22 32 1 21 22 1
		 23 8 1 22 23 1 24 34 1 23 24 1 25 35 1 24 25 1 26 36 1 25 26 1 27 13 1 26 27 1 28 38 1
		 27 28 1 29 39 1 28 29 1 29 20 1 30 3 1 31 21 1 30 31 1 32 15 1 31 32 1 33 23 1 32 33 1
		 34 5 1 33 34 1 35 7 1 34 35 1 36 10 1 35 36 1 37 27 1 36 37 1 38 19 1 37 38 1 39 1 1
		 38 39 1 39 30 1;
	setAttr -s 38 -ch 152 ".fc[0:37]" -type "polyFaces" 
		f 4 0 55 -2 -5
		mu 0 4 0 47 38 2
		f 4 26 42 41 -24
		mu 0 4 27 40 41 15
		f 4 2 46 -4 -9
		mu 0 4 4 42 43 6
		f 4 16 50 49 -14
		mu 0 4 17 44 45 25
		f 4 27 -16 18 -26
		mu 0 4 29 23 19 21
		f 4 13 24 23 14
		mu 0 4 16 24 26 14
		f 4 10 -15 12 8
		mu 0 4 12 16 14 13
		f 4 3 48 -17 -11
		mu 0 4 6 43 44 17
		f 4 -19 -12 -10 -18
		mu 0 4 21 19 10 11
		f 4 -42 44 -3 -13
		mu 0 4 15 41 42 4
		f 4 34 54 -1 -32
		mu 0 4 35 46 48 8
		f 4 32 31 4 6
		mu 0 4 32 34 0 2
		f 4 1 38 37 -7
		mu 0 4 2 38 39 33
		f 4 -34 35 -8 -6
		mu 0 4 1 37 31 3
		f 4 -38 40 -27 -30
		mu 0 4 33 39 40 27
		f 4 -25 21 -33 29
		mu 0 4 26 24 34 32
		f 4 -50 52 -35 -22
		mu 0 4 25 45 46 35
		f 4 -36 -21 -28 -29
		mu 0 4 31 37 23 29
		f 4 36 58 57 -39
		mu 0 4 38 49 50 39
		f 4 -41 -58 60 -40
		mu 0 4 40 39 50 51
		f 4 -43 39 62 61
		mu 0 4 41 40 51 52
		f 4 -45 -62 64 -44
		mu 0 4 42 41 52 53
		f 4 -47 43 66 -46
		mu 0 4 43 42 53 54
		f 4 -49 45 68 -48
		mu 0 4 44 43 54 55
		f 4 -51 47 70 69
		mu 0 4 45 44 55 56
		f 4 -53 -70 72 -52
		mu 0 4 46 45 56 57
		f 4 -55 51 74 -54
		mu 0 4 48 46 57 59
		f 4 -56 53 75 -37
		mu 0 4 38 47 58 49
		f 4 56 7 30 -59
		mu 0 4 49 3 30 50
		f 4 -61 -31 28 -60
		mu 0 4 51 50 30 28
		f 4 -63 59 25 19
		mu 0 4 52 51 28 20
		f 4 -65 -20 17 -64
		mu 0 4 53 52 20 5
		f 4 -67 63 9 -66
		mu 0 4 54 53 5 7
		f 4 -69 65 11 -68
		mu 0 4 55 54 7 18
		f 4 -71 67 15 22
		mu 0 4 56 55 18 22
		f 4 -73 -23 20 -72
		mu 0 4 57 56 22 36
		f 4 -75 71 33 -74
		mu 0 4 59 57 36 9
		f 4 -76 73 5 -57
		mu 0 4 49 58 1 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pPlane1";
	rename -uid "044DFD33-4109-6BAF-6413-F58E79912E7C";
	setAttr ".t" -type "double3" 0 -3.8768177324462192 0.71545458688056418 ;
	setAttr ".s" -type "double3" 1 1 1.3926119855565968 ;
createNode mesh -n "pPlaneShape1" -p "pPlane1";
	rename -uid "2BF4D9E2-45B4-4003-63A6-56B21C1995A3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.84310979782345363 0.5128318442845009 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pPlane2";
	rename -uid "32786346-40B6-EA7C-E3EE-00A354EAAB42";
	setAttr ".t" -type "double3" 0.61671623785528884 -3.8768177324462192 0.40834957345393197 ;
	setAttr ".r" -type "double3" 0 65.652046752123084 0 ;
	setAttr ".s" -type "double3" 1 1 1.3926119855565968 ;
createNode mesh -n "pPlaneShape2" -p "pPlane2";
	rename -uid "2AC1031B-4F32-2642-75C5-D8A5E3FAA019";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.67752394462391652 0.78631667470351985 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape9" -p "pPlane2";
	rename -uid "6AC9D4EC-4DE7-A710-DCA1-2CBCC5BEDFCF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.080711223185062408 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0 0 1 0 0 1 1 1 0
		 0.73750263 1 0.73750263 0 0.35097235 1 0.35097235 0 0.17081477 1 0.17081477 1 0.080711223
		 0 0.080711223;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt[10:11]" -type "float3"  0.070992626 0 0 -0.070992626 
		0 0;
	setAttr -s 12 ".vt[0:11]"  -0.20622614 -1.110223e-016 0.5 0.20622614 -1.110223e-016 0.5
		 -0.084332794 1.110223e-016 -0.5 0.084332794 1.110223e-016 -0.5 -0.34489086 0.18595053 -0.23750263
		 0.34489086 0.18595053 -0.23750263 -0.46862853 0.29710406 0.14902765 0.46862853 0.29710406 0.14902765
		 -0.5 0.21675289 0.32918522 0.5 0.21675289 0.32918522 0.34503645 0.10241732 0.41928875
		 -0.34503645 0.10241732 0.41928875;
	setAttr -s 16 ".ed[0:15]"  0 1 0 0 11 0 1 10 0 2 3 0 4 2 0 5 3 0 4 5 1
		 6 4 0 7 5 0 6 7 1 8 6 0 9 7 0 8 9 1 10 9 0 11 8 0 10 11 1;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 6 5 -4 -5
		mu 0 4 4 5 3 2
		f 4 9 8 -7 -8
		mu 0 4 6 7 5 4
		f 4 12 11 -10 -11
		mu 0 4 8 9 7 6
		f 4 0 2 15 -2
		mu 0 4 0 1 10 11
		f 4 -16 13 -13 -15
		mu 0 4 11 10 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pPlane3";
	rename -uid "48FB1E82-4E77-326B-3C99-A4888295A76F";
	setAttr ".t" -type "double3" 0.29899637286863912 -3.876817732446221 -0.61238961603180719 ;
	setAttr ".r" -type "double3" 0 160.71491556841809 0 ;
	setAttr ".s" -type "double3" 1 1 1.3926119855565968 ;
createNode mesh -n "pPlaneShape3" -p "pPlane3";
	rename -uid "B307E538-409B-86A7-C3D2-04AE811DD340";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.26725981704681256 0.23219333308606993 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape8" -p "pPlane3";
	rename -uid "BBFD27FC-407E-1E55-3AD2-10A008D021D1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.080711223185062408 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0 0 1 0 0 1 1 1 0
		 0.73750263 1 0.73750263 0 0.35097235 1 0.35097235 0 0.17081477 1 0.17081477 1 0.080711223
		 0 0.080711223;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt[10:11]" -type "float3"  0.070992626 0 0 -0.070992626 
		0 0;
	setAttr -s 12 ".vt[0:11]"  -0.20622614 -1.110223e-016 0.5 0.20622614 -1.110223e-016 0.5
		 -0.084332794 1.110223e-016 -0.5 0.084332794 1.110223e-016 -0.5 -0.34489086 0.18595053 -0.23750263
		 0.34489086 0.18595053 -0.23750263 -0.46862853 0.29710406 0.14902765 0.46862853 0.29710406 0.14902765
		 -0.5 0.21675289 0.32918522 0.5 0.21675289 0.32918522 0.34503645 0.10241732 0.41928875
		 -0.34503645 0.10241732 0.41928875;
	setAttr -s 16 ".ed[0:15]"  0 1 0 0 11 0 1 10 0 2 3 0 4 2 0 5 3 0 4 5 1
		 6 4 0 7 5 0 6 7 1 8 6 0 9 7 0 8 9 1 10 9 0 11 8 0 10 11 1;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 6 5 -4 -5
		mu 0 4 4 5 3 2
		f 4 9 8 -7 -8
		mu 0 4 6 7 5 4
		f 4 12 11 -10 -11
		mu 0 4 8 9 7 6
		f 4 0 2 15 -2
		mu 0 4 0 1 10 11
		f 4 -16 13 -13 -15
		mu 0 4 11 10 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pPlane4";
	rename -uid "55C5380B-41CA-2DEB-D685-86982EC8101C";
	setAttr ".t" -type "double3" -0.64630638259075845 -3.876817732446221 0.012963843489705695 ;
	setAttr ".r" -type "double3" 0 272.12185330537187 0 ;
	setAttr ".s" -type "double3" 1 1 1.3926119855565968 ;
createNode mesh -n "pPlaneShape4" -p "pPlane4";
	rename -uid "E8BE189D-4507-C2A5-4464-9983A79B86CA";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.69556322910324986 0.17696265629212471 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape7" -p "pPlane4";
	rename -uid "5873BD48-4528-366B-9616-5F9D356F9295";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.080711223185062408 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0 0 1 0 0 1 1 1 0
		 0.73750263 1 0.73750263 0 0.35097235 1 0.35097235 0 0.17081477 1 0.17081477 1 0.080711223
		 0 0.080711223;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt[10:11]" -type "float3"  0.070992626 0 0 -0.070992626 
		0 0;
	setAttr -s 12 ".vt[0:11]"  -0.20622614 -1.110223e-016 0.5 0.20622614 -1.110223e-016 0.5
		 -0.084332794 1.110223e-016 -0.5 0.084332794 1.110223e-016 -0.5 -0.34489086 0.18595053 -0.23750263
		 0.34489086 0.18595053 -0.23750263 -0.46862853 0.29710406 0.14902765 0.46862853 0.29710406 0.14902765
		 -0.5 0.21675289 0.32918522 0.5 0.21675289 0.32918522 0.34503645 0.10241732 0.41928875
		 -0.34503645 0.10241732 0.41928875;
	setAttr -s 16 ".ed[0:15]"  0 1 0 0 11 0 1 10 0 2 3 0 4 2 0 5 3 0 4 5 1
		 6 4 0 7 5 0 6 7 1 8 6 0 9 7 0 8 9 1 10 9 0 11 8 0 10 11 1;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 6 5 -4 -5
		mu 0 4 4 5 3 2
		f 4 9 8 -7 -8
		mu 0 4 6 7 5 4
		f 4 12 11 -10 -11
		mu 0 4 8 9 7 6
		f 4 0 2 15 -2
		mu 0 4 0 1 10 11
		f 4 -16 13 -13 -15
		mu 0 4 11 10 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pPlane5";
	rename -uid "67464EE7-466B-90D0-D307-C2B05DB2C063";
	setAttr ".t" -type "double3" -0.57563339851546835 -3.7449386524498793 -0.55600500280440546 ;
	setAttr ".r" -type "double3" -6.4194990951172981 228.35861271782613 0.34299211098484794 ;
	setAttr ".s" -type "double3" 1.2227313095481003 1.4154475471965986 1.5348570884966617 ;
createNode mesh -n "pPlaneShape5" -p "pPlane5";
	rename -uid "5FA75F99-40C8-1CA9-49A3-92B66153F477";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.39109061173540155 0.55923710316038011 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape6" -p "pPlane5";
	rename -uid "A89FB75D-40FE-2A98-EE80-BFAA338E28D0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.080711223185062408 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0 0 1 0 0 1 1 1 0
		 0.73750263 1 0.73750263 0 0.35097235 1 0.35097235 0 0.17081477 1 0.17081477 1 0.080711223
		 0 0.080711223;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt[10:11]" -type "float3"  0.070992626 0 0 -0.070992626 
		0 0;
	setAttr -s 12 ".vt[0:11]"  -0.20622614 -1.110223e-016 0.5 0.20622614 -1.110223e-016 0.5
		 -0.084332794 1.110223e-016 -0.5 0.084332794 1.110223e-016 -0.5 -0.34489086 0.18595053 -0.23750263
		 0.34489086 0.18595053 -0.23750263 -0.46862853 0.29710406 0.14902765 0.46862853 0.29710406 0.14902765
		 -0.5 0.21675289 0.32918522 0.5 0.21675289 0.32918522 0.34503645 0.10241732 0.41928875
		 -0.34503645 0.10241732 0.41928875;
	setAttr -s 16 ".ed[0:15]"  0 1 0 0 11 0 1 10 0 2 3 0 4 2 0 5 3 0 4 5 1
		 6 4 0 7 5 0 6 7 1 8 6 0 9 7 0 8 9 1 10 9 0 11 8 0 10 11 1;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 6 5 -4 -5
		mu 0 4 4 5 3 2
		f 4 9 8 -7 -8
		mu 0 4 6 7 5 4
		f 4 12 11 -10 -11
		mu 0 4 8 9 7 6
		f 4 0 2 15 -2
		mu 0 4 0 1 10 11
		f 4 -16 13 -13 -15
		mu 0 4 11 10 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pPlane6";
	rename -uid "730091B2-49C0-8CA1-A02A-FE8DABEDF1FC";
	setAttr ".t" -type "double3" 0.86936065454953648 -3.7449386524498789 -0.37159022822236532 ;
	setAttr ".r" -type "double3" -5.6492302702322545 113.9150840789361 0.56220753225187203 ;
	setAttr ".s" -type "double3" 1.2359366369259182 1.6149042083840306 1.8913439230271303 ;
createNode mesh -n "pPlaneShape6" -p "pPlane6";
	rename -uid "419A0F46-4650-3443-8CF5-E38D2543860D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.23447276328631117 0.82649298219270206 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape5" -p "pPlane6";
	rename -uid "0C3271AF-4A92-932A-CEAE-109B53411165";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.080711223185062408 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0 0 1 0 0 1 1 1 0
		 0.73750263 1 0.73750263 0 0.35097235 1 0.35097235 0 0.17081477 1 0.17081477 1 0.080711223
		 0 0.080711223;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt[10:11]" -type "float3"  0.070992626 0 0 -0.070992626 
		0 0;
	setAttr -s 12 ".vt[0:11]"  -0.20622614 -1.110223e-016 0.5 0.20622614 -1.110223e-016 0.5
		 -0.084332794 1.110223e-016 -0.5 0.084332794 1.110223e-016 -0.5 -0.34489086 0.18595053 -0.23750263
		 0.34489086 0.18595053 -0.23750263 -0.46862853 0.29710406 0.14902765 0.46862853 0.29710406 0.14902765
		 -0.5 0.21675289 0.32918522 0.5 0.21675289 0.32918522 0.34503645 0.10241732 0.41928875
		 -0.34503645 0.10241732 0.41928875;
	setAttr -s 16 ".ed[0:15]"  0 1 0 0 11 0 1 10 0 2 3 0 4 2 0 5 3 0 4 5 1
		 6 4 0 7 5 0 6 7 1 8 6 0 9 7 0 8 9 1 10 9 0 11 8 0 10 11 1;
	setAttr -s 5 -ch 20 ".fc[0:4]" -type "polyFaces" 
		f 4 6 5 -4 -5
		mu 0 4 4 5 3 2
		f 4 9 8 -7 -8
		mu 0 4 6 7 5 4
		f 4 12 11 -10 -11
		mu 0 4 8 9 7 6
		f 4 0 2 15 -2
		mu 0 4 0 1 10 11
		f 4 -16 13 -13 -15
		mu 0 4 11 10 9 8;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "EA27767A-45FA-7270-F47B-148110AE9808";
	setAttr -s 4 ".lnk";
	setAttr -s 4 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "84FEF444-473F-6B3E-D72F-F9A3A6D4DEF8";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "FD15A697-4E1A-3F6C-DAE7-9AB77CA05675";
createNode displayLayerManager -n "layerManager";
	rename -uid "BDA76A6B-4212-8C10-705C-98ACF2C970AA";
createNode displayLayer -n "defaultLayer";
	rename -uid "E1EE1C6B-40C8-2B50-FA77-538EA0A516EA";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "A3B8ABBB-4777-21C7-E5FC-54B8D25AC2E1";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "8FB7EABF-49EE-40B3-F217-F29503877E4F";
	setAttr ".g" yes;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "7E94F168-4316-44AF-5CFF-BEAD17888564";
	setAttr ".sa" 3;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "4D0531BC-4852-B1C3-FCF4-D087C0D03EEE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[6:8]";
	setAttr ".ix" -type "matrix" 0.018825299925258603 0 0 0 0 1 0 0 0 0 0.018825299925258603 0
		 0 0.97696767363213255 0 1;
	setAttr ".wt" 0.19699080288410187;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "3D99858F-4840-7CF3-E642-129812027A93";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[15:16]" "e[18]";
	setAttr ".ix" -type "matrix" 0.018825299925258603 0 0 0 0 1 0 0 0 0 0.018825299925258603 0
		 0 0.97696767363213255 0 1;
	setAttr ".wt" 0.23744827508926392;
	setAttr ".re" 18;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "3FC4A571-41CA-5996-E366-A6905F57B1F7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[21:22]" "e[24]";
	setAttr ".ix" -type "matrix" 0.018825299925258603 0 0 0 0 1 0 0 0 0 0.018825299925258603 0
		 0 0.97696767363213255 0 1;
	setAttr ".wt" 0.19412107765674591;
	setAttr ".re" 21;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "F1CD8B57-4246-8464-DC87-9983D8B68F7B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[27:28]" "e[30]";
	setAttr ".ix" -type "matrix" 0.018825299925258603 0 0 0 0 1 0 0 0 0 0.018825299925258603 0
		 0 0.97696767363213255 0 1;
	setAttr ".wt" 0.40186440944671631;
	setAttr ".re" 27;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "F541EB87-487B-AD2A-EB6D-1C96AAA443E6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[33:34]" "e[36]";
	setAttr ".ix" -type "matrix" 0.018825299925258603 0 0 0 0 1 0 0 0 0 0.018825299925258603 0
		 0 0.97696767363213255 0 1;
	setAttr ".wt" 0.4023527204990387;
	setAttr ".re" 33;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "BC3B71B2-473C-C9AB-5A99-15AE599EAB9F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[27:28]" "e[30]";
	setAttr ".ix" -type "matrix" 0.018825299925258603 0 0 0 0 1 0 0 0 0 0.018825299925258603 0
		 0 0.97696767363213255 0 1;
	setAttr ".wt" 0.38349080085754395;
	setAttr ".re" 28;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyCube -n "polyCube1";
	rename -uid "B50BA865-4CF5-9BD8-3664-7182ADC0845D";
	setAttr ".cuv" 4;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "DD3E6473-426C-59F0-CF97-75974093E3B4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[6:7]" "e[10:11]";
	setAttr ".ix" -type "matrix" 0.25469879216096702 0 0 0 0 0.035045201919064572 0 0
		 0 0 0.66307714702454479 0 0 2.0591992148577152 0.3741389583633431 1;
	setAttr ".wt" 0.80808180570602417;
	setAttr ".dr" no;
	setAttr ".re" 6;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing8";
	rename -uid "237B703A-464D-83F8-1CA3-32ACB34E2EEF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[6:7]" "e[13]" "e[15]";
	setAttr ".ix" -type "matrix" 0.25469879216096702 0 0 0 0 0.035045201919064572 0 0
		 0 0 0.66307714702454479 0 0 2.0591992148577152 0.3741389583633431 1;
	setAttr ".wt" 0.43353214859962463;
	setAttr ".re" 15;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak1";
	rename -uid "C77580EE-46A0-12C4-09D3-1AB333B35B6A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[8:11]" -type "float3"  -0.32508078 0 0 -0.32508078
		 0 0 0.32508078 0 0 0.32508078 0 0;
createNode polySplitRing -n "polySplitRing9";
	rename -uid "CDF26713-436D-3A44-3658-159B154B05EA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[6:7]" "e[20:21]";
	setAttr ".ix" -type "matrix" 0.25469879216096702 0 0 0 0 0.035045201919064572 0 0
		 0 0 0.66307714702454479 0 0 2.0591992148577152 0.3741389583633431 1;
	setAttr ".wt" 0.3359467089176178;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing10";
	rename -uid "13408949-4E6B-95D2-3A86-D6AE0D009A7E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[0:3]" "e[16]" "e[19]" "e[22]" "e[26]" "e[30]" "e[34]";
	setAttr ".ix" -type "matrix" 0.25469879216096702 0 0 0 0 0.010588080556456533 0 0
		 0 0 0.66307714702454479 0 0 2.0591992148577152 0.3741389583633431 1;
	setAttr ".wt" 0.25253617763519287;
	setAttr ".re" 1;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak2";
	rename -uid "8D608ABC-4D5B-FB41-13BE-6B8E353371BA";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk";
	setAttr ".tk[0]" -type "float3" 0 15.205384 0 ;
	setAttr ".tk[1]" -type "float3" 0 15.205384 0 ;
	setAttr ".tk[2]" -type "float3" 0 15.205384 0 ;
	setAttr ".tk[3]" -type "float3" 0 15.205384 0 ;
	setAttr ".tk[12]" -type "float3" 0.63669705 3.8420296 0 ;
	setAttr ".tk[13]" -type "float3" -0.63669705 3.8420296 0 ;
	setAttr ".tk[14]" -type "float3" -0.63669705 3.8420296 0 ;
	setAttr ".tk[15]" -type "float3" 0.63669705 3.8420296 0 ;
	setAttr ".tk[16]" -type "float3" 0.61617887 9.984417 0 ;
	setAttr ".tk[17]" -type "float3" -0.61617887 9.984417 0 ;
	setAttr ".tk[18]" -type "float3" -0.61617887 9.984417 0 ;
	setAttr ".tk[19]" -type "float3" 0.61617887 9.984417 0 ;
createNode polySplitRing -n "polySplitRing11";
	rename -uid "B9A7F1BB-4ED7-9837-F7C1-AF9C357301D2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[19]" "e[22]" "e[30]" "e[36]" "e[39]" "e[43]" "e[45]" "e[47]" "e[51]" "e[53]";
	setAttr ".ix" -type "matrix" 0.25469879216096702 0 0 0 0 0.010588080556456533 0 0
		 0 0 0.66307714702454479 0 0 2.0591992148577152 0.3741389583633431 1;
	setAttr ".wt" 0.59120774269104004;
	setAttr ".dr" no;
	setAttr ".re" 36;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak3";
	rename -uid "8AE4CA4F-44C5-0BAC-3FBC-FA95FA3B5414";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk[20:29]" -type "float3"  -5.5511151e-017 -2.80054474
		 0 0 -2.80054474 0 0 -2.80054474 0 -5.5511151e-017 -2.80054474 0 -5.5511151e-017 -2.80054474
		 0 -5.5511151e-017 -2.80054474 0 -5.5511151e-017 -2.80054474 0 0 -2.80054474 0 0 -2.80054474
		 0 -5.5511151e-017 -2.80054474 0;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "28769631-49FD-5A7C-9C1E-CAB631F7C85D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 0.25469879216096702 0 0 0 0 0.010588080556456533 0 0
		 0 0 0.66307714702454479 0 0 2.0591992148577152 0.3741389583633431 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak4";
	rename -uid "A2C1D024-4503-8E15-F53E-4C9A48820099";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk";
	setAttr ".tk[20]" -type "float3" 0 2.4715297 0.030245136 ;
	setAttr ".tk[29]" -type "float3" 0 2.4715297 0.030245136 ;
	setAttr ".tk[30]" -type "float3" -1.110223e-016 -0.44430941 0.030245136 ;
	setAttr ".tk[31]" -type "float3" -1.110223e-016 -2.9158392 0 ;
	setAttr ".tk[32]" -type "float3" 0 -2.9158392 0 ;
	setAttr ".tk[33]" -type "float3" -1.110223e-016 -2.9158392 0 ;
	setAttr ".tk[34]" -type "float3" -1.110223e-016 -2.9158392 0 ;
	setAttr ".tk[35]" -type "float3" -1.110223e-016 -2.9158392 0 ;
	setAttr ".tk[36]" -type "float3" -1.110223e-016 -2.9158392 0 ;
	setAttr ".tk[37]" -type "float3" 0 -2.9158392 0 ;
	setAttr ".tk[38]" -type "float3" -1.110223e-016 -2.9158392 0 ;
	setAttr ".tk[39]" -type "float3" -1.110223e-016 -0.44430941 0.030245136 ;
createNode polySphere -n "polySphere1";
	rename -uid "5CD405AD-4335-00CB-6826-B7802A23E6A4";
	setAttr ".sa" 4;
	setAttr ".sh" 4;
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "9502C793-44B1-F2F3-9E12-CAA0AA7040B5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 0.11383156635406552 0 0 0 0 0.11383156635406552 0 0
		 0 0 0.11383156635406552 0 0 2.0336845956301675 0 1;
	setAttr ".a" 180;
createNode polyPlane -n "polyPlane1";
	rename -uid "930CB5B2-4943-F7CD-CE8A-7D83F5C5DA9D";
	setAttr ".sw" 1;
	setAttr ".sh" 1;
	setAttr ".cuv" 2;
createNode polySplitRing -n "polySplitRing12";
	rename -uid "74CFD344-4789-1622-FD5D-2FB6E840F96C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[1:2]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1.3926119855565968 0 0 -3.8768177324462192 0.71545458688056418 1;
	setAttr ".wt" 0.73750263452529907;
	setAttr ".dr" no;
	setAttr ".re" 1;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing13";
	rename -uid "794F1BB0-4DC8-D5DC-7B20-0983BE8807F1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[1:2]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1.3926119855565968 0 0 -3.8768177324462192 0.71545458688056418 1;
	setAttr ".wt" 0.47589302062988281;
	setAttr ".dr" no;
	setAttr ".re" 1;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak5";
	rename -uid "E468BD06-4149-9612-822E-AE9C84417BBE";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[4:5]" -type "float3"  0 0.18595053 0 0 0.18595053
		 0;
createNode polySplitRing -n "polySplitRing14";
	rename -uid "F881DB0B-448D-5065-D5A1-EC8804E2F390";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[1:2]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1.3926119855565968 0 0 -3.8768177324462192 0.71545458688056418 1;
	setAttr ".wt" 0.48669010400772095;
	setAttr ".dr" no;
	setAttr ".re" 1;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak6";
	rename -uid "50D0FB6F-4F52-1A77-0A82-B49E2FF80A0D";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[6:7]" -type "float3"  0 0.2086115 0 0 0.2086115
		 0;
createNode polySplitRing -n "polySplitRing15";
	rename -uid "7E721CA5-47B5-FDA0-F7D4-A8BD6EFDD928";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[1:2]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1.3926119855565968 0 0 -3.8768177324462192 0.71545458688056418 1;
	setAttr ".wt" 0.4725072979927063;
	setAttr ".re" 2;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak7";
	rename -uid "F4EEF60B-4458-5EF0-2525-049857F01A6D";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk[0:9]" -type "float3"  0.29377386 0 0 -0.29377386
		 0 0 0.41566721 0 0 -0.41566721 0 0 0.15510914 0 0 -0.15510914 0 0 0.031371474 0 0
		 -0.031371474 0 0 0 0.072155289 0 0 0.072155289 0;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "7FBD0E2C-4281-0188-4F1A-199FD63835BF";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 601\n            -height 811\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n"
		+ "            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n"
		+ "            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n"
		+ "            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n"
		+ "            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n"
		+ "                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n"
		+ "                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 601\\n    -height 811\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 601\\n    -height 811\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "8AD0AC8D-40F4-37EC-004E-3DB5371B40B9";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyMapDel -n "polyMapDel1";
	rename -uid "BA62E360-45BC-0360-34A8-1B9619101F58";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyMapDel -n "polyMapDel2";
	rename -uid "F56E42F0-4431-F073-0551-12B9243BC685";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyMapDel -n "polyMapDel3";
	rename -uid "75A0F50E-4535-24C0-A7C4-0FBB220854DD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyMapDel -n "polyMapDel4";
	rename -uid "60A7FC37-4B1D-B65E-DB39-ABB9C623BB91";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyMapDel -n "polyMapDel5";
	rename -uid "05987889-4152-D318-0A8E-D884C556B1A9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyTweak -n "polyTweak8";
	rename -uid "18A13CF8-4159-A49C-5EBE-A885BC9A2DD8";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk";
	setAttr ".tk[8]" -type "float3" -0.73946369 0 0.53338927 ;
	setAttr ".tk[9]" -type "float3" -0.73946369 0 0.53338927 ;
	setAttr ".tk[10]" -type "float3" -0.73946369 0 0.53338927 ;
	setAttr ".tk[11]" -type "float3" -1.4097549 0 0.56782258 ;
	setAttr ".tk[12]" -type "float3" -1.4097549 0 0.56782258 ;
	setAttr ".tk[13]" -type "float3" -1.4097549 0 0.56782258 ;
	setAttr ".tk[20]" -type "float3" -0.61609817 0 -0.94275022 ;
	setAttr ".tk[21]" -type "float3" -0.61609817 0 -0.94275022 ;
	setAttr ".tk[22]" -type "float3" -0.61609817 0 -0.94275022 ;
	setAttr ".tk[23]" -type "float3" 0.6763736 0 0.021255895 ;
	setAttr ".tk[24]" -type "float3" 0.6763736 0 0.021255895 ;
	setAttr ".tk[25]" -type "float3" 0.6763736 0 0.021255895 ;
createNode polyMapDel -n "polyMapDel6";
	rename -uid "38D3339B-43F7-8EC6-65EB-D79E31AD3069";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyTweak -n "polyTweak9";
	rename -uid "74C911F9-4E40-24D9-F5DF-B994DB86F45D";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[10:11]" -type "float3"  0.070992626 0 0 -0.070992626
		 0 0;
createNode polyMapDel -n "polyMapDel7";
	rename -uid "900BAB8C-4143-7E23-1E4D-799AAD3E4483";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyMapDel -n "polyMapDel8";
	rename -uid "2DF25E2A-40B5-DB3D-52D7-DFAEFD40E4C3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyMapDel -n "polyMapDel9";
	rename -uid "DF52B4BC-4B74-5D6E-4A77-96A3C7264354";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyMapDel -n "polyMapDel10";
	rename -uid "D26A7058-4013-CD92-6098-2BB131AA60F5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyMapDel -n "polyMapDel11";
	rename -uid "8BDD5F0F-4A9F-0B3A-881A-9B9F023AE38F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyMapDel -n "polyMapDel12";
	rename -uid "D644E962-46D9-BCBF-C533-8C90EE6830DC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyMapDel -n "polyMapDel13";
	rename -uid "4ECC60F9-4D3C-344F-48DC-3ABB20154A71";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyPlanarProj -n "polyPlanarProj1";
	rename -uid "C419D428-486C-9090-D274-0EB0CD3E9A1F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:4]";
	setAttr ".ix" -type "matrix" -0.50100267397616394 -0.0049161826512724169 -1.1298280941420347 0
		 -0.16108192914997063 1.6055576422363931 0.064442779379002349 0 1.7186599322345391 0.20305373625455508 -0.76299348142450785 0
		 0.86936065454953648 -3.7449386524498789 -0.37159022822236532 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0.8998950719833374 -3.5411181449890137 -0.48121447861194611 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 1.8642306327819824 1.8642306327819824 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "25465DE0-499C-6DB0-8544-6B8BF3BBF580";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk[0:11]" -type "float2" -0.050253086 0.86491168 0.22984138
		 0.27121446 0.44487432 0.64803255 0.37638545 0.79320365 -0.59291255 0.73823148 -0.21232854
		 -0.068469279 -0.85139465 0.64960939 -0.44533247 -0.21109435 -0.98237067 0.27974781
		 -0.81488955 -0.075251333 -0.61144406 -0.200542 -0.94931138 0.51561373;
createNode polyPlanarProj -n "polyPlanarProj2";
	rename -uid "24491766-4A26-FEDE-07AA-828DC75F6DE9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:4]";
	setAttr ".ix" -type "matrix" 0.41227700612701446 0 -0.91105854379339735 0 0 1 0 0
		 1.2687510476304249 0 0.57414190010187083 0 0.61671623785528884 -3.8768177324462192 0.40834957345393197 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0.64184316992759705 -3.728265643119812 0.50532573461532593 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 1.3885417580604553 1.3885417580604553 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj3";
	rename -uid "AF5A106C-492B-AF84-78C6-83B5E8B10003";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1.3926119855565968 0 0 -3.8768177324462192 0.71545458688056418 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 -3.728265643119812 0.7154545783996582 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 1.3926119804382324 1.3926119804382324 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj4";
	rename -uid "78359B5B-45CB-E214-22F5-B18EE269D945";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:4]";
	setAttr ".ix" -type "matrix" 0.037024862004361725 0 0.99931434473520786 0 0 1 0 0
		 -1.3916571338168873 0 0.05156126659085318 0 -0.64630638259075845 -3.876817732446221 0.012963843489705695 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.64856293797492981 -3.728265643119812 0.029937043786048889 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 1.4024149775505066 1.4024149775505066 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj5";
	rename -uid "BBEEA2C7-4E59-270C-2C10-F8BFFAC23EBF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:4]";
	setAttr ".ix" -type "matrix" -0.8124490777775335 -0.0048636558473765133 0.91376949840725485 0
		 0.10984615493956162 1.4072553745217726 0.10515651187371347 0 -1.1408417399661877 0.16478148825372257 -1.0134660663230215 0
		 -0.57563339851546835 -3.7449386524498793 -0.55600500280440546 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.65871208906173706 -3.5638703107833862 -0.6612679660320282 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 1.4440313577651978 1.4440313577651978 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj6";
	rename -uid "2F0C869E-43EB-DACA-DBA8-D5B97544A519";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:4]";
	setAttr ".ix" -type "matrix" -0.9438869610184486 0 -0.33026868579893809 0 0 1 0 0
		 0.45993613029762698 0 -1.3144682949248838 0 0.29899637286863912 -3.876817732446221 -0.61238961603180719 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0.39328314363956451 -3.728265643119812 -0.63251838088035583 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 1.4104307293891907 1.4104307293891907 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyCylProj -n "polyCylProj1";
	rename -uid "89CFCADF-46DC-ACCF-A6A9-C0A5D8CAE14B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:26]";
	setAttr ".ix" -type "matrix" 0.023829963468087335 0 0 0 0 2.985100085073205 0 0 0 0 0.023829963468087335 0
		 0 -0.9660732008477898 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.0027807354927062988 -0.96607327461242676 -0.0044672600924968719 ;
	setAttr ".ps" -type "double2" 180 0.2826834332968054 ;
	setAttr ".r" 0.085457310080528259;
createNode polyMapDel -n "polyMapDel14";
	rename -uid "1286CC26-43AF-1CD5-35B1-E68207AF6127";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:26]";
createNode deleteComponent -n "deleteComponent1";
	rename -uid "45C33094-4E93-2057-B582-C9B6E7CE6947";
	setAttr ".dc" -type "componentList" 1 "f[3:5]";
createNode polyCylProj -n "polyCylProj2";
	rename -uid "0D6D41B2-46DE-01BE-217F-6E892EF0E31F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "f[0:2]" "f[6:23]";
	setAttr ".ix" -type "matrix" 0.023829963468087335 0 0 0 0 2.985100085073205 0 0 0 0 0.023829963468087335 0
		 0 -0.9660732008477898 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.0027807336300611496 -0.96607339382171631 -0.0044672600924968719 ;
	setAttr ".ps" -type "double2" 180 5.9701998233795166 ;
	setAttr ".r" 0.085457306355237961;
createNode polyMapCut -n "polyMapCut1";
	rename -uid "27857327-4BEF-42BA-FBC2-87A5563E4D80";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[1:4]" "e[8]" "e[31:35]" "e[37]" "e[42:43]";
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "C142BB98-43CA-C3E5-EB44-17B40F6E19CB";
	setAttr ".uopa" yes;
	setAttr -s 46 ".uvtk[0:45]" -type "float2" 0.42097428 0.29460433 -0.30315566
		 0.30011129 -0.22010666 0.10723144 0.11840126 0.077708364 -0.56151903 0.11584187 -0.15116063
		 -0.047624111 0.056210037 -0.22295867 -0.29422349 0.043450892 0.3362934 -0.15179265
		 -0.42085236 -0.023516834 -0.41934282 0.020350933 0.21463025 -0.2154007 -0.48054826
		 -0.094263434 -0.78803819 -0.034555733 0.80385804 -0.42496467 1.039212942 -0.36075521
		 0.92854172 -0.34840578 -0.45230532 -0.049555659 1.099780321 -0.47031385 -0.85871172
		 0.30456579 -0.85347617 0 -1.034105897 -0.084199496 -1.98603368 0.027224928 -1.48903918
		 -0.47744486 -0.26878113 -0.028309643 -1.99923396 -0.5065313 -2.11183143 -0.5529775
		 -0.60694379 -0.093602717 -0.74522406 -0.39460903 -1.78600419 0.034171343 -2.18533063
		 0.005467236 -2.51203275 -0.090380788 -0.30151606 -0.0041571856 -1.090132236 0.175574
		 0.60913897 -0.54687274 -1.15092349 -0.086739898 -1.017831326 -0.42861649 -0.015913188
		 0.027361751 -0.34640801 -0.2163628 -0.50525498 0.10235691 -1.05481863 0.20952398
		 -0.76770049 -0.14040554 0.54857171 -0.43731406 -0.1379706 -0.069172382 -0.10771358
		 0 -0.39439398 0.11142441;
createNode polyMapDel -n "polyMapDel15";
	rename -uid "67FA6A27-4166-81DC-1B14-16B9F920D461";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "f[0:2]" "f[6:23]";
createNode deleteComponent -n "deleteComponent2";
	rename -uid "EBBF2590-400C-AD9A-A551-65B524AF3352";
	setAttr ".dc" -type "componentList" 1 "f[3:5]";
createNode polyCylProj -n "polyCylProj3";
	rename -uid "66814961-44AD-08A5-2028-5AB549FBBCBD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:20]";
	setAttr ".ix" -type "matrix" 0.023829963468087335 0 0 0 0 2.985100085073205 0 0 0 0 0.023829963468087335 0
		 0 -0.9660732008477898 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.0027807336300611496 -0.96607339382171631 -0.0044672600924968719 ;
	setAttr ".ps" -type "double2" 180 1.1248603305351434 ;
	setAttr ".r" 0.085457306355237961;
createNode polyMapDel -n "polyMapDel16";
	rename -uid "F01CC735-4653-001D-3AD1-4F9C1BB130FA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:20]";
createNode polyCylProj -n "polyCylProj4";
	rename -uid "F8B5EDCA-4D56-E662-0149-A7B36211257D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:20]";
	setAttr ".ix" -type "matrix" 0.023829963468087335 0 0 0 0 2.985100085073205 0 0 0 0 0.023829963468087335 0
		 0 -0.9660732008477898 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.0027807336300611496 -0.96607339382171631 -0.0044672600924968719 ;
	setAttr ".ps" -type "double2" 224.94971478903608 1.8094900422067663 ;
	setAttr ".r" 1.5720701606726937;
createNode polyMapDel -n "polyMapDel17";
	rename -uid "DA79B8E3-4513-3D69-6670-C3B0A7ABE7B9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:20]";
createNode polyPlanarProj -n "polyPlanarProj7";
	rename -uid "743BC19C-4BBC-5AD1-3B7A-18B26907045A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:20]";
	setAttr ".ix" -type "matrix" 0.023829963468087335 0 0 0 0 2.985100085073205 0 0 0 0 0.023829963468087335 0
		 0 -0.9660732008477898 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.0027807336300611496 -0.96607339382171631 -0.0044672600924968719 ;
	setAttr ".ro" -type "double3" 0 90 0 ;
	setAttr ".ps" -type "double2" 5.9701998233795166 5.9701998233795166 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "529412A3-4705-E888-AE41-1397140F6EF1";
	setAttr ".uopa" yes;
	setAttr -s 24 ".uvtk[0:23]" -type "float2" -0.49179742 0.010243207 -0.49165571
		 0.010243207 -0.49161217 0.0062074959 -0.49175373 0.0062074959 -0.49172661 0.010243207
		 -0.49168298 0.0062074959 -0.49160931 0.0023012459 -0.49175099 0.0023012459 -0.49168015
		 0.0023012459 -0.49172661 -0.00013393164 -0.49165571 -0.00013393164 -0.49179742 -0.00013393164
		 -0.49172482 -0.0016918182 -0.49165407 -0.0016918182 -0.49179566 -0.0016918182 -0.49165571
		 -0.0041966438 -0.49172661 -0.0041966438 -0.49180362 -0.0066297054 -0.49173284 -0.0066297054
		 -0.49179742 -0.0041966438 -0.49187446 -0.0066297054 -0.49172661 -0.010243416 -0.49165571
		 -0.010243416 -0.49179742 -0.010243416;
createNode polyPlanarProj -n "polyPlanarProj8";
	rename -uid "8516C0A8-47F6-CDA7-CFA1-87ADE0FE7B83";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:37]";
	setAttr ".ix" -type "matrix" -0.52640860228129127 6.4466460982741542e-017 0 0 1.2966658962578819e-018 0.010588080556456533 1.2966658962578819e-018 0
		 9.9445643790957592e-033 8.1203530569136764e-017 -0.66307714702454479 0 0 1.7193677656661643 0.31791489366613795 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 1.8455778956413269 0.34679609537124634 ;
	setAttr ".ro" -type "double3" -82.004165001377629 0 0 ;
	setAttr ".ps" -type "double2" 1.3059983253479004 1.3059983253479004 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj9";
	rename -uid "5D0C06C4-45BC-52FB-5305-B2B996D7836B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:37]";
	setAttr ".ix" -type "matrix" 0.52640860228129127 -6.4466460982741542e-017 -6.4466460982741542e-017 0
		 1.2966658962578804e-018 0.010588080556456533 -1.0700770552549068e-017 0 8.1203530569136863e-017 6.7013434315266457e-016 0.66307714702454479 0
		 0 1.7193677656661643 -0.34667617040607779 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 1.8455778956413269 -0.37555736303329468 ;
	setAttr ".ro" -type "double3" -99.897013760838917 0 0 ;
	setAttr ".ps" -type "double2" 1.3059983253479004 1.3059983253479004 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj10";
	rename -uid "55C5C860-4CBF-9E97-8441-2D8C0F866DEE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:37]";
	setAttr ".ix" -type "matrix" 0.25469879216096702 0 0 0 0 0.010303838718039231 -0.0024368745438878292 0
		 0 0.15260894659821567 0.6452765394188702 0 0 2.0474123388854939 0.31791489366613795 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 2.1054418087005615 0.31108260154724121 ;
	setAttr ".ro" -type "double3" -119.5213962332092 0 0 ;
	setAttr ".ps" -type "double2" 0.67283517122268677 0.67283517122268677 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj11";
	rename -uid "FBEA4B9E-4419-9FE1-E2D9-2CB77BF5DF4F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:37]";
	setAttr ".ix" -type "matrix" -0.14486159294140782 0 -0.20949127337131088 0 -0.0020043438247738651 0.01030383871803923 0.0013859882303755137 0
		 0.5307437965157672 0.15260894659821642 -0.36700522446475081 0 0.25689184388747011 2.0474123388854948 -0.08812947138459265 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0.25518792867660522 2.1054418087005615 -0.12401323020458221 ;
	setAttr ".ro" -type "double3" -71.345658279116634 -3.9889901090287938 18.788176961218994 ;
	setAttr ".ps" -type "double2" 0.6742018461227417 0.6742018461227417 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj12";
	rename -uid "1D623E28-400D-EF48-D266-4A9246DB7D33";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:37]";
	setAttr ".ix" -type "matrix" -0.085372855008375437 0 0.23996447727939729 0 0.0022959014495441787 0.01030383871803923 0.00081681948839968104 0
		 -0.60794731756893483 0.15260894659821647 -0.2162911726934994 0 -0.29089250619706664 2.0474123388854948 -0.080917510413193006 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.27343747019767761 2.1054418087005615 -0.10014793276786804 ;
	setAttr ".ro" -type "double3" -74.215995449590181 0 -23.708530713640545 ;
	setAttr ".ps" -type "double2" 0.66119027137756348 0.66119027137756348 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj13";
	rename -uid "816A5B7E-4ACA-2BEB-138A-03B9E7B45343";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[12:15]";
	setAttr ".ix" -type "matrix" 0.11485416037514896 0 0 0 0 0.09567607042722906 0 0
		 0 0 0.12215182463898212 0 0 2.0336845956301675 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 2.1153491735458374 0 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 0.17274875938892365 0.17274875938892365 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj14";
	rename -uid "A864F3BC-4959-24A3-60AE-A8A3F2D64D43";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[8:11]";
	setAttr ".ix" -type "matrix" 0.11485416037514896 0 0 0 0 0.09567607042722906 0 0
		 0 0 0.12215182463898212 0 0 2.0336845956301675 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 1.9520196914672852 0 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 0.17274875938892365 0.17274875938892365 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj15";
	rename -uid "93811CE4-4DFE-C8AD-DF0D-5BBA37680C63";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "f[1]" "f[3]" "f[5]" "f[7]";
	setAttr ".ix" -type "matrix" 0.11485416037514896 0 0 0 0 0.09567607042722906 0 0
		 0 0 0.12215182463898212 0 0 2.0336845956301675 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 2.0336843729019165 0 ;
	setAttr ".ro" -type "double3" 0 132.07197267794842 0 ;
	setAttr ".ps" -type "double2" 0.24430364370346069 0.24430364370346069 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj16";
	rename -uid "837958E2-4990-0CCD-A4C7-9D9181B18940";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "f[0]" "f[2]" "f[4]" "f[6]";
	setAttr ".ix" -type "matrix" 0.11485416037514896 0 0 0 0 0.09567607042722906 0 0
		 0 0 0.12215182463898212 0 0 2.0336845956301675 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 2.033684253692627 0 ;
	setAttr ".ro" -type "double3" 0 47.415135449138809 0 ;
	setAttr ".ps" -type "double2" 0.24430364370346069 0.24430364370346069 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV4";
	rename -uid "8CC81375-4FE6-46D0-0605-6A88FC884A93";
	setAttr ".uopa" yes;
	setAttr -s 34 ".uvtk[0:33]" -type "float2" -0.25007123 -0.46838206 -0.029871285
		 -0.234191 -0.25007117 -0.234191 -0.25007117 5.9604645e-008 -0.47027105 -0.234191
		 0.47027099 -0.23419106 0.25007105 -0.46838212 0.25007111 -0.23419106 0.25007111 5.5511151e-017
		 0.029871225 -0.23419106 0.2251032 0.3261748 0.45235938 0.3261748 0.50327617 0.19646984
		 0.18188739 0.19646984 0.091335624 0.4772172 -0.13592058 0.4772172 -0.18683738 0.34751225
		 0.13455138 0.34751225 0.45235938 0.066763103 0.2251032 0.066763103 -0.13592058 0.2178055
		 0.091335624 0.2178055 -0.45280367 0.32580602 -0.22551522 0.32580602 -0.18187267 0.19610107
		 -0.50330704 0.19610107 -0.043206319 0.32798457 -0.77839804 0.062175691 -0.76787817
		 -0.40840966 0.27184004 -0.032499105 -0.22551522 0.066393852 -0.45280367 0.066393852
		 -0.47502124 -0.77692407 0.26017052 -0.51111525;
createNode polyMapSewMove -n "polyMapSewMove1";
	rename -uid "B92DE727-497F-B05A-55B6-20876D523A49";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[10]";
createNode polyTweakUV -n "polyTweakUV5";
	rename -uid "A5D2BD61-4F4D-88F4-A843-D4B2FD41C3E6";
	setAttr ".uopa" yes;
	setAttr -s 6 ".uvtk";
	setAttr ".uvtk[22]" -type "float2" 0.19263309 -0.28307393 ;
	setAttr ".uvtk[23]" -type "float2" 0.70053643 -0.017265022 ;
	setAttr ".uvtk[24]" -type "float2" 0.94974816 -0.25606737 ;
	setAttr ".uvtk[25]" -type "float2" 0.23146433 -0.63197798 ;
	setAttr ".uvtk[30]" -type "float2" 1.0039134 -0.59695274 ;
	setAttr ".uvtk[31]" -type "float2" 0.49601001 -0.86276162 ;
createNode polyMapSewMove -n "polyMapSewMove2";
	rename -uid "C134C3FE-4217-2B3A-A8E9-7CAEB9459BC4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0]";
createNode polyTweakUV -n "polyTweakUV6";
	rename -uid "5D64F444-45D6-7D30-850D-F2B3746237E4";
	setAttr ".uopa" yes;
	setAttr -s 6 ".uvtk";
	setAttr ".uvtk[10]" -type "float2" -0.44771087 -0.28344372 ;
	setAttr ".uvtk[11]" -type "float2" 0.060155749 -0.54925257 ;
	setAttr ".uvtk[12]" -type "float2" 0.022234917 -0.89866877 ;
	setAttr ".uvtk[13]" -type "float2" -0.69599706 -0.52275825 ;
	setAttr ".uvtk[18]" -type "float2" -0.24326366 -1.1289797 ;
	setAttr ".uvtk[19]" -type "float2" -0.75113028 -0.86317098 ;
createNode polyMapSewMove -n "polyMapSewMove3";
	rename -uid "F8E6E52E-4CE4-26CF-3CCF-9BB9862D33F1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[1]";
createNode polyTweakUV -n "polyTweakUV7";
	rename -uid "663DCAC2-49E7-D61A-C0C5-0894E1379A1D";
	setAttr ".uopa" yes;
	setAttr -s 6 ".uvtk";
	setAttr ".uvtk[12]" -type "float2" 0.43469164 -0.14919353 ;
	setAttr ".uvtk[13]" -type "float2" -0.073174998 0.11661541 ;
	setAttr ".uvtk[14]" -type "float2" -0.33867136 -0.11369163 ;
	setAttr ".uvtk[15]" -type "float2" 0.37956044 -0.48960221 ;
	setAttr ".uvtk[18]" -type "float2" -0.37659454 -0.46311182 ;
	setAttr ".uvtk[19]" -type "float2" 0.13127211 -0.7289207 ;
createNode polyMapSewMove -n "polyMapSewMove4";
	rename -uid "1CA4EA8A-4ECF-796B-D8F9-5F98ABAE6A5D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[11]";
createNode polyFlipUV -n "polyFlipUV1";
	rename -uid "AE88B82A-4729-50B2-1232-90B921512B58";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[0:1]" "f[4:5]" "f[8:11]";
	setAttr ".ix" -type "matrix" 0.11485416037514896 0 0 0 0 0.09567607042722906 0 0
		 0 0 0.12215182463898212 0 0 2.0336845956301675 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.59836142510000001;
	setAttr ".pv" 0.26578892770000001;
createNode polyTweakUV -n "polyTweakUV8";
	rename -uid "C9C1D615-4BC1-FB8B-98E2-BF91F88944E5";
	setAttr ".uopa" yes;
	setAttr -s 13 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" 0.5000999 0.2852523 ;
	setAttr ".uvtk[1]" -type "float2" 0.99995774 0.28525224 ;
	setAttr ".uvtk[2]" -type "float2" 0.76540554 0.53470755 ;
	setAttr ".uvtk[3]" -type "float2" 1.0307108 0.784163 ;
	setAttr ".uvtk[4]" -type "float2" 0.53085321 0.78416324 ;
	setAttr ".uvtk[12]" -type "float2" -0.03865099 0.81926799 ;
	setAttr ".uvtk[13]" -type "float2" -0.069404364 0.32035717 ;
	setAttr ".uvtk[14]" -type "float2" 0.20845556 0.19102362 ;
	setAttr ".uvtk[15]" -type "float2" 0.25194722 0.89659011 ;
	setAttr ".uvtk[20]" -type "float2" 1.0307111 1.3546679 ;
	setAttr ".uvtk[21]" -type "float2" 0.53085321 1.3546681 ;
	setAttr ".uvtk[22]" -type "float2" 0.43487352 1.0694181 ;
	setAttr ".uvtk[23]" -type "float2" 1.1417791 1.069418 ;
createNode polyMapSewMove -n "polyMapSewMove5";
	rename -uid "E8393D59-4F07-A896-30DF-2EB771AFB571";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[8]";
createNode polyTweakUV -n "polyTweakUV9";
	rename -uid "43713F36-4768-F360-A042-41BA190AB285";
	setAttr ".uopa" yes;
	setAttr -s 24 ".uvtk[0:23]" -type "float2" -0.23343009 -0.0078021288
		 -0.44105905 0.21301937 -0.45383328 0.0057839155 -0.67423624 0.019369841 -0.46660763
		 -0.2014519 -0.18902788 0.44999325 0.018601023 0.22917193 0.018601023 0.44999319 0.018601023
		 0.67081451 0.22622994 0.44999319 -0.060911871 0.8387925 -0.35454345 0.5265041 -0.24555752
		 -0.46762276 -0.012380105 -0.27397314 -0.070660986 -0.097501159 -0.40042353 -0.37136307
		 -0.23346525 0.90782177 -0.4410941 0.68700063 -0.35490996 0.37390816 -0.06127844 0.061619446
		 -0.92626762 -0.21760398 -0.71863884 -0.43842566 -0.55275667 -0.36234057 -0.84638798
		 -0.050051689;
createNode groupId -n "groupId1";
	rename -uid "9FA0387A-4931-CE87-085A-AC9BB42BEEFC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "A9F20EC2-4618-9706-007E-D8AF076647E5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:15]";
createNode polyTweakUV -n "polyTweakUV10";
	rename -uid "60978030-4B54-7CF7-184E-B1A9F93F36D8";
	setAttr ".uopa" yes;
	setAttr -s 40 ".uvtk[0:39]" -type "float2" -0.34774885 -0.18436372 -0.29729202
		 -0.19671655 -0.29729202 -0.19727552 -0.34774885 -0.1849227 -0.49569708 -0.064391196
		 -0.37051561 -0.070802957 -0.33508855 0.01917693 -0.42411667 0.025080264 -0.34774885
		 0.071517885 -0.29729202 0.098540246 -0.29729202 0.099116355 -0.34774885 0.072093993
		 -0.42411667 0.025656372 -0.33508855 0.019752979 -0.37051561 -0.070226848 -0.49569708
		 -0.063815087 -5.9604645e-008 -0.064391196 -5.9604645e-008 -0.063815087 -0.071580298
		 0.025656372 -0.071580298 0.025080264 -0.14794825 0.072093993 -0.14794825 0.071517885
		 -0.48322219 -0.14311886 -0.36434141 -0.14655226 -0.48322219 -0.14367783 -0.36434153
		 -0.14711124 -0.14794825 -0.18436372 -0.012474914 -0.14311886 -0.012474914 -0.14367783
		 -0.14794825 -0.1849227 -0.2089988 -0.19657111 -0.15631524 -0.14640677 -0.15146391
		 -0.070076942 -0.1793005 0.019902796 -0.2089988 0.099266231 -0.2089988 0.09984228
		 -0.1793005 0.020478904 -0.15146391 -0.069500834 -0.15631524 -0.1458478 -0.2089988
		 -0.19601202;
createNode polyTweakUV -n "polyTweakUV11";
	rename -uid "198C8EE1-4B43-031C-3A13-19A052007662";
	setAttr ".uopa" yes;
	setAttr -s 40 ".uvtk[0:39]" -type "float2" -0.12029684 0.0018614531
		 -0.099040747 -0.055917382 -0.10044879 -0.057879925 -0.12170482 -0.00010097027 0.0021159649
		 0.3373794 0.099977493 0.21638271 0.31088966 0.2994985 0.24827933 0.37301761 0.38770014
		 0.37826818 0.42719561 0.33588094 0.42860353 0.33784345 0.38910812 0.38023064 0.24968737
		 0.37498012 0.31229764 0.30146104 0.10138547 0.2183452 0.0035240054 0.33934194 0.37401706
		 -0.1635102 0.37542504 -0.1615476 0.48199952 0.062093735 0.4805916 0.060131192 0.52988958
		 0.19062108 0.5284816 0.18865862 -0.13335758 0.19426787 -0.045650005 0.086946785 -0.13476563
		 0.19230533 -0.047057986 0.084984362 0.020484686 -0.18774807 0.19833565 -0.25246835
		 0.19692761 -0.25443095 0.019076765 -0.1897105 -0.036462367 -0.13919657 0.10129356
		 -0.10995823 0.26609725 -0.0024905205 0.41532415 0.16370505 0.49118203 0.25456432
		 0.49259001 0.25652677 0.41673207 0.16566759 0.26750517 -0.00052803755 0.1027016 -0.10799569
		 -0.035054386 -0.13723403;
createNode polyTweakUV -n "polyTweakUV12";
	rename -uid "FAEB0AB1-4311-BB95-D7E3-4CBFA9A6A3F0";
	setAttr ".uopa" yes;
	setAttr -s 40 ".uvtk[0:39]" -type "float2" 0.49274641 -0.50901723 0.52891839
		 -0.4554612 0.53087348 -0.45740706 0.49470139 -0.51096308 0.1400283 -0.5989908 0.18450719
		 -0.44000399 -0.012028515 -0.31576681 -0.037757754 -0.4171252 -0.12703264 -0.30927998
		 -0.11359799 -0.2457096 -0.11555296 -0.24376374 -0.12898773 -0.3073341 -0.039712787
		 -0.41517925 -0.013983548 -0.31382096 0.18255222 -0.43805817 0.13807321 -0.59704494
		 0.33783782 0.0089906873 0.33588272 0.010936618 0.083851099 -0.035396725 0.085806131
		 -0.037342563 -0.054107726 -0.077185363 -0.052152634 -0.079131275 0.3407284 -0.6284436
		 0.37980646 -0.48605639 0.34268343 -0.63038957 0.38176149 -0.48800224 0.5676263 -0.2788685
		 0.51715189 -0.086193621 0.51910692 -0.088139459 0.56958127 -0.28081435 0.56149966
		 -0.35325074 0.45726067 -0.24592651 0.26945674 -0.16888101 0.040111601 -0.14548641
		 -0.082971573 -0.14155337 -0.084926546 -0.13960752 0.038156569 -0.14354056 0.26750177
		 -0.16693512 0.45530564 -0.24398057 0.55954468 -0.35130495;
createNode polyTweakUV -n "polyTweakUV13";
	rename -uid "33BFA29E-4C7D-69AC-B526-93A9997C5361";
	setAttr ".uopa" yes;
	setAttr -s 40 ".uvtk[0:39]" -type "float2" -0.19527198 0.62103981 -0.25534818
		 0.63811755 -0.25534818 0.64087915 -0.19527198 0.62380135 2.9802322e-008 0.32020247
		 -0.1587027 0.31246841 -0.21707347 0.093523026 -0.11793801 0.10125697 -0.19527198
		 -0.012873769 -0.25534818 -0.02060771 -0.25534818 -0.023369253 -0.19527198 -0.015635371
		 -0.11793801 0.098495305 -0.21707347 0.090761483 -0.1587027 0.30970693 2.9802322e-008
		 0.31744081 -0.62843543 0.32020247 -0.62843543 0.31744081 -0.51049733 0.098495305
		 -0.51049733 0.10125697 -0.43316346 -0.015635371 -0.43316346 -0.012873769 -0.033971418
		 0.51517081 -0.1755161 0.50743693 -0.033971418 0.51793247 -0.17551604 0.51019847 -0.43316346
		 0.62103981 -0.594464 0.51517081 -0.594464 0.51793247 -0.43316346 0.62380135 -0.3604739
		 0.6373992 -0.42320129 0.50671858 -0.4364123 0.30898839 -0.39054793 0.090043008 -0.3604739
		 -0.024087727 -0.3604739 -0.02684927 -0.39054793 0.087281466 -0.4364123 0.30622691
		 -0.42320129 0.50395691 -0.3604739 0.63463759;
createNode polyTweakUV -n "polyTweakUV14";
	rename -uid "2B955F6D-439F-393E-05FE-4B91D5283E2E";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk[0:11]" -type "float2" 0.48597616 -0.12417549 0.20024495
		 -0.12417549 0.30817622 -0.27560234 0.37804365 -0.27560234 0.53723222 0.098801181
		 0.14898853 0.098801181 0.55022722 0.20272848 0.13599233 0.20272848 0.42853647 0.30126598
		 0.25768429 0.30126598 0.17077653 0.25470623 0.51544404 0.25470623;
createNode polyTweakUV -n "polyTweakUV15";
	rename -uid "C2EA6C33-4ACE-74E0-A142-4AADCB06932B";
	setAttr ".uopa" yes;
	setAttr -s 40 ".uvtk[0:39]" -type "float2" -0.14819378 -0.20989221 -0.19873442
		 -0.19642183 -0.19873442 -0.19572991 -0.14819378 -0.20920035 -5.9604645e-008 -0.33084515
		 -0.12538923 -0.32295525 -0.16087507 -0.41323918 -0.071699098 -0.42050344 -0.14819378
		 -0.46643972 -0.19873442 -0.49286675 -0.19873442 -0.49357569 -0.14819378 -0.46714866
		 -0.071699098 -0.42121238 -0.16087507 -0.41394812 -0.12538923 -0.32366413 -5.9604645e-008
		 -0.33155406 -0.49651974 -0.33084515 -0.49651974 -0.33155406 -0.42482054 -0.42121238
		 -0.42482054 -0.42050344 -0.34832597 -0.46714866 -0.34832597 -0.46643972 -0.012495617
		 -0.25169438 -0.13157368 -0.24744499 -0.012495617 -0.2510024 -0.13157362 -0.24675302
		 -0.34832597 -0.20989221 -0.48402414 -0.25169438 -0.48402414 -0.2510024 -0.34832597
		 -0.20920035 -0.28717405 -0.19660179 -0.33994505 -0.2476249 -0.34480447 -0.32384855
		 -0.31692168 -0.41413248 -0.28717405 -0.49376005 -0.28717405 -0.4944689 -0.31692168
		 -0.41484141 -0.34480447 -0.32455736 -0.33994505 -0.24831682 -0.28717405 -0.19729371;
createNode polyTweakUV -n "polyTweakUV16";
	rename -uid "938EE7EA-4D63-21F0-CE86-67B292E1FE39";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk[0:11]" -type "float2" -0.37322831 0.079573125 -0.14696269
		 0.33405519 -0.34509474 0.33744204 -0.40042126 0.27521604 -0.24070647 -0.11952084
		 0.066736415 0.2262632 -0.16445057 -0.20822385 0.16357322 0.16070798 0.020206816 -0.17893896
		 0.15550052 -0.02677195 0.18260249 0.088005662 -0.09033221 -0.21896777;
createNode polyTweakUV -n "polyTweakUV17";
	rename -uid "646CE533-4358-03DF-5726-13B61A7BAA1F";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk[0:11]" -type "float2" 0.074888758 -0.46402395 0.06525784
		 -0.20408812 -0.068859778 -0.30737996 -0.066504203 -0.37093973 0.27946383 -0.50313735
		 0.26637709 -0.1499435 0.37444615 -0.51145607 0.36048424 -0.13461861 0.45998606 -0.39743
		 0.4542273 -0.24200214 0.40894166 -0.16451004 0.42055914 -0.47806108;
createNode polyTweakUV -n "polyTweakUV18";
	rename -uid "41455C38-4FA6-4948-2B6F-CC8859005C3E";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk[0:11]" -type "float2" -0.16543689 0.026293483 0.13867059
		 -0.30521017 0.19948122 -0.0188227 0.12512077 0.062236831 -0.47868586 -0.15155606
		 -0.065472804 -0.60199511 -0.61309242 -0.24709049 -0.17221725 -0.72768235 -0.59789777
		 -0.49315083 -0.41605753 -0.69137222 -0.26954257 -0.74264711 -0.6363759 -0.342767;
createNode polyTweakUV -n "polyTweakUV19";
	rename -uid "9724D6E7-4F35-F371-3BBD-76AF7C3685D9";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk[0:11]" -type "float2" 0.57430935 0.20444895 0.15305224
		 -0.20523898 0.52929664 -0.27373302 0.63230199 -0.17355606 0.33016843 0.60667902 -0.24222457
		 0.050006017 0.20031422 0.77853262 -0.41039699 0.18459462 -0.12038192 0.7493242 -0.3722707
		 0.50435305 -0.43364161 0.3110992 0.074505344 0.80529052;
createNode lambert -n "lambert2";
	rename -uid "B8E3B384-4593-FD22-0CAA-31B6AC8D433E";
	setAttr ".c" -type "float3" 0.54000002 0 0 ;
createNode shadingEngine -n "lambert2SG";
	rename -uid "52B27C27-4A48-3D06-48E0-26BFD5368A16";
	setAttr ".ihi" 0;
	setAttr -s 6 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "181E68E1-4C2A-69AA-E795-0499EED57E67";
createNode lambert -n "lambert3";
	rename -uid "96671297-4CC5-B17F-E47E-5183C9869382";
	setAttr ".c" -type "float3" 0 0.7101 0.2309 ;
createNode shadingEngine -n "lambert3SG";
	rename -uid "EA961A9D-4D08-3CDC-84F7-5FAB78418AEA";
	setAttr ".ihi" 0;
	setAttr -s 7 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "0B6A3702-4BD5-8571-688D-8DB4012C92BD";
createNode groupId -n "groupId2";
	rename -uid "0A7DF850-43F3-859D-8026-61ACE6DA6DAA";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "20E81BE0-456E-9A84-4CD1-DF91F9AF3824";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:15]";
createNode groupId -n "groupId3";
	rename -uid "EAD20B6F-4B5C-2006-C6E7-A892032FA553";
	setAttr ".ihi" 0;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "8E594F30-4B87-38B1-714D-1CB1B57C2DB5";
	setAttr ".pee" yes;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -73.809520876596963 -286.90475050419138 ;
	setAttr ".tgi[0].vh" -type "double2" 72.619044733426037 298.80951193590062 ;
	setAttr -s 2 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" -247.14285278320312;
	setAttr ".tgi[0].ni[0].y" 144.28572082519531;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" 60;
	setAttr ".tgi[0].ni[1].y" 144.28572082519531;
	setAttr ".tgi[0].ni[1].nvs" 1923;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 6 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polyTweakUV3.out" "pCylinderShape1.i";
connectAttr "polyTweakUV3.uvtk[0]" "pCylinderShape1.uvst[0].uvtw";
connectAttr "polyTweakUV13.out" "pCubeShape1.i";
connectAttr "polyTweakUV13.uvtk[0]" "pCubeShape1.uvst[0].uvtw";
connectAttr "groupParts2.og" "pSphereShape1.i";
connectAttr "groupId1.id" "pSphereShape1.iog.og[0].gid";
connectAttr "groupId2.id" "pSphereShape1.iog.og[1].gid";
connectAttr "lambert2SG.mwc" "pSphereShape1.iog.og[1].gco";
connectAttr "polyTweakUV9.uvtk[0]" "pSphereShape1.uvst[0].uvtw";
connectAttr "groupId3.id" "pSphereShape1.ciog.cog[0].cgid";
connectAttr "polyTweakUV11.out" "pCubeShape2.i";
connectAttr "polyTweakUV11.uvtk[0]" "pCubeShape2.uvst[0].uvtw";
connectAttr "polyTweakUV12.out" "pCubeShape3.i";
connectAttr "polyTweakUV12.uvtk[0]" "pCubeShape3.uvst[0].uvtw";
connectAttr "polyTweakUV10.out" "pCubeShape4.i";
connectAttr "polyTweakUV10.uvtk[0]" "pCubeShape4.uvst[0].uvtw";
connectAttr "polyTweakUV15.out" "pCubeShape5.i";
connectAttr "polyTweakUV15.uvtk[0]" "pCubeShape5.uvst[0].uvtw";
connectAttr "polyTweakUV14.out" "pPlaneShape1.i";
connectAttr "polyTweakUV14.uvtk[0]" "pPlaneShape1.uvst[0].uvtw";
connectAttr "polyTweakUV19.out" "pPlaneShape2.i";
connectAttr "polyTweakUV19.uvtk[0]" "pPlaneShape2.uvst[0].uvtw";
connectAttr "polyTweakUV18.out" "pPlaneShape3.i";
connectAttr "polyTweakUV18.uvtk[0]" "pPlaneShape3.uvst[0].uvtw";
connectAttr "polyTweakUV17.out" "pPlaneShape4.i";
connectAttr "polyTweakUV17.uvtk[0]" "pPlaneShape4.uvst[0].uvtw";
connectAttr "polyTweakUV16.out" "pPlaneShape5.i";
connectAttr "polyTweakUV16.uvtk[0]" "pPlaneShape5.uvst[0].uvtw";
connectAttr "polyTweakUV1.out" "pPlaneShape6.i";
connectAttr "polyTweakUV1.uvtk[0]" "pPlaneShape6.uvst[0].uvtw";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCylinder1.out" "polySplitRing1.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing1.mp";
connectAttr "polySplitRing1.out" "polySplitRing2.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing2.mp";
connectAttr "polySplitRing2.out" "polySplitRing3.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing3.out" "polySplitRing4.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing4.mp";
connectAttr "polySplitRing4.out" "polySplitRing5.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing5.mp";
connectAttr "polySplitRing5.out" "polySplitRing6.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing6.mp";
connectAttr "polyCube1.out" "polySplitRing7.ip";
connectAttr "pCubeShape1.wm" "polySplitRing7.mp";
connectAttr "polyTweak1.out" "polySplitRing8.ip";
connectAttr "pCubeShape1.wm" "polySplitRing8.mp";
connectAttr "polySplitRing7.out" "polyTweak1.ip";
connectAttr "polySplitRing8.out" "polySplitRing9.ip";
connectAttr "pCubeShape1.wm" "polySplitRing9.mp";
connectAttr "polyTweak2.out" "polySplitRing10.ip";
connectAttr "pCubeShape1.wm" "polySplitRing10.mp";
connectAttr "polySplitRing9.out" "polyTweak2.ip";
connectAttr "polyTweak3.out" "polySplitRing11.ip";
connectAttr "pCubeShape1.wm" "polySplitRing11.mp";
connectAttr "polySplitRing10.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polySoftEdge1.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge1.mp";
connectAttr "polySplitRing11.out" "polyTweak4.ip";
connectAttr "polySphere1.out" "polySoftEdge2.ip";
connectAttr "pSphereShape1.wm" "polySoftEdge2.mp";
connectAttr "polyPlane1.out" "polySplitRing12.ip";
connectAttr "pPlaneShape1.wm" "polySplitRing12.mp";
connectAttr "polyTweak5.out" "polySplitRing13.ip";
connectAttr "pPlaneShape1.wm" "polySplitRing13.mp";
connectAttr "polySplitRing12.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polySplitRing14.ip";
connectAttr "pPlaneShape1.wm" "polySplitRing14.mp";
connectAttr "polySplitRing13.out" "polyTweak6.ip";
connectAttr "polyTweak7.out" "polySplitRing15.ip";
connectAttr "pPlaneShape1.wm" "polySplitRing15.mp";
connectAttr "polySplitRing14.out" "polyTweak7.ip";
connectAttr "polySurfaceShape1.o" "polyMapDel1.ip";
connectAttr "polySurfaceShape2.o" "polyMapDel2.ip";
connectAttr "polySurfaceShape3.o" "polyMapDel3.ip";
connectAttr "polySoftEdge1.out" "polyMapDel4.ip";
connectAttr "polyTweak8.out" "polyMapDel5.ip";
connectAttr "polySplitRing6.out" "polyTweak8.ip";
connectAttr "polyTweak9.out" "polyMapDel6.ip";
connectAttr "polySplitRing15.out" "polyTweak9.ip";
connectAttr "polySurfaceShape4.o" "polyMapDel7.ip";
connectAttr "polySoftEdge2.out" "polyMapDel8.ip";
connectAttr "polySurfaceShape5.o" "polyMapDel9.ip";
connectAttr "polySurfaceShape6.o" "polyMapDel10.ip";
connectAttr "polySurfaceShape7.o" "polyMapDel11.ip";
connectAttr "polySurfaceShape8.o" "polyMapDel12.ip";
connectAttr "polySurfaceShape9.o" "polyMapDel13.ip";
connectAttr "polyMapDel9.out" "polyPlanarProj1.ip";
connectAttr "pPlaneShape6.wm" "polyPlanarProj1.mp";
connectAttr "polyPlanarProj1.out" "polyTweakUV1.ip";
connectAttr "polyMapDel13.out" "polyPlanarProj2.ip";
connectAttr "pPlaneShape2.wm" "polyPlanarProj2.mp";
connectAttr "polyMapDel6.out" "polyPlanarProj3.ip";
connectAttr "pPlaneShape1.wm" "polyPlanarProj3.mp";
connectAttr "polyMapDel11.out" "polyPlanarProj4.ip";
connectAttr "pPlaneShape4.wm" "polyPlanarProj4.mp";
connectAttr "polyMapDel10.out" "polyPlanarProj5.ip";
connectAttr "pPlaneShape5.wm" "polyPlanarProj5.mp";
connectAttr "polyMapDel12.out" "polyPlanarProj6.ip";
connectAttr "pPlaneShape3.wm" "polyPlanarProj6.mp";
connectAttr "polyMapDel5.out" "polyCylProj1.ip";
connectAttr "pCylinderShape1.wm" "polyCylProj1.mp";
connectAttr "polyCylProj1.out" "polyMapDel14.ip";
connectAttr "polyMapDel14.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "polyCylProj2.ip";
connectAttr "pCylinderShape1.wm" "polyCylProj2.mp";
connectAttr "polyCylProj2.out" "polyMapCut1.ip";
connectAttr "polyMapCut1.out" "polyTweakUV2.ip";
connectAttr "polyTweakUV2.out" "polyMapDel15.ip";
connectAttr "polyMapDel15.out" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "polyCylProj3.ip";
connectAttr "pCylinderShape1.wm" "polyCylProj3.mp";
connectAttr "polyCylProj3.out" "polyMapDel16.ip";
connectAttr "polyMapDel16.out" "polyCylProj4.ip";
connectAttr "pCylinderShape1.wm" "polyCylProj4.mp";
connectAttr "polyCylProj4.out" "polyMapDel17.ip";
connectAttr "polyMapDel17.out" "polyPlanarProj7.ip";
connectAttr "pCylinderShape1.wm" "polyPlanarProj7.mp";
connectAttr "polyPlanarProj7.out" "polyTweakUV3.ip";
connectAttr "polyMapDel1.out" "polyPlanarProj8.ip";
connectAttr "pCubeShape4.wm" "polyPlanarProj8.mp";
connectAttr "polyMapDel7.out" "polyPlanarProj9.ip";
connectAttr "pCubeShape5.wm" "polyPlanarProj9.mp";
connectAttr "polyMapDel4.out" "polyPlanarProj10.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj10.mp";
connectAttr "polyMapDel2.out" "polyPlanarProj11.ip";
connectAttr "pCubeShape2.wm" "polyPlanarProj11.mp";
connectAttr "polyMapDel3.out" "polyPlanarProj12.ip";
connectAttr "pCubeShape3.wm" "polyPlanarProj12.mp";
connectAttr "polyMapDel8.out" "polyPlanarProj13.ip";
connectAttr "pSphereShape1.wm" "polyPlanarProj13.mp";
connectAttr "polyPlanarProj13.out" "polyPlanarProj14.ip";
connectAttr "pSphereShape1.wm" "polyPlanarProj14.mp";
connectAttr "polyPlanarProj14.out" "polyPlanarProj15.ip";
connectAttr "pSphereShape1.wm" "polyPlanarProj15.mp";
connectAttr "polyPlanarProj15.out" "polyPlanarProj16.ip";
connectAttr "pSphereShape1.wm" "polyPlanarProj16.mp";
connectAttr "polyPlanarProj16.out" "polyTweakUV4.ip";
connectAttr "polyTweakUV4.out" "polyMapSewMove1.ip";
connectAttr "polyMapSewMove1.out" "polyTweakUV5.ip";
connectAttr "polyTweakUV5.out" "polyMapSewMove2.ip";
connectAttr "polyMapSewMove2.out" "polyTweakUV6.ip";
connectAttr "polyTweakUV6.out" "polyMapSewMove3.ip";
connectAttr "polyMapSewMove3.out" "polyTweakUV7.ip";
connectAttr "polyTweakUV7.out" "polyMapSewMove4.ip";
connectAttr "polyMapSewMove4.out" "polyFlipUV1.ip";
connectAttr "pSphereShape1.wm" "polyFlipUV1.mp";
connectAttr "polyFlipUV1.out" "polyTweakUV8.ip";
connectAttr "polyTweakUV8.out" "polyMapSewMove5.ip";
connectAttr "polyMapSewMove5.out" "polyTweakUV9.ip";
connectAttr "polyTweakUV9.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "polyPlanarProj8.out" "polyTweakUV10.ip";
connectAttr "polyPlanarProj11.out" "polyTweakUV11.ip";
connectAttr "polyPlanarProj12.out" "polyTweakUV12.ip";
connectAttr "polyPlanarProj10.out" "polyTweakUV13.ip";
connectAttr "polyPlanarProj3.out" "polyTweakUV14.ip";
connectAttr "polyPlanarProj9.out" "polyTweakUV15.ip";
connectAttr "polyPlanarProj5.out" "polyTweakUV16.ip";
connectAttr "polyPlanarProj4.out" "polyTweakUV17.ip";
connectAttr "polyPlanarProj6.out" "polyTweakUV18.ip";
connectAttr "polyPlanarProj2.out" "polyTweakUV19.ip";
connectAttr "lambert2.oc" "lambert2SG.ss";
connectAttr "pCubeShape5.iog" "lambert2SG.dsm" -na;
connectAttr "pCubeShape3.iog" "lambert2SG.dsm" -na;
connectAttr "pCubeShape2.iog" "lambert2SG.dsm" -na;
connectAttr "pCubeShape1.iog" "lambert2SG.dsm" -na;
connectAttr "pCubeShape4.iog" "lambert2SG.dsm" -na;
connectAttr "pSphereShape1.iog.og[1]" "lambert2SG.dsm" -na;
connectAttr "groupId2.msg" "lambert2SG.gn" -na;
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "lambert2.msg" "materialInfo1.m";
connectAttr "lambert3.oc" "lambert3SG.ss";
connectAttr "pCylinderShape1.iog" "lambert3SG.dsm" -na;
connectAttr "pPlaneShape1.iog" "lambert3SG.dsm" -na;
connectAttr "pPlaneShape2.iog" "lambert3SG.dsm" -na;
connectAttr "pPlaneShape3.iog" "lambert3SG.dsm" -na;
connectAttr "pPlaneShape4.iog" "lambert3SG.dsm" -na;
connectAttr "pPlaneShape5.iog" "lambert3SG.dsm" -na;
connectAttr "pPlaneShape6.iog" "lambert3SG.dsm" -na;
connectAttr "lambert3SG.msg" "materialInfo2.sg";
connectAttr "lambert3.msg" "materialInfo2.m";
connectAttr "groupParts1.og" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "lambert3.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "lambert3SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "lambert2.msg" ":defaultShaderList1.s" -na;
connectAttr "lambert3.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pSphereShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId3.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId1.msg" ":defaultLastHiddenSet.gn" -na;
connectAttr "pSphereShape1.iog.og[0]" ":defaultLastHiddenSet.dsm" -na;
// End of Poppy.ma
