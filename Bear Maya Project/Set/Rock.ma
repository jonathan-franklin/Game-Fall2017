//Maya ASCII 2017ff05 scene
//Name: Rock.ma
//Last modified: Mon, Nov 27, 2017 01:14:55 PM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "education";
createNode transform -s -n "persp";
	rename -uid "169D5FF7-4A6B-251C-68AD-F59C7CD5EFDD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.1246428586770081 2.5739468022331677 1.4273034833784872 ;
	setAttr ".r" -type "double3" -56.738352724303837 -679.39999999993586 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "380851F6-46DD-D5C5-7D29-C98519181357";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 2.9974525462306363;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0.054774940013885498 0.067552879452705383 0.17906716465950012 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "1EB5FA23-49B5-451E-34B6-DB9918B62289";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.054774940013885498 1000.100017532103 0.17906716465972217 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "57A29782-4D04-1A1E-29EE-D886CF6D7676";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.0324646526501;
	setAttr ".ow" 1.9222122061540161;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 0.054774940013885498 0.067552879452705383 0.17906716465950012 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "AEC132AE-42D6-5548-91A6-7EBC896D5B08";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.054774940013885498 0.067552879452705383 1000.1000037821418 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "EA9C882D-4228-B066-1F51-5B8FE4EBAEC7";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 999.92093661748231;
	setAttr ".ow" 1.2243465373390601;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 0.054774940013885498 0.067552879452705383 0.17906716465950012 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "438D552D-4B7E-E9E5-E390-60B930B586B8";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1000183133924 0.067552879452705383 0.17906716465972217 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "10FE01F2-462E-712C-0883-318ECD5C739A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.0452433733783;
	setAttr ".ow" 1.7165802027049819;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 0.054774940013885498 0.067552879452705383 0.17906716465950012 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCube1";
	rename -uid "6F58554A-4F5E-2B19-CD31-B6A3E8E90A02";
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "651CBD0B-4FC9-4E2A-109A-358E31DD3E96";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49705861508846283 0.48101884126663208 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape1" -p "pCube1";
	rename -uid "29230DBB-4867-D20E-B849-BEB2C30808A8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 152 ".uvst[0].uvsp[0:151]" -type "float2" 0.2584219 0.34066528
		 0.33711642 0.27512163 0.29457253 0.63814664 0.24887717 0.64398563 0.46478188 0.46872771
		 0.54997802 0.41645157 0.55613673 0.59577215 0.46734744 0.59029853 0.54270709 0.28913498
		 0.43519014 0.1411348 0.55884635 0.13710237 0.827981 0.28661114 0.84399092 0.63174796
		 0.70065153 0.60829616 0.6962564 0.34037328 0.54171002 0.99999988 0.2957083 0.95985043
		 0.38355517 0.83591938 0.54604542 0.85210192 0.42494637 0.38588774 0.38441026 0.61341345
		 0.1753341 0.40058649 0.15600908 0.65255201 0.47916412 -5.9604645e-008 0.57533479
		 0.0018857718 0.46494573 0.74031436 0.54941368 0.75280404 0.66708887 0.58667684 0.65356636
		 0.45756394 0.54679894 0.34788287 0.71170568 0.15533084 0.69311297 0.038571179 0.68683863
		 0.83591938 0.65498281 0.74031436 0.72784269 0.92649066 0.23277134 0.90088415 0.16526449
		 0.86605477 0.318537 0.36400509 0.71846581 0.34882176 0.68308473 0.50582039 0.36035562
		 0.50474715 0.16287106 0.40814495 0.21342432 0.54687238 0.67138326 0.58737946 0.39080399
		 0.59043086 0.28407961 0.64339614 0.082744837 0.54642606 0.18915975 0.64688134 5.9604645e-008
		 0.43741453 1 0.42243826 0.957461 0.56197274 0.91099453 0.63234484 0.70653307 0.29185629
		 0.32558203 0.30743235 0.35055429 0.45627522 0.66965663 0.44867414 0.25810945 0.56180406
		 0.23313719 0.43960357 0.12549055 0.46040398 0.16410291 0.58810866 0.47031075 0.56462955
		 0.66957498 0.56042647 0.37108821 0.64848149 0.26512611 0.67838097 5.9604645e-008
		 0.50726414 0.080494225 0.6113919 0.15351111 0.69224262 0.94046843 0.45627522 0.83842134
		 0.56040013 1 0.30743235 0.51600564 0.39378035 0.55581677 0.1659885 0.74726486 0.14373094
		 0.72709656 0.38221639 0.21861279 0.38301539 0.36762398 0.35668898 0.37808084 0.53906536
		 0.25910664 0.57398522 0.59249842 0.52645373 0.70484877 0.53165281 0.33461869 0.75496578
		 0.42655265 0.72616851 0.66673183 0.71747065 0.67151213 0.82217574 0.52915907 0.84030092
		 0 0.43230653 0.084560812 0.61468291 0.23755223 0.78558803 1 0.71185875 0.84611416
		 0.81085825 0.97996819 0.1659885 0.98396993 0.39378035 0.8424418 0.55140495 0.31195247
		 0.51625013 0.46817559 0.51185191 0.45794147 0.27659822 0.27232128 0.27214348 0.72297657
		 0.25346708 0.6707201 0.50159478 0.2981621 0.71671963 0.37012112 0.6716485 0.65138733
		 0.65625191 0.082759261 0.58030987 -5.9604645e-008 0.36273003 0.22097814 0.74243343
		 0.92053938 0.58802247 0.86420214 0.70169878 0.99999988 0.38591093 0.27232128 0.6081444
		 0.45794147 0.61014009 0.41148978 0.39653629 0.26352668 0.39277953 0.72297657 0.63377905
		 0.66042447 0.39590776 -5.9604645e-008 0.48496354 0.062419176 0.36762691 0.99999988
		 0.45127434 0.90817833 0.3740257 0.30356878 0.28243232 0.43960488 0.34429109 0.37513876
		 0.48524433 0.22308159 0.44037867 0.56717849 0.74092317 0.61568964 0.62132359 0.74773526
		 0.6355114 0.76253998 0.75949216 0.048433125 0.24316442 5.9604645e-008 0.38386118
		 0.90881467 0.62132359 1 0.74092317 0.29263318 0.45073885 0.64448261 0.45312971 0.71846581
		 0.58174467 0.318537 0.57516503 0.16287106 0.54573727 0.12722236 0.45243704 5.9604645e-008
		 0.49416322 0.053311408 0.41181213 0.96343446 0.42657346 1 0.5106883 0.69134498 0.56196296
		 0.39668047 0.56779814 0.32558203 0.63211036 0.70653307 0.63811421 0.23313719 0.49474835
		 0.30446959 0.42456543 0.14301866 0.40490371 0.12549055 0.48245937 0.051861346 0.36008966
		 5.9604645e-008 0.42443043 0.93206406 0.56170201 1 0.63211036;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 85 ".vt[0:84]"  -0.45555392 -0.17924529 0.39856413 0.42620751 -0.17924529 0.5583635
		 -0.27754676 0.17924529 0.29351616 0.43643925 0.17924529 0.42211306 -0.28856274 0.17924529 -0.23166561
		 0.32688934 0.17924529 -0.2998547 -0.44027576 -0.17924529 -0.36526519 0.36896676 -0.17924529 -0.37263563
		 -0.10055933 0.20464951 0.4350765 -0.14585918 0.17066486 -0.33749413 -0.10627671 -0.17924529 -0.39856413
		 -0.096583158 -0.17924529 0.53012103 0.065006435 -0.17924529 0.89051604 0.054838754 0.17924529 0.74549276
		 0.33400154 -0.17924529 0.89051604 0.28889561 0.17924529 0.70197332 -0.5 -0.095217079 0.5
		 -0.5 -0.095217079 -0.46218914 -0.061830625 -0.095217079 -0.60637212 0.45682383 -0.095217079 -0.42338467
		 0.60954988 -0.095217079 0.52241182 0.41768068 -0.095217079 0.96450633 0.020488702 -0.095217079 0.96450633
		 -0.15574248 -0.095217079 0.63189059 -0.42081395 0.094010942 0.46437216 -0.42081395 0.094010942 -0.37725574
		 -0.052038379 0.094010942 -0.49185327 0.38079417 0.094010942 -0.36434507 0.53036386 0.094010942 0.46437216
		 0.42142814 0.094010942 0.84966564 -0.01679204 0.094010942 0.84966564 -0.14595023 0.094010942 0.59626275
		 0.039104771 0.3043595 0.21663333 0.24563213 0.3043595 0.22922923 0.24718173 0.3043595 -0.089794382
		 0.039284062 0.3043595 -0.089794382 -0.25736237 0.098999381 0.54280275 -0.29528242 -0.10026952 0.57843059
		 -0.22142084 -0.18875647 0.45888439 -0.24785131 -0.18875647 -0.39856413 -0.23943643 -0.10026952 -0.60637212
		 -0.20151639 0.098999381 -0.49185327 -0.21471131 0.17670667 -0.27096328 -0.18664995 0.18875647 0.36112446
		 0.0067318687 0.18660727 0.58625209 0.30923581 0.18660727 0.57023489 0.42615187 0.097872168 0.66829669
		 0.51861417 -0.0792014 0.73845917 0.43998873 -0.18660727 0.78793567 -0.057534341 -0.18660727 0.76046228
		 -0.11418761 -0.087474465 0.78546959 -0.083730876 0.097872168 0.73038149 -0.0044746706 0.25428393 0.31010053
		 -0.049756009 0.25428393 -0.19766435 0.28203163 0.25428393 -0.19766435 0.29233453 0.25428393 0.36145392
		 0.16004804 0.18389465 0.74336499 0.14200994 0.19154099 0.59080184 0.12435382 0.18389465 0.41926548
		 0.12883027 0.2618317 0.35298091 0.13230802 0.31384146 0.27561575 0.13169065 0.31384146 -0.10388628
		 0.12800589 0.2618317 -0.2159228 0.12326304 0.18389465 -0.38279417 0.14176103 0.09536805 -0.51102644
		 0.19176133 -0.10116904 -0.63630843 0.13190994 -0.18844292 -0.40202469 0.18618219 -0.18844292 0.91790485
		 0.19976816 -0.10116904 0.99444276 0.18088183 0.09536805 0.8718611 0.26042557 0.31731588 0.083552785
		 0.13904558 0.32730588 0.073290735 0.041911494 0.31731588 0.079466544 -0.048820596 0.2645573 0.053386286
		 -0.14710169 0.20850429 0.025480082 -0.19709177 0.19551893 0.018892014 -0.29868802 0.18549815 0.0092267245
		 -0.44336087 0.095697001 0.0091102812 -0.52678967 -0.10366973 -0.043501206 -0.45892704 -0.19220012 -0.02183244
		 0.44904342 -0.19220012 0.070034839 0.63633955 -0.10366973 -0.032645781 0.5301308 0.095697001 0.016459258
		 0.45395377 0.18549815 0.032699648 0.29714268 0.2645573 0.059160031;
	setAttr -s 160 ".ed[0:159]"  0 38 0 2 43 1 4 42 1 6 39 0 0 16 1 1 20 1
		 2 76 1 3 83 1 4 25 1 5 27 1 6 79 0 7 80 0 8 58 1 9 63 1 8 74 0 10 66 0 9 26 1 11 23 1
		 11 49 0 8 44 1 12 22 1 1 48 0 12 67 0 3 45 1 14 21 1 13 56 1 16 24 1 17 6 1 16 78 1
		 18 10 1 17 40 1 19 7 1 18 65 1 20 28 1 19 81 1 21 29 1 20 47 1 22 30 1 21 68 1 23 31 0
		 22 50 1 23 37 1 24 2 1 25 17 1 24 77 1 26 18 1 25 41 1 27 19 1 26 64 1 28 3 1 27 82 1
		 29 15 1 28 46 1 30 13 1 29 69 1 31 8 0 30 51 1 31 36 1 8 52 1 3 55 1 32 60 1 5 54 1
		 33 70 1 9 53 1 35 61 1 32 72 1 36 24 1 37 16 1 36 37 1 38 11 0 37 38 1 39 10 0 40 18 1
		 39 40 1 41 26 1 40 41 1 42 9 1 41 42 1 43 8 1 42 75 1 43 36 1 44 13 1 45 15 1 44 57 1
		 46 29 1 45 46 1 47 21 1 46 47 1 48 14 0 47 48 1 49 12 0 50 23 1 49 50 1 51 31 1 50 51 1
		 51 44 1 52 32 1 53 35 1 52 73 1 54 34 1 53 62 1 55 33 1 54 84 1 55 59 1 56 15 1 57 45 1
		 56 57 1 58 3 1 57 58 1 59 52 1 58 59 1 60 33 1 59 60 1 61 34 1 60 71 1 62 54 1 61 62 1
		 63 5 1 62 63 1 64 27 1 63 64 1 65 19 1 64 65 1 66 7 0 65 66 1 67 14 0 68 22 1 67 68 1
		 69 30 1 68 69 1 69 56 1 70 34 1 71 61 1 70 71 1 72 35 1 71 72 1 73 53 1 72 73 1 74 9 1
		 73 74 1 75 43 1 74 75 1 76 4 1 75 76 1 77 25 1 76 77 1 78 17 1 77 78 1 79 0 0 78 79 1
		 80 1 0 81 20 1 80 81 1 82 28 1 81 82 1 83 5 1 82 83 1 84 55 1 83 84 1 84 70 1;
	setAttr -s 76 -ch 304 ".fc[0:75]" -type "polyFaces" 
		f 4 69 17 41 70
		mu 0 4 118 119 120 121
		f 4 78 14 141 140
		mu 0 4 0 1 2 3
		f 4 72 29 -72 73
		mu 0 4 108 109 110 111
		f 4 -151 152 151 -6
		mu 0 4 130 131 132 133
		f 4 148 4 28 149
		mu 0 4 140 141 142 143
		f 4 60 114 135 -66
		mu 0 4 4 5 6 7
		f 4 -30 32 124 -16
		mu 0 4 110 109 112 113
		f 4 -21 22 127 126
		mu 0 4 122 123 124 125
		f 4 -18 18 92 91
		mu 0 4 144 145 146 147
		f 4 5 36 89 -22
		mu 0 4 130 133 134 135
		f 4 -13 19 83 108
		mu 0 4 8 1 9 10
		f 4 -29 26 44 147
		mu 0 4 52 53 54 55
		f 4 74 45 -73 75
		mu 0 4 93 94 95 96
		f 4 -33 -46 48 122
		mu 0 4 97 95 94 98
		f 4 -152 154 153 -34
		mu 0 4 37 38 39 40
		f 4 -37 33 52 87
		mu 0 4 41 37 40 42
		f 4 -38 -127 129 128
		mu 0 4 70 71 72 73
		f 4 -40 -92 94 93
		mu 0 4 56 57 58 59
		f 4 -42 39 57 68
		mu 0 4 74 75 76 77
		f 4 -45 42 6 145
		mu 0 4 55 54 60 61
		f 4 76 16 -75 77
		mu 0 4 99 100 94 93
		f 4 -49 -17 13 120
		mu 0 4 98 94 100 101
		f 4 -154 156 -8 -50
		mu 0 4 40 39 43 44
		f 4 -53 49 23 85
		mu 0 4 42 40 44 45
		f 4 -54 -129 130 -26
		mu 0 4 78 70 73 79
		f 4 -56 -94 95 -20
		mu 0 4 62 56 59 63
		f 4 80 -58 55 -79
		mu 0 4 80 77 76 81
		f 4 12 110 109 -59
		mu 0 4 81 82 83 84
		f 4 7 158 157 -60
		mu 0 4 11 12 13 14
		f 4 -14 63 100 118
		mu 0 4 15 16 17 18
		f 4 -15 58 98 139
		mu 0 4 2 1 19 20
		f 4 -68 -69 66 -27
		mu 0 4 85 74 77 86
		f 4 0 -71 67 -5
		mu 0 4 126 118 121 127
		f 4 30 -74 -4 -28
		mu 0 4 114 108 111 115
		f 4 46 -76 -31 -44
		mu 0 4 102 93 96 103
		f 4 2 -78 -47 -9
		mu 0 4 104 99 93 102
		f 4 1 -141 143 -7
		mu 0 4 21 0 3 22
		f 4 -67 -81 -2 -43
		mu 0 4 86 77 80 87
		f 4 -84 81 25 106
		mu 0 4 10 9 23 24
		f 4 -85 -86 82 -52
		mu 0 4 46 42 45 47
		f 4 -87 -88 84 -36
		mu 0 4 48 41 42 46
		f 4 -90 86 -25 -89
		mu 0 4 135 134 136 137
		f 4 -93 90 20 40
		mu 0 4 147 146 148 149
		f 4 -95 -41 37 56
		mu 0 4 59 58 64 65
		f 4 -96 -57 53 -82
		mu 0 4 63 59 65 66
		f 4 -99 96 65 137
		mu 0 4 20 19 4 7
		f 4 -101 97 64 116
		mu 0 4 18 17 25 26
		f 4 -158 159 -63 -102
		mu 0 4 14 13 27 28
		f 4 -110 112 -61 -97
		mu 0 4 19 29 5 4
		f 4 -106 -107 104 -83
		mu 0 4 30 10 24 31
		f 4 -108 -109 105 -24
		mu 0 4 11 8 10 30
		f 4 -111 107 59 103
		mu 0 4 83 82 88 89
		f 4 -113 -104 101 -112
		mu 0 4 5 29 14 28
		f 4 -115 111 62 133
		mu 0 4 6 5 28 27
		f 4 -116 -117 113 -100
		mu 0 4 32 18 26 33
		f 4 -118 -119 115 -62
		mu 0 4 34 15 18 32
		f 4 -120 -121 117 9
		mu 0 4 105 98 101 106
		f 4 -122 -123 119 47
		mu 0 4 107 97 98 105
		f 4 -125 121 31 -124
		mu 0 4 113 112 116 117
		f 4 -128 125 24 38
		mu 0 4 125 124 128 129
		f 4 -130 -39 35 54
		mu 0 4 73 72 90 91
		f 4 -131 -55 51 -105
		mu 0 4 79 73 91 92
		f 4 -133 -134 131 -114
		mu 0 4 26 6 27 33
		f 4 -136 132 -65 -135
		mu 0 4 7 6 26 25
		f 4 -137 -138 134 -98
		mu 0 4 17 20 7 25
		f 4 -139 -140 136 -64
		mu 0 4 16 2 20 17
		f 4 -142 138 -77 79
		mu 0 4 3 2 16 35
		f 4 -144 -80 -3 -143
		mu 0 4 22 3 35 36
		f 4 -145 -146 142 8
		mu 0 4 67 55 61 68
		f 4 -147 -148 144 43
		mu 0 4 69 52 55 67
		f 4 10 -150 146 27
		mu 0 4 150 140 143 151
		f 4 -153 -12 -32 34
		mu 0 4 132 131 138 139
		f 4 -155 -35 -48 50
		mu 0 4 39 38 49 50
		f 4 -157 -51 -10 -156
		mu 0 4 43 39 50 51
		f 4 -159 155 61 102
		mu 0 4 13 12 34 32
		f 4 -160 -103 99 -132
		mu 0 4 27 13 32 33;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "CFC0F3D2-459F-EBB6-9C18-57BF2016D858";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "D491BC72-4640-15A8-15A4-F297C46C2DA1";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "86C0A3E2-4E7B-0983-B04E-1CB3FD8614D4";
createNode displayLayerManager -n "layerManager";
	rename -uid "6F68FB69-4D6F-7210-89D6-79BC252FC507";
createNode displayLayer -n "defaultLayer";
	rename -uid "C384076F-4E97-8226-930C-A49C11983837";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "2950FCC9-4D84-B661-5B82-FD87891B25B0";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "A619EDCF-486F-DB2B-5547-FEAA12A0C6D3";
	setAttr ".g" yes;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "4D346D9A-4EA1-0D7C-322D-978CD8B309D6";
	setAttr ".pee" yes;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -171.42856461661233 -286.90475050419138 ;
	setAttr ".tgi[0].vh" -type "double2" 167.85713618709957 298.80951193590062 ;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "C514A037-428F-6F41-ED21-B09AEF585CF8";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 430\n            -height 384\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 429\n            -height 383\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 430\n            -height 383\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 429\n            -height 384\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n"
		+ "            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n"
		+ "            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n"
		+ "            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n"
		+ "            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n"
		+ "                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n"
		+ "                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 430\\n    -height 384\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 430\\n    -height 384\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 429\\n    -height 384\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 429\\n    -height 384\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 429\\n    -height 383\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 429\\n    -height 383\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 430\\n    -height 383\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 430\\n    -height 383\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "5009656F-425F-5F34-BCB2-3EA2FE87C178";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyMapCut -n "polyMapCut1";
	rename -uid "4EDEB3D9-4712-4103-AA36-4EBD1225C91A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[7]" "e[12]" "e[14]" "e[19]" "e[23]" "e[55]" "e[58:59]" "e[78]" "e[107]";
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "DCD9E460-4572-B189-61F5-64B6391FE120";
	setAttr ".uopa" yes;
	setAttr -s 156 ".uvtk[0:155]" -type "float2" -0.19878234 -0.30249628 -0.23164953
		 -0.27512163 -0.21388085 -0.426741 -0.19479594 -0.42917967 -0.28496969 -0.35598224
		 -0.32055235 -0.33414882 -0.32312453 -0.40904304 -0.28604123 -0.40675697 -0.42298245
		 0.71086502 -0.37807745 0.77267808 -0.42972314 0.77436227 -0.43666178 -0.27992028
		 -0.44334841 -0.42406854 -0.38348192 -0.41427377 -0.38164628 -0.30237433 -0.31709915
		 -0.57787097 -0.2143552 -0.56110227 -0.25104493 -0.50934184 -0.31890985 -0.51610053
		 -0.26833218 -0.32138368 -0.25140208 -0.41641104 -0.16408028 -0.3275227 -0.15600908
		 -0.43275747 -0.39644343 0.83162385 -0.43660963 0.83083618 -0.28503814 -0.46941188
		 -0.32031661 -0.47462827 -0.36946431 -0.40524435 -0.36381656 -0.35131964 -0.31922457
		 -0.30551076 -0.49356565 0.76674902 -0.4858003 0.81551433 -0.37771291 -0.50934184
		 -0.36440814 -0.46941188 -0.39483848 -0.54716945 -0.18806925 -0.5364747 -0.15987465
		 -0.52192807 -0.096827254 0.30589706 -0.26385969 0.31223845 -0.24908257 0.24666709
		 -0.11429302 0.24711537 -0.031812534 0.28746182 -0.052926376 0.22952151 -0.2441954
		 0.21260351 -0.12700996 0.2113291 -0.082435936 0.18920785 0.0016526133 0.22970796
		 -0.042792156 0.18775225 0.036211416 0.2752372 -0.38144398 0.28149211 -0.36367732
		 0.22321481 -0.34427035 0.19382352 0.069347262 0.25805646 0.22845358 0.25155103 0.21802378
		 0.18938601 0.084748924 0.19256061 0.67436701 -0.088390559 0.68479681 -0.03735286
		 0.72975606 -0.046040267 0.71362936 -0.099376798 0.16800684 0.14413124 0.084783018
		 0.14588666 0.62718087 -0.12459183 0.67143649 -0.13707954 0.78216779 -0.06561166 0.74854898
		 -0.10910118 0.7180531 -0.14286894 -0.028357089 0.18938601 0.014263451 0.14589769
		 -0.053220749 0.25155103 -0.11767942 -0.24816543 -0.13430673 -0.15302694 -0.21426606
		 -0.14373094 -0.20584267 -0.24333568 0.5786075 -0.36768433 0.51637214 -0.35668898
		 0.51200479 -0.43285945 0.56169504 -0.4474439 -0.14962703 -0.30357718 -0.19655079
		 -0.30574858 0.53015697 -0.52303141 -0.020984113 -0.48294258 -0.12129629 -0.47930986
		 -0.12329274 -0.52304053 -0.063838243 -0.53061056 0.66991228 -0.38827106 0.63459504
		 -0.46444154 0.57069731 -0.53582096 -0.2604875 -0.47696602 -0.19621623 -0.51831371
		 -0.31145585 -0.15302694 -0.31312722 -0.24816543 -0.25401723 -0.31399819 -0.1302886
		 0.046518803 -0.19553602 0.048355758 -0.19126168 0.14661071 -0.11373642 0.14847127
		 -0.30195501 0.15627158 -0.28012982 0.052639663 -0.12452897 -0.037208378 -0.15458305
		 -0.018384159 -0.27205539 -0.011953712 -0.034564815 0.019763887 5.9604645e-008 0.11063731
		 -0.092292681 -0.047947884 -0.3844682 0.016542673 -0.36093867 -0.030934811 -0.41765529
		 0.10095567 0.22665146 -0.1690819 0.14912617 -0.16991541 0.16852695 -0.080702633 0.23032457
		 -0.0791336 0.038432837 -0.17978835 0.064558089 -0.080440134 0.34038794 -0.11763474
		 0.31431821 -0.06862849 -0.077267468 -0.10356429 -0.038917661 -0.071300983 0.58657891
		 0.44707566 0.52976269 0.42124003 0.5566873 0.36237013 0.62019479 0.38110852 0.019447386
		 -0.21918231 -0.0008135438 -0.16923091 -0.055963099 -0.17515653 -0.062146366 -0.22693777
		 0.69313771 0.46347612 0.71336603 0.40471339 -0.1232388 -0.16923091 -0.16132283 -0.21918231
		 0.11315474 0.3743481 -0.033797085 0.37334955 -0.06469655 0.31963283 0.10233587 0.32238084
		 0.16735059 0.33467156 0.18223944 0.37363887 0.23537453 0.35621172 0.21310875 0.39060611
		 -0.16700906 0.38444096 -0.18228084 0.34930992 -0.24297032 0.33852118 -0.11990207
		 0.33608407 -0.090207443 0.30922377 -0.24931368 0.3067162 0.62685847 -0.19172302 0.5970661
		 -0.16241074 0.66449696 -0.15419891 0.67181766 -0.18659043 0.70256931 -0.13548207
		 0.72422945 -0.16235435 -0.34350795 0.3386302 -0.37188172 0.30922377 -0.54212868 0.71191913
		 -0.23164953 -0.27512163 -0.33711642 0.71671778 0.49176025 -0.51100409;
createNode polyFlipUV -n "polyFlipUV1";
	rename -uid "72403730-47EE-C770-7A45-6EA9BD6075E8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[11]" "f[19]" "f[68:69]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.7504074275;
	setAttr ".pv" 0.62933677430000001;
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "93493770-4B0D-177A-768D-42B14004CA79";
	setAttr ".uopa" yes;
	setAttr -s 10 ".uvtk";
	setAttr ".uvtk[52]" -type "float2" -0.92444402 -0.31386334 ;
	setAttr ".uvtk[53]" -type "float2" -1.1192147 -0.60314357 ;
	setAttr ".uvtk[54]" -type "float2" -0.99596339 -0.66531956 ;
	setAttr ".uvtk[55]" -type "float2" -0.82879937 -0.42595357 ;
	setAttr ".uvtk[60]" -type "float2" -0.85124338 -0.63569701 ;
	setAttr ".uvtk[61]" -type "float2" -0.74645686 -0.48651856 ;
	setAttr ".uvtk[67]" -type "float2" -0.67657316 -0.23041946 ;
	setAttr ".uvtk[68]" -type "float2" -0.6550597 -0.36202633 ;
	setAttr ".uvtk[69]" -type "float2" -0.75407261 -0.10594493 ;
createNode polyMapSewMove -n "polyMapSewMove1";
	rename -uid "F2761F39-4006-E292-D850-DA99E04A975C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[6]" "e[142]";
createNode polyFlipUV -n "polyFlipUV2";
	rename -uid "7A1D073A-4F72-924A-C508-C2AB59D6F949";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "f[4]" "f[70]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.43174643070000002;
	setAttr ".pv" 0.92258131499999996;
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "6C0BB42B-4378-10FB-97CA-43AA30601BCB";
	setAttr ".uopa" yes;
	setAttr -s 7 ".uvtk";
	setAttr ".uvtk[137]" -type "float2" -0.66577828 -0.67087269 ;
	setAttr ".uvtk[138]" -type "float2" -0.81810868 -0.89118308 ;
	setAttr ".uvtk[139]" -type "float2" -0.80849469 -0.97777843 ;
	setAttr ".uvtk[140]" -type "float2" -0.60157084 -0.70015591 ;
	setAttr ".uvtk[147]" -type "float2" -0.53801399 -0.4932906 ;
	setAttr ".uvtk[148]" -type "float2" -0.45000124 -0.48063853 ;
createNode polyMapSewMove -n "polyMapSewMove2";
	rename -uid "B33DEBE6-4650-AB67-73BE-E39B8148CAB2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[28]" "e[146]";
createNode polyFlipUV -n "polyFlipUV3";
	rename -uid "EC083EFF-49A5-3A7F-0F1F-D793569C9EA4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "f[14:15]" "f[22:23]" "f[39:40]" "f[72:73]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.32738374920000002;
	setAttr ".pv" 0.74784690139999999;
createNode polyTweakUV -n "polyTweakUV4";
	rename -uid "114788CC-4444-338A-AAD8-BDB30902E003";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk";
	setAttr ".uvtk[37]" -type "float2" 0.53449976 0.33121294 ;
	setAttr ".uvtk[38]" -type "float2" 0.60263807 0.030534685 ;
	setAttr ".uvtk[39]" -type "float2" 0.4787468 0.03508991 ;
	setAttr ".uvtk[40]" -type "float2" 0.43385169 0.27961457 ;
	setAttr ".uvtk[41]" -type "float2" 0.47903004 0.44283277 ;
	setAttr ".uvtk[42]" -type "float2" 0.38114455 0.38490582 ;
	setAttr ".uvtk[43]" -type "float2" 0.41533247 0.032399297 ;
	setAttr ".uvtk[44]" -type "float2" 0.37328389 0.24442339 ;
	setAttr ".uvtk[45]" -type "float2" 0.31806299 0.31773454 ;
	setAttr ".uvtk[46]" -type "float2" 0.36297461 0.48392034 ;
	setAttr ".uvtk[47]" -type "float2" 0.30198058 0.38911498 ;
	setAttr ".uvtk[48]" -type "float2" 0.43379977 0.56201434 ;
	setAttr ".uvtk[49]" -type "float2" 0.58676851 -0.19307113 ;
	setAttr ".uvtk[50]" -type "float2" 0.47508726 -0.18062228 ;
	setAttr ".uvtk[51]" -type "float2" 0.41522023 -0.15540451 ;
createNode polyMapSewMove -n "polyMapSewMove3";
	rename -uid "F0077E22-4787-06D2-42DE-2EAFBDF1CA81";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[9]" "e[47]";
createNode polyFlipUV -n "polyFlipUV4";
	rename -uid "8047E410-4E2E-5A19-CEE2-14B412A370A9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[16]" "f[24]" "f[60:61]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.53458446260000003;
	setAttr ".pv" 0.1187033802;
createNode polyTweakUV -n "polyTweakUV5";
	rename -uid "C51DCD0F-4C42-10A1-34E5-1484A97799D6";
	setAttr ".uopa" yes;
	setAttr -s 10 ".uvtk";
	setAttr ".uvtk[67]" -type "float2" 0.23678738 1.3593817 ;
	setAttr ".uvtk[68]" -type "float2" 0.38138962 1.4837141 ;
	setAttr ".uvtk[69]" -type "float2" 0.51504862 1.3976909 ;
	setAttr ".uvtk[70]" -type "float2" 0.37736726 1.2562619 ;
	setAttr ".uvtk[75]" -type "float2" 0.21609181 1.2347918 ;
	setAttr ".uvtk[76]" -type "float2" 0.2849707 1.1726719 ;
	setAttr ".uvtk[87]" -type "float2" 0.65169919 1.2617044 ;
	setAttr ".uvtk[88]" -type "float2" 0.53501856 1.1144395 ;
	setAttr ".uvtk[89]" -type "float2" 0.36231947 1.0880649 ;
createNode polyMapSewMove -n "polyMapSewMove4";
	rename -uid "28E4AA89-4599-74EB-6C03-C0B1F39EAA12";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[35]";
createNode polyTweakUV -n "polyTweakUV6";
	rename -uid "9B8A3D3A-44B1-3C62-E23D-E58A087A885A";
	setAttr ".uopa" yes;
	setAttr -s 145 ".uvtk[0:144]" -type "float2" 0.19296469 0.61259401 0.18494318
		 0.61927497 0.18927978 0.58227098 0.19393761 0.58167583 0.17192993 0.59954029 0.16324568
		 0.60486889 0.16261792 0.58659029 0.17166841 0.58714831 0.62745535 -0.76435399 0.63841486
		 -0.74926794 0.62581027 -0.7488569 0.1349082 0.61810386 0.13327622 0.58292317 0.14788717
		 0.58531368 0.14833519 0.61262369 0.16408846 0.54538643 0.18916401 0.54947895 0.18020958
		 0.56211156 0.16364655 0.560462 0.17599046 0.6079843 0.18012241 0.58479208 0.20170005
		 0.60651624 0.20311318 0.58080214 0.63393247 -0.73488176 0.62412953 -0.73507398 0.17191324
		 0.57185674 0.1633032 0.5705837 0.1513083 0.58751744 0.15268669 0.60067821 0.16356975
		 0.61185825 0.6102289 -0.75071502 0.6121242 -0.7388134 0.14929515 0.56211156 0.15254229
		 0.57185674 0.14511552 0.55287939 0.19557932 0.55548954 0.20277585 0.55901057 -0.32662416
		 -0.53138494 -0.27801895 -0.44007352 -0.24754822 -0.46838927 -0.28974766 -0.54034197
		 -0.33700299 -0.57189691 -0.29944098 -0.57864094 -0.2308507 -0.48160714 -0.26664868
		 -0.54468048 -0.26869291 -0.57540727 -0.316531 -0.6077745 -0.28025538 -0.5970639 -0.3516404
		 -0.61208504 -0.22422215 -0.38623583 -0.20036045 -0.41509333 -0.18834358 -0.4330585
		 0.23689684 0.57643747 0.23557812 0.62567854 0.21578065 0.62219292 0.21726453 0.58096677
		 -0.36498839 -0.2968221 -0.36244291 -0.28436592 -0.35147023 -0.28648615 -0.35540611
		 -0.29950339 -0.3765046 -0.30565733 -0.36570364 -0.30870506 -0.33867869 -0.29126272
		 -0.34688365 -0.30187672 -0.35432643 -0.31011802 0.21670789 0.54595739 0.23681815
		 0.53849393 -0.3089453 -0.68003482 -0.34476483 -0.67757994 -0.35130513 -0.64837843
		 -0.31415266 -0.64762688 0.09996736 0.0080433972 0.084778309 0.010726914 0.083712399
		 -0.007863149 0.095839739 -0.011422619 -0.28969854 -0.66607267 -0.29071695 -0.64863986
		 0.088142633 -0.029870391 0.11438423 -0.0011073351 0.089902222 -0.0002207458 0.089414954
		 -0.010893583 0.10392535 -0.012741119 0.12225109 0.0030190386 0.11363161 -0.015571028
		 0.098036826 -0.032991797 0.055931389 0.00035129488 0.071617365 -0.0097399652 -0.28989738
		 -0.62707341 -0.10347565 -0.37710467 -0.12961355 -0.3832444 -0.13825959 -0.34372497
		 -0.10762893 -0.33482224 -0.18329206 -0.3515389 -0.16370186 -0.39044815 -0.092369467
		 -0.40979081 -0.10630201 -0.40547025 -0.15368991 -0.41528237 -0.062595658 -0.37766415
		 -0.058419961 -0.33789048 -0.078420475 -0.41066688 -0.046998918 -0.32351494 -0.065919638
		 -0.32371837 -0.061184704 -0.30194521 -0.046102464 -0.30156228 -0.092935324 -0.32612795
		 -0.086559236 -0.30188116 -0.019240558 -0.31095883 -0.025603116 -0.29899842 -0.12117296
		 -0.3075248 -0.11181337 -0.29965067 -0.74430954 -0.53287768 -0.75817609 -0.53918314
		 -0.75160486 -0.55355084 -0.73610532 -0.54897755 0.19024479 -0.22909033 0.18529993
		 -0.21689928 0.17184019 -0.21834548 0.17033112 -0.23098314 -0.71830297 -0.52887499
		 -0.71336609 -0.54321659 0.15542096 -0.21689928 0.14612627 -0.22909033 -0.26520339
		 -0.36898986 -0.30106825 -0.36923355 -0.30860952 -0.38234359 -0.26784381 -0.38167292
		 -0.25197643 -0.37867326 -0.24834266 -0.36916295 -0.23537458 -0.37341622 -0.24080874
		 -0.36502197 -0.33357975 -0.36652663 -0.33730698 -0.37510064 0.24602094 0.57855165
		 0.24464595 0.6166442 -0.74799371 0.015198678 -0.75526482 0.022352606 -0.73880774
		 0.024356753 -0.73702109 0.016451329 -0.72951585 0.028924763 -0.72422951 0.022366375
		 0.2465615 0.54742336 0.59837675 -0.76409674 0.18494318 0.61927497 0.64841175 -0.76292557
		 0.078771591 -0.026935026;
createNode polyFlipUV -n "polyFlipUV5";
	rename -uid "070C7243-409E-DB1D-4E60-5E8236D9C0E2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[10]" "f[38]" "f[49:50]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.76632040739999996;
	setAttr ".pv" 0.1661940217;
createNode polyTweakUV -n "polyTweakUV7";
	rename -uid "85ECC6F3-4BC7-CDD5-3B4B-E5A31C32E34A";
	setAttr ".uopa" yes;
	setAttr -s 10 ".uvtk";
	setAttr ".uvtk[8]" -type "float2" -0.37275276 0.35025772 ;
	setAttr ".uvtk[9]" -type "float2" -0.41194615 0.49710482 ;
	setAttr ".uvtk[10]" -type "float2" -0.31486174 0.46332401 ;
	setAttr ".uvtk[23]" -type "float2" -0.33572519 0.59341347 ;
	setAttr ".uvtk[24]" -type "float2" -0.26171905 0.56324744 ;
	setAttr ".uvtk[30]" -type "float2" -0.20177801 0.40356639 ;
	setAttr ".uvtk[31]" -type "float2" -0.18134512 0.49964899 ;
	setAttr ".uvtk[141]" -type "float2" -0.15080304 0.26706934 ;
	setAttr ".uvtk[143]" -type "float2" -0.52798164 0.42248568 ;
createNode polyMapSewMove -n "polyMapSewMove5";
	rename -uid "4D356A38-48BE-CAC5-984A-808BD0C3DF76";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[23]" "e[82]";
createNode polyTweakUV -n "polyTweakUV8";
	rename -uid "702BDB2D-40E2-DBAD-E157-979540C19AEC";
	setAttr ".uopa" yes;
	setAttr -s 142 ".uvtk[8:141]" -type "float2" 0.087312013 0.12402469 0.089233845
		 0.14019841 0.097559392 0.13298517 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0.10064146 0.14676368 0.10681283 0.14082551 0 0 0 0 0 0 0 0 0 0 0.10653263 0.12226921
		 0.11238629 0.13213724 0 0 0 0 0 0 0 0 0 0 0.13599128 0.09966588 0.1113463 0.053367049
		 0.095896304 0.067724347 0.1172933 0.10420752 0.14125383 0.12020725 0.12220824 0.12362677
		 0.087429941 0.074426383 0.10584515 0.10667402 0.13087362 0.13839877 0.14867562 0.14058441
		 0.084068984 0.026068926 0.071970046 0.040700972 0.065876991 0.049810082 0 0 0 0 0
		 0 0 0 -0.093181938 0.70663202 -0.10632291 0.85765964 0.02630347 0.87063104 0.025452912
		 0.70847476 -0.19487071 0.56657732 -0.061061859 0.56836385 0.18873081 0.85940403 0.13092715
		 0.71024412 0.073790193 0.59079266 0 0 0 0 0.12702733 0.17503792 0.1451894 0.1737932
		 0.14850563 0.15898675 0.1296677 0.15860569 0 0 0 0 0 0 0 0 0.11726838 0.1679585 0.11778474
		 0.15911931 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.11736917 0.14818412 0.022845209
		 0.021439038 0.036098272 0.024552166 0.040482193 0.0045140851 0.024951108 0 0.06331563
		 0.0084760599 0.053382546 0.028204784 0.017213888 0.03801237 0.024278298 0.035821632
		 0.048306033 0.040796831 0.0021172585 0.021722712 0 0.0015557315 0.010141134 0.038456589
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0.074722588 0.13737148 0 0;
createNode polyMapSewMove -n "polyMapSewMove6";
	rename -uid "961108BA-4CFE-F7C0-2FE2-399E818ADB9D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[19]" "e[81]";
createNode polyFlipUV -n "polyFlipUV6";
	rename -uid "7B02E8A5-40C9-BB0B-4707-97ADCCFDB7BD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "f[3]" "f[9]" "f[41]" "f[71]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.2402060926;
	setAttr ".pv" 0.47821508350000003;
createNode polyFlipUV -n "polyFlipUV7";
	rename -uid "EDB54FAF-484F-D049-8FF1-9DA9710AAEE1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "f[3]" "f[9]" "f[41]" "f[71]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.2402060926;
	setAttr ".pv" 0.47821508350000003;
createNode polyTweakUV -n "polyTweakUV9";
	rename -uid "798F7C81-4824-0A5C-343C-A5BC635A6ABC";
	setAttr ".uopa" yes;
	setAttr -s 7 ".uvtk";
	setAttr ".uvtk[110]" -type "float2" -0.82272434 0.056662723 ;
	setAttr ".uvtk[111]" -type "float2" -0.80561858 0.11575596 ;
	setAttr ".uvtk[112]" -type "float2" -0.85925007 0.14938526 ;
	setAttr ".uvtk[113]" -type "float2" -0.90135777 0.1073242 ;
	setAttr ".uvtk[116]" -type "float2" -0.91538543 0.20220287 ;
	setAttr ".uvtk[117]" -type "float2" -0.98480344 0.18430816 ;
createNode polyMapSewMove -n "polyMapSewMove7";
	rename -uid "2040C649-4C05-8793-9C21-46874670BBC9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[24]";
createNode polyFlipUV -n "polyFlipUV8";
	rename -uid "42E2AACD-48C9-8192-9039-70A878A8D5CC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "f[3]" "f[7]" "f[9]" "f[41]" "f[59]" "f[71]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.2172792107;
	setAttr ".pv" 0.43417361380000002;
createNode polyTweakUV -n "polyTweakUV10";
	rename -uid "3CF16EFC-446C-F4EF-081C-2CAC20F22026";
	setAttr ".uopa" yes;
	setAttr -s 15 ".uvtk";
	setAttr ".uvtk[110]" -type "float2" 0.34758937 -1.0196218 ;
	setAttr ".uvtk[111]" -type "float2" 0.37496692 -0.96997583 ;
	setAttr ".uvtk[112]" -type "float2" 0.43182576 -0.98302913 ;
	setAttr ".uvtk[113]" -type "float2" 0.43183798 -1.0378774 ;
	setAttr ".uvtk[116]" -type "float2" 0.50282401 -0.98521453 ;
	setAttr ".uvtk[117]" -type "float2" 0.53638011 -1.0421226 ;
	setAttr ".uvtk[118]" -type "float2" 0.5983274 -0.85277045 ;
	setAttr ".uvtk[119]" -type "float2" 0.70570201 -0.64154118 ;
	setAttr ".uvtk[120]" -type "float2" 0.80545259 -0.63569808 ;
	setAttr ".uvtk[121]" -type "float2" 0.68107903 -0.87462848 ;
	setAttr ".uvtk[122]" -type "float2" 0.61648458 -0.95953935 ;
	setAttr ".uvtk[123]" -type "float2" 0.54954875 -0.95292294 ;
	setAttr ".uvtk[124]" -type "float2" 0.78573453 -0.44141373 ;
	setAttr ".uvtk[125]" -type "float2" 0.84741324 -0.44471234 ;
createNode polyMapSewMove -n "polyMapSewMove8";
	rename -uid "B2D8F25A-4BBE-6DE6-C60E-8AA88A2B1BAB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[31]";
createNode polyTweakUV -n "polyTweakUV11";
	rename -uid "DB45DC6F-491B-B10E-6378-5D8252CAC60C";
	setAttr ".uopa" yes;
	setAttr -s 7 ".uvtk";
	setAttr ".uvtk[74]" -type "float2" -0.22954269 0.37715638 ;
	setAttr ".uvtk[75]" -type "float2" -0.23861419 0.38406295 ;
	setAttr ".uvtk[76]" -type "float2" -0.24166338 0.3801353 ;
	setAttr ".uvtk[77]" -type "float2" -0.23664154 0.37553906 ;
	setAttr ".uvtk[81]" -type "float2" -0.25137824 0.39339608 ;
	setAttr ".uvtk[82]" -type "float2" -0.24812143 0.38534969 ;
createNode polyMapSewMove -n "polyMapSewMove9";
	rename -uid "D0921F31-4B1F-4C30-C7F0-CAA3198C25C5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[58]" "e[103]" "e[109]";
createNode polyTweakUV -n "polyTweakUV12";
	rename -uid "541DECB1-47F4-05C1-4D7E-DFB974D4318F";
	setAttr ".uopa" yes;
	setAttr -s 7 ".uvtk";
	setAttr ".uvtk[103]" -type "float2" 0.75212181 -0.25453767 ;
	setAttr ".uvtk[104]" -type "float2" 0.75525016 -0.28665394 ;
	setAttr ".uvtk[105]" -type "float2" 0.78855324 -0.28333676 ;
	setAttr ".uvtk[106]" -type "float2" 0.79001659 -0.2491352 ;
	setAttr ".uvtk[111]" -type "float2" 0.7619344 -0.19966856 ;
	setAttr ".uvtk[112]" -type "float2" 0.79406434 -0.19960892 ;
createNode polyMapSewMove -n "polyMapSewMove10";
	rename -uid "F8215564-400F-308D-FF2F-C391AFD24FE6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[41]" "e[67]";
createNode polyTweakUV -n "polyTweakUV13";
	rename -uid "42370CC9-4B62-EE29-8119-F7B9AFA58B8F";
	setAttr ".uopa" yes;
	setAttr -s 7 ".uvtk";
	setAttr ".uvtk[120]" -type "float2" 0.31823266 0.69367731 ;
	setAttr ".uvtk[121]" -type "float2" 0.24494314 0.7907145 ;
	setAttr ".uvtk[122]" -type "float2" 0.44252276 0.78568131 ;
	setAttr ".uvtk[123]" -type "float2" 0.44982249 0.68933523 ;
	setAttr ".uvtk[124]" -type "float2" 0.56006533 0.82336742 ;
	setAttr ".uvtk[125]" -type "float2" 0.61098522 0.7368108 ;
createNode polyMapSewMove -n "polyMapSewMove11";
	rename -uid "E1DE0ED8-424F-0029-824F-DE81A48B8DFB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[40]" "e[91]";
createNode polyTweakUV -n "polyTweakUV14";
	rename -uid "5C8B2B08-4030-B6B5-AB58-8C9E4268FA92";
	setAttr ".uopa" yes;
	setAttr -s 125 ".uvtk[0:124]" -type "float2" 0.51282012 -0.59972435 0.50630367
		 -0.59429681 0.50982654 -0.62435794 0.51361048 -0.62484145 0.49573198 -0.61032879
		 0.48867714 -0.60599995 0.48816717 -0.62084901 0.49551955 -0.62039578 -0.08597973
		 -0.12213224 -0.087548912 -0.13824445 -0.096070737 -0.13095593 0.46565652 -0.59524828
		 0.46433073 -0.62382805 0.47620028 -0.62188607 0.47594374 -0.5995692 0.48936179 -0.65432203
		 0.50973254 -0.65099734 0.50245816 -0.64073491 0.48900282 -0.64207494 0.49919564 -0.60316163
		 0.5023874 -0.6223098 0.51991647 -0.60466176 0.52106446 -0.62555122 -0.099938035 -0.14454782
		 -0.10518301 -0.1386767 0.49571842 -0.63281816 0.48872387 -0.63385236 0.4789795 -0.62009585
		 0.48009929 -0.60940439 0.48876163 -0.60063428 -0.10490704 -0.12040353 -0.1106714
		 -0.13012099 0.4773441 -0.64073491 0.47998202 -0.63281816 0.47394866 -0.6482349 0.51494414
		 -0.64611447 0.52079046 -0.64325404 -0.1339162 -0.098145097 -0.10964727 -0.05255273
		 -0.094433039 -0.066690952 -0.11550355 -0.10261744 -0.13909847 -0.11837304 -0.12034345
		 -0.12174034 -0.08609584 -0.073290735 -0.10423005 -0.10504627 -0.12887663 -0.13628697
		 -0.14640701 -0.13843924 -0.082786173 -0.025671147 -0.07087186 -0.040079921 -0.064871758
		 -0.049050033 0.54850942 -0.62909693 0.54743814 -0.58909476 0.5313552 -0.59192634
		 0.53256065 -0.62541747 -0.079969317 -0.15206158 -0.074379057 -0.16865957 -0.088088244
		 -0.17171824 -0.092387199 -0.15558445 -0.073396802 -0.13488114 -0.1049602 -0.17680621
		 -0.10342383 -0.15873027 0.53210843 -0.65385818 0.54844552 -0.65992129 -0.12508905
		 -0.17236704 -0.14297396 -0.17114133 -0.14623958 -0.15656078 -0.12768912 -0.15618557
		 -0.80386204 0.22294536 -0.81619537 0.22515796 -0.81710923 0.21004783 -0.80725729
		 0.20715623 -0.11547899 -0.16539562 -0.11598748 -0.15669131 -0.81351024 0.19216973
		 0.50695169 -0.59441793 0.48933455 -0.59298766 -0.78593343 0.21892929 -0.79280365
		 0.20378616 -0.80547243 0.18963397 0.46396619 -0.59132683 -0.11557829 -0.14592302
		 -0.022496611 -0.021111906 -0.03554745 -0.024177521 -0.03986448 -0.0044452064 -0.024570383
		 5.5511151e-017 -0.062349498 -0.0083467253 -0.052567989 -0.027774408 -0.016951226
		 -0.037432343 -0.02390784 -0.035275042 -0.047568947 -0.04017432 -0.0020849518 -0.02139125
		 0 -0.0015319926 -0.0099863932 -0.037869781 -0.040228009 0.86207092 -0.055598676 0.86190557
		 -0.05175215 0.87959361 -0.03949973 0.87990463 -0.077545524 0.85994816 -0.072365761
		 0.87964565 -0.017677814 0.87227118 -0.02284658 0.88198745 -0.10048503 0.87506092
		 -0.09288156 0.88145763 -0.80387902 0.23693164 -0.81568068 0.23740743 -0.10504282
		 0.99752659 -0.10279626 0.98868728 -0.11090678 0.98396575 -0.11694092 0.99040323 -0.7844764
		 0.23062307 -0.11948186 0.97641325 -0.1296804 0.97940314 -0.11612594 0.95036089 -0.10549814
		 0.91375393 -0.11656547 0.90209651 -0.12824458 0.94382513 -0.13000089 0.96089774 -0.12141532
		 0.96748328 0.55592161 -0.62737942 0.55480462 -0.59643406 -0.065629303 -0.17909497
		 -0.086396307 -0.18080056 -0.098291069 -0.18607932 0.55636078 -0.65266722 -0.821123
		 0.19455434;
createNode polyFlipUV -n "polyFlipUV9";
	rename -uid "BAB6A739-429A-50DB-BE64-F0ABCC3D7431";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "f[1]" "f[4:5]" "f[11]" "f[19]" "f[27:30]" "f[36]" "f[45:48]" "f[51:55]" "f[62:70]" "f[74:75]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.77818039059999999;
	setAttr ".pv" 0.17003935580000001;
createNode polyTweakUV -n "polyTweakUV15";
	rename -uid "16D75517-4105-4E64-08B6-73B64021A35F";
	setAttr ".uopa" yes;
	setAttr -s 42 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" -0.59885198 0.53107256 ;
	setAttr ".uvtk[1]" -type "float2" -0.52059513 0.57431543 ;
	setAttr ".uvtk[2]" -type "float2" -0.61681795 0.27007452 ;
	setAttr ".uvtk[3]" -type "float2" -0.65695632 0.27258891 ;
	setAttr ".uvtk[4]" -type "float2" -0.4430041 0.38731384 ;
	setAttr ".uvtk[5]" -type "float2" -0.36135671 0.41811162 ;
	setAttr ".uvtk[6]" -type "float2" -0.38558924 0.263356 ;
	setAttr ".uvtk[7]" -type "float2" -0.46081284 0.28266191 ;
	setAttr ".uvtk[11]" -type "float2" -0.10163864 0.48367739 ;
	setAttr ".uvtk[12]" -type "float2" -0.14471585 0.18513626 ;
	setAttr ".uvtk[13]" -type "float2" -0.26374942 0.22883376 ;
	setAttr ".uvtk[14]" -type "float2" -0.21673745 0.45938551 ;
	setAttr ".uvtk[15]" -type "float2" -0.46448675 -0.080838069 ;
	setAttr ".uvtk[16]" -type "float2" -0.6687907 -0.0059282929 ;
	setAttr ".uvtk[17]" -type "float2" -0.57307768 0.085867122 ;
	setAttr ".uvtk[18]" -type "float2" -0.43642834 0.045249864 ;
	setAttr ".uvtk[19]" -type "float2" -0.46462047 0.46840471 ;
	setAttr ".uvtk[20]" -type "float2" -0.53572392 0.276494 ;
	setAttr ".uvtk[21]" -type "float2" -0.68213892 0.49405611 ;
	setAttr ".uvtk[22]" -type "float2" -0.73554301 0.28005558 ;
	setAttr ".uvtk[25]" -type "float2" -0.48756182 0.15443973 ;
	setAttr ".uvtk[26]" -type "float2" -0.417198 0.12982954 ;
	setAttr ".uvtk[27]" -type "float2" -0.28896657 0.25289312 ;
	setAttr ".uvtk[28]" -type "float2" -0.27931049 0.36581481 ;
	setAttr ".uvtk[29]" -type "float2" -0.35156712 0.4738341 ;
	setAttr ".uvtk[32]" -type "float2" -0.31305471 0.035952166 ;
	setAttr ".uvtk[33]" -type "float2" -0.32463166 0.12316303 ;
	setAttr ".uvtk[34]" -type "float2" -0.29280588 -0.048448354 ;
	setAttr ".uvtk[35]" -type "float2" -0.71304572 0.05498521 ;
	setAttr ".uvtk[36]" -type "float2" -0.76789093 0.096220866 ;
	setAttr ".uvtk[50]" -type "float2" -1.0267469 0.29789174 ;
	setAttr ".uvtk[51]" -type "float2" -0.93614948 0.70993221 ;
	setAttr ".uvtk[52]" -type "float2" -0.77525973 0.64864933 ;
	setAttr ".uvtk[53]" -type "float2" -0.85430521 0.30428928 ;
	setAttr ".uvtk[61]" -type "float2" -0.90615016 0.0089243203 ;
	setAttr ".uvtk[62]" -type "float2" -1.0873493 -0.021380648 ;
	setAttr ".uvtk[74]" -type "float2" -0.52754569 0.57434952 ;
	setAttr ".uvtk[75]" -type "float2" -0.34230074 0.55414385 ;
	setAttr ".uvtk[79]" -type "float2" -0.076344013 0.52091873 ;
	setAttr ".uvtk[118]" -type "float2" -1.1000764 0.33040625 ;
	setAttr ".uvtk[119]" -type "float2" -1.0270065 0.64858478 ;
	setAttr ".uvtk[123]" -type "float2" -1.1548834 0.069457248 ;
createNode polyMapSewMove -n "polyMapSewMove12";
	rename -uid "3D992429-4B4F-3EDB-2A58-3F9D017F0F9D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[155]";
createNode polyMapSewMove -n "polyMapSewMove13";
	rename -uid "098D733C-432D-CAE7-3A4C-68B322FDD310";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[7]" "e[59]" "e[107]";
createNode polyTweakUV -n "polyTweakUV16";
	rename -uid "45099C5A-4CD7-505E-52D6-2786BD04389E";
	setAttr ".uopa" yes;
	setAttr -s 120 ".uvtk[0:119]" -type "float2" 0.061695397 -0.34105641 0.049182028
		 -0.37328535 0.075896487 -0.088457704 0.081142709 -0.095981061 0.058874071 -0.17133263
		 0.045184314 -0.18623424 0.048913449 -0.043656111 0.061314315 -0.073558658 0.008741349
		 -0.32511449 -0.045232862 -0.47531652 -0.045480013 -0.42062259 -0.0046063066 -0.21315315
		 0.022848099 0.072949886 0.036695331 0.010790884 0.022978693 -0.2034727 0.06743978
		 0.28527892 0.090116248 0.17772695 0.080352709 0.10040805 0.061755478 0.16170585 0.061644167
		 -0.25625867 0.068613321 -0.080913275 0.07582891 -0.3150205 0.091383025 -0.11344028
		 -0.064975441 -0.54416704 -0.059922487 -0.49892741 0.067518145 0.045609683 0.057133466
		 0.079054505 0.032643795 -0.018857121 0.031994432 -0.12401497 0.04666996 -0.24325925
		 -0.040953726 -0.33779824 -0.054337919 -0.42569256 0.045532912 0.18735012 0.041965038
		 0.098999023 0.049539506 0.27982122 0.09484069 0.11079603 0.10245183 0.06245324 -0.054403841
		 -0.16143996 0.0029022694 0.23778972 0.015063733 0.12408248 -0.034957826 -0.18643287
		 -0.075397313 -0.3362475 -0.055852771 -0.35370082 -0.074758172 -0.47910318 -0.08942169
		 -0.51112503 0.032350868 0.4764871 0.043071777 0.35532296 0.12642683 -0.16939047 0.099628225
		 -0.55765116 0.077033043 -0.47823301 0.10265106 -0.15348408 -0.056029201 -0.57765281
		 -0.057985991 -0.71470088 -0.079760313 -0.75600308 -0.075419039 -0.62099731 -0.031659722
		 -0.43042737 -0.1033701 -0.82441288 -0.091158718 -0.66218638 0.12105295 0.12754488
		 0.150364 0.13392228 -0.098221302 -0.78993213 -0.11087596 -0.79417074 -0.1024977 -0.6692614
		 -0.087673068 -0.65141881 0.1301904 -0.13920747 0.11927129 -0.13688341 0.12635025
		 -0.1471481 0.13442732 -0.15083119 -0.088093609 -0.72141099 -0.082269162 -0.64591873
		 0.11437221 -0.15317865 0.073457152 -0.3733457 0.14609173 -0.14323482 0.14543118 -0.15563506
		 0.11788665 -0.15262714 -0.077792406 -0.54968899 0.087792382 0.50313169 0.072527096
		 0.48208719 0.058551639 0.65011173 0.072732642 0.67997944 0.036176562 0.62459332 0.05322361
		 0.45993274 0.093332559 0.35198289 0.087947026 0.37789035 0.065204173 0.3503812 0.1066656
		 0.48747718 0.094459906 0.6524362 0.099824205 0.34388351 -0.26815552 -0.057723224
		 -0.33695543 -0.10901052 -0.25623989 -0.0044768453 -0.20081329 0.037395716 -0.44164985
		 -0.1913498 -0.34770769 -0.071890652 -0.13128082 0.07135886 -0.11850923 0.10318279
		 -0.48843664 -0.19237822 -0.43175375 -0.12990832 0.12820862 -0.12641536 0.11881575
		 -0.1254475 -0.044741154 0.45200622 -0.067544401 0.4170233 -0.12360191 0.36316448
		 -0.12715948 0.37244034 0.1457728 -0.13216674 -0.19130331 0.29304188 -0.22669953 0.2698586
		 -0.27991399 0.15709639 -0.36812145 -0.0078442097 -0.45680124 -0.11946899 -0.35456711
		 0.069574952 -0.29744959 0.1658752 -0.23977023 0.23456171 0.13173841 -0.21141747 0.11423596
		 -0.51058841 -0.05972442 -0.78315592 -0.092668593 -0.83067292 -0.10344416 -0.89347333
		 0.15488328 0.038232118 0.11267117 -0.15589333;
createNode polyMapSewMove -n "polyMapSewMove14";
	rename -uid "8DA6AD77-431D-70F4-10A4-FA92772070F6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[1]" "e[42]";
createNode polyMapSewMove -n "polyMapSewMove15";
	rename -uid "0544FCF8-4312-3DED-51FC-53BDF56EA52E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[12]" "e[14]" "e[17]" "e[39]" "e[55]" "e[78]";
createNode polyTweakUV -n "polyTweakUV17";
	rename -uid "EE53C317-4205-63FB-0943-639A20EAEEE8";
	setAttr ".uopa" yes;
	setAttr -s 111 ".uvtk[0:110]" -type "float2" 0.015491396 -0.0020612478
		 0.0044006407 0.00044578314 0.0029567331 0.0010401905 0.0037808865 0.00020059943 -0.0014649928
		 0.000338763 -0.0024830103 0.00045135617 -0.00020474195 0.0012941062 0.0001757741
		 0.0011028945 -0.0060571134 0.00060912967 -0.016280562 0.0012418032 -0.011136323 0.00026768446
		 -0.0030008852 -0.00074142218 0.00047010183 0.0014469028 -0.00020498037 0.001457572
		 -0.0031705201 0.00016885996 0.0003887713 0.0038036108 0.00020033121 0.001906395 0.001022473
		 0.002161026 0.00069779158 0.0028695166 -0.0025583208 -0.00053852797 0.0013405979
		 0.0010667145 0.013863519 -0.0025174618 0.0051254034 -0.0013567209 -0.015610248 0.0030505359
		 -0.012064904 0.00042565167 0.00081086159 0.0019704401 0.00055548549 0.0021933913
		 -0.00044861436 0.0013450682 -0.0018757284 0.00071871281 -0.0038711131 0.00036001205
		 -0.0065822899 -0.0010411292 -0.0080637038 -0.001482591 0.00076454878 0.0028924346
		 0.00038391352 0.0022495091 0.0010714233 0.0036569834 0.00097315013 0.00078892708
		 0.0018571168 -0.00047445297 -0.0013782382 -0.0025200546 0.0027706623 0.0013125539
		 0.0010848939 0.0013746321 -0.002419591 -0.0013915896 -0.003770411 -0.0037709028 -0.0052562356
		 -0.0023767799 -0.0059816837 -0.0037330389 -0.0051900148 -0.0054764599 0.003036797
		 0.0043206811 0.0017023385 0.0039773583 0.0059214234 -0.0068012476 0.018862352 -0.0065583289
		 0.0190918 -0.0033073276 0.0065731555 -0.0037877858 0.00076457858 -0.0043967217 0.00012537837
		 -0.015344694 -0.0066903234 -0.0014432296 -0.018408805 0.0019767433 0.00512743 0.013932187
		 -0.013539076 0.0088875815 0.00035512447 -0.0028745532 -0.00082737952 -0.0059651732
		 -0.0093825758 -0.0052426122 -0.0083390474 -0.0068570878 -0.0068457723 -0.0064030215
		 -0.0077959299 -0.0046672374 0.015294939 -0.0051403716 0.022070229 -0.0064800233 -0.0093028843
		 -0.0039399043 -0.0085001588 -0.0035324469 0.015242785 0.0057945848 -0.0075834394
		 -0.0028282404 -0.00080950558 0.0071126819 -7.4625015e-005 0.0064552426 0.0008867681
		 0.0076262951 -2.2307038e-005 0.0084583163 0.0022539496 0.0065187812 0.00092893839
		 0.005649209 -0.0020122081 0.006116569 -0.0013542026 0.0060566664 0.00016155839 0.0049320459
		 -0.0023241267 0.007786274 -0.0018960685 0.0092301369 -0.0025411844 0.0063268542 0.5998565
		 -0.11808726 0.53981298 -0.094914705 0.52646333 -0.17660674 0.57413769 -0.19581357
		 0.456815 -0.054030687 0.44554287 -0.14646605 0.67424357 -0.1962274 0.63597918 -0.23032889
		 0.33728129 -0.083754122 0.36172289 -0.12473905 0.0015558153 -0.001034379 -0.0021719635
		 -0.025847949 0.13519245 -0.64322412 0.16282594 -0.60944295 0.1372788 -0.57224751
		 0.098261416 -0.58867544 0.0079022497 0.009732537 0.11408371 -0.52286625 0.06381762
		 -0.51679742 0.16160542 -0.39806417 0.25339609 -0.24655914 0.21066785 -0.17452538
		 0.10742968 -0.3473537 0.086213768 -0.42977294 0.11879349 -0.46957898 0.0063874349
		 -0.0085065365 0.016302861 -0.0081523061 0.0047427714 -0.0056833699 0.013409913 0.0069222348
		 0.00060870918 -0.0077478588;
createNode polyMapSewMove -n "polyMapSewMove16";
	rename -uid "B3E2C0FD-4681-57B8-78B7-3C9DAD74E1FD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[151]";
createNode polyTweakUV -n "polyTweakUV18";
	rename -uid "534951B7-4C93-BE0B-ABBF-10A4C4BFE442";
	setAttr ".uopa" yes;
	setAttr -s 109 ".uvtk[0:108]" -type "float2" 0.048304915 0.03528446 0.05499813
		 0.032650292 0.042771816 0.055973828 0.039873704 0.055330962 0.058209687 0.049831241
		 0.06423679 0.048960567 0.060468346 0.059890747 0.055521458 0.057527661 0.068006366
		 0.038320124 0.064692825 0.026397258 0.073044032 0.02981019 0.085589617 0.046959281
		 0.078884155 0.069522947 0.06997633 0.064460069 0.075280279 0.047661841 0.050617695
		 0.08686322 0.03530544 0.07765764 0.044755384 0.071360856 0.054438144 0.076432467
		 0.057606608 0.042699337 0.049420387 0.056731462 0.041660979 0.037643582 0.03418459
		 0.053846866 0.072059661 0.019693866 0.078217447 0.022323027 0.052066326 0.066907883
		 0.056830883 0.069464087 0.067029327 0.061854035 0.069390982 0.053797573 0.066112995
		 0.044010311 0.082432926 0.035432979 0.084768325 0.027836204 0.063777715 0.078243405
		 0.063256502 0.0709894 0.06528312 0.086418033 0.03280209 0.072083503 0.02947025 0.068054199
		 0.10631943 0.047828674 0.096280634 0.081394881 0.084874183 0.073053867 0.093558967
		 0.047393024 0.10659236 0.033440962 0.093469203 0.033460841 0.097359419 0.02308698
		 0.11017388 0.018816009 0.078187406 0.10192525 0.069470644 0.092387915 0.011657648
		 0.049010426 0.024708122 0.018378273 0.035961777 0.024795309 0.024916336 0.050404847
		 0.054764479 0.019414768 0.05076009 0.0065598339 0.060619354 0.0040417239 0.063037187
		 0.015153095 0.073236704 -0.0011722296 0.070715368 0.011658214 0.017291129 0.072850227
		 0.0036881752 0.073231995 0.089147598 -0.0022675581 0.10268456 -0.0044072419 0.10734206
		 0.0056826472 0.093635261 0.0089229196 0.041713923 0.010471635 0.047141135 0.021774724
		 0.082597256 0.0043583065 0.084250301 0.010575004 0.029028043 0.015736878 0.084875226
		 0.018819317 0.034825131 0.10330057 0.04359898 0.10202783 0.045005172 0.11559844 0.034038559
		 0.11730379 0.060855359 0.11415982 0.055160105 0.10099077 0.030762285 0.089851797
		 0.035989165 0.092675149 0.052828729 0.091743767 0.020124413 0.10081434 0.016152784
		 0.11376476 0.025753349 0.088796616 0.13479525 0.13796878 0.12476259 0.13298869 0.13367534
		 0.12167132 0.14174491 0.12553048 0.10975862 0.12714553 0.12029415 0.11483163 0.15356731
		 0.13832688 0.15344435 0.1294077 0.099519134 0.10831308 0.10765165 0.10663074 0.038481444
		 0.0013313703 0.045920849 -0.0013317503 0.14754796 0.016605929 0.14646214 0.024123311
		 0.13868588 0.025218278 0.13620663 0.018280357 0.026040837 0.0082949623 0.12962252
		 0.028047696 0.1229341 0.02231133 0.1191957 0.04881826 0.11054707 0.078408569 0.11440068
		 0.035416439 0.1233387 0.034916967 0.00583167 0.045462459 0.016790234 0.021907464
		 0.05886963 -0.0023928788 0.068895906 -0.0069222348 -0.00060870918 0.065673649;
createNode polyMapSewMove -n "polyMapSewMove17";
	rename -uid "8FF461DC-4BDA-2769-9F85-C8A4BE43CC06";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[2]" "e[13]" "e[76]" "e[117]";
createNode polyTweakUV -n "polyTweakUV19";
	rename -uid "60705CEE-4779-E07F-2C74-C49770A3B2CE";
	setAttr ".uopa" yes;
	setAttr -s 105 ".uvtk[0:104]" -type "float2" -0.0065558851 0.012058705
		 -0.0061919093 0.0076649487 0.0024926364 0.017192215 0.0019080341 0.019682944 0.0025855601
		 0.0072171688 0.0035790801 0.0045436025 0.0073019862 0.0058858693 0.0054803491 0.0086770654
		 0.00017693639 0.0023622811 -0.0064841807 0.0025843084 -0.0022780001 -0.00010980666
		 0.0080271363 -0.00058439374 0.012599111 0.0023399293 0.010262758 0.0029526651 0.0057937801
		 0.0013636053 0.015561998 -0.0069982409 0.017383158 -0.021757901 0.012897074 0.016250312
		 0.014474064 0.0073744655 -0.00075703859 0.0071502626 0.0040171146 0.012438953 -0.0068493187
		 0.016672194 0.00068050623 0.024431169 -0.0080031455 -0.0016097575 -0.004355371 -0.003151536
		 0.009812206 0.010296077 0.011033267 0.007116586 0.0089868605 0.0036967397 0.006595403
		 0.0029862821 0.0020338595 0.0035980642 0.0029855967 -0.0017315447 0.00092500448 -0.0036609024
		 0.013088048 0.0023344159 0.011444032 0.0038372278 0.013661712 -0.00050485134 0.020782396
		 -0.028129697 0.022265986 -0.034576058 0.012506485 -0.0013011694 0.014339387 0.0061882138
		 0.013531268 0.0032438636 0.0098944902 -0.00097361207 0.0081787705 -0.006123811 0.0054301023
		 -0.004034102 0.0038157701 -0.0065051466 0.0052655935 -0.0099775493 -0.0035500526
		 0.011064827 0.0068671405 0.0025155544 -0.0051833317 0.040520996 -0.021920711 0.025381163
		 -0.015181318 0.018390372 -0.0024396032 0.031021059 -0.012979239 0.0065015107 -0.020680666
		 0.0067726448 -0.01983431 0.0011983886 -0.013119489 0.001731351 -0.020382881 -0.0062327627
		 -0.013610333 -0.0026941523 0.012615368 0.041345537 0.011109918 0.051014423 -0.0031519532
		 -0.0092578065 -0.0011725426 -0.012578109 0.0018675923 -0.011764839 8.72612e-005 -0.0082205832
		 -0.020693362 0.012414806 -0.013850212 0.011022195 -0.0028932095 -0.0066432729 -0.0012454987
		 -0.0058850944 -0.020250827 0.020531178 0.00065469742 -0.0045420527 -0.010685027 -0.075818121
		 -0.01015228 -0.051241755 -0.041259199 -0.050828218 -0.045262873 -0.081818819 -0.032870144
		 -0.014759541 -0.0090128779 -0.021956325 0.0020913035 -0.1166085 -0.03038957 -0.13085663
		 0.0087526441 0.0050721169 0.0095760822 0.0047265291 0.0097941756 0.0058538914 0.0091407895
		 0.006139636 0.010710597 0.0041261315 0.010904312 0.005403161 0.0077525377 0.0061860681
		 0.0082971454 0.0066435337 0.012386262 0.0045070052 0.012057543 0.0050667524 -0.026118949
		 0.012725074 -0.025930107 0.0081533119 0.012366116 0.0031822771 0.011969209 0.0027250946
		 0.012308478 0.0021988004 0.012856245 0.0024107844 -0.024715796 0.02094008 0.012610137
		 0.0015046597 0.013304889 0.0014002025 0.01194191 -8.4966421e-005 0.013142288 0.0053336024
		 0.012954593 0.00018888712 0.012523353 0.00076729059 -0.008417882 0.044087023 -0.021210134
		 0.03172192 -0.023632765 0.0010634381 -0.024204105 -0.0049155205 0.0050357436 0.052743793;
createNode polyMapSewMove -n "polyMapSewMove18";
	rename -uid "34AE29DA-4F41-B5CD-6BFF-548F9E788773";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[8]" "e[43]";
createNode polyTweakUV -n "polyTweakUV20";
	rename -uid "1F6ABC03-4FBE-2DA0-9D23-08BFBDBB6D08";
	setAttr ".uopa" yes;
	setAttr -s 103 ".uvtk[0:102]" -type "float2" -0.0015083253 -0.0020276904
		 -0.0015141964 -0.0026892722 0.0038013756 -0.0025363266 0.0037495792 -0.0023886859
		 0.0018112361 -0.0021803975 0.0017033815 -0.002004981 0.0024887621 -0.0014573634 0.0026753247
		 -0.0019220412 0.00071805716 -0.0026369393 -0.0016715527 -0.0032224804 -0.00014582276
		 -0.0030964464 0.0022706389 -0.0010128021 0.0016511679 0.0010030866 0.0019981861 -0.00028499961
		 0.0019073784 -0.0015135407 -0.0013003647 -0.00043487549 0.003718257 -0.0047741532
		 0.0032453239 -0.0026082993 0.0013548732 -0.00071978569 0.00076827407 -0.0025055707
		 0.0031914115 -0.0023031533 -0.001609832 -0.0011358857 0.0031589717 -0.0011845231
		 -0.0021089911 -0.0040907115 -0.00060802698 -0.0037295818 0.0027869344 -0.0017337799
		 0.0021300614 -0.0010273457 0.0021369457 -0.00076469779 0.0019913912 -0.0013283193
		 0.0012841821 -0.0022783279 0.001388371 -0.0022770464 0.0011504889 -0.0028102547 0.0006480813
		 0.0005286932 0.001609832 -0.00022977591 -0.00090357661 0.0015690327 0.0063495487
		 -0.0049963593 0.0085147917 -0.0051722527 0.0031626225 0.00043472648 0.00086170435
		 0.0036821961 0.0013676286 0.0018978715 0.0025924444 -0.0004029572 0.0030407906 -0.0012884438
		 0.0022419095 -0.0018354058 0.0025734305 -0.0022336543 0.0033444762 -0.0022014529
		 -0.0051652193 0.004891932 -0.0025024712 0.0027222037 -0.00013239682 0.014493406 -0.011987448
		 0.0063630491 -0.006663084 0.00092582405 0.0011017174 0.0030981898 -0.0043660998 -0.0025973916
		 -0.0070459843 -0.0022876114 -0.0065432787 -0.0038302988 -0.0041977167 -0.0036864877
		 -0.0067563355 -0.0059480909 -0.00441733 -0.0048827454 0.01369074 -0.0088358521 0.010423742
		 -0.010330975 0.0025817752 -0.0036928393 0.0033319592 -0.0035710754 0.0034077168 -0.0029574409
		 0.0026264787 -0.0030294731 -0.0073603541 -0.00061693043 -0.0051184893 -0.0015191287
		 0.0021239519 -0.0034574494 0.0021044612 -0.0031035244 -0.007335335 0.0019948035 0.0019952655
		 -0.0026623458 -0.0024857372 -0.0096347928 -0.004288435 -0.0046194792 -0.015204012
		 -0.0069785714 -0.018339112 -0.018163979 -0.011961341 0.00052899122 -0.0055205226
		 -0.0001667738 -0.0007661581 0.0087504983 -0.00095140934 0.0078823566 0.00019043684
		 0.0078778863 0.00034850836 0.0085690022 -0.0013279915 0.0066630244 -4.4465065e-005
		 0.0067101121 0.00013673306 0.0099334717 0.00068438053 0.0094863176 -0.00066310167
		 0.0051031709 -0.00015842915 0.0055209398 -0.0090188831 -0.00032184273 -0.0087763369
		 -0.0017343201 0.0071430206 0.0015385151 0.0066228509 0.0018415749 0.0061717033 0.0014128089
		 0.0064801574 0.00091700256 -0.0086501837 0.0022992566 0.0055493712 0.00098961592
		 0.0055758953 0.00029173493 0.003749609 0.0013934076 0.0016974211 0.0039994121 0.0043414831
		 0.00040480494 0.0048133135 0.00093799829 -0.0019485764 0.019819587 -0.011254407 0.011332735
		 -0.0077472627 -0.0038203597 -0.0079131126 -0.00551954 0.021701297 0.033200741;
createNode polyMapSewMove -n "polyMapSewMove19";
	rename -uid "340A6E3E-4A7D-A687-080B-D7A93F603213";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[25]" "e[51]" "e[104]";
createNode polyTweakUV -n "polyTweakUV21";
	rename -uid "5D85E21D-42D0-32CF-E665-969246EA8857";
	setAttr ".uopa" yes;
	setAttr -s 100 ".uvtk[0:99]" -type "float2" 0.0077652037 -0.010708809
		 0.0091824532 -0.0085840821 5.3018332e-005 -0.0092095435 -2.1100044e-005 -0.010333121
		 0.0031735599 -0.0045268238 0.0036646426 -0.0023501217 0.0005068779 -0.0025544167
		 0.00074711442 -0.0045456886 0.0065914989 -0.0019247532 0.012871236 -0.0059241205
		 0.0098340511 -0.00076700747 0.0048443079 0.0051654279 4.3511391e-006 0.0038129389
		 0.00024616718 0.0008765161 0.0043698549 0.001596719 -0.0075896382 -0.0030227304 -0.0070564896
		 -0.008603096 -0.0041554272 -0.0064859986 -0.004399389 -0.0029392242 0.0054242611
		 -0.0057522357 0.00044202805 -0.0067695677 0.0062809885 -0.0127877 -0.00015000999
		 -0.012558728 -0.018350601 0.046921 -0.0073103309 0.041405693 -0.0021447241 -0.0046561062
		 -0.0023311079 -0.0028684437 0.00058689713 -0.00024574995 0.0026602149 -5.2899122e-005
		 0.0050576329 -0.0021288395 0.0060225725 0.0045394599 0.0054776073 0.036157504 -0.0038107038
		 -5.2630901e-005 -0.0020600259 -0.00072479248 -0.005687356 0.00090450048 -0.0057574213
		 -0.010144651 -0.0049594194 -0.011773944 0.0074187517 0.0053675473 -0.00014698505
		 0.0082855821 -1.1086464e-005 0.0054846406 0.0056609511 0.0062038004 0.0060839057
		 -0.0005889833 0.0050585866 0.008074373 -0.0023712516 0.012617514 -0.023587942 -0.016375914
		 -0.0092429817 0.0047771931 -0.007050544 0.002184689 -0.0018399507 -0.020858496 0.011050507
		 -0.021976069 0.010570064 -0.017280355 -0.00021973252 -0.016322702 0.015768081 -0.011047468
		 0.021565944 -0.014508046 0.024532169 -0.010515779 0.019713521 -0.0083813667 0.028805465
		 -0.0050677862 0.022641838 -0.0050142035 -0.0081011653 -0.014559686 -0.011026096 -0.018024027
		 -0.08608678 0.07725703 -0.10428029 0.02851956 -0.070326805 0.0025952458 -0.048038304
		 0.044623338 0.018233851 -0.017675787 0.013522506 -0.013612241 0.014104158 -0.022263259
		 -0.010258332 -0.0098142028 -0.010343283 -0.0066649318 -0.014703348 -0.0071033239
		 -0.014585719 -0.011547029 -0.014162898 -0.0011815429 -0.010271817 -0.0026010871 -0.0037904978
		 0.017920494 -0.0040311217 0.016234636 -0.0018455386 0.016375899 -0.0016336441 0.017719269
		 -0.004592061 0.013851881 -0.0021417141 0.014110446 -0.0022178888 0.020302713 -0.0011113882
		 0.019518793 -0.0031196475 0.010948777 -0.0022034645 0.011820138 0.022047237 -0.020404663
		 0.024512172 -0.017627906 0.014788508 0.0078897178 0.013753295 0.0084014237 0.012946129
		 0.0075217038 0.013601601 0.0066133738 0.017093673 -0.024699822 0.011810839 0.0066302121
		 0.011953056 0.0052981824 0.0082797408 0.007129699 0.0019307137 0.0095713735 0.0095795989
		 0.0053566992 0.010408998 0.0064349473 -0.0015992019 -0.023369968 0.0085407421 -0.023952931
		 0.027331829 -0.012098076 0.030984372 -0.0078720404 -0.0093738604 -0.021015167;
createNode polyMapSewMove -n "polyMapSewMove20";
	rename -uid "E9F339DC-460D-045A-3EB3-45BA53878E9F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[37]" "e[53]";
createNode polyTweakUV -n "polyTweakUV22";
	rename -uid "44964733-425E-DEE4-FCF0-9D80671062BC";
	setAttr ".uopa" yes;
	setAttr -s 98 ".uvtk[0:97]" -type "float2" -0.019073604 0.001999855
		 -0.020205824 0.0040403605 -0.019384114 0.00086829066 -0.019108085 0.00042447448 -0.020686893
		 0.0030576885 -0.021263598 0.0035148561 -0.020475714 0.002162993 -0.020261316 0.0021032989
		 -0.022496281 0.0058783591 -0.022664694 0.0081413984 -0.024621278 0.0080276728 -0.023074267
		 0.0011621118 -0.020446716 0.00014400482 -0.020596204 0.0014234185 -0.022222308 0.002974391
		 -0.019230751 5.7578087e-005 -0.018881334 -0.00042074919 -0.019359468 0.00052314997
		 -0.01960185 0.00072562695 -0.020766972 0.0037275851 -0.019868372 0.0016411245 -0.018299339
		 0.00020805001 -0.018490775 -0.00046083331 -0.027494103 0.01407969 -0.029327899 0.0095185488
		 -0.019804446 0.0012356937 -0.019920258 0.0012125373 -0.020656226 0.0018330812 -0.0213401
		 0.0026854575 -0.021755395 0.0044053793 -0.025070369 0.0039378405 -0.028042138 0.0048180073
		 -0.019775985 0.00055229664 -0.020081637 0.001121521 -0.019496379 2.9504299e-005 -0.018756924
		 -0.00056624413 -0.018504275 -0.00089919567 -0.025070844 -0.0060700476 -0.019522665
		 -0.0031327009 -0.020236669 -0.00092852116 -0.023792205 -0.0015004873 -0.030678034
		 -0.0089579821 -0.027486444 -0.00091943145 -0.034189343 -0.0024799556 -0.044861257
		 -0.011097282 -0.019176243 -0.0013176799 -0.019317983 -0.00045454502 -0.0160762 -0.003565222
		 -0.013137652 -0.0040991604 -0.015172256 -0.0030697733 -0.017431585 -0.0019281805
		 -0.01611313 0.0036415309 -0.0039957743 -0.00049044937 -0.00051528029 0.0057825521
		 -0.017124472 0.0079131275 -0.029495776 0.01883591 -0.030152082 0.019700468 -0.017939754
		 -0.0020332336 -0.01732881 -0.0033398867 -0.058376133 -0.0013224334 -0.03924042 0.0075272173
		 -0.0066497605 -0.0045664534 -0.015441446 0.00044322014 -0.0088543277 -0.011204481
		 -0.018577499 -0.0011978149 -0.018828465 -0.00074052811 -0.018631143 -0.0013824701
		 -0.018295825 -0.0021092892 -0.018847881 -0.0011106133 -0.019019721 -0.00045531988
		 -0.011004744 -0.012676477 -0.011220155 -0.010501206 -0.01421815 -0.011971891 -0.014074503
		 -0.013717234 -0.011217533 -0.0072950721 -0.014545558 -0.0090648532 -0.012416838 -0.016483784
		 -0.014214156 -0.016197264 -0.014226673 -0.004668355 -0.015206872 -0.0062674284 0.00063878484
		 -0.0074494667 0.003422888 -0.0034232177 -0.034734309 -0.013422057 -0.033125401 -0.013422951
		 -0.032288432 -0.011882275 -0.033497989 -0.011177287 -0.0036870223 -0.013903968 -0.030998349
		 -0.010132119 -0.031631589 -0.0086110681 -0.025990903 -0.0086090565 -0.020867167 -0.0062901378
		 -0.028298676 -0.0072720945 -0.029109478 -0.0090627074 -0.015216634 -0.0044136345
		 -0.012948206 -0.0047034621 0.0088914335 0.0049621444 0.02061072 0.018307101 -0.016500492
		 -0.0041447878;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "polyTweakUV22.out" "pCubeShape1.i";
connectAttr "polyTweakUV22.uvtk[0]" "pCubeShape1.uvst[0].uvtw";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polySurfaceShape1.o" "polyMapCut1.ip";
connectAttr "polyMapCut1.out" "polyTweakUV1.ip";
connectAttr "polyTweakUV1.out" "polyFlipUV1.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV1.mp";
connectAttr "polyFlipUV1.out" "polyTweakUV2.ip";
connectAttr "polyTweakUV2.out" "polyMapSewMove1.ip";
connectAttr "polyMapSewMove1.out" "polyFlipUV2.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV2.mp";
connectAttr "polyFlipUV2.out" "polyTweakUV3.ip";
connectAttr "polyTweakUV3.out" "polyMapSewMove2.ip";
connectAttr "polyMapSewMove2.out" "polyFlipUV3.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV3.mp";
connectAttr "polyFlipUV3.out" "polyTweakUV4.ip";
connectAttr "polyTweakUV4.out" "polyMapSewMove3.ip";
connectAttr "polyMapSewMove3.out" "polyFlipUV4.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV4.mp";
connectAttr "polyFlipUV4.out" "polyTweakUV5.ip";
connectAttr "polyTweakUV5.out" "polyMapSewMove4.ip";
connectAttr "polyMapSewMove4.out" "polyTweakUV6.ip";
connectAttr "polyTweakUV6.out" "polyFlipUV5.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV5.mp";
connectAttr "polyFlipUV5.out" "polyTweakUV7.ip";
connectAttr "polyTweakUV7.out" "polyMapSewMove5.ip";
connectAttr "polyMapSewMove5.out" "polyTweakUV8.ip";
connectAttr "polyTweakUV8.out" "polyMapSewMove6.ip";
connectAttr "polyMapSewMove6.out" "polyFlipUV6.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV6.mp";
connectAttr "polyFlipUV6.out" "polyFlipUV7.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV7.mp";
connectAttr "polyFlipUV7.out" "polyTweakUV9.ip";
connectAttr "polyTweakUV9.out" "polyMapSewMove7.ip";
connectAttr "polyMapSewMove7.out" "polyFlipUV8.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV8.mp";
connectAttr "polyFlipUV8.out" "polyTweakUV10.ip";
connectAttr "polyTweakUV10.out" "polyMapSewMove8.ip";
connectAttr "polyMapSewMove8.out" "polyTweakUV11.ip";
connectAttr "polyTweakUV11.out" "polyMapSewMove9.ip";
connectAttr "polyMapSewMove9.out" "polyTweakUV12.ip";
connectAttr "polyTweakUV12.out" "polyMapSewMove10.ip";
connectAttr "polyMapSewMove10.out" "polyTweakUV13.ip";
connectAttr "polyTweakUV13.out" "polyMapSewMove11.ip";
connectAttr "polyMapSewMove11.out" "polyTweakUV14.ip";
connectAttr "polyTweakUV14.out" "polyFlipUV9.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV9.mp";
connectAttr "polyFlipUV9.out" "polyTweakUV15.ip";
connectAttr "polyTweakUV15.out" "polyMapSewMove12.ip";
connectAttr "polyMapSewMove12.out" "polyMapSewMove13.ip";
connectAttr "polyMapSewMove13.out" "polyTweakUV16.ip";
connectAttr "polyTweakUV16.out" "polyMapSewMove14.ip";
connectAttr "polyMapSewMove14.out" "polyMapSewMove15.ip";
connectAttr "polyMapSewMove15.out" "polyTweakUV17.ip";
connectAttr "polyTweakUV17.out" "polyMapSewMove16.ip";
connectAttr "polyMapSewMove16.out" "polyTweakUV18.ip";
connectAttr "polyTweakUV18.out" "polyMapSewMove17.ip";
connectAttr "polyMapSewMove17.out" "polyTweakUV19.ip";
connectAttr "polyTweakUV19.out" "polyMapSewMove18.ip";
connectAttr "polyMapSewMove18.out" "polyTweakUV20.ip";
connectAttr "polyTweakUV20.out" "polyMapSewMove19.ip";
connectAttr "polyMapSewMove19.out" "polyTweakUV21.ip";
connectAttr "polyTweakUV21.out" "polyMapSewMove20.ip";
connectAttr "polyMapSewMove20.out" "polyTweakUV22.ip";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
// End of Rock.ma
