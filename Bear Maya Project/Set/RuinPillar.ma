//Maya ASCII 2017ff05 scene
//Name: RuinPillar.ma
//Last modified: Mon, Nov 27, 2017 01:20:13 PM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "A05466BD-4DCE-BD49-1184-479BEC2EDE28";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 5.2275961134848874 5.188509487416753 -2.9147593937679597 ;
	setAttr ".r" -type "double3" -27.938352730222775 482.99999999995407 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "BE068E75-4794-8999-7172-56949C45A4C9";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 6.6475559535053286;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "070AA1E2-4CF5-5C99-E699-C898211A0B56";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "B1DF282D-4017-79E1-1A56-15AEDAF44CDB";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "9AC372DC-4B46-ECE8-511A-7597EBAB5170";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "739F7E53-4DF2-4638-CB1A-4A8244EC693E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "01707DCC-454C-AE09-53D0-589299BE5A02";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "9CBFD602-481A-BB27-A7E5-3D8BE236F6D5";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCube1";
	rename -uid "E0850D14-4259-36D7-8730-4499A18C1036";
	setAttr ".t" -type "double3" 0 0.32217298865632915 0 ;
	setAttr ".s" -type "double3" 1 0.60728689018988002 1 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "1DF96542-4086-5BAC-077F-EBBCE1B03AAB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.32954201474785805 0.8290596604347229 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "E6887ECE-4FD1-2E92-D20B-F1A2C8D17C21";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "30FEB287-4327-8B2E-6859-C596FD3C1340";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "9EEDCD91-4BC9-343E-BB55-2D936CC10F52";
createNode displayLayerManager -n "layerManager";
	rename -uid "CDB7ADD0-4536-0AE4-62D2-81B05FD9E02C";
createNode displayLayer -n "defaultLayer";
	rename -uid "F9272589-40A4-70D1-E845-798D77D3B4FA";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "FA67E704-4199-D508-7D6F-1F95B4D31B94";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "F2A084CD-4D74-D4FC-80DC-E6A96B3FAA3F";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "5E633418-4290-CFC7-82E9-B09FC29F8A2F";
	setAttr ".cuv" 4;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "4E3E0689-4FB1-C79F-D679-4A89D5303EED";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 0.6258164 0 ;
	setAttr ".rs" 65118;
	setAttr ".lt" -type "double3" 0 1.2325951644078309e-032 0.29160068824725405 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.5 0.62581643375126916 -0.5 ;
	setAttr ".cbx" -type "double3" 0.5 0.62581643375126916 0.5 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "340E96AB-4EBA-1559-2E81-F989FCCB7BDF";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 0.91741711 0 ;
	setAttr ".rs" 44941;
	setAttr ".lt" -type "double3" 0 -2.9582283945787943e-031 2.4489938855422011 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.35587623715400696 0.91741713343008757 -0.35587623715400696 ;
	setAttr ".cbx" -type "double3" 0.35587623715400696 0.91741713343008757 0.35587623715400696 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "95122FAE-4F5C-001B-E990-358113FE2230";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[8:11]" -type "float3"  0.14412376 0 -0.14412376 -0.14412376
		 0 -0.14412376 -0.14412376 0 0.14412376 0.14412376 0 0.14412376;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "CED3E81A-4A1A-3947-184A-A69EE44F9CFE";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 3.3664112 0 ;
	setAttr ".rs" 61668;
	setAttr ".lt" -type "double3" 0 7.3955709864469857e-032 0.44673418599527714 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.35587623715400696 3.3664112489641962 -0.35587623715400696 ;
	setAttr ".cbx" -type "double3" 0.35587623715400696 3.3664112489641962 0.35587623715400696 ;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "D194216A-4321-05C4-0D61-FE94C4DAD15C";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 3.8131456 0 ;
	setAttr ".rs" 41046;
	setAttr ".lt" -type "double3" 0 3.6977854932234928e-031 0.82287265784445829 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.51279610395431519 3.8131456714154011 -0.51279610395431519 ;
	setAttr ".cbx" -type "double3" 0.51279610395431519 3.8131456714154011 0.51279610395431519 ;
createNode polyTweak -n "polyTweak2";
	rename -uid "30AC7BC9-4789-25D2-FE00-39862C0C7760";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[16:19]" -type "float3"  -0.15691987 0 0.15691987 0.15691987
		 0 0.15691987 0.15691987 0 -0.15691987 -0.15691987 0 -0.15691987;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "450756A1-43C7-EBFD-23E5-619F374E77CA";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 4.6360188 0 ;
	setAttr ".rs" 65103;
	setAttr ".lt" -type "double3" 0 -1.2325951644078309e-031 0.44776546321032384 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.51279610395431519 4.6360188400518574 -0.51279610395431519 ;
	setAttr ".cbx" -type "double3" 0.51279610395431519 4.6360188400518574 0.51279610395431519 ;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "1D161269-4458-185A-0183-368679FCC54B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[44:45]" "e[47]" "e[49]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".wt" 0.46500840783119202;
	setAttr ".re" 47;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak3";
	rename -uid "40DBED32-4B53-145B-6ADF-8B9EEBC3C0E4";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk[16:27]" -type "float3"  0.050970413 -0.29676449 -0.050970413
		 -0.050970413 -0.29676449 -0.050970413 -0.050970413 -0.29676449 0.050970413 0.050970413
		 -0.29676449 0.050970413 0.050970413 -0.10310122 -0.050970413 -0.050970413 -0.10310122
		 -0.050970413 -0.050970413 -0.10310122 0.050970413 0.050970413 -0.10310122 0.050970413
		 0.41573134 0 -0.41573134 -0.41573134 0 -0.41573134 -0.41573134 0 0.41573134 0.41573134
		 0 0.41573134;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "BFCC5274-4212-1576-21B7-E99FD2E473B8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[52:53]" "e[55]" "e[57]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".wt" 0.5612868070602417;
	setAttr ".dr" no;
	setAttr ".re" 53;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "D0957B8F-4BEB-4118-5083-54973496D6F2";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[28:31]" -type "float3"  0.068498425 0 -0.068498425
		 0.068498425 0 0.068498425 -0.068498425 0 0.068498425 -0.068498425 0 -0.068498425;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "C27E9B8F-4C7A-351D-29DA-B8809F2932D2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[12:13]" "e[15]" "e[17]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".wt" 0.69613474607467651;
	setAttr ".dr" no;
	setAttr ".re" 13;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak5";
	rename -uid "0A9061F5-4196-C054-2F35-DA96283C5ABD";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[32:35]" -type "float3"  0.041277792 0 0.041277792
		 -0.041277792 0 0.041277792 -0.041277792 0 -0.041277792 0.041277792 0 -0.041277792;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "40DFBE0E-4570-DAAC-D591-DD9C3A8DC6AB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[12:13]" "e[15]" "e[17]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".wt" 0.58824735879898071;
	setAttr ".dr" no;
	setAttr ".re" 13;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "D66BC812-42D1-EA77-B2BB-429AD6F7CFB8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[28:29]" "e[31]" "e[33]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".wt" 0.13232052326202393;
	setAttr ".re" 31;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak6";
	rename -uid "B099AB4E-4E18-31D8-5AD7-4B88039DCBD9";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[40:43]" -type "float3"  -0.034408148 0 -0.034408148
		 0.034408148 0 -0.034408148 0.034408148 0 0.034408148 -0.034408148 0 0.034408148;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "0687D96B-45AE-EB10-3534-52A54FFFE1F9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[84:85]" "e[87]" "e[89]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".wt" 0.30258733034133911;
	setAttr ".re" 84;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "DC81BF58-44D6-75DD-5A96-6A9EF8DFF0F8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[92:93]" "e[95]" "e[97]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".wt" 0.13133135437965393;
	setAttr ".re" 93;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak7";
	rename -uid "F36A02E4-4162-46EF-D40A-CEA7B24E5206";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[48:51]" -type "float3"  -0.028588913 0 0.028588913
		 -0.028588913 0 -0.028588913 0.028588913 0 -0.028588913 0.028588913 0 0.028588913;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "6BB5528F-496A-E048-7D7C-24B99A0C4932";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 12 "e[22]" "e[24]" "e[26:27]" "e[86]" "e[88]" "e[90:91]" "e[94]" "e[96]" "e[98:99]" "e[102]" "e[104]" "e[106:107]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".a" 0;
createNode polyTweak -n "polyTweak8";
	rename -uid "9253B6AF-4AD1-5097-4A25-539639B164DE";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk";
	setAttr ".tk[12]" -type "float3" 0 0.015930407 0 ;
	setAttr ".tk[13]" -type "float3" 0 0.015930407 0 ;
	setAttr ".tk[14]" -type "float3" 0 0.015930407 0 ;
	setAttr ".tk[15]" -type "float3" 0 0.015930407 0 ;
	setAttr ".tk[52]" -type "float3" -0.026907191 0 -0.026907191 ;
	setAttr ".tk[53]" -type "float3" 0.026907191 0 -0.026907191 ;
	setAttr ".tk[54]" -type "float3" 0.026907191 0 0.026907191 ;
	setAttr ".tk[55]" -type "float3" -0.026907191 0 0.026907191 ;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "60C10343-4617-2E59-8172-2AA6AF9ABB71";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 430\n            -height 384\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 429\n            -height 383\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 430\n            -height 383\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 866\n            -height 811\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n"
		+ "            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n"
		+ "            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n"
		+ "            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n"
		+ "            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n"
		+ "                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n"
		+ "                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 866\\n    -height 811\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 866\\n    -height 811\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "41BAD971-40A8-9A6E-5987-C78AB462240E";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "4BC823F0-45A7-8348-01BD-A099315F931C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 11 "e[1:2]" "e[6:7]" "e[14]" "e[16]" "e[18:19]" "e[70]" "e[72]" "e[74:75]" "e[78]" "e[80]" "e[82:83]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".a" 0;
createNode polySplitRing -n "polySplitRing8";
	rename -uid "1ED3A2E8-4CF9-2C0C-4A05-699D8C12FCF3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[36:37]" "e[39]" "e[41]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".wt" 0.6189415454864502;
	setAttr ".dr" no;
	setAttr ".re" 39;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySoftEdge -n "polySoftEdge3";
	rename -uid "147771CA-48AD-9450-8CE4-B1BF84B074E9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[46]" "e[48]" "e[50:51]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak9";
	rename -uid "70020697-4268-DA6F-553B-2AA9A654B195";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk";
	setAttr ".tk[20]" -type "float3" 0.024718311 0 -0.024718311 ;
	setAttr ".tk[21]" -type "float3" -0.024718311 0 -0.024718311 ;
	setAttr ".tk[22]" -type "float3" -0.024718311 0 0.024718311 ;
	setAttr ".tk[23]" -type "float3" 0.024718311 0 0.024718311 ;
	setAttr ".tk[24]" -type "float3" 0.0051952009 0 -0.0051952009 ;
	setAttr ".tk[25]" -type "float3" -0.0051952009 0 -0.0051952009 ;
	setAttr ".tk[26]" -type "float3" -0.0051952009 0 0.0051952009 ;
	setAttr ".tk[27]" -type "float3" 0.0051952009 0 0.0051952009 ;
	setAttr ".tk[28]" -type "float3" -0.019306149 0 0.019306149 ;
	setAttr ".tk[29]" -type "float3" -0.019306149 0 -0.019306149 ;
	setAttr ".tk[30]" -type "float3" 0.019306149 0 -0.019306149 ;
	setAttr ".tk[31]" -type "float3" 0.019306149 0 0.019306149 ;
	setAttr ".tk[32]" -type "float3" -0.013595175 0 -0.013595175 ;
	setAttr ".tk[33]" -type "float3" 0.013595175 0 -0.013595175 ;
	setAttr ".tk[34]" -type "float3" 0.013595175 0 0.013595175 ;
	setAttr ".tk[35]" -type "float3" -0.013595175 0 0.013595175 ;
	setAttr ".tk[56]" -type "float3" 0.0078542633 0 -0.0078542633 ;
	setAttr ".tk[57]" -type "float3" 0.0078542633 0 0.0078542633 ;
	setAttr ".tk[58]" -type "float3" -0.0078542633 0 0.0078542633 ;
	setAttr ".tk[59]" -type "float3" -0.0078542633 0 -0.0078542633 ;
createNode polySoftEdge -n "polySoftEdge4";
	rename -uid "DDFD7A3C-4402-DC82-799F-A89F0042CC8E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[60:61]" "e[63]" "e[65]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".a" 0;
createNode polySoftEdge -n "polySoftEdge5";
	rename -uid "5AEC9F64-4665-0C4C-0BFE-AA880019F9DD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[46]" "e[48]" "e[50:51]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".a" 0;
createNode polySplitRing -n "polySplitRing9";
	rename -uid "128439CE-411D-6052-74D4-3A9585A09546";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[12:13]" "e[15]" "e[17]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".wt" 0.8497692346572876;
	setAttr ".dr" no;
	setAttr ".re" 13;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing10";
	rename -uid "F3F5EBE3-4C49-6B63-B26E-25A7F5F7BE7D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[68:69]" "e[71]" "e[73]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".wt" 0.2583048939704895;
	setAttr ".dr" no;
	setAttr ".re" 68;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak10";
	rename -uid "AF0525CB-4A87-B36E-F9B1-4A8585399D69";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[60:63]" -type "float3"  -0.026310306 0 -0.026310306
		 0.026310306 0 -0.026310306 0.026310306 0 0.026310306 -0.026310306 0 0.026310306;
createNode polyMapDel -n "polyMapDel1";
	rename -uid "1E06405D-4F5B-CBA6-03B0-A69879259BFD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyTweak -n "polyTweak11";
	rename -uid "FA6AEEE0-4732-D311-47A6-54A84503CC9D";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk";
	setAttr ".tk[8]" -type "float3" 0 0.01850328 0 ;
	setAttr ".tk[9]" -type "float3" 0 0.018503282 0 ;
	setAttr ".tk[10]" -type "float3" 0 0.018503282 0 ;
	setAttr ".tk[11]" -type "float3" 0 0.01850328 0 ;
	setAttr ".tk[64]" -type "float3" -0.013081228 0 -0.013081228 ;
	setAttr ".tk[65]" -type "float3" 0.013081228 0 -0.013081228 ;
	setAttr ".tk[66]" -type "float3" 0.013081228 0 0.013081228 ;
	setAttr ".tk[67]" -type "float3" -0.013081228 0 0.013081228 ;
createNode polyPlanarProj -n "polyPlanarProj1";
	rename -uid "C9A59F07-4F23-94B0-B261-18B3BCB1910B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 28 "f[4:5]" "f[7]" "f[9]" "f[11]" "f[13]" "f[15]" "f[17]" "f[19]" "f[21]" "f[23]" "f[25:26]" "f[28]" "f[31]" "f[33]" "f[35]" "f[37]" "f[39]" "f[41:42]" "f[44]" "f[46]" "f[48]" "f[51]" "f[53:54]" "f[56]" "f[59]" "f[61]" "f[63]" "f[65]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 2.5511568337678909 0 ;
	setAttr ".ro" -type "double3" 0 90 0 ;
	setAttr ".ps" -type "double2" 4.7960715390048883 5.0652545392513275 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj2";
	rename -uid "1C2B28C2-4996-5FE0-BCBC-8A8510F6D6EA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 29 "f[0]" "f[2]" "f[6]" "f[8]" "f[10]" "f[12]" "f[14]" "f[16]" "f[18]" "f[20]" "f[22]" "f[24]" "f[27]" "f[29:30]" "f[32]" "f[34]" "f[36]" "f[38]" "f[40]" "f[43]" "f[45]" "f[47]" "f[49:50]" "f[52]" "f[55]" "f[57:58]" "f[60]" "f[62]" "f[64]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 2.5511568337678909 0 ;
	setAttr ".ps" -type "double2" 5.0454770981517569 5.0652545392513275 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj3";
	rename -uid "974513F1-45C8-4733-2D4E-3E9D45048B03";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "f[1]" "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 2.5511568337678909 0 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "5E9AF473-44DE-0ED2-5743-A8AFAA5AF9ED";
	setAttr ".uopa" yes;
	setAttr -s 144 ".uvtk[0:143]" -type "float2" 0.11877877 -5.9604645e-008
		 0.037174582 -5.9604645e-008 0.037174582 -0.046923593 0.11877877 -0.046923593 0.16502666
		 -5.9604645e-008 0.24663085 -5.9604645e-008 0.24663085 -0.046923593 0.16502666 -0.046923593
		 0.045800269 -0.054764003 0.11015308 -0.054764003 0.23800516 -0.054764003 0.17365235
		 -0.054764003 0.10701764 -0.07032308 0.048935652 -0.07032308 0.048935652 -0.25942999
		 0.10701764 -0.25942999 0.17678779 -0.07032308 0.23486978 -0.07032308 0.23486978 -0.25942999
		 0.17678779 -0.25942999 0.0477916 -0.26140735 0.10816169 -0.26140735 0.23601383 -0.26140735
		 0.17564374 -0.26140735 0.11566359 -0.27927527 0.04028976 -0.27927527 0.039648771
		 -0.32425302 0.11630452 -0.32425302 0.16814184 -0.27927527 0.24351567 -0.27927527
		 0.2441566 -0.32425302 0.16750091 -0.32425302 0.11364645 -0.35194397 0.0423069 -0.35194397
		 0.050116897 -0.37028188 0.10583645 -0.37028188 0.17015898 -0.35194397 0.24149853
		 -0.35194397 0.23368853 -0.37028188 0.17796904 -0.37028188 0.058358133 -0.38212377
		 0.097595215 -0.38212377 0.2254473 -0.38212377 0.18621022 -0.38212377 0.21332568 -0.39137959
		 0.19833183 -0.39137959 0.070479751 -0.39137959 0.085473597 -0.39137959 0.17321396
		 -0.062608384 0.23844355 -0.062608384 0.23645294 -0.064376861 0.17520463 -0.064376861
		 0.11059147 -0.062608384 0.045361876 -0.062608384 0.047352493 -0.064376861 0.1086008
		 -0.064376861 0.17265069 -0.056150131 0.23900682 -0.056150131 0.11115474 -0.056150131
		 0.044798613 -0.056150131 0.047854662 -0.26681396 0.10809869 -0.26681396 0.23595077
		 -0.26681396 0.17570674 -0.26681396 0.049056828 -0.2684505 0.10689646 -0.2684505 0.23474854
		 -0.2684505 0.17690897 -0.2684505 0.36309296 0.077761278 0.28552258 0.077761278 0.28552258
		 0.030837744 0.36309296 0.030837744 0.4784717 0.10881376 0.40090132 0.10881376 0.40090132
		 0.1557373 0.4784717 0.1557373 0.29372185 0.022997335 0.35489368 0.022997335 0.47027242
		 0.10097335 0.40910059 0.10097335 0.35191321 0.0074382424 0.29670233 0.0074382424
		 0.29670233 -0.18166864 0.35191321 -0.18166864 0.41208106 0.08541429 0.46729195 0.08541429
		 0.46729195 -0.10369265 0.41208106 -0.10369265 0.29561484 -0.18364602 0.3530007 -0.18364602
		 0.46837944 -0.10567003 0.41099358 -0.10567003 0.36013174 -0.20151395 0.2884838 -0.20151395
		 0.28787452 -0.24649167 0.36074102 -0.24649167 0.40386254 -0.12353796 0.47551048 -0.12353796
		 0.47611976 -0.16851568 0.40325326 -0.16851568 0.35821438 -0.27418268 0.29040116 -0.27418268
		 0.29782522 -0.29252052 0.35079038 -0.29252052 0.4057799 -0.19620663 0.47359312 -0.19620663
		 0.46616912 -0.21454453 0.4132039 -0.21454453 0.305659 -0.30436242 0.34295654 -0.30436242
		 0.45833528 -0.22638643 0.42103773 -0.22638643 0.31718147 -0.31361824 0.33143413 -0.31361824
		 0.44681287 -0.23564225 0.43256015 -0.23564225 0.35531038 0.015152946 0.29330522 0.015152946
		 0.29519743 0.013384476 0.35341817 0.013384476 0.4086839 0.093128979 0.47068912 0.093128979
		 0.46879691 0.091360509 0.41057616 0.091360509 0.35584581 0.021611199 0.29276979 0.021611199
		 0.40814853 0.099587217 0.47122455 0.099587217 0.29567474 -0.18905261 0.3529408 -0.18905261
		 0.46831954 -0.11107659 0.41105342 -0.11107659 0.29681754 -0.19068918 0.35179806 -0.19068918
		 0.46717674 -0.11271316 0.41219628 -0.11271316 0.42725933 0.27825144 0.22135621 0.27825144
		 0.22135621 0.072348371 0.42725933 0.072348371 0 -0.48584932 -0.4858492 -0.48584932
		 -0.4858492 -1.1920929e-007 0 -1.1920929e-007;
createNode polyMapSewMove -n "polyMapSewMove1";
	rename -uid "B8A5C2F6-4E61-E1B6-996B-C19FD8CBC2BA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[46]";
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "221FF4DD-43D8-27FA-0BB9-AFAAA5813A3D";
	setAttr ".uopa" yes;
	setAttr -s 35 ".uvtk";
	setAttr ".uvtk[72]" -type "float2" -0.87937301 0.59646213 ;
	setAttr ".uvtk[73]" -type "float2" -0.4858492 0.59646213 ;
	setAttr ".uvtk[74]" -type "float2" -0.4858492 0.35841346 ;
	setAttr ".uvtk[75]" -type "float2" -0.87937301 0.35841346 ;
	setAttr ".uvtk[78]" -type "float2" -0.83777678 0.63623756 ;
	setAttr ".uvtk[79]" -type "float2" -0.5274452 0.63623756 ;
	setAttr ".uvtk[84]" -type "float2" -0.54256523 0.71517056 ;
	setAttr ".uvtk[85]" -type "float2" -0.82265675 0.71517056 ;
	setAttr ".uvtk[86]" -type "float2" -0.82265675 1.6745322 ;
	setAttr ".uvtk[87]" -type "float2" -0.54256523 1.6745322 ;
	setAttr ".uvtk[90]" -type "float2" -0.82817376 1.6845636 ;
	setAttr ".uvtk[91]" -type "float2" -0.53704852 1.6845636 ;
	setAttr ".uvtk[96]" -type "float2" -0.50087178 1.7752097 ;
	setAttr ".uvtk[97]" -type "float2" -0.86435044 1.7752097 ;
	setAttr ".uvtk[98]" -type "float2" -0.86744118 2.003387 ;
	setAttr ".uvtk[99]" -type "float2" -0.4977808 2.003387 ;
	setAttr ".uvtk[104]" -type "float2" -0.5105989 2.143867 ;
	setAttr ".uvtk[105]" -type "float2" -0.85462308 2.143867 ;
	setAttr ".uvtk[106]" -type "float2" -0.81696057 2.236897 ;
	setAttr ".uvtk[107]" -type "float2" -0.54826182 2.236897 ;
	setAttr ".uvtk[110]" -type "float2" -0.77721846 2.2969723 ;
	setAttr ".uvtk[111]" -type "float2" -0.58800375 2.2969723 ;
	setAttr ".uvtk[114]" -type "float2" -0.71876407 2.3439283 ;
	setAttr ".uvtk[115]" -type "float2" -0.64645839 2.3439283 ;
	setAttr ".uvtk[120]" -type "float2" -0.52533138 0.67603296 ;
	setAttr ".uvtk[121]" -type "float2" -0.83989084 0.67603296 ;
	setAttr ".uvtk[122]" -type "float2" -0.83029151 0.68500471 ;
	setAttr ".uvtk[123]" -type "float2" -0.53493077 0.68500471 ;
	setAttr ".uvtk[126]" -type "float2" -0.5226149 0.64326942 ;
	setAttr ".uvtk[127]" -type "float2" -0.84260726 0.64326942 ;
	setAttr ".uvtk[130]" -type "float2" -0.82787001 1.7119921 ;
	setAttr ".uvtk[131]" -type "float2" -0.5373522 1.7119921 ;
	setAttr ".uvtk[134]" -type "float2" -0.82207251 1.7202945 ;
	setAttr ".uvtk[135]" -type "float2" -0.54314995 1.7202945 ;
createNode polyMapSewMove -n "polyMapSewMove2";
	rename -uid "53D40ED6-4250-BBB6-60ED-1B9E8F9117EB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[3]";
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "88B8DD64-40DA-14A9-0E11-60975FF49518";
	setAttr ".uopa" yes;
	setAttr -s 140 ".uvtk[0:139]" -type "float2" -0.27563769 0.0063033402
		 -0.20183268 0.0063033402 -0.20183268 0.048742309 -0.27563769 0.048742309 -0.12726876
		 0.0063033402 -0.20107403 0.0063033402 -0.20107403 0.048742309 -0.12726876 0.048742309
		 -0.20963392 0.05583331 -0.26783639 0.05583331 -0.19327256 0.05583331 -0.13507026
		 0.05583331 -0.26500052 0.06990543 -0.21246979 0.06990543 -0.21246979 0.2409389 -0.26500052
		 0.2409389 -0.1379059 0.06990543 -0.19043699 0.06990543 -0.19043699 0.2409389 -0.1379059
		 0.2409389 -0.21143499 0.24272737 -0.26603532 0.24272737 -0.19147143 0.24272737 -0.13687107
		 0.24272737 -0.27282012 0.25888774 -0.20464984 0.25888774 -0.20407042 0.29956666 -0.27339983
		 0.29956666 -0.13008639 0.25888774 -0.19825646 0.25888774 -0.19883618 0.29956666 -0.12950668
		 0.29956666 -0.27099597 0.32461116 -0.20647439 0.32461116 -0.2135379 0.34119657 -0.26393211
		 0.34119657 -0.13191071 0.32461116 -0.19643208 0.32461116 -0.18936852 0.34119657 -0.13897431
		 0.34119657 -0.2209916 0.35190675 -0.25647861 0.35190675 -0.18191507 0.35190675 -0.14642778
		 0.35190675 -0.17095178 0.36027798 -0.15739098 0.36027798 -0.23195444 0.36027798 -0.24551541
		 0.36027798 -0.13467366 0.062928036 -0.19366917 0.062928036 -0.19186869 0.064527661
		 -0.13647425 0.064527661 -0.26823282 0.062928036 -0.20923725 0.062928036 -0.21103778
		 0.064527661 -0.26643252 0.064527661 -0.13416418 0.057086967 -0.19417861 0.057086967
		 -0.26874226 0.057086967 -0.20872781 0.057086967 -0.21149203 0.24761736 -0.26597834
		 0.24761736 -0.19141468 0.24761736 -0.13692841 0.24761736 -0.2125794 0.24909744 -0.26489097
		 0.24909744 -0.19032738 0.24909744 -0.13801554 0.24909744 -0.7158891 -0.071457997
		 -0.64573216 -0.071457997 -0.64573216 -0.029019028 -0.7158891 -0.029019028 0 -0.55969477
		 -0.34873784 -0.55969477 -0.34873784 -0.34873784 0 -0.34873784 -0.6531477 -0.021928027
		 -0.70847344 -0.021928027 -0.036862269 -0.59494346 -0.31187576 -0.59494346 -0.70577765
		 -0.0078558922 -0.6558435 -0.0078558922 -0.6558435 0.16317776 -0.70577765 0.16317776
		 -0.29847652 -0.66489327 -0.05026152 -0.66489327 -0.05026152 -1.51507235 -0.29847652
		 -1.51507235 -0.65485984 0.1649662 -0.70676124 0.1649662 -0.045372408 -1.52396202
		 -0.30336541 -1.52396202 -0.7132107 0.18112627 -0.64841032 0.18112627 -0.64785928
		 0.22180557 -0.71376181 0.22180557 -0.33542496 -1.60429192 -0.013312895 -1.60429192
		 -0.010573898 -1.80650115 -0.33816415 -1.80650115 -0.7114768 0.24684998 -0.65014446
		 0.24684998 -0.65685904 0.26343539 -0.70476222 0.26343539 -0.32680485 -1.93099332
		 -0.021933202 -1.93099332 -0.055309433 -2.013435841 -0.29342824 -2.013435841 -0.66394418
		 0.27414539 -0.69767678 0.27414539 -0.090528607 -2.066673994 -0.25820923 -2.066673994
		 -0.67436528 0.28251633 -0.68725574 0.28251687 -0.14233045 -2.10828614 -0.20640719
		 -2.10828614 -0.70885003 -0.01483316 -0.652771 -0.01483316 -0.65448248 -0.013233677
		 -0.70713878 -0.013233677 -0.31374902 -0.63020986 -0.034988806 -0.63020986 -0.04349567
		 -0.63816059 -0.30524212 -0.63816059 -0.70933449 -0.020674214 -0.65228677 -0.020674214
		 -0.31615636 -0.60117507 -0.032581527 -0.60117507 -0.65491414 0.16985601 -0.70670712
		 0.16985601 -0.045641579 -1.54826903 -0.30309626 -1.54826903 -0.65594786 0.17133611
		 -0.70567381 0.17133611 -0.050779287 -1.55562663 -0.29795837 -1.55562663 -0.67436433
		 0.29540643 -0.68725479 0.29540762 -0.34873784 0 0 0;
createNode polyMapSewMove -n "polyMapSewMove3";
	rename -uid "CA013D1C-4232-512B-2149-07AC54AC5AE4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[5]";
createNode polyFlipUV -n "polyFlipUV1";
	rename -uid "D0584D96-4A1D-6768-2EAF-F39E772F656E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 18 "f[0:1]" "f[4]" "f[6:7]" "f[10:11]" "f[14:15]" "f[18:19]" "f[22:23]" "f[26:27]" "f[30]" "f[33:34]" "f[37:38]" "f[41:43]" "f[46:47]" "f[50]" "f[53:55]" "f[58]" "f[61:62]" "f[65]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 0.60728689018988002 0 0 0 0 1 0 0 0.32217298865632915 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.24384953079999999;
	setAttr ".pv" 0.50512875619999997;
createNode polyTweakUV -n "polyTweakUV4";
	rename -uid "7AFDB5CD-467B-3B7A-FF31-14BCD39AE2E9";
	setAttr ".uopa" yes;
	setAttr -s 37 ".uvtk";
	setAttr ".uvtk[70]" -type "float2" 0.64200985 -0.14376241 ;
	setAttr ".uvtk[71]" -type "float2" 0.66738069 -0.14376241 ;
	setAttr ".uvtk[72]" -type "float2" 0.66738069 -0.15910962 ;
	setAttr ".uvtk[73]" -type "float2" 0.64200985 -0.15910962 ;
	setAttr ".uvtk[76]" -type "float2" 0.64469159 -0.14119805 ;
	setAttr ".uvtk[77]" -type "float2" 0.66469896 -0.14119805 ;
	setAttr ".uvtk[82]" -type "float2" 0.66372418 -0.13610917 ;
	setAttr ".uvtk[83]" -type "float2" 0.64566642 -0.13610917 ;
	setAttr ".uvtk[84]" -type "float2" 0.64566642 -0.074258313 ;
	setAttr ".uvtk[85]" -type "float2" 0.66372418 -0.074258313 ;
	setAttr ".uvtk[88]" -type "float2" 0.6453107 -0.073611543 ;
	setAttr ".uvtk[89]" -type "float2" 0.66407979 -0.073611543 ;
	setAttr ".uvtk[94]" -type "float2" 0.66641217 -0.067767546 ;
	setAttr ".uvtk[95]" -type "float2" 0.64297837 -0.067767546 ;
	setAttr ".uvtk[96]" -type "float2" 0.64277911 -0.053056762 ;
	setAttr ".uvtk[97]" -type "float2" 0.66661143 -0.053056762 ;
	setAttr ".uvtk[102]" -type "float2" 0.66578507 -0.043999836 ;
	setAttr ".uvtk[103]" -type "float2" 0.64360553 -0.043999836 ;
	setAttr ".uvtk[104]" -type "float2" 0.64603364 -0.038002118 ;
	setAttr ".uvtk[105]" -type "float2" 0.6633569 -0.038002118 ;
	setAttr ".uvtk[108]" -type "float2" 0.64859587 -0.034129068 ;
	setAttr ".uvtk[109]" -type "float2" 0.66079468 -0.034129068 ;
	setAttr ".uvtk[112]" -type "float2" 0.65236443 -0.031101748 ;
	setAttr ".uvtk[113]" -type "float2" 0.65702605 -0.031101748 ;
	setAttr ".uvtk[118]" -type "float2" 0.66483521 -0.13863242 ;
	setAttr ".uvtk[119]" -type "float2" 0.64455533 -0.13863242 ;
	setAttr ".uvtk[120]" -type "float2" 0.64517421 -0.138054 ;
	setAttr ".uvtk[121]" -type "float2" 0.66421634 -0.138054 ;
	setAttr ".uvtk[124]" -type "float2" 0.66501033 -0.14074472 ;
	setAttr ".uvtk[125]" -type "float2" 0.64438015 -0.14074472 ;
	setAttr ".uvtk[128]" -type "float2" 0.64533031 -0.071843192 ;
	setAttr ".uvtk[129]" -type "float2" 0.66406024 -0.071843192 ;
	setAttr ".uvtk[132]" -type "float2" 0.64570409 -0.071307942 ;
	setAttr ".uvtk[133]" -type "float2" 0.66368645 -0.071307942 ;
	setAttr ".uvtk[136]" -type "float2" 0.66738069 -0.18448044 ;
	setAttr ".uvtk[137]" -type "float2" 0.64200985 -0.18448044 ;
createNode polyMapSewMove -n "polyMapSewMove4";
	rename -uid "43F4EF28-4B50-35C9-2AAE-7999826013E6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[8]";
createNode polyTweakUV -n "polyTweakUV5";
	rename -uid "B31B7D2E-49C4-CBA9-BF21-EEBBC3E8DF4E";
	setAttr ".uopa" yes;
	setAttr -s 68 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[1]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[2]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[3]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[8]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[9]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[12]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[13]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[14]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[15]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[20]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[21]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[24]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[25]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[26]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[27]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[32]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[33]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[34]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[35]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[40]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[41]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[46]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[47]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[52]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[53]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[54]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[55]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[58]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[59]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[60]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[61]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[64]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[65]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[68]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[69]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[72]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[73]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[76]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[77]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[78]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[79]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[84]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[85]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[88]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[89]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[90]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[91]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[96]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[97]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[98]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[99]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[104]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[105]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[108]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[109]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[112]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[113]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[114]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[115]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[120]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[121]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[124]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[125]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[128]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[129]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[132]" -type "float2" 0.0017109513 0 ;
	setAttr ".uvtk[133]" -type "float2" 0.0017109513 0 ;
createNode polyMapSewMove -n "polyMapSewMove5";
	rename -uid "78F9A06D-42C9-B7E3-700C-97AD0C3F4EA1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[4]";
createNode polyTweakUV -n "polyTweakUV6";
	rename -uid "F5A03446-41D2-8654-94D3-2C8D2E65208C";
	setAttr ".uopa" yes;
	setAttr -s 134 ".uvtk[0:133]" -type "float2" -0.081639275 0.15681306 -0.049816132
		 0.15681306 -0.049816132 0.13777548 -0.081639275 0.13777548 -0.14371258 0.15681306
		 -0.11188939 0.15681306 -0.11188939 0.13777548 -0.14371258 0.13777548 -0.053179905
		 0.1345945 -0.078275532 0.1345945 -0.11525318 0.1345945 -0.14034879 0.1345945 -0.077052787
		 0.12828195 -0.05440262 0.12828195 -0.05440262 0.051558435 -0.077052787 0.051558435
		 -0.13912612 0.12828195 -0.11647588 0.12828195 -0.11647588 0.051558435 -0.13912612
		 0.051558435 -0.05395646 0.050756216 -0.077498943 0.050756216 -0.11602977 0.050756216
		 -0.13957226 0.050756216 -0.080424428 0.04350692 -0.051030912 0.04350692 -0.050781004
		 0.02525878 -0.08067438 0.02525878 -0.14249772 0.04350692 -0.11310425 0.04350692 -0.1128543
		 0.02525878 -0.1427477 0.02525878 -0.079637855 0.014024138 -0.051817559 0.014024138
		 -0.054863259 0.0065841675 -0.076592132 0.0065841675 -0.14171112 0.014024138 -0.11389086
		 0.014024138 -0.11693653 0.0065841675 -0.13866544 0.0065841675 -0.05807706 0.0017797351
		 -0.07337831 0.0017797351 -0.12015036 0.0017797351 -0.13545164 0.0017797351 -0.12487742
		 -0.0019755363 -0.13072458 -0.0019755363 -0.062804081 -0.0019755363 -0.068651251 -0.0019755363
		 -0.14051977 0.13141191 -0.1150822 0.13141191 -0.11585853 0.13069442 -0.13974348 0.13069442
		 -0.078446478 0.13141191 -0.053008892 0.13141191 -0.053785205 0.13069442 -0.077670202
		 0.13069442 -0.14073944 0.13403213 -0.11486253 0.13403213 -0.078666136 0.13403213
		 -0.052789256 0.13403213 -0.053981043 0.048562646 -0.077474371 0.048562646 -0.1160543
		 0.048562646 -0.13954762 0.048562646 -0.054449901 0.04789865 -0.077005535 0.04789865
		 -0.11652315 0.04789865 -0.13907886 0.04789865 -0.17396271 0.13777548 -0.17396271
		 0.15681306 -0.084836736 0.1345945 -0.10869193 0.1345945 -0.14691007 0.13459451 -0.17076522
		 0.13459451 -0.10752961 0.12828195 -0.085999042 0.12828195 -0.085999042 0.051558435
		 -0.10752961 0.051558435 -0.16960293 0.12828195 -0.14807236 0.12828195 -0.14807236
		 0.051558435 -0.16960293 0.051558435 -0.085574955 0.050756156 -0.1079537 0.050756156
		 -0.14764827 0.050756216 -0.17002702 0.050756216 -0.11073458 0.04350692 -0.082794055
		 0.04350692 -0.082556456 0.02525878 -0.1109722 0.02525878 -0.17280793 0.04350692 -0.14486736
		 0.04350692 -0.14462978 0.025258839 -0.17304552 0.025258839 -0.1099869 0.014024138
		 -0.083541781 0.014024138 -0.086436927 0.0065841675 -0.10709175 0.0065841675 -0.17206019
		 0.014024079 -0.1456151 0.014024079 -0.14851022 0.0065841675 -0.16916507 0.0065841675
		 -0.089491889 0.0017797351 -0.10403675 0.0017797351 -0.15156519 0.0017797947 -0.1661101
		 0.0017797947 -0.09398526 -0.0019752979 -0.099543363 -0.0019756556 -0.15605855 -0.0019754767
		 -0.16161668 -0.0019754767 -0.10885438 0.13141191 -0.084674254 0.13141191 -0.085412189
		 0.13069442 -0.10811648 0.13069442 -0.1709277 0.13141191 -0.14674759 0.13141191 -0.14748549
		 0.13069443 -0.1701898 0.13069443 -0.10906321 0.1340321 -0.084465459 0.1340321 -0.1711365
		 0.13403213 -0.14653873 0.13403213 -0.08559832 0.048562646 -0.10793036 0.048562646
		 -0.14767164 0.048562646 -0.17000365 0.048562646 -0.086044014 0.04789865 -0.10748473
		 0.04789865 -0.14811724 0.04789871 -0.16955799 0.04789871 -0.093984798 -0.0077577829
		 -0.099542916 -0.0077582598 -0.17396271 0.18828453 -0.14371258 0.18828453;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polyTweakUV6.out" "pCubeShape1.i";
connectAttr "polyTweakUV6.uvtk[0]" "pCubeShape1.uvst[0].uvtw";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube1.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyTweak1.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak1.ip";
connectAttr "polyExtrudeFace2.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polyTweak2.out" "polyExtrudeFace4.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak2.ip";
connectAttr "polyExtrudeFace4.out" "polyExtrudeFace5.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace5.mp";
connectAttr "polyTweak3.out" "polySplitRing1.ip";
connectAttr "pCubeShape1.wm" "polySplitRing1.mp";
connectAttr "polyExtrudeFace5.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polySplitRing2.ip";
connectAttr "pCubeShape1.wm" "polySplitRing2.mp";
connectAttr "polySplitRing1.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polySplitRing3.ip";
connectAttr "pCubeShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing2.out" "polyTweak5.ip";
connectAttr "polySplitRing3.out" "polySplitRing4.ip";
connectAttr "pCubeShape1.wm" "polySplitRing4.mp";
connectAttr "polyTweak6.out" "polySplitRing5.ip";
connectAttr "pCubeShape1.wm" "polySplitRing5.mp";
connectAttr "polySplitRing4.out" "polyTweak6.ip";
connectAttr "polySplitRing5.out" "polySplitRing6.ip";
connectAttr "pCubeShape1.wm" "polySplitRing6.mp";
connectAttr "polyTweak7.out" "polySplitRing7.ip";
connectAttr "pCubeShape1.wm" "polySplitRing7.mp";
connectAttr "polySplitRing6.out" "polyTweak7.ip";
connectAttr "polyTweak8.out" "polySoftEdge1.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge1.mp";
connectAttr "polySplitRing7.out" "polyTweak8.ip";
connectAttr "polySoftEdge1.out" "polySoftEdge2.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge2.mp";
connectAttr "polySoftEdge2.out" "polySplitRing8.ip";
connectAttr "pCubeShape1.wm" "polySplitRing8.mp";
connectAttr "polyTweak9.out" "polySoftEdge3.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge3.mp";
connectAttr "polySplitRing8.out" "polyTweak9.ip";
connectAttr "polySoftEdge3.out" "polySoftEdge4.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge4.mp";
connectAttr "polySoftEdge4.out" "polySoftEdge5.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge5.mp";
connectAttr "polySoftEdge5.out" "polySplitRing9.ip";
connectAttr "pCubeShape1.wm" "polySplitRing9.mp";
connectAttr "polyTweak10.out" "polySplitRing10.ip";
connectAttr "pCubeShape1.wm" "polySplitRing10.mp";
connectAttr "polySplitRing9.out" "polyTweak10.ip";
connectAttr "polyTweak11.out" "polyMapDel1.ip";
connectAttr "polySplitRing10.out" "polyTweak11.ip";
connectAttr "polyMapDel1.out" "polyPlanarProj1.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj1.mp";
connectAttr "polyPlanarProj1.out" "polyPlanarProj2.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj2.mp";
connectAttr "polyPlanarProj2.out" "polyPlanarProj3.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj3.mp";
connectAttr "polyPlanarProj3.out" "polyTweakUV1.ip";
connectAttr "polyTweakUV1.out" "polyMapSewMove1.ip";
connectAttr "polyMapSewMove1.out" "polyTweakUV2.ip";
connectAttr "polyTweakUV2.out" "polyMapSewMove2.ip";
connectAttr "polyMapSewMove2.out" "polyTweakUV3.ip";
connectAttr "polyTweakUV3.out" "polyMapSewMove3.ip";
connectAttr "polyMapSewMove3.out" "polyFlipUV1.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV1.mp";
connectAttr "polyFlipUV1.out" "polyTweakUV4.ip";
connectAttr "polyTweakUV4.out" "polyMapSewMove4.ip";
connectAttr "polyMapSewMove4.out" "polyTweakUV5.ip";
connectAttr "polyTweakUV5.out" "polyMapSewMove5.ip";
connectAttr "polyMapSewMove5.out" "polyTweakUV6.ip";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
// End of RuinPillar.ma
