//Maya ASCII 2017ff05 scene
//Name: SwampTallTreeNoLeaves.ma
//Last modified: Wed, Nov 29, 2017 03:34:33 PM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "5695FB89-4506-52D8-095F-4AB4055D956D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -13.731153876420294 8.44780354536136 15.681386825567735 ;
	setAttr ".r" -type "double3" -5.7383527287395975 -41.000000000000007 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "5B42ED32-4C83-62E4-AEB8-6EB06814D3CB";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 21.65473138202848;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.34023125097155571 3.7620683989070822 -0.83990132808685303 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "6F793389-4912-F08F-DA48-10A812D9AD99";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "3CBF10C4-47E5-4D1A-9355-AEB4FBA7CD4A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "0B661F04-419C-C769-F203-18BCCC18099D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "E4437BA5-4E85-C9D0-85B8-6C8D2EA47E13";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "52026FCC-464C-61BD-22FE-85B5FD08CDBF";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 9.1271820448877783 -1.1970074812967575 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "AD1A2424-496D-4FC0-F645-B0923CAA1545";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCylinder1";
	rename -uid "B7261E9E-46F5-AF55-6B2D-5FA0A4ABFD6C";
	setAttr ".t" -type "double3" 0 11.246859072099889 0 ;
	setAttr ".s" -type "double3" 1 11.354926954990134 1 ;
createNode mesh -n "pCylinderShape1" -p "pCylinder1";
	rename -uid "B03F504F-4D62-8E2D-AA64-B1A3582FD8DB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.31126818060874939 0.24865185166977433 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "C408A248-47D2-B02D-E427-8DBC9D639123";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "E759AAB9-4BC6-D37B-BFE9-71A340C6964E";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "809827B0-49B5-E1B2-E818-1992BBDC204E";
createNode displayLayerManager -n "layerManager";
	rename -uid "24D6D31F-4BC2-3830-8132-FAA8ABBDF1CD";
createNode displayLayer -n "defaultLayer";
	rename -uid "F07D10EA-408C-255B-7002-5DB7A3DC1F95";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "E9986836-4CEB-497E-3B65-7E8A238C8B52";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "8210B0FC-4DC2-B1E9-8FE3-23AE6D5DBA9D";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "917A6DB4-4F0D-4013-92F4-D3A3D4B772B8";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 252\n            -height 384\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 252\n            -height 383\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 252\n            -height 383\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 561\n            -height 811\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n"
		+ "            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n"
		+ "            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n"
		+ "            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n"
		+ "            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n"
		+ "                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n"
		+ "                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 561\\n    -height 811\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 561\\n    -height 811\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "052F647A-40D3-94F5-84EA-B88F8AA01C82";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polySplitRing -n "polySplitRing11";
	rename -uid "9504941D-4E9D-12F8-B5F6-68A4260F64FB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 11 "e[7]" "e[18]" "e[57]" "e[79]" "e[101]" "e[142]" "e[155]" "e[186]" "e[189]" "e[211]" "e[270]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".wt" 0.59137845039367676;
	setAttr ".dr" no;
	setAttr ".re" 7;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "F90F0145-4A54-C8CB-2E73-2F99EA10C5CC";
	setAttr ".uopa" yes;
	setAttr -s 24 ".tk";
	setAttr ".tk[46]" -type "float3" 0.0026679756 0.036776047 0.097433522 ;
	setAttr ".tk[47]" -type "float3" -0.048345219 0.036776047 0.10476811 ;
	setAttr ".tk[48]" -type "float3" -0.095225617 0.036776047 0.083358534 ;
	setAttr ".tk[49]" -type "float3" -0.12308906 0.036776047 0.04000219 ;
	setAttr ".tk[50]" -type "float3" -0.12308903 0.036776047 -0.011535584 ;
	setAttr ".tk[51]" -type "float3" -0.095225602 0.036776047 -0.054891907 ;
	setAttr ".tk[52]" -type "float3" -0.04834519 0.036776047 -0.07630147 ;
	setAttr ".tk[53]" -type "float3" 0.0026680026 0.036776047 -0.068966858 ;
	setAttr ".tk[54]" -type "float3" 0.041617643 0.036776047 -0.03521679 ;
	setAttr ".tk[55]" -type "float3" 0.056137495 0.036776047 0.014233315 ;
	setAttr ".tk[56]" -type "float3" 0.041617632 0.036776047 0.063683443 ;
	setAttr ".tk[113]" -type "float3" -0.12308903 0.036776047 0.010339202 ;
	setAttr ".tk[122]" -type "float3" -0.031507473 0 0.033308238 ;
	setAttr ".tk[123]" -type "float3" -0.034567773 0 0.042019784 ;
	setAttr ".tk[124]" -type "float3" -0.040297657 0 0.050170727 ;
	setAttr ".tk[125]" -type "float3" -0.042051107 0 0.064904854 ;
	setAttr ".tk[126]" -type "float3" -0.05628024 0 0.064019702 ;
	setAttr ".tk[127]" -type "float3" -0.067188464 0 0.06245137 ;
	setAttr ".tk[128]" -type "float3" -0.081052437 0 0.057213485 ;
	setAttr ".tk[129]" -type "float3" -0.077454135 0 0.044697214 ;
	setAttr ".tk[130]" -type "float3" -0.07678625 0 0.033428513 ;
	setAttr ".tk[131]" -type "float3" -0.071735293 0 0.020415889 ;
	setAttr ".tk[132]" -type "float3" -0.054915812 0 0.025605187 ;
	setAttr ".tk[133]" -type "float3" -0.040965997 0 0.027202472 ;
createNode polySplitRing -n "polySplitRing10";
	rename -uid "AE887E70-4557-26D2-3E1E-928398AAA25C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[22:32]" "e[233]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".wt" 0.64851754903793335;
	setAttr ".dr" no;
	setAttr ".re" 27;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak3";
	rename -uid "7FCF0EAB-447E-DFDE-CE5A-6E87511DB3D1";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk[112:121]" -type "float3"  -0.29078391 -0.0062695825
		 0.14456449 -5.9604645e-008 0 0 -5.9604645e-008 0 0 -5.9604645e-008 0 0 -5.9604645e-008
		 0 0 -5.9604645e-008 0 0 -5.9604645e-008 0 0 -5.9604645e-008 0 0 -5.9604645e-008 0
		 0 -5.9604645e-008 0 0;
createNode polySplitRing -n "polySplitRing9";
	rename -uid "9590D88C-4CE8-1E03-ABD5-A882A25819AE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[4]" "e[15]" "e[63]" "e[85]" "e[107]" "e[127]" "e[161]" "e[171]" "e[195]" "e[217]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".wt" 0.42444172501564026;
	setAttr ".re" 4;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "D5D287DE-4BD0-4AEC-FE8C-D0B46B5B55D3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak2";
	rename -uid "300A92C4-4611-5709-217E-2BA50A65B9F8";
	setAttr ".uopa" yes;
	setAttr -s 41 ".tk";
	setAttr ".tk[0]" -type "float3" 0.80520749 -9.9920072e-016 -0.28787607 ;
	setAttr ".tk[3]" -type "float3" -0.61163747 -1.3322676e-015 -0.79471856 ;
	setAttr ".tk[5]" -type "float3" -1.2786845 -9.9920072e-016 0.84983289 ;
	setAttr ".tk[6]" -type "float3" -0.76948631 -9.9920072e-016 0.3894026 ;
	setAttr ".tk[7]" -type "float3" -0.19848078 -1.110223e-016 -0.044202104 ;
	setAttr ".tk[8]" -type "float3" 0.66141355 -3.9968029e-015 0.93881863 ;
	setAttr ".tk[9]" -type "float3" 0.18461646 -7.7715612e-016 0.095718533 ;
	setAttr ".tk[10]" -type "float3" -0.1698775 -4.4408921e-016 -0.0053386176 ;
	setAttr ".tk[68]" -type "float3" 0.17649217 1.3045121e-015 0.52462113 ;
	setAttr ".tk[69]" -type "float3" 0.17649217 1.3045121e-015 0.52462113 ;
	setAttr ".tk[70]" -type "float3" 0.17649217 1.3045121e-015 0.52462113 ;
	setAttr ".tk[71]" -type "float3" 0.17649217 1.3045121e-015 0.52462113 ;
	setAttr ".tk[72]" -type "float3" 0.17649217 1.3045121e-015 0.52462113 ;
	setAttr ".tk[73]" -type "float3" 0.17649217 1.3045121e-015 0.52462113 ;
	setAttr ".tk[74]" -type "float3" 0.17649217 1.3045121e-015 0.52462113 ;
	setAttr ".tk[75]" -type "float3" 0.17649217 1.3045121e-015 0.52462113 ;
	setAttr ".tk[76]" -type "float3" 0.17649217 1.3045121e-015 0.52462113 ;
	setAttr ".tk[77]" -type "float3" 0.17649217 1.3045121e-015 0.52462113 ;
	setAttr ".tk[78]" -type "float3" 0.17649217 1.3045121e-015 0.52462113 ;
	setAttr ".tk[90]" -type "float3" 0.0096344166 0 0.40718257 ;
	setAttr ".tk[91]" -type "float3" 0.0096344166 0 0.40718257 ;
	setAttr ".tk[92]" -type "float3" 0.0096344166 0 0.40718257 ;
	setAttr ".tk[93]" -type "float3" 0.0096344166 0 0.40718257 ;
	setAttr ".tk[94]" -type "float3" 0.0096344166 0 0.40718257 ;
	setAttr ".tk[95]" -type "float3" 0.0096344166 0 0.40718257 ;
	setAttr ".tk[96]" -type "float3" 0.0096344166 0 0.40718257 ;
	setAttr ".tk[97]" -type "float3" 0.0096344166 0 0.40718257 ;
	setAttr ".tk[98]" -type "float3" 0.0096344166 0 0.40718257 ;
	setAttr ".tk[99]" -type "float3" 0.0096344166 0 0.40718257 ;
	setAttr ".tk[100]" -type "float3" 0.0096344166 0 0.40718257 ;
	setAttr ".tk[101]" -type "float3" 0.16685483 0 0.17821768 ;
	setAttr ".tk[102]" -type "float3" 0.16685483 0 0.17821768 ;
	setAttr ".tk[103]" -type "float3" 0.16685483 0 0.17821768 ;
	setAttr ".tk[104]" -type "float3" 0.16685483 0 0.17821768 ;
	setAttr ".tk[105]" -type "float3" 0.16685483 0 0.17821768 ;
	setAttr ".tk[106]" -type "float3" 0.16685483 0 0.17821768 ;
	setAttr ".tk[107]" -type "float3" 0.16685483 0 0.17821768 ;
	setAttr ".tk[108]" -type "float3" 0.16685483 0 0.17821768 ;
	setAttr ".tk[109]" -type "float3" 0.16685483 0 0.17821768 ;
	setAttr ".tk[110]" -type "float3" 0.16685483 0 0.17821768 ;
	setAttr ".tk[111]" -type "float3" 0.16685483 0 0.17821768 ;
createNode polySplitRing -n "polySplitRing8";
	rename -uid "7130DEA5-48EA-F1DA-B0D9-47915B718297";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[187:188]" "e[190]" "e[192]" "e[194]" "e[196]" "e[198]" "e[200]" "e[202]" "e[204]" "e[206]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".wt" 0.58391290903091431;
	setAttr ".dr" no;
	setAttr ".re" 187;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "47F0484E-48FC-E162-3A58-E0B25A24F59D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[143:144]" "e[146]" "e[148]" "e[150]" "e[152]" "e[154]" "e[156]" "e[158]" "e[160]" "e[162]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".wt" 0.41143766045570374;
	setAttr ".re" 152;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak1";
	rename -uid "3B02A6D5-42E5-E997-05CB-6F9E802EC27F";
	setAttr ".uopa" yes;
	setAttr -s 66 ".tk[24:89]" -type "float3"  0.03187605 0 0.0025646861
		 0.03187605 0 0.0025646861 0.03187605 0 0.0025646861 0.03187605 0 0.0025646861 0.03187605
		 0 0.0025646861 0.03187605 0 0.0025646861 0.03187605 0 0.0025646861 0.03187605 0 0.0025646861
		 0.03187605 0 0.0025646861 0.03187605 0 0.0025646861 0.03187605 0 0.0025646861 0.021867251
		 0.03474997 -0.15501419 0.021867251 0.03474997 -0.15501419 0.021867251 0.03474997
		 -0.15501419 0.021867251 0.03474997 -0.15501419 0.021867251 0.03474997 -0.15501419
		 0.021867251 0.03474997 -0.15501419 0.021867251 0.03474997 -0.15501419 0.021867251
		 0.03474997 -0.15501419 0.021867251 0.03474997 -0.15501419 0.021867251 0.03474997
		 -0.15501419 0.021867251 0.03474997 -0.15501419 -0.0716867 0 0.04912129 -0.0716867
		 0 0.04912129 -0.0716867 0 0.04912129 -0.0716867 0 0.04912129 -0.0716867 0 0.04912129
		 -0.0716867 0 0.04912129 -0.0716867 0 0.04912129 -0.0716867 0 0.04912129 -0.0716867
		 0 0.04912129 -0.0716867 0 0.04912129 -0.0716867 0 0.04912129 0.058954597 0 0.0047433744
		 0.058954597 0 0.0047433744 0.058954597 0 0.0047433744 0.058954597 0 0.0047433744
		 0.058954597 0 0.0047433744 0.058954597 0 0.0047433744 0.058954597 0 0.0047433744
		 0.058954597 0 0.0047433744 0.058954597 0 0.0047433744 0.058954597 0 0.0047433744
		 0.058954597 0 0.0047433744 0.065064073 0 0.067848191 0.065064073 0 0.067848191 0.065064073
		 0 0.067848191 0.065064073 0 0.067848191 0.065064073 0 0.067848191 0.065064073 0 0.067848191
		 0.065064073 0 0.067848191 0.065064073 0 0.067848191 0.065064073 0 0.067848191 0.065064073
		 0 0.067848191 0.065064073 0 0.067848191 0.08483661 3.3306691e-016 0.063203812 0.08483661
		 3.3306691e-016 0.063203812 0.08483661 3.3306691e-016 0.063203812 0.08483661 3.3306691e-016
		 0.063203812 0.08483661 3.3306691e-016 0.063203812 0.08483661 3.3306691e-016 0.063203812
		 0.08483661 3.3306691e-016 0.063203812 0.08483661 3.3306691e-016 0.063203812 0.08483661
		 3.3306691e-016 0.063203812 0.08483661 3.3306691e-016 0.063203812 0.08483661 3.3306691e-016
		 0.063203812;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "B691DDE9-416C-8869-9D22-0F8FA5BB9A05";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[55:56]" "e[58]" "e[60]" "e[62]" "e[64]" "e[66]" "e[68]" "e[70]" "e[72]" "e[74]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".wt" 0.52041566371917725;
	setAttr ".dr" no;
	setAttr ".re" 56;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "81E22296-4882-D5D4-46C7-7FB3105EC1A5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[121:122]" "e[124]" "e[126]" "e[128]" "e[130]" "e[132]" "e[134]" "e[136]" "e[138]" "e[140]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".wt" 0.32216677069664001;
	setAttr ".re" 130;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "5B9CC96A-45B0-B7F0-1BE8-F98D0B3D0D78";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[77:78]" "e[80]" "e[82]" "e[84]" "e[86]" "e[88]" "e[90]" "e[92]" "e[94]" "e[96]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".wt" 0.13565081357955933;
	setAttr ".re" 78;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "A44C25E1-4084-5463-3C05-6DB1395CCDEA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[22:32]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".wt" 0.47315710783004761;
	setAttr ".re" 30;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "7D538DB5-4A33-9661-7D57-3C94083C198E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[55:56]" "e[58]" "e[60]" "e[62]" "e[64]" "e[66]" "e[68]" "e[70]" "e[72]" "e[74]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".wt" 0.16070576012134552;
	setAttr ".re" 55;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "B463884A-41CF-5AA4-2BD0-E08F0058410F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[22:32]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".wt" 0.13421225547790527;
	setAttr ".re" 30;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "4B394CEA-4672-1D47-770B-96AF5E8BDC7B";
	setAttr ".sa" 11;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polyMapDel -n "polyMapDel1";
	rename -uid "277694BB-4521-311D-CED2-3397C7F39784";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyTweak -n "polyTweak5";
	rename -uid "A07AB582-4E73-6AFA-9FB5-94AF1669A3A7";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk";
	setAttr ".tk[6]" -type "float3" -0.85480493 4.4408921e-016 0.42207453 ;
	setAttr ".tk[8]" -type "float3" 0.43863896 4.4408921e-016 0.47652444 ;
	setAttr ".tk[24]" -type "float3" 0.20768233 0 0.31695893 ;
	setAttr ".tk[25]" -type "float3" -0.029793315 0 -0.17313696 ;
	setAttr ".tk[26]" -type "float3" -0.23555577 0 0.027819112 ;
	setAttr ".tk[27]" -type "float3" -0.17356072 0 0.23075101 ;
	setAttr ".tk[46]" -type "float3" 0.25035921 0 0.25385427 ;
	setAttr ".tk[47]" -type "float3" 0.009176258 -2.220446e-016 -0.16288884 ;
	setAttr ".tk[48]" -type "float3" -0.27510181 -1.110223e-016 0.16893971 ;
	setAttr ".tk[49]" -type "float3" -0.15747374 0 0.22963381 ;
	setAttr ".tk[79]" -type "float3" -0.0034214626 0 -0.081377491 ;
	setAttr ".tk[89]" -type "float3" 0.063528851 0 0.14772344 ;
	setAttr ".tk[122]" -type "float3" -0.15231122 0 0.096590027 ;
	setAttr ".tk[131]" -type "float3" 0.22938988 0 0.20139422 ;
	setAttr ".tk[133]" -type "float3" -0.41087955 0 0.1742768 ;
	setAttr ".tk[134]" -type "float3" 0 0 0.21571249 ;
	setAttr ".tk[135]" -type "float3" 0 0 5.9604645e-008 ;
	setAttr ".tk[136]" -type "float3" 0 0 5.9604645e-008 ;
	setAttr ".tk[137]" -type "float3" 0 0 5.9604645e-008 ;
	setAttr ".tk[138]" -type "float3" 0 0 5.9604645e-008 ;
createNode polyCylProj -n "polyCylProj1";
	rename -uid "59649791-4DF0-52A2-D45E-9097F9BF9605";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[99:120]" "f[128:129]" "f[150:151]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0.11164775490760803 19.756573677062988 0.22355777025222778 ;
	setAttr ".ro" -type "double3" -0.63655215429484313 0 0 ;
	setAttr ".ps" -type "double2" 180 3.5531785390946742 ;
	setAttr ".r" 2.4267584085464478;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "2032F6BC-48B9-2C0C-6D53-24AC9EA5E9FA";
	setAttr ".uopa" yes;
	setAttr -s 42 ".uvtk[0:41]" -type "float2" 0.001868844 0.30075091 -0.043034673
		 0.30075091 -0.070912719 -0.18074554 -0.013582826 -0.18074554 0.07805264 0.30075091
		 0.085177392 -0.18074554 0.15657026 0.30075091 0.18627432 -0.18074554 0.2410571 0.30075091
		 0.2030561 0.30075091 0.24487491 -0.18074554 0.28685173 -0.18074554 0.33622715 0.30075091
		 0.38435012 -0.18074554 0.4461453 0.30075091 0.47776145 -0.18074554 -0.46219158 -0.18074554
		 -0.34660798 0.30075091 -0.37458289 -0.18074554 -0.24473029 0.30075091 -0.28753889
		 -0.18074554 -0.15583903 0.30075091 -0.19934458 -0.18074554 -0.075019717 0.30075091
		 -0.10840535 -0.18074554 -0.042931825 -0.52385235 0.026754618 -0.52385235 0.13228136
		 -0.52385235 0.22285618 -0.52385235 0.27166161 -0.52385235 0.30399036 -0.52385235
		 0.38035148 -0.52385235 0.45538005 -0.52385235 -0.41566288 -0.52385235 -0.32355088
		 -0.52385235 -0.21595252 -0.52385235 -0.094012678 -0.52385235 -0.46295524 0.30075091
		 -0.58364177 0.30075091 -0.55202562 -0.18074554 -0.57440698 -0.52385235 -0.49766618
		 -0.52385235;
createNode polyCylProj -n "polyCylProj2";
	rename -uid "701C1661-4AFD-95EF-9FED-679C4783291C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[77:87]" "f[127]" "f[149]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0.16499596834182739 14.922400951385498 0.51979240775108337 ;
	setAttr ".ro" -type "double3" -0.23899843756578693 -0.0069494199169201128 0.12178664792351192 ;
	setAttr ".ps" -type "double2" 180 3.3012144058586164 ;
	setAttr ".r" 2.1531206369400024;
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "A5705ED5-4591-CED1-3B6C-349609A52BC1";
	setAttr ".uopa" yes;
	setAttr -s 28 ".uvtk[42:69]" -type "float2" 0.47590512 0.10263084 0.44947439
		 -0.51515752 -0.48777354 -0.51492161 -0.35903633 0.10324623 -0.3937189 -0.51454216
		 -0.27084953 0.10364875 -0.29245234 -0.51413959 -0.18519771 0.10394654 -0.18615562
		 -0.51384181 -0.10009485 0.10404503 -0.080434144 -0.51374328 -0.013533205 0.10391299
		 -0.065559268 0.10399108 -0.038190752 -0.51379734 0.019545615 -0.51387542 0.076415926
		 0.10359229 0.11232138 -0.5141961 0.17124516 0.1031848 0.19930643 -0.5146035 0.27113461
		 0.10281986 0.22792478 0.10297476 0.2481536 -0.5148136 0.28287268 -0.51496845 0.37398273
		 0.10261337 0.36546144 -0.51517498 -0.45152456 0.10286676 -0.5492295 0.10263084 -0.57566023
		 -0.51515752;
createNode polyCylProj -n "polyCylProj3";
	rename -uid "C81F8DBF-4974-FDCB-5531-8D966F6CB2C8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "f[44:54]" "f[66:76]" "f[125:126]" "f[147:148]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0.15196526050567627 9.7138381004333496 0.21872758865356445 ;
	setAttr ".ro" -type "double3" 6.3787145727922097 0 0 ;
	setAttr ".ps" -type "double2" 180 2.8523710097112907 ;
	setAttr ".r" 2.7271263599395752;
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "77CB0CE0-4D93-F698-F2DB-0A82A4D64A45";
	setAttr ".uopa" yes;
	setAttr -s 70 ".uvtk[42:111]" -type "float2" 0.040629387 0.98173368 0.0459705
		 0.88228875 -0.10427961 0.86779541 -0.093226343 0.96888745 -0.089207977 0.86931449
		 -0.079095751 0.97031921 -0.072980613 0.87094897 -0.065369815 0.97169489 -0.055945188
		 0.87264484 -0.051728874 0.97303009 -0.038998902 0.87429971 -0.037850469 0.97435099
		 -0.046191782 0.97355688 -0.032226235 0.87494606 -0.022969544 0.87582856 -0.023426086
		 0.97569418 -0.0080920756 0.87721568 -0.0082180202 0.97709906 0.0058585107 0.87849885
		 0.0078005195 0.97858918 0.0008712858 0.97794414 0.01369223 0.87922257 0.019260347
		 0.87973601 0.024290919 0.9801507 0.032502979 0.88098329 -0.10804686 0.96739262 -0.12370595
		 0.96584004 -0.11836478 0.86639494 -0.03173539 0.63057584 -0.093157858 0.63163757
		 -0.10143325 0.25902241 -0.036196977 0.25796068 0.067523539 0.63581175 0.068994164
		 0.26319656 0.16158655 0.64641649 0.16770357 0.27380139 0.25354987 0.65902328 0.21480268
		 0.65367246 0.22309443 0.2810573 0.26250747 0.28640816 0.34662968 0.66962945 0.35639867
		 0.29701433 0.44386202 0.67486763 0.45205927 0.3022525 -0.59720355 0.30045962 -0.48954928
		 0.66482002 -0.49216723 0.29220489 -0.37189949 0.65272427 -0.38105643 0.28010917 -0.25297296
		 0.6406278 -0.26516044 0.2680127 -0.13850424 0.63237119 -0.1485056 0.25975606 -0.072153956
		 -0.67486858 0.034569591 -0.66963267 0.1458656 -0.65902793 0.21141212 -0.65177196
		 0.25971776 -0.64642107 0.37303141 -0.63581496 0.48269463 -0.63057679 -0.56187242
		 -0.63236964 -0.46269202 -0.64062434 -0.36670941 -0.65272009 -0.27128875 -0.66481656
		 -0.17378941 -0.67307323 -0.13368848 -0.67380685 -0.60111743 0.67307478 -0.70497859
		 0.67486763 -0.69678134 0.3022525 -0.66614598 -0.63057679;
createNode polyMapSewMove -n "polyMapSewMove1";
	rename -uid "FC9735F6-496B-572B-53FA-3AAFCCE63EB9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 12 "e[145]" "e[147]" "e[149]" "e[151]" "e[153]" "e[155]" "e[157]" "e[159]" "e[161]" "e[163:164]" "e[242]" "e[287]";
createNode polyTweakUV -n "polyTweakUV4";
	rename -uid "7AF3C939-48DB-F778-4C6D-1D9F5B408336";
	setAttr ".uopa" yes;
	setAttr -s 42 ".uvtk[0:41]" -type "float2" -0.063808814 1.47624052 -0.061533436
		 1.47624803 -0.060266748 1.49940503 -0.062988713 1.4993974 -0.06765075 1.47627771
		 -0.067680255 1.49943471 -0.071573511 1.47635329 -0.07250239 1.49951017 -0.075720489
		 1.47644305 -0.073885947 1.47640502 -0.075308993 1.49956191 -0.077334136 1.49960005
		 -0.080242574 1.47651863 -0.082049161 1.49967551 -0.085231006 1.47655594 -0.086581707
		 1.49971282 -0.041492715 1.49970007 -0.04644759 1.47648442 -0.045729831 1.4996413
		 -0.051374152 1.47639823 -0.04992117 1.49955511 -0.055821672 1.47631204 -0.054146722
		 1.49946904 -0.059913442 1.47625327 -0.05848293 1.49941015 -0.061657295 1.51590848
		 -0.064739719 1.51590109 -0.069556817 1.51593828 -0.073871747 1.51601386 -0.07623741
		 1.5160656 -0.077847421 1.51610351 -0.081650585 1.51617908 -0.085425884 1.51621652
		 -0.044031814 1.51614499 -0.048659161 1.51605868 -0.053859547 1.51597261 -0.059399381
		 1.51591372 -0.041125789 1.47654319 -0.035784498 1.47655594 -0.037135199 1.49971282
		 -0.035979345 1.51621652 -0.039867714 1.51620364;
createNode polyMapSewMove -n "polyMapSewMove2";
	rename -uid "9CF8F94D-4613-5F8B-8E64-5C98EFAFA8FE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 12 "e[189]" "e[191]" "e[193]" "e[195]" "e[197]" "e[199]" "e[201]" "e[203]" "e[205]" "e[207:208]" "e[244]" "e[289]";
createNode polyTweakUV -n "polyTweakUV5";
	rename -uid "510C6FEE-4995-E50B-013B-5FAA086B3002";
	setAttr ".uopa" yes;
	setAttr -s 84 ".uvtk[0:83]" -type "float2" -0.18279332 -0.83078086 -0.20892517
		 -0.83073306 -0.22582738 -1.099580765 -0.19413984 -1.09960556 -0.13972314 -0.83119267
		 -0.13952632 -1.10023403 -0.097475663 -0.83184546 -0.083393946 -1.10131299 -0.054686524
		 -0.83257484 -0.073113702 -0.83226407 -0.050723925 -1.10203075 -0.02715037 -1.10255837
		 -0.0099320933 -0.83319312 0.027735367 -1.10363293 0.037855931 -0.83354926 0.080498025
		 -1.10425496 -0.44439143 -1.10223877 -0.37609789 -0.83179516 -0.39506418 -1.10172987
		 -0.3248162 -0.83135355 -0.3462683 -1.10090041 -0.27520028 -0.83092779 -0.29707399
		 -1.10007262 -0.2277839 -0.83069801 -0.2465933 -1.099567652 -0.21032317 -1.29176068
		 -0.17443979 -1.29180026 -0.11836457 -1.29243398 -0.068136603 -1.29349184 -0.040599614
		 -1.29419148 -0.021858476 -1.29470181 0.022411708 -1.2957387 0.066358969 -1.2963295
		 -0.41551471 -1.293782 -0.36164305 -1.29297066 -0.30110109 -1.29218292 -0.23660833
		 -1.2917279 -0.42807004 -0.83206755 -0.4787772 -0.83204246 -0.49511901 -1.10220683
		 -0.50925803 -1.29428124 -0.463992 -1.29429364 0.028707299 -0.55134898 -0.35536557
		 -0.55291963 -0.31560859 -0.55540085 -0.27653506 -0.5579043 -0.23714951 -0.55964905
		 -0.19657344 -0.56009477 -0.22105464 -0.55983078 -0.15417728 -0.55911374 -0.10973567
		 -0.55703163 -0.063629963 -0.55452371 -0.083376765 -0.55558783 -0.016976379 -0.55240035
		 -0.3967514 -0.55123454 -0.44036663 -0.55086625 -0.18010347 -0.018524893 -0.2057932
		 -0.018080838 -0.20925435 -0.1739258 -0.18196949 -0.17436987 -0.13858876 -0.016335003
		 -0.13797367 -0.17217997 -0.099247232 -0.01189957 -0.096688807 -0.16774455 -0.060783885
		 -0.0066268705 -0.076989785 -0.0088648088 -0.073521785 -0.16470978 -0.057037421 -0.16247182
		 -0.021853611 -0.0021908316 -0.017767765 -0.15803583 0.018813487 0 0.022241965 -0.15584496
		 -0.41660854 -0.15659483 -0.37158251 -0.0042023519 -0.37267748 -0.16004735 -0.32237592
		 -0.0092613865 -0.32620576 -0.16510636 -0.27263528 -0.01432069 -0.27773264 -0.17016566
		 -0.22475916 -0.017773967 -0.2289422 -0.17361896 -0.41824546 -0.00074984919 -0.46168509
		 0 -0.45825657 -0.15584496;
createNode polyCylProj -n "polyCylProj4";
	rename -uid "32EAAB3E-4F57-EF80-FD7C-52A87A0DFBBC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "f[33:43]" "f[88:98]" "f[123:124]" "f[145:146]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.0081705451011657715 4.7170543670654297 0.0421600341796875 ;
	setAttr ".ro" -type "double3" -2.1033238235592622 -0.6334251254871972 0.54314665561555153 ;
	setAttr ".ps" -type "double2" 180 3.5543632507324219 ;
	setAttr ".r" 2.3739912509918213;
createNode polyTweakUV -n "polyTweakUV6";
	rename -uid "C3670315-4B38-DEA6-151E-4C8F653736D3";
	setAttr ".uopa" yes;
	setAttr -s 126 ".uvtk[0:125]" -type "float2" 0.27982023 1.58158267 0.31510016
		 1.58151817 0.33791944 1.94448328 0.29513887 1.9445163 0.22167215 1.5821389 0.22140643
		 1.94536495 0.16463473 1.58302009 0.14562342 1.94682181 0.10686621 1.58400464 0.1317443
		 1.58358514 0.1015164 1.94779098 0.069690257 1.9485029 0.046444327 1.58483958 -0.0044097602
		 1.94995403 -0.018073171 1.58532023 -0.075643271 1.95079362 0.63299763 1.94807172
		 0.54079616 1.58295214 0.56640208 1.94738472 0.471562 1.58235586 0.50052392 1.94626498
		 0.40457669 1.58178127 0.43410781 1.9451474 0.34056094 1.58147085 0.36595502 1.94446552
		 0.31698754 2.20394063 0.26854226 2.20399451 0.19283643 2.20484972 0.12502483 2.20627809
		 0.087847739 2.2072227 0.062545806 2.20791173 0.0027776659 2.20931172 -0.056554526
		 2.210109 0.59401202 2.20666957 0.521281 2.20557427 0.43954483 2.20451093 0.35247454
		 2.20389676 0.61096251 1.5833199 0.67942107 1.58328593 0.70148385 1.94802845 0.72057259
		 2.20734382 0.65945983 2.20736051 -0.0057218373 1.20432806 0.51280606 1.20644867 0.45913097
		 1.20979822 0.40637878 1.21317828 0.35320517 1.21553373 0.29842439 1.2161355 0.33147594
		 1.21577919 0.24118635 1.21481109 0.18118677 1.21200013 0.1189405 1.20861423 0.14560023
		 1.21005094 0.055954605 1.20574737 0.56867993 1.20417345 0.62756383 1.20367634 0.2761887
		 0.48497477 0.31087181 0.48437527 0.31554458 0.69477785 0.27870801 0.69537747 0.22014067
		 0.48201823 0.21931025 0.69242096 0.16702655 0.47603008 0.16357252 0.68643272 0.11509809
		 0.4689115 0.13697729 0.47193289 0.13229522 0.68233562 0.1100401 0.67931414 0.06253925
		 0.46292251 0.057023078 0.6733253 0.007635504 0.45996472 0.0030068457 0.67036736 0.59548867
		 0.67137969 0.53470004 0.46563819 0.53617835 0.67604089 0.46826735 0.47246829 0.47343794
		 0.68287098 0.40111366 0.47929874 0.40799555 0.68970144 0.33647725 0.48396096 0.34212467
		 0.69436359 0.59769869 0.46097708 0.65634537 0.45996472 0.65171671 0.67036736 0.016824365
		 0.010431923 -0.060626805 0.011549212 -0.065203607 -0.22126208 -0.0067862272 -0.22186759
		 0.12938941 0.009335503 0.085068077 -0.22331771 0.17949709 0.0076115588 0.18294284
		 -0.2261783 0.29889297 0.0036984105 0.26108772 0.0049439855 0.24088748 -0.22786734
		 0.28366897 -0.22911289 0.38662624 0.0016215006 0.38371944 -0.2311898 0.47095653 0.0010617003
		 0.47993737 -0.23174961 -0.43762678 -0.23061459 -0.37125623 0.0046662278 -0.35057056
		 -0.22814508 -0.28478175 0.0076861507 -0.26603049 -0.22512516 -0.19352096 0.010297682
		 -0.18205106 -0.22251362 -0.095460117 0.013589621 -0.091099322 -0.22028853 0.0017847717
		 -0.49351296 0.10800859 -0.49539131 0.20991701 -0.49825186 0.26629621 -0.49994093
		 0.30425477 -0.50118649 0.39168936 -0.50326335 0.4745259 -0.50382322 -0.37246418 -0.50021869
		 -0.28853101 -0.49719876 -0.19913375 -0.49458721 -0.10220408 -0.49321318 -0.061246514
		 -0.49333566 -0.45491254 0.0021967245 -0.53786719 0.0010617003 -0.52888638 -0.23174961
		 -0.53429782 -0.50382322 -0.45355189 -0.50268817;
createNode polyMapSewMove -n "polyMapSewMove3";
	rename -uid "58D1ECF6-470D-75C7-3586-2E89002BF77A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 12 "e[79]" "e[81]" "e[83]" "e[85]" "e[87]" "e[89]" "e[91]" "e[93]" "e[95]" "e[97:98]" "e[238]" "e[283]";
createNode polyTweakUV -n "polyTweakUV7";
	rename -uid "2C95C523-4C16-C75E-50BB-C9AF737FFD1F";
	setAttr ".uopa" yes;
	setAttr -s 112 ".uvtk[0:111]" -type "float2" -0.31010816 -1.52802789 -0.34839422
		 -1.52795792 -0.37315786 -1.92185009 -0.32673204 -1.92188597 -0.24700546 -1.52863145
		 -0.24671711 -1.92280698 -0.1851081 -1.52958775 -0.16447687 -1.92438793 -0.12241728
		 -1.53065634 -0.14941514 -1.53020096 -0.11661161 -1.92543972 -0.082073696 -1.92621231
		 -0.056847025 -1.53156233 -0.0016598701 -1.92778707 0.013167808 -1.53208399 0.075643271
		 -1.92869818 -0.69337875 -1.9257443 -0.59332103 -1.52951407 -0.62110877 -1.92499876
		 -0.51818764 -1.52886701 -0.54961729 -1.92378354 -0.44549471 -1.52824342 -0.4775421
		 -1.92257071 -0.37602443 -1.52790654 -0.40358222 -1.92183089 -0.35044241 -2.20341492
		 -0.29786924 -2.20347357 -0.21571276 -2.20440149 -0.14212313 -2.20595169 -0.10177832
		 -2.20697689 -0.07432048 -2.20772457 -0.0094596855 -2.20924377 0.054928012 -2.210109
		 -0.65107119 -2.20637655 -0.57214308 -2.2051878 -0.4834424 -2.20403385 -0.38895315
		 -2.20336723 -0.66946602 -1.52991319 -0.74375778 -1.52987623 -0.76770037 -1.92569733
		 -0.78841567 -2.20710826 -0.72209573 -2.20712638 -0.00023595244 -1.11862862 -0.56294596
		 -1.12092996 -0.50469744 -1.12456489 -0.44745037 -1.12823296 -0.38974601 -1.13078928
		 -0.33029753 -1.13144219 -0.36616528 -1.13105559 -0.2681824 -1.13000488 -0.20307046
		 -1.12695456 -0.13552038 -1.12328005 -0.16445169 -1.12483919 -0.067167655 -1.12016892
		 -0.62358069 -1.11846089 -0.68748188 -1.11792147 -0.31507128 -0.33009356 -0.35527444
		 -0.32966423 -0.34887651 -0.56566131 -0.30890116 -0.56631196 -0.24862298 -0.32920057
		 -0.24444233 -0.56310356 -0.18523139 -0.32699284 -0.18395536 -0.5566051 -0.12505165
		 -0.3241916 -0.14980039 -0.32538199 -0.15001303 -0.55215883 -0.12586161 -0.54887998
		 -0.066871226 -0.32170749 -0.068327166 -0.54238081 -0.0089773424 -0.32035014 -0.0097083822
		 -0.53917086 -0.65267372 -0.54026949 -0.58231366 -0.32161582 -0.58830976 -0.54532784
		 -0.5177896 -0.32435575 -0.52022344 -0.55273986 -0.45102084 -0.32723826 -0.44920492
		 -0.56015235 -0.38306332 -0.32936937 -0.37772137 -0.56521177 -0.64400864 -0.319868
		 -0.70322543 -0.31964755 -0.71369278 -0.53917086 -0.31306857 0.019712448 -0.35911915
		 0.020565094 -0.3642571 -0.13739321 -0.32941258 -0.13787559 -0.23669198 0.018811697
		 -0.26708919 -0.13898748 -0.20269509 0.017572161 -0.20068273 -0.14106481 -0.1216875
		 0.014750677 -0.14733759 0.015648492 -0.16136825 -0.14229158 -0.13234168 -0.14319631
		 -0.062161244 0.013219232 -0.064457849 -0.14474492 -0.0049418174 0.012721923 0.00082759187
		 -0.14525878 -0.62176067 -0.1432105 -0.57639885 0.016340869 -0.5626874 -0.14165613
		 -0.51771963 0.018269503 -0.50532073 -0.13972481 -0.45579326 0.019914366 -0.44833493
		 -0.13806979 -0.39575243 0.022024777 -0.3914918 -0.1366767 -0.63316512 0.014781781
		 -0.68945348 0.014127193 -0.68368411 -0.14385352;
createNode polyPlanarProj -n "polyPlanarProj1";
	rename -uid "D921BDF1-4EBC-EE47-87FA-359F975A8B5E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "f[0]" "f[6:7]" "f[55:56]" "f[139:140]" "f[142:144]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.31634551286697388 1.4159016609191895 0.70767158269882202 ;
	setAttr ".ro" -type "double3" 0 -11.661207812467701 0 ;
	setAttr ".ps" -type "double2" 3.9256132841110229 3.9256132841110229 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyMapDel -n "polyMapDel2";
	rename -uid "C50499D0-4E4B-CF96-A892-8B82A1540B9A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0]";
createNode polyPlanarProj -n "polyPlanarProj2";
	rename -uid "A84706E0-443E-FE7A-2A09-6E9454206D31";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[0:2]" "f[60:62]" "f[133:135]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0.18998152017593384 1.4159016609191895 -1.0386021435260773 ;
	setAttr ".ro" -type "double3" 0 -4.3848224708384187 0 ;
	setAttr ".ps" -type "double2" 3.0479421615600586 3.0479421615600586 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj3";
	rename -uid "8C595C1B-40A2-0330-CD52-80BA3937156E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[8:10]" "f[63:65]" "f[136:138]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 1.1216082572937012 1.4159016609191895 0.74822917580604553 ;
	setAttr ".ro" -type "double3" 0 90 0 ;
	setAttr ".ps" -type "double2" 3.1534916758537292 3.1534916758537292 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj4";
	rename -uid "9CF65E6E-43E3-1B95-9A52-07A6E53F0D4C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "f[3:5]" "f[57:59]" "f[121:122]" "f[130:132]" "f[141]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -1.4510683417320251 1.3803062438964844 0.0083792209625244141 ;
	setAttr ".ro" -type "double3" 0 80.914592198714104 0 ;
	setAttr ".ps" -type "double2" 3.1191329956054687 3.1191329956054687 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj5";
	rename -uid "B4A395CF-444B-00C8-9B86-148923D2F012";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[11:32]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.31634551286697388 11.21125602722168 0.38725340366363525 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 3.9256132841110229 3.9256132841110229 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV8";
	rename -uid "4FBA0F04-4709-657D-DC1C-D4A9FF0282C9";
	setAttr ".uopa" yes;
	setAttr -s 208 ".uvtk[0:207]" -type "float2" 0.27610004 1.79699647 0.29943621
		 1.79587913 0.32559568 2.03528738 0.29729712 2.036613226 0.23765188 1.79913855 0.24854851
		 2.03942275 0.19994771 1.80145907 0.19846177 2.042695999 0.16176355 1.80387115 0.1782074
		 1.80283546 0.16931427 2.044681549 0.14828259 2.046123505 0.1218195 1.80626631 0.099309146
		 2.04934144 0.079154968 1.8085506 0.052212954 2.05206871 0.52090186 2.028666258 0.44877928
		 1.78994751 0.47682714 2.030241489 0.40296257 1.79166317 0.43321437 2.031509638 0.35863358
		 1.79332519 0.38924539 2.032794952 0.31627721 1.79507113 0.34414083 2.034421206 0.3196584
		 2.20755863 0.28761321 2.20907092 0.2375592 2.21194553 0.19274503 2.21495676 0.16818088
		 2.21671581 0.15146405 2.217942 0.11196989 2.22068977 0.072745442 2.22302675 0.50299525
		 2.20091915 0.45485014 2.20241189 0.40074855 2.20419955 0.34313178 2.2064476 0.49520618
		 1.78805137 0.54049087 1.78594255 0.56620443 2.026550531 0.58673692 2.19750714 0.5463109
		 2.19938111 0.075711429 1.55614436 0.41878659 1.54174018 0.38338196 1.54559231 0.34858912
		 1.54943657 0.31348646 1.55261576 0.27726662 1.55468392 0.29911977 1.55344057 0.23936284
		 1.5555532 0.19958717 1.5555222 0.15830725 1.55518007 0.17598671 1.55531728 0.11655402
		 1.55520344 0.45567822 1.53853261 0.49461508 1.53640878 0.24547428 1.066634536 0.26996958
		 1.065243721 0.27269888 1.20927978 0.24834925 1.21079946 0.20494521 1.067956686 0.20896703
		 1.21065378 0.16624141 1.068391204 0.1719138 1.20839226 0.12947911 1.068374276 0.14459866
		 1.068405151 0.15109879 1.20663524 0.13628483 1.20531487 0.09394455 1.068494916 0.10103089
		 1.20296931 0.058616042 1.06929338 0.065208197 1.20265913 0.4571709 1.18526745 0.40813953
		 1.053959846 0.41807866 1.19015884 0.36888409 1.057442307 0.37678349 1.19659007 0.32826555
		 1.061074972 0.33370119 1.2031033 0.28690046 1.064282894 0.29026902 1.20819545 0.44569767
		 1.051160932 0.4817878 1.049363852 0.49433529 1.18288422 0.23442829 0.85346031 0.26247448
		 0.8516463 0.27004391 0.94778866 0.24881709 0.94906193 0.18789679 0.8561545 0.21085858
		 0.95148993 0.16720784 0.8578651 0.17043746 0.95462203 0.11790758 0.86186087 0.13351738
		 0.8605929 0.14650714 0.95647424 0.12883884 0.95784074 0.081665516 0.86446625 0.087502182
		 0.96069169 0.046799898 0.86637717 0.04772079 0.96283901 0.42717332 0.94410175 0.39504039
		 0.84811825 0.39112085 0.94481289 0.35921693 0.84859091 0.3560974 0.94524765 0.32142287
		 0.8493278 0.32131416 0.94583929 0.28476411 0.84972793 0.28662574 0.94658709 0.42968744
		 0.84747392 0.46401685 0.84629178 0.4649384 0.94275403 0.56862009 0.16919169 0.28966945
		 0.16919169 0.28272098 0.0220204 0.45081431 0.0220204 0.13662779 0.16919169 0.20098537
		 0.022020549 0.27504581 -0.12345469 0.21744597 -0.12345469 0.20335972 -0.31042814
		 0.26277983 -0.31042814 0.40264142 -0.12345469 0.3745321 -0.31042814 -0.040272236
		 0.16919169 0.10275197 0.0220204 0.13359201 -0.12345469 0.12719262 -0.31042814 -0.26701427
		 0.098526165 -0.016990602 0.098526165 0.0045200586 -0.091023728 -0.13999057 -0.091023728
		 0.09695673 0.098526165 0.11623868 -0.091023728 0.33281636 0.098526165 0.26106757
		 -0.091023728 0.12163365 -0.27838933 0.23072243 -0.27838933 0.19042262 -0.51920283
		 0.090475589 -0.51920283 -0.0027359128 -0.27838933 -0.023471713 -0.51920283 -0.10289973
		 -0.27838933 -0.11524171 -0.51920283 0.34351856 0.46406189 0.012740195 0.46406189
		 0.013368845 0.28085637 0.18059933 0.28085637 -0.11296022 0.46406189 -0.097286224
		 0.28085637 -0.27421033 0.46406189 -0.22019207 0.28085637 -0.099504113 0.099762082
		 -0.21509534 0.099762082 -0.21731675 -0.13299119 -0.11141211 -0.13299119 0.016087174
		 0.099762082 -0.0055074096 -0.13299119 0.14470592 0.099762082 0.12886149 -0.13299119
		 -0.60594499 -0.014098954 -0.36743134 -0.014098954 -0.34867606 -0.19932258 -0.49586827
		 -0.19932258 -0.19578479 2.9802322e-008 -0.25978741 -0.1943666 -0.051058315 -0.014098954
		 0.035421193 -0.014098954 -0.077628016 -0.19932258 -0.14593558 -0.19932258 -0.18388774
		 -0.38241178 -0.1012988 -0.38241178 -0.16175604 -0.61772895 -0.20718168 -0.61772895
		 -0.35398993 -0.38241178 -0.30294225 -0.38241178 -0.3211568 -0.61772895 -0.36792663
		 -0.61772895 -0.46556565 -0.38241178 -0.4701522 -0.61772895 -0.40881342 -0.17741251
		 -0.60252905 -0.16464835 -0.34344429 -0.034274161 -0.32104981 -0.19003105 -0.14414994
		 -0.27825356 -0.19245981 -0.078607202 0.0087521896 0.14378741 -0.061299704 0.03853482
		 0.015199876 0.21234244 -0.28981715 0.1145272 -0.58191609 0.33158064 -0.42173904 0.23248753
		 -0.50487369 0.065862477 -0.47407115 -0.035114229 0.097907662 -0.58733463 0.16491699
		 -0.64539862 0.23028612 -0.50226021 0.2526806 -0.65801716 0.33333409 -0.62118393 0.38127059
		 -0.54659325 0.38127059 -0.50895965 0.38127059 -0.45792723 0.33333409 -0.38333654
		 0.2526806 -0.34650332 0.20077914 -0.35396564 0.16491705 -0.3591218 0.097907662 -0.41718575
		 0.072927535 -0.50226021;
createNode polyMapSewMove -n "polyMapSewMove4";
	rename -uid "78D30249-4542-0A7E-1879-1EAEB019D88C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[75]";
createNode polyTweakUV -n "polyTweakUV9";
	rename -uid "AF32CED9-4418-3AB3-DFFD-99B24CD74A0A";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[112:127]" -type "float2" -0.3124364 0.64876932 -0.47005075
		 0.44309029 -0.36546287 0.35481161 -0.27048582 0.4787519 -0.5565232 0.33024803 -0.41164571
		 0.29454529 -0.26253629 0.2669552 -0.29508173 0.22448504 -0.16517949 0.10845387 -0.13160563
		 0.15226626 -0.19044149 0.36103535 -0.06846267 0.2346645 -0.65647626 0.19981423 -0.46715
		 0.22211486 -0.34246141 0.16265696 -0.20821583 0.052293599;
createNode polyMapSewMove -n "polyMapSewMove5";
	rename -uid "C20D1F16-401D-7D70-37D5-F3955143A9F8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[30]" "e[99]" "e[267]";
createNode polyMapCut -n "polyMapCut1";
	rename -uid "D63E8E3B-4E86-4322-43E9-83B8C828E1FF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[75]";
createNode polyFlipUV -n "polyFlipUV1";
	rename -uid "3B9684C6-4FF8-4366-519B-D1B4C9110CD7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[0:2]" "f[60:62]" "f[133:135]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.52911600469999998;
	setAttr ".pv" 0.2896616682;
createNode polyTweakUV -n "polyTweakUV10";
	rename -uid "701E7640-4CB2-60DF-C27F-AEAB1428FF18";
	setAttr ".uopa" yes;
	setAttr -s 16 ".uvtk[128:143]" -type "float2" 0.38227618 0.38227287 0.31036037
		 0.48185986 0.22867346 0.43590635 0.27023995 0.37834626 0.27758503 0.52724624 0.19653922
		 0.48040509 0.20974326 0.62119162 0.15488124 0.5380919 0.12035769 0.42866087 0.08897984
		 0.47211206 0.0046530366 0.38679361 0.03340143 0.34698367 0.15613091 0.37912321 0.066176772
		 0.30159724 0.18494159 0.33922696 0.092573106 0.26504439;
createNode polyMapSewMove -n "polyMapSewMove6";
	rename -uid "C32FE82B-421B-E04E-39ED-F78AF96D8546";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[22]" "e[114]" "e[261]";
createNode polyFlipUV -n "polyFlipUV2";
	rename -uid "143E9F02-4CAC-40E1-7C77-0881F0BD6CE2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "f[3:5]" "f[57:59]" "f[121:122]" "f[130:132]" "f[141]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.19844925399999999;
	setAttr ".pv" 0.1911355257;
createNode polyTweakUV -n "polyTweakUV11";
	rename -uid "33989E3E-4318-4127-4C5F-CAB7F86001FC";
	setAttr ".uopa" yes;
	setAttr -s 20 ".uvtk[150:169]" -type "float2" 0.17874099 1.11752117 -0.013515994
		 0.97098923 0.085159048 0.81016505 0.20380518 0.90059328 -0.16053568 0.87690204 0.010464475
		 0.75955075 -0.26853251 0.77662402 -0.33824041 0.72349495 -0.13332269 0.64364547 -0.078262538
		 0.68561053 0.064810932 0.56134528 -0.0017609596 0.51060635 0.19153956 0.35806811
		 0.22815545 0.38597554 0.20192397 0.6658482 0.16077635 0.63448685 0.32002655 0.45599669
		 0.35772595 0.48472989 0.29186103 0.73439533 0.44012618 0.54753262;
createNode polyMapSewMove -n "polyMapSewMove7";
	rename -uid "884CCD93-4D83-09F4-BD90-6F9202EA1F76";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[28]" "e[102]" "e[271]";
createNode polyFlipUV -n "polyFlipUV3";
	rename -uid "B8A9DADF-48AC-1189-B3EF-43982CE13CAD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "f[0:10]" "f[55:65]" "f[121:122]" "f[130:144]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.47293415480000001;
	setAttr ".pv" 0.78846856949999999;
createNode polyTweakUV -n "polyTweakUV12";
	rename -uid "0F5DCC69-4CA0-4EFA-5A84-C4A05A132286";
	setAttr ".uopa" yes;
	setAttr -s 56 ".uvtk";
	setAttr ".uvtk[103]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[105]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[112]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[113]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[114]" -type "float2" 0.10637629 -0.31450373 ;
	setAttr ".uvtk[115]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[116]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[117]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[118]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[119]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[120]" -type "float2" 0.10637629 -0.31450373 ;
	setAttr ".uvtk[121]" -type "float2" 0.10637629 -0.31450373 ;
	setAttr ".uvtk[122]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[123]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[124]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[125]" -type "float2" 0.10637629 -0.31450373 ;
	setAttr ".uvtk[126]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[127]" -type "float2" 0.10637629 -0.31450373 ;
	setAttr ".uvtk[128]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[129]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[130]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[131]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[132]" -type "float2" 0.10637625 -0.31450373 ;
	setAttr ".uvtk[133]" -type "float2" 0.10637625 -0.31450373 ;
	setAttr ".uvtk[134]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[135]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[136]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[137]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[138]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[139]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[140]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[141]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[142]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[143]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[144]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[145]" -type "float2" 0.10637629 -0.31450373 ;
	setAttr ".uvtk[146]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[147]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[148]" -type "float2" 0.10637626 -0.31450373 ;
	setAttr ".uvtk[149]" -type "float2" 0.10637629 -0.31450373 ;
	setAttr ".uvtk[150]" -type "float2" 0.10637629 -0.31450373 ;
	setAttr ".uvtk[151]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[152]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[153]" -type "float2" 0.10637629 -0.31450373 ;
	setAttr ".uvtk[154]" -type "float2" 0.10637629 -0.31450373 ;
	setAttr ".uvtk[155]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[156]" -type "float2" 0.10637629 -0.31450373 ;
	setAttr ".uvtk[157]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[158]" -type "float2" 0.10637629 -0.31450373 ;
	setAttr ".uvtk[159]" -type "float2" 0.10637629 -0.31450373 ;
	setAttr ".uvtk[160]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[161]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[162]" -type "float2" 0.10637629 -0.31450373 ;
	setAttr ".uvtk[163]" -type "float2" 0.10637629 -0.31450373 ;
	setAttr ".uvtk[164]" -type "float2" 0.10637623 -0.31450373 ;
	setAttr ".uvtk[165]" -type "float2" 0.10637623 -0.31450373 ;
createNode polyMapCut -n "polyMapCut2";
	rename -uid "092B5EA6-4350-8462-FD09-9FBAED771B77";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[24]" "e[110]" "e[257]";
createNode polyTweakUV -n "polyTweakUV13";
	rename -uid "E35EFEF5-4F11-25AF-2F3E-2C819AB5EA9E";
	setAttr ".uopa" yes;
	setAttr -s 8 ".uvtk";
	setAttr ".uvtk[134]" -type "float2" 1.0102627 0.5001229 ;
	setAttr ".uvtk[135]" -type "float2" 0.81163502 0.38135275 ;
	setAttr ".uvtk[137]" -type "float2" 0.65161222 0.23517707 ;
	setAttr ".uvtk[138]" -type "float2" 0.44477981 0.04822436 ;
	setAttr ".uvtk[196]" -type "float2" 0.68201196 0.48405269 ;
	setAttr ".uvtk[197]" -type "float2" 0.55397707 0.31253347 ;
	setAttr ".uvtk[198]" -type "float2" 0.35532677 0.11909822 ;
	setAttr ".uvtk[199]" -type "float2" 0.7991668 0.66737443 ;
createNode polyMapSewMove -n "polyMapSewMove8";
	rename -uid "0C7E97C8-46FD-B293-5D3C-069F5C042E53";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[25]" "e[108]" "e[255]";
createNode polyFlipUV -n "polyFlipUV4";
	rename -uid "08CD0F52-42C9-D3EE-5562-90BD9E7AE461";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "f[0:10]" "f[55:65]" "f[121:122]" "f[130:144]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 11.354926954990134 0 0 0 0 1 0 0 11.246859072099889 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.64020292459999995;
	setAttr ".pv" 0.5228088796;
createNode polyTweakUV -n "polyTweakUV14";
	rename -uid "02A251EC-4CC3-D6B5-9F76-82A5FC31F362";
	setAttr ".uopa" yes;
	setAttr -s 56 ".uvtk";
	setAttr ".uvtk[103]" -type "float2" -0.23981851 0.45929611 ;
	setAttr ".uvtk[105]" -type "float2" -0.24684691 0.38942814 ;
	setAttr ".uvtk[112]" -type "float2" -0.15074876 -0.14937502 ;
	setAttr ".uvtk[113]" -type "float2" -0.025086701 0.0054183602 ;
	setAttr ".uvtk[114]" -type "float2" -0.10714431 0.088407427 ;
	setAttr ".uvtk[115]" -type "float2" -0.21114415 -0.019321769 ;
	setAttr ".uvtk[116]" -type "float2" 0.057001159 0.094624579 ;
	setAttr ".uvtk[117]" -type "float2" -0.063303068 0.1360504 ;
	setAttr ".uvtk[118]" -type "float2" -0.18782319 0.17091066 ;
	setAttr ".uvtk[119]" -type "float2" -0.15692805 0.20448494 ;
	setAttr ".uvtk[120]" -type "float2" -0.25835711 0.31298393 ;
	setAttr ".uvtk[121]" -type "float2" -0.29022872 0.27834839 ;
	setAttr ".uvtk[122]" -type "float2" -0.25928438 0.094449759 ;
	setAttr ".uvtk[123]" -type "float2" -0.34040898 0.22907931 ;
	setAttr ".uvtk[124]" -type "float2" 0.13788466 0.1586414 ;
	setAttr ".uvtk[125]" -type "float2" 0.0016080588 0.21624726 ;
	setAttr ".uvtk[126]" -type "float2" -0.10760619 0.26412255 ;
	setAttr ".uvtk[127]" -type "float2" -0.23663329 0.32908088 ;
	setAttr ".uvtk[128]" -type "float2" 0.15650843 0.50428283 ;
	setAttr ".uvtk[129]" -type "float2" 0.11961852 0.61729753 ;
	setAttr ".uvtk[130]" -type "float2" 0.014203653 0.60735846 ;
	setAttr ".uvtk[131]" -type "float2" 0.035557613 0.51623017 ;
	setAttr ".uvtk[132]" -type "float2" 0.10661982 0.67919207 ;
	setAttr ".uvtk[133]" -type "float2" 0.0014591068 0.6680426 ;
	setAttr ".uvtk[134]" -type "float2" -0.53986716 -0.22401994 ;
	setAttr ".uvtk[135]" -type "float2" -0.52154982 -0.081136346 ;
	setAttr ".uvtk[136]" -type "float2" -0.10093093 0.64959908 ;
	setAttr ".uvtk[137]" -type "float2" -0.51568735 0.051865458 ;
	setAttr ".uvtk[138]" -type "float2" -0.52337652 0.22039628 ;
	setAttr ".uvtk[139]" -type "float2" -0.22818309 0.60520303 ;
	setAttr ".uvtk[140]" -type "float2" -0.086743131 0.58204323 ;
	setAttr ".uvtk[141]" -type "float2" -0.21518433 0.54330844 ;
	setAttr ".uvtk[142]" -type "float2" -0.07735087 0.51994431 ;
	setAttr ".uvtk[143]" -type "float2" -0.21875316 0.51131219 ;
	setAttr ".uvtk[144]" -type "float2" 0.14583413 0.3377685 ;
	setAttr ".uvtk[145]" -type "float2" 0.024926826 0.34951156 ;
	setAttr ".uvtk[146]" -type "float2" 0.15417574 0.42069638 ;
	setAttr ".uvtk[147]" -type "float2" 0.032270059 0.42251366 ;
	setAttr ".uvtk[148]" -type "float2" -0.087055519 0.43599445 ;
	setAttr ".uvtk[149]" -type "float2" -0.09472622 0.35973573 ;
	setAttr ".uvtk[150]" -type "float2" -0.40003204 -0.18836808 ;
	setAttr ".uvtk[151]" -type "float2" -0.40882719 -0.060557485 ;
	setAttr ".uvtk[152]" -type "float2" -0.28197116 -0.17799217 ;
	setAttr ".uvtk[153]" -type "float2" -0.34796065 -0.053593397 ;
	setAttr ".uvtk[154]" -type "float2" -0.18544509 -0.15161735 ;
	setAttr ".uvtk[155]" -type "float2" -0.27131397 -0.037006527 ;
	setAttr ".uvtk[156]" -type "float2" -0.31832412 0.082769036 ;
	setAttr ".uvtk[157]" -type "float2" -0.36145902 0.23967212 ;
	setAttr ".uvtk[158]" -type "float2" -0.43369955 0.063009441 ;
	setAttr ".uvtk[159]" -type "float2" -0.39907539 0.068939328 ;
	setAttr ".uvtk[160]" -type "float2" -0.43876523 0.22643238 ;
	setAttr ".uvtk[161]" -type "float2" -0.47048771 0.22099948 ;
	setAttr ".uvtk[192]" -type "float2" -0.63708764 -0.058375835 ;
	setAttr ".uvtk[193]" -type "float2" -0.59931624 0.073447585 ;
	setAttr ".uvtk[194]" -type "float2" -0.57776868 0.2497763 ;
	setAttr ".uvtk[195]" -type "float2" -0.68509781 -0.18923157 ;
createNode polyMapSewMove -n "polyMapSewMove9";
	rename -uid "B594EF6A-424B-597E-BB8F-B3BF1E364A9A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 12 "e[57]" "e[59]" "e[61]" "e[63]" "e[65]" "e[67]" "e[69]" "e[71]" "e[73]" "e[75:76]" "e[234]" "e[279]";
createNode polyTweakUV -n "polyTweakUV15";
	rename -uid "4A995DBB-4865-F56E-5FCC-A280867C3186";
	setAttr ".uopa" yes;
	setAttr -s 182 ".uvtk[0:181]" -type "float2" -0.2154216 -1.73938346 -0.24218738
		 -1.73862386 -0.26681471 -2.01354599 -0.23435779 -2.014433384 -0.17131595 -1.74097824
		 -0.17843403 -2.016563177 -0.12805912 -1.74279547 -0.12096664 -2.019195318 -0.084250003
		 -1.74470639 -0.10311632 -1.74388707 -0.087522164 -2.020819187 -0.063389882 -2.022001266
		 -0.038424671 -1.74655819 -0.007199347 -2.024595261 0.010515422 -1.74822271 0.046828896
		 -2.026667833 -0.49076352 -2.010322332 -0.413452 -1.73516369 -0.44022334 -2.011142969
		 -0.36091217 -1.73610616 -0.39021909 -2.011621475 -0.31007862 -1.73702025 -0.33980647
		 -2.012111902 -0.26150352 -1.73807466 -0.28808498 -2.012967825 -0.25616232 -2.21081853
		 -0.21940795 -2.21183562 -0.16198698 -2.21401119 -0.11056714 -2.2164607 -0.082379863
		 -2.21792698 -0.063196838 -2.21895909 -0.017879024 -2.2212255 0.027120352 -2.22302675
		 -0.46639612 -2.20730639 -0.41119313 -2.20794129 -0.34915835 -2.20878148 -0.28308532
		 -2.21007013 -0.46669477 -1.73402834 -0.5186336 -1.73262334 -0.54272306 -2.0089097023
		 -0.56243157 -2.20526767 -0.5160656 -2.20651197 0.008822158 -1.45891404 -0.38462868
		 -1.45007348 -0.34397265 -1.45369649 -0.30401769 -1.45732427 -0.26372245 -1.46018291
		 -0.22217214 -1.46174359 -0.2472413 -1.46080709 -0.17871884 -1.4618926 -0.13314055
		 -1.46096861 -0.085845768 -1.45965433 -0.10610153 -1.46020675 -0.038000375 -1.45874834
		 -0.42697445 -1.44722188 -0.47163966 -1.44565809 -0.19664562 -0.90177858 -0.22474545
		 -0.90073192 -0.22465488 -1.065843582 -0.19671884 -1.067041039 -0.1501735 -0.9023881
		 -0.1515941 -1.065994501 -0.10581331 -0.90202153 -0.10918526 -1.062574863 -0.063687831
		 -0.90118074 -0.081012592 -0.90155375 -0.085372612 -1.060096502 -0.068426773 -1.058252573
		 -0.022966072 -0.90052474 -0.028081849 -1.054777145 0.017534591 -0.90065056 0.012960196
		 -1.053621531 -0.4365775 -1.042449594 -0.38332617 -0.89088887 -0.39167252 -1.047181249
		 -0.33826587 -0.89400256 -0.34420881 -1.053627968 -0.29163975 -0.89725769 -0.29469529
		 -1.060129046 -0.244168 -0.90000939 -0.2448128 -1.06499362 -0.42642653 -0.88852096
		 -0.46782249 -0.88726765 -0.47921735 -1.040548801 -0.18428789 -0.63756979 -0.22519404
		 -0.63623792 -0.22745489 -0.77042598 -0.20310281 -0.77146387 -0.12978317 -0.63729346
		 -0.15955167 -0.77350301 -0.12106699 -0.65084314 -0.11316332 -0.77633405 -0.04588519
		 -0.69915223 -0.070613332 -0.68367428 -0.094068497 -0.77800775 -0.069549516 -0.77924252
		 -0.01523874 -0.72063082 -0.017992452 -0.78171253 -0.001042828 -0.75187415 0.028791092
		 -0.78115237 -0.40759161 -0.76967293 -0.38485843 -0.68003458 -0.36626306 -0.76968324
		 -0.33991486 -0.67994642 -0.3261202 -0.76938325 -0.27705124 -0.66204464 -0.28624892
		 -0.76927882 -0.2369951 -0.63137549 -0.24648258 -0.76936507 -0.40921471 -0.68720055
		 -0.43429428 -0.71901339 -0.4480201 -0.76676166 0.072710186 -0.20716317 -0.14363357
		 -0.20985979 -0.15599847 -0.32181263 0.0057554245 -0.33440274 -0.27502543 -0.20335005
		 -0.22604169 -0.31814057 -0.16369498 -0.43798262 -0.21101661 -0.43546227 -0.055194274
		 -0.44466743 -0.38457519 -0.18035318 -0.33778566 -0.31978625 -0.291381 -0.43575233
		 -0.64798504 -0.43738887 -0.7125259 -0.51557916 -0.67163867 -0.61736965 -0.60017717
		 -0.52444059 -0.77662444 -0.55943567 -0.70960689 -0.66925412 0.40516013 -0.47982132
		 0.2723532 -0.5624662 -0.59269303 -0.73470998 0.14916474 -0.6342026 -0.56193298 -0.66264319
		 -0.51843446 -0.6041922 -0.53947192 -0.29810923 -0.46487322 -0.3969866 -0.61424434
		 -0.35934615 -0.52616173 -0.45063177 -0.44900772 -0.54414368 -0.37623867 -0.49300212
		 0.27832854 -0.38796735 0.17709488 -0.47827518 0.18769479 -0.29671973 0.1291109 -0.4297727
		 0.098673008 -0.23453981 0.062179923 -0.37442887 -0.0066417903 -0.4869706 0.08832702
		 -0.57304436 0.059992015 -0.54619884 0.43416035 -0.30598971 0.42931184 -0.30991462
		 0.39968705 -0.33089691 0.42115504 -0.29668283 0.39583361 -0.32617947 0.39676768 -0.33003971
		 0.39287716 -0.3343398 0.39423168 -0.33230472 0.39275253 -0.33566535 0.39865011 -0.33377403
		 0.40429795 -0.33797085 0.40120089 -0.33605486 0.40280831 -0.33283311 0.40221274 -0.33088067
		 0.058913112 0.0046565682 0.057617486 0.0057792515 0.056353569 0.0030116215 0.055920541
		 0.0060232431 0.054361105 0.005311057 0.053434193 0.0038688183 0.053434193 0.0031411573
		 0.053434193 0.0021544248 0.054361105 0.00071218424 0.055920541 0 0.056924105 0.00014428701
		 0.057617486 0.00024398323 0.058913112 0.0013666712 0.059396148 0.0030116215 0.33348849
		 -0.68092293 0.18343154 -0.72969091 0.45509622 -0.59388566;
createNode animCurveTL -n "pCylinderShape1_pnts_29__pntx";
	rename -uid "0B6A26BB-4B5C-CE7A-C1CF-569422C06663";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_29__pnty";
	rename -uid "BFAA2166-4EAC-1142-D88C-29B94D97E8E0";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_29__pntz";
	rename -uid "6CDE11A1-4E0C-1F86-66B5-D6BDF9194CF6";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_30__pntx";
	rename -uid "D604C652-4818-69CB-E266-C6BAA07A53EE";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_30__pnty";
	rename -uid "28B819A2-45BA-37EB-EC35-439DB2881264";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_30__pntz";
	rename -uid "BDB64AAA-4DD5-39D9-1F77-7F876170BD1F";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_31__pntx";
	rename -uid "376B809F-4434-C0FF-F9E0-37A31310A4CF";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_31__pnty";
	rename -uid "8E97B2AB-422C-3563-D09F-20B73220E21A";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_31__pntz";
	rename -uid "9836B349-4B24-C606-8DF2-5D9AF1DEE260";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_83__pntx";
	rename -uid "FE724FF4-41F2-9064-D710-F485E88F5DB3";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_83__pnty";
	rename -uid "FD4A72A7-4DCE-4B48-9B04-4CA7FB5C82D5";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_83__pntz";
	rename -uid "36404E0F-46A5-A581-F535-91B24A03335D";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_84__pntx";
	rename -uid "C366884B-417D-E19C-814D-E59DDC59B93B";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_84__pnty";
	rename -uid "CDE74472-43CF-043A-9180-1CBF2CB265E6";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_84__pntz";
	rename -uid "0591D222-4243-9168-4948-2782A62CC317";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_85__pntx";
	rename -uid "ECE931FF-4FFB-1910-99C5-D2B088100267";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_85__pnty";
	rename -uid "FE023068-4CF6-D062-C79F-0198174B403B";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
createNode animCurveTL -n "pCylinderShape1_pnts_85__pntz";
	rename -uid "833FFC18-4020-9580-D500-A2BA7670BE3E";
	setAttr ".tan" 3;
	setAttr ".ktv[0]"  1 0;
	setAttr ".kot[0]"  5;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polyTweakUV15.out" "pCylinderShape1.i";
connectAttr "polyTweakUV15.uvtk[0]" "pCylinderShape1.uvst[0].uvtw";
connectAttr "pCylinderShape1_pnts_29__pntx.o" "pCylinderShape1.pt[29].px";
connectAttr "pCylinderShape1_pnts_29__pnty.o" "pCylinderShape1.pt[29].py";
connectAttr "pCylinderShape1_pnts_29__pntz.o" "pCylinderShape1.pt[29].pz";
connectAttr "pCylinderShape1_pnts_30__pntx.o" "pCylinderShape1.pt[30].px";
connectAttr "pCylinderShape1_pnts_30__pnty.o" "pCylinderShape1.pt[30].py";
connectAttr "pCylinderShape1_pnts_30__pntz.o" "pCylinderShape1.pt[30].pz";
connectAttr "pCylinderShape1_pnts_31__pntx.o" "pCylinderShape1.pt[31].px";
connectAttr "pCylinderShape1_pnts_31__pnty.o" "pCylinderShape1.pt[31].py";
connectAttr "pCylinderShape1_pnts_31__pntz.o" "pCylinderShape1.pt[31].pz";
connectAttr "pCylinderShape1_pnts_83__pntx.o" "pCylinderShape1.pt[83].px";
connectAttr "pCylinderShape1_pnts_83__pnty.o" "pCylinderShape1.pt[83].py";
connectAttr "pCylinderShape1_pnts_83__pntz.o" "pCylinderShape1.pt[83].pz";
connectAttr "pCylinderShape1_pnts_84__pntx.o" "pCylinderShape1.pt[84].px";
connectAttr "pCylinderShape1_pnts_84__pnty.o" "pCylinderShape1.pt[84].py";
connectAttr "pCylinderShape1_pnts_84__pntz.o" "pCylinderShape1.pt[84].pz";
connectAttr "pCylinderShape1_pnts_85__pntx.o" "pCylinderShape1.pt[85].px";
connectAttr "pCylinderShape1_pnts_85__pnty.o" "pCylinderShape1.pt[85].py";
connectAttr "pCylinderShape1_pnts_85__pntz.o" "pCylinderShape1.pt[85].pz";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyTweak4.out" "polySplitRing11.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing11.mp";
connectAttr "polySplitRing10.out" "polyTweak4.ip";
connectAttr "polyTweak3.out" "polySplitRing10.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing10.mp";
connectAttr "polySplitRing9.out" "polyTweak3.ip";
connectAttr "polySoftEdge1.out" "polySplitRing9.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing9.mp";
connectAttr "polyTweak2.out" "polySoftEdge1.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge1.mp";
connectAttr "polySplitRing8.out" "polyTweak2.ip";
connectAttr "polySplitRing7.out" "polySplitRing8.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing8.mp";
connectAttr "polyTweak1.out" "polySplitRing7.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing7.mp";
connectAttr "polySplitRing6.out" "polyTweak1.ip";
connectAttr "polySplitRing5.out" "polySplitRing6.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing6.mp";
connectAttr "polySplitRing4.out" "polySplitRing5.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing5.mp";
connectAttr "polySplitRing3.out" "polySplitRing4.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing4.mp";
connectAttr "polySplitRing2.out" "polySplitRing3.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing1.out" "polySplitRing2.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing2.mp";
connectAttr "polyCylinder1.out" "polySplitRing1.ip";
connectAttr "pCylinderShape1.wm" "polySplitRing1.mp";
connectAttr "polyTweak5.out" "polyMapDel1.ip";
connectAttr "polySplitRing11.out" "polyTweak5.ip";
connectAttr "polyMapDel1.out" "polyCylProj1.ip";
connectAttr "pCylinderShape1.wm" "polyCylProj1.mp";
connectAttr "polyCylProj1.out" "polyTweakUV1.ip";
connectAttr "polyTweakUV1.out" "polyCylProj2.ip";
connectAttr "pCylinderShape1.wm" "polyCylProj2.mp";
connectAttr "polyCylProj2.out" "polyTweakUV2.ip";
connectAttr "polyTweakUV2.out" "polyCylProj3.ip";
connectAttr "pCylinderShape1.wm" "polyCylProj3.mp";
connectAttr "polyCylProj3.out" "polyTweakUV3.ip";
connectAttr "polyTweakUV3.out" "polyMapSewMove1.ip";
connectAttr "polyMapSewMove1.out" "polyTweakUV4.ip";
connectAttr "polyTweakUV4.out" "polyMapSewMove2.ip";
connectAttr "polyMapSewMove2.out" "polyTweakUV5.ip";
connectAttr "polyTweakUV5.out" "polyCylProj4.ip";
connectAttr "pCylinderShape1.wm" "polyCylProj4.mp";
connectAttr "polyCylProj4.out" "polyTweakUV6.ip";
connectAttr "polyTweakUV6.out" "polyMapSewMove3.ip";
connectAttr "polyMapSewMove3.out" "polyTweakUV7.ip";
connectAttr "polyTweakUV7.out" "polyPlanarProj1.ip";
connectAttr "pCylinderShape1.wm" "polyPlanarProj1.mp";
connectAttr "polyPlanarProj1.out" "polyMapDel2.ip";
connectAttr "polyMapDel2.out" "polyPlanarProj2.ip";
connectAttr "pCylinderShape1.wm" "polyPlanarProj2.mp";
connectAttr "polyPlanarProj2.out" "polyPlanarProj3.ip";
connectAttr "pCylinderShape1.wm" "polyPlanarProj3.mp";
connectAttr "polyPlanarProj3.out" "polyPlanarProj4.ip";
connectAttr "pCylinderShape1.wm" "polyPlanarProj4.mp";
connectAttr "polyPlanarProj4.out" "polyPlanarProj5.ip";
connectAttr "pCylinderShape1.wm" "polyPlanarProj5.mp";
connectAttr "polyPlanarProj5.out" "polyTweakUV8.ip";
connectAttr "polyTweakUV8.out" "polyMapSewMove4.ip";
connectAttr "polyMapSewMove4.out" "polyTweakUV9.ip";
connectAttr "polyTweakUV9.out" "polyMapSewMove5.ip";
connectAttr "polyMapSewMove5.out" "polyMapCut1.ip";
connectAttr "polyMapCut1.out" "polyFlipUV1.ip";
connectAttr "pCylinderShape1.wm" "polyFlipUV1.mp";
connectAttr "polyFlipUV1.out" "polyTweakUV10.ip";
connectAttr "polyTweakUV10.out" "polyMapSewMove6.ip";
connectAttr "polyMapSewMove6.out" "polyFlipUV2.ip";
connectAttr "pCylinderShape1.wm" "polyFlipUV2.mp";
connectAttr "polyFlipUV2.out" "polyTweakUV11.ip";
connectAttr "polyTweakUV11.out" "polyMapSewMove7.ip";
connectAttr "polyMapSewMove7.out" "polyFlipUV3.ip";
connectAttr "pCylinderShape1.wm" "polyFlipUV3.mp";
connectAttr "polyFlipUV3.out" "polyTweakUV12.ip";
connectAttr "polyTweakUV12.out" "polyMapCut2.ip";
connectAttr "polyMapCut2.out" "polyTweakUV13.ip";
connectAttr "polyTweakUV13.out" "polyMapSewMove8.ip";
connectAttr "polyMapSewMove8.out" "polyFlipUV4.ip";
connectAttr "pCylinderShape1.wm" "polyFlipUV4.mp";
connectAttr "polyFlipUV4.out" "polyTweakUV14.ip";
connectAttr "polyTweakUV14.out" "polyMapSewMove9.ip";
connectAttr "polyMapSewMove9.out" "polyTweakUV15.ip";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCylinderShape1.iog" ":initialShadingGroup.dsm" -na;
// End of SwampTallTreeNoLeaves.ma
