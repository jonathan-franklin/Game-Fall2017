//Maya ASCII 2017ff05 scene
//Name: TallMountain.ma
//Last modified: Tue, Nov 14, 2017 12:32:53 PM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "42D872F8-4BE1-640A-0DA4-EFA6CCD6B50A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 15.918835943167846 2.618500259329557 8.0209519100176436 ;
	setAttr ".r" -type "double3" -9.9383527098340814 1147.7999999979111 -2.1044278384823229e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "2D930285-4B44-D55B-369B-14A8C5F924A4";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 18.766778214647598;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.95339601634466309 3.7584469704562946 2.6577032886519492 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "3121EDCF-46FE-C9EB-9E86-858067E4AAAF";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "E8AEA026-4531-7E36-BEE7-2BAB8F24E230";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "918877CC-45D9-66B4-A1FD-D6A52A86357D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "4892AA03-49B6-3A01-635B-678F9BC9DDC0";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "4E04BC27-4D93-0E46-B8A0-6C9E3D80B016";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "BE7BDAF2-4246-C27B-364B-F5BD1E540741";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCube1";
	rename -uid "59568839-4594-6285-8843-F1BE9CA60403";
	setAttr ".t" -type "double3" 0 7.247934129457982 0 ;
	setAttr ".s" -type "double3" 1.8281981762119068 6.4799443071415848 4.2992373660819165 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "9707D77F-4851-8F1B-F5AD-B29643F73B5B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.48294205174897797 0.083830041927732676 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "DDAB1FF1-47E2-7D17-E473-BD9ECCFC725B";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "7DEB1617-44B4-FF3A-4854-62AB3766D7F7";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "2BE2BB68-41F1-8A44-1D96-1DB58AC56463";
createNode displayLayerManager -n "layerManager";
	rename -uid "C7737C9B-494A-7E9A-2371-26A134AC1A89";
createNode displayLayer -n "defaultLayer";
	rename -uid "6B9D3E9D-40F6-A6EC-ECE5-71BD0C87E492";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "5C9ED005-4247-A0FB-E9F1-979AE2A9C49A";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "BB6AC91B-4C85-24A0-06C3-E0820E9643E0";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "2AAB5868-45F4-4126-8EA9-3DA7FA095D60";
	setAttr ".cuv" 4;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "FE08EAD4-4BA0-2EA2-6849-B49E7C4CB0E6";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 543\n            -height 811\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n"
		+ "            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n"
		+ "            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n"
		+ "            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n"
		+ "            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n"
		+ "                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n"
		+ "                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n"
		+ "                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n"
		+ "                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n"
		+ "                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 543\\n    -height 811\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 543\\n    -height 811\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "2A80D019-48CE-671C-68C0-74B9E71A9799";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "CF5D8786-4A5F-AD91-1A69-EAA30639DDC7";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 5.4448720920258875 0 0 0 0 19.299039007326776 0 0 0 0 12.80429980522036 0
		 0 7.247934129457982 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.0456269 -2.4015853 0.10049151 ;
	setAttr ".rs" 33420;
	setAttr ".lt" -type "double3" 0 -1.5040783102635924e-015 13.992614006607305 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -4.69650633794814 -2.4015853742054061 -7.6888597629714743 ;
	setAttr ".cbx" -type "double3" 6.7877601146462627 -2.4015853742054061 7.8898427823178272 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "42188005-4022-BAB0-E20F-298F881F9FF9";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  -0.29940128 0 0 0 0 0.11618701
		 0 -0.34218422 -0.22698207 0 -0.6430285 0 0 0 0.34277654 0.13930893 -0.35907668 0.27724341
		 -0.36255589 0 -0.10049048 0.74663353 0 0;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "05264470-499A-C282-DD22-FC99FBA246BE";
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.35108504 -0.69026971 0.033741545 ;
	setAttr ".rs" 44376;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.6450695822802044 -0.69026971056849895 -4.0306190821830485 ;
	setAttr ".cbx" -type "double3" 3.3472396747888888 -0.69026971056849895 4.0981021714472909 ;
createNode polyTweak -n "polyTweak2";
	rename -uid "0F6AB997-4C9E-5ED0-3DE8-98A69D56CDD9";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[8:11]" -type "float3"  -0.58426195 0 -0.3370291 0.58426195
		 0 -0.28135571 0.17061536 0 0.3370291 -0.54927307 0 0.2726599;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "F48DF91F-4683-AA2D-9CA6-FFB96597D4D1";
	setAttr ".ics" -type "componentList" 1 "f[12]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.61981302 -1.6131897 3.709975 ;
	setAttr ".rs" 45994;
	setAttr ".lt" -type "double3" 0 0 3.7377760995804721 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.4656438906876335 -2.5361096640134972 3.3218480476585945 ;
	setAttr ".cbx" -type "double3" 1.2260178055913782 -0.69026971056849895 4.0981021714472909 ;
createNode polyTweak -n "polyTweak3";
	rename -uid "613B66AA-4D60-DFB9-7348-87B098C3370D";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[12:15]" -type "float3"  0 -0.28485435 0 0 -0.28485435
		 0 0 -0.28485435 0 0 -0.28485435 0;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "C2F39E54-4C08-9870-C40C-C580B589B69E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[6:7]" "e[10:11]" "e[16]" "e[19]" "e[24]" "e[27]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.54755783081054688;
	setAttr ".dr" no;
	setAttr ".re" 24;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "23B4A3BA-48DF-7123-D785-D4A7D2B44DED";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk";
	setAttr ".tk[8]" -type "float3" 0 0.14116213 0 ;
	setAttr ".tk[9]" -type "float3" 0 -0.19944403 0 ;
	setAttr ".tk[12]" -type "float3" -0.36357391 -0.20551032 -0.44934565 ;
	setAttr ".tk[13]" -type "float3" 0 -0.20551032 0 ;
	setAttr ".tk[14]" -type "float3" 0 -0.20551032 0 ;
	setAttr ".tk[15]" -type "float3" 0 -0.20551032 0 ;
	setAttr ".tk[16]" -type "float3" 0 -0.24886653 -0.56129521 ;
	setAttr ".tk[17]" -type "float3" 0 -0.24886653 -0.67849243 ;
	setAttr ".tk[18]" -type "float3" 0 -0.14579128 -0.56129521 ;
	setAttr ".tk[19]" -type "float3" 0 -0.14579128 -0.67849243 ;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "DA3AAE13-4AE8-AB68-7267-E8AD0AFF97C9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[0:3]" "e[14]" "e[18]" "e[22]" "e[26]" "e[30]" "e[34]" "e[38]" "e[46]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.42133930325508118;
	setAttr ".re" 14;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak5";
	rename -uid "E71F0BFB-45F6-628C-E378-91A71EFE44A7";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[20:27]" -type "float3"  0.80482602 0 0 -0.80482602
		 0 0 -0.70865232 0 0 -0.37825945 0 0 -0.18650614 0 0 0.43500301 0 0 0.59565496 0 0
		 0.80482602 0 0;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "B36A2846-4A5F-E6B5-2068-1F8B1BE1A083";
	setAttr ".ics" -type "componentList" 2 "f[12]" "f[30]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.3889443 -2.89187 4.9382243 ;
	setAttr ".rs" 43015;
	setAttr ".lt" -type "double3" -0.11018764045064766 3.5527136788005009e-015 3.2995393252140355 ;
	setAttr ".ls" -type "double3" 0.15546997419665262 1 1 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -3.2347750610348287 -3.480829117874479 4.5664934009736733 ;
	setAttr ".cbx" -type "double3" 0.45688652627508003 -2.3029111468041998 5.3099548906030352 ;
createNode polyTweak -n "polyTweak6";
	rename -uid "991FFF91-474B-1236-DEE5-AEA218FB0A15";
	setAttr ".uopa" yes;
	setAttr -s 18 ".tk";
	setAttr ".tk[11]" -type "float3" -0.95497841 0 0 ;
	setAttr ".tk[15]" -type "float3" -0.95497841 0 0 ;
	setAttr ".tk[16]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[17]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[18]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[19]" -type "float3" 0 0 -5.9604645e-008 ;
	setAttr ".tk[28]" -type "float3" 0 0 -0.11198693 ;
	setAttr ".tk[29]" -type "float3" 0 0 -0.14623289 ;
	setAttr ".tk[30]" -type "float3" 0 0 -0.0034078744 ;
	setAttr ".tk[31]" -type "float3" 0 0 0.11460716 ;
	setAttr ".tk[32]" -type "float3" 0 0 0.14623293 ;
	setAttr ".tk[33]" -type "float3" 0 0 0.14623293 ;
	setAttr ".tk[34]" -type "float3" 0 0 0.11460716 ;
	setAttr ".tk[35]" -type "float3" 0 0 0.075124264 ;
	setAttr ".tk[36]" -type "float3" 0 0 0.051377736 ;
	setAttr ".tk[37]" -type "float3" 0 1.1350494e-008 0.018395718 ;
	setAttr ".tk[38]" -type "float3" 0 0 -0.021520024 ;
	setAttr ".tk[39]" -type "float3" 0 0 -0.070687726 ;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "47579C3E-4F13-9998-BAAB-329725DA81DC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[75:76]" "e[78]" "e[80]" "e[83]" "e[85]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.34007462859153748;
	setAttr ".re" 76;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "2B438EAA-4491-384A-106A-BBAEA998A580";
	setAttr ".ics" -type "componentList" 2 "f[44]" "f[49]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.8725655 -2.302911 6.976572 ;
	setAttr ".rs" 56921;
	setAttr ".lt" -type "double3" -3.0545278627410952e-017 1.0447397545058738e-015 4.6232005558584266 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -4.1002911463780674 -2.3029119192737566 5.6694845891469123 ;
	setAttr ".cbx" -type "double3" 0.35516021827900257 -2.3029103743346431 8.2836599091366736 ;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "BF98DCCD-4071-B21E-393D-11BB72C86E82";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[99:100]" "e[102]" "e[104]" "e[107]" "e[109]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.42478996515274048;
	setAttr ".dr" no;
	setAttr ".re" 100;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak7";
	rename -uid "12529FF2-4EEA-A90D-B72C-3EBF7CDCE0A5";
	setAttr ".uopa" yes;
	setAttr -s 18 ".tk";
	setAttr ".tk[16]" -type "float3" 0 -0.060679201 0 ;
	setAttr ".tk[17]" -type "float3" 0 -0.12106396 0 ;
	setAttr ".tk[18]" -type "float3" -0.65900904 0 -0.028606448 ;
	setAttr ".tk[19]" -type "float3" 0.49549645 0 0.12165167 ;
	setAttr ".tk[33]" -type "float3" 0 0.090033941 -0.11341606 ;
	setAttr ".tk[41]" -type "float3" 0 0 0.084762678 ;
	setAttr ".tk[42]" -type "float3" -0.61485797 0 0.10978372 ;
	setAttr ".tk[43]" -type "float3" 0 0 0.21457906 ;
	setAttr ".tk[45]" -type "float3" 0.27571103 0 0 ;
	setAttr ".tk[46]" -type "float3" 0 0.10040203 -0.19356912 ;
	setAttr ".tk[48]" -type "float3" 0.58257252 0 0.13967009 ;
	setAttr ".tk[50]" -type "float3" -0.32823628 2.220446e-016 0.0077132275 ;
	setAttr ".tk[52]" -type "float3" -0.025021737 0.11250409 -0.014208421 ;
	setAttr ".tk[53]" -type "float3" -0.70917988 -0.084833995 0.14310609 ;
	setAttr ".tk[54]" -type "float3" 0.045792975 -0.037177201 -0.17249154 ;
	setAttr ".tk[55]" -type "float3" -0.64633828 -0.041725677 -0.1935944 ;
	setAttr ".tk[56]" -type "float3" 0.52734995 0.041725591 0.19359449 ;
	setAttr ".tk[57]" -type "float3" 0.70917982 -0.026601156 -0.12342128 ;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "86F3ECF2-4625-4BF2-ADAD-02A9932E0CBB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "e[6:7]" "e[36:37]" "e[39]" "e[41]" "e[47]" "e[49]" "e[58]" "e[69]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.48142388463020325;
	setAttr ".re" 37;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak8";
	rename -uid "D7770525-4010-2D20-A23D-40932F6705AE";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk[58:63]" -type "float3"  0.07909146 0 -0.01263988 0.072082996
		 0 0.020619983 -0.0051070605 0 0.022739315 -0.07909146 0 0.013688136 -0.058812838
		 0 -0.017627226 0.0027905463 0 -0.022739315;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "A341C2F3-4A2C-E6BA-40A5-BF8BE6E3D123";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[10:11]" "e[16]" "e[19]" "e[24]" "e[27]" "e[43]" "e[45]" "e[56]" "e[71]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.45963317155838013;
	setAttr ".re" 19;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak9";
	rename -uid "D4990AAF-45C5-2B38-79AF-D59BC050AA80";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk[64:73]" -type "float3"  -0.066765338 0 -0.0033678575
		 -0.061879396 0 0.0004428151 -0.01891895 0 -0.0042419494 -0.0039673871 0 -0.0067621241
		 0.016667977 0 -0.003111741 0.045008257 0 -0.0010385809 0.053169895 0 -0.00017551401
		 0.066765338 0 0.0067621246 0.066765338 0 0.0067621246 -0.00088770175 0 0.0028306998;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "8062C488-4596-C3F0-4E9B-FF9F2F8E88AA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 11 "e[12:13]" "e[15]" "e[17]" "e[42]" "e[50]" "e[65]" "e[74]" "e[128]" "e[138]" "e[146]" "e[156]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.45386642217636108;
	setAttr ".re" 156;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak10";
	rename -uid "4886C1F7-4B60-7A86-FAB7-1CAD48B2CF7B";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk[74:83]" -type "float3"  -0.30629462 0 -0.011920009
		 -0.17291239 0 0.021747643 -0.093174957 0 0.072103098 0.014222781 0 0.072145909 0.16172114
		 0 0.073585965 0.25860906 0 0.040901031 0.36429644 0 0.017844699 0.36429644 0 0.017844699
		 -0.057311632 0 -0.049841233 -0.36429644 0 -0.073585965;
createNode polySplitRing -n "polySplitRing8";
	rename -uid "61CAB89A-4FF4-336C-E9F7-95A4729FA90A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[112:113]" "e[115]" "e[117]" "e[119]" "e[121]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.53023618459701538;
	setAttr ".dr" no;
	setAttr ".re" 113;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak11";
	rename -uid "57A42910-4E64-7242-C3C0-199B3B7938CC";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk[84:95]" -type "float3"  -0.12993087 0 0.017735759
		 -0.10117069 0 0.045989573 0.01180313 0 0.057986937 0.094062567 0 0.055291295 0.12781593
		 0 0.029176043 0.12993087 0 -0.00048561956 0.12812409 0 -0.022516984 0.12027552 0
		 -0.046573587 0.037539478 0 -0.057986937 -0.032059919 0 -0.057328217 -0.079152741
		 0 -0.033307124 -0.11440794 0 -0.010582892;
createNode polySplitRing -n "polySplitRing9";
	rename -uid "AC7CFD7E-4AFD-D9D8-3270-E99130E36438";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[4:5]" "e[8:9]" "e[44]" "e[48]" "e[67]" "e[73]" "e[130]" "e[136]" "e[148]" "e[154]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.48495030403137207;
	setAttr ".re" 148;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak12";
	rename -uid "16110B17-43C5-3E3C-06BC-D2BCBDF2CFB8";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk[96:101]" -type "float3"  0.023674939 0 0.0070970324
		 -0.0016773571 0 0.0073138024 -0.025976807 0 0.0049064187 -0.01931647 0 -0.0049899854
		 0.00091652514 0 -0.0073138024 0.025976814 0 -0.0034138619;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "B0CD9168-4877-470A-4107-2889081DB04B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak13";
	rename -uid "EF88B05A-4C06-DF80-D0CF-DA8242730C8F";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk[102:113]" -type "float3"  0.061644316 0 0.011773137
		 0.047371905 0 0.025166154 0.0090509215 0 0.027298702 -0.043578513 0 0.024057483 -0.062214915
		 0 0.0079828538 -0.058573134 0 -0.0040629986 -0.042006217 0 -0.015216253 -0.018487858
		 0 -0.027298702 0.018578012 0 -0.025372086 0.045566723 0 -0.01786888 0.054847628 0
		 -0.0074685137 0.062214915 0 0.0016019852;
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "1D13D1C1-4166-5F4D-A2CE-00AC0F2E1771";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[1]" "e[5]" "e[7]" "e[15]" "e[45]" "e[66]" "e[133]" "e[151]" "e[181]" "e[213]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".a" 0;
createNode polySoftEdge -n "polySoftEdge3";
	rename -uid "1D101594-4332-FF73-2168-379BC65331FE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[8]" "e[12]" "e[20]" "e[169]" "e[201]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".a" 0;
createNode polySplitRing -n "polySplitRing10";
	rename -uid "59119334-47CA-A48C-5F64-9CA1420E56DF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 12 "e[88]" "e[90]" "e[92]" "e[94]" "e[96]" "e[103]" "e[106]" "e[110]" "e[114]" "e[120]" "e[194]" "e[199]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.50914132595062256;
	setAttr ".dr" no;
	setAttr ".re" 103;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing11";
	rename -uid "2A291E3E-4922-D4B1-E057-B7B2EE978E8B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 19 "e[20:21]" "e[23]" "e[25]" "e[32]" "e[35]" "e[40]" "e[51]" "e[54]" "e[79]" "e[82]" "e[87]" "e[91]" "e[97]" "e[126]" "e[140]" "e[158]" "e[163]" "e[234]" "e[240]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.48120367527008057;
	setAttr ".re" 40;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak14";
	rename -uid "ED72727B-4972-0385-A212-1DAB4971AC4A";
	setAttr ".uopa" yes;
	setAttr -s 22 ".tk";
	setAttr ".tk[11]" -type "float3" 0.59004766 0 0 ;
	setAttr ".tk[16]" -type "float3" 0 0.061524536 0 ;
	setAttr ".tk[17]" -type "float3" 0 0.1301046 0.068159737 ;
	setAttr ".tk[18]" -type "float3" 0 -0.050987057 0 ;
	setAttr ".tk[19]" -type "float3" 0 -0.028735636 0 ;
	setAttr ".tk[34]" -type "float3" -0.20630741 0 0 ;
	setAttr ".tk[42]" -type "float3" 0 -0.026398685 0 ;
	setAttr ".tk[50]" -type "float3" 0 -0.050987057 0 ;
	setAttr ".tk[52]" -type "float3" 0 -0.099960253 0 ;
	setAttr ".tk[56]" -type "float3" -0.063704051 -0.039420202 0 ;
	setAttr ".tk[114]" -type "float3" -0.0016829676 0.01490738 0 ;
	setAttr ".tk[115]" -type "float3" 0.023374133 0.011733005 0 ;
	setAttr ".tk[116]" -type "float3" 0.035705894 0.0061065322 0 ;
	setAttr ".tk[117]" -type "float3" 0.046948425 -0.00024423026 0 ;
	setAttr ".tk[118]" -type "float3" 0.05770221 -0.00908936 0 ;
	setAttr ".tk[119]" -type "float3" 0.079314157 -0.014907377 0 ;
	setAttr ".tk[120]" -type "float3" -0.0022422445 -0.01490738 0 ;
	setAttr ".tk[121]" -type "float3" -0.079314157 -0.038327228 0 ;
	setAttr ".tk[122]" -type "float3" -0.055277564 -0.00908936 0 ;
	setAttr ".tk[123]" -type "float3" -0.045433037 0.00070507522 0 ;
	setAttr ".tk[124]" -type "float3" -0.035141088 0.0077374382 0 ;
	setAttr ".tk[125]" -type "float3" -0.023852006 0.013967771 0 ;
createNode polyMapDel -n "polyMapDel1";
	rename -uid "C1844931-49BB-9ED4-A28C-C5A9E30CD154";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyTweak -n "polyTweak15";
	rename -uid "5C132649-41D6-3487-D6F9-EB9D0D222F7D";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk[126:145]" -type "float3"  -0.048357435 0 -0.012573887
		 -0.048619483 0 -0.026875746 -0.034457013 0 -0.037162948 -0.00070693 0 -0.036371104
		 0.045644905 0 -0.028167194 0.055863645 0 -0.017666802 0.049561933 0 -0.0062192027
		 0.036310896 0 0.002667889 0.018832525 0 0.011916221 0.015050975 0 0.01811434 0.014809076
		 0 0.023869164 0.013258127 0 0.029237837 0.0086350348 0 0.034413736 -0.022152014 0
		 0.037162941 -0.055863645 0 0.033183265 -0.05219866 0 0.026935451 -0.045207769 0 0.02045493
		 -0.045452785 0 0.014090889 -0.043337189 0 0.0077438341 -0.050587837 0 -0.0028279042;
createNode polySplitRing -n "polySplitRing12";
	rename -uid "3DB8E0F0-4933-6398-589A-46983C2BC4BF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 11 "e[42]" "e[65]" "e[128]" "e[146]" "e[164:165]" "e[167]" "e[169]" "e[177]" "e[181]" "e[183]" "e[185]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.49330458045005798;
	setAttr ".re" 164;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing13";
	rename -uid "4C8BE79F-4515-83F7-D4FD-B6B149EE1463";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 11 "e[12:13]" "e[15]" "e[17]" "e[50]" "e[74]" "e[138]" "e[156]" "e[171]" "e[173]" "e[175]" "e[179]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.52495837211608887;
	setAttr ".dr" no;
	setAttr ".re" 138;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak16";
	rename -uid "F703BD2A-425E-566A-30AF-5DA4FC06E311";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk";
	setAttr ".tk[27]" -type "float3" 0 0 3.7252903e-009 ;
	setAttr ".tk[71]" -type "float3" 0 0 3.7252903e-009 ;
	setAttr ".tk[146]" -type "float3" -0.083776668 0 0.011625983 ;
	setAttr ".tk[147]" -type "float3" -0.066449255 0 0.029074516 ;
	setAttr ".tk[148]" -type "float3" 0.0050214538 0 0.036684319 ;
	setAttr ".tk[149]" -type "float3" 0.057061303 -8.8817842e-016 0.03495907 ;
	setAttr ".tk[150]" -type "float3" 0.079281874 -8.8817842e-016 0.018863462 ;
	setAttr ".tk[151]" -type "float3" 0.080877855 -8.8817842e-016 -0.00032711762 ;
	setAttr ".tk[152]" -type "float3" 0.083776668 0 -0.014330342 ;
	setAttr ".tk[153]" -type "float3" 0.069946721 0 -0.029483786 ;
	setAttr ".tk[154]" -type "float3" 0.025705181 0 -0.036684319 ;
	setAttr ".tk[155]" -type "float3" -0.022727661 0 -0.036287494 ;
	setAttr ".tk[156]" -type "float3" -0.051788848 0 -0.02115652 ;
	setAttr ".tk[157]" -type "float3" -0.073698401 0 -0.0067149648 ;
createNode polySplitRing -n "polySplitRing14";
	rename -uid "4BA11DBB-4C87-D6F8-E1E1-CEA27559B259";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 11 "e[4:5]" "e[44]" "e[67]" "e[130]" "e[148]" "e[201]" "e[203]" "e[205]" "e[207]" "e[209]" "e[211]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.49225500226020813;
	setAttr ".re" 209;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak17";
	rename -uid "8437D9A5-4D6A-9D70-3E56-15BFA8D34898";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk[158:169]" -type "float3"  -0.054425426 -2.220446e-016
		 -0.022930492 -0.07922636 -2.220446e-016 -0.0072937631 -0.089674801 -2.220446e-016
		 0.011816571 -0.068846554 -2.220446e-016 0.031812403 0.0092475032 -2.220446e-016 0.040083934
		 0.06611 -2.220446e-016 0.038242295 0.08849474 -2.220446e-016 0.019724758 0.089674801
		 -2.220446e-016 -0.00031394372 0.08400932 -2.220446e-016 -0.015471721 0.074513048
		 -2.220446e-016 -0.03217262 0.02703796 -2.220446e-016 -0.040083934 -0.021073181 -2.220446e-016
		 -0.039606847;
createNode polySplitRing -n "polySplitRing15";
	rename -uid "414D1CBC-4B98-249F-2CE1-64AB0241B9F7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 11 "e[8:9]" "e[48]" "e[73]" "e[136]" "e[154]" "e[200]" "e[213]" "e[215]" "e[217]" "e[219]" "e[221]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.58843791484832764;
	setAttr ".dr" no;
	setAttr ".re" 154;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak18";
	rename -uid "F964567A-4164-4712-553F-7B806504A94E";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk[170:181]" -type "float3"  0.027744615 -2.8310687e-015
		 0.0023086574 0.030453743 -2.8310687e-015 -0.0038145205 0.022242093 -2.8310687e-015
		 -0.011199253 -0.0039759763 -2.8310687e-015 -0.013470122 -0.023066102 -2.8310687e-015
		 -0.012665834 -0.03017479 -2.8310687e-015 -0.0061391257 -0.030453743 -2.8310687e-015
		 -0.00024092173 -0.026709192 -2.8310687e-015 0.0045847148 -0.021906493 -2.8310687e-015
		 0.010025673 -0.0094201788 -2.8310687e-015 0.013153103 0.007728348 -2.8310687e-015
		 0.013470122 0.019344553 -2.8310687e-015 0.007672831;
createNode polySplitRing -n "polySplitRing16";
	rename -uid "C3F09A80-4756-72CF-FABC-799CFB2F02C7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[99:100]" "e[102]" "e[104]" "e[107]" "e[109]" "e[232]" "e[242]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.44337442517280579;
	setAttr ".re" 100;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak19";
	rename -uid "57059A64-42CC-D0BC-72F7-CB920E70FBAE";
	setAttr ".uopa" yes;
	setAttr -s 12 ".tk[182:193]" -type "float3"  -0.031672243 -4.1633363e-016
		 0.0041732695 -0.021114113 -4.3021142e-016 0.012925185 0.0051418538 -4.7184479e-016
		 0.013770666 0.024259567 -4.9960036e-016 0.012403355 0.031381372 -4.9960036e-016 0.0055510723
		 0.031672243 -4.7184479e-016 0.0014542248 0.028084351 -4.4755866e-016 -0.0027099079
		 0.023665048 -4.3021142e-016 -0.0075929454 0.0090739392 -3.8857806e-016 -0.012053824
		 -0.01096526 -3.3306691e-016 -0.013770666 -0.022815511 -3.8857806e-016 -0.0074822181
		 -0.030907603 -3.8857806e-016 -0.0016923369;
createNode polySplitRing -n "polySplitRing17";
	rename -uid "B83CF591-4B2D-CA29-0A54-B28B149A04BC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 42 "e[38]" "e[52:53]" "e[57]" "e[59]" "e[61:62]" "e[64]" "e[66]" "e[68]" "e[70]" "e[72]" "e[84]" "e[86]" "e[89]" "e[93]" "e[101]" "e[105]" "e[116]" "e[123]" "e[134]" "e[142]" "e[152]" "e[160]" "e[168]" "e[182]" "e[190]" "e[198]" "e[206]" "e[216]" "e[226]" "e[236]" "e[256]" "e[274]" "e[292]" "e[306]" "e[320]" "e[334]" "e[342]" "e[356]" "e[364]" "e[378]" "e[390]" "e[399]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".wt" 0.50698292255401611;
	setAttr ".dr" no;
	setAttr ".re" 62;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak20";
	rename -uid "CF4F3EEE-499A-AFDA-8F36-6F85D61D7C16";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk";
	setAttr ".tk[33]" -type "float3" 0 0.038930569 -0.060668945 ;
	setAttr ".tk[46]" -type "float3" 0 0.030823728 -0.059862476 ;
	setAttr ".tk[194]" -type "float3" 0.037147619 0 -0.0064825253 ;
	setAttr ".tk[195]" -type "float3" 0.037227392 0 0.0016333264 ;
	setAttr ".tk[196]" -type "float3" 0.033852406 0 0.0094577698 ;
	setAttr ".tk[197]" -type "float3" -0.0024411457 0 0.010804793 ;
	setAttr ".tk[198]" -type "float3" -0.037227459 0 0.0061355596 ;
	setAttr ".tk[199]" -type "float3" -0.034237579 0 -0.0012314153 ;
	setAttr ".tk[200]" -type "float3" -0.02769275 0 -0.0088727865 ;
	setAttr ".tk[201]" -type "float3" 0.0012721827 0 -0.010804793 ;
createNode polyTweak -n "polyTweak21";
	rename -uid "A1E1B27A-4A60-2004-F2E5-00B1F6820241";
	setAttr ".uopa" yes;
	setAttr -s 44 ".tk[202:245]" -type "float3"  5.8207661e-011 -0.0097850319
		 4.6566129e-010 -3.6379927e-012 -0.0057704169 4.6566129e-010 -2.7755576e-017 -0.0018619006
		 -1.1641532e-010 -2.9103858e-011 0.0012660341 4.6566129e-010 -8.7311547e-011 0.0047226478
		 -3.4924597e-010 1.1641527e-010 0.0069042942 -9.3132257e-010 -5.5511151e-017 0.0090193842
		 -1.3096724e-010 5.8207605e-011 0.010897515 4.3655746e-011 -1.1641538e-010 0.013582807
		 2.910383e-011 4.6566118e-010 0.014993878 9.3132257e-010 2.3283042e-010 0.016303856
		 1.8626451e-009 -1.110223e-016 0.01808333 -1.3969839e-009 -2.3283075e-010 0.019596944
		 3.7252903e-009 2.3283053e-010 0.01508892 0 -2.3283075e-010 0.011935942 4.6566129e-010
		 -2.220446e-016 0.0083851619 9.3132257e-010 -4.6566151e-010 0.0047226478 6.0535967e-009
		 -4.6566151e-010 0.00077845284 5.5879354e-009 -9.313228e-010 -0.0027906997 9.3132257e-010
		 9.3132235e-010 -0.0072505404 0 -2.220446e-016 -0.011831446 -8.3819032e-009 -9.313228e-010
		 -0.015860155 0 -4.6566151e-010 -0.019596945 -9.3132257e-010 -2.220446e-016 -0.019596945
		 3.7252903e-009 -2.220446e-016 -0.019596945 0 2.3283053e-010 -0.019596945 2.8421709e-014
		 2.9103803e-011 -0.019596945 4.6566129e-010 2.7755576e-017 -0.01869351 0 2.7755576e-017
		 -0.018402003 -3.7252903e-009 -1.1641527e-010 -0.01870029 -6.519258e-009 -2.3283053e-010
		 -0.018402003 0 2.3283075e-010 -0.016651724 0.017522687 4.6566151e-010 -0.014764711
		 0.017522672 -9.3132235e-010 -0.012224753 0.017522689 4.6566151e-010 -0.0090360139
		 0.017522696 4.6566151e-010 -0.004922837 0.017522689 2.220446e-016 -0.0012787577 0.017522698
		 4.6566151e-010 -0.00050252385 -7.9162419e-009 9.313228e-010 -0.0012255691 -0.012990761
		 2.220446e-016 -0.0038967333 -0.012990767 -4.6566107e-010 -0.0080248043 -0.012990762
		 -2.3283053e-010 -0.011225091 -0.012990762 -2.3283053e-010 -0.013470167 -0.012990774
		 1.110223e-016 -0.013400761 3.259629e-009;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "674DD9C9-4DD8-B98E-0EC9-74B163DAD641";
	setAttr ".dc" -type "componentList" 11 "f[3]" "f[16]" "f[18]" "f[27:29]" "f[40]" "f[42]" "f[46:47]" "f[70:71]" "f[79:80]" "f[117:118]" "f[222:229]";
createNode polyPlanarProj -n "polyPlanarProj1";
	rename -uid "FE71D6B7-49E3-7CB1-625A-E7B0E61E5015";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "f[1]" "f[19]" "f[28:29]" "f[55:56]" "f[62:63]" "f[192:195]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0.28476572036743164 8.4045114517211914 0.59596759080886841 ;
	setAttr ".ro" -type "double3" -42.887724257543553 15.604140818614674 -14.028317555208339 ;
	setAttr ".ps" -type "double2" 3.3613415920009833 3.358658620004396 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "C3B435F5-40E7-93FB-94FA-2CA1A64FBF87";
	setAttr ".uopa" yes;
	setAttr -s 20 ".uvtk[0:19]" -type "float2" -0.0082561728 -0.12710634
		 -0.14634044 -0.057084311 -0.15485962 -0.15939675 -0.046040066 -0.21868168 -0.031398296
		 -0.0029282812 -0.13866757 0.08998654 -0.14898667 0.019632904 -0.015769243 -0.062986374
		 -0.21598813 0.13559425 -0.24183539 0.063157327 -0.23927948 -0.015790982 -0.22476408
		 -0.12037616 -0.04802037 0.065663151 -0.12099589 0.16756354 -0.17682095 0.21480937
		 -0.29558191 0.19420037 -0.23509702 0.27366692 -0.33691427 0.12098289 -0.33496889
		 0.041556727 -0.29849792 -0.063799471;
createNode polyPlanarProj -n "polyPlanarProj2";
	rename -uid "CF586D22-46C1-B755-534E-09BA9BEEB5EA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 13 "f[2]" "f[5]" "f[9]" "f[23]" "f[30:31]" "f[69:70]" "f[87:88]" "f[110:111]" "f[129:130]" "f[143:144]" "f[154:155]" "f[165:166]" "f[196:205]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0.060466408729553223 3.2465572357177734 -3.3192017078399658 ;
	setAttr ".ro" -type "double3" 16.83235895814008 -13.887371450051171 -4.1530984392046353 ;
	setAttr ".ps" -type "double2" 14.194286032578086 14.482696533203125 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "9DFD88A8-45E8-CAEE-6FF3-069D4C74F6D4";
	setAttr ".uopa" yes;
	setAttr -s 64 ".uvtk[0:63]" -type "float2" 0.00019618095 0.0005243948
		 -0.00059463148 0.00092541374 -0.00064340065 0.00033952395 -2.0209747e-005 2.8080649e-009
		 6.3643049e-005 0.0012355732 -0.00055067148 0.001767684 -0.00060978183 0.0013647417
		 0.00015315258 0.00089159678 -0.00099351304 0.0020288671 -0.0011415454 0.0016140335
		 -0.0011269046 0.0011619033 -0.001043768 0.00056298269 -3.1549676e-005 0.0016283689
		 -0.00044948171 0.0022119624 -0.00076917477 0.0024825279 -0.0014493529 0.0023645086
		 -0.0011029538 0.0028196122 -0.0016860683 0.0019451994 -0.0016749246 0.0014903258
		 -0.0014660376 0.00088693015 0.30580291 -0.6341148 0.20916384 -0.60622537 0.21210277
		 -0.52735567 0.3277778 -0.54251111 0.34124643 -0.44975886 0.21156082 -0.44784835 0.20710427
		 -0.38680249 0.340579 -0.40063763 0.4790529 -0.092732087 0.2511836 -0.071146652 0.26626644
		 0.017382126 0.5038355 0.024720892 0.078069016 -0.06138251 0.084849186 0.0072095525
		 0.13771811 -0.59453362 0.12576045 -0.52447009 0.11407094 -0.45388827 0.10663596 -0.38331911
		 0.21619833 -0.25255862 0.21230961 -0.32641527 0.097238675 -0.31485245 0.086584002
		 -0.23073255 0.36501524 -0.35127744 0.38802019 -0.29112583 0.2871829 -0.82641566 0.21427299
		 -0.7706387 0.20930843 -0.67481965 0.29345721 -0.7140944 0.14777493 -0.65544242 0.16201255
		 -0.74034297 0.43499246 -0.2245653 0.22815217 -0.17168535 0.071999803 -0.13984452
		 0.086797662 -0.62627792 0.10971231 -0.69747752 0.06733942 -0.57499969 0.041163426
		 -0.51592821 0.018910265 -0.45653135 0.0088761877 -0.37939909 -0.014451566 -0.30551699
		 -0.038912475 -0.21446717 -0.078957938 -0.1169736 -0.089079574 -0.06273587 -0.090091385
		 -0.016087295;
createNode polyPlanarProj -n "polyPlanarProj3";
	rename -uid "9DBB81BC-43A0-C0F7-E7E6-BD81635808DA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 15 "f[3]" "f[6]" "f[10]" "f[20:22]" "f[57:59]" "f[64:66]" "f[68]" "f[77:79]" "f[89:92]" "f[112:115]" "f[128]" "f[137:142]" "f[151:153]" "f[162:164]" "f[173:175]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 2.5659870803356171 2.1466531753540039 0.33450102806091309 ;
	setAttr ".ro" -type "double3" 0 90 0 ;
	setAttr ".ps" -type "double2" 11.735043644459022 12.028913497924805 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj4";
	rename -uid "98FE5D88-4771-D38D-DDA2-F6AA698F20ED";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "f[0]" "f[7]" "f[26:27]" "f[75:76]" "f[93:94]" "f[135:136]" "f[149:150]" "f[160:161]" "f[171:172]" "f[184:191]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.9533960223197937 3.7584471702575684 2.6577032804489136 ;
	setAttr ".ro" -type "double3" -19.122578232231586 -9.2085643243948336 3.1758315544960678 ;
	setAttr ".ps" -type "double2" 8.5420519222521865 9.0242471694946289 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "D3267A36-416D-51D1-6D2C-F9BEE84A59F0";
	setAttr ".uopa" yes;
	setAttr -s 155 ".uvtk[0:154]" -type "float2" 0.26601833 -0.2382257 0.06308388
		 -0.13531834 0.050563727 -0.28568098 0.21048954 -0.37280858 0.2320077 -0.055728313
		 0.074360251 0.080823198 0.059194852 -0.022571487 0.25497681 -0.14399222 -0.039273329
		 0.14785017 -0.077259503 0.041393854 -0.073503293 -0.074631974 -0.052170794 -0.22833462
		 0.20757918 0.045076597 0.10033132 0.19483359 0.018288398 0.2642681 -0.15624771 0.23398022
		 -0.067356549 0.35076758 -0.21699144 0.12637672 -0.21413243 0.0096486593 -0.16053319
		 -0.14518711 -0.47922885 0.46102628 -0.52500224 0.44152325 -0.52361017 0.38636997
		 -0.46882045 0.39696798 -0.46244082 0.33210683 -0.52386683 0.33077076 -0.52597767
		 0.28808165 -0.4627569 0.29775643 -0.39716837 0.082439333 -0.5050993 0.06734474 -0.49795538
		 0.0054368903 -0.38543013 0.00030492249 -0.58709562 0.060516696 -0.58388424 0.012550534
		 -0.55884272 0.43334725 -0.56450635 0.38435203 -0.57004333 0.33499444 -0.57356495
		 0.28564575 -0.52167022 0.19420543 -0.52351224 0.24585314 -0.57801592 0.23776726 -0.58306265
		 0.17894252 -0.45118266 0.26323915 -0.44028628 0.22117528 -0.48804834 0.59550136 -0.52258229
		 0.55649674 -0.52493358 0.48949099 -0.4850764 0.51695561 -0.55407923 0.47594056 -0.54733557
		 0.53531122 -0.4180378 0.17462979 -0.51600832 0.13765106 -0.58997035 0.1153849 -0.58296126
		 0.45554578 -0.57210779 0.50533545 -0.59217781 0.41968712 -0.60457611 0.37837875 -0.61511636
		 0.33684281 -0.61986893 0.28290454 -0.63091832 0.23123896 -0.64250422 0.16756821 -0.66147178
		 0.099391349 -0.66626602 0.061463103 -0.66674554 0.028841935 0.077656351 -0.23872165
		 0.040859822 -0.2418462 0.051634554 -0.269014 0.084671631 -0.26286024 0.032493249
		 -0.21382353 0.071356043 -0.21382353 0.069626622 -0.17683473 0.03115516 -0.17471112
		 -0.0048846668 -0.022865858 0.051256053 -0.027154796 0.053389423 0.0019920617 -0.0014832219
		 0.0019920617 0.13489537 -0.23234409 0.10816688 -0.23504826 0.10942857 -0.25562552
		 0.13235103 -0.2502996 0.10639445 -0.17933136 0.10629905 -0.21382353 0.13563745 -0.21382353
		 0.13647914 -0.18116929 0.11321206 0.0019920617 0.11246098 -0.032197107 0.15997614
		 -0.035909075 0.15965396 0.0019920617 0.16378209 -0.22943115 0.15718324 -0.24456269
		 0.16856401 -0.18314904 0.16728403 -0.21382353 0.20942269 -0.039907474 0.20798369
		 0.0019920617 0.022626273 -0.1393178 0.065343447 -0.14336312 0.060850449 -0.10153858
		 0.014243952 -0.095092013 0.14841518 -0.11469671 0.14251563 -0.15162003 0.17883332
		 -0.15539128 0.18883123 -0.12070657 0.10984059 -0.10911745 0.1081586 -0.14811899 0.09017773
		 -0.28429449 0.059096649 -0.29313824 0.065973185 -0.3276301 0.096825719 -0.31494048
		 0.11100853 -0.27389744 0.11376412 -0.30002204 0.13157046 -0.26624355 0.13264975 -0.28903961
		 0.1539029 -0.25799894 0.15325333 -0.27720973 -0.0014832219 -0.049665719 0.053389423
		 -0.0585787 0.11321206 -0.069057211 0.15965396 -0.07677108 0.20798369 -0.085080273
		 0.37201178 0.16587043 0.30908373 0.18782334 0.30271983 0.1356788 0.35862055 0.10285117
		 0.31464109 0.23940931 0.38168529 0.22919622 0.41003507 0.29259843 0.31936288 0.30297336
		 0.27174473 0.23736547 0.27570227 0.30219656 0.26869625 0.19375236 0.26670808 0.14931145
		 0.32615453 0.36677834 0.44904909 0.3550165 0.46217093 0.43144175 0.34502685 0.44455138
		 0.27658561 0.36711016 0.28413385 0.44628 0.29212472 0.029816618 0.29785532 0.091117837
		 0.26420224 0.11159929 0.2589708 0.060264431 0.34990421 0.048248202 0.34312853 -0.028302763
		 0.48905751 0.51486135 0.36708412 0.53055668 0.28896716 0.53368652 0.22466595 0.44382888
		 0.21257439 0.52972317 0.22830066 0.36609954 0.23329984 0.30234772 0.23023656 0.23875977
		 0.22972317 0.20446211 0.23208258 0.16902713 0.23195262 0.13932009 0.22732422 0.09959995;
createNode deleteComponent -n "deleteComponent2";
	rename -uid "9C1BDDA1-46A0-F82F-B0A6-CEB3ADCBACAB";
	setAttr ".dc" -type "componentList" 16 "f[4]" "f[8]" "f[12]" "f[16:18]" "f[52:54]" "f[60:61]" "f[67]" "f[71:74]" "f[86]" "f[95:97]" "f[108:109]" "f[126:127]" "f[131:134]" "f[145:148]" "f[156:159]" "f[167:170]";
createNode deleteComponent -n "deleteComponent3";
	rename -uid "EF61650F-4AB3-AF98-47B6-B494D026683F";
	setAttr ".dc" -type "componentList" 5 "f[11]" "f[27]" "f[32]" "f[83]" "f[100:103]";
createNode deleteComponent -n "deleteComponent4";
	rename -uid "41975342-4F95-2C2D-1AE1-F587706AB5D4";
	setAttr ".dc" -type "componentList" 5 "f[35]" "f[40]" "f[63]" "f[80:82]" "f[132:133]";
createNode polyPlanarProj -n "polyPlanarProj5";
	rename -uid "557A83D9-4867-7B5C-5695-A9B8AA75A253";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "f[9]" "f[17]" "f[31:32]" "f[34:35]" "f[37:40]" "f[59:62]" "f[88:89]" "f[124:127]" "f[150:155]" "f[158:161]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -2.281280905008316 -0.62515854835510254 7.1234619617462158 ;
	setAttr ".ps" -type "double2" 5.9930311024756167 6.3667810634650364 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV4";
	rename -uid "86EDF52E-4813-72F8-5156-93AF2E9FEFDF";
	setAttr ".uopa" yes;
	setAttr -s 203 ".uvtk[0:202]" -type "float2" 0.38563609 0.22981805 0.3863115
		 0.22947551 0.38635314 0.22997595 0.38582093 0.23026597 0.38574931 0.22921066 0.38627398
		 0.22875617 0.38632447 0.22910029 0.38567287 0.22950442 0.38665217 0.22853309 0.38677859
		 0.22888739 0.38676608 0.22927354 0.38669512 0.22978511 0.38583064 0.22887516 0.38618755
		 0.22837672 0.3864606 0.2281456 0.38704148 0.22824644 0.38674566 0.22785777 0.38724366
		 0.22860457 0.38723415 0.22899306 0.38705575 0.2295084 0.0023171159 0.0053361924 0.0017687599
		 0.0051025534 0.0017854336 0.0044418545 0.0024417893 0.0045688068 0.0025182045 0.0037918328
		 0.0017823683 0.003775849 0.0017570793 0.0032644439 0.0025144205 0.0033803482 0.0033001308
		 0.00080103974 0.0020072071 0.00062020757 0.0020927833 -0.00012139724 0.0034407456
		 -0.00018287401 0.001024932 0.00053841021 0.0010634108 -3.6182042e-005 0.0013633837
		 0.0050046137 0.0012955372 0.0044176881 0.0012292148 0.0038264378 0.0011870203 0.0032352761
		 0.0018086752 0.0021398822 0.0017866171 0.0027585896 0.0011337029 0.0026617178 0.0010732448
		 0.001957048 0.0026530749 0.0029668533 0.0027836156 0.0024629587 0.0022114613 0.0069471011
		 0.0017977566 0.0064798351 0.0017695894 0.0056771818 0.0022470602 0.0060061812 0.0014204421
		 0.005514835 0.0015012315 0.0062260651 0.0030501196 0.001905387 0.0018765097 0.0014624215
		 0.00099049974 0.0011956992 0.0010744616 0.0052705244 0.0012044806 0.005866996 0.00096405757
		 0.0048409784 0.00081553234 0.0043461332 0.00068926788 0.0038485525 0.00063233537
		 0.0032024218 0.00049997459 0.0025835265 0.00036118511 0.0018207977 0.00013397183
		 0.0010041028 7.6540266e-005 0.00054975139 7.0796021e-005 0.00015897489 0.0052813306
		 0.0041874619 0.005949846 0.0042442568 0.0057541006 0.0047378121 0.0051538763 0.0046260268
		 0.006101802 0.0037351679 0.0053957971 0.0037351679 0.0054272288 0.0030631653 0.006126137
		 0.0030245727 0.0067808777 0.00026601506 0.0057609677 0.00034393201 0.005722207 -0.00018558104
		 0.0067190691 -0.00018558104 0.0042414735 0.0040716389 0.0047270777 0.0041207587 0.0047041378
		 0.0044945665 0.0042877039 0.0043978076 0.0047592549 0.0031085187 0.0047610095 0.0037351679
		 0.0042279954 0.0037351679 0.004212725 0.0031419005 0.0046354225 -0.00018558104 0.0046490715
		 0.0004355321 0.0037858216 0.00050296821 0.0037917059 -0.00018558104 0.0037167158
		 0.0040187077 0.0038365573 0.0042936159 0.0036298232 0.0031778784 0.0036531019 0.0037351679
		 0.0028875375 0.00057560741 0.0029136799 -0.00018558104 0.0062810839 0.0023815862
		 0.0055050566 0.0024550899 0.005586681 0.0016952489 0.0064333645 0.0015781454 0.0039958563
		 0.0019343033 0.0041030324 0.0026050939 0.0034432851 0.0026736057 0.0032616104 0.0020434763
		 0.0046966304 0.0018329426 0.0047272309 0.0025414771 0.0050538485 0.0050154296 0.0056184963
		 0.0051760748 0.0054935813 0.0058027031 0.0049331044 0.005572136 0.0046754573 0.0048265122
		 0.0046253549 0.0053011538 0.0043018931 0.004687475 0.0042823055 0.005101623 0.0038961929
		 0.0045376997 0.0039079948 0.0048866929 0.0067190691 0.0007528842 0.005722207 0.00091480499
		 0.0046354225 0.001105172 0.0037917059 0.0012453062 0.0029136799 0.0013962524 0.034224231
		 -0.09448792 0.034714103 -0.094658852 0.03476372 -0.094252877 0.034328423 -0.0939973
		 0.034670874 -0.095060505 0.034148831 -0.094980992 0.03392813 -0.095474645 0.034634132
		 -0.095555417 0.035004873 -0.095044583 0.034974009 -0.095549352 0.03502861 -0.094704986
		 0.035044089 -0.09435901 0.034581263 -0.096052185 0.033624392 -0.095960632 0.03352223
		 -0.096555673 0.034434229 -0.096657723 0.034967124 -0.096054778 0.034908433 -0.096671201
		 0.034846175 -0.093428627 0.034801591 -0.093905874 0.035063609 -0.094065405 0.035104334
		 -0.093665712 0.034396287 -0.093572147 0.034449078 -0.092976086 0.033312824 -0.097205207
		 0.034262542 -0.097327366 0.034870736 -0.097351737 0.035371456 -0.096652113 0.035465505
		 -0.09732087 0.035343103 -0.096046925 0.035304207 -0.095550522 0.035327993 -0.095055461
		 0.035332073 -0.094788387 0.035313655 -0.094512515 0.035314713 -0.094281234 0.03535071
		 -0.093971908 -0.22843251 0.53860563 0.22217271 0.70618081 0.26098487 0.83284211 -0.2652308
		 0.62535286 -0.44495925 0.48441967 -0.4981162 0.56763107 -0.75181258 -0.011575667
		 -0.56042713 -0.08870668 -0.55971962 -0.19074243 -0.74147505 -0.12191848 -0.20280233
		 0.44034296 -0.3917979 0.39330769 -0.33927214 0.26424772 -0.16025445 0.3103033 -0.25564426
		 0.037814058 -0.27027261 -0.099319555 0.14491455 0.55767399 0.17065048 0.41970339
		 -0.1085179 0.14648075 -0.26234955 0.1059272 -0.15985869 -0.097189188 -0.042255092
		 -0.064994358 0.17734663 0.23784767 0.17844084 0.00072564767 -0.55790991 -0.3561509
		 -0.30869469 -0.27148128 -0.36415628 -0.49355397 -0.55527836 -0.56951231 -0.71440226
		 -0.27920893 -0.67529184 -0.48209876 -0.062627055 -0.2749708 0.015485608 -0.25267699
		 0.16446523 -0.21439601 -0.40453398 -0.65229279 -0.55239183 -0.66216385 -0.63239205
		 -0.61338335 -0.64424157 0.39829767 -0.71212077 0.47455722 -0.56558543 0.31792766
		 -0.50507754 0.1949196 -0.40582243 0.048350602 -0.27088788 -0.13837975 -0.13772164
		 -0.29921782 -0.7919991 -0.40450373 -0.71018839 -0.56827748 -0.86658365 -0.21964754
		 -0.91822368 -0.076337658 -0.93792576 0.037814058;
createNode polyPlanarProj -n "polyPlanarProj6";
	rename -uid "AF0C7459-434F-CE48-721F-1793D91708E7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "f[10]" "f[18]" "f[25:26]" "f[28]" "f[30]" "f[72]" "f[77]" "f[156:157]" "f[162:163]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -1.1515483260154724 0.17458415031433105 5.4115086793899536 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 4.7551323175430298 4.5814372799362184 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV5";
	rename -uid "36E73FE8-4DE3-5449-7457-A6AABC13DBC4";
	setAttr ".uopa" yes;
	setAttr -s 227 ".uvtk[0:226]" -type "float2" -0.17529038 0.055918444 -0.17896944
		 0.057784118 -0.17919643 0.055058122 -0.17629708 0.053478528 -0.17590697 0.059227057
		 -0.17876503 0.061702669 -0.17903997 0.059828166 -0.17549053 0.057626817 -0.18082517
		 0.062917836 -0.18151382 0.060987826 -0.18144575 0.058884349 -0.18105897 0.056097738
		 -0.17634985 0.061054595 -0.17829418 0.063769624 -0.17978159 0.065028459 -0.18294585
		 0.064479344 -0.1813343 0.066596657 -0.18404709 0.062528566 -0.18399529 0.060412321
		 -0.18302356 0.057605229 -0.012334139 -0.029880553 -0.0093484912 -0.028608441 -0.0094392905
		 -0.025010966 -0.013013033 -0.025702246 -0.013429151 -0.021471575 -0.0094225509 -0.021384411
		 -0.0092848688 -0.018599952 -0.013408534 -0.019231001 -0.017686667 -0.0051866141 -0.010646682
		 -0.004202039 -0.011112653 -0.00016399508 -0.018452303 0.00017074586 -0.0052983533
		 -0.0037566668 -0.0055078245 -0.00062799378 -0.0071411994 -0.028075149 -0.0067717694
		 -0.02487937 -0.0064106123 -0.021659922 -0.0061809178 -0.018441081 -0.0095658312 -0.012476735
		 -0.0094456887 -0.015845524 -0.0058905892 -0.015318117 -0.0055614063 -0.011481171
		 -0.014163489 -0.01697957 -0.014874224 -0.014235889 -0.011758866 -0.038651921 -0.0095063364
		 -0.03610776 -0.0093529737 -0.031737216 -0.011952716 -0.033528656 -0.007451904 -0.030853363
		 -0.007891763 -0.034725927 -0.016325425 -0.011199866 -0.0099351332 -0.0087878834 -0.0051108454
		 -0.0073355348 -0.0055680238 -0.029523076 -0.0062759635 -0.032770675 -0.0049668574
		 -0.027184119 -0.0041581574 -0.024489725 -0.003470653 -0.02178048 -0.0031606539 -0.018262263
		 -0.0024399429 -0.014892297 -0.001684231 -0.010739274 -0.0004470415 -0.0062923292
		 -0.00013433013 -0.0038183965 -0.00010305367 -0.0016906272 -0.033506814 -0.023633331
		 -0.037147887 -0.023942523 -0.036081661 -0.026630826 -0.032812618 -0.026021879 -0.037975729
		 -0.021169625 -0.034130208 -0.021169625 -0.034301326 -0.017509539 -0.038108137 -0.017299404
		 -0.041674323 -0.0022741423 -0.036119156 -0.0026985391 -0.035908017 0.00018558104
		 -0.04133774 0.00018558104 -0.0278429 -0.023002276 -0.030487763 -0.023269856 -0.0303629
		 -0.025306014 -0.028094687 -0.024778983 -0.030663103 -0.017756589 -0.030672565 -0.021169625
		 -0.027769478 -0.021169625 -0.02768621 -0.017938452 -0.029988518 0.00018558104 -0.03006283
		 -0.0031974777 -0.025361136 -0.0035647859 -0.025393033 0.00018558104 -0.024984533
		 -0.022714015 -0.025637513 -0.024211315 -0.024511375 -0.018134367 -0.024638021 -0.021169625
		 -0.020468317 -0.0039604302 -0.020610722 0.00018558104 -0.038952097 -0.013797199 -0.034725178
		 -0.014197499 -0.035169788 -0.010058898 -0.039781541 -0.0094210021 -0.026505139 -0.011360911
		 -0.027088899 -0.015014523 -0.023495192 -0.015387695 -0.022505881 -0.011955601 -0.030322129
		 -0.010808843 -0.030488549 -0.014668084 -0.03226779 -0.028142815 -0.035343282 -0.029017944
		 -0.034662873 -0.032430951 -0.031609952 -0.031175271 -0.03020657 -0.027114032 -0.029933892
		 -0.029699095 -0.028171929 -0.02635666 -0.028065108 -0.028612349 -0.025962079 -0.025540845
		 -0.026026357 -0.027441761 -0.04133774 -0.0049260124 -0.035908017 -0.0058079716 -0.029988518
		 -0.0068448274 -0.025393033 -0.0076081306 -0.020610722 -0.0084303366 -0.044868451
		 -0.0055027171 -0.0475371 -0.004571727 -0.047807056 -0.0067830868 -0.045436334 -0.0081752408
		 -0.047301445 -0.0023840179 -0.044458218 -0.0028171258 -0.043255925 -0.00012834922
		 -0.04710127 0.00031161739 -0.049120639 -0.0024707341 -0.048952796 0.0002786612 -0.049249869
		 -0.0043202913 -0.049334243 -0.0062049441 -0.046813156 0.0030174679 -0.041601419 0.0025186741
		 -0.041044913 0.0057597323 -0.046012808 0.006315704 -0.048915371 0.0030315709 -0.048595253
		 0.0063890032 -0.048256312 -0.011272539 -0.048013289 -0.0086728409 -0.049440533 -0.0078042354
		 -0.04966237 -0.0099813379 -0.045805998 -0.010490933 -0.046093363 -0.013737223 -0.039904714
		 0.0092974501 -0.045077439 0.0099630244 -0.048390206 0.010095759 -0.051117193 0.006285069
		 -0.051629905 0.0099276695 -0.050963033 0.0029886914 -0.050751004 0.00028509353 -0.050880902
		 -0.0024115795 -0.050902709 -0.0038660862 -0.050802648 -0.0053688213 -0.05080818 -0.0066286591
		 -0.051004399 -0.0083131222 -0.016152354 -0.030624008 -0.015925551 -0.038032927 -0.014266484
		 -0.039224353 -0.014703693 -0.030514047 -0.015876589 -0.027193686 -0.014398909 -0.026827347
		 0.04856576 0.49572217 0.098613515 0.51813591 0.098798491 0.54778653 0.051269118 0.52778685
		 -0.017714208 -0.030514047 -0.017469695 -0.027521217 -0.019614037 -0.027652351 -0.019823693
		 -0.030494608 0.17831506 0.48137 0.17448969 0.52121991 -0.017714225 -0.036172219 -0.019856689
		 -0.03586806 -0.022471985 -0.030442879 -0.02230609 -0.027995637 -0.025778625 -0.028491661
		 -0.025888024 -0.030368418 -0.022546388 -0.035069399 -0.026015894 -0.033916518 0.099271834
		 0.59585285 0.16444217 0.57124859 0.14993884 0.63578117 0.099960059 0.65785396 0.058348734
		 0.57349426 0.068576083 0.63245225 -0.028855121 -0.029035708 -0.028914535 -0.030286761
		 -0.029089674 -0.032651905 0.13937999 0.68190932 0.10071492 0.68477768 0.079794653
		 0.67060256 -0.016152371 -0.023857884 -0.014703675 -0.023242133 -0.017714225 -0.024610773
		 -0.019809509 -0.024888337 -0.022439968 -0.02561585 -0.025833096 -0.026666565 -0.028839173
		 -0.027819121 0.038056895 0.60990387 0.059450809 0.6574949 0.018552927 0.55618614
		 0.0050489116 0.51454139 -0.00010306787 0.48137 0.28161129 -0.026739554 0.72053784
		 -0.095426708 0.66480422 0.18233642 0.33800435 0.088325992 0.092313454 0.086744443
		 0.015007154 -0.10768342 0.65831029 0.44067314 0.32356191 0.23087358 0.071886815 0.2451221
		 0.40663815 0.048600595 0.51122636 0.040086206 0.48723596 0.17487767 0.37934792 0.17374919
		 0.67165607 0.1497943 0.65925723 0.23887818 0.46283603 0.30449584 0.36013031 0.30127501
		 0.65290493 0.3262319 0.27443174 0.17265217 0.26875293 0.26570916 0.29862994 0.08092656
		 -0.14660951 0.085206419 -0.17968629 0.28302345 -0.24425299 -0.18639745;
createNode polyPlanarProj -n "polyPlanarProj7";
	rename -uid "2D49D8FB-4E34-9351-8B02-A1A63861640F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[10]" "f[18]" "f[163]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -1.004378616809845 -1.4938511848449707 4.2268856763839722 ;
	setAttr ".ro" -type "double3" -32.420551204711991 0 0 ;
	setAttr ".ps" -type "double2" 4.4607928991317749 4.4639296891665925 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV6";
	rename -uid "9AEFC533-46D6-F4BA-41AD-DA9390110F02";
	setAttr ".uopa" yes;
	setAttr -s 231 ".uvtk[0:230]" -type "float2" 0.13499661 -0.075081035 0.1354003
		 -0.075285748 0.13542518 -0.074986629 0.13510706 -0.074813321 0.13506424 -0.075444043
		 0.13537784 -0.075715639 0.135408 -0.075509958 0.13501856 -0.0752685 0.13560385 -0.075848959
		 0.13567944 -0.075637206 0.13567196 -0.075406425 0.13562953 -0.075100705 0.13511285
		 -0.075644523 0.13532619 -0.075942442 0.13548936 -0.076080568 0.13583654 -0.076020308
		 0.13565972 -0.076252595 0.13595739 -0.075806282 0.13595171 -0.075574078 0.13584508
		 -0.075266078 0.0013470761 0.0032988882 0.001019476 0.0031593379 0.0010294439 0.0027646194
		 0.0014215703 0.0028404542 0.0014672166 0.0023762423 0.0010276012 0.0023667039 0.0010125012
		 0.0020611575 0.0014649484 0.0021304076 0.0019343718 0.00058943423 0.0011619293 0.00048139971
		 0.0012130607 3.8333488e-005 0.0020183716 1.6047584e-006 0.0005750882 0.00043253094
		 0.00059807365 8.9244561e-005 0.00077729305 0.0031008329 0.00073674775 0.002750166
		 0.00069713034 0.0023969139 0.00067192706 0.0020437413 0.0010433284 0.0013893219 0.0010301397
		 0.0017589401 0.00064006611 0.0017010653 0.00060394872 0.0012800748 0.0015478052 0.0018833855
		 0.00162578 0.0015823299 0.0012839549 0.0042613158 0.0010367949 0.0039821584 0.0010199668
		 0.0035026283 0.0013052236 0.0036991674 0.0008113847 0.003405638 0.0008596413 0.0038305451
		 0.001785003 0.0012492102 0.0010838457 0.0009845756 0.00055451295 0.00082521199 0.00060468051
		 0.0032596935 0.00068235688 0.0036160036 0.00053871295 0.0030030694 0.00044997889
		 0.0027074262 0.00037454549 0.0024101632 0.00034053455 0.0020241269 0.00026145333
		 0.0016543488 0.00017853372 0.0011986684 4.278704e-005 0.00071075343 8.4752965e-006
		 0.00043930268 5.0435679e-006 0.00020583977 0.0061283172 0.0026135501 0.0065278225
		 0.0026474814 0.0064108563 0.0029424352 0.0060521332 0.0028756002 0.006618666 0.0023432332
		 0.0061967308 0.0023432332 0.0062155141 0.0019415912 0.0066332174 0.0019185327 0.0070244828
		 0.0002698944 0.0064149611 0.00031645776 0.0063918168 5.4961935e-010 0.006987548 5.4961935e-010
		 0.005506841 0.0025442981 0.0057970742 0.0025736729 0.0057833651 0.0027970518 0.0055344612
		 0.0027392646 0.0058163223 0.0019686802 0.0058173249 0.0023432332 0.005498786 0.0023432332
		 0.0054896474 0.0019886552 0.0057422938 5.4961935e-010 0.0057504131 0.00037120626
		 0.0052345213 0.00041151189 0.005238058 5.4961935e-010 0.0051932377 0.0025126853 0.0052648713
		 0.0026769703 0.0051413057 0.0020101487 0.005155215 0.0023432332 0.0046976609 0.0004549216
		 0.0047132741 5.4961935e-010 0.0067258109 0.0015342442 0.0062620169 0.0015781701 0.0063107805
		 0.0011240685 0.0068168347 0.0010540829 0.0053600785 0.001266935 0.0054241382 0.0016678243
		 0.0050298236 0.0017087526 0.0049212645 0.0013321858 0.0057788943 0.0012063546 0.005797151
		 0.0016298 0.0059923902 0.0031083398 0.0063298363 0.0032043459 0.0062551489 0.0035788468
		 0.0059201946 0.0034410632 0.005766198 0.0029954645 0.0057362677 0.003279096 0.0055429428
		 0.0029123388 0.0055312212 0.0031598443 0.0053004995 0.002822832 0.005307558 0.0030314045
		 0.006987548 0.00056087342 0.0063918168 0.00065764715 0.0057422938 0.00077140727 0.005238058
		 0.00085516431 0.0047132741 0.00094538694 0.00061115262 0.0077554169 0.00090393482
		 0.0076532923 0.00093354093 0.007895899 0.00067343551 0.0080486331 0.00087811454 0.0074132378
		 0.00056611467 0.0074607506 0.00043425921 0.0071657812 0.00085610495 0.0071174819
		 0.0010777145 0.0074227559 0.0010592949 0.007121102 0.0010918941 0.0076256674 0.0011011115
		 0.0078324536 0.00082448765 0.0068206168 0.00025271988 0.0068753352 0.00019164271
		 0.0065197162 0.00073671091 0.006458743 0.0010551786 0.006819061 0.0010200378 0.006450715
		 0.0009828856 0.0083884206 0.00095617492 0.008103258 0.0011127549 0.0080079427 0.0011371104
		 0.0082467292 0.00071399682 0.0083026662 0.00074551359 0.0086588264 6.6558277e-005
		 0.0061315768 0.0006340695 0.0060585435 0.00099755474 0.0060440055 0.0012967433 0.0064621065
		 0.0013530126 0.0060624429 0.0012798507 0.0068237879 0.0012566034 0.0071203937 0.0012708388
		 0.0074162954 0.0012732362 0.0075758523 0.0012622416 0.0077407518 0.001262859 0.007878948
		 0.0012843928 0.0080637941 0.0027499502 0.0035937282 0.0027250461 0.0044066478 0.0025430361
		 0.0045372699 0.0025909971 0.0035816538 0.0027196934 0.0032173491 0.0025575538 0.003177159
		 0.39218014 -0.0012596555 0.39251363 -0.0011103026 0.39251485 -0.00091279374 0.39219815
		 -0.0010460486 0.0029213156 0.0035816538 0.0028944772 0.0032532786 0.0031297526 0.0032676621
		 0.0031527595 0.0035795262 0.39304477 -0.0013553428 0.39301926 -0.0010897891 0.0029213042
		 0.0042024539 0.0031563751 0.0041690771 0.0034433401 0.0035738405 0.0034251246 0.0033053216
		 0.0038061389 0.0033597501 0.0038181492 0.0035656872 0.0034514873 0.0040814681 0.0038321854
		 0.0039549759 0.39251801 -0.00059241912 0.39295232 -0.00075644854 0.39285564 -0.00032636899
		 0.39252257 -0.00017926002 0.39224532 -0.0007414156 0.39231345 -0.00034853333 0.0041436795
		 0.0034194586 0.0041501927 0.0035567256 0.0041694026 0.0038162002 0.39278531 -1.8979365e-005
		 0.39252764 1.4422957e-007 0.39238825 -9.4305848e-005 0.0027499388 0.0028513621 0.0025909857
		 0.0027837807 0.0029213042 0.0029339495 0.0031512028 0.0029644205 0.003439808 0.0030442146
		 0.0038121189 0.0031595023 0.0041419505 0.0032859703 0.39211005 -0.00049882429 0.39225262
		 -0.00018170077 0.39198011 -0.00085675711 0.39189011 -0.0011342815 0.39185578 -0.0013553428
		 0.38112816 -0.23260802 0.69217086 -0.31785691 0.14221774 -0.66191393 0.14217719 -0.6496864
		 0.14453669 -0.64117259 0.72113633 0.025218189 0.15121716 -0.66422778 0.1472497 -0.65058756
		 0.15021552 -0.64202231 0.082349978 -0.22654454 0.082398862 -0.2264051 0.082206719
		 -0.22638965 0.082169726 -0.22653705 0.082306668 -0.22614735 0.082180925 -0.22613245
		 0.082021341 -0.22637664 0.081989087 -0.22651771 0.082059644 -0.22610995 0.082133755
		 -0.22668038 0.082004897 -0.22665489 0.082267329 -0.22668016 0.14683117 -0.63289332
		 0.15399854 -0.63369298 0.45279533 -0.085277297 0.27199695 0.016196698 0.16485251
		 -0.21819568 -0.046072692 -0.22210631 0.095347449 0.090325393;
createNode polyMapSewMove -n "polyMapSewMove1";
	rename -uid "EB19630D-49C9-85CB-422D-89BA819472C3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[17]" "e[41]" "e[280]";
createNode polyPlanarProj -n "polyPlanarProj8";
	rename -uid "1B5A8FCE-49D3-79BF-8832-25B07133DF80";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "f[11]" "f[27]" "f[29]" "f[33]" "f[36]" "f[63]" "f[73:76]" "f[84:87]" "f[122:123]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0.19816899299621582 -0.90894699096679688 6.2648570537567139 ;
	setAttr ".ro" -type "double3" 0 90 0 ;
	setAttr ".ps" -type "double2" 5.6966603952563659 5.9177131652832031 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV7";
	rename -uid "980E6140-4BBD-9B6E-7C04-8581A022726E";
	setAttr ".uopa" yes;
	setAttr -s 254 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 0.19168119 -0.57152265 0.19126004 -0.57130909
		 0.19123404 -0.57162118 0.19156589 -0.57180202 0.19161054 -0.57114393 0.19128346 -0.57086051
		 0.19125195 -0.57107508 0.19165829 -0.57132715 0.19104761 -0.57072139 0.19096874 -0.57094234
		 0.19097653 -0.57118315 0.19102083 -0.57150215 0.1915599 -0.57093471 0.19133732 -0.57062393
		 0.19116707 -0.57047981 0.19080484 -0.57054269 0.19098927 -0.57030028 0.19067881 -0.57076597
		 0.19068474 -0.57100827 0.19079596 -0.57132959 -0.001342055 -0.0034429773 -0.0010001453
		 -0.0032972714 -0.001010544 -0.0028853489 -0.0014197954 -0.0029645213 -0.0014674335
		 -0.0024800343 -0.0010086342 -0.0024700565 -0.00099286088 -0.0021511635 -0.001465076
		 -0.0022234381 -0.0019549702 -0.0006151842 -0.0011488207 -0.00050244352 -0.001202175
		 -4.003576e-005 -0.0020426535 -1.7040431e-006 -0.00053636078 -0.00045144313 -0.00056034722
		 -9.316887e-005 -0.00074738666 -0.0032362235 -0.00070508238 -0.0028702759 -0.00066372356
		 -0.0025016139 -0.00063741737 -0.0021329806 -0.0010250375 -0.0014499987 -0.0010112802
		 -0.0018357617 -0.00060417835 -0.001775364 -0.00056647533 -0.001335987 -0.001551521
		 -0.0019656082 -0.0016329219 -0.0016514366 -0.0012761794 -0.0044473745 -0.0010182289
		 -0.0041560656 -0.0010006591 -0.0036555778 -0.0012983732 -0.0038607088 -0.00078296888
		 -0.0035543789 -0.00083333591 -0.0039978032 -0.0017990893 -0.0013037784 -0.001067327
		 -0.0010275777 -0.000514885 -0.00086127257 -0.00056724285 -0.0034020289 -0.00064830674
		 -0.0037739181 -0.00049839594 -0.0031342038 -0.00040579279 -0.002825663 -0.00032706212
		 -0.0025153847 -0.00029156252 -0.0021125041 -0.00020903436 -0.0017265949 -0.00012249468
		 -0.0012510304 1.917794e-005 -0.00074180256 5.4987264e-005 -0.00045851304 5.856881e-005
		 -0.00021485516 -0.0032160699 -0.002727641 -0.0036330451 -0.0027630127 -0.0035109152
		 -0.0030709044 -0.0031365878 -0.0030011446 -0.0037278405 -0.0024454773 -0.0032874723
		 -0.0024454773 -0.0033070608 -0.0020263449 -0.0037429961 -0.0020022735 -0.0041513508
		 -0.00028167636 -0.0035152126 -0.00033027437 -0.0034910562 -3.5514652e-010 -0.0041128471
		 -3.5514652e-010 -0.0025674603 -0.0026553671 -0.0028703213 -0.0026860195 -0.0028560238
		 -0.0029191554 -0.0025963269 -0.002858825 -0.002890409 -0.0020546361 -0.0028915005
		 -0.0024454773 -0.00255906 -0.0024454773 -0.0025495423 -0.0020754549 -0.0028131567
		 -3.5514652e-010 -0.0028217009 -0.00038740828 -0.0022832802 -0.0004294714 -0.0022869408
		 -3.5514652e-010 -0.0022401626 -0.0026223348 -0.0023148984 -0.0027938269 -0.0021859459
		 -0.0020978816 -0.0022004752 -0.0024454773 -0.0017229448 -0.00047477707 -0.0017392527
		 -3.5514652e-010 -0.0038396118 -0.0016012171 -0.0033555846 -0.0016470578 -0.0034064886
		 -0.0011731478 -0.0039345971 -0.0011000836 -0.0024142801 -0.0013222271 -0.0024811153
		 -0.0017406342 -0.0020695736 -0.0017833574 -0.0019563213 -0.0013903367 -0.0028513554
		 -0.0012590239 -0.0028704449 -0.0017009596 -0.0030741678 -0.0032440259 -0.0034263774
		 -0.0033442525 -0.0033484604 -0.0037350832 -0.0029988405 -0.0035912723 -0.0028381217
		 -0.0031262098 -0.0028069071 -0.0034222496 -0.0026051549 -0.003039507 -0.0025929047
		 -0.0032977881 -0.0023521022 -0.0029460674 -0.0023594678 -0.0031637445 -0.0041128471
		 -0.0005853512 -0.0034910562 -0.00068634807 -0.0028131567 -0.00080507784 -0.0022869408
		 -0.00089248776 -0.0017392527 -0.000986645 0.030969225 -0.11755962 0.029953064 -0.11720511
		 0.029850289 -0.11804715 0.030753003 -0.11857726 0.030042831 -0.1163721 0.03112537
		 -0.11653703 0.031583242 -0.11551321 0.030119011 -0.11534565 0.029350149 -0.11640509
		 0.029414022 -0.11535823 0.029300911 -0.11710935 0.029268762 -0.11782701 0.030228755
		 -0.11431532 0.032213181 -0.11450526 0.032425094 -0.11327115 0.030533481 -0.11305942
		 0.029428255 -0.11430998 0.029550141 -0.11303153 0.029679198 -0.11975667 0.02977176
		 -0.11876675 0.029228309 -0.11843602 0.029143784 -0.11926495 0.030612245 -0.11945903
		 0.030502824 -0.12069513 0.032873724 -0.11195911 0.030914737 -0.11164042 0.029616633
		 -0.11161938 0.028589912 -0.11307109 0.028353142 -0.1117107 0.028648525 -0.1143263
		 0.028729318 -0.11535576 0.028679814 -0.11638257 0.028671501 -0.11693642 0.028709613
		 -0.11750862 0.028707568 -0.11798837 0.028632835 -0.11862975 -0.0016716311 -0.0056661787
		 -0.0016456469 -0.0065145073 -0.0014556966 -0.0066509177 -0.0015057514 -0.0056536095
		 -0.0016400587 -0.0052733892 -0.0014708575 -0.0052314517 -0.39212862 0.0013067542
		 -0.39247456 0.0011518457 -0.39247584 0.00094685861 -0.3921473 0.0010850362 -0.0018504559
		 -0.0056536095 -0.0018224708 -0.0053109028 -0.0020680088 -0.0053259004 -0.0020920169
		 -0.0056513627 -0.39302555 0.0014059929 -0.39299905 0.0011304885 -0.0018504676 -0.0063014682
		 -0.002095785 -0.0062666493 -0.0023952711 -0.005645412 -0.0023762349 -0.0053652241
		 -0.0027738898 -0.0054220138 -0.0027864175 -0.0056369225 -0.0024037622 -0.0061751856
		 -0.0028010563 -0.0060431622 -0.39247912 0.0006144928 -0.39292964 0.00078457856 -0.39282939
		 0.00033851471 -0.39248389 0.00018597186 -0.39219621 0.00076906715 -0.3922669 0.00036157083
		 -0.0031261558 -0.0054843319 -0.0031329498 -0.0056275446 -0.0031529902 -0.0058983653
		 -0.39275637 1.9687841e-005 -0.39248911 -1.8592539e-007 -0.3923445 9.7766337e-005
		 -0.001671619 -0.0048914109 -0.0015057393 -0.0048209419 -0.0018504676 -0.0049776509
		 -0.0020903754 -0.0050094244 -0.0023915747 -0.0050927349 -0.0027801029 -0.0052130581
		 -0.0031243293 -0.0053449934 -0.39205593 0.00051737827 -0.39220384 0.00018845724 -0.39192113
		 0.00088870665 -0.39182779 0.001176676 -0.39179215 0.0014059929 -0.29737306 0.69608796
		 -0.29737461 0.69655377 -0.29728472 0.69687814 0.03291215 -0.10987434 -0.29703024
		 0.69599986 -0.29718143 0.69651949 -0.29706839 0.69684577 -0.18739919 0.64106333 -0.18745035
		 0.64091766 -0.18724962 0.64090157 -0.18721098 0.64105552 -0.18735406 0.64064848 -0.18722267
		 0.64063293 -0.18705602 0.64088792 -0.18702233 0.64103532 -0.18709593 0.64060938 -0.18717335
		 0.64120519 -0.18703884 0.64117861 -0.18731298 0.64120501 -0.29719734 0.6971935 -0.29692426
		 0.69716311 0.031313788 -0.11067328 0.030158006 -0.11013122 0.029039871 -0.10975523
		 -0.089312769 0.63022673 -0.083103783 0.45183223 0.059883017 0.52692842 0.063856974
		 0.60680252 0.19264333 0.51894677 0.21522659 0.585078 0.31649578 0.52810049 0.31845111
		 0.59634823 0.28272808 0.45452106 0.14245009 0.4476493 0.15019616 0.35813913 0.28179157
		 0.35892078 0.27869678 0.23890123 0.16753922 0.24576516 0.19199528 0.10081315 0.27401888
		 0.084087089 0.21701697 -0.027607167 0.26797071 -0.053070832 0.3530975 0.077029102
		 0.31709468 -0.060197849 0.38586277 0.23192099 0.41796896 0.44764939 0.40866193 0.35200098;
	setAttr ".uvtk[250:253]" 0.43590054 0.51894689 0.41796896 0.585078 -0.089312769
		 0.25950113 0.035984449 0.44081444;
createNode polyPlanarProj -n "polyPlanarProj9";
	rename -uid "D9F85229-47E7-1058-D812-2CA38232EACF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[84]";
	setAttr ".ix" -type "matrix" 1.8281981762119068 0 0 0 0 6.4799443071415848 0 0 0 0 4.2992373660819165 0
		 0 7.247934129457982 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0.85866695642471313 -1.8363451957702637 4.7136058807373047 ;
	setAttr ".ro" -type "double3" -16.970038070373178 45.734763182720599 0 ;
	setAttr ".ps" -type "double2" 2.198397486443131 2.2921514511108398 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV8";
	rename -uid "35106706-4726-DD17-F849-DF9849F5E33F";
	setAttr ".uopa" yes;
	setAttr -s 257 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" -0.80257577 0.67064917 -0.80130208
		 0.67000329 -0.80122346 0.67094696 -0.8022272 0.67149389 -0.80236226 0.66950381 -0.80137283
		 0.66864669 -0.80127764 0.66929567 -0.80250651 0.67005771 -0.80065966 0.668226 -0.80042124
		 0.66889417 -0.80044478 0.66962236 -0.80057859 0.67058712 -0.80220896 0.66887105 -0.80153579
		 0.66793114 -0.80102086 0.66749531 -0.79992539 0.66768539 -0.80048335 0.66695243 -0.79954416
		 0.66836077 -0.79956216 0.66909343 -0.79989851 0.67006522 0.0042742551 0.010397868
		 0.0032405334 0.0099574439 0.0032719707 0.0087118829 0.0045092977 0.0089512067 0.0046533877
		 0.0074864342 0.0032661776 0.0074562556 0.0032185153 0.0064921943 0.0046462528 0.0067106816
		 0.0061274688 0.0018480743 0.0036900064 0.0015071774 0.0038513467 0.00010908276 0.0063925539
		 -6.8151626e-006 0.001838253 0.0013529772 0.0019107818 0.00026973453 0.0024762987
		 0.0097728129 0.0023483937 0.0086663039 0.002223359 0.007551644 0.0021438263 0.0064371866
		 0.0033157868 0.004372145 0.0032741916 0.0055385302 0.0020433017 0.0053559099 0.0019293325
		 0.0040274579 0.0049076206 0.0059311693 0.0051537193 0.0049812146 0.0040750778 0.013434789
		 0.0032951878 0.012553938 0.0032420889 0.0110407 0.0041421894 0.011660976 0.002583877
		 0.010734684 0.0027361724 0.012075507 0.0056561483 0.0039300537 0.0034436525 0.00309494
		 0.0017733339 0.0025920973 0.0019316215 0.010274101 0.0021767293 0.011398559 0.0017234716
		 0.0094642965 0.0014434741 0.0085314354 0.0012054386 0.0075933849 0.001098112 0.0063752821
		 0.0008485794 0.0052084918 0.00058692996 0.0037705766 0.00015857416 0.0022309022 5.0303945e-005
		 0.0013743452 3.9474959e-005 0.00063765183 0.0083783045 0.0082349451 0.0096389661
		 0.0083419988 0.0092698205 0.0092727635 0.0081379805 0.0090619568 0.0099256355 0.0073819133
		 0.0085941572 0.0073819133 0.0086534182 0.006114692 0.0099714659 0.0060419282 0.011206187
		 0.00083966122 0.0092827836 0.0009865989 0.0092097083 -1.1984909e-005 0.011089651
		 -1.1984909e-005 0.0064172866 0.0080164531 0.0073329983 0.0081091141 0.0072897626
		 0.0088140676 0.0065044365 0.0086316122 0.007393721 0.0062002176 0.0073970184 0.0073819133
		 0.0063918582 0.0073819133 0.0063630054 0.0062631741 0.0071601537 -1.1984909e-005
		 0.007185909 0.0011593495 0.0055580027 0.0012865245 0.0055690468 -1.1984909e-005 0.0054276078
		 0.0079166694 0.0056536915 0.0084350659 0.0052637858 0.0063310093 0.0053076493 0.0073819133
		 0.0038639577 0.0014235113 0.0039132554 -1.1984909e-005 0.01026367 0.0048293341 0.0088001573
		 0.0049679209 0.0089540854 0.0035349962 0.010550823 0.0033141507 0.0059540938 0.0039858101
		 0.0061561954 0.0052508069 0.0049119527 0.0053800233 0.0045694248 0.0041917115 0.0072756661
		 0.003794648 0.0073332745 0.0051308684 0.0079493066 0.0097963037 0.0090141864 0.010099305
		 0.008778559 0.011280996 0.007721581 0.010846246 0.0072356528 0.0094400765 0.0071412302
		 0.010335124 0.0065311748 0.0091778804 0.0064942301 0.0099588726 0.0057660653 0.008895413
		 0.0057883463 0.009553574 0.011089651 0.0017578286 0.0092097083 0.0020631957 0.0071601537
		 0.0024221926 0.0055690468 0.0026864703 0.0039132554 0.0029711467 0.017413219 0.0010349794
		 0.018324697 0.00071701652 0.018416861 0.0014722768 0.017607151 0.0019477888 0.018244203
		 -3.0203819e-005 0.017273083 0.00011772807 0.016862435 -0.00080064038 0.018175812
		 -0.00095091178 0.018865528 -6.2750593e-007 0.018808242 -0.00093965826 0.018909683
		 0.00063112652 0.018938519 0.0012748188 0.018077452 -0.001875103 0.01629735 -0.0017047495
		 0.016107256 -0.0028117539 0.017804056 -0.0030016329 0.018795433 -0.0018799396 0.018686093
		 -0.0030266754 0.018570364 0.0030056653 0.018487353 0.0021177351 0.018974772 0.0018210603
		 0.01905054 0.0025646717 0.017733432 0.0027387172 0.017831592 0.0038475201 0.015704835
		 -0.0039886087 0.017462101 -0.0042744856 0.018626401 -0.0042933421 0.019547476 -0.0029911848
		 0.019759836 -0.0042114314 0.019494789 -0.0018652863 0.019422406 -0.00094184047 0.019466802
		 -2.0779024e-005 0.019474212 0.00047597013 0.019440059 0.00098924024 0.019441925 0.0014195325
		 0.019508978 0.0019949065 0.0040949862 0.011295062 0.0040164511 0.013860195 0.0034420553
		 0.014272756 0.0035934129 0.011257031 0.0039995103 0.010107432 0.0034879155 0.0099806068
		 0.76774383 -0.0099557759 0.76878995 -0.0094873086 0.76879382 -0.0088675497 0.76780033
		 -0.009285544 0.0046357322 0.011257031 0.0045510707 0.010220795 0.0052934829 0.010266221
		 0.0053660767 0.011250275 0.77045572 -0.010255748 0.77037579 -0.0094228592 0.004635721
		 0.013215972 0.0053774915 0.013110676 0.0062829582 0.011232348 0.0062255207 0.010385099
		 0.0074277571 0.010556785 0.0074656536 0.011206599 0.0063087228 0.012834185 0.0075099058
		 0.012435011 0.76880366 -0.0078628827 0.7701658 -0.0083771562 0.76986265 -0.0070283646
		 0.76881808 -0.0065669427 0.76794827 -0.0083302334 0.76816207 -0.0070979493 0.0084929224
		 0.010745187 0.0085134795 0.011178298 0.0085741095 0.011997185 0.769642 -0.0060641575
		 0.76883382 -0.0060042338 0.76839656 -0.006300481 0.0040949937 0.0089524947 0.0035934208
		 0.0087393327 0.004635721 0.0092131747 0.0053611677 0.0093092769 0.0062718815 0.0095611205
		 0.0074466169 0.0099249398 0.0084873876 0.010323958 0.76752418 -0.0075692274 0.76797128
		 -0.0065744659 0.76711655 -0.0086919712 0.76683426 -0.009562403 0.76672655 -0.010255748
		 0.18930772 -0.68072069 0.27218407 -0.55892909 0.35361841 -0.4903903 0.015670408 -0.0058586481
		 0.26298401 -0.76500916 0.31645653 -0.60243273 0.40426719 -0.5375123 0.1916261 -0.70264047
		 0.1917806 -0.70220029 0.19117393 -0.7021516 0.19105719 -0.70261675 0.19148959 -0.70138687
		 0.19109263 -0.70133984 0.19058907 -0.70211047 0.19048718 -0.70255572 0.19070975 -0.70126861
		 0.19094375 -0.70306915 0.19053715 -0.70298868 0.19136538 -0.70306844 0.43280971 -0.42373925
		 0.49859104 -0.48050219 0.017104117 -0.0051419977 0.018140901 -0.005628217 0.019143803
		 -0.0059654792 0.012015894 0.011945873 0.011988857 0.012723313 0.011365723 0.012396059
		 0.011348419 0.012047982 0.010787171 0.012430842 0.01068877 0.012142636 0.010247475
		 0.012390939 0.010238916 0.012093507 0.010394598 0.01271161 0.01100592 0.012741548
		 0.010972153 0.013131586 0.01039868 0.013128211 0.010412183 0.013651244 0.010896584
		 0.013621315 0.010790023 0.014253023 0.010432544 0.014325952 0.010680984 0.014812641
		 0.010458918 0.014923587 0.010087946 0.014356649 0.010244863 0.014954721 0.0099451859
		 0.013681706 0.0098052807 0.012741558 0.0098458277 0.013158342;
	setAttr ".uvtk[250:256]" 0.0097270999 0.012430842 0.0098052807 0.012142636
		 0.61189759 0.17177773 0.011469894 0.01277132 0.18574418 -0.13404137 0.18064891 -0.74967307
		 0.69484168 -0.15647724;
createNode polyMapSewMove -n "polyMapSewMove2";
	rename -uid "70DBC814-4AC7-4251-C5E2-E68EFA3CF881";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[25]";
createNode polyMapSewMove -n "polyMapSewMove3";
	rename -uid "1F270217-49F8-80F3-548A-D38DEC2128E3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[40]" "e[350]";
createNode polyTweakUV -n "polyTweakUV9";
	rename -uid "278495FC-479D-0648-D49F-688463FB3341";
	setAttr ".uopa" yes;
	setAttr -s 253 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 0.8393302 -0.23730485 0.83211029 -0.23364356
		 0.83628267 -0.23636292 0.83722413 -0.24247845 0.83783108 -0.23134029 0.83184755 -0.2261046
		 0.8313008 -0.22973229 0.83862388 -0.23513104 0.82735586 -0.2233922 0.82560498 -0.22749186
		 0.82581455 -0.23273756 0.83257824 -0.23328634 0.83689213 -0.22729994 0.83300596 -0.22210984
		 0.83010393 -0.21916294 0.82337242 -0.21890481 0.82794029 -0.21502145 0.82019532 -0.22292334
		 0.82015121 -0.22806998 0.83359039 -0.22354181 -0.017047089 -0.046846401 -0.01242754
		 -0.04487814 -0.014166894 -0.0391156 -0.018940683 -0.040113628 -0.019936351 -0.0327386
		 -0.014829149 -0.032181483 -0.012445001 -0.028900752 -0.017339712 -0.029682193 -0.028475622
		 -0.0088409465 -0.014894295 -0.0088441754 -0.015371163 -0.0017444934 -0.029737754
		 0.00011857725 -0.0055023977 -0.00715954 -0.0076999278 -0.0033107686 -0.0083219251
		 -0.043605693 -0.0083980355 -0.038535442 -0.0079514887 -0.032943249 -0.0067928694
		 -0.029295892 -0.012485509 -0.020211285 -0.012902915 -0.025094474 -0.0060301614 -0.024384119
		 -0.0056906273 -0.018321902 -0.019601882 -0.025866631 -0.020600513 -0.020977216 -0.016663179
		 -0.060725398 -0.012882434 -0.056665629 -0.012173964 -0.049760889 -0.016100587 -0.052779667
		 -0.008518585 -0.048175529 -0.009530508 -0.054503012 -0.024525488 -0.017209264 -0.013472547
		 -0.015944283 -0.0045873825 -0.012614997 -0.0048989663 -0.04562724 -0.0061142747 -0.05129746
		 -0.0042041312 -0.041480508 -0.0030420725 -0.037558023 -0.0018302959 -0.033227012
		 -0.00077419565 -0.029340265 0.00083574158 -0.023630146 0.0017841305 -0.016710203
		 0.0040795282 -0.0099227559 0.0032581738 -0.006201481 -3.443272e-008 -0.0065824646
		 0.17293017 -0.065852843 0.16501641 -0.066524856 0.17547859 -0.068364114 0.17881311
		 -0.06890469 0.15717651 -0.057407532 0.16785413 -0.059759945 0.16712393 -0.047953364
		 0.1592952 -0.041890007 0.1532452 0.0036474436 0.16734788 -0.0068082074 0.17123649
		 0.0028348442 0.15970376 0.010542393 0.18101311 -0.05147377 0.18249248 -0.060314119
		 0.1831387 -0.062649578 0.17982586 -0.053635214 0.1760266 -0.046051338 0.18078423
		 -0.055368125 0.17989856 -0.047898766 0.17363966 -0.040398538 0.18840389 -0.00032597824
		 0.18910873 -0.010642445 0.19156441 -0.0099096717 0.18819366 0.00028920089 0.17136034
		 -0.040969394 0.17063223 -0.043240976 0.16435474 -0.032678861 0.1698245 -0.038169917
		 0.18321876 -0.0070736893 0.17921549 0.0032414296 0.15696806 -0.030442813 0.16605718
		 -0.040538672 0.16719978 -0.029306347 0.15780672 -0.015992794 0.18024032 -0.027381787
		 0.17725231 -0.035260115 0.16834602 -0.028470132 0.17202194 -0.022004658 0.1805205
		 -0.031077696 0.17827179 -0.040179551 0.18355498 -0.070463687 0.18243767 -0.07087829
		 0.18970852 -0.075884148 0.19024137 -0.074158721 0.18556371 -0.064516865 0.19163837
		 -0.0680952 0.18154737 -0.055869095 0.1881025 -0.059508968 0.17259468 -0.045811716
		 0.17956597 -0.049403109 0.15192205 -0.0048949602 0.16568102 -0.019339615 0.18867953
		 -0.023122268 0.19232617 -0.021458542 0.18453448 -0.017821599 -0.3319526 -0.13500005
		 -0.3373661 -0.14854637 -0.32604563 -0.15028858 -0.31815195 -0.1377698 -0.34829363
		 -0.14626035 -0.3453005 -0.13408682 -0.35762703 -0.12932104 -0.36299244 -0.14629018
		 -0.35094059 -0.15623415 -0.36468846 -0.15632975 -0.34115204 -0.15767804 -0.33049828
		 -0.1587597 -0.37580994 -0.14442459 -0.36618948 -0.12185845 -0.38023746 -0.1173 -0.3897509
		 -0.13975044 -0.37889725 -0.15410021 -0.39313123 -0.15001239 -0.30007696 -0.15134829
		 -0.31562498 -0.15102459 -0.32171068 -0.15926719 -0.30943358 -0.15947917 -0.30578271
		 -0.13950852 -0.28745547 -0.14031017 -0.3924956 -0.11172923 -0.4032371 -0.13483885
		 -0.4094536 -0.14602266 -0.39742616 -0.16034269 -0.41707852 -0.158834 -0.38228416
		 -0.16380039 -0.36749253 -0.16597591 -0.35386741 -0.16618867 -0.34580511 -0.16678877
		 -0.33640888 -0.16687946 -0.32962683 -0.16712534 -0.32047445 -0.16703413 0.054705661
		 0.068184033 0.19253683 -0.076492809 0.24465387 -0.068346441 0.08021351 0.09707433
		 0.0019002906 0.13434237 0.020752281 0.16743079 -0.16619805 -0.14139582 -0.17169672
		 -0.15137108 -0.1715823 -0.15143423 -0.16650218 -0.14500219 0.025593296 0.03492387
		 -0.022751871 0.095756516 -0.060911827 0.053618964 -0.015812721 -0.0043671415 -0.18018971
		 -0.13874319 -0.18220305 -0.14381801 0.13028179 -0.081774876 0.081579506 -0.11549448
		 -0.067470826 -0.053358585 -0.10634875 -0.0031735075 -0.16436593 -0.077490993 -0.13428016
		 -0.11633102 0.014270126 -0.14967281 -0.074337877 -0.19093168 -0.17147429 -0.15586403
		 -0.18152151 -0.1495344 -0.17867041 -0.15688284 -0.17134088 -0.16405609 -0.16649343
		 -0.15030739 -0.16764133 -0.15718868 -0.21427236 -0.14577565 -0.19367643 -0.17231271
		 -0.15636835 -0.22268136 -0.1761909 -0.1610876 -0.17121485 -0.16551846 -0.17139556
		 -0.16006957 -0.061505832 0.18841936 -0.047822647 0.22552341 -0.078148253 0.14486942
		 -0.11258895 0.10126256 -0.15002398 0.038649887 -0.19669154 -0.044352759 -0.23475038
		 -0.12263481 -0.16743985 -0.14998604 -0.17307957 -0.15418153 -0.16327485 -0.14429221
		 -0.16114673 -0.13881014 -0.16213599 -0.13397008 -0.41291019 -0.097726062 -0.40999857
		 -0.12619716 -0.42158121 -0.14936846 -0.43172294 -0.080707386 -0.42736071 -0.11818796
		 -0.45010766 -0.15725648 -0.85105717 0.47745979 -0.86153525 0.48161468 -0.85453784
		 0.47612697 -0.84998029 0.47741497 -0.86681664 0.46960527 -0.86292982 0.46326983 -0.85500562
		 0.46594861 -0.84612453 0.46932203 -0.86369193 0.45820662 -0.8441118 0.47496459 -0.84155202
		 0.47317359 -0.84396476 0.47248334 -0.43809071 -0.17199337 -0.47741857 -0.19396827
		 -0.1028055 -0.11699484 -0.10257675 -0.1235673 -0.097748436 -0.12249376 -0.095406808
		 -0.12493894 -0.090684734 -0.12702842 -0.087028123 -0.13314554 -0.083657987 -0.12463672
		 -0.082306549 -0.12647754 -0.087656505 -0.12045891 -0.096779548 -0.11825079 -0.097908057
		 -0.12350008 -0.08813376 -0.12498786 -0.089153156 -0.13038419 -0.097803429 -0.12907737
		 -0.096377827 -0.13556902 -0.089969546 -0.13719131 -0.094104618 -0.13961522 -0.090423398
		 -0.14190434 -0.084207013 -0.13592909 -0.086771026 -0.14112633 -0.0816238 -0.12907387
		 -0.079693452 -0.11848 -0.079671606 -0.12366617 -0.07807418 -0.1176376 -0.080462918
		 -0.11652407 -0.44426119 -0.081174709 -0.10272474 -0.11822843;
	setAttr ".uvtk[250:252]" -0.44029725 -0.091675177 -0.43678427 -0.092610523
		 -0.44155085 -0.080825508;
createNode polyMapSewMove -n "polyMapSewMove4";
	rename -uid "786D8431-4EAC-3FCC-8C24-4C8761C5F184";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "e[5]" "e[14]" "e[21]" "e[126]" "e[155]" "e[209]" "e[230]" "e[245]" "e[260]";
createNode polyTweakUV -n "polyTweakUV10";
	rename -uid "4EA741BA-4E30-9D35-A15E-94901937ABAE";
	setAttr ".uopa" yes;
	setAttr -s 242 ".uvtk[0:241]" -type "float2" -0.15301159 0.17184569 -0.1477057
		 0.16915511 -0.14701071 0.17329583 -0.15157014 0.17533377 -0.15214542 0.16703208 -0.14805342
		 0.16349187 -0.14765745 0.16619928 -0.15274788 0.16928804 -0.14511807 0.1617655 -0.14415666
		 0.16452402 -0.14424883 0.16746865 -0.14432856 0.17187893 -0.15151222 0.16443257 -0.14871378
		 0.16050608 -0.14656731 0.15872857 -0.1420455 0.15962678 -0.14425777 0.15655166 -0.14053817
		 0.16242506 -0.14062454 0.16539794 -0.14110787 0.17024526 0.018154856 0.043890681
		 0.013750344 0.042014033 0.013757009 0.036722615 0.019089274 0.037748091 0.019675136
		 0.031572897 0.013677543 0.031477943 0.013647265 0.027288435 0.019848885 0.028234884
		 0.025800539 0.0074454756 0.015629027 0.0058714799 0.016335871 -1.7723005e-005 0.026923863
		 -0.00040445675 0.0078279031 0.0052937111 0.0079877796 0.00059924502 0.010549041 0.041262921
		 0.0099525051 0.036558416 0.0094107818 0.031857666 0.00913586 0.027003001 0.014093127
		 0.018192807 0.013867848 0.02318877 0.0087325247 0.022402285 0.0082333917 0.016751939
		 0.020875512 0.024939924 0.021932043 0.020943653 0.017265912 0.056805931 0.013966426
		 0.053062603 0.013777692 0.046626348 0.017620549 0.049249467 0.011030023 0.045339804
		 0.011652551 0.051025994 0.023939183 0.016390875 0.014604852 0.012636263 0.0076010297
		 0.010579869 0.0083069811 0.04341637 0.0093417959 0.048155982 0.0074013988 0.040007956
		 0.0062012891 0.036013454 0.0051988489 0.03202777 0.0047874288 0.026713673 0.0037636033
		 0.021781689 0.0026311777 0.015694385 0.0008363864 0.0091267377 0.00027116423 0.005468654
		 -3.8207872e-005 0.0020373026 0.063394055 0.056649189 0.080683187 0.058117356 0.076637201
		 0.06869638 0.064178415 0.06534227 0.076846108 0.043838143 0.062445112 0.049618904
		 0.056182731 0.040594365 0.067014471 0.030578205 0.026238898 -0.0062896181 0.018523635
		 0.010983348 0.010028845 0.0077409144 0.018475117 -0.009343775 0.051581077 0.062350653
		 0.056673281 0.059600338 0.057631828 0.064669631 0.052448824 0.065103218 0.048924189
		 0.049031857 0.054820672 0.055043813 0.049691319 0.060047366 0.043841448 0.056421012
		 0.0031500096 0.025436848 0.012848213 0.028358126 0.009099301 0.043245535 -0.0010966219
		 0.040680539 0.04767165 0.065642409 0.048868947 0.065681025 0.043108683 0.064637348
		 0.046824113 0.066491872 0.006367797 0.0592614 -0.004767877 0.056705907 0.057815898
		 0.020044722 0.048552126 0.032473348 0.038143631 0.023883078 0.045985013 0.0095340339
		 0.028772164 0.049516372 0.037520893 0.053088896 0.035751283 0.06232737 0.02551013
		 0.060260456 0.032772012 0.037929136 0.042164557 0.043674331 0.063226156 0.072125956
		 0.073818445 0.076853938 0.070616171 0.088106543 0.060344778 0.081547782 0.057629827
		 0.069343083 0.056128334 0.075734124 0.052756865 0.067743041 0.051941197 0.071995832
		 0.049257517 0.067432269 0.047270767 0.069408834 0.033755589 -0.0014203305 0.026649948
		 0.01562658 0.022324299 0.032523923 0.019295696 0.046660043 0.013434589 0.061285269
		 0.052102521 0.065272823 0.048679151 0.066705048 0.04817849 0.065406255 0.049729746
		 0.062493611 0.048027344 0.068716496 0.052214991 0.070387073 0.050644897 0.077368662
		 0.04501405 0.070762776 0.046791658 0.067665175 0.044891965 0.067429103 0.047721874
		 0.066826142 0.048232634 0.066517867 0.038563948 0.073505066 0.046985243 0.086096577
		 0.038888134 0.093787402 0.028880855 0.079530351 0.037582267 0.067693509 0.026348107
		 0.069952272 0.042587508 0.061429489 0.046255149 0.064314701 0.046583533 0.066648968
		 0.042445611 0.065356553 0.047166135 0.060289033 0.043820713 0.055338711 0.029476788
		 0.10466178 0.015932633 0.089291669 0.0097453669 0.076047726 0.16374187 0.078639463
		 0.17435175 0.078964286 0.17584889 0.081517056 0.16345695 0.080763586 0.15909734 0.078423217
		 0.15823624 0.080471739 -0.44812614 0.15417111 -0.44373485 0.15553963 -0.44370791
		 0.15839551 -0.44788951 0.15697786 0.16392069 0.075815931 0.15968122 0.075874902 0.16007087
		 0.072730042 0.16399622 0.072656944 -0.4367207 0.15299743 -0.43725011 0.15643853 0.17225474
		 0.075569637 0.17180212 0.072399125 0.1641168 0.06867408 0.16075669 0.068757668 0.16168849
		 0.06363418 0.16425839 0.063542023 0.17074294 0.068401732 0.16923459 0.063274384 -0.44365358
		 0.16268072 -0.43816528 0.1608106 -0.43933767 0.16645224 -0.4435766 0.16801095 -0.44720572
		 0.16096579 -0.44631028 0.16610669 0.16263296 0.059067819 0.16436742 0.058954887 0.16761741
		 0.058725551 -0.44015908 0.17056838 -0.44349372 0.17049235 -0.44552666 0.16955839
		 0.1544905 0.077396266 0.15326427 0.079365827 0.15566206 0.075132713 0.15626937 0.072104961
		 0.15749376 0.068283267 0.1592021 0.063355133 0.16097723 0.058965482 -0.44923896 0.16450439
		 -0.4476237 0.16876239 -0.45078933 0.1597746 -0.45192289 0.15619278 -0.45249876 0.15337743
		 0.012697724 0.11604656 0.0088634295 0.09786386 -0.0020333889 0.088760369 0.0028218979
		 0.12103467 0.0031401878 0.1016422 -0.0077761118 0.093997493 0.35024303 -0.3913033
		 0.35012221 -0.38893992 0.34787774 -0.38915178 0.34770155 -0.39119753 0.34835804 -0.38613927
		 0.34683388 -0.38642624 0.34513956 -0.38977212 0.34537604 -0.39155981 0.34500551 -0.3865009
		 0.34764451 -0.39348173 0.34597084 -0.39325288 0.34960365 -0.39367607 -0.011861598
		 0.079953037 -0.019613054 0.086616047 -0.20114931 -0.069929242 -0.20135485 -0.064022623
		 -0.20612411 -0.066643871 -0.20608091 -0.069717392 -0.21034682 -0.066717148 -0.21086967
		 -0.069587678 -0.21425138 -0.066856705 -0.21421425 -0.069463082 -0.21335261 -0.063872084
		 -0.20902291 -0.063448623 -0.20939194 -0.060640369 -0.21335684 -0.060786963 -0.21332648
		 -0.056890748 -0.21000873 -0.05703412 -0.21077667 -0.052326452 -0.21322286 -0.051852793
		 -0.21149756 -0.048019946 -0.21304098 -0.047284447 -0.21561429 -0.051497899 -0.21452077
		 -0.046965629 -0.21658961 -0.056534842 -0.21759312 -0.063466892 -0.21725591 -0.060432341
		 -0.21811047 -0.065969571 -0.21765435 -0.06826447 8.3600164e-005 0.066827662 -0.20565888
		 -0.063200541 0.006042867 0.069449358;
createNode polyMapCut -n "polyMapCut1";
	rename -uid "51A70174-4800-0294-9817-2682869BD3DE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[21]";
createNode polyTweakUV -n "polyTweakUV11";
	rename -uid "072B432D-4F27-87EB-8A61-01AE752F5AA3";
	setAttr ".uopa" yes;
	setAttr -s 49 ".uvtk";
	setAttr ".uvtk[20]" -type "float2" 0.82150793 0.049958855 ;
	setAttr ".uvtk[21]" -type "float2" 0.82656717 0.02494964 ;
	setAttr ".uvtk[22]" -type "float2" 0.85419524 0.019294053 ;
	setAttr ".uvtk[23]" -type "float2" 0.85457659 0.048230737 ;
	setAttr ".uvtk[24]" -type "float2" 0.88744056 0.044648021 ;
	setAttr ".uvtk[25]" -type "float2" 0.88148636 0.013239086 ;
	setAttr ".uvtk[26]" -type "float2" 0.9033227 0.0085756183 ;
	setAttr ".uvtk[27]" -type "float2" 0.90505159 0.041965395 ;
	setAttr ".uvtk[28]" -type "float2" 1.0199708 0.050675429 ;
	setAttr ".uvtk[29]" -type "float2" 1.0172486 -0.004111588 ;
	setAttr ".uvtk[30]" -type "float2" 1.0487498 -0.0067550838 ;
	setAttr ".uvtk[31]" -type "float2" 1.0621549 0.048097268 ;
	setAttr ".uvtk[32]" -type "float2" 1.0118752 -0.045454033 ;
	setAttr ".uvtk[33]" -type "float2" 1.0365518 -0.049667951 ;
	setAttr ".uvtk[34]" -type "float2" 0.82704532 0.0074313581 ;
	setAttr ".uvtk[35]" -type "float2" 0.85096097 -0.00074175 ;
	setAttr ".uvtk[36]" -type "float2" 0.8749159 -0.0086246729 ;
	setAttr ".uvtk[37]" -type "float2" 0.89996111 -0.015280455 ;
	setAttr ".uvtk[38]" -type "float2" 0.95128053 0.0011215806 ;
	setAttr ".uvtk[39]" -type "float2" 0.9249599 0.0053183138 ;
	setAttr ".uvtk[40]" -type "float2" 0.92354262 -0.022333384 ;
	setAttr ".uvtk[41]" -type "float2" 0.95250028 -0.031015217 ;
	setAttr ".uvtk[42]" -type "float2" 0.92335498 0.043780833 ;
	setAttr ".uvtk[43]" -type "float2" 0.94535136 0.044998318 ;
	setAttr ".uvtk[44]" -type "float2" 0.75313544 0.059207767 ;
	setAttr ".uvtk[45]" -type "float2" 0.76912719 0.037959188 ;
	setAttr ".uvtk[46]" -type "float2" 0.80252075 0.030052513 ;
	setAttr ".uvtk[47]" -type "float2" 0.792961 0.05293265 ;
	setAttr ".uvtk[48]" -type "float2" 0.80628151 0.014326364 ;
	setAttr ".uvtk[49]" -type "float2" 0.77726966 0.02369073 ;
	setAttr ".uvtk[50]" -type "float2" 0.97127497 0.050579295 ;
	setAttr ".uvtk[51]" -type "float2" 0.98083556 -0.0021827966 ;
	setAttr ".uvtk[52]" -type "float2" 0.98403794 -0.040953562 ;
	setAttr ".uvtk[53]" -type "float2" 0.81339335 -0.0019561946 ;
	setAttr ".uvtk[54]" -type "float2" 0.78976607 0.0085424483 ;
	setAttr ".uvtk[55]" -type "float2" 0.83021122 -0.010348648 ;
	setAttr ".uvtk[56]" -type "float2" 0.8497715 -0.020908833 ;
	setAttr ".uvtk[57]" -type "float2" 0.86949837 -0.030427635 ;
	setAttr ".uvtk[58]" -type "float2" 0.89679497 -0.038290024 ;
	setAttr ".uvtk[59]" -type "float2" 0.92143846 -0.048938155 ;
	setAttr ".uvtk[60]" -type "float2" 0.95199591 -0.061395645 ;
	setAttr ".uvtk[61]" -type "float2" 0.98434836 -0.077827111 ;
	setAttr ".uvtk[62]" -type "float2" 1.0028354 -0.08471144 ;
	setAttr ".uvtk[63]" -type "float2" 1.0204141 -0.090016365 ;
	setAttr ".uvtk[92]" -type "float2" -0.054309282 -0.083274186 ;
	setAttr ".uvtk[239]" -type "float2" -0.054309253 -0.083274186 ;
	setAttr ".uvtk[241]" -type "float2" -0.054309253 -0.083274186 ;
	setAttr ".uvtk[242]" -type "float2" -0.054309282 -0.083274186 ;
createNode polyMapSewMove -n "polyMapSewMove5";
	rename -uid "D67812DE-44D6-1831-BB1F-458DB461C157";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[9]" "e[12]" "e[19]" "e[117]" "e[147]" "e[177]" "e[200]" "e[221]" "e[236]" "e[251]";
createNode polyTweakUV -n "polyTweakUV12";
	rename -uid "93F2564A-47C9-9696-7BE3-C6BBB96499BD";
	setAttr ".uopa" yes;
	setAttr -s 233 ".uvtk[0:232]" -type "float2" 0.082679451 -0.26193422 -0.063910306
		 -0.2573325 -0.036137998 -0.35704529 0.086962581 -0.35488051 0.013017118 -0.16351968
		 -0.11535257 -0.12725866 -0.095888615 -0.19184422 0.050052524 -0.20758808 -0.19893569
		 -0.11940145 -0.19156146 -0.1910401 -0.1587199 -0.25581902 -0.11083317 -0.35345224
		 -0.028294861 -0.11210161 -0.13182354 -0.053692877 -0.19832772 -0.036447346 -0.28989378
		 -0.10377449 -0.27265012 -0.011990249 -0.2942932 -0.18200719 -0.2612859 -0.24747726
		 -0.19981915 -0.35064769 -0.48952472 -0.4029595 -0.4764854 -0.30108276 -0.5701738
		 -0.24516745 -0.61096615 -0.35108912 -0.72614783 -0.29119495 -0.65976828 -0.18604814
		 -0.73107761 -0.13780382 -0.78623927 -0.25541681 -1.20755899 -0.11216613 -1.11426044
		 0.076680496 -1.22102952 0.13459206 -1.35256338 -0.038380746 -1.031617284 0.21240346
		 -1.11123002 0.2666088 -0.4565725 -0.2362868 -0.53213221 -0.17798716 -0.60541743 -0.1181982
		 -0.68448263 -0.057188433 -0.89044762 -0.040919099 -0.80338871 -0.094571091 -0.75815302
		 0.0017577007 -0.84766316 0.074024953 -0.85486048 -0.23435485 -0.93516743 -0.20543434
		 -0.24322432 -0.52227271 -0.27716726 -0.42218867 -0.39305523 -0.35080692 -0.38564309
		 -0.4492406 -0.38801569 -0.28701502 -0.29024911 -0.35807863 -1.03547585 -0.18591824
		 -0.98948187 0.014645883 -0.94225961 0.15420489 -0.39081189 -0.20673893 -0.31213346
		 -0.27478716 -0.44763976 -0.15843585 -0.51006651 -0.10165294 -0.56391579 -0.044786654
		 -0.63812214 0.026722169 -0.71145612 0.092947818 -0.79974288 0.17435907 -0.88962191
		 0.26319817 -0.9359405 0.3131429 -0.97827619 0.36158675 -0.4409509 -0.016450739 -0.38451052
		 -0.064068921 -0.49276683 0.028613687 -0.55463773 0.099403143 -0.82317972 0.37525859
		 -0.86287719 0.43111539 -0.31538624 0.086459614 -0.38152882 0.036155168 -0.33983314
		 -0.0072498256 -0.2855311 0.045022074 -0.47143978 0.15206601 -0.42069754 0.077886149
		 -0.34494102 0.12646812 -0.38986585 0.20002885 -0.74771786 0.51939237 -0.70179623
		 0.45140314 -0.58998024 0.5204367 -0.64223123 0.59617835 -0.23854446 0.14669926 -0.22220072
		 0.10616126 -0.29380485 0.25561246 -0.25541255 0.1837223 -0.16890571 0.026529858 -0.52251911
		 0.68205529 -0.61834329 0.16411561 -0.69470245 0.24175246 -0.48030618 0.35287222 -0.43045071
		 0.26961833 -0.32507259 0.32889637 -0.36367539 0.41523197 -0.58291364 0.30055332 -0.5222069
		 0.21926658 -0.33267367 -0.10676407 -0.26073775 -0.16659367 -0.30248991 -0.04677242
		 -0.25044426 -0.10259094 -0.25855419 0.0085083973 -0.21904033 -0.042790033 -0.20545623
		 0.070957854 -0.18008275 0.024112612 -0.77482933 0.32158425 -0.64758503 0.38273707
		 -0.53115577 0.44049221 -0.39919195 0.50910902 -0.072921723 0.17724952 -0.14926252
		 0.16616693 -0.12664562 0.10728787 -0.050066452 0.10465062 -0.17023568 0.22267985
		 -0.097222395 0.24769342 -0.099592499 0.32775238 -0.19781603 0.29653484 -0.21256281
		 0.20122685 -0.24567793 0.27569941 -0.19347847 0.15264839 -0.17249939 0.10228281 -0.22028904
		 0.37051398 -0.090004504 0.40915883 -0.10824058 0.49439865 -0.23427324 0.46391726
		 -0.27338892 0.34986514 -0.29995158 0.44074756 -0.075439602 -0.012054113 -0.10527131
		 0.056783613 -0.15220705 0.059781659 -0.12519765 0.0020862597 -0.026787294 0.042787846
		 0.0098582562 -0.040933199 -0.11509496 0.59097791 -0.24695124 0.5681628 -0.32605216
		 0.53859472 0.31027576 -0.23736009 0.30567536 -0.23750091 0.30502608 -0.23860776 0.31039926
		 -0.23828107 0.31228963 -0.23726633 0.31266302 -0.23815453 0.27678055 -0.75135219
		 0.27487651 -0.75194561 0.27486482 -0.75318396 0.27667797 -0.7525692 0.31019825 -0.23613584
		 0.31203648 -0.23616143 0.31186751 -0.23479781 0.31016546 -0.23476607 0.27183518 -0.75084335
		 0.27206469 -0.75233537 0.30658454 -0.23602901 0.30678082 -0.23465431 0.31011313 -0.23303908
		 0.31157017 -0.23307534 0.31116611 -0.23085383 0.31005186 -0.23081382 0.30724004 -0.232921
		 0.30789417 -0.2306978 0.27484125 -0.75504196 0.27246156 -0.7542311 0.27296987 -0.75667733
		 0.27480787 -0.75735313 0.27638146 -0.75429833 0.2759932 -0.75652748 0.31075659 -0.22887382
		 0.3100045 -0.22882488 0.30859527 -0.22872539 0.27332607 -0.75846201 0.27477196 -0.75842911
		 0.27565345 -0.75802416 0.31428716 -0.23682106 0.3148188 -0.23767506 0.31377918 -0.23583956
		 0.31351584 -0.23452678 0.31298491 -0.23286963 0.31224421 -0.23073281 0.3114745 -0.22882947
		 0.27726305 -0.75583273 0.27656272 -0.75767899 0.2779353 -0.75378191 0.27842686 -0.7522288
		 0.27867651 -0.75100809 -0.15952411 0.71278119 -0.24596554 0.63522273 -0.32558462
		 0.63366205 -0.19307286 0.77368551 -0.26009375 0.67273688 -0.33200893 0.67469144 -0.016380571
		 -0.041811787 -0.016328221 -0.042836532 -0.015354978 -0.04274467 -0.015278578 -0.041857637
		 -0.015563233 -0.044050895 -0.014902396 -0.043926451 -0.014167747 -0.042475708 -0.01427028
		 -0.041700557 -0.014109601 -0.04389409 -0.015253895 -0.040867217 -0.014528185 -0.040966466
		 -0.016103374 -0.040782966 -0.39406335 0.63278389 -0.40300396 0.68452483 0.49890438
		 -0.45330402 0.49899355 -0.45586509 0.50106144 -0.45472854 0.50104272 -0.45339584
		 0.50289243 -0.45469674 0.50311917 -0.45345211 0.50458544 -0.45463625 0.50456929 -0.45350614
		 0.50419575 -0.45593035 0.50231838 -0.45611396 0.50247842 -0.4573316 0.50419754 -0.45726806
		 0.50418442 -0.45895746 0.50274581 -0.45889527 0.50307876 -0.46093649 0.50413948 -0.46114188
		 0.50339139 -0.46280378 0.50406063 -0.46312267 0.50517642 -0.46129575 0.50470221 -0.46326092
		 0.50559932 -0.45911175 0.50603443 -0.45610607 0.50588816 -0.45742181 0.50625873 -0.45502087
		 0.50606096 -0.45402583 -0.16332486 0.028371541 0.5008598 -0.45622152 -0.16314617
		 0.024491696 -0.17025112 0.019634819 -0.46197179 0.59980643;
createNode polyMapSewMove -n "polyMapSewMove6";
	rename -uid "841B8B6C-46D0-E530-E6B5-DC81FA62B375";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[7]" "e[30]" "e[97]" "e[108]";
createNode polyTweakUV -n "polyTweakUV13";
	rename -uid "37A08E3B-470A-CE22-87CF-D28D6FD6DE59";
	setAttr ".uopa" yes;
	setAttr -s 27 ".uvtk";
	setAttr ".uvtk[198]" -type "float2" -0.4978348 0.44927654 ;
	setAttr ".uvtk[199]" -type "float2" -0.44433033 0.31337166 ;
	setAttr ".uvtk[200]" -type "float2" -0.35484439 0.41377059 ;
	setAttr ".uvtk[201]" -type "float2" -0.38120013 0.48501572 ;
	setAttr ".uvtk[202]" -type "float2" -0.25707734 0.45030585 ;
	setAttr ".uvtk[203]" -type "float2" -0.26857185 0.52149022 ;
	setAttr ".uvtk[204]" -type "float2" -0.16726696 0.48575991 ;
	setAttr ".uvtk[205]" -type "float2" -0.18962801 0.54617226 ;
	setAttr ".uvtk[206]" -type "float2" -0.16358852 0.40881804 ;
	setAttr ".uvtk[207]" -type "float2" -0.26096106 0.36324289 ;
	setAttr ".uvtk[208]" -type "float2" -0.22920233 0.30086467 ;
	setAttr ".uvtk[209]" -type "float2" -0.1380446 0.3369813 ;
	setAttr ".uvtk[210]" -type "float2" -0.10661787 0.24596414 ;
	setAttr ".uvtk[211]" -type "float2" -0.18509102 0.22194028 ;
	setAttr ".uvtk[212]" -type "float2" -0.12837332 0.11860439 ;
	setAttr ".uvtk[213]" -type "float2" -0.067479849 0.12774411 ;
	setAttr ".uvtk[214]" -type "float2" -0.076061249 0.024225593 ;
	setAttr ".uvtk[215]" -type "float2" -0.034038663 0.019820392 ;
	setAttr ".uvtk[216]" -type "float2" -0.0088418722 0.1392009 ;
	setAttr ".uvtk[217]" -type "float2" 0.0030642748 0.024597853 ;
	setAttr ".uvtk[218]" -type "float2" -0.027664423 0.26458621 ;
	setAttr ".uvtk[219]" -type "float2" -0.061459661 0.43435246 ;
	setAttr ".uvtk[220]" -type "float2" -0.044286847 0.36087832 ;
	setAttr ".uvtk[221]" -type "float2" -0.070047617 0.49692208 ;
	setAttr ".uvtk[222]" -type "float2" -0.099601269 0.5466221 ;
	setAttr ".uvtk[224]" -type "float2" -0.33728278 0.32971752 ;
createNode polyMapSewMove -n "polyMapSewMove7";
	rename -uid "B83C3DFD-4FF2-6AD6-EA41-079BEF903A88";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[64]" "e[75]" "e[84]" "e[133]" "e[193]" "e[270]";
createNode polyTweakUV -n "polyTweakUV14";
	rename -uid "3BEA2750-4E91-3752-1155-21B90A723253";
	setAttr ".uopa" yes;
	setAttr -s 221 ".uvtk[0:220]" -type "float2" 0.05879347 0.0089740111 -0.0032664465
		 -0.0010109777 0.010095863 -0.036382452 0.063096099 -0.030440077 0.013037278 0.037202235
		 -0.026939685 0.02372171 -0.015581342 0.013977356 0.034446802 0.028075418 -0.038464125
		 0.01231461 -0.030309802 0.0065282518 -0.024952695 -0.0020868364 -0.019199837 -0.029292818
		 -0.0091003673 0.043027978 -0.043310679 0.031158531 -0.053712383 0.01804479 -0.042322848
		 -0.0012549094 -0.053474609 0.0013323724 -0.037744038 -0.00098767108 -0.036661342
		 0.0018923685 -0.036679935 -0.0062456103 -0.030409075 0.0094861388 -0.028955553 0.0036346158
		 -0.02409778 0.002328428 -0.023259403 0.0082440106 -0.016707301 0.006286006 -0.019117672
		 0.00021746005 -0.014982098 -0.0016801604 -0.013206245 0.0050263838 0.01098971 0.0019336963
		 0.0080422033 -0.0091753928 0.01437663 -0.0111137 0.019556632 -0.00055563945 0.0052319188
		 -0.017292323 0.010039256 -0.019226488 -0.02939412 0.00091760745 -0.025561184 -0.0012170746
		 -0.021460842 -0.0036852243 -0.016885562 -0.0063425857 -0.0053924876 -0.0052915355
		 -0.010677488 -0.0032842578 -0.012279538 -0.0089443475 -0.0065389373 -0.012059112
		 -0.0093429806 0.0046672174 -0.004736478 0.003954737 -0.041229825 0.0044304412 -0.036909986
		 0.0032561787 -0.032467622 0.0042612473 -0.0352185 0.0075935214 -0.031459644 0.0024735068
		 -0.034458425 0.0021829673 0.00086037972 0.0040405528 0.00060872844 -0.0072134496
		 -0.00028271411 -0.015293111 -0.032110121 8.9150635e-005 -0.02991773 -0.0021170909
		 -0.026830954 -0.0047330526 -0.023687394 -0.0073373448 -0.01905586 -0.010996761 -0.014031685
		 -0.014517589 -0.0078711919 -0.01862994 -0.0013608392 -0.02257156 0.0019190273 -0.024591368
		 0.0049280552 -0.026684079 -0.031129219 -0.00852268 -0.034054361 -0.00550324 -0.02822257
		 -0.011288224 -0.024132483 -0.015721779 -0.0036030416 -0.031255495 -5.7422702e-005
		 -0.034134932 -0.040476523 -0.01275347 -0.03526374 -0.010885167 -0.037276518 -0.007643295
		 -0.041497488 -0.0089352066 -0.029803261 -0.019373327 -0.033068527 -0.013974341 -0.038961526
		 -0.016397117 -0.036037318 -0.022689447 -0.0059108501 -0.04199126 -0.010279806 -0.038268466
		 -0.016668245 -0.04511451 -0.011470013 -0.049378626 -0.048379932 -0.015378982 -0.048570257
		 -0.010847117 -0.044652734 -0.027111782 -0.04761159 -0.019508826 0.33650565 0.16368394
		 -0.018069308 -0.058076438 -0.019529009 -0.019714547 -0.013722336 -0.024278292 -0.027864313
		 -0.034528945 -0.032667208 -0.02832165 -0.041412823 -0.034072626 -0.036543649 -0.041573167
		 -0.020832876 -0.029452102 -0.025925074 -0.024076965 -0.03645977 -0.0028755008 -0.038780916
		 -0.0050198254 -0.041817356 -0.005689336 -0.048329275 -0.006632491 -0.0075388579 -0.028552977
		 -0.015130294 -0.034433838 -0.022450399 -0.040469695 -0.030829519 -0.048655957 -0.069283068
		 -0.014088446 -0.058799304 -0.014917196 -0.05930594 -0.0065185786 -0.069735184 -0.0028734938
		 -0.057545476 -0.022341041 -0.067370914 -0.024605358 -0.067032665 -0.035520308 -0.054919291
		 -0.031198883 -0.052348807 -0.020557571 -0.049578868 -0.02897056 -0.053215835 -0.014695273
		 -0.053494085 -0.0081411051 -0.051832747 -0.039426848 -0.066926323 -0.046216171 -0.062601179
		 -0.056471623 -0.048489235 -0.049618348 -0.046367098 -0.036667224 -0.042090576 -0.045534313
		 -0.060066421 0.013220945 -0.059518546 0.0014004215 -0.053342409 -0.0017830448 -0.053125605
		 0.0082020378 -0.070115551 0.007193116 -0.071811721 0.021817604 -0.058643498 -0.068059586
		 -0.043752808 -0.060831029 -0.03646896 -0.054584216 0.1059321 0.10199923 -0.029227572
		 -0.037062466 -0.01643664 -0.08964996 0.13677697 0.0778694 0.16348705 0.16446705 0.20084715
		 0.14899834 0.33185589 0.19969201 0.32995406 0.19909932 0.32994241 0.19786251 0.33175343
		 0.19847646 0.067545436 0.13666141 0.12328271 0.19021456 0.078429699 0.22551104 0.026407685
		 0.17690256 0.3269164 0.2002003 0.32714567 0.19871007 -0.044809163 0.033926964 -0.07926853
		 0.081127308 -0.025601368 0.22765401 0.019677218 0.26868132 -0.056722678 0.32505989
		 -0.092412584 0.2940008 -0.11640544 0.1470608 -0.16203684 0.2340661 0.32991895 0.19600669
		 0.3275421 0.19681659 0.32804981 0.19437332 0.3298856 0.19369827 0.33145723 0.19674942
		 0.33106947 0.19452296 -0.12745565 0.37530106 -0.15245788 0.35443804 -0.19895922 0.31564471
		 0.3284055 0.1925907 0.32984972 0.19262362 0.33073008 0.19302811 0.20987648 0.23521794
		 0.25100288 0.22743353 0.16728494 0.25376198 0.11800721 0.28058976 0.054995149 0.31463528
		 -0.027069645 0.35865107 -0.10260863 0.39766324 0.33233783 0.19521689 0.33163828 0.19337283
		 0.33300924 0.19726528 0.33350018 0.19881648 0.3337495 0.20003574 -0.048584398 -0.081537098
		 -0.041186608 -0.068564497 -0.032281015 -0.064718962 -0.041841183 -0.087825507 -0.037760064
		 -0.072376467 -0.029416624 -0.068939947 0.19792682 0.59975421 0.19423658 0.5397343
		 0.25151423 0.53868192 0.26179788 0.58983535 0.23079455 0.46398568 0.27009761 0.46688527
		 0.32242253 0.54653525 0.32154942 0.59235036 0.31647754 0.46355516 0.26975206 0.64734924
		 0.31136033 0.636796 0.22083849 0.65784329 -0.025130188 -0.061130676 -0.02130354 -0.066100836
		 0.37126023 0.50703704 0.26958048 0.53267187 0.28927314 0.43875897 0.34247029 0.42297953
		 0.26642805 0.36514637 0.31359163 0.33987978 0.24666472 0.29625785 0.29246312 0.28209841
		 0.1996748 0.32847494 0.21685472 0.40699181 0.16504964 0.41669878 0.14531422 0.34616053
		 0.077358022 0.36838657 0.09777905 0.42569011 0.011220372 0.43661407 -0.009291824
		 0.39665094 -0.066804573 0.44464207 -0.08675117 0.42144394 0.33848509 0.16433714 0.23206159
		 0.4669309 0.33854848 0.16296104 0.33602846 0.16123842 -0.024164407 -0.05366341;
createNode polyMapSewMove -n "polyMapSewMove8";
	rename -uid "72E04664-40A3-993C-ED09-E89F78D3F408";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[76]" "e[82]" "e[336]";
createNode polyTweakUV -n "polyTweakUV15";
	rename -uid "8655E668-4ECA-5EF9-1F0D-D48226DEA1D9";
	setAttr ".uopa" yes;
	setAttr -s 217 ".uvtk[0:216]" -type "float2" 0.0037092681 0.0022333786
		 0.0030787641 0.0021319452 0.0032385243 0.0017470608 0.0037874877 0.001831384 0.0033042158
		 0.0025682747 0.002794805 0.0025114832 0.0029162143 0.0023364166 0.0034913241 0.0024388416
		 0.0025301806 0.0024761213 0.0026067568 0.0022754192 0.0027509378 0.0020673645 0.0029572265
		 0.0017547939 0.0030733177 0.0026894247 0.002653525 0.0027441022 0.0024271128 0.0026814453
		 0.0022799699 0.0023961344 0.0022175969 0.0026456828 0.0023322499 0.0022142199 0.0024701634
		 0.0020968369 0.0026523455 0.0017833431 0.0025432378 0.0012026863 0.0023393491 0.0013913563
		 0.0020607824 0.0012773278 0.0022075621 0.0010224718 0.0018929774 0.00088745833 0.0017751901
		 0.0011785669 0.0015594411 0.0011353091 0.0017057657 0.00083504687 0.00074644689 0.00017544281
		 0.00049485528 0.00066747697 0.00018851722 0.00054251641 0.00036343557 -6.2926326e-009
		 0.00034809412 0.0010230399 0.00010899371 0.00094803789 0.0022286421 0.0015526343
		 0.0019488552 0.0014758996 0.001735208 0.0014244659 0.0014498782 0.0013457331 0.0010880941
		 0.00092519372 0.0013371245 0.0010297789 0.0011936252 0.001281418 0.00092325755 0.0012136691
		 0.0015577844 0.00070236641 0.0013619894 0.00060944381 0.0032888004 0.0014787125 0.0030003525
		 0.0015931297 0.002608876 0.0014983756 0.0028525791 0.0013272533 0.0024868501 0.0016138552
		 0.0028285505 0.0016894862 0.0011714576 0.00040348657 0.00081557885 0.00080847315
		 0.00058827491 0.001133212 0.0023048068 0.0017211315 0.0020704458 0.0017102342 0.0018665133
		 0.0016372475 0.0016490164 0.001619123 0.0013811443 0.0015857382 0.0010657645 0.0015322466
		 0.00077035819 0.0014788701 0.00042718215 0.0014236849 0.00023259383 0.0013992429
		 9.9754558e-005 0.0013765714 0.001786502 0.0019794547 0.0020379957 0.0019936378 0.0016292449
		 0.0019285429 0.0013604261 0.0018954372 0.00025034117 0.001794528 2.5627651e-005 0.00179238
		 0.0017976054 0.0024313603 0.0018013646 0.002212774 0.0019812733 0.0021816546 0.0019554768
		 0.0024228992 0.0013927829 0.0022139635 0.0016414173 0.0021621624 0.0016512596 0.0024326753
		 0.0013981395 0.0024671899 4.6841473e-005 0.0022252812 0.00030767653 0.0021923815
		 0.00031379735 0.0025445165 6.9215414e-005 0.0026017884 0.0017914948 0.0026955239
		 0.0019464333 0.002661264 0.0014604109 0.0027865232 0.0016985837 0.0027304927 0.082474336
		 0.041485097 0.0001053925 0.0030106562 0.0010915851 0.0018592068 0.00077542185 0.001826372
		 0.00091291493 0.0025047064 0.0012000104 0.0024865861 0.0012289744 0.0028249447 0.00099098927
		 0.0028996998 0.00086741446 0.0021820373 0.0011408549 0.0021898546 0.0021953792 0.0020297244
		 0.0021482243 0.0022093353 0.0021008085 0.0024411706 0.0020407813 0.0026392057 0.00044564198
		 0.0018189061 0.00055534998 0.0022071237 0.00060825649 0.0025739176 0.00070228527
		 0.0029941604 0.0020160151 0.0031565537 0.0019315029 0.0029678992 0.0020688928 0.0028996947
		 0.0022244502 0.0030798428 0.0017390491 0.0030119889 0.0018018634 0.0032215524 0.0016436727
		 0.0033308959 0.0015369657 0.0030643567 0.0017243875 0.0028509554 0.0014855951 0.0029241452
		 0.0018925634 0.0027956611 0.0020480785 0.0027952814 0.0013356962 0.0031229076 0.0014897659
		 0.0034934636 0.0012460205 0.0035974295 0.0011137009 0.00327954 0.0012977403 0.0030146183
		 0.0010263276 0.0030864712 0.0024611528 0.0028108272 0.0022536786 0.0028760955 0.0021830115
		 0.0027544182 0.0023443326 0.0027392267 0.0024080193 0.0030607206 0.0026745566 0.0029806488
		 0.0010177875 0.0037309763 0.0008306388 0.0034264205 0.00075824175 0.0032086547 0.01255024
		 -0.00092159747 0.012580356 -0.001903728 0.01283194 -0.0020307144 0.012712997 -0.00086449139
		 0.012354665 -0.00046168093 0.012524555 -0.00041170238 0.011507732 0.0048204786 0.011755033
		 0.0048975348 0.011756483 0.0050583296 0.011521025 0.0049785092 0.012157088 -0.00088149664
		 0.012108675 -0.00049543637 0.011745169 -0.0004686703 0.01177904 -0.0008759934 0.012149884
		 0.0047543957 0.012120148 0.0049481494 0.012156937 -0.0016861518 0.011778833 -0.0016691465
		 0.011284873 -0.00096461648 0.01124309 -0.00057155103 0.010606918 -0.00099873357 0.0107742
		 -0.0011288218 0.011222209 -0.0016727297 0.010339036 -0.001341802 0.011759643 0.0052995868
		 0.012068634 0.0051942929 0.012002573 0.00551192 0.011763928 0.0055997027 0.011559605
		 0.0052030305 0.011609952 0.0054924735 0.010616716 -0.0019353325 0.010900694 -0.0010445643
		 0.010934939 -0.00068831706 0.011956302 0.0057436526 0.011768574 0.0057393829 0.011654085
		 0.0056868233 0.012252598 -2.5083938e-007 0.012495406 0.00014886167 0.012027347 -6.5801055e-006
		 0.011657577 -5.9453385e-005 0.011127852 -0.00012773497 0.010256931 -0.00064306718
		 0.010596411 -0.0023984432 0.01144511 0.005402266 0.011536038 0.0056420029 0.011357843
		 0.0051359627 0.01129404 0.0049342993 0.011261535 0.0047757942 0.00068197772 0.0038801716
		 0.00072104251 0.0035508266 0.00058907061 0.0033877394 0.00049880898 0.0039207619
		 0.00059002836 0.0035614197 0.00043603554 0.0034573905 0.014574586 -0.0013070987 0.012492852
		 0.00074700295 0.011409882 -0.00046959534 0.011485767 -0.0014336316 0.010419322 -0.00021538141
		 0.010868924 -0.00094442139 0.012239981 -0.0028971788 0.013963328 -0.0030048513 0.00044017917
		 0.0032366987 0.00031117591 0.0032752214 0.012717198 3.7610128e-008 0.013122214 0.00067248923
		 0.012342745 0.00069574825 0.012206044 0.00047093496 0.012136326 0.00064256811 0.012160766
		 0.00065318646 0.012211596 0.00043266235 0.012367915 0.00053332717 0.011898269 0.00040786027
		 0.012007379 0.00085392216 0.011659275 0.0010140408 0.011585748 0.00045996931 0.010944252
		 0.00037281858 0.010852948 0.0011195553 0.0094938111 0.00052229944 0.0098178294 -0.00021154815
		 0.0084238071 -0.0010769486 0.0088781519 -0.0017742877 0.082217 0.041400168 0.012259306
		 0.00096468977 0.082208753 0.041579079 0.082536399 0.041802995 0.00040673229 0.0030216111;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polyTweakUV15.out" "pCubeShape1.i";
connectAttr "polyTweakUV15.uvtk[0]" "pCubeShape1.uvst[0].uvtw";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyTweak1.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyCube1.out" "polyTweak1.ip";
connectAttr "polyTweak2.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak2.ip";
connectAttr "polyTweak3.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polyExtrudeFace2.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polySplitRing1.ip";
connectAttr "pCubeShape1.wm" "polySplitRing1.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polySplitRing2.ip";
connectAttr "pCubeShape1.wm" "polySplitRing2.mp";
connectAttr "polySplitRing1.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polyExtrudeFace4.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace4.mp";
connectAttr "polySplitRing2.out" "polyTweak6.ip";
connectAttr "polyExtrudeFace4.out" "polySplitRing3.ip";
connectAttr "pCubeShape1.wm" "polySplitRing3.mp";
connectAttr "polySplitRing3.out" "polyExtrudeFace5.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace5.mp";
connectAttr "polyTweak7.out" "polySplitRing4.ip";
connectAttr "pCubeShape1.wm" "polySplitRing4.mp";
connectAttr "polyExtrudeFace5.out" "polyTweak7.ip";
connectAttr "polyTweak8.out" "polySplitRing5.ip";
connectAttr "pCubeShape1.wm" "polySplitRing5.mp";
connectAttr "polySplitRing4.out" "polyTweak8.ip";
connectAttr "polyTweak9.out" "polySplitRing6.ip";
connectAttr "pCubeShape1.wm" "polySplitRing6.mp";
connectAttr "polySplitRing5.out" "polyTweak9.ip";
connectAttr "polyTweak10.out" "polySplitRing7.ip";
connectAttr "pCubeShape1.wm" "polySplitRing7.mp";
connectAttr "polySplitRing6.out" "polyTweak10.ip";
connectAttr "polyTweak11.out" "polySplitRing8.ip";
connectAttr "pCubeShape1.wm" "polySplitRing8.mp";
connectAttr "polySplitRing7.out" "polyTweak11.ip";
connectAttr "polyTweak12.out" "polySplitRing9.ip";
connectAttr "pCubeShape1.wm" "polySplitRing9.mp";
connectAttr "polySplitRing8.out" "polyTweak12.ip";
connectAttr "polyTweak13.out" "polySoftEdge1.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge1.mp";
connectAttr "polySplitRing9.out" "polyTweak13.ip";
connectAttr "polySoftEdge1.out" "polySoftEdge2.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge2.mp";
connectAttr "polySoftEdge2.out" "polySoftEdge3.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge3.mp";
connectAttr "polySoftEdge3.out" "polySplitRing10.ip";
connectAttr "pCubeShape1.wm" "polySplitRing10.mp";
connectAttr "polyTweak14.out" "polySplitRing11.ip";
connectAttr "pCubeShape1.wm" "polySplitRing11.mp";
connectAttr "polySplitRing10.out" "polyTweak14.ip";
connectAttr "polyTweak15.out" "polyMapDel1.ip";
connectAttr "polySplitRing11.out" "polyTweak15.ip";
connectAttr "polyMapDel1.out" "polySplitRing12.ip";
connectAttr "pCubeShape1.wm" "polySplitRing12.mp";
connectAttr "polyTweak16.out" "polySplitRing13.ip";
connectAttr "pCubeShape1.wm" "polySplitRing13.mp";
connectAttr "polySplitRing12.out" "polyTweak16.ip";
connectAttr "polyTweak17.out" "polySplitRing14.ip";
connectAttr "pCubeShape1.wm" "polySplitRing14.mp";
connectAttr "polySplitRing13.out" "polyTweak17.ip";
connectAttr "polyTweak18.out" "polySplitRing15.ip";
connectAttr "pCubeShape1.wm" "polySplitRing15.mp";
connectAttr "polySplitRing14.out" "polyTweak18.ip";
connectAttr "polyTweak19.out" "polySplitRing16.ip";
connectAttr "pCubeShape1.wm" "polySplitRing16.mp";
connectAttr "polySplitRing15.out" "polyTweak19.ip";
connectAttr "polyTweak20.out" "polySplitRing17.ip";
connectAttr "pCubeShape1.wm" "polySplitRing17.mp";
connectAttr "polySplitRing16.out" "polyTweak20.ip";
connectAttr "polySplitRing17.out" "polyTweak21.ip";
connectAttr "polyTweak21.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "polyPlanarProj1.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj1.mp";
connectAttr "polyPlanarProj1.out" "polyTweakUV1.ip";
connectAttr "polyTweakUV1.out" "polyPlanarProj2.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj2.mp";
connectAttr "polyPlanarProj2.out" "polyTweakUV2.ip";
connectAttr "polyTweakUV2.out" "polyPlanarProj3.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj3.mp";
connectAttr "polyPlanarProj3.out" "polyPlanarProj4.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj4.mp";
connectAttr "polyPlanarProj4.out" "polyTweakUV3.ip";
connectAttr "polyTweakUV3.out" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "deleteComponent3.ig";
connectAttr "deleteComponent3.og" "deleteComponent4.ig";
connectAttr "deleteComponent4.og" "polyPlanarProj5.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj5.mp";
connectAttr "polyPlanarProj5.out" "polyTweakUV4.ip";
connectAttr "polyTweakUV4.out" "polyPlanarProj6.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj6.mp";
connectAttr "polyPlanarProj6.out" "polyTweakUV5.ip";
connectAttr "polyTweakUV5.out" "polyPlanarProj7.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj7.mp";
connectAttr "polyPlanarProj7.out" "polyTweakUV6.ip";
connectAttr "polyTweakUV6.out" "polyMapSewMove1.ip";
connectAttr "polyMapSewMove1.out" "polyPlanarProj8.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj8.mp";
connectAttr "polyPlanarProj8.out" "polyTweakUV7.ip";
connectAttr "polyTweakUV7.out" "polyPlanarProj9.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj9.mp";
connectAttr "polyPlanarProj9.out" "polyTweakUV8.ip";
connectAttr "polyTweakUV8.out" "polyMapSewMove2.ip";
connectAttr "polyMapSewMove2.out" "polyMapSewMove3.ip";
connectAttr "polyMapSewMove3.out" "polyTweakUV9.ip";
connectAttr "polyTweakUV9.out" "polyMapSewMove4.ip";
connectAttr "polyMapSewMove4.out" "polyTweakUV10.ip";
connectAttr "polyTweakUV10.out" "polyMapCut1.ip";
connectAttr "polyMapCut1.out" "polyTweakUV11.ip";
connectAttr "polyTweakUV11.out" "polyMapSewMove5.ip";
connectAttr "polyMapSewMove5.out" "polyTweakUV12.ip";
connectAttr "polyTweakUV12.out" "polyMapSewMove6.ip";
connectAttr "polyMapSewMove6.out" "polyTweakUV13.ip";
connectAttr "polyTweakUV13.out" "polyMapSewMove7.ip";
connectAttr "polyMapSewMove7.out" "polyTweakUV14.ip";
connectAttr "polyTweakUV14.out" "polyMapSewMove8.ip";
connectAttr "polyMapSewMove8.out" "polyTweakUV15.ip";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
// End of TallMountain.ma
