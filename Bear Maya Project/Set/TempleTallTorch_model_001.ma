//Maya ASCII 2017ff05 scene
//Name: TempleTallTorch_model_001.ma
//Last modified: Mon, Nov 27, 2017 11:25:11 AM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "education";
createNode transform -s -n "persp";
	rename -uid "172652BB-4EBA-816C-0758-52AA09A0F3A5";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.86921933708802956 4.8247152557376607 8.0213859971048667 ;
	setAttr ".r" -type "double3" -12.338352728987806 1086.1999999998939 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "1A6991F8-48CF-B562-BC48-8590BB963679";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 8.2593429801356013;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.0021815565973486717 3.0598228813413262 4.4544529241297148e-006 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "C58771E2-40F2-CB9E-51AB-FA89FE711B8D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "D57AA540-4608-43D5-0B4C-EB850FA88C55";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "3DEE205D-47B5-2F91-E4E4-369DE7642F9B";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.65624711105678613 0.0035525845988292337 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "C8906404-4592-FB6B-7E61-6AB709839824";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 5.1837519220738137;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "1C4AAC2B-4BD2-2E88-7E94-5CAFD86DE799";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "80ED0B8F-4DB5-1FA8-3015-C39AFDE014BF";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCylinder1";
	rename -uid "821B9FBF-41F8-8E9E-44A4-1CAE2EDC4F82";
	setAttr ".t" -type "double3" 0 2.7845193101505887 0 ;
	setAttr ".s" -type "double3" 0.13111108573680588 0.13111108573680588 0.13111108573680588 ;
createNode mesh -n "pCylinderShape1" -p "pCylinder1";
	rename -uid "8F59B684-4A2C-3541-A606-DE8339CC427C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000002980232239 0.4999997615814209 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "8C8F07C4-468B-63B4-9FE4-7BABDADE3CC5";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "AAFD766E-49FA-1787-F93E-FBA53958FF66";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "FE6564C9-46EF-B81B-6D41-AF8D16FA73BF";
createNode displayLayerManager -n "layerManager";
	rename -uid "D1CAD3FD-40B3-7A89-EA43-2E9157CB11D3";
createNode displayLayer -n "defaultLayer";
	rename -uid "20BE1496-47F3-D96F-69F4-49B3A3668704";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "FA3CEA41-49DE-6BBA-F64A-93B29090E584";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "9E44C771-4E6E-7271-86C3-819E18D6B611";
	setAttr ".g" yes;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "7DDF2D9E-44FD-EF98-88B7-FC8618AD238B";
	setAttr ".sa" 6;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "1A0AFFD6-4C3F-B1D6-A599-3FAF311AADB7";
	setAttr ".ics" -type "componentList" 1 "f[12:17]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 5.4974666 -3.9074148e-009 ;
	setAttr ".rs" 42476;
	setAttr ".lt" -type "double3" -3.3087224502121107e-024 7.0406738144115667e-019 0.0031708375967021141 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.13111108573680588 5.4974665712253579 -0.11354554455755138 ;
	setAttr ".cbx" -type "double3" 0.13111108573680588 5.4974665712253579 0.11354553674272168 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "AC68ED2B-4271-F126-17A5-988C18C2B5ED";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk[0:13]" -type "float3"  0 -19.69197464 0 0 -19.69197464
		 0 0 -19.69197464 0 0 -19.69197464 0 0 -19.69197464 0 0 -19.69197464 0 0 19.69197464
		 0 0 19.69197464 0 0 19.69197464 0 0 19.69197464 0 0 19.69197464 0 0 19.69197464 0
		 0 -19.69197464 0 0 19.69197464 0;
createNode polyTweak -n "polyTweak2";
	rename -uid "47C320B6-40CD-5981-540D-80BF058A9C00";
	setAttr ".uopa" yes;
	setAttr -s 7 ".tk[13:19]" -type "float3"  0.47239998 0.068673462 -0.81822014
		 -0.47239926 0.068673462 -0.81822014 -6.7227172e-017 0.068673462 1.2852027e-008 -0.94479889
		 0.068673462 -1.4272585e-007 -0.47239944 0.068673462 0.81822026 0.47239944 0.068673462
		 0.81822026 0.94479877 0.068673462 4.7107559e-008;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "26263AB2-41B4-B239-0AF5-3696DF4BE64B";
	setAttr ".ics" -type "componentList" 1 "f[12:17]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.5629659e-008 5.5096412 7.8148297e-009 ;
	setAttr ".rs" 56041;
	setAttr ".lt" -type "double3" -6.6174449004242214e-024 -2.7543205265162049e-017 
		0.025406833412591999 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.2549847013997586 5.5096412006237241 -0.22082328310936569 ;
	setAttr ".cbx" -type "double3" 0.25498467014043985 5.5096412006237241 0.22082329873902509 ;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "8AE10152-428C-BBF7-1CC3-FCA2486DC12E";
	setAttr ".ics" -type "componentList" 1 "f[12:17]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.5629659e-008 5.535048 7.8148297e-009 ;
	setAttr ".rs" 41982;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.2549847013997586 5.5350480246929425 -0.22082331436868446 ;
	setAttr ".cbx" -type "double3" 0.25498467014043985 5.5350480246929425 0.22082332999834384 ;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "75DB41FC-4285-15A1-9BCE-B7B9DB27F034";
	setAttr ".ics" -type "componentList" 1 "f[12:17]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.5629659e-008 5.535048 7.8148297e-009 ;
	setAttr ".rs" 55146;
	setAttr ".lt" -type "double3" -6.6174449004242214e-024 -3.4693991690905847e-018 
		-0.015624784805115866 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.23009689142966891 5.5350480246929425 -0.19926982652373043 ;
	setAttr ".cbx" -type "double3" 0.23009686017035014 5.5350480246929425 0.1992698421533898 ;
createNode polyTweak -n "polyTweak3";
	rename -uid "F7AD0AFC-493D-E157-2116-8EB69989822C";
	setAttr ".uopa" yes;
	setAttr -s 7 ".tk[25:31]" -type "float3"  -0.094911225 0 0.16439101
		 0.094911113 0 0.16439104 -1.1635435e-008 0 1.6336047e-008 0.18982229 0 7.4336533e-008
		 0.094911173 0 -0.16439095 -0.094911173 0 -0.16439104 -0.18982229 0 -3.505719e-009;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "1F8A4F1C-48A5-9050-C86E-C3B9517B591D";
	setAttr ".ics" -type "componentList" 1 "f[12:17]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.5629659e-008 5.519423 1.5629659e-008 ;
	setAttr ".rs" 56675;
	setAttr ".lt" -type "double3" 0 -7.3943564788764668e-018 0.060448778917087331 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.091070242846682026 5.5194228666519276 -0.078869121175661708 ;
	setAttr ".cbx" -type "double3" 0.091070211587363253 5.5194228666519276 0.078869152434980466 ;
createNode polyTweak -n "polyTweak4";
	rename -uid "C97FA986-48F7-AFB1-01CB-D488641AD2B5";
	setAttr ".uopa" yes;
	setAttr -s 7 ".tk[31:37]" -type "float3"  -0.53018725 0 0.91831064 0.53018624
		 0 0.91831064 -6.4997124e-008 0 1.5847009e-007 1.060372949 0 5.4487077e-007 0.53018653
		 0 -0.91830981 -0.53018653 0 -0.91831064 -1.060372949 0 1.6430274e-008;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "0C94F708-4A16-B2A3-C0FC-8B8715F6E395";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n"
		+ "            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 866\n            -height 811\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n"
		+ "            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n"
		+ "            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n"
		+ "            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n"
		+ "                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n"
		+ "                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                -outliner \"graphEditor1OutlineEd\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n"
		+ "                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n"
		+ "                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n"
		+ "                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n"
		+ "                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 866\\n    -height 811\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 866\\n    -height 811\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "62DE21C0-4FCC-5681-F04C-F9B0465C4AE0";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyExtrudeFace -n "polyExtrudeFace6";
	rename -uid "2452CEE2-45F8-0E2E-0F4C-62A598785A97";
	setAttr ".ics" -type "componentList" 1 "f[30:35]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -1.5629659e-008 5.535048 7.8148297e-009 ;
	setAttr ".rs" 33759;
	setAttr ".lt" -type "double3" -0.0021815870783603451 -6.2031610859221431e-017 0.062537715738069349 ;
	setAttr ".ls" -type "double3" 0.400000017946891 0.400000017946891 0.400000017946891 ;
	setAttr ".kft" no;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.2549847013997586 5.5350480246929425 -0.22082331436868446 ;
	setAttr ".cbx" -type "double3" 0.25498467014043985 5.5350480246929425 0.22082332999834384 ;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	rename -uid "FB49CEAE-4571-1EE7-EA9E-DD89AFCBDF67";
	setAttr ".ics" -type "componentList" 1 "f[30:35]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.0021816036 5.5975857 -7.3459398e-007 ;
	setAttr ".rs" 54944;
	setAttr ".lt" -type "double3" 4.3368086899420177e-019 8.3580129037370832e-017 0.14582873369701671 ;
	setAttr ".ls" -type "double3" 0.51666665737266593 0.51666665737266593 0.51666665737266593 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.30977675429684121 5.597584917666766 -0.31233954954585802 ;
	setAttr ".cbx" -type "double3" 0.30541354732418757 5.5975864181140667 0.31233808035787602 ;
createNode polyTweak -n "polyTweak5";
	rename -uid "5E35B264-46EF-0D95-BA73-3ABC82B25EBD";
	setAttr ".uopa" yes;
	setAttr -s 24 ".tk[44:67]" -type "float3"  0.1769979 -1.706693e-006 -0.74502319
		 -0.17699689 -1.706693e-006 -0.74502307 -0.159721 -1.7070862e-006 -0.71510071 0.15972215
		 -1.7070862e-006 -0.71510077 -0.5567109 -7.9876479e-009 -0.52579325 -0.73370862 -1.2018646e-008
		 -0.21922483 -0.699157 -1.2018607e-008 -0.21922477 -0.53943384 -8.3810932e-009 -0.49587053
		 -0.73370892 1.664055e-006 0.21922845 -0.55671042 1.6600243e-006 0.52579761 -0.5394364
		 1.6604182e-006 0.49587524 -0.69915742 1.664055e-006 0.21922839 -0.17699745 -2.4696838e-008
		 0.74502367 0.17699756 -2.4696824e-008 0.74502313 0.15972182 -2.4303638e-008 0.71510124
		 -0.15972182 -2.4304072e-008 0.71510053 0.55671084 -1.7234026e-006 0.52579898 0.73370844
		 -1.7193722e-006 0.21923059 0.699157 -1.7193722e-006 0.21923059 0.53943563 -1.7230094e-006
		 0.49587768 0.73370868 1.6698204e-006 -0.21922381 0.55671132 1.6738513e-006 -0.52579248
		 0.5394364 1.6734582e-006 -0.49587047 0.69915712 1.6698204e-006 -0.21922381;
createNode polyExtrudeFace -n "polyExtrudeFace8";
	rename -uid "CE717D76-4B77-2710-967D-879EFE2E6944";
	setAttr ".ics" -type "componentList" 1 "f[30:35]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.0021815721 5.7434139 4.4544531e-006 ;
	setAttr ".rs" 36700;
	setAttr ".lt" -type "double3" 0 9.9556864488481445e-017 0.2022132881904786 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.28934100591299705 5.7434126406039763 -0.30944412388585402 ;
	setAttr ".cbx" -type "double3" 0.28497786145898096 5.7434156414985775 0.30945303279170228 ;
createNode polyExtrudeFace -n "polyExtrudeFace9";
	rename -uid "E576BCD7-4B4F-5C9E-7BD8-DF838354355D";
	setAttr ".ics" -type "componentList" 1 "f[30:35]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.0021815721 5.9456272 1.2441209e-005 ;
	setAttr ".rs" 56919;
	setAttr ".lt" -type "double3" 0 4.6833722203111147e-017 0.26400496996263018 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.28934100591299705 5.9456254225818936 -0.30943374579202376 ;
	setAttr ".cbx" -type "double3" 0.28497786145898096 5.9456289236255948 0.30945862820976133 ;
createNode polyExtrudeFace -n "polyExtrudeFace10";
	rename -uid "FEC478C3-446D-C56F-CD98-33B69236D251";
	setAttr ".ics" -type "componentList" 1 "f[30:35]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.0021815721 6.2096319 2.0740557e-005 ;
	setAttr ".rs" 54677;
	setAttr ".lt" -type "double3" 4.3368086899420177e-019 -5.0328156627008763e-017 0.095820008030388246 ;
	setAttr ".ls" -type "double3" 2.3000000575750894 2.3000000575750894 2.3000000575750894 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.28934100591299705 6.2096301254488075 -0.30942130458315492 ;
	setAttr ".cbx" -type "double3" 0.28497786145898096 6.2096336264925096 0.30946278569915719 ;
createNode polyExtrudeFace -n "polyExtrudeFace11";
	rename -uid "12A0D130-4481-2D64-EE61-43AC04F1F1DD";
	setAttr ".ics" -type "componentList" 1 "f[6:11]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 0.071569547 -3.9074148e-009 ;
	setAttr ".rs" 55818;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.13111108573680588 0.0715695483303187 -0.11354554455755138 ;
	setAttr ".cbx" -type "double3" 0.13111108573680588 0.0715695483303187 0.11354553674272168 ;
createNode polyTweak -n "polyTweak6";
	rename -uid "E4BA70F9-4533-46CB-E1FB-26BBAEB8D123";
	setAttr ".uopa" yes;
	setAttr -s 71 ".tk";
	setAttr ".tk[68]" -type "float3" 0.087924488 -4.0118143e-006 -0.70968157 ;
	setAttr ".tk[69]" -type "float3" -0.087923966 -4.0118143e-006 -0.70968157 ;
	setAttr ".tk[70]" -type "float3" -0.079342112 -4.0118143e-006 -0.69481754 ;
	setAttr ".tk[71]" -type "float3" 0.079342626 -4.0118143e-006 -0.69481754 ;
	setAttr ".tk[72]" -type "float3" -0.57063973 -4.0118143e-006 -0.43099654 ;
	setAttr ".tk[73]" -type "float3" -0.65856421 -4.0118143e-006 -0.27870736 ;
	setAttr ".tk[74]" -type "float3" -0.64140052 -4.0118143e-006 -0.27870736 ;
	setAttr ".tk[75]" -type "float3" -0.56205761 -4.0118143e-006 -0.41613233 ;
	setAttr ".tk[76]" -type "float3" -0.65856433 -1.7005023e-006 0.27868646 ;
	setAttr ".tk[77]" -type "float3" -0.57063985 -1.7005023e-006 0.43097591 ;
	setAttr ".tk[78]" -type "float3" -0.56205827 -1.7005023e-006 0.41611183 ;
	setAttr ".tk[79]" -type "float3" -0.64140075 -1.7005023e-006 0.27868646 ;
	setAttr ".tk[80]" -type "float3" -0.087924317 -4.0118143e-006 0.70968157 ;
	setAttr ".tk[81]" -type "float3" 0.087924093 -4.0118143e-006 0.70968157 ;
	setAttr ".tk[82]" -type "float3" 0.079342291 -4.0118143e-006 0.6948176 ;
	setAttr ".tk[83]" -type "float3" -0.079342462 -4.0118143e-006 0.69481754 ;
	setAttr ".tk[84]" -type "float3" 0.57063979 -4.0118143e-006 0.43097353 ;
	setAttr ".tk[85]" -type "float3" 0.65856427 -4.0118143e-006 0.27868432 ;
	setAttr ".tk[86]" -type "float3" 0.64140052 -4.0118143e-006 0.27868432 ;
	setAttr ".tk[87]" -type "float3" 0.56205785 -4.0118143e-006 0.41610953 ;
	setAttr ".tk[88]" -type "float3" 0.65856433 4.0118143e-006 -0.27870291 ;
	setAttr ".tk[89]" -type "float3" 0.57064009 4.0118143e-006 -0.43099207 ;
	setAttr ".tk[90]" -type "float3" 0.56205827 4.0118143e-006 -0.41612801 ;
	setAttr ".tk[91]" -type "float3" 0.64140052 4.0118143e-006 -0.27870291 ;
	setAttr ".tk[92]" -type "float3" 0.063745521 -2.8814152e-006 -0.51451731 ;
	setAttr ".tk[93]" -type "float3" -0.063745126 -2.8814152e-006 -0.51451731 ;
	setAttr ".tk[94]" -type "float3" -0.057523262 -2.8814152e-006 -0.50374079 ;
	setAttr ".tk[95]" -type "float3" 0.057523664 -2.8814152e-006 -0.50374079 ;
	setAttr ".tk[96]" -type "float3" -0.41371545 -2.8814152e-006 -0.3124845 ;
	setAttr ".tk[97]" -type "float3" -0.47746101 -2.8814152e-006 -0.20207432 ;
	setAttr ".tk[98]" -type "float3" -0.46501726 -2.8814152e-006 -0.20207432 ;
	setAttr ".tk[99]" -type "float3" -0.40749347 -2.8814152e-006 -0.30170792 ;
	setAttr ".tk[100]" -type "float3" -0.47746104 -1.1803886e-006 0.20203537 ;
	setAttr ".tk[101]" -type "float3" -0.41371551 -1.1803886e-006 0.31244558 ;
	setAttr ".tk[102]" -type "float3" -0.40749404 -1.1803886e-006 0.30166903 ;
	setAttr ".tk[103]" -type "float3" -0.46501735 -1.1803886e-006 0.20203537 ;
	setAttr ".tk[104]" -type "float3" -0.063745417 -2.8814152e-006 0.51451731 ;
	setAttr ".tk[105]" -type "float3" 0.063745238 -2.8814152e-006 0.51451731 ;
	setAttr ".tk[106]" -type "float3" 0.057523385 -2.8814152e-006 0.50374085 ;
	setAttr ".tk[107]" -type "float3" -0.057523545 -2.8814152e-006 0.50374079 ;
	setAttr ".tk[108]" -type "float3" 0.41371548 -2.8814152e-006 0.31243861 ;
	setAttr ".tk[109]" -type "float3" 0.47746101 -2.8814152e-006 0.2020285 ;
	setAttr ".tk[110]" -type "float3" 0.46501729 -2.8814152e-006 0.2020285 ;
	setAttr ".tk[111]" -type "float3" 0.40749365 -2.8814152e-006 0.30166212 ;
	setAttr ".tk[112]" -type "float3" 0.47746104 2.8814152e-006 -0.20207098 ;
	setAttr ".tk[113]" -type "float3" 0.41371584 2.8814152e-006 -0.31248114 ;
	setAttr ".tk[114]" -type "float3" 0.4074941 2.8814152e-006 -0.30170462 ;
	setAttr ".tk[115]" -type "float3" 0.46501729 2.8814152e-006 -0.20207098 ;
	setAttr ".tk[140]" -type "float3" -7.4505806e-009 0 0 ;
	setAttr ".tk[142]" -type "float3" 0 0 5.9604645e-008 ;
	setAttr ".tk[143]" -type "float3" -7.4505806e-009 0 5.9604645e-008 ;
	setAttr ".tk[144]" -type "float3" 5.9604645e-008 0 -5.9604645e-008 ;
	setAttr ".tk[145]" -type "float3" -5.9604645e-008 0 5.9604645e-008 ;
	setAttr ".tk[146]" -type "float3" 0 0 5.9604645e-008 ;
	setAttr ".tk[147]" -type "float3" 1.1920929e-007 0 5.9604645e-008 ;
	setAttr ".tk[148]" -type "float3" 5.9604645e-008 -5.6843419e-014 -2.9802322e-008 ;
	setAttr ".tk[149]" -type "float3" -1.7881393e-007 -5.6843419e-014 5.9604645e-008 ;
	setAttr ".tk[150]" -type "float3" 0 -5.6843419e-014 -8.9406967e-008 ;
	setAttr ".tk[151]" -type "float3" 1.7881393e-007 -5.6843419e-014 -2.9802322e-008 ;
	setAttr ".tk[152]" -type "float3" -7.4505806e-009 0 0 ;
	setAttr ".tk[153]" -type "float3" 1.4901161e-008 0 0 ;
	setAttr ".tk[154]" -type "float3" 0 0 1.7881393e-007 ;
	setAttr ".tk[155]" -type "float3" -7.4505806e-009 0 1.7881393e-007 ;
	setAttr ".tk[156]" -type "float3" -1.1920929e-007 4.5474735e-013 -5.9604645e-008 ;
	setAttr ".tk[157]" -type "float3" 0 4.5474735e-013 0 ;
	setAttr ".tk[158]" -type "float3" 0 4.5474735e-013 0 ;
	setAttr ".tk[159]" -type "float3" 5.9604645e-008 4.5474735e-013 8.9406967e-008 ;
	setAttr ".tk[160]" -type "float3" -5.9604645e-008 -4.5474735e-013 8.9406967e-008 ;
	setAttr ".tk[161]" -type "float3" 1.1920929e-007 -4.5474735e-013 -5.9604645e-008 ;
	setAttr ".tk[162]" -type "float3" -5.9604645e-008 -4.5474735e-013 8.9406967e-008 ;
	setAttr ".tk[163]" -type "float3" 0 -4.5474735e-013 8.9406967e-008 ;
createNode polyExtrudeFace -n "polyExtrudeFace12";
	rename -uid "39777027-429A-BD3B-8AD1-DAB04C85583B";
	setAttr ".ics" -type "componentList" 1 "f[6:11]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 0.055119645 -7.8148297e-009 ;
	setAttr ".rs" 57968;
	setAttr ".lt" -type "double3" 0 2.04253789598698e-017 0.11379241928559129 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.20714418958777805 0.055119644422986891 -0.17939216638747205 ;
	setAttr ".cbx" -type "double3" 0.20714418958777805 0.055119644422986891 0.17939215075781267 ;
createNode polyTweak -n "polyTweak7";
	rename -uid "4A8377A0-4611-06F9-34BB-69824FCD150E";
	setAttr ".uopa" yes;
	setAttr -s 7 ".tk[163:169]" -type "float3"  0.28995699 -0.12546255 -0.50221992
		 -0.28995675 -0.12546255 -0.50222003 0 -0.12546255 1.7282774e-008 -0.57991368 -0.12546255
		 -6.9131097e-008 -0.28995687 -0.12546255 0.50221997 0.28995681 -0.12546255 0.50222003
		 0.57991368 -0.12546255 1.7282774e-008;
createNode polyMapDel -n "polyMapDel1";
	rename -uid "6397D155-4CC5-26FD-9FBE-9A9F3806F58D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:179]";
createNode polyTweak -n "polyTweak8";
	rename -uid "724ADD10-4010-F588-6725-84A3C7116446";
	setAttr ".uopa" yes;
	setAttr -s 24 ".tk";
	setAttr ".tk[0]" -type "float3" -0.11331169 0.23732102 0.19626151 ;
	setAttr ".tk[1]" -type "float3" 0.1133116 0.23732102 0.19626154 ;
	setAttr ".tk[2]" -type "float3" 0.22662328 0.23732102 2.70156e-008 ;
	setAttr ".tk[3]" -type "float3" 0.11331168 0.23732102 -0.19626153 ;
	setAttr ".tk[4]" -type "float3" -0.11331163 0.23732102 -0.19626154 ;
	setAttr ".tk[5]" -type "float3" -0.22662328 0.23732102 -6.753901e-009 ;
	setAttr ".tk[6]" -type "float3" -0.11331169 -0.23732102 0.19626151 ;
	setAttr ".tk[7]" -type "float3" 0.1133116 -0.23732102 0.19626154 ;
	setAttr ".tk[8]" -type "float3" 0.22662328 -0.23732102 2.70156e-008 ;
	setAttr ".tk[9]" -type "float3" 0.11331168 -0.23732102 -0.19626153 ;
	setAttr ".tk[10]" -type "float3" -0.11331163 -0.23732102 -0.19626154 ;
	setAttr ".tk[11]" -type "float3" -0.22662328 -0.23732102 -6.753901e-009 ;
	setAttr ".tk[163]" -type "float3" 0.29822302 -0.64202631 -0.51653713 ;
	setAttr ".tk[164]" -type "float3" -0.29822278 -0.64202631 -0.51653719 ;
	setAttr ".tk[165]" -type "float3" -0.59644574 -0.64202631 -5.9850954e-008 ;
	setAttr ".tk[166]" -type "float3" -0.29822296 -0.64202631 0.51653713 ;
	setAttr ".tk[167]" -type "float3" 0.29822284 -0.64202631 0.51653719 ;
	setAttr ".tk[168]" -type "float3" 0.59644574 -0.64202631 2.902638e-008 ;
	setAttr ".tk[169]" -type "float3" 0.29822302 -0.96967739 -0.51653713 ;
	setAttr ".tk[170]" -type "float3" -0.29822278 -0.96967739 -0.51653719 ;
	setAttr ".tk[172]" -type "float3" -0.59644574 -0.96967739 -5.9850954e-008 ;
	setAttr ".tk[173]" -type "float3" -0.29822296 -0.96967739 0.51653713 ;
	setAttr ".tk[174]" -type "float3" 0.29822284 -0.96967739 0.51653719 ;
	setAttr ".tk[175]" -type "float3" 0.59644574 -0.96967739 2.902638e-008 ;
createNode polyCylProj -n "polyCylProj1";
	rename -uid "14263F27-45B7-28BD-2405-6290DC619594";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 59 "f[0:11]" "f[24:29]" "f[42:48]" "f[50]" "f[52]" "f[54]" "f[56]" "f[58]" "f[60]" "f[62]" "f[64]" "f[66]" "f[68]" "f[70:72]" "f[74]" "f[76]" "f[78]" "f[80]" "f[82]" "f[84]" "f[86]" "f[88]" "f[90]" "f[92]" "f[94:96]" "f[98]" "f[100]" "f[102]" "f[104]" "f[106]" "f[108]" "f[110]" "f[112]" "f[114]" "f[116]" "f[118:120]" "f[122]" "f[124]" "f[126]" "f[128]" "f[130]" "f[132]" "f[134]" "f[136]" "f[138]" "f[140]" "f[142:144]" "f[146]" "f[148]" "f[150]" "f[152]" "f[154]" "f[156]" "f[158]" "f[160]" "f[162]" "f[164]" "f[166:167]" "f[174:179]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.0021815448999404907 3.0598229169845581 4.4554471969604492e-006 ;
	setAttr ".ps" -type "double2" 180 0.31468366170612827 ;
	setAttr ".r" 0.80499139428138733;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "E011C81E-423A-DA26-2BDB-53A1AAC7A14C";
	setAttr ".uopa" yes;
	setAttr -s 185 ".uvtk[0:184]" -type "float2" 0.34039766 8.90045643 0.3403981
		 -7.14943361 -1.23238492 -7.14943361 0.019882338 8.90045643 0.019882604 -7.14943361
		 -0.300648 8.90045643 -0.30064785 -7.14943409 -0.61506259 8.90045643 -0.61506248 -7.14943409
		 -0.92373049 8.90045643 -0.92373031 -7.14943361 0.7911855 10.10554123 -0.88552231
		 9.82761478 -0.54580122 9.4415369 0.45573908 9.95741367 0.13454767 9.86900902 -0.19197269
		 9.8397007 -0.53672469 9.82639503 -1.1470741 -8.20999146 0.51951253 -8.095637321 0.52191222
		 -8.17621708 -1.14344907 -8.29074001 0.18552037 -8.11970711 0.18791626 -8.20033169
		 -0.14939612 -8.14364719 -0.14703833 -8.22436047 -0.48274627 -8.16692352 -0.48048311
		 -8.24772739 -0.8146717 -8.1889782 -0.81252444 -8.26984882 -0.78967446 -8.29601669
		 0.87718135 -8.2115345 0.88835943 -8.40360928 -0.77846372 -8.48810387 0.53715044 -8.22842884
		 0.5483923 -8.42051983 0.19713108 -8.24538708 0.20835607 -8.43748379 -0.13605928 -8.26222801
		 -0.12486206 -8.45431328 -0.46286982 -8.27912712 -0.45166633 -8.47120285 -1.37109709
		 -8.51865864 0.75269264 -8.13371086 -1.21727777 -8.46668339 0.29062963 -8.39335728
		 0.4380995 -8.37932777 0.33839369 -8.18081951 0.61203504 -8.30165577 0.42654702 -8.32907391
		 -0.041814417 -8.41764927 0.10624624 -8.40365314 -0.059030067 -8.21836567 0.20953879
		 -8.35517311 0.033873379 -8.37701988 -0.37552157 -8.44143391 -0.22695543 -8.42782593
		 -0.44179443 -8.2482729 -0.17837341 -8.39762402 -0.34705669 -8.4150219 -0.71040243
		 -8.46431255 -0.56128657 -8.45144463 -0.81257075 -8.26829147 -0.55425006 -8.4307642
		 -0.71745521 -8.44241619 -1.044796348 -8.48453522 -0.89590228 -8.47262955 -0.90022677
		 -8.47134876 -1.03406775 -8.46342468 -1.31163776 -8.97879601 -1.23706591 -8.95025635
		 0.34168908 -8.85324097 0.41944307 -8.84382439 0.5352484 -8.71044445 0.43223408 -8.72887135
		 0.0093072634 -8.87827301 0.087490901 -8.86885166 0.1415911 -8.77579212 0.045180075
		 -8.79046345 -0.32476422 -8.90269375 -0.24624522 -8.89354229 -0.23939647 -8.82656097
		 -0.33112869 -8.8382349 -0.66044658 -8.9263382 -0.58153069 -8.91770554 -0.6069392
		 -8.86585617 -0.69499612 -8.8736887 -0.99780041 -8.94678974 -0.91838628 -8.94023705
		 -0.921345 -8.93959618 -0.99125147 -8.93424511 -1.28260541 -9.62122726 -1.22346818
		 -9.6179533 0.36090764 -9.49402905 0.43868899 -9.48522091 0.49255392 -9.2842865 0.39149576
		 -9.30153847 0.028574504 -9.51997948 0.10677116 -9.51114082 0.10762334 -9.36447239
		 0.012749321 -9.37819862 -0.30605778 -9.54508591 -0.22754256 -9.5365181 -0.26641279
		 -9.42572021 -0.35691598 -9.43663025 -0.64282608 -9.5695858 -0.56391168 -9.56151867
		 -0.62516379 -9.47319984 -0.7122283 -9.48052979 -0.98579836 -9.59125137 -0.90625221
		 -9.58613873 -0.90907818 -9.58568287 -0.98000455 -9.58120918 -1.24377894 -10.45942211
		 -1.20480216 -10.49016762 0.38653067 -10.33016682 0.46443471 -10.32294178 0.43427268
		 -10.032194138 0.338346 -10.046357155 0.054357331 -10.35732746 0.13259855 -10.35006523
		 0.061298773 -10.13204098 -0.029544737 -10.14330578 -0.28106213 -10.38340282 -0.20255315
		 -10.37637234 -0.30323809 -10.20713615 -0.39049706 -10.21608162 -0.61926758 -10.40912819
		 -0.54042125 -10.4025116 -0.65014219 -10.2654047 -0.73457748 -10.27142239 -0.97162658
		 -10.43252945 -0.89184862 -10.42884445 -0.8947472 -10.42844963 -0.96655065 -10.42501545
		 -1.27824938 -10.77117348 -1.10337663 -10.74604321 -1.15740705 -10.80948639 -1.30350387
		 -10.79911423 0.34788841 -10.63864708 0.52114618 -10.62203407 0.47665954 -10.29484749
		 0.25668949 -10.32742405 0.015541875 -10.66625404 0.18957742 -10.64954567 0.10440126
		 -10.40362644 -0.10387804 -10.42952442 -0.32034564 -10.69251823 -0.14568368 -10.67634678
		 -0.25925174 -10.48519802 -0.45927641 -10.50576401 -0.65928882 -10.71839046 -0.48384976
		 -10.70317364 -0.60393518 -10.54928398 -0.79746234 -10.5631218 -1.017258525 -10.74091148
		 -0.83888429 -10.73449135 -0.84331787 -10.7339344 -1.0094720125 -10.72748375 0.8682555
		 9.5995903 0.51855558 9.45929241 0.16885856 9.37088776 -0.17871626 9.33436394 -0.53293931
		 9.3178196 -1.23238504 8.90045643 -1.54677188 8.90045643 -1.54677176 -7.14943361 -1.55807018
		 9.86738205 -1.48062074 -8.2463398 -1.122805 -8.31289196 -1.11165297 -8.50496674 -1.47699833
		 -8.3271122 -1.22245145 -8.49749565 -1.43656337 -8.24163151 -1.13184726 -8.25648308
		 -1.34053326 -8.45802879 -1.23303473 -8.96458244 -1.29827499 -8.94442177 -1.20398927
		 -9.6079216 -1.28539979 -9.61247253 -1.16517901 -10.44849777 -1.26860988 -10.4856596
		 -1.24151123 9.33463097 -0.88922447 9.3194437 -1.23317337 9.84317684;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "4BE4DEE4-4F5F-C2F6-C258-D58F2DEABC52";
	setAttr ".dc" -type "componentList" 1 "f[6:11]";
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "9E80B9EE-40AC-5D45-61CE-EF9BF78A7073";
	setAttr ".uopa" yes;
	setAttr -s 183 ".uvtk[0:182]" -type "float2" -0.00015526431 -0.0012170164
		 -0.00015526378 -2.3154921e-007 -4.0247076e-005 -2.3154921e-007 -0.00013226028 -0.0012170164
		 -0.00013225937 -2.3154921e-007 -0.00010925836 -0.0012170164 -0.00010925767 -2.0588595e-007
		 -8.6249725e-005 -0.0012170164 -8.625241e-005 -2.0588595e-007 -6.3251209e-005 -0.0012170164
		 -6.324923e-005 -2.3154921e-007 -0.033410471 0.028792927 -0.02371178 0.04030218 -0.033410441
		 0.12081867 -0.04823193 0.15548311 -0.055357978 0.13212681 -0.041636385 0.093389779
		 -0.00047844579 -0.0011292483 -0.00047856168 -0.0009893477 -0.00047563345 -0.0009891392
		 -0.00047400288 -0.0011292483 -0.00047647741 -0.0010187987 -0.00047355436 -0.0010186436
		 -0.00047551424 -0.0010480811 -0.0004726293 -0.0010480396 -0.00047556224 -0.001076553
		 -0.00047278983 -0.0010766302 -0.00047670925 -0.001103536 -0.00047407416 -0.0011036935
		 -0.077794939 -0.0011963351 -0.077794947 -0.0010924786 -0.077781208 -0.0010924573
		 -0.077781178 -0.0011963351 -0.077794969 -0.0011132433 -0.0777812 -0.0011132469 -0.077794962
		 -0.0011340915 -0.077781208 -0.0011341061 -0.077794977 -0.0011547945 -0.0777812 -0.0011547927
		 -0.077794977 -0.0011755734 -0.0777812 -0.0011755592 -0.00045490073 -0.0011649653
		 -0.0001935925 -0.00093712693 -0.00044314863 -0.0011013804 -0.00046096314 -0.0010116578
		 -0.0004622007 -0.00099449442 -0.00028943282 -0.00099477009 -0.0002441429 -0.00089946884
		 -0.00029993319 -0.00093301293 -0.00045987347 -0.0010413727 -0.00046040493 -0.0010242573
		 -0.00036463217 -0.001040697 -0.00032876199 -0.00096494477 -0.00037253887 -0.00099165854
		 -0.00046030901 -0.0010704788 -0.00046022036 -0.0010538209 -0.00042513711 -0.001077295
		 -0.00039555016 -0.0010168883 -0.0004307515 -0.0010381658 -0.00046221211 -0.0010984764
		 -0.00046146417 -0.0010827377 -0.00047413423 -0.0011017842 -0.0004475899 -0.0010574358
		 -0.00047609344 -0.0010716831 -0.00046349908 -0.0011232111 -0.0004630113 -0.0011086367
		 -0.00046302821 -0.0011070712 -0.00045563566 -0.0010973858 -0.00042500225 -0.0011609604
		 -0.00042605624 -0.0011260431 -0.00044132213 -0.001007351 -0.00044219298 -0.00099583052
		 -0.00029678244 -0.00083263137 -0.00033426998 -0.00085519231 -0.00044015149 -0.001037976
		 -0.00044050757 -0.0010264454 -0.00037059755 -0.00091259112 -0.00039997589 -0.00093053578
		 -0.00044106567 -0.0010678631 -0.0004410175 -0.0010566518 -0.00042890763 -0.00097472023
		 -0.00045256954 -0.00098899275 -0.00044392465 -0.0010967883 -0.00044338629 -0.0010862224
		 -0.00047074287 -0.0010227934 -0.00048991229 -0.0010323611 -0.00044883584 -0.0011217904
		 -0.00044769779 -0.0011137659 -0.00044759375 -0.0011129857 -0.000444562 -0.0011064375
		 -0.00038947255 -0.0011607602 -0.00040941717 -0.0011567587 -0.00041780833 -0.0010051425
		 -0.00041864868 -0.00099437509 -0.00034902952 -0.00074853608 -0.00038410866 -0.00076965243
		 -0.00041657142 -0.0010368883 -0.00041692445 -0.0010260814 -0.00041215727 -0.0008466522
		 -0.00043967654 -0.00086343999 -0.00041816902 -0.0010676116 -0.000418101 -0.0010571274
		 -0.00046194074 -0.00092156191 -0.00048411472 -0.00093491981 -0.00042234806 -0.0010975893
		 -0.00042181346 -0.0010877238 -0.00049302517 -0.00097965519 -0.00051098544 -0.00098863058
		 -0.00043417979 -0.0011240742 -0.00043284293 -0.0011178175 -0.00043259145 -0.0011172573
		 -0.00043080348 -0.001111789 -0.00034196637 -0.0011598379 -0.00038658481 -0.0011974535
		 -0.00038645041 -0.0010016976 -0.0003871636 -0.0009928681 -0.00042032488 -0.00063714711
		 -0.00044913639 -0.00065450586 -0.00038504941 -0.0010349196 -0.00038531481 -0.0010260469
		 -0.00046883439 -0.00075930142 -0.00049142208 -0.0007730971 -0.0003875902 -0.001066829
		 -0.00038752411 -0.0010582341 -0.00050700875 -0.00085117028 -0.00052519504 -0.00086210488
		 -0.00039353923 -0.0010983107 -0.00039309487 -0.0010902185 -0.00052360102 -0.00092246826
		 -0.00053834898 -0.00092982355 -0.00041681618 -0.001126923 -0.00041521757 -0.0011224096
		 -0.00041504923 -0.0011219318 -0.00041433671 -0.0011177252 -0.00032534712 -0.00116872
		 -0.0003249814 -0.0011379663 -0.00038574764 -0.0012155918 -0.00036160916 -0.0012029011
		 -0.00037495466 -0.0010065732 -0.0003765588 -0.00098624115 -0.00042562591 -0.00058599142
		 -0.00049188407 -0.00062576117 -0.00037371824 -0.0010403518 -0.00037439595 -0.0010199026
		 -0.00047324959 -0.00071897713 -0.00052519928 -0.00075071567 -0.00037686247 -0.001072484
		 -0.0003767513 -0.0010527016 -0.00051033363 -0.00081885129 -0.00055219029 -0.00084401446
		 -0.0003837249 -0.0011041402 -0.00038265961 -0.0010855292 -0.00052421918 -0.00089724996
		 -0.00055813091 -0.000914164 -0.00041386034 -0.0011316731 -0.00040920917 -0.0011238173
		 -0.00040928405 -0.0011231304 -0.00040969058 -0.0011152449 -0.079703808 0.036236897
		 -0.066161916 0.12054344 -0.053312529 0.15507017 -0.039882839 0.13924307 -0.016891846
		 0.10393687 -4.0240651e-005 -0.0012170164 -1.723841e-005 -0.0012170164 -1.724482e-005
		 -2.3154921e-007 -0.00047872993 -0.0011737255 -0.077794924 -0.0012170821 -0.077781208
		 -0.0012170665 -0.00047430498 -0.0011737539 -0.00045469648 -0.0011390721 -0.00042515277
		 -0.001069161 -0.00046011904 -0.001087333 -0.00042278264 -0.0010907886 -0.00042483077
		 -0.0011435704 -0.00041236638 -0.0011189078 -0.00038929662 -0.0011444892 -0.00039661198
		 -0.0011500546 -0.00034181564 -0.0011464686 -0.00037607225 -0.0011919362 0.027620429
		 -0.016338909 0.0084153479 0.050498907 -0.0090862531 -0.02690473;
createNode polyMapCut -n "polyMapCut1";
	rename -uid "09A34081-4F6A-B684-23BF-C691F85C9E77";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[45:46]" "e[48]" "e[57]" "e[95]" "e[105]";
createNode polyMapCut -n "polyMapCut2";
	rename -uid "45A21B0A-40B9-4473-89E6-43AC78C93D2B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[47:48]" "e[50]" "e[59]" "e[103]" "e[113]";
createNode polyMapCut -n "polyMapCut3";
	rename -uid "E8A0A937-41E5-88EF-4DED-40BBD303EC9E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[49:50]" "e[52]" "e[61]" "e[111]" "e[121]";
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "A716F324-41A3-F710-6D3A-FBB29DB50F0D";
	setAttr ".uopa" yes;
	setAttr -s 187 ".uvtk[0:186]" -type "float2" 0.00036366889 -9.4980282e-007
		 0.00036366886 1.5722591e-008 0.00036375187 1.5722591e-008 0.00036368592 -9.4980282e-007
		 0.00036368612 1.5722591e-008 0.00036370169 -9.4980282e-007 0.00036370358 -3.1394023e-008
		 0.00036371819 -9.4980282e-007 0.00036371878 -3.1394023e-008 0.00036373659 -9.4980282e-007
		 0.00036373772 1.5722591e-008 -0.064865403 5.2524263e-007 -0.064865373 9.5042303e-008
		 -0.064865403 4.287804e-007 -0.064865373 3.6160154e-007 -0.064865373 2.590819e-007
		 -0.064865373 1.7601964e-007 -0.06449239 0.058341667 -0.064581193 0.15433675 -0.062566765
		 0.15447845 -0.06144933 0.058341667 -0.06314832 0.13413122 -0.061137129 0.13423534
		 -0.062486555 0.11403462 -0.060507305 0.11406425 -0.062513009 0.094495222 -0.060613193
		 0.094448783 -0.063300185 0.075981341 -0.061497666 0.075878859 0.18219815 1.3024806e-008
		 0.18219815 7.1180764e-008 0.18219818 7.2402088e-008 0.18219815 1.3024806e-008 0.18219814
		 6.3776248e-008 0.18219817 6.5006304e-008 0.18219818 5.3001841e-008 0.18219815 5.0662859e-008
		 0.18219815 3.8424329e-008 0.18219818 3.6986194e-008 0.18219815 2.5692175e-008 0.18219817
		 2.7416133e-008 -0.048346769 0.033837929 0.17124772 -0.20615824 -0.040264446 0.077468775
		 -0.052495815 0.13902511 -0.053352065 0.15080224 0.21306026 -0.18101579 0.19329706
		 -0.22258937 0.21763037 -0.20795599 -0.051748857 0.11863549 -0.052110985 0.13038458
		 0.32384691 -0.14433998 0.31189483 -0.16957852 0.32647225 -0.16067478 -0.052058853
		 0.098666996 -0.051997673 0.11009024 0.27847371 -0.11337582 0.27214336 -0.1262926
		 0.27967274 -0.1217417 -0.053355854 0.079458989 -0.052831959 0.090261012 -0.061536051
		 0.07718619 0.27015036 -0.094636716 0.27125198 -0.094086036 -0.054247029 0.062487788
		 -0.0539078 0.072482064 -0.053930536 0.07355722 -0.048848215 0.080209114 -0.027820943
		 0.036583073 -0.028539453 0.06054081 -0.039022956 0.14198081 -0.039625697 0.14988561
		 0.21626331 -0.25173858 0.23261175 -0.24190392 -0.038223099 0.12096917 -0.038466159
		 0.12887792 0.32582927 -0.18701562 0.33562168 -0.18103626 -0.038840033 0.10046747
		 -0.038801022 0.10814967 0.27927539 -0.13530858 0.28433666 -0.13225494 -0.040807836
		 0.080619015 -0.040436219 0.087865725 0.27104512 -0.095974952 0.27178583 -0.095604777
		 -0.04418486 0.063462876 -0.043394119 0.068963572 -0.043321833 0.069501556 -0.041243151
		 0.073993526 -0.0034486107 0.036718376 -0.017125824 0.039466716 -0.022889704 0.14349546
		 -0.023469528 0.15088949 0.23904982 -0.28843144 0.25435418 -0.27922386 -0.022050118
		 0.12171286 -0.022282356 0.12913252 0.33967355 -0.20898774 0.34883949 -0.20339356
		 -0.023137214 0.10063559 -0.023100538 0.10782785 0.28634211 -0.14666973 0.29108214
		 -0.14381592 -0.026012564 0.08006902 -0.025642712 0.086841002 0.27190623 -0.097640857
		 0.27260002 -0.097294442 -0.0341097 0.061892968 -0.033208303 0.066184804 -0.033024602
		 0.066567481 -0.031802002 0.070322931 0.029145831 0.037355617 -0.0014579108 0.011546168
		 -0.0013775809 0.14585952 -0.0018547805 0.15192463 0.27015355 -0.33702394 0.28271908
		 -0.32946497 -0.00040708398 0.12305943 -0.00060196634 0.12915574 0.35855356 -0.23808038
		 0.36607686 -0.23348936 -0.00215508 0.10117037 -0.0021224455 0.1070721 0.29597461
		 -0.1617204 0.29986638 -0.15938047 -0.0062414748 0.079575069 -0.005928128 0.085129388
		 0.27308702 -0.099850282 0.27365661 -0.099565879 -0.022212738 0.059941188 -0.021116542
		 0.063034587 -0.02099376 0.063366003 -0.020507554 0.066248856 0.040548567 0.03126251
		 0.04078918 0.05235824 -0.00089280441 -0.00089943031 0.015674483 0.0078075277 0.0065187169
		 0.14251879 0.0054168385 0.15646462 0.27246475 -0.35935831 0.30136693 -0.34197184
		 0.0073479488 0.11933918 0.0068983524 0.13336509 0.36002463 -0.25149277 0.37733018
		 -0.24093792 0.0052023688 0.097289979 0.0052826651 0.11086521 0.29668859 -0.16863416
		 0.30563769 -0.16325456 0.00049630977 0.075569041 0.0012247674 0.088342868 0.27311119
		 -0.10082439 0.2744208 -0.10017039 -0.020185363 0.056680474 -0.016992141 0.062069897
		 -0.017039834 0.062537432 -0.01732293 0.067952454 -0.064865313 5.3675461e-007 -0.064865343
		 4.3174086e-007 -0.064865336 3.5514236e-007 -0.064865358 2.5603947e-007 -0.064865351
		 1.8178152e-007 0.00036375335 -9.4980282e-007 0.00036377175 -9.4980282e-007 0.00036377608
		 1.5722591e-008 -0.064693019 0.027828878 0.18219818 2.519249e-010 0.1821982 2.4433286e-010
		 -0.061652172 0.027808866 -0.048214853 0.051603295 -0.027925255 0.099565998 -0.051921986
		 0.087098815 -0.026297186 0.084733926 -0.027711453 0.048514709 -0.019159416 0.065438673
		 -0.0033300633 0.047887865 -0.008350363 0.044067565 0.029247904 0.046526108 0.0057453886
		 0.015330448 -0.064865343 1.5685762e-008 -0.064865328 9.491616e-008 -0.064865366 1.5459134e-008
		 0.29879814 -0.15964207 0.26553878 -0.12119889 0.26928368 -0.093869328 0.2711761 -0.092923217;
createNode polyPlanarProj -n "polyPlanarProj1";
	rename -uid "52787AAD-469E-9259-BA2B-6BA6A366F8CB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[162:167]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 0.036813497543334961 -7.4505805969238281e-009 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 0.73685712804393177 0.6381369745553036 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV4";
	rename -uid "1C1372A3-433D-6FA8-5120-23B4ADDB6FD5";
	setAttr ".uopa" yes;
	setAttr -s 199 ".uvtk[0:198]" -type "float2" 0.00020075629 0.0010859009
		 0.00020075578 2.5513081e-008 9.811129e-005 2.5513081e-008 0.0001802269 0.0010859009
		 0.00018022684 2.5513081e-008 0.00015969736 0.0010859009 0.00015969828 3.8507409e-008
		 0.0001391691 0.0010859009 0.00013916957 3.8507409e-008 0.00011863995 0.0010859009
		 0.00011864088 2.5513081e-008 -0.0010320588 0.00073577487 -0.0010316896 0.0010262171
		 -0.0010320588 0.00079694443 -0.0010314452 0.00085550873 -0.0010310941 0.00091294438
		 -0.0010313172 0.00096968003 0.0021959858 0.00094114413 0.0021961944 0.00071202166
		 0.0021913943 0.0007116897 0.0021887252 0.00094114413 0.002192775 0.00076025992 0.0021879713
		 0.00075998931 0.0021911922 0.00080820674 0.0021864765 0.00080815249 0.0021912651
		 0.00085484912 0.0021867265 0.00085496297 0.0021931455 0.00089904037 0.0021888334
		 0.00089928473 0.10059118 0.0010644572 0.10059112 0.00097217289 0.10057896 0.00097216055
		 0.1005789 0.0010644572 0.10059117 0.0009906335 0.10057891 0.00099063886 0.10059118
		 0.0010091563 0.10057891 0.0010091645 0.10059115 0.0010275532 0.10057891 0.0010275496
		 0.10059115 0.0010460087 0.10057893 0.001045995 0.0021574448 0.00099961646 -0.0027736283
		 0.0010576636 0.0021381632 0.00089548138 0.0021673476 0.00074855663 0.0021694019 0.0007204694
		 -0.0027333961 0.0010818529 -0.0027524047 0.0010418575 -0.0027290247 0.0010559361
		 0.0021655646 0.00079723221 0.0021664314 0.00076919119 -0.0044619692 0.0010828616
		 -0.0044811098 0.0010425291 -0.0044578053 0.0010567575 0.0021663143 0.00084489386
		 0.0021661634 0.00081763533 -0.0028051846 0.0010817469 -0.0028247889 0.0010417346
		 -0.0028014551 0.0010558312 0.0021694088 0.00089073385 0.0021681576 0.00086495886
		 0.0021889238 0.00089616451 -0.0010666129 -0.00076287758 -0.001042343 -0.00075075525
		 0.0021715325 0.00093124784 0.0021707229 0.00090738916 0.0021707802 0.00090482138
		 0.0021586497 0.00088894123 0.0021084524 0.00099306297 0.0021101648 0.00093588443
		 0.0021351934 0.00074151857 0.0021366326 0.00072265393 -0.0027303374 0.0010138103
		 -0.002714592 0.0010232755 0.002133291 0.00079165847 0.0021338635 0.00077278202 -0.0044588153
		 0.0010146629 -0.0044431454 0.0010242157 0.0021347546 0.00084060174 0.0021346619 0.00082225795
		 -0.0028026951 0.0010138046 -0.0027869912 0.0010232666 0.0021394559 0.00088796538
		 0.0021385704 0.00087067805 -0.0010469161 -0.00079233677 -0.0010305715 -0.00078418478
		 0.0021475193 0.00092891906 0.002145624 0.00091578468 0.0021454531 0.00091450708 0.0021404927
		 0.00090377755 0.0020502694 0.00099273922 0.002082933 0.00098618458 0.002096684 0.00073790079
		 0.0020980644 0.00072024175 -0.0027083901 0.00097851048 -0.0026936533 0.00098737131
		 0.0020946714 0.00078989798 0.0020952388 0.00077218655 -0.004436695 0.00097954553
		 -0.0044220616 0.00098848878 0.0020972746 0.00084019185 0.0020971906 0.00082303863
		 -0.0027807895 0.00097861257 -0.0027661109 0.00098745141 0.0021041331 0.00088928506
		 0.0021032391 0.00087312015 -0.0010279455 -0.00082900008 -0.0010126748 -0.00082137936
		 0.0021234716 0.00093266333 0.002121317 0.000922415 0.0021208744 0.0009215062 0.0021179665
		 0.00091253902 0.0019724807 0.00099122257 0.0020455276 0.0010528245 0.0020453248 0.0007322671
		 0.002046484 0.00071778381 -0.0026784558 0.00093176164 -0.0026663686 0.00093902677
		 0.0020430121 0.00078668352 0.0020434796 0.00077212136 -0.0044065085 0.00093305815
		 -0.0043944884 0.00094038685 0.0020471909 0.0008389171 0.0020471136 0.00082482782
		 -0.0027509336 0.00093199231 -0.0027388849 0.00093923195 0.0020569477 0.00089046254
		 0.0020562033 0.00087719975 -0.0010019642 -0.00087763835 -0.0009893903 -0.00087137206
		 0.0020950714 0.0009373207 0.0020924441 0.00092993933 0.0020921458 0.00092915021 0.0020909957
		 0.00092226412 0.0019452774 0.0010057661 0.0019446885 0.0009554188 0.0020441776 0.0010825273
		 0.0020046351 0.0010617453 0.0020264864 0.00074022933 0.0020291235 0.00070693903 -0.002676246
		 0.00091027404 -0.0026484311 0.00092700293 0.0020245162 0.00079555006 0.0020255921
		 0.00076206663 -0.0044041574 0.0009116141 -0.004376499 0.00092848478 0.0020296241
		 0.00084817322 0.0020294413 0.00081578275 -0.0027487306 0.0009105748 -0.0027210349
		 0.0009272357 0.0020408549 0.00090002333 0.0020391126 0.00086953689 -0.0010014334
		 -0.00089907122 -0.00097259978 -0.00088468543 0.002090222 0.00094510638 0.0020826047
		 0.00093224068 0.0020827237 0.0009311196 0.0020833963 0.00091819657 -0.0010655967
		 0.00073618267 -0.0010648193 0.00079722324 -0.0010633128 0.00085597462 -0.0010624101
		 0.00091304851 -0.0010624079 0.00096957188 9.8112883e-005 0.0010859009 7.7580014e-005
		 0.0010859009 7.758422e-005 2.5513081e-008 0.0021964682 0.0010139585 0.10059115 0.0010828917
		 0.10057894 0.0010828782 0.0021892032 0.0010140034 0.0021571377 0.00095722388 0.0021086978
		 0.0008427563 0.0021659867 0.000872512 0.0021048039 0.00087815133 0.0021081865 0.00096458866
		 0.0020877714 0.00092420407 0.0020500028 0.00096608541 0.0020619882 0.00097520661
		 0.0019722376 0.00096934143 0.0020283244 0.001043793 -0.0010629342 0.0010823651 -0.001062698
		 0.0010260538 -0.0010320015 0.0010825202 -0.004502031 0.0010584063 -0.0028452256 0.0010575135
		 -0.0010856584 -0.00074598595 -0.0010440148 -0.00072515936 0.26588339 -0.57104331
		 0.12797254 -0.58795762 0.038247012 -0.86096573 0.43198195 -0.80004001 0.32097527
		 -0.4235279 0.57297087 -0.38142964 0.23772234 -0.29353699 0.32827812 -0.021797001
		 0.099440783 -0.31024164 -0.061657749 -0.078745306 0.044402812 -0.4573361 -0.20810789
		 -0.49800798;
createNode polyPlanarProj -n "polyPlanarProj2";
	rename -uid "993D4ADE-4A4E-1E0D-0191-1785B112C479";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[12:17]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -1.4901161193847656e-008 5.4879927635192871 7.4505805969238281e-009 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 0.50996938347816467 0.44164659082889557 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV5";
	rename -uid "601A2820-4896-3FA8-53EB-398B9BF1BD40";
	setAttr ".uopa" yes;
	setAttr -s 211 ".uvtk[0:210]" -type "float2" 8.676252e-005 0.00018672241
		 8.6762571e-005 7.1086141e-008 6.9123358e-005 7.1086141e-008 8.3233441e-005 0.00018672241
		 8.3233332e-005 7.1086141e-008 7.9705082e-005 0.00018672241 7.970514e-005 1.1731397e-007
		 7.6179647e-005 0.00018672241 7.6180098e-005 1.1731397e-007 7.2650451e-005 0.00018672241
		 7.2651339e-005 7.1086141e-008 -9.5709031e-005 0.00012615872 -9.5660413e-005 0.00017616519
		 -9.5709031e-005 0.00013668153 -9.5609525e-005 0.00014677274 -9.5555253e-005 0.00015666311
		 -9.5585121e-005 0.00016642899 -0.0017797496 0.0019670378 -0.0017797133 0.0019275688
		 -0.0017805465 0.0019275064 -0.0017810031 0.0019670378 -0.0017803127 0.0019358783
		 -0.0017811368 0.0019358333 -0.0017805804 0.0019441498 -0.0017813887 0.0019441198
		 -0.0017805711 0.0019521761 -0.0017813483 0.0019521897 -0.0017802409 0.0019597854
		 -0.0017809909 0.0019598335 0.012341714 0.00018276289 0.01234172 0.00016699568 0.012339664
		 0.00016699593 0.012339657 0.00018276289 0.012341752 0.00017015029 0.012339618 0.0001701506
		 0.01234172 0.00017331372 0.012339621 0.00017331613 0.012341747 0.00017645577 0.012339631
		 0.00017645562 0.012341742 0.00017961365 0.012339649 0.00017960984 -0.0017863986 0.0019771187
		 0.007045005 0.00018160234 -0.0017897174 0.001959177 -0.0017846862 0.0019338775 -0.0017843368
		 0.0019290274 0.0070518642 0.00018573683 0.0070486269 0.00017890226 0.0070526209 0.00018130851
		 -0.0017849912 0.001942253 -0.0017848399 0.0019374308 0.0070273331 0.0019914629 0.0070240782
		 0.0019845881 0.007028054 0.001987013 -0.0017848706 0.0019504615 -0.0017848978 0.0019457658
		 0.0034644927 0.00018571931 0.0034611458 0.00017888381 0.0034651319 0.00018129226
		 -0.0017843314 0.0019583539 -0.0017845462 0.0019539217 -0.0017809743 0.0019592906
		 0.0016750147 0.00017949659 0.0016792066 0.00018158056 -0.0017839693 0.001965333 -0.0017841021
		 0.0019612266 -0.0017840948 0.0019607805 -0.001786183 0.0019580531 -0.0017948373 0.0019759869
		 -0.0017945434 0.0019661351 -0.0017902284 0.001932659 -0.0017899754 0.0019294128 0.0070523955
		 0.00017410672 0.0070550875 0.00017572532 -0.0017905553 0.0019412938 -0.0017904607
		 0.001938041 0.0070278817 0.0019798386 0.0070305476 0.0019814654 -0.0017902985 0.0019497215
		 -0.0017903179 0.001946566 0.0034649032 0.00017410945 0.0034676117 0.00017572434 -0.0017894945
		 0.0019578808 -0.0017896419 0.0019549027 0.0016784411 0.00017442704 0.0016812246 0.00017582963
		 -0.0017881092 0.00196494 -0.0017884333 0.0019626778 -0.0017884583 0.0019624576 -0.0017893105
		 0.0019606038 -0.0018048553 0.0019759296 -0.0017992232 0.0019748027 -0.0017968587
		 0.0019320252 -0.0017966261 0.0019289792 0.0070561464 0.00016807044 0.0070586717 0.00016958729
		 -0.0017972046 0.0019409864 -0.0017971081 0.001937937 0.0070316526 0.0019738604 0.0070341374
		 0.0019753817 -0.0017967626 0.0019496598 -0.0017967722 0.001946704 0.0034686301 0.00016809721
		 0.0034711729 0.00016961094 -0.0017955777 0.0019581115 -0.0017957297 0.0019553218
		 0.0016816831 0.00016812426 0.0016843181 0.00016943269 -0.001792244 0.0019655772 -0.0017926197
		 0.0019638184 -0.0017926908 0.0019636613 -0.001793195 0.0019621109 -0.0018182626 0.0019756693
		 -0.0018056763 0.0019862829 -0.0018057034 0.0019310532 -0.0018055105 0.0019285631
		 0.0070612621 0.00016007866 0.0070633111 0.00016132377 -0.0018061133 0.0019404375
		 -0.0018060193 0.0019379219 0.0070367837 0.0019659274 0.0070388168 0.0019671773 -0.001805389
		 0.001949432 -0.0018053968 0.0019470155 0.0034737256 0.00016013595 0.0034757953 0.00016137288
		 -0.0018037142 0.0019583148 -0.0018038421 0.0019560328 0.0016861408 0.00015976313
		 0.0016882913 0.00016083453 -0.0017971402 0.0019663838 -0.0017975951 0.0019651114
		 -0.0017976456 0.0019649758 -0.0017978332 0.0019637903 -0.001822946 0.0019781776 -0.0018230376
		 0.001969506 -0.0018059104 0.0019914007 -0.0018127093 0.0019878191 -0.0018089495 0.0019324303
		 -0.0018085057 0.0019267019 0.0070616459 0.00015641433 0.007066397 0.00015927324 -0.0018092868
		 0.0019419574 -0.0018091055 0.001936205 0.0070371651 0.0019622785 0.0070418757 0.0019651575
		 -0.0018084005 0.0019510281 -0.0018084375 0.0019454345 0.0034741475 0.00015648146
		 0.0034788502 0.00015932033 -0.0018064725 0.0019599542 -0.0018067787 0.001954698 0.0016862564
		 0.00015606724 0.0016911922 0.00015854252 -0.0017979643 0.00196773 -0.0017992785 0.0019655109
		 -0.0017992592 0.0019653186 -0.0017991409 0.0019630911 -0.00010148355 0.00012622798
		 -0.00010134776 0.0001367435 -0.00010109303 0.00014685391 -0.00010093032 0.00015668134
		 -0.00010093545 0.00016640752 6.9119174e-005 0.00018672241 6.5592052e-005 0.00018672241
		 6.5596229e-005 7.1086141e-008 -0.0017796692 0.0019795862 0.012341739 0.00018591301
		 0.012339646 0.00018591089 -0.0017809186 0.0019795946 -0.0017864469 0.0019698066 -0.0017947874
		 0.0019501005 -0.0017849267 0.0019552186 -0.0017954606 0.0019561932 -0.0017948799
		 0.0019710828 -0.0017983925 0.0019641258 -0.0018049025 0.0019713393 -0.0018028357
		 0.00197291 -0.0018183093 0.0019718986 -0.0018086352 0.0019847264 -0.00010103918 0.00018583458
		 -0.000100975 0.000176138 -9.5704716e-005 0.00018586169 0.0070205047 0.0019872934
		 0.0034576557 0.00018158009 0.0016717362 0.00018239894 0.001678917 0.00018598021 0.010581282
		 0.0019793278 0.010581328 0.0019825581 0.01057463 0.0019870219 0.010573369 0.0019753855
		 0.010583879 0.0019774348 0.010583443 0.001969402 0.010586646 0.0019788924 0.010593208
		 0.0019746677 0.010586786 0.0019820812 0.010593686 0.001985546 0.010584162 0.0019838919
		 0.010584687 0.001991661 0.18046431 -0.68709999 0.37929633 -0.68226558 0.52255768
		 -0.97977221 0.022557478 -0.99192929 0.48354682 -0.48101646 0.78471488 -0.47369379
		 0.38896534 -0.28460163 0.54687196 0.020227686 0.19013324 -0.28943607 0.046871882
		 0.0080706328 0.085882761 -0.49068525 -0.2152852 -0.49800789;
createNode polyPlanarProj -n "polyPlanarProj3";
	rename -uid "1D72F13E-4E03-76F9-5C47-ED9D835358FE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "f[6:11]" "f[24:35]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.0021815896034240723 5.9124383926391602 2.3186206817626953e-005 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 0.63111615180969238 0.62690985202789307 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV6";
	rename -uid "56E62B43-49AB-2A57-2449-F395CC2D5F12";
	setAttr ".uopa" yes;
	setAttr -s 254 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 0.00025624447 -0.00038794609 0.00025624459
		 8.4355634e-011 0.000292918 8.4355634e-011 0.00026357861 -0.00038794609 0.00026357861
		 8.4355634e-011 0.0002709172 -0.00038794609 0.00027091743 -2.6958674e-008 0.00027824659
		 -0.00038794609 0.00027824758 -2.6958674e-008 0.00028558201 -0.00038794609 0.00028558393
		 8.4355634e-011 8.3401435e-005 -0.00026399724 8.3284678e-005 -0.00036771339 8.3401435e-005
		 -0.00028582828 8.3169667e-005 -0.00030674235 8.3055471e-005 -0.00032725171 8.3126317e-005
		 -0.00034751312 0.00080005435 -0.00033652558 0.00079998682 -0.0002544999 0.00080170919
		 -0.00025438252 0.00080266007 -0.00033652558 0.00080120901 -0.00027175265 0.00080292573
		 -0.00027168833 0.00080176687 -0.00028894204 0.00080346823 -0.00028891501 0.00080175191
		 -0.00030563358 0.00080336822 -0.00030567052 0.00080108253 -0.00032145018 0.00080261676
		 -0.00032154066 -0.0053246873 0.0014325241 -0.0053246641 0.0014654704 -0.0053203302
		 0.0014654847 -0.0053203302 0.0014325241 -0.0053247125 0.0014588839 -0.0053203162
		 0.0014588832 -0.0053246785 0.0014522668 -0.0053202752 0.00145227 -0.0053247181 0.0014456984
		 -0.0053203008 0.0014457038 -0.0053246301 0.0014391122 -0.0053202696 0.0014391172
		 0.00081384816 -0.00035746116 -0.0029313101 -0.0021929082 0.00082076323 -0.00032017104
		 0.00081030483 -0.00026757346 0.00080958585 -0.00025753057 -0.0029456213 -0.0022015201
		 -0.0029388478 -0.0021872784 -0.0029471999 -0.0021922926 0.00081095583 -0.00028499571
		 0.00081063173 -0.00027497471 -0.0027281917 -0.00038726724 -0.0027214109 -0.00037298427
		 -0.0027297027 -0.0003780199 0.0008106741 -0.00030207081 0.00081074104 -0.00029229856
		 -0.0031716526 -0.00038756873 -0.0031646735 -0.00037334909 -0.003173019 -0.00037835873
		 0.00080958341 -0.00031847702 0.00081002567 -0.00030924074 0.0008025844 -0.00032042625
		 -0.0015212666 -0.00037464406 -0.001529936 -0.0003789723 0.00080881012 -0.00033297201
		 0.00080910599 -0.00032444068 0.00080907659 -0.00032352659 0.0008134261 -0.00031784226
		 0.00083139981 -0.00035511007 0.00083077361 -0.00033464574 0.00082181714 -0.00026506776
		 0.00082130829 -0.00025832548 -0.0029467314 -0.0021772964 -0.0029523247 -0.0021806627
		 0.00082249969 -0.00028301706 0.00082229218 -0.00027625193 -0.002729364 -0.00036311304
		 -0.0027348732 -0.00036649857 0.00082198606 -0.00030054964 0.00082201383 -0.00029397639
		 -0.003172511 -0.0003634234 -0.0031781071 -0.00036678225 0.00082029559 -0.00031749494
		 0.00082062074 -0.00031129614 -0.0015283041 -0.00036412987 -0.0015341463 -0.00036703536
		 0.00081741688 -0.00033214875 0.00081808137 -0.00032744423 0.00081814808 -0.00032698922
		 0.00081992341 -0.00032314946 0.00085220422 -0.00035499697 0.00084052276 -0.00035264704
		 0.00083559967 -0.00026376112 0.00083510426 -0.00025746156 -0.0029545133 -0.0021647201
		 -0.0029597541 -0.002167881 0.00083631236 -0.00028239161 0.00083612348 -0.00027604151
		 -0.0027371675 -0.00035067293 -0.0027423475 -0.00035384423 0.00083538418 -0.00030038605
		 0.00083541265 -0.0002942502 -0.0031803097 -0.00035091274 -0.0031855493 -0.00035405654
		 0.00083293166 -0.0003179643 0.00083326112 -0.00031216358 -0.0015350627 -0.00035103349
		 -0.0015405237 -0.00035375386 0.00082601473 -0.00033349634 0.00082677981 -0.00032982044
		 0.00082694134 -0.00032949552 0.00082799164 -0.00032628613 0.00088007638 -0.00035445113
		 0.00085391972 -0.00037650252 0.00085399603 -0.00026176614 0.00085358013 -0.00025655533
		 -0.0029651492 -0.0021480797 -0.0029694934 -0.0021506746 0.00085481745 -0.00028121879
		 0.00085464929 -0.00027601549 -0.0027478661 -0.00033421096 -0.0027521458 -0.00033680836
		 0.00085331674 -0.00029992798 0.00085335027 -0.00029488854 -0.0031909524 -0.00033433604
		 -0.0031951906 -0.00033692049 0.00084982021 -0.00031838348 0.0008500847 -0.00031363836
		 -0.0015443269 -0.0003336654 -0.001548833 -0.00033589866 0.00083617814 -0.00033515782
		 0.00083712605 -0.00033251953 0.00083721982 -0.00033222756 0.00083764532 -0.00032976735
		 0.00088982313 -0.00035966054 0.00089002104 -0.00034164262 0.00085439358 -0.0003871372
		 0.00086853583 -0.00037969989 0.00086071395 -0.0002646193 0.00085977232 -0.00025268819
		 -0.0029660056 -0.0021404307 -0.0029758681 -0.00214638 0.00086143252 -0.00028440799
		 0.00086103368 -0.00027240795 -0.0027486535 -0.00032663005 -0.0027584997 -0.00033259462
		 0.00085961644 -0.00030324489 0.00085967942 -0.00029166063 -0.0031917407 -0.00032673674
		 -0.0032015552 -0.00033264741 0.00085558242 -0.00032181019 0.00085620792 -0.00031087376
		 -0.0015445355 -0.00032601337 -0.0015548391 -0.00033114775 0.00083790382 -0.00033793377
		 0.00084064045 -0.00033333024 0.00084059761 -0.00033293106 0.00084035064 -0.00032831513
		 9.540427e-005 -0.00026414188 9.5111434e-005 -0.00028591804 9.4556461e-005 -0.00030690094
		 9.4247429e-005 -0.00032729513 9.4240429e-005 -0.00034747546 0.00029291288 -0.00038794609
		 0.00030024705 -0.00038794609 0.00030025549 8.4355634e-011 0.0007998799 -0.00036258865
		 -0.0053246636 0.0014259414 -0.0053202827 0.0014259462 0.0008024908 -0.00036261004
		 0.00081396184 -0.00034227889 0.00083129003 -0.00030130168 0.00081078889 -0.00031194428
		 0.00083267968 -0.00031396997 0.00083147013 -0.0003449205 0.00083878502 -0.00033045048
		 0.00085232244 -0.00034545563 0.00084801938 -0.00034871337 0.00088015076 -0.0003466183
		 0.00086007465 -0.00037327263 9.4449788e-005 -0.00038777001 9.4328927e-005 -0.00036766002
		 8.3366482e-005 -0.00038782536 -0.0027140535 -0.00037860707 -0.0031574168 -0.00037895641
		 -0.0015144802 -0.00038067464 -0.0015293225 -0.00038811137 -0.0058176001 -0.00036251353
		 -0.0058177444 -0.00036911175 -0.0058040386 -0.00037821836 -0.005801497 -0.00035446137
		 -0.0058229812 -0.00035864976 -0.0058220411 -0.00034222478 -0.0058285957 -0.00036162024
		 -0.0058419337 -0.00035300371 -0.0058288211 -0.00036813624 -0.0058430019 -0.00037520329
		 -0.0058234525 -0.00037182862 -0.0058245831 -0.00038769084 0.014627903 0.027815741
		 0.014627771 0.040933337 -0.0052411268 0.050867915 -0.0052411268 0.017881162 0.02774564
		 0.047492124 0.02774564 0.067361288 0.040863246 0.040933337 0.060732327 0.050867878
		 0.040863246 0.027815659 0.060732327 0.01788112 0.02774564 0.021256868 0.02774564
		 0.0013877867 0.054191332 -0.55635095 0.19849156 -0.53951269 0.11166129 -0.42212561
		 0.25596154 -0.40528703 0.16913129 -0.28790033 0.024831038 -0.30473897 -0.032638934
		 -0.4389638 0.13944177 -0.917741 0.41887754 -0.88514477 0.4024666 -0.86295664 0.1503052
		 -0.89237136 0.66203618 -0.75602913 0.78545552 -0.54082626 0.76136273 -0.54082626
		 0.6499896 -0.73502421 0.57867092 -0.25994009 0.45525181 -0.044737495 0.44320577 -0.065742411
		 0.55457836 -0.25994009 0.40703458 0.10977069 0.12759881 0.07717441 0.14400963 0.054986682
		 0.39617112 0.084401272 -0.26006711 -0.035675492 -0.38348684 -0.25087866 -0.3593941
		 -0.25087866 -0.24802089 -0.056680024 -0.15493543 -0.54263991 -0.031516042 -0.75784284
		 -0.019469999 -0.73683804 -0.13084251 -0.54263991 0.2394999 -0.73751944 0.60408711
		 -0.6947968 0.47143856 -0.5156492 0.32713833 -0.53255844 0.74913359 -0.3555752 0.52884668
		 -0.38138852 0.52959287 -0.059076045 0.44195443 -0.26403692;
	setAttr ".uvtk[250:253]" 0.16500571 -0.10179858 0.29765415 -0.28094614 0.019959183
		 -0.4410204 0.24024621 -0.41520697;
createNode polyMapDel -n "polyMapDel2";
	rename -uid "49E9FB24-4CE2-3E0A-A5A0-A6A7FDC52BC3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "f[65]" "f[89]" "f[113]" "f[137]" "f[161]";
createNode polyTweakUV -n "polyTweakUV7";
	rename -uid "9210A2B1-49A4-79DF-683E-198B64C33F29";
	setAttr ".uopa" yes;
	setAttr -s 254 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 4.4010667e-006 0.001815084 4.4002472e-006
		 5.7562374e-008 -0.00016716639 5.7562374e-008 -2.99121e-005 0.001815084 -2.9912597e-005
		 5.7562374e-008 -6.4223837e-005 0.001815084 -6.4224012e-005 4.4717865e-008 -9.8540397e-005
		 0.001815084 -9.8538039e-005 4.4717865e-008 -0.00013285065 0.001815084 -0.00013285001
		 5.7562374e-008 -0.23571208 -0.00058182213 -0.23571151 -9.6324729e-005 -0.23571208
		 -0.00047958331 -0.23571105 -0.00038165864 -0.23571047 -0.00028567802 -0.23571086
		 -0.00019084722 0.036083296 -0.064680986 0.048461299 -0.067719705 0.047896076 -0.0678849
		 0.036026582 -0.064680986 0.046746776 -0.06208517 0.046171475 -0.062281512 0.044552334
		 -0.056099426 0.044004496 -0.056412738 0.04035629 -0.050952241 0.040037367 -0.051357407
		 0.035990734 -0.049705464 0.036652919 -0.049832944 0.015126375 -3.5658828e-005 0.015126359
		 -0.00018960863 0.015106 -0.0001896295 0.015105967 -3.5658828e-005 0.01512639 -0.00015882716
		 0.015105911 -0.00015881559 0.015126386 -0.00012791867 0.015105963 -0.00012790256
		 0.015126378 -9.722801e-005 0.015106011 -9.7234624e-005 0.015126386 -6.6438064e-005
		 0.015105952 -6.6458291e-005 0.036055543 -0.064085007 0.014697915 -4.016409e-005 0.17896283
		 -0.013739544 0.044123031 -0.064262569 0.045053009 -0.06741412 0.014764708 -1.6882384e-011
		 0.01473316 -6.6411609e-005 0.014772002 -4.3036682e-005 0.041973233 -0.058657281 0.043134704
		 -0.061941832 0.01480169 -4.9378436e-006 0.014769938 -7.1967916e-005 0.014808633 -4.8320664e-005
		 0.038805991 -0.054256089 0.041054212 -0.057068884 0.016541589 -0.0018188732 0.016508956
		 -0.0018854714 0.016547754 -0.0018620068 0.036628619 -0.051601797 0.038668334 -0.052598957
		 0.18742695 -0.016647333 0.016496841 -6.3968218e-005 0.016537365 -4.3713615e-005 0.043244168
		 -0.06304653 0.042657703 -0.053956669 0.18492027 -0.010880899 0.18133868 -0.012803975
		 0.035687417 -0.063866191 0.17722894 -0.011236213 0.040895808 -0.065832682 0.041299257
		 -0.067763522 0.014769808 -0.00011297702 0.014795942 -9.7264769e-005 0.038647097 -0.060526431
		 0.039225399 -0.062495392 0.01480694 -0.00011827608 0.014832952 -0.00010239655 0.036174223
		 -0.057303071 0.037509102 -0.059051022 0.016545724 -0.0019319579 0.016571801 -0.0019162152
		 0.03565497 -0.054154299 0.037052702 -0.054936226 0.016529752 -0.0001131871 0.016556999
		 -9.9574281e-005 0.057134286 -0.060069311 0.055232868 -0.051294163 0.17945448 -0.0088697076
		 0.17911661 -0.010483644 0.035298254 -0.063475735 0.17568825 -0.0082444092 0.037309036
		 -0.066496901 0.037430882 -0.068109199 0.014806228 -0.00017158857 0.014830679 -0.00015688107
		 0.035032459 -0.061584696 0.035415903 -0.063224085 0.014843694 -0.00017662863 0.014868053
		 -0.00016177239 0.032908816 -0.059877954 0.033907916 -0.061410237 0.016582154 -0.0019905411
		 0.016606567 -0.0019758209 0.034003608 -0.056998167 0.035304952 -0.057759032 0.016561417
		 -0.00017446326 0.01658695 -0.00016172099 0.077773631 -0.055300895 0.075386614 -0.045180485
		 0.17583607 -0.010635353 0.17626064 -0.011141743 0.034749184 -0.062897794 0.17324534
		 -0.0040761437 0.03294152 -0.066746332 0.032886978 -0.068004496 0.014855915 -0.00024920798
		 0.014875985 -0.00023714195 0.030470436 -0.062774561 0.03077488 -0.064097166 0.014893836
		 -0.00025389131 0.014913824 -0.00024170587 0.028687976 -0.062620923 0.029412642 -0.063839443
		 0.016631804 -0.0020681401 0.016651865 -0.0020560694 0.0319153 -0.060867369 0.033008058
		 -0.061502624 0.016604856 -0.00025572139 0.016625805 -0.00024525833 0.10873125 -0.047809597
		 0.10588957 -0.038528442 0.1749602 -0.011675956 0.17594469 -0.011571719 0.034664214
		 -0.062578112 0.034267705 -0.06294179 0.17315136 -0.0021541892 0.17059641 -0.0035154403
		 0.031512 -0.065739483 0.031173192 -0.068657033 0.014859617 -0.00028488491 0.014905755
		 -0.00025711805 0.028576549 -0.062261954 0.029301126 -0.065387681 0.014897752 -0.00028951623
		 0.014943711 -0.00026148692 0.026711252 -0.062713094 0.028362267 -0.065508664 0.016635478
		 -0.0021037883 0.01668163 -0.0020760477 0.030350238 -0.061819132 0.032929786 -0.063346758
		 0.016605759 -0.00029155213 0.016653892 -0.00026749691 0.12471169 -0.050516143 0.11662786
		 -0.025360176 0.17431636 -0.016694818 0.17910428 -0.013479202 -0.23576815 -0.00058111869
		 -0.23576687 -0.00047907536 -0.23576432 -0.00038089772 -0.23576283 -0.00028550407
		 -0.23576283 -0.00019101806 -0.00016716709 0.001815084 -0.00020147527 0.001815084
		 -0.00020147458 5.7562374e-008 0.036406465 -0.064149372 0.015126357 -4.9047899e-006
		 0.015106001 -4.9275018e-006 0.036353465 -0.064116456 0.035842992 -0.064402938 0.17593721
		 -0.016532566 0.18005612 -0.016327392 0.17660427 -0.014377816 0.035510056 -0.064067364
		 0.17593031 -0.011836194 0.035104889 -0.063660309 0.17442098 -0.0089251073 0.034579799
		 -0.063051447 0.17215262 -0.0046717259 -0.23576371 -2.4694364e-006 -0.2357633 -9.6595657e-005
		 -0.23571202 -2.2104414e-006 0.014735154 -4.5577497e-005 0.016474904 -0.0018592086
		 0.01646496 -3.5744393e-005 0.016534582 -9.4581003e-007 0.014916966 -0.00012071885
		 0.014917517 -8.9868758e-005 0.014853491 -4.723078e-005 0.014841657 -0.0001583706
		 0.014941975 -0.00013879094 0.014937678 -0.00021557452 0.014968343 -0.00012489081
		 0.015030824 -0.00016521312 0.01496954 -9.4422059e-005 0.015035757 -6.1329541e-005
		 0.014944426 -7.7117365e-005 0.01494967 -2.926244e-006 0.01436237 0.0043170042 0.015219452
		 0.004983353 0.017473489 0.0071585071 0.015141181 0.0047064247 0.013537878 0.0047926717
		 0.01450546 0.0071354872 0.011207655 0.0039580651 0.010196313 0.0042971526 0.010459116
		 0.0033456711 0.008468708 0.0011874493 0.012023943 0.003515109 0.010849267 0.0010446219
		 0.2945216 -0.02190746 0.2945216 -0.021938158 0.29454836 -0.021922804 0.29454842 -0.021953506
		 0.2945751 -0.021938158 0.29457512 -0.02190746 0.29454836 -0.021892108 -0.38423607
		 -0.034635827 -0.38423607 -0.034694221 -0.38423112 -0.034691367 -0.38423112 -0.034638673
		 -0.39333048 -0.061931551 -0.39330056 -0.061879404 -0.39330637 -0.061879404 -0.39333341
		 -0.061926462 0.29274169 -0.063742839 0.29271123 -0.063689895 0.29270831 -0.063695058
		 0.29273573 -0.063742839 -0.12464186 -0.038314268 -0.12464186 -0.038256325 -0.12464681
		 -0.038259149 -0.12464679 -0.038311444 -0.20102578 -0.078227744 -0.20105579 -0.078280069
		 -0.20104991 -0.078280069 -0.20102282 -0.078232855 0.044204906 -0.041905496 0.044234514
		 -0.041957173 0.044237457 -0.041952133 0.044210728 -0.041905496 0.0022049858 0.0087923128
		 0.0021742366 0.0086908154 0.0022342391 0.0086284289 0.0022856421 0.0087460214 0.0023385216
		 0.0086658951 0.0023384292 0.0085411882 0.0025027143 0.0086907968 0.0024429236 0.0086284466;
	setAttr ".uvtk[250:253]" 0.0025027143 0.0087406049 0.0024429236 0.0088029699
		 0.0023385216 0.0087655224 0.0023384292 0.0088902293;
createNode polyPlanarProj -n "polyPlanarProj4";
	rename -uid "28CEA1C8-40AF-49CF-949F-AF8E6CFB2581";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 20 "f[47]" "f[49]" "f[59]" "f[61]" "f[71]" "f[73]" "f[83]" "f[85]" "f[95]" "f[97]" "f[107]" "f[109]" "f[119]" "f[121]" "f[131]" "f[133]" "f[143]" "f[145]" "f[155]" "f[157]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.0021815896034240723 5.9202497005462646 -2.1159648895263672e-006 ;
	setAttr ".ro" -type "double3" 0 -31.910465126258568 0 ;
	setAttr ".ps" -type "double2" 0.7470088005065918 0.77040338516235352 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj5";
	rename -uid "4814F894-4B17-9EAA-64AB-4FB38115F28E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 20 "f[43]" "f[45]" "f[55]" "f[57]" "f[67]" "f[69]" "f[79]" "f[81]" "f[91]" "f[93]" "f[103]" "f[105]" "f[115]" "f[117]" "f[127]" "f[129]" "f[139]" "f[141]" "f[151]" "f[153]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 3.7252902984619141e-008 5.9202477931976318 4.4554471969604492e-006 ;
	setAttr ".ro" -type "double3" 0 90 0 ;
	setAttr ".ps" -type "double2" 0.80499139428138733 0.77039957046508789 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV8";
	rename -uid "51425078-497C-A01A-F3B8-949C0FDEDF18";
	setAttr ".uopa" yes;
	setAttr -s 350 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 1.3449212e-005 -0.0016555809 1.3449951e-005
		 -6.102843e-008 0.00016992848 -6.102843e-008 4.4747194e-005 -0.0016555809 4.4745695e-005
		 -6.102843e-008 7.6044162e-005 -0.0016555809 7.6044249e-005 -7.449043e-008 0.00010733908
		 -0.0016555809 0.00010733791 -7.449043e-008 0.00013863562 -0.0016555809 0.00013863074
		 -6.102843e-008 0.00089309836 0.0025261682 0.00089255505 0.0020832734 0.00089309836
		 0.0024329 0.00089216017 0.0023435566 0.00089161855 0.0022560286 0.00089195871 0.0021695131
		 0.0029908989 0.00028009267 0.0030112071 0.00062553538 0.00301762 0.00062577589 0.0030019097
		 0.00028009267 0.0030135778 0.00056114979 0.0030199625 0.00056118984 0.0030123412
		 0.00049774966 0.0030186547 0.00049733627 0.0030052471 0.00043496856 0.0030116506
		 0.00043412924 0.0029950999 0.00036945104 0.0030027868 0.00036886451 -0.16805282 0.0075146104
		 -0.16805276 0.0076550678 -0.1680342 0.0076550897 -0.16803414 0.0075146104 -0.16805284
		 0.0076269866 -0.16803409 0.0076269787 -0.16805278 0.0075987899 -0.16803412 0.0075987754
		 -0.16805279 0.0075707887 -0.16803418 0.0075707925 -0.16805278 0.0075426898 -0.16803417
		 0.0075427084 0.0030497985 0.0001916137 0.22147492 0.0020320178 -0.0012555834 0.00043480608
		 0.0030481017 0.00057539361 0.0030465256 0.00061313517 0.22141406 0.0019954271 0.22144274
		 0.0020559297 0.22140738 0.0020346351 0.0030472421 0.00051029329 0.0030478563 0.00054771261
		 0.22359516 -0.0016578329 0.22362417 -0.001596685 0.22358887 -0.0016182563 0.0030408315
		 0.00044470219 0.0030448085 0.00048171679 0.22471972 0.00016671179 0.22474951 0.00022744297
		 0.22471409 0.00020604492 0.0030324678 0.0003789927 0.0030377812 0.00041676973 -0.0013191553
		 0.00042893505 0.22447862 0.00022489188 0.22444165 0.00020641713 0.0030402413 0.00029795387
		 0.0030405018 0.00034958319 -0.0012955474 0.00042529442 -0.0012829707 0.00044637176
		 0.0031241276 0.00020200276 -0.001215666 0.0003771729 0.0030919092 0.00058356521 0.0030903798
		 0.00060920091 0.22140929 0.002098351 0.22138555 0.002084038 0.0030910743 0.00051568897
		 0.0030911523 0.0005412926 0.22359031 -0.0015544432 0.22356658 -0.0015689284 0.0030847031
		 0.00044619027 0.0030870731 0.0004713267 0.22471598 0.00026983564 0.22469215 0.0002554768
		 0.0030766581 0.00037896959 0.003080335 0.00040412295 0.22444861 0.00026978413 0.22442374
		 0.00025736666 0.00310012 0.0003064757 0.0030998392 0.00034118045 -0.0012659327 0.00041383307
		 -0.001258899 0.00042755052 0.0032124692 0.00020314635 -0.0011765775 0.00030522817
		 0.0031448351 0.00058797741 0.0031429206 0.00061229424 0.22137621 0.0021517435 0.22135393
		 0.0021383485 0.003144101 0.00051664363 0.0031438987 0.00054099562 0.22355677 -0.0015012125
		 0.22353457 -0.0015147637 0.003136602 0.00044251978 0.003138395 0.00046621781 0.22468273
		 0.00032325345 0.22466047 0.00030983199 0.0031279093 0.00037222239 0.0031314467 0.00039567417
		 0.2244197 0.0003256588 0.22439641 0.00031404893 0.0031713096 0.00030869295 0.0031706251
		 0.00034122547 -0.0012343755 0.00040018148 -0.001229191 0.00041305021 0.0033305567
		 0.00020643631 -0.0011234297 0.00021023145 0.0032161078 0.00059619982 0.0032142696
		 0.0006162505 0.22133094 0.0022224628 0.22131261 0.0022114641 0.0032155155 0.00051958131
		 0.0032153181 0.00053963094 0.22351108 -0.0014307197 0.22349283 -0.001441848 0.0032061725
		 0.00043989107 0.0032074922 0.00045940807 0.22463746 0.00039401231 0.22461914 0.00038301156
		 0.0031966309 0.00036397242 0.0031995848 0.00038319177 0.22438008 0.00039978774 0.22436099
		 0.00039023618 0.0032663464 0.00031405161 0.0032656023 0.00034080652 -0.0011918995
		 0.00038676194 -0.001188483 0.000397453 0.0033720485 0.00018472155 0.0033722771 0.00026114131
		 -0.0011215425 0.00016799619 -0.0010653186 0.00019751801 0.0032425423 0.00058568607
		 0.0032379632 0.00063173537 0.22132754 0.0022549676 0.22128551 0.002229658 0.0032406896
		 0.00050684402 0.0032402494 0.00055285444 0.22350745 -0.0013982288 0.22346559 -0.0014237968
		 0.0032297461 0.00042557711 0.0032327874 0.00047048143 0.22463407 0.00042651541 0.224592
		 0.0004012248 0.0032186087 0.0003477617 0.0032255757 0.00039184457 0.22437927 0.00043245399
		 0.22433533 0.00041052417 0.0033003821 0.00029763632 0.0032985671 0.00035923239 -0.0011785309
		 0.00037536849 -0.0011715982 0.00040049467 0.0009442601 0.0025255168 0.00094307202
		 0.0024324455 0.00094074995 0.0023428728 0.00093938946 0.0022558605 0.00093939691
		 0.0021696582 0.00016992757 -0.0016555809 0.00020122618 -0.0016555809 0.00020123123
		 -6.102843e-008 0.0029907085 0.00016956564 -0.16805275 0.0074865529 -0.16803421 0.0074865744
		 0.0030017234 0.00016954797 0.0030499327 0.0002559539 -0.0012155782 0.00051084434
		 -0.0012963251 0.00046565098 -0.0012085341 0.00046027557 0.0031242371 0.00024523077
		 -0.0011835797 0.00039406068 0.0032125742 0.00024362009 -0.0011466278 0.00032088876
		 0.0033306514 0.00023966398 -0.0010989706 0.00022305454 0.00094019889 0.0019976604
		 0.00093981775 0.0020835202 0.00089303014 0.0019974236 0.22365579 -0.0016207602 0.22478053
		 0.00020349419 0.22450766 0.00019914949 0.22444421 0.00016741037 0.17504676 0.056972004
		 0.17504618 0.056943867 0.1751046 0.056904979 0.17511538 0.057006352 0.17502391 0.056988489
		 0.17502783 0.057058547 0.17499989 0.056975812 0.17494281 0.057012599 0.17499869 0.056948029
		 0.17493831 0.056917835 0.17502157 0.056932244 0.17501679 0.056864571 -0.0069333483
		 0.14471804 -0.0069318935 0.14474908 -0.0069734296 0.14477535 -0.0069773556 0.14469604
		 -0.0069047809 0.14476369 -0.0069031492 0.14481291 -0.0068787229 0.14474735 -0.0068351161
		 0.14477058 -0.0068799946 0.14471643 -0.006838053 0.14469019 -0.0069072815 0.14470175
		 -0.0069092861 0.14465232 -0.41027406 -0.029019741 -0.41027406 -0.028991785 -0.41029844
		 -0.029005764 -0.41029844 -0.0289778 -0.41032284 -0.028991785 -0.41032282 -0.029019743
		 -0.41029844 -0.029033722 0.1949565 -0.03084808 0.1949565 -0.030793618 0.19495183
		 -0.030796278 0.19495183 -0.030845422 0.52417892 0.031343792 0.5241518 0.031296641
		 0.52415711 0.031296641 0.5241816 0.031339195 -0.37026283 0.022191102 -0.37023535
		 0.022143215 -0.37023264 0.022147888 -0.37025747 0.022191102 -0.028134953 -0.023494033
		 -0.028134953 -0.023547867 -0.02813038 -0.023545239 -0.028130367 -0.023496661 0.085262857
		 0.036765102 0.08529038 0.03681311 0.085285015 0.03681311 0.085260138 0.036769792
		 -0.37023339 0.020352822 -0.37026015 0.020399529 -0.37026277 0.02039497 -0.37023863
		 0.020352822 -0.45988843 0.066151552 -0.45988846 0.066222563 -0.45992595 0.066200949
		 -0.4599258 0.066172972 -0.45995027 0.066258088 -0.45995024 0.066214882 -0.46001208
		 0.066222548 -0.45997462 0.066200942;
	setAttr ".uvtk[250:349]" -0.46001208 0.066151462 -0.45997462 0.066173077 -0.45995027
		 0.066115916 -0.45995024 0.066159129 0.38043141 0.00382139 0.35678351 0.00382139 0.47310951
		 -0.064054079 0.48687321 -0.064054079 0.25212365 0.075147904 0.28223082 0.075147904
		 0.40775296 -0.0040532611 0.39022973 -0.0040532611 -0.27417815 0.082463384 -0.24520865
		 0.082463384 -0.38263366 0.0032636148 -0.3994945 0.0032636148 -0.1706571 0.0019925088
		 -0.19533324 0.0019925088 -0.29405048 -0.066156484 -0.27968872 -0.066156484 0.57939327
		 -0.22232667 0.58864272 -0.22232667 0.53019869 -0.18873517 0.5184226 -0.18873517 -0.51283079
		 -0.18141969 -0.52416146 -0.18141969 -0.39440519 -0.22507086 -0.3847537 -0.22507086
		 0.55147374 -0.44179907 0.56013513 -0.44179907 0.49498633 -0.44482872 0.48395884 -0.44482872
		 -0.47862527 -0.43751323 -0.48923564 -0.43751323 -0.36554199 -0.44543156 -0.35650402
		 -0.44543156 0.47786495 -0.728338 0.48497617 -0.728338 0.40215009 -0.77917945 0.39309645
		 -0.77917945 -0.38846189 -0.771864 -0.39717323 -0.771864 -0.28946054 -0.73313022 -0.28204012
		 -0.73313022 0.47125456 -0.83233792 0.48761037 -0.83233792 0.40993372 -0.90053284
		 0.38911036 -0.90053284 -0.38036466 -0.89321268 -0.40040091 -0.89321268 -0.29584062
		 -0.83754712 -0.27877399 -0.83754712 -0.18904354 0.051371954 -0.16161759 0.051371954
		 -0.28953192 -0.02782828 -0.30549431 -0.02782828 -0.24456961 0.0038209914 -0.2668286
		 0.0038209914 -0.36134022 -0.063662454 -0.34838513 -0.063662454 0.26780486 0.0038209914
		 0.24554594 0.0038209914 0.34935984 -0.063663237 0.36231488 -0.063663237 0.32732844
		 0.08063408 0.35475445 0.08063408 0.47120345 0.0014329187 0.45524085 0.0014329187
		 -0.40948173 -0.21251205 -0.42020881 -0.21251205 -0.45444268 -0.22102432 -0.4457365
		 -0.22102432 0.446722 -0.22102429 0.45542806 -0.22102429 0.58593106 -0.18324992 0.57520401
		 -0.18324992 -0.37759042 -0.46860701 -0.38763553 -0.46860701 -0.42800617 -0.43923289
		 -0.41985348 -0.43923289 0.42085549 -0.4392328 0.42900807 -0.4392328 0.55337811 -0.43934485
		 0.54333311 -0.43934485 -0.29353365 -0.80295914 -0.30178097 -0.80295914 -0.35832649
		 -0.72412133 -0.35163292 -0.72412133 0.35265207 -0.72412121 0.35934553 -0.72412121
		 0.4675447 -0.77369702 0.45929751 -0.77369702 -0.2879152 -0.92430782 -0.30688399 -0.92430782
		 -0.36246812 -0.82751787 -0.34707299 -0.82751787 0.34809738 -0.82751775 0.36349234
		 -0.82751775 0.4726541 -0.8950457 0.45368549 -0.8950457;
createNode polyPlanarProj -n "polyPlanarProj6";
	rename -uid "2E09D70E-435A-E272-114F-98B78FF91264";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 20 "f[51]" "f[53]" "f[63]" "f[65]" "f[75]" "f[77]" "f[87]" "f[89]" "f[99]" "f[101]" "f[111]" "f[113]" "f[123]" "f[125]" "f[135]" "f[137]" "f[147]" "f[149]" "f[159]" "f[161]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.0021815448999404907 5.9202511310577393 -1.5646219253540039e-007 ;
	setAttr ".ro" -type "double3" 0 32.377639107409856 0 ;
	setAttr ".ps" -type "double2" 0.75168625777195763 0.77040624618530273 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV9";
	rename -uid "C817130F-4B47-6387-D98C-F4B8BC395B07";
	setAttr ".uopa" yes;
	setAttr -s 398 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 9.6855592e-006 0.0015705358 9.6849244e-006
		 -9.0267683e-008 -0.00013877127 -9.0267683e-008 -2.0006679e-005 0.0015705358 -2.0007285e-005
		 -9.0267683e-008 -4.9697948e-005 0.0015705358 -4.9698298e-005 -1.4894135e-007 -7.9389516e-005
		 0.0015705358 -7.9388548e-005 -1.4894135e-007 -0.00010908131 0.0015705358 -0.00010908007
		 -9.0267683e-008 -0.0011972452 -0.00077814067 -0.0011967397 -0.00035797802 -0.0011972452
		 -0.00068966462 -0.0011963535 -0.0006048893 -0.0011958468 -0.00052185683 -0.001196165
		 -0.00043978548 -0.0028300264 -0.00037944817 -0.0028492834 -0.00070715247 -0.0028553701
		 -0.00070736866 -0.0028404729 -0.00037944817 -0.0028515332 -0.0006460654 -0.0028575873
		 -0.00064610067 -0.0028503588 -0.00058591267 -0.0028563468 -0.00058552914 -0.0028436298
		 -0.00052636996 -0.002849712 -0.00052556355 -0.0028340009 -0.00046420956 -0.002841295
		 -0.0004636528 0.076721512 0.0052016126 0.076721542 0.0050679962 0.076703884 0.0050679757
		 0.076703794 0.0052016126 0.076721542 0.0050947056 0.076703727 0.0050947103 0.076721556
		 0.0051215258 0.076703794 0.0051215384 0.076721586 0.0051481603 0.076703839 0.0051481603
		 0.076721556 0.0051748957 0.076703832 0.0051748781 -0.0028859035 -0.00029550941 -0.22437814
		 -0.00029127451 -0.00023372291 -0.00052638841 -0.0028842834 -0.00065957208 -0.002882788
		 -0.00069539517 -0.22432041 -0.00025658956 -0.22434771 -0.00031393423 -0.22431409
		 -0.00029375468 -0.0028834774 -0.00059780531 -0.002884049 -0.00063330587 -0.2277351
		 -0.00025384969 -0.22776258 -0.00031175063 -0.2277291 -0.00029132611 -0.0028773909
		 -0.00053559977 -0.0028811612 -0.00057072134 -0.2263924 -0.00026572813 -0.2264206
		 -0.00032331748 -0.22638708 -0.0003030299 -0.0028694603 -0.00047326274 -0.0028744938
		 -0.00050910556 -0.00017337498 -0.0005208003 -0.22659263 -0.00032710165 -0.22655757
		 -0.00030956938 -0.0028768261 -0.00039639624 -0.0028770815 -0.00044536457 -0.00019578919
		 -0.00051735179 -0.00020773719 -0.00053736457 -0.0029564016 -0.00030536312 -0.00027162075
		 -0.00047166937 -0.0029258367 -0.00066732318 -0.0029243999 -0.00069166103 -0.22431606
		 -0.00035414624 -0.22429346 -0.00034058493 -0.0029250504 -0.00060293043 -0.002925118
		 -0.00062722329 -0.22773056 -0.00035176193 -0.22770809 -0.00033803066 -0.0029190173
		 -0.00053701864 -0.0029212539 -0.00056085712 -0.22638887 -0.00036351863 -0.22636624
		 -0.00034990037 -0.0029113744 -0.00047324071 -0.0029148764 -0.00049710955 -0.22656412
		 -0.00036971131 -0.22654057 -0.00035792973 -0.0029336473 -0.00040447904 -0.0029333753
		 -0.00043738462 -0.00022388538 -0.00050647854 -0.00023058822 -0.00051948108 -0.0030402241
		 -0.00030645027 -0.00030873658 -0.00040338252 -0.0029760543 -0.00067151338 -0.0029742278
		 -0.00069456233 -0.22428447 -0.00040475995 -0.22426346 -0.00039205179 -0.0029753658
		 -0.00060383079 -0.0029751617 -0.00062693888 -0.22769883 -0.00040216552 -0.22767782
		 -0.00038933312 -0.0029682468 -0.00053352525 -0.0029699453 -0.00055601983 -0.22635739
		 -0.00041416739 -0.22633623 -0.0004014452 -0.0029600032 -0.00046685198 -0.0029633513
		 -0.00048908958 -0.22653669 -0.00042273826 -0.22651465 -0.00041172444 -0.0030011607
		 -0.00040657714 -0.0030005255 -0.00043744026 -0.00025387033 -0.00049351517 -0.00025877226
		 -0.00050572981 -0.0031522422 -0.00030957159 -0.00035917945 -0.00031320067 -0.0030436735
		 -0.0006792986 -0.0030419119 -0.0006983403 -0.22424157 -0.00047182583 -0.22422428
		 -0.00046139755 -0.0030431144 -0.00060663471 -0.0030429098 -0.00062564708 -0.22765552
		 -0.00046890895 -0.22763823 -0.00045837596 -0.0030342613 -0.00053104921 -0.0030355076
		 -0.00054956751 -0.2263144 -0.00048126359 -0.22629701 -0.00047082247 -0.003025189
		 -0.00045901194 -0.0030279814 -0.00047725756 -0.2264991 -0.00049308519 -0.22648101
		 -0.00048402743 -0.0030913169 -0.00041166387 -0.0030906314 -0.00043704215 -0.00029414456
		 -0.00048077092 -0.00029741699 -0.00049093284 -0.0031915894 -0.00028897083 -0.003191812
		 -0.00036147126 -0.00036097114 -0.00027310752 -0.00041430286 -0.00030112953 -0.0030687628
		 -0.00066934782 -0.0030643956 -0.00071304035 -0.22423844 -0.00050261768 -0.22419855
		 -0.00047863188 -0.0030669819 -0.00059455226 -0.0030665644 -0.00063818245 -0.22765212
		 -0.00049968087 -0.22761242 -0.00047544419 -0.0030566156 -0.00051745441 -0.0030594864
		 -0.00056006503 -0.22631118 -0.00051208795 -0.22627136 -0.0004880885 -0.0030460507
		 -0.00044363525 -0.0030526652 -0.00048546161 -0.22649841 -0.00052409276 -0.22645672
		 -0.0005032745 -0.0031236191 -0.00039608899 -0.0031218876 -0.00045452183 -0.00030685213
		 -0.00046996109 -0.0003134158 -0.00049381348 -0.0012457827 -0.00077753037 -0.0012446528
		 -0.00068923412 -0.0012424528 -0.00060427375 -0.0012411579 -0.00052170444 -0.0012411608
		 -0.00043993609 -0.00013876685 0.0015705358 -0.00016846403 0.0015705358 -0.00016845774
		 -9.0267683e-008 -0.0028298325 -0.00027459394 0.076721467 0.0052283029 0.076703846
		 0.0052282829 -0.002840278 -0.00027457633 -0.0028860285 -0.00035654713 -0.00027169642
		 -0.00059856765 -0.00019506697 -0.00055566075 -0.00027840945 -0.00055055792 -0.0029565098
		 -0.000346382 -0.00030208004 -0.00048769542 -0.0030403016 -0.00034484669 -0.00033712285
		 -0.00041824538 -0.0031523285 -0.00034109456 -0.00038240579 -0.00032537262 -0.0012419289
		 -0.00027676253 -0.0012415589 -0.00035821414 -0.0011971768 -0.00027653817 -0.22779259
		 -0.00028896809 -0.22645018 -0.00030060051 -0.22662018 -0.00030267076 -0.22655998
		 -0.00027255027 -0.11153166 0.089646168 -0.11153109 0.089672841 -0.11158653 0.089709751
		 -0.1115967 0.089613564 -0.11150987 0.089630514 -0.11151362 0.089564078 -0.11148714
		 0.089642532 -0.11143307 0.089607678 -0.11148603 0.089668915 -0.11142883 0.089697547
		 -0.11150777 0.089683868 -0.11150324 0.089748085 -0.33605501 0.0031143976 -0.33605641
		 0.0030849504 -0.33601698 0.0030600266 -0.33601329 0.0031352944 -0.33608207 0.0030710658
		 -0.33608368 0.0030243502 -0.33610675 0.0030865925 -0.33614823 0.0030645365 -0.33610559
		 0.0031159308 -0.33614546 0.0031408728 -0.33607969 0.0031298879 -0.3360779 0.0031767874
		 0.19943181 0.049426224 0.19943181 0.049396686 0.19945762 0.049411461 0.19945762 0.049381912
		 0.19948331 0.049396686 0.19948336 0.049426224 0.19945762 0.049441002 0.12814996 -0.0094882613
		 0.12814996 -0.0095396694 0.12815425 -0.0095371604 0.12815425 -0.0094907694 0.0048548719
		 -0.048212525 0.0048803757 -0.048167903 0.0048754909 -0.048167903 0.0048523499 -0.04820817
		 0.22146569 -0.038987156 0.22144018 -0.038942773 0.22143769 -0.038947102 0.22146076
		 -0.038987156 0.19265252 -0.0095385406 0.19265252 -0.009487032 0.19264819 -0.0094895465
		 0.19264813 -0.0095360316 0.10553209 0.095251255 0.10550634 0.095206268 0.1055113
		 0.095206268 0.10553462 0.095246851 0.31336898 -0.061047532 0.31339449 -0.0610919
		 0.31339702 -0.061087571 0.31337398 -0.061047532 0.24283615 0.019725008 0.24283618
		 0.019657509 0.24287166 0.019678026 0.24287167 0.019704625 0.24289498 0.019623734
		 0.24289486 0.019664798 0.24295354 0.019657509 0.24291807 0.019678041;
	setAttr ".uvtk[250:397]" 0.24295354 0.019725088 0.24291807 0.019704549 0.24289498
		 0.019758869 0.24289486 0.019717798 0.039985672 0.04905767 0.039601635 0.04905767
		 0.047047429 0.053234376 0.047213148 0.05235374 0.36845824 -0.071958207 0.37217289
		 -0.071958207 0.38262689 -0.056742154 0.38054484 -0.055898517 0.32492435 -0.080250502
		 0.32850513 -0.080250502 0.31930056 -0.063346818 0.31716496 -0.064000197 0.29371852
		 0.0014443521 0.29342079 0.0014443521 0.2873871 0.0052490165 0.28761736 0.0060757962
		 0.043408658 0.045001268 0.043392111 0.044646632 0.40268329 -0.038548339 0.40115207
		 -0.038210522 0.2998983 -0.044396374 0.29841483 -0.044839974 0.2909126 -0.0020053263
		 0.29094508 -0.0016757484 0.042424127 0.041956697 0.042334136 0.042003077 0.39892232
		 -0.0044425516 0.39739063 -0.0044890558 0.29757282 -0.012897938 0.29607868 -0.01290961
		 0.29185867 -0.0037943327 0.29179725 -0.0038399163 0.042244222 0.036778085 0.042219486
		 0.036894966 0.38650832 0.038433362 0.38529855 0.038325135 0.31243816 0.029647684
		 0.31118822 0.029788038 0.29183361 -0.0076900795 0.29183048 -0.0077981972 0.041014645
		 0.035386607 0.041179527 0.035948057 0.38835403 0.054864157 0.38578266 0.054373484
		 0.3105768 0.046769295 0.30778366 0.047087174 0.29262492 -0.0082491869 0.29283029
		 -0.0087358896 0.088075772 -0.048262388 0.091524608 -0.048262388 0.080906235 -0.033225432
		 0.078745864 -0.033775453 0.17918704 -0.00062355452 0.17876764 -0.00062355452 0.17002533
		 0.0038390667 0.1701488 0.0044174339 0.26370764 0.0012086463 0.2632806 0.0012086463
		 0.27037191 0.0049674935 0.27050412 0.0041897492 0.18695131 -0.077129535 0.19037698
		 -0.077129535 0.20014095 -0.062840216 0.19800675 -0.062093284 0.064083375 -0.011957581
		 0.062540486 -0.012285732 0.17184743 -0.0039667734 0.1718415 -0.0035737699 0.26726258
		 -0.0035485828 0.26718745 -0.0038594648 0.21936671 -0.043193717 0.21777318 -0.04288967
		 0.062684439 0.020155124 0.061151337 0.020132257 0.17776224 -0.0092312088 0.17764148
		 -0.0092137894 0.26639521 -0.0071959416 0.26623684 -0.0071713668 0.21574125 -0.0088269301
		 0.21417333 -0.0088512618 0.076537505 0.063703157 0.075260848 0.063814864 0.17556381
		 -0.015102097 0.17544828 -0.015213769 0.2660718 -0.013112891 0.2659907 -0.013019778
		 0.2039485 0.034640022 0.20270956 0.034547903 0.074602105 0.080876544 0.071773969
		 0.081165574 0.17854902 -0.015642021 0.17837203 -0.015910881 0.26487905 -0.014836546
		 0.26490831 -0.014307524 0.20572686 0.051235352 0.20309673 0.050717343 0.63404095
		 0.1577048 0.59802324 0.1577048 0.74793452 0.04842867 0.7682175 0.048447717 0.28562796
		 0.2289754 0.31359005 0.21901561 0.47547993 0.1201818 0.45965129 0.12322938 -0.17967519
		 0.092886552 -0.15033945 0.082430728 -0.25412899 0.039087199 -0.27484244 0.040790554
		 0.15580499 0.15912139 0.12029768 0.15912139 -0.016288619 0.062850952 0.00022941011
		 0.060830802 0.88686591 -0.17311347 0.90006632 -0.17307782 0.61507714 -0.083392195
		 0.60453993 -0.082959726 -0.39914063 -0.10018387 -0.41187471 -0.099993207 -0.15929501
		 -0.099730581 -0.14995511 -0.10104065 0.85109252 -0.46727887 0.86313599 -0.46722609
		 0.57564044 -0.35432899 0.56572235 -0.35501373 -0.36042628 -0.36955556 -0.37049058
		 -0.36923447 -0.14921397 -0.31935218 -0.14052309 -0.31918877 0.7564491 -0.84551394
		 0.76624805 -0.84547025 0.46013203 -0.71190536 0.45200056 -0.71276921 -0.26923725
		 -0.74279368 -0.27741393 -0.74254405 -0.042261481 -0.62211967 -0.034827609 -0.62129003
		 0.75602925 -0.9828918 0.77849787 -0.98280114 0.44688863 -0.83758628 0.42842773 -0.83996624
		 -0.26229194 -0.86993444 -0.28205794 -0.86947829 -0.026074138 -0.73658633 -0.0090069734
		 -0.73409951;
createNode polyMapSewMove -n "polyMapSewMove1";
	rename -uid "A307FEE7-41C5-5A45-7546-48B106025FB6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[78]";
createNode polyMapCut -n "polyMapCut4";
	rename -uid "EB0664AC-4C35-C96A-6477-24B2F6D914B2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[66]" "e[71]" "e[74]" "e[77]" "e[80]";
createNode polyTweakUV -n "polyTweakUV10";
	rename -uid "A6A43951-4843-0ED8-6E12-CC9212231B6F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[33]" -type "float2" 0.020323366 0.0011521131 ;
	setAttr ".uvtk[34]" -type "float2" 0.014653832 -0.0085609034 ;
	setAttr ".uvtk[400]" -type "float2" 0.0056658685 0.0096953213 ;
	setAttr ".uvtk[401]" -type "float2" -2.9802322e-008 -5.2154064e-008 ;
createNode polyMapSewMove -n "polyMapSewMove2";
	rename -uid "FDDA562C-4150-6B27-F43A-2A957566BC88";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[75]";
createNode polyTweakUV -n "polyTweakUV11";
	rename -uid "22FC02CC-41FB-CE12-496F-89B3B16D371F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[30]" -type "float2" 0.046076715 -0.024193138 ;
	setAttr ".uvtk[31]" -type "float2" 0.029332578 -0.033874266 ;
	setAttr ".uvtk[400]" -type "float2" 0.031508088 0.0011510253 ;
	setAttr ".uvtk[401]" -type "float2" 0.014653921 -0.0085610822 ;
createNode polyMapSewMove -n "polyMapSewMove3";
	rename -uid "43CD9F03-4CDF-FE84-54E0-06ACE202F845";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[72]";
createNode polyTweakUV -n "polyTweakUV12";
	rename -uid "D9721E60-40BE-B932-4786-11B558D4F4A3";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[37]" -type "float2" 0.0055899918 -0.0097002871 ;
	setAttr ".uvtk[38]" -type "float2" 2.9802322e-008 -2.6077032e-008 ;
	setAttr ".uvtk[394]" -type "float2" 0.020227581 -0.0012617484 ;
	setAttr ".uvtk[395]" -type "float2" 0.014626056 0.0084394403 ;
createNode polyMapSewMove -n "polyMapSewMove4";
	rename -uid "4DFD071F-4345-73F1-CD85-1D99EBE56178";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[81]";
createNode polyTweakUV -n "polyTweakUV13";
	rename -uid "9FF98074-44BE-8E9C-C9F8-CE97517D15B3";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[39]" -type "float2" 0.031377524 -0.0012670532 ;
	setAttr ".uvtk[40]" -type "float2" 0.014626056 0.0084394515 ;
	setAttr ".uvtk[398]" -type "float2" 0.046039313 0.023966555 ;
	setAttr ".uvtk[399]" -type "float2" 0.029287606 0.033696055 ;
createNode polyMapSewMove -n "polyMapSewMove5";
	rename -uid "571F9B39-4959-B7A5-8E2F-89BCC1A77A70";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[83]";
createNode polyTweakUV -n "polyTweakUV14";
	rename -uid "03546A9A-4158-29F0-94AE-47B0221EFE83";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[29]" -type "float2" 0.051653445 0.033694241 ;
	setAttr ".uvtk[32]" -type "float2" 0.029284418 0.03369626 ;
	setAttr ".uvtk[167]" -type "float2" 0.051598072 0.067364216 ;
	setAttr ".uvtk[168]" -type "float2" 0.029348195 0.067341492 ;
createNode polyMapSewMove -n "polyMapSewMove6";
	rename -uid "94963249-41AB-6EEB-9671-72889005E6B0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[68]";
createNode polyTweakUV -n "polyTweakUV15";
	rename -uid "522564AE-498F-5727-282A-178E4D345EA0";
	setAttr ".uopa" yes;
	setAttr -s 19 ".uvtk";
	setAttr ".uvtk[29]" -type "float2" -0.014580458 0.11997569 ;
	setAttr ".uvtk[30]" -type "float2" -0.014553517 0.11984766 ;
	setAttr ".uvtk[31]" -type "float2" -0.014526516 0.11989442 ;
	setAttr ".uvtk[32]" -type "float2" -0.014526397 0.1199757 ;
	setAttr ".uvtk[33]" -type "float2" -0.014428884 0.11980668 ;
	setAttr ".uvtk[34]" -type "float2" -0.014455706 0.11985368 ;
	setAttr ".uvtk[35]" -type "float2" -0.014330596 0.11989415 ;
	setAttr ".uvtk[36]" -type "float2" -0.014384776 0.11989419 ;
	setAttr ".uvtk[37]" -type "float2" -0.014357656 0.12002239 ;
	setAttr ".uvtk[38]" -type "float2" -0.014384657 0.11997546 ;
	setAttr ".uvtk[39]" -type "float2" -0.014482349 0.12006322 ;
	setAttr ".uvtk[40]" -type "float2" -0.014455467 0.12001622 ;
	setAttr ".uvtk[167]" -type "float2" -0.01458028 0.11989434 ;
	setAttr ".uvtk[210]" -type "float2" -0.014455587 0.11993495 ;
	setAttr ".uvtk[391]" -type "float2" -0.014428467 0.12006319 ;
	setAttr ".uvtk[392]" -type "float2" -0.014330596 0.11997548 ;
	setAttr ".uvtk[393]" -type "float2" -0.014358014 0.11984725 ;
	setAttr ".uvtk[394]" -type "float2" -0.014482945 0.11980669 ;
	setAttr ".uvtk[395]" -type "float2" -0.014553279 0.12002274 ;
createNode polyMapSewMove -n "polyMapSewMove7";
	rename -uid "685A6EFB-4AAB-A5EB-D857-85872528C30B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[62]";
createNode polyMapCut -n "polyMapCut5";
	rename -uid "91CF0926-41C3-96E7-56A6-1E877D93FE02";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[54:55]" "e[57]" "e[59]" "e[61]" "e[63]";
createNode polyTweakUV -n "polyTweakUV16";
	rename -uid "15581CE5-49BB-4258-8D33-AB93E8840DE1";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[240]" -type "float2" 0.0055843592 0.009626016 ;
	setAttr ".uvtk[241]" -type "float2" 0.0057131052 0.0096100271 ;
	setAttr ".uvtk[399]" -type "float2" 0.0056351423 0.0096550733 ;
	setAttr ".uvtk[400]" -type "float2" 0.0055843592 0.0095359534 ;
createNode polyMapSewMove -n "polyMapSewMove8";
	rename -uid "8886B974-4F65-468D-CD23-5791F02F2610";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[60]";
createNode polyTweakUV -n "polyTweakUV17";
	rename -uid "7CF6BFB2-4E7F-9E6C-FB5B-29BDAFC869F4";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[237]" -type "float2" 0.016621709 0.0096164644 ;
	setAttr ".uvtk[239]" -type "float2" 0.01668191 0.0094754249 ;
	setAttr ".uvtk[399]" -type "float2" 0.01668191 0.0095820725 ;
	setAttr ".uvtk[400]" -type "float2" 0.016529441 0.009563148 ;
createNode polyMapSewMove -n "polyMapSewMove9";
	rename -uid "F51F2E1F-4EBC-A342-1AA4-B1AAFC628A59";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[58]";
createNode polyTweakUV -n "polyTweakUV18";
	rename -uid "FF75EE78-416B-46A5-7986-099EC99580DB";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[236]" -type "float2" 0.022015065 -8.3036721e-005 ;
	setAttr ".uvtk[237]" -type "float2" 0.022089869 1.6368926e-005 ;
	setAttr ".uvtk[399]" -type "float2" 0.022089988 -3.9719045e-005 ;
	setAttr ".uvtk[400]" -type "float2" 0.022014946 5.9328973e-005 ;
createNode polyMapSewMove -n "polyMapSewMove10";
	rename -uid "0CACB190-41E5-1C18-6C94-3683DEC6D120";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[56]";
createNode polyTweakUV -n "polyTweakUV19";
	rename -uid "D6DBF687-4A83-87E9-8CA3-5190C55AAE4B";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[235]" -type "float2" 0.016399294 -0.0096352324 ;
	setAttr ".uvtk[242]" -type "float2" 0.016577989 -0.009657152 ;
	setAttr ".uvtk[391]" -type "float2" 0.016578048 -0.0095322505 ;
	setAttr ".uvtk[400]" -type "float2" 0.016507298 -0.0096971765 ;
createNode polyMapSewMove -n "polyMapSewMove11";
	rename -uid "7E402F1F-487C-EB42-288F-3B884FD7A691";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[65]";
createNode polyTweakUV -n "polyTweakUV20";
	rename -uid "72E49A6E-4B67-5B91-C80B-11B91CA12906";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[240]" -type "float2" 0.0055491328 -0.0096777454 ;
	setAttr ".uvtk[241]" -type "float2" 0.0055115819 -0.0095896497 ;
	setAttr ".uvtk[391]" -type "float2" 0.0055115819 -0.0096562579 ;
	setAttr ".uvtk[392]" -type "float2" 0.0056067705 -0.0096444413 ;
createNode polyMapSewMove -n "polyMapSewMove12";
	rename -uid "B2ED9111-402B-F2B6-692D-D3A6FB5C2DAD";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[64]";
createNode polyTweakUV -n "polyTweakUV21";
	rename -uid "69BEB1B3-4C66-56FC-EF87-6AA40457C3A4";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk";
	setAttr ".uvtk[51]" -type "float2" 0.030967772 0.22447746 ;
	setAttr ".uvtk[52]" -type "float2" 0.031893909 0.22401746 ;
	setAttr ".uvtk[53]" -type "float2" 0.031574368 0.22456335 ;
	setAttr ".uvtk[76]" -type "float2" 0.032549143 0.22452129 ;
	setAttr ".uvtk[77]" -type "float2" 0.032334566 0.22488798 ;
	setAttr ".uvtk[98]" -type "float2" 0.033372283 0.22501867 ;
	setAttr ".uvtk[99]" -type "float2" 0.033171475 0.2253619 ;
	setAttr ".uvtk[120]" -type "float2" 0.034462452 0.22569753 ;
	setAttr ".uvtk[121]" -type "float2" 0.034297705 0.22597925 ;
	setAttr ".uvtk[144]" -type "float2" 0.034960091 0.22574271 ;
	setAttr ".uvtk[145]" -type "float2" 0.034581244 0.2263907 ;
	setAttr ".uvtk[182]" -type "float2" 0.031516969 0.22353946 ;
createNode polyMapSewMove -n "polyMapSewMove13";
	rename -uid "845FD368-46F7-9BDF-E220-E2B7AF877309";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[48]";
createNode polyTweakUV -n "polyTweakUV22";
	rename -uid "C44D6EA1-45D6-E8BA-5CA1-5792D90E5152";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk";
	setAttr ".uvtk[42]" -type "float2" 0.12601042 0.20133431 ;
	setAttr ".uvtk[46]" -type "float2" 0.12575233 0.24480197 ;
	setAttr ".uvtk[47]" -type "float2" 0.14862382 0.21076021 ;
	setAttr ".uvtk[48]" -type "float2" 0.14847261 0.23605746 ;
	setAttr ".uvtk[72]" -type "float2" 0.18139929 0.21510741 ;
	setAttr ".uvtk[73]" -type "float2" 0.18129516 0.23210475 ;
	setAttr ".uvtk[94]" -type "float2" 0.21987307 0.21588713 ;
	setAttr ".uvtk[95]" -type "float2" 0.21977502 0.23179972 ;
	setAttr ".uvtk[116]" -type "float2" 0.27124584 0.21763095 ;
	setAttr ".uvtk[117]" -type "float2" 0.27116591 0.23069546 ;
	setAttr ".uvtk[140]" -type "float2" 0.28941005 0.20924923 ;
	setAttr ".uvtk[141]" -type "float2" 0.28922623 0.23929882 ;
createNode polyMapSewMove -n "polyMapSewMove14";
	rename -uid "0BF2F86E-4247-8A8D-68E1-E4A3190A758F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[46]";
createNode polyTweakUV -n "polyTweakUV23";
	rename -uid "D4E094D2-4965-B687-B6F1-5CA3688BA296";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk";
	setAttr ".uvtk[56]" -type "float2" 0.11828557 0.17300421 ;
	setAttr ".uvtk[57]" -type "float2" 0.078480273 0.17074153 ;
	setAttr ".uvtk[58]" -type "float2" 0.099706441 0.15873808 ;
	setAttr ".uvtk[80]" -type "float2" 0.066393942 0.14126016 ;
	setAttr ".uvtk[81]" -type "float2" 0.080648869 0.13318412 ;
	setAttr ".uvtk[102]" -type "float2" 0.048553795 0.10874921 ;
	setAttr ".uvtk[103]" -type "float2" 0.061889499 0.10117756 ;
	setAttr ".uvtk[124]" -type "float2" 0.025288224 0.065026209 ;
	setAttr ".uvtk[125]" -type "float2" 0.036229759 0.058805227 ;
	setAttr ".uvtk[148]" -type "float2" 0.0095575452 0.053905368 ;
	setAttr ".uvtk[149]" -type "float2" 0.034714848 0.039598823 ;
	setAttr ".uvtk[183]" -type "float2" 0.081808954 0.19361798 ;
createNode polyMapSewMove -n "polyMapSewMove15";
	rename -uid "C02D71D5-42CC-ED84-FBD4-FBB1E4CF9ACB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[50]";
createNode polyTweakUV -n "polyTweakUV24";
	rename -uid "981A4981-44EF-8D16-1DB1-9D8ED57DEADC";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk";
	setAttr ".uvtk[62]" -type "float2" 0.14610785 0.10543488 ;
	setAttr ".uvtk[63]" -type "float2" 0.18977451 0.10406005 ;
	setAttr ".uvtk[84]" -type "float2" 0.15148312 0.048582502 ;
	setAttr ".uvtk[85]" -type "float2" 0.18084407 0.047653973 ;
	setAttr ".uvtk[106]" -type "float2" 0.15028799 -0.017932624 ;
	setAttr ".uvtk[107]" -type "float2" 0.17778379 -0.018811658 ;
	setAttr ".uvtk[128]" -type "float2" 0.14991045 -0.10680109 ;
	setAttr ".uvtk[129]" -type "float2" 0.17248333 -0.10752253 ;
	setAttr ".uvtk[152]" -type "float2" 0.13424855 -0.13761544 ;
	setAttr ".uvtk[153]" -type "float2" 0.18614995 -0.13927159 ;
	setAttr ".uvtk[184]" -type "float2" 0.13198638 0.14399627 ;
	setAttr ".uvtk[185]" -type "float2" 0.20700353 0.1416364 ;
createNode polyMapSewMove -n "polyMapSewMove16";
	rename -uid "A586FDE2-4A84-AFA3-2EC7-428F77A6159D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[52]";
createNode polyTweakUV -n "polyTweakUV25";
	rename -uid "4BA073C0-4F50-64F8-2128-6CB9FD5A9B34";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk";
	setAttr ".uvtk[61]" -type "float2" 0.37737411 -0.015248545 ;
	setAttr ".uvtk[66]" -type "float2" 0.38207692 -0.027146362 ;
	setAttr ".uvtk[67]" -type "float2" 0.39523649 -0.027261667 ;
	setAttr ".uvtk[88]" -type "float2" 0.38479763 -0.043951176 ;
	setAttr ".uvtk[89]" -type "float2" 0.39304215 -0.043489598 ;
	setAttr ".uvtk[110]" -type "float2" 0.38702649 -0.062249497 ;
	setAttr ".uvtk[111]" -type "float2" 0.39437872 -0.061158217 ;
	setAttr ".uvtk[132]" -type "float2" 0.39231366 -0.085527159 ;
	setAttr ".uvtk[133]" -type "float2" 0.39818758 -0.084210843 ;
	setAttr ".uvtk[156]" -type "float2" 0.39066273 -0.094792083 ;
	setAttr ".uvtk[157]" -type "float2" 0.40416759 -0.091201976 ;
	setAttr ".uvtk[171]" -type "float2" 0.40053946 -0.01586137 ;
createNode polyMapSewMove -n "polyMapSewMove17";
	rename -uid "03E46270-4B82-543F-A7F7-63B8177327D4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[53]";
createNode polyTweakUV -n "polyTweakUV26";
	rename -uid "29395E1D-401A-2976-73CB-699F4D60F02B";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk";
	setAttr ".uvtk[43]" -type "float2" 0.40314996 0.021612823 ;
	setAttr ".uvtk[69]" -type "float2" 0.41123754 0.059263587 ;
	setAttr ".uvtk[91]" -type "float2" 0.4253695 0.1019709 ;
	setAttr ".uvtk[113]" -type "float2" 0.44345832 0.15895535 ;
	setAttr ".uvtk[136]" -type "float2" 0.45966268 0.17559157 ;
	setAttr ".uvtk[137]" -type "float2" 0.42670476 0.18702465 ;
	setAttr ".uvtk[170]" -type "float2" 0.35765809 0.0090715736 ;
	setAttr ".uvtk[172]" -type "float2" 0.37526268 0.030899331 ;
	setAttr ".uvtk[174]" -type "float2" 0.39241737 0.06577225 ;
	setAttr ".uvtk[176]" -type "float2" 0.40784758 0.10807423 ;
	setAttr ".uvtk[178]" -type "float2" 0.42912173 0.16394272 ;
	setAttr ".uvtk[386]" -type "float2" 0.40610218 -0.0063008666 ;
createNode polyMapSewMove -n "polyMapSewMove18";
	rename -uid "167E8C4A-4198-B81D-2080-7D9E661F2EFE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[44]";
createNode polyTweakUV -n "polyTweakUV27";
	rename -uid "FA56DB79-483C-010D-A327-62819B1A1DFF";
	setAttr ".uopa" yes;
	setAttr -s 385 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" 1.4597469e-005 -0.00028877691 1.4597516e-005
		 1.8799074e-007 4.1905041e-005 1.8799074e-007 2.0059617e-005 -0.00028877691 2.005922e-005
		 1.8799074e-007 2.55184e-005 -0.00028877691 2.5518289e-005 1.7500679e-007 3.0986663e-005
		 -0.00028877691 3.0983269e-005 1.7500679e-007 3.6443824e-005 -0.00028877691 3.6442863e-005
		 1.8799074e-007 0.38438851 -0.0020492512 0.38438842 -0.0021265177 0.38438851 -0.0020655307
		 0.38438836 -0.0020811039 0.38438827 -0.0020963843 0.38438833 -0.002111471 0.38651931
		 0.0015825799 0.38652283 0.0016428742 0.38652399 0.001642914 0.38652122 0.0015825799
		 0.38652328 0.0016316307 0.38652438 0.0016316382 0.38652307 0.0016205669 0.38652414
		 0.0016204889 0.38652179 0.0016096026 0.38652292 0.0016094587 0.38652003 0.0015981665
		 0.3865214 0.001598064 -0.41046631 -0.0010056021 -0.37827414 -0.021746131 -0.38245466
		 -0.0065041101 -0.39934897 0.010247589 -0.34409082 -0.0042272736 -0.35940358 -0.000150771
		 -0.34208083 0.034229852 -0.35324466 0.022953806 -0.37434304 0.054991417 -0.37013963
		 0.039705191 -0.40851143 0.037421733 -0.39319158 0.033352517 0.38652956 0.001567136
		 -0.38780296 -0.057215724 -0.45777926 -0.041916337 0.3865293 0.0016341277 0.38652903
		 0.001640704 -0.32926095 -0.04149805 -0.36709267 -0.084385909 -0.33302289 -0.075237364
		 0.38652918 0.0016227491 0.38652924 0.0016292938 -0.30251932 0.028610477 -0.28417739
		 -0.026102027 -0.27521452 0.0080381418 0.38652802 0.0016112995 0.38652873 0.0016177584
		 -0.34968629 0.086646706 -0.29316774 0.074691869 -0.31828889 0.099538065 0.38652658
		 0.0015998323 0.38652748 0.0016064224 -0.43472594 0.063138485 -0.38498065 0.11702841
		 -0.41907829 0.10774658 0.38652796 0.0015856989 0.38652796 0.0015947048 -0.46783233
		 0.05813954 -0.47586241 0.024648124 0.38654256 0.0015689497 -0.48671964 -0.077400818
		 0.38653696 0.0016355442 0.38653666 0.0016400139 -0.34964013 -0.1270595 -0.32674924
		 -0.12090914 0.38653678 0.0016236914 0.38653681 0.0016281751 -0.2384478 -0.032119747
		 -0.23242459 -0.0091887703 0.3865357 0.0016115584 0.38653609 0.001615945 -0.26507008
		 0.11132472 -0.28193527 0.1280314 0.38653424 0.0015998271 0.38653493 0.0016042185
		 -0.4026069 0.15972468 -0.42553473 0.15348689 0.38653839 0.0015871845 0.3865383 0.0015932376
		 -0.51228708 0.061080355 -0.5159567 0.039783973 0.386558 0.0015691492 -0.52479601
		 -0.11496712 0.38654616 0.0016363164 0.38654587 0.0016405598 -0.3349849 -0.17868082
		 -0.31355515 -0.17292224 0.38654602 0.0016238728 0.38654599 0.0016281137 -0.18635795
		 -0.045022335 -0.1807161 -0.023562046 0.38654473 0.0016109223 0.38654506 0.0016150618
		 -0.22777639 0.15000843 -0.24354768 0.16566086 0.38654324 0.0015986502 0.38654384
		 0.0016027412 -0.41744077 0.21145082 -0.43891436 0.20561659 0.38655084 0.0015875733
		 0.38655069 0.001593249 -0.56026137 0.066153385 -0.5618006 0.046762299 0.38657862
		 0.0015697224 -0.57478893 -0.16550757 0.38655859 0.0016377538 0.38655832 0.0016412459
		 -0.3144682 -0.24736071 -0.2968739 -0.24263363 0.3865585 0.001624382 0.3865585 0.0016278846
		 -0.116569 -0.061319411 -0.11193753 -0.043706108 0.38655686 0.0016104587 0.38655713
		 0.0016138692 -0.17863795 0.20230906 -0.19157447 0.21516344 0.38655522 0.001597215
		 0.38655576 0.0016005689 -0.43820271 0.28027138 -0.45583162 0.27548158 0.38656738
		 0.0015885088 0.38656729 0.0015931701 -0.62273198 0.066354401 -0.62282807 0.050600942
		 0.38658586 0.0015659333 0.38658583 0.0015792735 -0.60094887 -0.17435025 -0.57224357
		 -0.20429276 0.38656321 0.0016359114 0.38656244 0.0016439637 -0.31935188 -0.27482593
		 -0.27888313 -0.26395306 0.38656288 0.0016221558 0.38656285 0.0016301798 -0.095169015
		 -0.079191625 -0.084507033 -0.038684908 0.38656101 0.0016079633 0.38656154 0.001615804
		 -0.15241808 0.21188442 -0.18216141 0.24144413 0.38655904 0.00159438 0.38656029 0.0016020733
		 -0.43339008 0.30781382 -0.47392309 0.29679868 0.38657334 0.0015856383 0.38657302
		 0.0015963846 -0.64538866 0.076011069 -0.64417231 0.039460611 0.38439748 -0.0020493711
		 0.38439724 -0.0020656192 0.38439685 -0.0020812443 0.38439661 -0.0020964195 0.38439661
		 -0.0021114468 4.1908432e-005 -0.00028877691 4.7358142e-005 -0.00028877691 4.7359692e-005
		 1.8799074e-007 0.38651928 0.0015632889 -0.39349902 -0.017708352 0.38652122 0.0015632859
		 0.38652962 0.0015783644 -0.40282837 -0.05326039 -0.44990695 0.00442283 -0.43328059
		 -0.066841736 0.38654256 0.0015764945 -0.47031698 -0.094477735 0.38655797 0.0015762118
		 -0.50954849 -0.13091233 0.38657862 0.0015755215 -0.56230974 -0.17854719 0.38439673
		 -0.0021414503 0.3843967 -0.0021264763 0.38438851 -0.0021414915 -0.31791666 -0.030057119
		 -0.30651101 0.04396826 -0.36492327 0.090550363 -0.42350101 0.074603394 0.051754262
		 -0.00022807368 0.051754195 -0.00023296446 0.051764347 -0.00023972345 0.051766157
		 -0.00022208678 0.051750284 -0.00022520751 0.051750895 -0.00021301561 0.051746096
		 -0.00022740805 0.051736135 -0.00022099548 0.051745906 -0.00023222479 0.051735349
		 -0.00023748167 0.051749833 -0.00023498033 0.051748991 -0.00024674871 0.42016646 0.072015695
		 0.42016667 0.072021112 0.42015943 0.072025679 0.42015874 0.072011858 0.42017132 0.072023645
		 0.42017162 0.072032221 0.42017594 0.072020799 0.42018351 0.072024852 0.42017573 0.072015405
		 0.42018306 0.072010845 0.4201709 0.072012857 0.42017058 0.072004244 -0.37629732 0.01660059
		 -0.42979479 -0.0060257935 -0.39711478 -0.039446898 -0.39586562 -0.035034947 -0.42535597
		 -0.0048758695 -0.72292143 0.065167256 -0.72292578 0.06515979 -0.72292495 0.06515979
		 -0.72292101 0.065166533 -0.54473257 0.077526219 -0.54472804 0.077518329 -0.54472756
		 0.077519096 -0.54473168 0.077526219 -0.049458694 0.099759713 -0.049458694 0.099750176
		 -0.049457885 0.099750631 -0.049457859 0.099759251 -0.39754429 -0.042853896 -0.39753926
		 -0.04284507 -0.39754021 -0.04284507 -0.39754477 -0.042853035 -0.64406896 0.071960665
		 -0.64407372 0.07196898 -0.64407414 0.071968161 -0.64406991 0.071960665 -0.37524828
		 -0.050295692 -0.37524715 -0.050295692 -0.3752512 -0.050291069 -0.37525186 -0.0502913
		 -0.52708316 0.008973279 -0.52708197 0.008973279 -0.52707827 0.0089779291 -0.52707905
		 0.0089781582 -0.56259435 0.010825534 -0.56259328 0.010825534 -0.56259632 0.010830191
		 -0.56259704 0.01083003 -0.54526019 -0.00028951475 -0.54525906 -0.00028951475 -0.5452556
		 -0.00028500496;
	setAttr ".uvtk[250:384]" -0.54525632 -0.00028478328 -0.3752577 -0.050285038
		 -0.37525818 -0.050285134 -0.52707213 0.0089840945 -0.52707261 0.0089841885 -0.56260186
		 0.010835999 -0.5626024 0.01083589 -0.54524952 -0.00027880966 -0.54525 -0.00027872191
		 -0.37525648 -0.050274458 -0.37525699 -0.05027445 -0.52707338 0.0089950487 -0.52707392
		 0.0089950375 -0.56260228 0.010845338 -0.56260276 0.010845337 -0.54525077 -0.00026800885
		 -0.54525119 -0.00026802302 -0.37525272 -0.050260969 -0.37525314 -0.050260931 -0.52707732
		 0.0090089059 -0.52707773 0.0090088788 -0.56259805 0.010857894 -0.56259841 0.010857932
		 -0.54525465 -0.0002543157 -0.54525501 -0.00025434204 -0.37525272 -0.050255954 -0.3752535
		 -0.050255809 -0.52707672 0.0090141669 -0.52707762 0.0090140374 -0.56259853 0.010862858
		 -0.5625993 0.010862945 -0.54525411 -0.00024911787 -0.54525489 -0.00024924951 0.1364042
		 -0.00028951443 0.13640538 -0.00028951443 0.13640162 -0.00028492525 0.13640097 -0.00028507368
		 0.13855554 0.001563066 0.13855667 0.001563066 0.13855946 0.0015679011 0.13855876
		 0.0015680552 0.14072524 -0.00028892784 0.14072646 -0.00028892784 0.14072242 -0.00028398016
		 0.14072177 -0.00028420085 0.14105076 -0.00028940593 0.14105189 -0.00028940593 0.14105532
		 -0.00028500977 0.14105463 -0.00028480706 0.13639638 -0.00027794528 0.13639583 -0.0002780332
		 0.13856493 0.0015743271 0.13856445 0.0015744329 0.14071597 -0.00027734466 0.14071545
		 -0.00027743113 0.14106128 -0.00027845471 0.14106072 -0.0002783713 0.13639615 -0.00026755733
		 0.13639574 -0.00026756522 0.13856512 0.0015847187 0.13856466 0.0015847237 0.14071718
		 -0.00026582472 0.14071667 -0.00026581474 0.14106008 -0.00026743306 0.14105955 -0.00026743824
		 0.1364004 -0.0002535572 0.13640004 -0.00025353173 0.13856073 0.0015985491 0.13856038
		 0.0015985137 0.14072104 -0.00025111143 0.14072061 -0.0002510894 0.14105627 -0.00025342451
		 0.14105594 -0.0002534489 0.1364 -0.00024810669 0.13639905 -0.00024803256 0.13856182
		 0.0016039883 0.13856089 0.0016039133 0.14072095 -0.00024565804 0.1407201 -0.0002455033
		 0.14105687 -0.00024812561 0.14105599 -0.00024826662 -0.43276781 -0.015075075 -0.43276933
		 -0.015075075 -0.43276557 -0.015081475 -0.43276486 -0.015081467 -0.096022308 -0.10398409
		 -0.096022308 -0.10398626 -0.096014649 -0.10399013 -0.096014552 -0.10398946 -0.50472909
		 -0.024352841 -0.50472909 -0.024355195 -0.50472349 -0.024346676 -0.50472432 -0.024346285
		 -0.13620482 -0.048416875 -0.1362067 -0.048416875 -0.13621034 -0.04842069 -0.13621028
		 -0.048421193 -0.43276224 -0.015088826 -0.43276182 -0.015088811 -0.096010603 -0.10399326
		 -0.096010551 -0.10399316 -0.5047282 -0.02433541 -0.5047285 -0.02433536 -0.13621587
		 -0.048413955 -0.13621627 -0.048414283 -0.43276277 -0.015096048 -0.43276241 -0.015096039
		 -0.096011855 -0.10399512 -0.096011795 -0.10399526 -0.50472701 -0.02433696 -0.50472689
		 -0.024336893 -0.13622186 -0.048403144 -0.13622223 -0.048403107 -0.4327642 -0.015104147
		 -0.43276399 -0.015104136 -0.096017599 -0.10399837 -0.096017554 -0.10399857 -0.50472635
		 -0.024343848 -0.50472623 -0.024343785 -0.13621734 -0.048393108 -0.13621758 -0.048392899
		 -0.43276352 -0.015107109 -0.43276298 -0.015107089 -0.096021086 -0.10399867 -0.096020877
		 -0.10399919 -0.50472558 -0.024344465 -0.50472546 -0.024344362 -0.13621259 -0.04839059
		 -0.1362132 -0.048389982 -0.39740998 0.048643269 -0.33795711 0.018875973 -0.355221
		 -0.015486126 -0.41467494 0.014335462 -0.35901311 0.050966114 -0.44579706 -0.010765701;
createNode polyMapSewMove -n "polyMapSewMove19";
	rename -uid "275CC139-42A7-411C-4D31-148C33938C09";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[282]";
createNode polyTweakUV -n "polyTweakUV28";
	rename -uid "FB617638-4D2B-6F5F-4F83-97936C02568E";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk[229:232]" -type "float2" -0.048762918 0.032666299
		 -0.011555389 -0.015526358 -0.0093297362 -0.010016348 -0.042905271 0.033472452;
createNode polyMapSewMove -n "polyMapSewMove20";
	rename -uid "52F60E14-427B-C3B0-CD37-D1A7DD79BFF0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[322]";
createNode polyTweakUV -n "polyTweakUV29";
	rename -uid "1F24B029-4F8F-367A-DFE8-E4A99AB39CA0";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk[225:228]" -type "float2" -0.060599223 0.23106718 -0.093194775
		 0.17388666 -0.086802699 0.17390332 -0.057388708 0.22550282;
createNode polyMapSewMove -n "polyMapSewMove21";
	rename -uid "75B56FA6-43B2-4C80-8864-8CB32D5D4B1B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[314]";
createNode polyTweakUV -n "polyTweakUV30";
	rename -uid "4BC820E0-418A-5019-1303-D4AA22959DE4";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk[221:224]" -type "float2" -0.43242973 0.28824434 -0.46547964
		 0.32088509 -0.46664426 0.3164798 -0.43682015 0.287025;
createNode polyMapSewMove -n "polyMapSewMove22";
	rename -uid "F4CFF023-4DF4-0A0D-7B53-81967C549BCA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[306]";
createNode polyTweakUV -n "polyTweakUV31";
	rename -uid "6EB8A6D5-4978-56CC-ED0B-DBACF8D6257A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk[217:220]" -type "float2" 0.32345474 0.19609804 0.2907986
		 0.25313807 0.28760293 0.24757542 0.31707188 0.19610281;
createNode polyMapSewMove -n "polyMapSewMove23";
	rename -uid "45FAC046-4855-7DCA-824C-3DBB3D9C0AEB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[298]";
createNode polyTweakUV -n "polyTweakUV32";
	rename -uid "48F5F9F0-4E64-DDBF-7B27-F5A223A46B62";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk[213:216]" -type "float2" 0.26962605 -0.011035036 0.3025617
		 0.045858394 0.296177 0.045884196 0.26645634 -0.0054561757;
createNode polyMapSewMove -n "polyMapSewMove24";
	rename -uid "69EBD083-4EF9-AF09-8EC9-2182A7127905";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[290]";
createNode polyTweakUV -n "polyTweakUV33";
	rename -uid "2DEFED99-4B40-D66E-DA38-EEA008F0D2BC";
	setAttr ".uopa" yes;
	setAttr -s 373 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" -2.001304e-006 4.3610478e-007 -2.0013738e-006
		 -4.7192771e-008 -2.0478433e-006 -4.7192771e-008 -2.0111677e-006 4.3610478e-007 -2.011564e-006
		 -4.7192771e-008 -2.0218743e-006 4.3610478e-007 -2.0219848e-006 -6.017649e-008 -2.0280715e-006
		 4.3610478e-007 -2.0277396e-006 -6.017649e-008 -2.0362609e-006 4.3610478e-007 -2.0372197e-006
		 -4.7192771e-008 0.0057665338 -3.4905713e-007 0.0057665352 -5.563324e-008 0.0057665338
		 -2.9248295e-007 0.0057665189 -2.377092e-007 0.0057665287 -1.6598611e-007 0.0057665426
		 -1.1027024e-007 0.021706283 0.49556947 0.11843467 0.0650189 0.11065938 0.062464193
		 0.0082033724 0.49169093 0.092857003 0.1430988 0.085050121 0.14078633 0.072056606
		 0.22124678 0.064172193 0.21953139 0.058649465 0.30067348 0.050501153 0.29945385 0.048022553
		 0.38455564 0.038396537 0.38256904 0.0033010335 0.0037470146 0.0033067914 0.0037482232
		 0.0033046883 0.0037494188 0.0033010289 0.0037494262 0.0033086252 0.0037538209 0.0033065067
		 0.0037526118 0.0033046845 0.0037582568 0.0033046978 0.0037557983 0.0032989259 0.0037570402
		 0.0033010319 0.0037558028 0.0032970856 0.0037514125 0.0032991942 0.0037526214 -0.081643239
		 0.58327562 0.0033095765 0.003743324 0.0033002656 0.0037374422 0.055556715 0.11349104
		 0.070778951 0.067777708 0.0033142653 0.0037513645 0.0033147624 0.0037425924 0.0033174884
		 0.0037472674 0.033683956 0.19359186 0.046107948 0.14750458 0.0033096299 0.0037619083
		 0.003317524 0.0037579192 0.0033148325 0.0037625991 0.018454775 0.27622372 0.026622176
		 0.22944897 0.0032982128 0.0037631434 0.0033056659 0.0037679605 0.0033002354 0.0037679547
		 0.0055730343 0.35970834 0.012362555 0.3115325 0.0032914525 0.0037514088 0.0032910677
		 0.0037626433 0.0032883575 0.0037579793 -0.032484531 0.45630324 -0.014623523 0.39291471
		 0.003288381 0.0037472849 0.0032911205 0.0037427556 -0.16909392 0.54437345 0.0033009518
		 0.0037304373 0.0047320873 0.088045321 0.015633866 0.057151183 0.0033212791 0.0037398224
		 0.0033231229 0.0037429575 -0.01815027 0.1715309 -0.0092326403 0.14013185 0.0033231974
		 0.0037621786 0.0033213589 0.0037653642 -0.034801081 0.2589421 -0.028852701 0.22729897
		 0.0033047723 0.0037749605 0.0033011364 0.0037749638 -0.048600778 0.34417233 -0.044262663
		 0.3120403 0.0032845316 0.0037654133 0.00328271 0.0037622862 -0.10289055 0.42477232
		 -0.090324923 0.38232821 0.003283215 0.0037428103 0.0032851009 0.0037400904 -0.27697
		 0.51186848 0.0033008528 0.0037222281 -0.058584914 0.063989095 -0.047680274 0.034865998
		 0.0033284798 0.0037357628 0.0033301972 0.0037386992 -0.082810462 0.1516951 -0.073978677
		 0.12191235 0.0033302756 0.0037663821 0.0033285641 0.0037693337 -0.099700272 0.24518009
		 -0.093559027 0.215489 0.0033046815 0.0037832148 0.0033012724 0.0037832349 -0.1138168
		 0.33439299 -0.10987173 0.30440146 0.0032773407 0.0037694641 0.0032756238 0.0037664969
		 -0.18936278 0.39699516 -0.17706501 0.35735554 0.0032774273 0.0037381798 0.0032793342
		 0.0037359039 -0.42056081 0.46625853 0.0033008335 0.0037113128 -0.14304405 0.028827928
		 -0.13373864 0.004900299 0.0033381372 0.0037304799 0.003339563 0.0037328948 -0.16931795
		 0.12295473 -0.16200721 0.098445497 0.0033396455 0.0037721533 0.0033382641 0.0037745682
		 -0.18590178 0.22389458 -0.18065673 0.19950444 0.0033043893 0.0037942445 0.0033015916
		 0.0037942566 -0.20093817 0.32031757 -0.19780557 0.29571393 0.00326765 0.0037747384
		 0.0032662426 0.0037723153 -0.3039594 0.35696816 -0.29363784 0.32442287 0.0032705872
		 0.0037314643 0.0032722696 0.0037297332 -0.47906327 0.47826594 -0.45242515 0.38452202
		 0.0032989455 0.0037075232 0.0033052922 0.0037073516 -0.17916608 0.032410532 -0.15732755
		 -0.022426464 0.003340543 0.0037269541 0.0033438101 0.0037325048 -0.20463698 0.12969926
		 -0.18790899 0.073466666 0.0033439451 0.0037725314 0.0033407388 0.0037780728 -0.21983296
		 0.23315074 -0.2077549 0.17703614 0.0033062398 0.0037981393 0.0032998011 0.0037981537
		 -0.23360281 0.33244723 -0.22660856 0.27595073 0.0032651958 0.0037782667 0.0032619627
		 0.0037726969 -0.35145739 0.36510679 -0.32754543 0.29023698 0.0032670686 0.0037300701
		 0.0032711457 0.0037262109 0.005766497 -3.4945759e-007 0.0057664886 -2.9125073e-007
		 0.0057664923 -2.2059159e-007 0.0057664891 -1.7121612e-007 0.0057665193 -1.2009531e-007
		 -2.04611e-006 4.3610478e-007 -2.0600901e-006 4.3610478e-007 -2.0659911e-006 -4.7192771e-008
		 -0.016967908 0.63110638 0.0033046808 0.0037470299 -0.030474856 0.62725079 -0.059149012
		 0.50437433 0.0033074913 0.0037421363 0.0032961366 0.0037433521 0.0033056489 0.0037373651
		 -0.15400159 0.49134985 0.0033045893 0.0037303497 -0.26285562 0.46221954 0.00330422
		 0.0037221333 -0.40897316 0.42549968 0.0033036068 0.0037112369 0.0057665226 7.1565776e-010
		 0.0057664933 -5.5090727e-008 0.005766545 8.4746343e-010 0.0033142723 0.0037538577
		 0.0033075332 0.0037631448 0.0032961154 0.0037619411 0.0032914411 0.0037538593 0.00031824177
		 0.0018532078 0.00031823604 0.001853215 0.00031817099 0.0018532122 0.00031818968 0.0018531918
		 0.00031820571 0.001853192 0.00031821732 0.0018531607 0.00031825827 0.0018532015 0.00031818141
		 0.001853193 0.00031824908 0.0018532081 0.00031821799 0.0018532109 0.00031823388 0.0018532035
		 0.00031822801 0.0018532319 0.00035560076 0.0018537112 0.00035561793 0.0018537422
		 0.00035554159 0.0018537617 0.00035558015 0.0018536942 0.00035558568 0.0018537537
		 0.00035557762 0.0018537807 0.000355608 0.0018537348 0.00035564066 0.001853757 0.00035560044
		 0.0018536989 0.00035568245 0.0018536954 0.00035563545 0.0018536957 0.0003555913 0.0018536611
		 0.0033028633 0.0037526092 0.003298582 0.0037069325 0.0033056389 0.0037067425 0.0033408897
		 0.0037263392 0.0033445212 0.0037324948 0.0033446278 0.0037725316 0.0033410559 0.0037786958
		 0.0033065884 0.0037987288 0.0032994449 0.0037987498 0.0032648609 0.0037788735 0.0032612644
		 0.0037726818 0.0032664903 0.0037298996 0.003271013 0.0037256184 -0.0036181035 0.0018533431
		 -0.0036180825 0.0018533431 -0.0036181773 0.0018534567 -0.0036181838 0.0018534511
		 0.0056910794 0.0037064713 0.0056909965 0.0037064713 0.0056908107 0.0037062194 0.0056908373
		 0.0037062073 0.0019523675 0.0018533586 0.0019523718 0.0018533586 0.0019523449 0.0018533893
		 0.0019523583 0.0018533894 0.0075558145 0.0018533223 0.0075558256 0.0018533223 0.0075558554
		 0.001853363 0.0075558573 0.0018533653 -0.0036183379 0.0018536065 -0.0036183505 0.0018536029
		 0.0056904689 0.0037058829 0.0056904899 0.0037058743 0.0019523244 0.0018534244 0.0019523175
		 0.0018534235 0.0075559011 0.0018534209 0.0075559099 0.0018534227 -0.0036183053 0.0018538627
		 -0.0036183274 0.0018538622 0.0056905411 0.0037052887;
	setAttr ".uvtk[250:372]" 0.0056905663 0.0037052869 0.0019523174 0.001853484
		 0.0019523224 0.0018534848 0.0075559178 0.001853514 0.0075559099 0.0018535189 -0.003618212
		 0.0018541977 -0.0036182299 0.0018541913 0.0056907553 0.0037045281 0.0056907716 0.00370454
		 0.0019523533 0.0018535706 0.0019523328 0.0018535674 0.0075558736 0.001853638 0.0075558643
		 0.0018536415 -0.0036182052 0.0018543176 -0.0036182266 0.0018543266 0.0056907218 0.0037042475
		 0.0056907781 0.0037042531 0.0019523471 0.001853595 0.0019523418 0.0018536039 0.0075558736
		 0.0018536928 0.0075558741 0.0018536841 0.0021762825 3.5160905e-009 0.002176346 3.5160905e-009
		 0.0021762315 8.1778531e-008 0.0021762373 7.9822108e-008 0.00034001441 4.5712849e-008
		 0.00034000789 4.5712849e-008 0.00034017445 2.601067e-007 0.000340103 2.6687087e-007
		 -0.0014959855 -0.0018532961 -0.0014959602 -0.0018532961 -0.0014960244 -0.0018531173
		 -0.0014961154 -0.0018531249 -0.003331885 0.0018532572 -0.0033319837 0.0018532572
		 -0.0033322053 0.0018529163 -0.0033321783 0.0018529014 0.0021761544 2.051201e-007
		 0.002176133 2.0391387e-007 0.00034042413 5.4231487e-007 0.00034033839 5.4861147e-007
		 -0.0014963102 -0.001852878 -0.0014963471 -0.0018528815 -0.0033326915 0.0018524111
		 -0.0033326375 0.0018524036 0.0021761972 3.8309565e-007 0.002176197 3.8269414e-007
		 0.00034043848 1.0036418e-006 0.00034035451 1.0012693e-006 -0.0014962922 -0.0018524594
		 -0.001496323 -0.0018524627 -0.0033325911 0.0018515565 -0.0033325334 0.0018515588
		 0.0021762128 6.2392905e-007 0.0021762066 6.1951494e-007 0.00034018382 1.6150399e-006
		 0.00034023129 1.6128149e-006 -0.0014961371 -0.0018519353 -0.0014961404 -0.0018519314
		 -0.0033323038 0.0018504722 -0.0033322705 0.0018504777 0.0021762208 7.1244767e-007
		 0.0021762191 7.2348297e-007 0.00034022293 1.8576324e-006 0.00034026307 1.8571748e-006
		 -0.0014961659 -0.001851739 -0.0014961655 -0.0018517369 -0.0033323083 0.0018500605
		 -0.0033322843 0.0018500688 0.0075742532 1.4505047e-006 0.0075742155 1.4505047e-006
		 0.0075743562 1.1545555e-006 0.0075743869 1.1551729e-006 0.0075900005 -0.0018522192
		 0.0075900005 -0.001852315 0.0075903018 -0.0018524779 0.0075903079 -0.0018524363 4.640597e-005
		 -0.00370639 4.640597e-005 -0.0037064359 4.6521087e-005 -0.0037062645 4.6501209e-005
		 -0.0037062576 0.00029463391 0.0018534284 0.00029460661 0.0018534284 0.00029461252
		 0.0018534292 0.00029457951 0.0018534234 0.007574542 8.2356735e-007 0.0075745606 8.248723e-007
		 0.007590469 -0.0018526065 0.0075904881 -0.0018526008 4.6427926e-005 -0.0037060394
		 4.6412479e-005 -0.0037060368 0.00029456188 0.0018534447 0.00029457081 0.0018534365
		 0.0075744959 4.9966167e-007 0.0075745252 5.0128619e-007 0.0075904247 -0.0018526777
		 0.0075904285 -0.001852684 4.6449008e-005 -0.0037060704 4.6445257e-005 -0.0037060655
		 0.00029458196 0.0018534679 0.00029458475 0.0018534716 0.0075744349 1.3274735e-007
		 0.0075744432 1.3406378e-007 0.0075902147 -0.0018528135 0.0075902045 -0.0018528199
		 4.6455258e-005 -0.0037062054 4.6458466e-005 -0.0037062129 0.00029456805 0.0018535039
		 0.00029456784 0.0018535039 0.0075744768 -6.4658368e-010 0.007574487 2.412881e-010
		 0.0075900406 -0.0018528254 0.0075900392 -0.0018528461 4.6472742e-005 -0.0037062259
		 4.6480571e-005 -0.0037062152 0.00029458984 0.0018535126 0.000294586 0.0018535057
		 0.0032970852 0.0037538693 0.0033068017 0.0037570035 0.0033086357 0.0037514074 0.0032989092
		 0.0037482409 0.0033010398 0.0037582498 0.0032982111 0.0037421391;
createNode polyMapSewMove -n "polyMapSewMove25";
	rename -uid "7C4A2FAA-40B3-DF6F-55A2-EBBFF30ED941";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[294]";
createNode polyMapCut -n "polyMapCut6";
	rename -uid "2921298A-40F3-5E3E-A1FB-9DB1D36346BA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[32]" "e[34]" "e[36]" "e[38]" "e[40:41]";
createNode polyTweakUV -n "polyTweakUV34";
	rename -uid "079C85C1-438B-CEEC-FE5A-5499640733B7";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk";
	setAttr ".uvtk[24]" -type "float2" -0.28569841 0.16691609 ;
	setAttr ".uvtk[54]" -type "float2" -0.29080623 0.1230479 ;
	setAttr ".uvtk[55]" -type "float2" -0.2740365 0.14645447 ;
	setAttr ".uvtk[78]" -type "float2" -0.26070237 0.10734595 ;
	setAttr ".uvtk[79]" -type "float2" -0.24953738 0.12336363 ;
	setAttr ".uvtk[100]" -type "float2" -0.2271747 0.08511053 ;
	setAttr ".uvtk[101]" -type "float2" -0.21693638 0.10038234 ;
	setAttr ".uvtk[122]" -type "float2" -0.18134123 0.056853965 ;
	setAttr ".uvtk[123]" -type "float2" -0.17301586 0.069485798 ;
	setAttr ".uvtk[146]" -type "float2" -0.17093062 0.038239196 ;
	setAttr ".uvtk[147]" -type "float2" -0.1517809 0.067305967 ;
	setAttr ".uvtk[376]" -type "float2" -0.31447309 0.12704505 ;
createNode polyMapSewMove -n "polyMapSewMove26";
	rename -uid "0028E959-426A-87E4-CDA9-009D3C298AAF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[302]";
createNode polyTweakUV -n "polyTweakUV35";
	rename -uid "094E4CFA-4068-A7E0-54A6-7488A89B5F64";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk";
	setAttr ".uvtk[26]" -type "float2" -0.60730439 -0.038733259 ;
	setAttr ".uvtk[59]" -type "float2" -0.57523483 -0.10960539 ;
	setAttr ".uvtk[60]" -type "float2" -0.57142353 -0.059395835 ;
	setAttr ".uvtk[82]" -type "float2" -0.51702338 -0.10590331 ;
	setAttr ".uvtk[83]" -type "float2" -0.51428878 -0.07245712 ;
	setAttr ".uvtk[104]" -type "float2" -0.44892395 -0.1104681 ;
	setAttr ".uvtk[105]" -type "float2" -0.44626638 -0.079272941 ;
	setAttr ".uvtk[126]" -type "float2" -0.35772657 -0.11554374 ;
	setAttr ".uvtk[127]" -type "float2" -0.35544154 -0.08997564 ;
	setAttr ".uvtk[150]" -type "float2" -0.32739109 -0.13504486 ;
	setAttr ".uvtk[151]" -type "float2" -0.32194269 -0.076378003 ;
	setAttr ".uvtk[370]" -type "float2" -0.6134848 -0.12544353 ;
createNode polyMapSewMove -n "polyMapSewMove27";
	rename -uid "6D39DC4C-4147-AD18-E548-E0A848D378CA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[310]";
createNode polyTweakUV -n "polyTweakUV36";
	rename -uid "CCE43518-4B47-0A65-E02D-F8A7FFC3E89B";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk";
	setAttr ".uvtk[28]" -type "float2" -0.54684699 -0.49056879 ;
	setAttr ".uvtk[64]" -type "float2" -0.4626233 -0.5328722 ;
	setAttr ".uvtk[65]" -type "float2" -0.49760339 -0.48327982 ;
	setAttr ".uvtk[86]" -type "float2" -0.41113061 -0.48386028 ;
	setAttr ".uvtk[87]" -type "float2" -0.43507934 -0.45084009 ;
	setAttr ".uvtk[108]" -type "float2" -0.34452599 -0.43317741 ;
	setAttr ".uvtk[109]" -type "float2" -0.36738101 -0.40251157 ;
	setAttr ".uvtk[130]" -type "float2" -0.25724596 -0.36321637 ;
	setAttr ".uvtk[131]" -type "float2" -0.27620584 -0.33810782 ;
	setAttr ".uvtk[154]" -type "float2" -0.21347791 -0.35570508 ;
	setAttr ".uvtk[155]" -type "float2" -0.25724047 -0.29799473 ;
	setAttr ".uvtk[370]" -type "float2" -0.48711103 -0.5761112 ;
createNode polyMapSewMove -n "polyMapSewMove28";
	rename -uid "8FD7D16C-48CD-1CE2-DD0F-1ABF6B35DAFC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[318]";
createNode polyTweakUV -n "polyTweakUV37";
	rename -uid "B84BB3E3-4343-3B9E-E877-56AFD12A4AE1";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk";
	setAttr ".uvtk[20]" -type "float2" -0.27814403 -0.71656203 ;
	setAttr ".uvtk[41]" -type "float2" -0.17869972 -0.73546404 ;
	setAttr ".uvtk[68]" -type "float2" -0.13954456 -0.67089152 ;
	setAttr ".uvtk[90]" -type "float2" -0.084274545 -0.60127008 ;
	setAttr ".uvtk[112]" -type "float2" -0.011761144 -0.50708038 ;
	setAttr ".uvtk[134]" -type "float2" 0.03155224 -0.48852697 ;
	setAttr ".uvtk[135]" -type "float2" -0.027906135 -0.43977347 ;
	setAttr ".uvtk[169]" -type "float2" -0.22879173 -0.6944629 ;
	setAttr ".uvtk[173]" -type "float2" -0.17319192 -0.64332682 ;
	setAttr ".uvtk[175]" -type "float2" -0.11576916 -0.57545191 ;
	setAttr ".uvtk[177]" -type "float2" -0.037616715 -0.48588544 ;
	setAttr ".uvtk[376]" -type "float2" -0.19206099 -0.78699178 ;
createNode polyMapSewMove -n "polyMapSewMove29";
	rename -uid "113BF6AE-40C8-F0E8-AA4F-D1ADBA9903A1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[278]";
createNode polyTweakUV -n "polyTweakUV38";
	rename -uid "5B2C4BDA-4C18-ECE9-D4C9-18BA2FAE5FB7";
	setAttr ".uopa" yes;
	setAttr -s 12 ".uvtk";
	setAttr ".uvtk[19]" -type "float2" -0.052389458 -0.3356418 ;
	setAttr ".uvtk[44]" -type "float2" -0.012057379 -0.31454119 ;
	setAttr ".uvtk[45]" -type "float2" -0.041253999 -0.31394762 ;
	setAttr ".uvtk[70]" -type "float2" -0.016263619 -0.28034899 ;
	setAttr ".uvtk[71]" -type "float2" -0.036120191 -0.28029859 ;
	setAttr ".uvtk[92]" -type "float2" -0.017138675 -0.23930539 ;
	setAttr ".uvtk[93]" -type "float2" -0.035984531 -0.23961644 ;
	setAttr ".uvtk[114]" -type "float2" -0.020060435 -0.18393281 ;
	setAttr ".uvtk[115]" -type "float2" -0.035614505 -0.18438604 ;
	setAttr ".uvtk[138]" -type "float2" -0.010676816 -0.16403332 ;
	setAttr ".uvtk[139]" -type "float2" -0.046427444 -0.16536459 ;
	setAttr ".uvtk[372]" -type "float2" -0.0024618059 -0.33692798 ;
createNode polyMapSewMove -n "polyMapSewMove30";
	rename -uid "B8580492-41E5-F617-21DA-228DE32C925B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[286]";
createNode polyTweakUV -n "polyTweakUV39";
	rename -uid "2D1B3BE8-405B-9C89-FCB3-CE8E34E4A242";
	setAttr ".uopa" yes;
	setAttr -s 373 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" -4.3876695e-005 -0.00034498138 -4.3876491e-005
		 -7.5142225e-008 -1.1265245e-005 -7.5142225e-008 -3.735525e-005 -0.00034498138 -3.7355556e-005
		 -7.5142225e-008 -3.0833518e-005 -0.00034498138 -3.0833573e-005 -8.8254637e-008 -2.4307463e-005
		 -0.00034498138 -2.4307092e-005 -8.8254637e-008 -1.7785507e-005 -0.00034498138 -1.7786402e-005
		 -7.5142225e-008 0.29111099 0.001583279 0.29111096 0.0014909569 0.29111099 0.0015638403
		 0.29111078 0.0015452179 0.29111072 0.0015269703 0.29111084 0.0015089328 -0.036316127
		 -0.29701683 -0.054583468 -0.25958571 0.1496796 0.15767805 0.14715785 0.15648915 -0.050532695
		 -0.26619866 0.14989011 0.16048031 -0.046919189 -0.27292195 0.14759451 0.16202872
		 -0.043956749 -0.27992386 0.14505906 0.16080642 -0.041152533 -0.28739551 0.14538041
		 0.15776001 0.14733174 0.15905784 0.14753081 0.1590997 0.14745809 0.15914167 0.14733168
		 0.15914188 0.14759454 0.15929356 0.14752145 0.15925176 0.14745852 0.15944639 0.14745845
		 0.15936211 0.14725909 0.15940438 0.14733207 0.15936229 0.14719556 0.15921034 0.14726868
		 0.1592522 0.1474442 0.15663533 0.14762731 0.15893008 0.1473057 0.15872654 0.14970593
		 0.15799086 0.1495945 0.15782312 0.14778976 0.15920813 0.14780702 0.15890475 0.14790155
		 0.15906659 0.1496288 0.16064639 0.14972432 0.16046999 0.14762898 0.15957293 0.14790265
		 0.15943466 0.14780898 0.15959719 0.14731432 0.1618886 0.14751838 0.16188008 0.14723401
		 0.15961605 0.1474922 0.15978198 0.14730452 0.15978201 0.14505634 0.16047992 0.14515935
		 0.16066453 0.14700048 0.15920961 0.14698714 0.15959901 0.14689298 0.15943661 0.14564796
		 0.15767683 0.1455213 0.15780531 0.14689401 0.15906706 0.14698878 0.15891053 0.14741671
		 0.15687361 0.1473289 0.15848437 0.14949134 0.15809283 0.14941762 0.15797742 0.14803234
		 0.15880869 0.14809589 0.15891744 0.1494382 0.16050585 0.14950514 0.16038612 0.14809792
		 0.15958275 0.14803503 0.1596919 0.14733814 0.16164891 0.14747658 0.16164495 0.14746159
		 0.16002537 0.14733547 0.16002546 0.14528461 0.16039146 0.14535397 0.16051407 0.14676109
		 0.15969455 0.14669776 0.15958534 0.1457752 0.15784557 0.14568894 0.15793082 0.14671506
		 0.15891215 0.14678076 0.15881798 0.14741985 0.15715593 0.14732534 0.1582002 0.14924923
		 0.1582395 0.14918132 0.15812866 0.14828053 0.15866837 0.14834 0.15877016 0.14919299
		 0.16036202 0.14925803 0.16024873 0.14834295 0.15972856 0.14828412 0.15983072 0.14733647
		 0.1613636 0.1474669 0.16136214 0.14745802 0.1603108 0.14734 0.16031103 0.14553599
		 0.16025406 0.14560108 0.1603682 0.14651187 0.15983462 0.14645252 0.15973234 0.14594595
		 0.15802658 0.14586402 0.15810542 0.14651485 0.15875216 0.14658146 0.15867311 0.14741847
		 0.15753347 0.14732517 0.1578225 0.14891611 0.15842727 0.14886121 0.15833506 0.14861457
		 0.15848547 0.14866342 0.15856904 0.1488671 0.16016047 0.1489208 0.16006732 0.14866753
		 0.15992761 0.14861926 0.16001144 0.14734688 0.1609818 0.1474542 0.16098148 0.14744839
		 0.16069195 0.14735144 0.16069224 0.14587446 0.16007403 0.1459282 0.16016741 0.14617637
		 0.16001707 0.14612764 0.15993318 0.14616798 0.15827414 0.14610018 0.15833856 0.14627805
		 0.15851957 0.14633651 0.15845969 0.14749098 0.15766436 0.14724685 0.15767094 0.14725934
		 0.15769139 0.14747965 0.15768546 0.14883497 0.15855549 0.14871047 0.15834245 0.14869869
		 0.15836345 0.14881098 0.15855567 0.14871606 0.16015375 0.14883909 0.15994002 0.14881516
		 0.15993999 0.14870413 0.16013284 0.14727691 0.16084777 0.14752379 0.16084704 0.14751166
		 0.16082601 0.14728889 0.16082664 0.14595564 0.15994628 0.14607976 0.16016014 0.14609167
		 0.16013919 0.14597966 0.15994614 0.1462927 0.15831742 0.14613633 0.15846545 0.14615636
		 0.15847147 0.14629747 0.15833788 0.29112166 0.0015831452 0.29112145 0.0015637465
		 0.29112092 0.0015450774 0.29112074 0.001526931 0.29112068 0.0015089629 -1.1269343e-005
		 -0.00034498138 -4.7515214e-006 -0.00034498138 -4.7457847e-006 -7.5142225e-008 -0.029810313
		 -0.30862084 0.14745823 0.15905808 -0.028653936 -0.3079724 0.14723866 0.15664069 0.14755571
		 0.15888889 0.14716239 0.15893103 0.1474914 0.15872386 0.14727861 0.15687728 0.14745468
		 0.15848109 0.14729056 0.15715939 0.1474425 0.1581969 0.14731228 0.15753631 0.147421
		 0.15781984 0.2911208 0.0014731078 0.29112083 0.0014910087 0.29111102 0.0014730585
		 0.14778991 0.15929373 0.14755656 0.15961601 0.14716186 0.15957391 0.14700007 0.15929486
		 -0.49852082 -0.029321898 -0.49852094 -0.029327758 -0.49850878 -0.029335877 -0.49850652
		 -0.029314753 -0.49852562 -0.029318476 -0.49852479 -0.029303854 -0.49853066 -0.029321097
		 -0.49854252 -0.029313451 -0.49853092 -0.029326899 -0.49854347 -0.029333198 -0.49852607
		 -0.029330188 -0.49852705 -0.029344307 -0.23711233 -0.085595712 -0.2371121 -0.085589208
		 -0.23712079 -0.085583746 -0.23712152 -0.085600294 -0.2371064 -0.085586153 -0.23710607
		 -0.085575871 -0.23710094 -0.085589573 -0.23709188 -0.085584715 -0.23710121 -0.08559604
		 -0.23709244 -0.085601524 -0.23710693 -0.085599102 -0.23710732 -0.085609421 0.14739507
		 0.15925197 -0.096988343 0.37916708 -0.096986845 0.37916708 -0.096992254 0.37917325
		 -0.096993163 0.37917292 0.55259502 0.27748439 0.55259651 0.27748439 0.55260122 0.27749041
		 0.55260038 0.27749071 0.60510081 0.25024661 0.6051023 0.25024661 0.60509789 0.25025314
		 0.60509706 0.25025293 0.44123089 0.32287645 0.44123244 0.32287645 0.44123697 0.32288232
		 0.44123608 0.32288262 -0.097000889 0.37918127 -0.09700153 0.37918115 0.55260926 0.27749836
		 0.55260861 0.27749848 0.6050902 0.25026128 0.60508955 0.25026113 0.44124487 0.32289043
		 0.44124424 0.32289052 -0.096999317 0.37919539 -0.096999928 0.37919536 0.55260772
		 0.27751249 0.55260706 0.27751249 0.6050896 0.25027436 0.60508907 0.25027433 0.44124323
		 0.32290453 0.44124272 0.3229045 -0.096994296 0.37921333 -0.096994795 0.37921339 0.55260259
		 0.27753037 0.55260205 0.27753031 0.60509557 0.25029191 0.60509497 0.25029194 0.44123816
		 0.32292238;
	setAttr ".uvtk[250:372]" 0.44123766 0.32292235 -0.096994266 0.37922007 -0.096995331
		 0.37922022 0.5526033 0.27753714 0.55260223 0.27753699 0.60509491 0.25029886 0.60509384
		 0.25029898 0.44123894 0.32292917 0.44123778 0.32292899 0.056850512 0.0014730716 0.056851938
		 0.0014730716 0.056847293 0.001478954 0.056846417 0.0014787643 0.057829499 -0.00034208826
		 0.057830915 -0.00034208826 0.057834528 -0.00033579362 0.057833645 -0.00033559301
		 -0.011985275 0.12858148 -0.011983922 0.12858148 -0.011988501 0.12858717 -0.011989339
		 0.12858692 -0.021939043 0.14674063 -0.021937573 0.14674063 -0.021933112 0.14674646
		 -0.021933958 0.14674671 0.056840442 0.0014879016 0.056839857 0.0014877863 0.057841733
		 -0.00032743128 0.057841092 -0.0003272951 -0.011995967 0.12859482 -0.011996583 0.12859471
		 -0.02192522 0.14675513 -0.021925868 0.14675525 0.056840204 0.0015012148 0.056839589
		 0.0015012065 0.057841972 -0.00031390972 0.057841297 -0.0003139044 -0.011994519 0.12860808
		 -0.011995087 0.12860809 -0.0219268 0.14676972 -0.021927429 0.1467697 0.056845725
		 0.0015191495 0.056845207 0.0015191839 0.057836249 -0.00029590016 0.057835724 -0.00029594119
		 -0.011990127 0.12862501 -0.01199063 0.12862505 -0.021931708 0.14678825 -0.021932231
		 0.14678822 0.056845106 0.0015261295 0.056843966 0.0015262377 0.057837557 -0.00028881227
		 0.057836484 -0.0002889061 -0.011990249 0.12863129 -0.011991231 0.12863146 -0.021931052
		 0.1467953 -0.021932157 0.14679509 -0.28418612 0.26844937 -0.28418848 0.26844937 -0.28418273
		 0.26843956 -0.28418159 0.26843956 -0.32125518 0.15767615 -0.32125518 0.15767269 -0.32124314
		 0.15766664 -0.32124299 0.15766768 0.037542049 0.23573701 0.037542049 0.23573339 0.037550639
		 0.23574653 0.037549362 0.23574713 -0.36120486 0.095937185 -0.36120772 0.095937185
		 -0.36121309 0.09593159 -0.36121303 0.095930837 -0.28417751 0.2684283 -0.28417692
		 0.2684283 -0.32123676 0.15766169 -0.32123664 0.15766183 0.037543356 0.23576392 0.037542928
		 0.23576398 -0.36122119 0.095941514 -0.36122179 0.095941022 -0.28417832 0.26841724
		 -0.28417787 0.26841724 -0.3212387 0.15765877 -0.32123861 0.15765855 0.037545182 0.23576152
		 0.037545435 0.23576163 -0.36122999 0.095957384 -0.36123058 0.095957443 -0.28418058
		 0.26840481 -0.28418025 0.26840484 -0.32124779 0.15765363 -0.3212477 0.15765333 0.037546221
		 0.2357509 0.03754646 0.23575099 -0.36122343 0.095972173 -0.36122376 0.095972508 -0.28417957
		 0.26840028 -0.28417879 0.26840031 -0.32125324 0.15765318 -0.32125294 0.15765236 0.037547462
		 0.23574995 0.037547652 0.23575011 -0.36121646 0.095975883 -0.36121729 0.095976762
		 0.14719561 0.1592942 0.14753145 0.15940376 0.14759456 0.15920946 0.14725854 0.15910009
		 0.14733201 0.15944637 0.14723468 0.158889 -0.040311828 -0.28700325 0.14488272 0.16048703
		 -0.035160638 -0.29636633 0.14559716 0.15753812 -0.043235019 -0.27963427 0.14724609
		 0.16204454 -0.046232525 -0.2725926 0.14972827 0.1607821 -0.049866147 -0.26581612
		 0.14987172 0.15796392 -0.053925097 -0.25918165 0.14751096 0.15648001;
createNode polyMapCut -n "polyMapCut7";
	rename -uid "C0992DF1-41D4-1DB5-1DE3-B499DA287126";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[30]" "e[33]" "e[35]" "e[37]" "e[39]";
createNode polyTweakUV -n "polyTweakUV40";
	rename -uid "D8A2C9C6-4455-70DA-BA39-9F8CCCC6C074";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[18]" -type "float2" 0.0062013566 0.12999812 ;
	setAttr ".uvtk[371]" -type "float2" 0.0072171986 0.13620578 ;
	setAttr ".uvtk[379]" -type "float2" 0.068511695 0.11974317 ;
	setAttr ".uvtk[380]" -type "float2" 0.069704086 0.12588648 ;
createNode polyMapSewMove -n "polyMapSewMove31";
	rename -uid "E2348C05-4318-F5E7-DD09-68B5128023B3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[22]";
createNode polyTweakUV -n "polyTweakUV41";
	rename -uid "42908D50-465C-FE4E-4D1D-069CE9CC0962";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[17]" -type "float2" 0.094528735 -0.10340397 ;
	setAttr ".uvtk[166]" -type "float2" 0.12915105 -0.11336455 ;
	setAttr ".uvtk[168]" -type "float2" 0.13014334 -0.10991403 ;
	setAttr ".uvtk[362]" -type "float2" 0.09551549 -0.099951208 ;
createNode polyMapSewMove -n "polyMapSewMove32";
	rename -uid "80C53388-4438-1BA9-B381-EF8D8069C0C7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[20]";
createNode polyTweakUV -n "polyTweakUV42";
	rename -uid "97CA48E5-49B4-8D79-1E0D-4FBF3D2F8FBC";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[27]" -type "float2" 0.11140695 -0.081905797 ;
	setAttr ".uvtk[358]" -type "float2" 0.11321577 -0.083413333 ;
	setAttr ".uvtk[377]" -type "float2" 0.094528764 -0.10340397 ;
	setAttr ".uvtk[378]" -type "float2" 0.097270697 -0.10535581 ;
createNode polyMapSewMove -n "polyMapSewMove33";
	rename -uid "FBC767F1-4380-E113-144D-BD82E07FB898";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[29]";
createNode polyTweakUV -n "polyTweakUV43";
	rename -uid "D71186FA-42DB-D882-7550-0FBAB5170D1B";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[21]" -type "float2" 0.068511665 0.11974303 ;
	setAttr ".uvtk[365]" -type "float2" 0.074264407 0.12582558 ;
	setAttr ".uvtk[373]" -type "float2" 0.12730992 0.060946457 ;
	setAttr ".uvtk[374]" -type "float2" 0.13343954 0.066537805 ;
createNode polyMapSewMove -n "polyMapSewMove34";
	rename -uid "964A1A97-4A71-7A70-83B0-DBB8141016BB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[24]";
createNode polyTweakUV -n "polyTweakUV44";
	rename -uid "D5480393-4C49-5B68-7874-AAB183DBAA79";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[23]" -type "float2" 0.12731007 0.060946427 ;
	setAttr ".uvtk[362]" -type "float2" 0.13555792 0.062635504 ;
	setAttr ".uvtk[370]" -type "float2" 0.14028373 -0.022092573 ;
	setAttr ".uvtk[371]" -type "float2" 0.14880064 -0.020924114 ;
createNode polyMapSewMove -n "polyMapSewMove35";
	rename -uid "CCD5E50E-4A1C-1EC2-4126-119BFA61920C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[26]";
createNode polyTweakUV -n "polyTweakUV45";
	rename -uid "F2845E49-43EA-C425-E7E9-9482B01EDB0D";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[25]" -type "float2" 0.14028367 -0.022092625 ;
	setAttr ".uvtk[359]" -type "float2" 0.1461741 -0.024774268 ;
	setAttr ".uvtk[367]" -type "float2" 0.11140689 -0.081905916 ;
	setAttr ".uvtk[368]" -type "float2" 0.11859891 -0.084714323 ;
createNode polyMapSewMove -n "polyMapSewMove36";
	rename -uid "DEF31339-4B40-8F7B-914D-AA9B5190A487";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[28]";
createNode polyTweakUV -n "polyTweakUV46";
	rename -uid "49C1F061-4735-9E96-3290-4CA484E3BE2A";
	setAttr ".uopa" yes;
	setAttr -s 371 ".uvtk";
	setAttr ".uvtk[0:249]" -type "float2" -1.0175628e-005 -0.00045833172 -1.0175453e-005
		 -9.6464603e-008 3.3143529e-005 -9.6464603e-008 -1.5126882e-006 -0.00045833172 -1.5128678e-006
		 -9.6464603e-008 7.1512604e-006 -0.00045833172 7.1512763e-006 -1.0974794e-007 1.5816871e-005
		 -0.00045833172 1.5817299e-005 -1.0974794e-007 2.4479161e-005 -0.00045833172 2.4478351e-005
		 -9.6464603e-008 -0.0025131125 -0.0021150352 -0.0025131851 -0.0022375598 -0.0025131125
		 -0.0021408338 -0.0025133421 -0.0021655571 -0.0025134545 -0.0021897601 -0.0025133938
		 -0.0022137018 -0.066154756 0.025495183 -0.06616883 0.025207352 -0.00087857118 0.0014079658
		 -0.0010354926 0.0013339823 -0.0664239 0.025070984 -0.00086549856 0.0015823296 -0.066670865
		 0.025224671 -0.0010082975 0.0016787176 -0.066660374 0.025516458 -0.0011660958 0.0016026509
		 -0.06640178 0.025653834 -0.0011460904 0.0014130669 -0.0010246467 0.0014938361 -0.0010122842
		 0.0014964427 -0.0010168094 0.001499041 -0.0010246404 0.0014990636 -0.0010082867 0.0015085046
		 -0.0010128567 0.0015058958 -0.0010167505 0.0015179911 -0.0010167779 0.0015127831
		 -0.0010291642 0.0015153787 -0.0010246621 0.0015127693 -0.001033135 0.0015033182 -0.0010285651
		 0.0015059112 -0.0010176632 0.001343079 -0.0010062795 0.0014858871 -0.0010262631 0.0014732217
		 -0.00087690382 0.0014274429 -0.00088385632 0.0014169957 -0.00099615951 0.0015031849
		 -0.00099510106 0.0014843097 -0.00098917552 0.0014943638 -0.00088170869 0.0015926646
		 -0.00087577628 0.0015817105 -0.0010061698 0.0015258635 -0.00098912115 0.0015172667
		 -0.00099498092 0.0015273697 -0.0010257305 0.0016699719 -0.001013058 0.0016694433
		 -0.0010307512 0.0015285473 -0.0010146503 0.0015388731 -0.0010263626 0.0015389071
		 -0.0011662642 0.0015823112 -0.0011598566 0.0015937944 -0.0010452736 0.0015032783
		 -0.0010461158 0.001527485 -0.0010519465 0.0015173795 -0.0011294286 0.0014078974 -0.0011373264
		 0.0014158911 -0.0010519023 0.0014944037 -0.0010460108 0.0014846607 -0.0010193496
		 0.0013579057 -0.0010248477 0.0014581332 -0.00089025777 0.0014337766 -0.0008948365
		 0.0014265889 -0.00098104274 0.0014783305 -0.0009771277 0.0014850902 -0.00089357968
		 0.0015839534 -0.00088941876 0.0015764821 -0.00097700011 0.0015265053 -0.00098087976
		 0.0015332676 -0.0010242502 0.0016550607 -0.0010156243 0.0016548398 -0.0010165692
		 0.0015540541 -0.001024413 0.0015540295 -0.001152061 0.0015768273 -0.0011477361 0.0015844236
		 -0.0010601612 0.0015334267 -0.0010641033 0.0015266539 -0.0011215084 0.0014183931
		 -0.0011268775 0.0014236849 -0.0010630337 0.0014847692 -0.0010589401 0.0014788921
		 -0.0010191771 0.0013754702 -0.0010250758 0.0014404564 -0.00090535573 0.0014428953
		 -0.00090954936 0.0014359971 -0.00096563651 0.0014696022 -0.0009619164 0.0014759188
		 -0.0009088323 0.0015749708 -0.00090478396 0.0015679271 -0.00096173008 0.0015355407
		 -0.00096538174 0.0015419417 -0.0010243724 0.001637321 -0.001016228 0.0016372221 -0.0010167871
		 0.0015718018 -0.0010241256 0.001571825 -0.0011363939 0.001568252 -0.0011323561 0.0015753878
		 -0.001075678 0.001542181 -0.00107937 0.0015357821 -0.0011108859 0.0014296633 -0.0011159849
		 0.001434551 -0.0010754912 0.0014748162 -0.001071355 0.001469883 -0.0010192464 0.0013989669
		 -0.0010250713 0.0014169592 -0.00092607806 0.0014546016 -0.00092948327 0.001448851
		 -0.00094484194 0.001458218 -0.00094181154 0.0014634026 -0.00092913077 0.001562426
		 -0.00092575519 0.0015566624 -0.00094154268 0.0015479657 -0.00094453903 0.0015531873
		 -0.0010237354 0.001613531 -0.0010170218 0.0016135332 -0.0010173804 0.0015955369 -0.0010234141
		 0.0015955273 -0.0011153512 0.0015570692 -0.0011119846 0.0015628657 -0.0010965456
		 0.0015535367 -0.0010995911 0.0015483106 -0.0010970817 0.0014450661 -0.0011012874
		 0.0014490781 -0.0010902341 0.0014603271 -0.0010865874 0.0014566178 -0.00101474 0.0014071114
		 -0.0010299514 0.0014075215 -0.0010291503 0.0014088016 -0.0010154361 0.0014084318
		 -0.00093113637 0.0014625801 -0.00093887106 0.0014493072 -0.00093960337 0.0014506095
		 -0.00093259232 0.001462586 -0.00093853578 0.0015620324 -0.0009308742 0.0015487196
		 -0.00093234325 0.0015487059 -0.00093925663 0.0015607212 -0.0010280509 0.0016052197
		 -0.001012689 0.0016051837 -0.0010134517 0.0016038815 -0.0010273063 0.0016039098 -0.001110292
		 0.0015490971 -0.0011025552 0.0015624415 -0.0011018249 0.0015611125 -0.0011087956
		 0.0015491253 -0.0010893135 0.0014477518 -0.0010990376 0.001456975 -0.0010978015 0.0014573361
		 -0.0010890294 0.0014490355 -0.0024989487 -0.0021152038 -0.0024992132 -0.0021409644
		 -0.0024999385 -0.002165738 -0.002500274 -0.0021898032 -0.0025002735 -0.0022136585
		 3.3141034e-005 -0.00045833172 4.1808704e-005 -0.00045833172 4.1807063e-005 -9.6464603e-008
		 -0.001016768 0.0014938496 -0.066140153 0.025205964 -0.001030462 0.0013434113 -0.0010107023
		 0.0014833098 -0.0010351947 0.0014859401 -0.0010147115 0.0014730522 -0.0010279809
		 0.0013581333 -0.0010169925 0.0014579293 -0.0010272402 0.0013756902 -0.0010177859
		 0.0014402635 -0.0010258912 0.0013991517 -0.0010190849 0.0014167868 -0.0025000123
		 -0.0022612445 -0.0025001816 -0.002237492 -0.0025130631 -0.0022613099 -0.00099612458
		 0.00150851 -0.0010106779 0.0015285603 -0.001035242 0.0015259631 -0.0010453056 0.0015085853
		 0.044125218 0.037376028 0.044125088 0.037368271 0.044141188 0.037357517 0.044144183
		 0.037385531 0.044118907 0.037380591 0.044120014 0.037399948 0.044112284 0.037377078
		 0.044096533 0.037387252 0.044111978 0.037369419 0.044095293 0.037361067 0.044118289
		 0.037365053 0.04411697 0.037346356 -0.066314481 0.025415314 -0.066319659 0.025302736
		 -0.066418059 0.025249593 -0.066512503 0.025308926 -0.06650801 0.025421197 -0.06640888
		 0.025474455 -0.0010207009 0.0015058985 -0.0011599689 0.0015077535 -0.0011580279 0.0015077535
		 -0.0011650468 0.0015157284 -0.0011662029 0.001515314 -0.0051165791 0.0014650548 -0.0051145102
		 0.0014650548 -0.0051085013 0.0014728548 -0.0051096044 0.0014732225 -0.001219469 -0.00034817719
		 -0.001217536 -0.00034817719 -0.001223095 -0.00033984057 -0.001224182 -0.00034012919
		 -0.0025134545 -0.0039215088 -0.0025116387 -0.0039215088 -0.0025061218 -0.003914332
		 -0.0025071669 -0.0039139958 -0.0011762269 0.0015261527 -0.0011770661 0.0015260006
		 -0.0050981017 0.0014831516 -0.0050988561 0.001483312 -0.0012329766 -0.00032942672
		 -0.0012337915 -0.00032962437 -0.0024964304 -0.0039044924 -0.0024971983 -0.0039043461
		 -0.0011741831 0.0015444156 -0.0011749982 0.0015444536 -0.0051001874 0.0015014884
		 -0.0051009469 0.0015014604 -0.0012336478 -0.00031266111 -0.0012344356 -0.00031267887
		 -0.0024984207 -0.0038872913 -0.0024991862 -0.0038873325 -0.0011676711 0.0015677441
		 -0.0011683144 0.0015677626 -0.0051067676 0.0015246692 -0.0051073898 0.0015246185
		 -0.001226168 -0.00029014991 -0.0012267474 -0.00029009584 -0.0025045848 -0.0038655435
		 -0.002505207 -0.0038655743 -0.0011676413 0.0015764064 -0.0011690215 0.0015766559
		 -0.0051058843 0.0015334458 -0.0051071909 0.0015332301 -0.0012269148 -0.00028125651
		 -0.001228282 -0.00028112467;
	setAttr ".uvtk[250:370]" -0.0025037567 -0.0038572496 -0.0025050361 -0.0038574883
		 -0.0028568667 -0.00046342719 -0.0028549775 -0.00046342719 -0.0028612739 -0.00045564113
		 -0.0028623943 -0.00045589209 -0.0050808452 -0.00046339375 -0.0050790627 -0.00046339375
		 -0.0050743073 -0.00045518272 -0.0050754179 -0.00045492078 -0.0029807554 -0.00040465765
		 -0.0029787945 -0.00040465765 -0.002985229 -0.00039660427 -0.0029863848 -0.00039696522
		 -0.0033187391 -0.00039834867 -0.0033168788 -0.00039834867 -0.0033113267 -0.00039116829
		 -0.0033124976 -0.00039084029 -0.0028702512 -0.00044380259 -0.0028711304 -0.00044395079
		 -0.0050649457 -0.00044427387 -0.0050657494 -0.00044409628 -0.0029957907 -0.00038580608
		 -0.002996661 -0.00038594715 -0.0033016994 -0.00038044818 -0.0033024512 -0.00038031777
		 -0.0028705485 -0.00042617726 -0.0028713853 -0.00042618869 -0.005064691 -0.00042662836
		 -0.0050655277 -0.0004266208 -0.0029938463 -0.0003670764 -0.0029946209 -0.00036705899
		 -0.0033036058 -0.00036244234 -0.0033043798 -0.00036244318 -0.00286326 -0.00040243671
		 -0.002864013 -0.00040238388 -0.0050720423 -0.00040314483 -0.0050727096 -0.00040319597
		 -0.002987538 -0.0003431199 -0.0029882281 -0.00034307395 -0.0033097279 -0.00033954717
		 -0.0033103779 -0.00033959301 -0.0028641128 -0.00039318524 -0.0028656665 -0.00039305969
		 -0.0050703734 -0.00039389546 -0.0050718035 -0.00039402393 -0.0029876649 -0.00033424149
		 -0.0029891201 -0.00033399867 -0.0033088517 -0.00033088963 -0.0033102459 -0.00033111809
		 -0.0011740181 -0.00028107484 -0.0011770289 -0.00028107484 -0.0011696161 -0.00029368509
		 -0.0011681755 -0.00029367322 -0.0011770289 -0.011121508 -0.0011770289 -0.011126184
		 -0.0011606951 -0.011134452 -0.0011604889 -0.011133028 -0.0015361005 -0.03988051 -0.0015361005
		 -0.039885338 -0.0015246964 -0.039867878 -0.0015263746 -0.039867088 0.12781975 0.0086190812
		 0.12781635 0.0086190812 0.12780975 0.0086121773 0.12780987 0.0086112553 -0.0011629405
		 -0.00030819717 -0.0011621631 -0.00030816373 -0.0011520627 -0.011141136 -0.0011518811
		 -0.011140952 -0.0015343525 -0.039844796 -0.0015349283 -0.03984471 0.12779975 0.0086243721
		 0.12779906 0.008623776 -0.0011639785 -0.00032243255 -0.0011633987 -0.0003224106 -0.0011546962
		 -0.011145116 -0.0011545389 -0.011145426 -0.0015319162 -0.039847977 -0.0015316083
		 -0.039847828 0.12778893 0.0086439028 0.12778823 0.0086439792 -0.001166875 -0.00033841046
		 -0.001166439 -0.00033839274 -0.0011670169 -0.011152087 -0.0011668926 -0.011152486
		 -0.0015305401 -0.039862074 -0.0015302424 -0.03986197 0.12779704 0.0086621046 0.12779658
		 0.0086624939 -0.0011655448 -0.00034425617 -0.0011645656 -0.00034421048 -0.0011744334
		 -0.011152712 -0.0011740223 -0.011153811 -0.0015289024 -0.039863348 -0.0015286535
		 -0.039863139 0.12780559 0.0086666346 0.12780453 0.0086677596 -0.0010331444 0.0015085399
		 -0.0010122126 0.0015153476 -0.0010083067 0.0015032694 -0.0010292083 0.0014964481
		 -0.0010246257 0.0015179844 -0.001030719 0.0014833111 -0.066387519 0.025674704 -0.0011770661
		 0.0015827597 -0.066126049 0.025493821 -0.0011326058 0.0013992661 -0.066674359 0.025541358
		 -0.0010300085 0.0016796838 -0.066700101 0.025224324 -0.00087552296 0.0016011302 -0.066438667
		 0.025045699 -0.00086660613 0.0014257572 -0.066155218 0.025181968 -0.0010135242 0.0013334142
		 -0.066420034 0.025682567 -0.066690229 0.025518084 -0.066687301 0.025200753 -0.066411115
		 0.025045345 -0.066136643 0.025526479;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "3A2DA25C-4422-2DF5-8265-608276B32FA1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 83 "e[4]" "e[10]" "e[28]" "e[40]" "e[52]" "e[64]" "e[81]" "e[86]" "e[88]" "e[90:91]" "e[94]" "e[96]" "e[98:99]" "e[102]" "e[104]" "e[106:107]" "e[110]" "e[112]" "e[114:115]" "e[118]" "e[120]" "e[122:123]" "e[126]" "e[128]" "e[130:131]" "e[134]" "e[136]" "e[138:139]" "e[142]" "e[144]" "e[146:147]" "e[150]" "e[152]" "e[154:155]" "e[158]" "e[160]" "e[162:163]" "e[166]" "e[168]" "e[170:171]" "e[174]" "e[176]" "e[178:179]" "e[182]" "e[184]" "e[186:187]" "e[190]" "e[192]" "e[194:195]" "e[198]" "e[200]" "e[202:203]" "e[206]" "e[208]" "e[210:211]" "e[214]" "e[216]" "e[218:219]" "e[222]" "e[224]" "e[226:227]" "e[230]" "e[232]" "e[234:235]" "e[238]" "e[240]" "e[242:243]" "e[246]" "e[248]" "e[250:251]" "e[254]" "e[256]" "e[258:259]" "e[262]" "e[264]" "e[266:267]" "e[270]" "e[272]" "e[274:275]" "e[310]" "e[314]" "e[334]" "e[346]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".a" 180;
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "80F2567E-4C9E-B9C4-8BD6-F3A714B83A93";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[6:11]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".a" 0;
createNode polySoftEdge -n "polySoftEdge3";
	rename -uid "352A4AF8-47DA-5B73-0AE6-EDA1028A979D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[20]" "e[22]" "e[24]" "e[26]" "e[28:29]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".a" 0;
createNode polySoftEdge -n "polySoftEdge4";
	rename -uid "A8413A26-4B24-CF1A-7416-459758C2387F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[36]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".a" 0;
createNode polySoftEdge -n "polySoftEdge5";
	rename -uid "DEBDAD96-43D1-4EE7-E467-E58B294F015A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[32]" "e[34]" "e[36]" "e[40:41]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".a" 0;
createNode polySoftEdge -n "polySoftEdge6";
	rename -uid "BC19B10D-4811-824C-631E-0DA6D009205C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[38]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".a" 0;
createNode polySoftEdge -n "polySoftEdge7";
	rename -uid "5535D907-4DE9-6296-6B00-03A0B8369678";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[36]";
	setAttr ".ix" -type "matrix" 0.13111108573680588 0 0 0 0 0.13111108573680588 0 0
		 0 0 0.13111108573680588 0 0 2.7845193101505887 0 1;
	setAttr ".a" 0;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polySoftEdge7.out" "pCylinderShape1.i";
connectAttr "polyTweakUV46.uvtk[0]" "pCylinderShape1.uvst[0].uvtw";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyTweak1.out" "polyExtrudeFace1.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyCylinder1.out" "polyTweak1.ip";
connectAttr "polyExtrudeFace1.out" "polyTweak2.ip";
connectAttr "polyTweak2.out" "polyExtrudeFace2.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace2.out" "polyExtrudeFace3.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polyTweak3.out" "polyExtrudeFace4.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polyExtrudeFace5.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace5.mp";
connectAttr "polyExtrudeFace4.out" "polyTweak4.ip";
connectAttr "polyExtrudeFace5.out" "polyExtrudeFace6.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace6.mp";
connectAttr "polyTweak5.out" "polyExtrudeFace7.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace7.mp";
connectAttr "polyExtrudeFace6.out" "polyTweak5.ip";
connectAttr "polyExtrudeFace7.out" "polyExtrudeFace8.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace8.mp";
connectAttr "polyExtrudeFace8.out" "polyExtrudeFace9.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace9.mp";
connectAttr "polyExtrudeFace9.out" "polyExtrudeFace10.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace10.mp";
connectAttr "polyTweak6.out" "polyExtrudeFace11.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace11.mp";
connectAttr "polyExtrudeFace10.out" "polyTweak6.ip";
connectAttr "polyTweak7.out" "polyExtrudeFace12.ip";
connectAttr "pCylinderShape1.wm" "polyExtrudeFace12.mp";
connectAttr "polyExtrudeFace11.out" "polyTweak7.ip";
connectAttr "polyTweak8.out" "polyMapDel1.ip";
connectAttr "polyExtrudeFace12.out" "polyTweak8.ip";
connectAttr "polyMapDel1.out" "polyCylProj1.ip";
connectAttr "pCylinderShape1.wm" "polyCylProj1.mp";
connectAttr "polyCylProj1.out" "polyTweakUV1.ip";
connectAttr "polyTweakUV1.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "polyTweakUV2.ip";
connectAttr "polyTweakUV2.out" "polyMapCut1.ip";
connectAttr "polyMapCut1.out" "polyMapCut2.ip";
connectAttr "polyMapCut2.out" "polyMapCut3.ip";
connectAttr "polyMapCut3.out" "polyTweakUV3.ip";
connectAttr "polyTweakUV3.out" "polyPlanarProj1.ip";
connectAttr "pCylinderShape1.wm" "polyPlanarProj1.mp";
connectAttr "polyPlanarProj1.out" "polyTweakUV4.ip";
connectAttr "polyTweakUV4.out" "polyPlanarProj2.ip";
connectAttr "pCylinderShape1.wm" "polyPlanarProj2.mp";
connectAttr "polyPlanarProj2.out" "polyTweakUV5.ip";
connectAttr "polyTweakUV5.out" "polyPlanarProj3.ip";
connectAttr "pCylinderShape1.wm" "polyPlanarProj3.mp";
connectAttr "polyPlanarProj3.out" "polyTweakUV6.ip";
connectAttr "polyTweakUV6.out" "polyMapDel2.ip";
connectAttr "polyMapDel2.out" "polyTweakUV7.ip";
connectAttr "polyTweakUV7.out" "polyPlanarProj4.ip";
connectAttr "pCylinderShape1.wm" "polyPlanarProj4.mp";
connectAttr "polyPlanarProj4.out" "polyPlanarProj5.ip";
connectAttr "pCylinderShape1.wm" "polyPlanarProj5.mp";
connectAttr "polyPlanarProj5.out" "polyTweakUV8.ip";
connectAttr "polyTweakUV8.out" "polyPlanarProj6.ip";
connectAttr "pCylinderShape1.wm" "polyPlanarProj6.mp";
connectAttr "polyPlanarProj6.out" "polyTweakUV9.ip";
connectAttr "polyTweakUV9.out" "polyMapSewMove1.ip";
connectAttr "polyMapSewMove1.out" "polyMapCut4.ip";
connectAttr "polyMapCut4.out" "polyTweakUV10.ip";
connectAttr "polyTweakUV10.out" "polyMapSewMove2.ip";
connectAttr "polyMapSewMove2.out" "polyTweakUV11.ip";
connectAttr "polyTweakUV11.out" "polyMapSewMove3.ip";
connectAttr "polyMapSewMove3.out" "polyTweakUV12.ip";
connectAttr "polyTweakUV12.out" "polyMapSewMove4.ip";
connectAttr "polyMapSewMove4.out" "polyTweakUV13.ip";
connectAttr "polyTweakUV13.out" "polyMapSewMove5.ip";
connectAttr "polyMapSewMove5.out" "polyTweakUV14.ip";
connectAttr "polyTweakUV14.out" "polyMapSewMove6.ip";
connectAttr "polyMapSewMove6.out" "polyTweakUV15.ip";
connectAttr "polyTweakUV15.out" "polyMapSewMove7.ip";
connectAttr "polyMapSewMove7.out" "polyMapCut5.ip";
connectAttr "polyMapCut5.out" "polyTweakUV16.ip";
connectAttr "polyTweakUV16.out" "polyMapSewMove8.ip";
connectAttr "polyMapSewMove8.out" "polyTweakUV17.ip";
connectAttr "polyTweakUV17.out" "polyMapSewMove9.ip";
connectAttr "polyMapSewMove9.out" "polyTweakUV18.ip";
connectAttr "polyTweakUV18.out" "polyMapSewMove10.ip";
connectAttr "polyMapSewMove10.out" "polyTweakUV19.ip";
connectAttr "polyTweakUV19.out" "polyMapSewMove11.ip";
connectAttr "polyMapSewMove11.out" "polyTweakUV20.ip";
connectAttr "polyTweakUV20.out" "polyMapSewMove12.ip";
connectAttr "polyMapSewMove12.out" "polyTweakUV21.ip";
connectAttr "polyTweakUV21.out" "polyMapSewMove13.ip";
connectAttr "polyMapSewMove13.out" "polyTweakUV22.ip";
connectAttr "polyTweakUV22.out" "polyMapSewMove14.ip";
connectAttr "polyMapSewMove14.out" "polyTweakUV23.ip";
connectAttr "polyTweakUV23.out" "polyMapSewMove15.ip";
connectAttr "polyMapSewMove15.out" "polyTweakUV24.ip";
connectAttr "polyTweakUV24.out" "polyMapSewMove16.ip";
connectAttr "polyMapSewMove16.out" "polyTweakUV25.ip";
connectAttr "polyTweakUV25.out" "polyMapSewMove17.ip";
connectAttr "polyMapSewMove17.out" "polyTweakUV26.ip";
connectAttr "polyTweakUV26.out" "polyMapSewMove18.ip";
connectAttr "polyMapSewMove18.out" "polyTweakUV27.ip";
connectAttr "polyTweakUV27.out" "polyMapSewMove19.ip";
connectAttr "polyMapSewMove19.out" "polyTweakUV28.ip";
connectAttr "polyTweakUV28.out" "polyMapSewMove20.ip";
connectAttr "polyMapSewMove20.out" "polyTweakUV29.ip";
connectAttr "polyTweakUV29.out" "polyMapSewMove21.ip";
connectAttr "polyMapSewMove21.out" "polyTweakUV30.ip";
connectAttr "polyTweakUV30.out" "polyMapSewMove22.ip";
connectAttr "polyMapSewMove22.out" "polyTweakUV31.ip";
connectAttr "polyTweakUV31.out" "polyMapSewMove23.ip";
connectAttr "polyMapSewMove23.out" "polyTweakUV32.ip";
connectAttr "polyTweakUV32.out" "polyMapSewMove24.ip";
connectAttr "polyMapSewMove24.out" "polyTweakUV33.ip";
connectAttr "polyTweakUV33.out" "polyMapSewMove25.ip";
connectAttr "polyMapSewMove25.out" "polyMapCut6.ip";
connectAttr "polyMapCut6.out" "polyTweakUV34.ip";
connectAttr "polyTweakUV34.out" "polyMapSewMove26.ip";
connectAttr "polyMapSewMove26.out" "polyTweakUV35.ip";
connectAttr "polyTweakUV35.out" "polyMapSewMove27.ip";
connectAttr "polyMapSewMove27.out" "polyTweakUV36.ip";
connectAttr "polyTweakUV36.out" "polyMapSewMove28.ip";
connectAttr "polyMapSewMove28.out" "polyTweakUV37.ip";
connectAttr "polyTweakUV37.out" "polyMapSewMove29.ip";
connectAttr "polyMapSewMove29.out" "polyTweakUV38.ip";
connectAttr "polyTweakUV38.out" "polyMapSewMove30.ip";
connectAttr "polyMapSewMove30.out" "polyTweakUV39.ip";
connectAttr "polyTweakUV39.out" "polyMapCut7.ip";
connectAttr "polyMapCut7.out" "polyTweakUV40.ip";
connectAttr "polyTweakUV40.out" "polyMapSewMove31.ip";
connectAttr "polyMapSewMove31.out" "polyTweakUV41.ip";
connectAttr "polyTweakUV41.out" "polyMapSewMove32.ip";
connectAttr "polyMapSewMove32.out" "polyTweakUV42.ip";
connectAttr "polyTweakUV42.out" "polyMapSewMove33.ip";
connectAttr "polyMapSewMove33.out" "polyTweakUV43.ip";
connectAttr "polyTweakUV43.out" "polyMapSewMove34.ip";
connectAttr "polyMapSewMove34.out" "polyTweakUV44.ip";
connectAttr "polyTweakUV44.out" "polyMapSewMove35.ip";
connectAttr "polyMapSewMove35.out" "polyTweakUV45.ip";
connectAttr "polyTweakUV45.out" "polyMapSewMove36.ip";
connectAttr "polyMapSewMove36.out" "polyTweakUV46.ip";
connectAttr "polyTweakUV46.out" "polySoftEdge1.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge1.mp";
connectAttr "polySoftEdge1.out" "polySoftEdge2.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge2.mp";
connectAttr "polySoftEdge2.out" "polySoftEdge3.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge3.mp";
connectAttr "polySoftEdge3.out" "polySoftEdge4.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge4.mp";
connectAttr "polySoftEdge4.out" "polySoftEdge5.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge5.mp";
connectAttr "polySoftEdge5.out" "polySoftEdge6.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge6.mp";
connectAttr "polySoftEdge6.out" "polySoftEdge7.ip";
connectAttr "pCylinderShape1.wm" "polySoftEdge7.mp";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCylinderShape1.iog" ":initialShadingGroup.dsm" -na;
// End of TempleTallTorch_model_001.ma
