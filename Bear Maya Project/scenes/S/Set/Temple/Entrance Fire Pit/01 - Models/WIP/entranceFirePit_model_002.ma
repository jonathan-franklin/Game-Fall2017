//Maya ASCII 2017ff05 scene
//Name: entranceFirePit_model_002.ma
//Last modified: Tue, Oct 17, 2017 11:47:44 AM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "education";
createNode transform -s -n "persp";
	rename -uid "322A929F-4EF1-D6B4-0706-328490617B83";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.9495772550052364 3.6814574556056838 -2.0712626633544824 ;
	setAttr ".r" -type "double3" 313.46164727037944 1214.1999999996865 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "35D87C16-44D9-877D-0712-D388E6080829";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 3.9569783275927106;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "1CAB591B-48A9-0B25-A227-3C8BBB17491F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "169651D2-4AC5-B7C7-2366-1895CA12111C";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "1A840E39-42C9-E2FC-11AC-DB80895FEE23";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "EE804475-4B6C-F774-F2A5-2AB975625C4A";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "58A46D18-45EE-61B2-9B89-A185FD86140A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "664A4C86-42D6-4371-6F12-C6A7A3C0B2F1";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCube1";
	rename -uid "14A6209C-4414-20AE-25BF-679E6ECACEE0";
	setAttr ".t" -type "double3" 0 0.73565021076298032 0 ;
createNode transform -n "transform5" -p "pCube1";
	rename -uid "8510DA45-4211-0E27-3090-41A88F9E3D8F";
	setAttr ".v" no;
createNode mesh -n "pCubeShape1" -p "transform5";
	rename -uid "CF99669C-4C48-80F7-9FF9-1A87901CCCFD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.59552085399627686 0.14535024762153625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube2";
	rename -uid "2863F600-4BAF-4CAB-D3C4-97BEBCB1F4BE";
	setAttr ".t" -type "double3" 0 0.57069106643202316 0 ;
	setAttr ".r" -type "double3" -1.3690842423592851 4.0619618396068953 -0.060265515185400888 ;
	setAttr ".s" -type "double3" 0.83675397595331213 0.83675397595331213 0.83675397595331213 ;
createNode mesh -n "polySurfaceShape2" -p "pCube2";
	rename -uid "D2D46F88-42B0-D327-5203-679C4BA7BE14";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0 0.40816814 0 0 0.40816814 
		0 0 -0.40816814 0 0 -0.40816814 0 0 -0.40816814 0 0 -0.40816814 0 0 0.40816814 0 
		0 0.40816814 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform6" -p "pCube2";
	rename -uid "9F234381-46DC-5DE2-5531-4A8AB7EE87A9";
	setAttr ".v" no;
createNode mesh -n "pCubeShape2" -p "transform6";
	rename -uid "2012193D-4173-2736-EB2F-4E8C31FF5FB8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.74876250326633453 0.22225302457809448 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube5";
	rename -uid "B1C25E4D-423F-13F4-8BF5-FCB77C3C718C";
	setAttr ".t" -type "double3" -0.22956569213589129 0.25370942134715668 -0.28598677956645085 ;
	setAttr ".r" -type "double3" 0 -5.6769683566293132 0 ;
	setAttr ".s" -type "double3" 1.4098220620753179 1.4098220620753179 1.4098220620753179 ;
createNode mesh -n "polySurfaceShape1" -p "pCube5";
	rename -uid "834604A8-4437-CBB7-D6E0-408D5CCBA07E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0.35101193 0.32822156 -0.33597836 
		-0.35101193 0.32822156 -0.33597836 0.35101193 -0.32822156 -0.33597836 -0.35101193 
		-0.32822156 -0.33597836 0.35101193 -0.32822156 0.33597836 -0.35101193 -0.32822156 
		0.33597836 0.35101193 0.32822156 0.33597836 -0.35101193 0.32822156 0.33597836;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform1" -p "pCube5";
	rename -uid "9A963DEB-42B8-BB61-474C-46A3E6451AE5";
	setAttr ".v" no;
createNode mesh -n "pCubeShape5" -p "transform1";
	rename -uid "539C7876-4DA4-5E95-7714-7FAE20C505D1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube6";
	rename -uid "5BC42C56-4A54-9E26-9CD0-8AA58678AA1B";
	setAttr ".t" -type "double3" 0.22972442718086772 0.25370942134715757 -0.28957447760203181 ;
	setAttr ".r" -type "double3" 0 -6.8165851601195371 0 ;
	setAttr ".s" -type "double3" 1.4098220620753179 1.4098220620753179 1.4098220620753179 ;
createNode mesh -n "polySurfaceShape1" -p "pCube6";
	rename -uid "A930331B-4E62-5876-7D19-288D951F8341";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0.35101193 0.32822156 -0.33597836 
		-0.35101193 0.32822156 -0.33597836 0.35101193 -0.32822156 -0.33597836 -0.35101193 
		-0.32822156 -0.33597836 0.35101193 -0.32822156 0.33597836 -0.35101193 -0.32822156 
		0.33597836 0.35101193 0.32822156 0.33597836 -0.35101193 0.32822156 0.33597836;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform2" -p "pCube6";
	rename -uid "232D311F-4670-222E-672B-45ACB5108903";
	setAttr ".v" no;
createNode mesh -n "pCubeShape6" -p "transform2";
	rename -uid "7838EF52-4C8B-8492-CB99-06BEDE61377A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:25]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 38 ".uvst[0].uvsp[0:37]" -type "float2" 0.61042184 0.48436731
		 0.60429835 0.48436728 0.60429835 0.37300938 0.61042184 3.6502594e-009 0.61042184
		 0.0053110984 0.72678071 0.48436728 0.72065723 0.48436731 0.72678071 0.37300938 0.73234296
		 0.0053111031 0.60429835 0.12804464 0.60429835 0.23940253 0.61042184 0.11717132 0.72065723
		 0.11717132 0.72678071 0.12804464 0.60429835 0.25027588 0.60429835 0.3621361 0.61042184
		 0.23940253 0.72065723 0.23940253 0.72678071 0.25027585 0.72678071 0.36213607 0.61042184
		 0.36213607 0.72065723 0.36213607 0.72065723 0.37300938 0.72065723 0.0053111017 0.61042184
		 0.12804464 0.72065723 0.12804464 0.61042184 0.25027588 0.72065723 0.25027588 0.61042184
		 0.37300938 0.84370083 0.0053111026 0.84370089 0.11717131 0.48737818 0.0053110975
		 0.59873605 0.0053110993 0.59873611 0.11717132 0.48737818 0.11717132 0.72065723 0
		 0.73234296 0.11717132 0.72678071 0.23940253;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".vt[0:23]"  -0.13409077 -0.17177841 0.14912432 -0.13409077 -0.15688109 0.16402164
		 -0.14898808 -0.15688109 0.14912432 0.1489881 -0.15688109 0.14912432 0.13409078 -0.15688109 0.16402164
		 0.13409078 -0.17177841 0.14912432 -0.14898808 0.15688121 0.14912432 -0.13409077 0.15688121 0.16402164
		 -0.13409077 0.1717785 0.14912432 0.13409078 0.1717785 0.14912432 0.13409078 0.15688121 0.16402164
		 0.1489881 0.15688121 0.14912432 -0.14898808 0.15688121 -0.14912438 -0.13409077 0.1717785 -0.14912438
		 -0.13409077 0.15688121 -0.1640217 0.13409078 0.15688121 -0.1640217 0.13409078 0.1717785 -0.14912438
		 0.1489881 0.15688121 -0.14912438 -0.14898808 -0.15688109 -0.14912438 -0.13409077 -0.15688109 -0.1640217
		 -0.13409077 -0.17177841 -0.14912438 0.13409078 -0.17177841 -0.14912438 0.13409078 -0.15688109 -0.1640217
		 0.1489881 -0.15688109 -0.14912438;
	setAttr -s 48 ".ed[0:47]"  0 2 0 2 18 0 18 20 0 20 0 0 1 0 0 0 5 0 5 4 0
		 4 1 0 2 1 0 1 7 0 7 6 0 6 2 0 3 5 0 5 21 0 21 23 0 23 3 0 4 3 0 3 11 0 11 10 0 10 4 0
		 6 8 0 8 13 0 13 12 0 12 6 0 8 7 0 7 10 0 10 9 0 9 8 0 9 11 0 11 17 0 17 16 0 16 9 0
		 12 14 0 14 19 0 19 18 0 18 12 0 14 13 0 13 16 0 16 15 0 15 14 0 15 17 0 17 23 0 23 22 0
		 22 15 0 20 19 0 19 22 0 22 21 0 21 20 0;
	setAttr -s 26 -ch 96 ".fc[0:25]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 28
		f 4 4 5 6 7
		mu 0 4 4 3 35 23
		f 4 8 9 10 11
		mu 0 4 32 4 11 33
		f 4 12 13 14 15
		mu 0 4 5 6 22 7
		f 4 16 17 18 19
		mu 0 4 23 8 36 12
		f 4 20 21 22 23
		mu 0 4 9 24 16 10
		f 4 24 25 26 27
		mu 0 4 24 11 12 25
		f 4 28 29 30 31
		mu 0 4 25 13 37 17
		f 4 32 33 34 35
		mu 0 4 14 26 20 15
		f 4 36 37 38 39
		mu 0 4 26 16 17 27
		f 4 40 41 42 43
		mu 0 4 27 18 19 21
		f 4 44 45 46 47
		mu 0 4 28 20 21 22
		f 4 -8 -20 -26 -10
		mu 0 4 4 23 12 11
		f 4 -28 -32 -38 -22
		mu 0 4 24 25 17 16
		f 4 -40 -44 -46 -34
		mu 0 4 26 27 21 20
		f 4 -48 -14 -6 -4
		mu 0 4 28 22 6 0
		f 4 -16 -42 -30 -18
		mu 0 4 8 29 30 36
		f 4 -2 -12 -24 -36
		mu 0 4 31 32 33 34
		f 3 -5 -9 -1
		mu 0 3 3 4 32
		f 3 -17 -7 -13
		mu 0 3 8 23 35
		f 3 -11 -25 -21
		mu 0 3 33 11 24
		f 3 -27 -19 -29
		mu 0 3 25 12 36
		f 3 -23 -37 -33
		mu 0 3 10 16 26
		f 3 -39 -31 -41
		mu 0 3 27 17 37
		f 3 -35 -45 -3
		mu 0 3 15 20 28
		f 3 -47 -43 -15
		mu 0 3 22 21 19;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube7";
	rename -uid "AB474811-48C4-4AC1-0571-5EAD7A987368";
	setAttr ".t" -type "double3" 0.27107485411291116 0.25370942134715668 0.25388077966091488 ;
	setAttr ".r" -type "double3" 0 27.297148981362444 0 ;
	setAttr ".s" -type "double3" 1.4098220620753179 1.4098220620753179 1.4098220620753179 ;
createNode mesh -n "polySurfaceShape1" -p "pCube7";
	rename -uid "16759149-4E2C-E96F-7EFE-60905A06473D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0.35101193 0.32822156 -0.33597836 
		-0.35101193 0.32822156 -0.33597836 0.35101193 -0.32822156 -0.33597836 -0.35101193 
		-0.32822156 -0.33597836 0.35101193 -0.32822156 0.33597836 -0.35101193 -0.32822156 
		0.33597836 0.35101193 0.32822156 0.33597836 -0.35101193 0.32822156 0.33597836;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform3" -p "pCube7";
	rename -uid "36FEBCC1-4BA8-8050-A8AB-0AB796ED9ECF";
	setAttr ".v" no;
createNode mesh -n "pCubeShape7" -p "transform3";
	rename -uid "093AB6C8-46AE-6B6C-6B7A-D59775FF2C6B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:25]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 38 ".uvst[0].uvsp[0:37]" -type "float2" 0.36500445 0.59902376
		 0.35888094 0.5990237 0.35888094 0.48766583 0.36500445 0.11465645 0.36500445 0.11996754
		 0.48136333 0.5990237 0.47523984 0.59902376 0.48136333 0.48766583 0.48692557 0.11996755
		 0.35888094 0.24270108 0.35888094 0.35405898 0.36500445 0.23182777 0.47523984 0.23182777
		 0.48136333 0.24270108 0.35888094 0.3649323 0.35888094 0.47679251 0.36500445 0.35405898
		 0.47523984 0.35405898 0.48136333 0.36493227 0.48136333 0.47679248 0.36500445 0.47679248
		 0.47523984 0.47679248 0.47523987 0.48766583 0.47523984 0.11996755 0.36500445 0.24270108
		 0.47523987 0.24270108 0.36500445 0.3649323 0.47523984 0.3649323 0.36500445 0.48766583
		 0.59828347 0.11996755 0.59828347 0.23182775 0.24196081 0.11996754 0.35331869 0.11996754
		 0.35331872 0.23182777 0.24196081 0.23182777 0.47523984 0.11465645 0.48692557 0.23182777
		 0.48136333 0.35405898;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".vt[0:23]"  -0.13409077 -0.17177841 0.14912432 -0.13409077 -0.15688109 0.16402164
		 -0.14898808 -0.15688109 0.14912432 0.1489881 -0.15688109 0.14912432 0.13409078 -0.15688109 0.16402164
		 0.13409078 -0.17177841 0.14912432 -0.14898808 0.15688121 0.14912432 -0.13409077 0.15688121 0.16402164
		 -0.13409077 0.1717785 0.14912432 0.13409078 0.1717785 0.14912432 0.13409078 0.15688121 0.16402164
		 0.1489881 0.15688121 0.14912432 -0.14898808 0.15688121 -0.14912438 -0.13409077 0.1717785 -0.14912438
		 -0.13409077 0.15688121 -0.1640217 0.13409078 0.15688121 -0.1640217 0.13409078 0.1717785 -0.14912438
		 0.1489881 0.15688121 -0.14912438 -0.14898808 -0.15688109 -0.14912438 -0.13409077 -0.15688109 -0.1640217
		 -0.13409077 -0.17177841 -0.14912438 0.13409078 -0.17177841 -0.14912438 0.13409078 -0.15688109 -0.1640217
		 0.1489881 -0.15688109 -0.14912438;
	setAttr -s 48 ".ed[0:47]"  0 2 0 2 18 0 18 20 0 20 0 0 1 0 0 0 5 0 5 4 0
		 4 1 0 2 1 0 1 7 0 7 6 0 6 2 0 3 5 0 5 21 0 21 23 0 23 3 0 4 3 0 3 11 0 11 10 0 10 4 0
		 6 8 0 8 13 0 13 12 0 12 6 0 8 7 0 7 10 0 10 9 0 9 8 0 9 11 0 11 17 0 17 16 0 16 9 0
		 12 14 0 14 19 0 19 18 0 18 12 0 14 13 0 13 16 0 16 15 0 15 14 0 15 17 0 17 23 0 23 22 0
		 22 15 0 20 19 0 19 22 0 22 21 0 21 20 0;
	setAttr -s 26 -ch 96 ".fc[0:25]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 28
		f 4 4 5 6 7
		mu 0 4 4 3 35 23
		f 4 8 9 10 11
		mu 0 4 32 4 11 33
		f 4 12 13 14 15
		mu 0 4 5 6 22 7
		f 4 16 17 18 19
		mu 0 4 23 8 36 12
		f 4 20 21 22 23
		mu 0 4 9 24 16 10
		f 4 24 25 26 27
		mu 0 4 24 11 12 25
		f 4 28 29 30 31
		mu 0 4 25 13 37 17
		f 4 32 33 34 35
		mu 0 4 14 26 20 15
		f 4 36 37 38 39
		mu 0 4 26 16 17 27
		f 4 40 41 42 43
		mu 0 4 27 18 19 21
		f 4 44 45 46 47
		mu 0 4 28 20 21 22
		f 4 -8 -20 -26 -10
		mu 0 4 4 23 12 11
		f 4 -28 -32 -38 -22
		mu 0 4 24 25 17 16
		f 4 -40 -44 -46 -34
		mu 0 4 26 27 21 20
		f 4 -48 -14 -6 -4
		mu 0 4 28 22 6 0
		f 4 -16 -42 -30 -18
		mu 0 4 8 29 30 36
		f 4 -2 -12 -24 -36
		mu 0 4 31 32 33 34
		f 3 -5 -9 -1
		mu 0 3 3 4 32
		f 3 -17 -7 -13
		mu 0 3 8 23 35
		f 3 -11 -25 -21
		mu 0 3 33 11 24
		f 3 -27 -19 -29
		mu 0 3 25 12 36
		f 3 -23 -37 -33
		mu 0 3 10 16 26
		f 3 -39 -31 -41
		mu 0 3 27 17 37
		f 3 -35 -45 -3
		mu 0 3 15 20 28
		f 3 -47 -43 -15
		mu 0 3 22 21 19;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube8";
	rename -uid "3EFB0FAD-4340-D70B-B9AF-49A0DD431385";
	setAttr ".t" -type "double3" -0.23119824625306706 0.25370942134715713 0.20749027339871035 ;
	setAttr ".r" -type "double3" 0 -7.0420464735682469 0 ;
	setAttr ".s" -type "double3" 1.4098220620753179 1.4098220620753179 1.4098220620753179 ;
createNode mesh -n "polySurfaceShape1" -p "pCube8";
	rename -uid "91EED2D0-4EE7-B80A-71BB-3581ADD6DF4B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  0.35101193 0.32822156 -0.33597836 
		-0.35101193 0.32822156 -0.33597836 0.35101193 -0.32822156 -0.33597836 -0.35101193 
		-0.32822156 -0.33597836 0.35101193 -0.32822156 0.33597836 -0.35101193 -0.32822156 
		0.33597836 0.35101193 0.32822156 0.33597836 -0.35101193 0.32822156 0.33597836;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform4" -p "pCube8";
	rename -uid "2243A085-4FAB-FFE4-A212-838AF8C2F881";
	setAttr ".v" no;
createNode mesh -n "pCubeShape8" -p "transform4";
	rename -uid "8215C179-49B2-960B-8CFB-E597B03B50D1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:25]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 38 ".uvst[0].uvsp[0:37]" -type "float2" 0.12304365 0.48436731
		 0.11692015 0.48436728 0.11692015 0.37300938 0.12304365 3.6502594e-009 0.12304365
		 0.0053110984 0.23940253 0.48436728 0.23327903 0.48436731 0.23940253 0.37300938 0.24496476
		 0.0053111031 0.11692015 0.12804464 0.11692015 0.23940253 0.12304365 0.11717132 0.23327903
		 0.11717132 0.23940253 0.12804464 0.11692015 0.25027588 0.11692015 0.3621361 0.12304365
		 0.23940253 0.23327903 0.23940253 0.23940253 0.25027585 0.23940253 0.36213607 0.12304365
		 0.36213607 0.23327903 0.36213607 0.23327906 0.37300938 0.23327903 0.0053111017 0.12304365
		 0.12804464 0.23327906 0.12804464 0.12304365 0.25027588 0.23327903 0.25027588 0.12304365
		 0.37300938 0.35632265 0.0053111026 0.35632268 0.11717131 0 0.0053110975 0.11135788
		 0.0053110993 0.11135791 0.11717132 0 0.11717132 0.23327903 0 0.24496476 0.11717132
		 0.23940253 0.23940253;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".vt[0:23]"  -0.13409077 -0.17177841 0.14912432 -0.13409077 -0.15688109 0.16402164
		 -0.14898808 -0.15688109 0.14912432 0.1489881 -0.15688109 0.14912432 0.13409078 -0.15688109 0.16402164
		 0.13409078 -0.17177841 0.14912432 -0.14898808 0.15688121 0.14912432 -0.13409077 0.15688121 0.16402164
		 -0.13409077 0.1717785 0.14912432 0.13409078 0.1717785 0.14912432 0.13409078 0.15688121 0.16402164
		 0.1489881 0.15688121 0.14912432 -0.14898808 0.15688121 -0.14912438 -0.13409077 0.1717785 -0.14912438
		 -0.13409077 0.15688121 -0.1640217 0.13409078 0.15688121 -0.1640217 0.13409078 0.1717785 -0.14912438
		 0.1489881 0.15688121 -0.14912438 -0.14898808 -0.15688109 -0.14912438 -0.13409077 -0.15688109 -0.1640217
		 -0.13409077 -0.17177841 -0.14912438 0.13409078 -0.17177841 -0.14912438 0.13409078 -0.15688109 -0.1640217
		 0.1489881 -0.15688109 -0.14912438;
	setAttr -s 48 ".ed[0:47]"  0 2 0 2 18 0 18 20 0 20 0 0 1 0 0 0 5 0 5 4 0
		 4 1 0 2 1 0 1 7 0 7 6 0 6 2 0 3 5 0 5 21 0 21 23 0 23 3 0 4 3 0 3 11 0 11 10 0 10 4 0
		 6 8 0 8 13 0 13 12 0 12 6 0 8 7 0 7 10 0 10 9 0 9 8 0 9 11 0 11 17 0 17 16 0 16 9 0
		 12 14 0 14 19 0 19 18 0 18 12 0 14 13 0 13 16 0 16 15 0 15 14 0 15 17 0 17 23 0 23 22 0
		 22 15 0 20 19 0 19 22 0 22 21 0 21 20 0;
	setAttr -s 26 -ch 96 ".fc[0:25]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 28
		f 4 4 5 6 7
		mu 0 4 4 3 35 23
		f 4 8 9 10 11
		mu 0 4 32 4 11 33
		f 4 12 13 14 15
		mu 0 4 5 6 22 7
		f 4 16 17 18 19
		mu 0 4 23 8 36 12
		f 4 20 21 22 23
		mu 0 4 9 24 16 10
		f 4 24 25 26 27
		mu 0 4 24 11 12 25
		f 4 28 29 30 31
		mu 0 4 25 13 37 17
		f 4 32 33 34 35
		mu 0 4 14 26 20 15
		f 4 36 37 38 39
		mu 0 4 26 16 17 27
		f 4 40 41 42 43
		mu 0 4 27 18 19 21
		f 4 44 45 46 47
		mu 0 4 28 20 21 22
		f 4 -8 -20 -26 -10
		mu 0 4 4 23 12 11
		f 4 -28 -32 -38 -22
		mu 0 4 24 25 17 16
		f 4 -40 -44 -46 -34
		mu 0 4 26 27 21 20
		f 4 -48 -14 -6 -4
		mu 0 4 28 22 6 0
		f 4 -16 -42 -30 -18
		mu 0 4 8 29 30 36
		f 4 -2 -12 -24 -36
		mu 0 4 31 32 33 34
		f 3 -5 -9 -1
		mu 0 3 3 4 32
		f 3 -17 -7 -13
		mu 0 3 8 23 35
		f 3 -11 -25 -21
		mu 0 3 33 11 24
		f 3 -27 -19 -29
		mu 0 3 25 12 36
		f 3 -23 -37 -33
		mu 0 3 10 16 26
		f 3 -39 -31 -41
		mu 0 3 27 17 37
		f 3 -35 -45 -3
		mu 0 3 15 20 28
		f 3 -47 -43 -15
		mu 0 3 22 21 19;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube9";
	rename -uid "AD567DF0-43E4-E1F5-D34A-DF9042C6DD3A";
	setAttr ".rp" -type "double3" 0.031889639316879992 0.41950725094069552 0.0057937973690566635 ;
	setAttr ".sp" -type "double3" 0.031889639316879992 0.41950725094069552 0.0057937973690566635 ;
createNode mesh -n "pCube9Shape" -p "pCube9";
	rename -uid "1E13C798-4951-00EB-B49D-FE9F3421C426";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "FDE23396-47BC-42F9-9ECA-11A2CB4FA3E5";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "979EEA29-4170-9C7D-AA2F-8AA5E2776ED7";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "29A83FB5-4232-6DA8-1B94-EFAB9BAA3FC5";
createNode displayLayerManager -n "layerManager";
	rename -uid "1A05F5D9-43F5-6195-A18C-519366D3DAAB";
createNode displayLayer -n "defaultLayer";
	rename -uid "551D03B9-4B62-A929-9F52-939F6ED130D8";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "E014B441-4EB0-98D0-36FE-91947CE1FC84";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "7BB4F5D9-466B-6B7B-45D4-76ABF199DB12";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "F5770B5E-44DC-7A53-A1FB-279D48E4FE32";
	setAttr ".cuv" 4;
createNode polyBevel3 -n "polyBevel1";
	rename -uid "14C45BB0-493B-FC40-F92E-42A90DCBDEEC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 1.4098220620753179 0 0 0 0 1.4098220620753179 0 0 0 0 1.4098220620753179 0
		 -0.22300743306343662 0.25370942134715713 -0.27505357395891467 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.099999999999999978;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "4BB2740E-494D-B39F-4DA5-869B4DC10CAB";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0.73565021076298032 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 0.82748204 0 ;
	setAttr ".rs" 55194;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.5 0.82748207368946347 -0.5 ;
	setAttr ".cbx" -type "double3" 0.5 0.82748207368946347 0.5 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "3ACE063A-4645-1BDF-5906-129D82510CE7";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  0 0.40816814 0 0 0.40816814
		 0 0 -0.40816814 0 0 -0.40816814 0 0 -0.40816814 0 0 -0.40816814 0 0 0.40816814 0
		 0 0.40816814 0;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "B9281FA5-4033-8676-5065-9DB7A010A917";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0.73565021076298032 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 0.82748204 0 ;
	setAttr ".rs" 48350;
	setAttr ".lt" -type "double3" 0 6.1629758220391547e-033 -0.16424168784123938 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.19072020053863525 0.82748207368946347 -0.19072020053863525 ;
	setAttr ".cbx" -type "double3" 0.19072020053863525 0.82748207368946347 0.19072020053863525 ;
createNode polyTweak -n "polyTweak2";
	rename -uid "FBBA225A-400D-7062-9DD1-21807A4443E2";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[8:11]" -type "float3"  0.3092798 -2.7755576e-017
		 -0.3092798 -0.3092798 2.7755576e-017 -0.3092798 -0.3092798 2.7755576e-017 0.3092798
		 0.3092798 -2.7755576e-017 0.3092798;
createNode polyBevel3 -n "polyBevel2";
	rename -uid "85806C11-481E-A615-A4D1-09980E11A634";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 0.83465161305484559 -0.00087791331994462247 -0.059271665800979798 0
		 -0.00053628962825183924 0.8365161324525201 -0.01994213664050706 0 0.059275741176766449 0.019930019783148449 0.83441380447177671 0
		 0 0.57069106643202316 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.099999999999999978;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode polyBevel3 -n "polyBevel3";
	rename -uid "AE7B77C1-4819-F32F-E9C1-1B90BC056CF7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0.73565021076298032 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.3;
	setAttr ".at" 180;
	setAttr ".sn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "D7A546F1-44C1-DE52-7591-7F87C82D4E21";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n"
		+ "            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n"
		+ "            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 705\n            -height 811\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n"
		+ "            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n"
		+ "            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n"
		+ "            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n"
		+ "                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                -outliner \"graphEditor1OutlineEd\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n"
		+ "                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n"
		+ "                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n"
		+ "                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n"
		+ "                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n"
		+ "                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n"
		+ "\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 705\\n    -height 811\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 705\\n    -height 811\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "B6D8B6F1-4282-9F2D-6F1D-E491BFDD09D3";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyMapDel -n "polyMapDel1";
	rename -uid "A4C420EE-49E0-D9EF-8F1F-1EB53FEC2F38";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyPlanarProj -n "polyPlanarProj1";
	rename -uid "E3066E27-4C81-B057-C976-8EBF6DF81BE7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 13 "f[0:1]" "f[3]" "f[5:11]" "f[13]" "f[15]" "f[17:18]" "f[20]" "f[22]" "f[24:27]" "f[29]" "f[31]" "f[34:37]" "f[42:57]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0.73565021076298032 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 0.73565018177032471 0 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj2";
	rename -uid "6971E257-4B24-3343-CC91-FD80653E1F77";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 12 "f[2]" "f[4]" "f[12]" "f[14]" "f[16]" "f[19]" "f[21]" "f[23]" "f[28]" "f[30]" "f[38]" "f[40]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0.73565021076298032 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 0.73614612221717834 0 ;
	setAttr ".ps" -type "double2" 1 0.98875668697704977 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj3";
	rename -uid "A725499C-45E4-1251-BD10-68BDE2421ED7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[32:33]" "f[39]" "f[41]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0.73565021076298032 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 0.73614612221717834 0 ;
	setAttr ".ro" -type "double3" 0 90 0 ;
	setAttr ".ps" -type "double2" 0.95271623134613037 0.93000512071100172 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "C39047AE-4BE5-286B-A3B2-2A8ED68BB721";
	setAttr ".uopa" yes;
	setAttr -s 104 ".uvtk[0:103]" -type "float2" -0.012321776 0.48747963 0
		 0.48747963 0 0.011924505 -0.012321776 0.011924505 -0.012321776 0.4998014 -0.48787686
		 0.48747963 -0.48787686 0.4998014 -0.5001986 0.48747963 -0.48787686 0.011924505 -0.5001986
		 0.011924505 -0.012780854 -0.025561649 -0.025561709 -0.012780795 -0.15470134 -0.14214072
		 -0.14214081 -0.15470128 0 -0.012073632 -0.012780854 -0.47463691 0 -0.48837295 -0.011825639
		 5.9604645e-008 -0.48812491 5.9604645e-008 -0.47463694 -0.012780795 -0.48741779 -0.025561649
		 -0.35805783 -0.15470128 -0.34549731 -0.14214072 -0.5001986 -0.011825579 -0.5001986
		 -0.48812488 -0.48741779 -0.47463691 -0.025561709 -0.48741776 -0.14214081 -0.34549722
		 -0.15470134 -0.3580578 -0.012073722 -0.5001986 -0.47463694 -0.48741776 -0.48837304
		 -0.5001986 -0.34549731 -0.3580578 -0.35805783 -0.34549722 -0.012321776 -0.00039726496
		 -0.48787686 -0.00039726496 -0.15470134 -0.16677468 -0.15470134 -0.33342385 -0.16677473
		 -0.15470128 -0.33342388 -0.15470128 -0.34549731 -0.16677468 -0.34549731 -0.33342385
		 -0.33342388 -0.34549722 -0.16677473 -0.34549722 0.19091886 -0.17625126 0.17859709
		 -0.17625129 0.17859709 -0.34240371 0.19091886 -0.34240365 0.17859709 -0.16392949
		 0.012444675 -0.16392949 0.012444675 -0.17625129 0.00012284517 -0.17625126 0.00012284517
		 -0.34240365 0.012444675 -0.34240371 0.012444675 -0.35472542 0.17859709 -0.35472542
		 0.50019866 -0.095541596 0.48787689 -0.095541596 0.48837298 -0.16403258 0.50019866
		 -0.16378167 0.01232177 -0.095541596 0 -0.095541596 0 -0.16403258 0.012073696 -0.16378167
		 -2.9802322e-008 0.4995755 -0.012073785 0.49982643 -0.0123218 0.56806648 -2.9802322e-008
		 0.56806648 -0.48837301 0.4995755 -0.5001986 0.49982643 -0.5001986 0.56806648 -0.48787686
		 0.56806648 0.38358796 -0.36413521 0.37151456 -0.36413521 0.37126619 -0.30572015 0.38358796
		 -0.30572015 0.2048654 -0.36413521 0.192792 -0.36413521 0.192792 -0.30572015 0.20511377
		 -0.30572015 0.192792 -0.30576655 0.2048654 -0.30576655 0.20511377 -0.24735148 0.192792
		 -0.24735148 0.37151456 -0.30576655 0.38358796 -0.30576655 0.38358796 -0.24735148
		 0.37126619 -0.24735148 0.4996779 -0.42721117 0.00052076578 -0.42721117 0.00026035309
		 -0.49976221 0.50019866 -0.50002897 0.00026065111 -0.35445118 0.49941772 -0.35445118
		 0.49967813 -0.42700225 -0.00026017427 -0.42726901 0.36771834 -0.24800396 0.19279826
		 -0.24800396 0.19305897 -0.18589863 0.36745763 -0.18589863 0.00012916327 -0.037332833
		 0.17504925 -0.037332833 0.17478853 0.024772525 0.00038987398 0.024772525;
createNode polyMapSewMove -n "polyMapSewMove1";
	rename -uid "BD031AE5-445B-EC4C-FA3E-B88D136C5BA4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[61]";
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "BC439756-4A2C-E742-BAEB-D6AF19DDD6B1";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk[90:93]" -type "float2" -0.99921948 0.91453075 -0.50045884
		 0.43935341 -0.56926429 0.36661214 -1.069059491 0.8422665;
createNode polyMapSewMove -n "polyMapSewMove2";
	rename -uid "DCC3F395-43B6-B36B-BA8C-449848792914";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[1]";
createNode polyFlipUV -n "polyFlipUV1";
	rename -uid "A44BA7C7-42C9-34BE-E21F-359A887B22F2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "f[0]" "f[33]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0.73565021076298032 0 1;
	setAttr ".up" yes;
	setAttr ".pv" 0.74970206620000002;
createNode polyFlipUV -n "polyFlipUV2";
	rename -uid "0ED95813-4C0E-AF69-AB33-CEBF88055FF4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "f[2]" "f[4]" "f[28]" "f[32]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0.73565021076298032 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.75009933110000004;
	setAttr ".pv" 0.20221561190000001;
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "E312DCCA-440B-C0F2-2B1C-78A514EC9B5B";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk[86:89]" -type "float2" -0.49967819 0.51211345 -0.00091761351
		 0.98729074 0.06840837 0.91504532 -0.43087894 0.43885767;
createNode polyMapSewMove -n "polyMapSewMove3";
	rename -uid "E0A2A002-479D-7BDB-85AA-4AACC65E3E0C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[15]";
createNode polyTweakUV -n "polyTweakUV4";
	rename -uid "068E0B98-44C9-4BEB-6941-0C9CE376EB4E";
	setAttr ".uopa" yes;
	setAttr -s 8 ".uvtk[56:63]" -type "float2" -0.5001986 0.16380674 -0.5001986
		 0.16380674 -0.5001986 0.16380674 -0.5001986 0.16380674 -0.5001986 0.16380674 -0.5001986
		 0.16380674 -0.5001986 0.16380674 -0.5001986 0.16380674;
createNode polyMapSewMove -n "polyMapSewMove4";
	rename -uid "AFA7BAD3-49E6-D0DD-AF04-A19DCC1EC3F9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[7]";
createNode polyTweakUV -n "polyTweakUV5";
	rename -uid "8C8236D2-4019-AE1F-BB44-D591C0A29C9A";
	setAttr ".uopa" yes;
	setAttr -s 26 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" 0.24526602 0.25402552 ;
	setAttr ".uvtk[1]" -type "float2" 0.24526602 0.25402552 ;
	setAttr ".uvtk[3]" -type "float2" 0.24526602 0.25402558 ;
	setAttr ".uvtk[4]" -type "float2" 0.24526602 0.25402552 ;
	setAttr ".uvtk[5]" -type "float2" 0.24526604 0.25402552 ;
	setAttr ".uvtk[6]" -type "float2" 0.24526601 0.25402552 ;
	setAttr ".uvtk[7]" -type "float2" 0.24526601 0.25402552 ;
	setAttr ".uvtk[8]" -type "float2" 0.24526604 0.25402558 ;
	setAttr ".uvtk[9]" -type "float2" 0.24526601 0.25402558 ;
	setAttr ".uvtk[34]" -type "float2" 0.24526602 0.25402558 ;
	setAttr ".uvtk[35]" -type "float2" 0.24526604 0.25402558 ;
	setAttr ".uvtk[56]" -type "float2" 0.24526602 0.25402552 ;
	setAttr ".uvtk[57]" -type "float2" 0.24526602 0.25402552 ;
	setAttr ".uvtk[58]" -type "float2" 0.24526602 0.25402552 ;
	setAttr ".uvtk[59]" -type "float2" 0.24526601 0.25402552 ;
	setAttr ".uvtk[60]" -type "float2" 0.24526601 0.25402552 ;
	setAttr ".uvtk[61]" -type "float2" 0.24526604 0.25402552 ;
	setAttr ".uvtk[62]" -type "float2" 0.24526602 0.25402552 ;
	setAttr ".uvtk[63]" -type "float2" 0.24526602 0.25402552 ;
	setAttr ".uvtk[64]" -type "float2" 0.24526602 0.25402558 ;
	setAttr ".uvtk[65]" -type "float2" 0.24526604 0.25402552 ;
	setAttr ".uvtk[66]" -type "float2" 0.24526601 0.25402552 ;
	setAttr ".uvtk[67]" -type "float2" 0.24526601 0.25402558 ;
	setAttr ".uvtk[84]" -type "float2" 0.24526601 0.25402558 ;
	setAttr ".uvtk[85]" -type "float2" 0.24526601 0.25402552 ;
	setAttr ".uvtk[96]" -type "float2" 0.24526602 0.25402558 ;
createNode polyFlipUV -n "polyFlipUV3";
	rename -uid "7A63B60C-4143-E575-1B40-3A8E6DD5323B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "f[0]" "f[33]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0.73565021076298032 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.028503838;
	setAttr ".pv" 0.74982595439999999;
createNode polyTweakUV -n "polyTweakUV6";
	rename -uid "1FF230A7-4893-160F-3FED-14B8E0DB974B";
	setAttr ".uopa" yes;
	setAttr -s 6 ".uvtk";
	setAttr ".uvtk[2]" -type "float2" 0.18825835 0.25402555 ;
	setAttr ".uvtk[86]" -type "float2" 0.18825835 0.25402555 ;
	setAttr ".uvtk[87]" -type "float2" 0.18825835 0.25402555 ;
	setAttr ".uvtk[97]" -type "float2" 0.18825835 0.25402555 ;
	setAttr ".uvtk[98]" -type "float2" 0.18825835 0.25402555 ;
	setAttr ".uvtk[99]" -type "float2" 0.18825835 0.25402555 ;
createNode polyMapSewMove -n "polyMapSewMove5";
	rename -uid "81F34B8E-44B7-8447-D3BA-A88DA31D2426";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[3]";
createNode polyTweakUV -n "polyTweakUV7";
	rename -uid "59800C9E-4544-4136-308E-CC9B18D45619";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk[92:95]" -type "float2" -0.23390287 -0.21099268 -0.059121758
		 -0.37750942 -0.0002604723 -0.31520522 -0.17452061 -0.14918482;
createNode polyMapSewMove -n "polyMapSewMove6";
	rename -uid "A4C03BEF-4590-7134-AA56-6E96A4F29682";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[99]";
createNode polyTweakUV -n "polyTweakUV8";
	rename -uid "01D11B3F-4E5A-FE65-B666-80803B31F7E3";
	setAttr ".uopa" yes;
	setAttr -s 8 ".uvtk[76:83]" -type "float2" -0.19266909 0.132167 -0.19266909
		 0.132167 -0.19266909 0.132167 -0.19266909 0.132167 -0.19266909 0.132167 -0.19266909
		 0.132167 -0.19266909 0.132167 -0.19266909 0.132167;
createNode polyMapSewMove -n "polyMapSewMove7";
	rename -uid "46C9CDA3-4320-9B56-F8F7-D8B89FA64C83";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[109]";
createNode polyFlipUV -n "polyFlipUV4";
	rename -uid "B9648B75-457A-CF4C-0751-B4BC133864A3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "f[16]" "f[19]" "f[38:39]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0.73565021076298032 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.78818997739999996;
	setAttr ".pv" 0.23485568170000001;
createNode polyTweakUV -n "polyTweakUV9";
	rename -uid "3062E1B4-431C-560B-209A-95BB199D45D8";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk[86:89]" -type "float2" -0.13354737 -0.16683826 0.041233718
		 -0.00032152236 -0.018148482 0.061486334 -0.19240862 -0.10453409;
createNode polyMapSewMove -n "polyMapSewMove8";
	rename -uid "ADBE9FF4-46C1-B2E4-1507-0A9EEBD1B4C5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[105]";
createNode polyTweakUV -n "polyTweakUV10";
	rename -uid "136B21C5-423A-083B-D622-8E89319E2131";
	setAttr ".uopa" yes;
	setAttr -s 8 ".uvtk[68:75]" -type "float2" -0.19266903 -0.00010874867
		 -0.19266903 -0.00010874867 -0.19266903 -0.00010874867 -0.19266903 -0.00010874867
		 -0.19266903 -0.00010874867 -0.19266903 -0.00010874867 -0.19266903 -0.00010874867
		 -0.19266903 -0.00010874867;
createNode polyMapSewMove -n "polyMapSewMove9";
	rename -uid "26ADAAA8-45F2-29A7-D882-7CA247B42277";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[101]";
createNode polyMapDel -n "polyMapDel2";
	rename -uid "FBC21BC3-4964-32DF-741C-F289B4685EAF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyPlanarProj -n "polyPlanarProj4";
	rename -uid "69FB29F7-4977-9D15-A5C0-4DAB3172A204";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "f[0:1]" "f[3]" "f[5:7]" "f[9]" "f[11]" "f[13]" "f[15]" "f[18:25]";
	setAttr ".ix" -type "matrix" 0.83465161305484559 -0.00087791331994462247 -0.059271665800979798 0
		 -0.00053628962825183924 0.8365161324525201 -0.01994213664050706 0 0.059275741176766449 0.019930019783148449 0.83441380447177671 0
		 0 0.57069106643202316 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -5.9604644775390625e-008 0.57069100439548492 -4.4703483581542969e-008 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 0.89292740821838379 0.89589348435401917 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj5";
	rename -uid "11EDEBA4-4A44-1DBD-B83D-25B775C13882";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "f[2]" "f[4]" "f[8]" "f[10]" "f[16:17]";
	setAttr ".ix" -type "matrix" 0.83465161305484559 -0.00087791331994462247 -0.059271665800979798 0
		 -0.00053628962825183924 0.8365161324525201 -0.01994213664050706 0 0.059275741176766449 0.019930019783148449 0.83441380447177671 0
		 0 0.57069106643202316 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -7.4505805969238281e-008 0.57069103419780731 -4.4703483581542969e-008 ;
	setAttr ".ro" -type "double3" 0 90 0 ;
	setAttr ".ps" -type "double2" 0.89589348435401917 0.81398760429777117 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj6";
	rename -uid "B83B7FFC-4963-1F38-E366-0FB4B28725C1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "f[12]" "f[14]";
	setAttr ".ix" -type "matrix" 0.83465161305484559 -0.00087791331994462247 -0.059271665800979798 0
		 -0.00053628962825183924 0.8365161324525201 -0.01994213664050706 0 0.059275741176766449 0.019930019783148449 0.83441380447177671 0
		 0 0.57069106643202316 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 0.57069103419780731 -4.4703483581542969e-008 ;
	setAttr ".ps" -type "double2" 0.87868803739547729 0.92584830521384731 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV11";
	rename -uid "B2A38EF6-46B8-C5A0-9756-75A8B7342124";
	setAttr ".uopa" yes;
	setAttr -s 48 ".uvtk[0:47]" -type "float2" -0.035695162 0.48050809 -0.031550013
		 0.48070261 -9.9480152e-005 0.039444506 -0.0042445441 0.039250016 -0.035986688 0.48453698
		 -0.47854507 0.44916379 -0.4788366 0.45319271 -0.48268473 0.44877189 -0.4470945 0.0079057217
		 -0.45123422 0.0075138211 -0.031450678 -0.0075137154 -0.035590392 -0.0079055242 -0.0041398592
		 -0.44916362 -5.9604645e-008 -0.4487718 -0.035887267 -0.0036793947 -0.47873724 -0.03502363
		 -0.47844034 -0.039249875 -0.48258549 -0.039444372 -0.45113486 -0.48070246 -0.44698974
		 -0.48050797 -0.0038483324 -0.4531925 -0.44669828 -0.48453689 -0.003947753 0.035023808
		 -0.44679764 0.0036795139 0.51169461 -0.36442399 0.51552904 -0.36452764 0.51375318
		 -0.44651467 0.50991887 -0.44641095 0.44712478 -0.27535427 0.44270402 -0.27524099
		 0.44092822 -0.35722792 0.44534898 -0.35734117 0.068660736 -0.434811 0.064240038 -0.43469763
		 0.06601584 -0.35271075 0.070436537 -0.35282403 -0.0041642189 -0.34552425 -0.0003298521
		 -0.34562796 0.0014459491 -0.26364097 -0.0023884177 -0.26353729 -0.02358149 -0.51258075
		 -0.48130101 -0.47805002 -0.48670906 -0.55137265 -0.028989583 -0.58590347 0.4982293
		 -0.17051062 0.048202813 -0.17006135 0.048154116 -0.097980112 0.49818048 -0.098429322;
createNode polyMapSewMove -n "polyMapSewMove10";
	rename -uid "EB0F4AFF-4208-191E-D25D-83AEF690F68C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[25]";
createNode polyTweakUV -n "polyTweakUV12";
	rename -uid "DA9FDE3B-4800-49C8-0E23-389726AA084C";
	setAttr ".uopa" yes;
	setAttr -s 8 ".uvtk";
	setAttr ".uvtk[28]" -type "float2" 0.09343487 -0.1111512 ;
	setAttr ".uvtk[29]" -type "float2" 0.088355005 -0.10629115 ;
	setAttr ".uvtk[30]" -type "float2" -0.00139606 -0.2008484 ;
	setAttr ".uvtk[31]" -type "float2" 0.0036838055 -0.20570831 ;
	setAttr ".uvtk[36]" -type "float2" -0.51252419 0.28882211 ;
	setAttr ".uvtk[37]" -type "float2" -0.50812405 0.28460032 ;
	setAttr ".uvtk[38]" -type "float2" -0.41837278 0.37915751 ;
	setAttr ".uvtk[39]" -type "float2" -0.42277285 0.38337916 ;
createNode polyMapSewMove -n "polyMapSewMove11";
	rename -uid "DFA8CE69-4832-894C-6306-EC8A6FFDA7D0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[29]";
createNode polyFlipUV -n "polyFlipUV5";
	rename -uid "D4FFDB35-4A77-E583-DFC8-6D95483A0716";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "f[2]" "f[8]" "f[14]" "f[17]";
	setAttr ".ix" -type "matrix" 0.83465161305484559 -0.00087791331994462247 -0.059271665800979798 0
		 -0.00053628962825183924 0.8365161324525201 -0.01994213664050706 0 0.059275741176766449 0.019930019783148449 0.83441380447177671 0
		 0 0.57069106643202316 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.74876250330000005;
	setAttr ".pv" 0.22225302459999999;
createNode polyTweakUV -n "polyTweakUV13";
	rename -uid "62F150C3-49F6-CBC7-E4DE-A9AD8DDAE11D";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk[40:43]" -type "float2" -0.99517047 0.087996364 -0.038036883
		 0.12207806 -0.043239027 0.2753922 -1.00037240982 0.2413106;
createNode polyMapSewMove -n "polyMapSewMove12";
	rename -uid "B6338A04-456E-3DBE-5268-F9B3D8D2059E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[39]";
createNode polyTweakUV -n "polyTweakUV14";
	rename -uid "B193D87F-482F-ED11-70C5-01B388AD7A65";
	setAttr ".uopa" yes;
	setAttr -s 8 ".uvtk";
	setAttr ".uvtk[24]" -type "float2" -1.0320773 -0.064942166 ;
	setAttr ".uvtk[25]" -type "float2" -1.0358914 -0.069163278 ;
	setAttr ".uvtk[26]" -type "float2" -0.94608223 -0.15119383 ;
	setAttr ".uvtk[27]" -type "float2" -0.94226813 -0.14697254 ;
	setAttr ".uvtk[31]" -type "float2" -0.50296819 0.33847612 ;
	setAttr ".uvtk[32]" -type "float2" -0.498564 0.34333658 ;
	setAttr ".uvtk[33]" -type "float2" -0.58837306 0.42536691 ;
	setAttr ".uvtk[34]" -type "float2" -0.59277731 0.42050648 ;
createNode polyMapSewMove -n "polyMapSewMove13";
	rename -uid "E03BA30F-4720-390B-504A-A4A14524B911";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[23]";
createNode polyTweakUV -n "polyTweakUV15";
	rename -uid "736E9963-4344-5611-FA25-72969E9AAAAE";
	setAttr ".uopa" yes;
	setAttr -s 40 ".uvtk[0:39]" -type "float2" 0.70403492 0.11452776 0.70657909
		 0.11464718 0.72588241 -0.15618241 0.72333831 -0.15630174 0.70385599 0.11700058 0.43222845
		 0.095289707 0.43204951 0.097762465 0.42968762 0.095049202 0.45153171 -0.17553985
		 0.44899088 -0.17578036 0.31913662 0.74481308 0.31659579 0.74457264 0.33589906 0.47374308
		 0.33843994 0.47398356 0.31641358 0.74716645 0.044607043 0.7279284 0.044789195 0.72533453
		 0.04224503 0.72521514 0.061548412 0.45438558 0.064092547 0.45450491 0.33607802 0.47127026
		 0.06427139 0.45203209 0.72352046 -0.15889573 0.45171395 -0.17813373 0.36946797 0.7481665
		 0.36929843 0.75051975 0.31896704 0.7471664 -0.0082482696 0.72410929 -0.008053124
		 0.72139615 0.042049766 0.72792822 0.33863157 0.47127017 0.38896286 0.47462359 0.38877127
		 0.47733697 0.061714321 0.45203203 0.011250198 0.45056665 0.011416137 0.4482131 0.31325921
		 0.79069686 0.041452765 0.77145869 0.067279905 0.40849143 0.33908641 0.42772949;
createNode groupId -n "groupId1";
	rename -uid "B85D15C8-4959-BDDE-01DF-E9A3D83C3C45";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "B1C600B6-4DDE-A44A-2260-E59B1A446217";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:25]";
createNode polyTweakUV -n "polyTweakUV16";
	rename -uid "2B25B6A9-4113-B940-127B-D7A9280BC072";
	setAttr ".uopa" yes;
	setAttr -s 38 ".uvtk[0:37]" -type "float2" 0.21946651 -0.019912243 0.22584176
		 -0.019912243 0.22584176 0.09602356 0.21946651 0.48436731 0.21946651 0.47883785 0.09832412
		 -0.019912243 0.10469937 -0.019912243 0.09832412 0.09602356 0.092533231 0.47883788
		 0.22584176 0.35105875 0.22584176 0.23512298 0.21946651 0.36237907 0.10469937 0.36237907
		 0.09832412 0.35105875 0.22584176 0.22380263 0.22584176 0.10734385 0.21946651 0.23512298
		 0.10469937 0.23512298 0.09832412 0.22380269 0.09832412 0.10734391 0.21946651 0.10734391
		 0.10469937 0.10734391 0.10469931 0.09602356 0.10469937 0.47883788 0.21946651 0.35105875
		 0.10469931 0.35105875 0.21946651 0.22380263 0.10469937 0.22380263 0.21946651 0.09602356
		 -0.023402572 0.47883788 -0.023402572 0.36237907 0.34756848 0.47883785 0.23163271
		 0.47883785 0.23163265 0.36237907 0.34756848 0.36237907 0.10469937 0.48436731 0.092533231
		 0.36237907 0.09832412 0.23512298;
createNode polyTweakUV -n "polyTweakUV17";
	rename -uid "A00E73D5-4952-45C7-7DA3-85BC52B82F0A";
	setAttr ".uopa" yes;
	setAttr -s 90 ".uvtk[0:89]" -type "float2" -0.21758425 -0.24224973 -0.21130428
		 -0.24224973 -0.21130428 -0.48462367 -0.21758425 -0.48462367 -0.21758425 -0.23596972
		 -0.45995823 -0.24224973 -0.4599582 -0.23596972 -0.46623817 -0.24224973 -0.45995823
		 -0.48462367 -0.4662382 -0.48462367 0.72628158 0.1041434 0.71976763 0.11065736 0.65394968
		 0.044727087 0.66035134 0.038325399 0.7327956 0.11101778 0.72628158 -0.1247347 0.7327956
		 -0.13173547 0.72676843 0.11717132 0.48401523 0.11717132 0.49088958 0.11065736 0.48437563
		 0.1041434 0.55030584 0.038325399 0.55670756 0.044727087 0.47786164 0.11114419 0.47786164
		 -0.13160905 0.48437563 -0.1247347 0.71976763 -0.13124865 0.66035134 -0.058916658
		 0.65394968 -0.065318376 0.72664201 -0.13776261 0.49088958 -0.13124865 0.48388875
		 -0.13776261 0.55670756 -0.065318376 0.55030584 -0.058916658 -0.21758425 -0.49090362
		 -0.45995823 -0.49090362 0.65394968 0.032172024 0.65394968 -0.052763283 0.64779627
		 0.038325399 0.56286097 0.038325399 0.55670756 0.032172024 0.55670756 -0.052763283
		 0.56286097 -0.058916658 0.64779627 -0.058916658 0.34452039 0.23900509 0.33824044
		 0.23900506 0.33824044 0.15432295 0.34452039 0.15432298 0.33824044 0.24528506 0.25355834
		 0.24528506 0.25355828 0.23900506 0.24727833 0.23900509 0.24727833 0.15432298 0.25355828
		 0.15432295 0.25355828 0.14804298 0.33824044 0.14804298 -0.21130428 -0.23596972 -0.21733136
		 -0.20106223 -0.21130428 -0.20119014 -0.46623817 -0.23596972 -0.46623817 -0.20106223
		 -0.46008462 -0.20119014 -0.21130426 -0.52581114 -0.2174578 -0.52568316 -0.21130426
		 -0.49090362 -0.4602111 -0.52581114 -0.4662382 -0.52568316 -0.4662382 -0.49090362
		 0.34452045 0.27505723 0.33836704 0.27505723 0.34452045 0.24528506 0.25343174 0.27505723
		 0.24727833 0.27505723 0.24727833 0.24528506 0.24727833 0.11827087 0.25343174 0.11827087
		 0.24727833 0.14804298 0.33836704 0.11827087 0.34452039 0.11827087 0.34452039 0.14804298
		 -0.50146663 -0.48475015 -0.50159609 -0.24199682 -0.17607594 -0.24212331 -0.17594635
		 -0.48487651 0.21712208 0.23913166 0.21712208 0.15419638 0.3746767 0.15419641 0.37467667
		 0.23913167 -0.21130428 -0.48462367 -0.21130428 -0.24224973;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "8B9EF0C6-4CD0-09FE-1BE8-6399F164F3A9";
	setAttr ".pee" yes;
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -73.809520876596963 -286.90475050419138 ;
	setAttr ".tgi[0].vh" -type "double2" 72.619044733426037 298.80951193590062 ;
createNode polyUnite -n "polyUnite1";
	rename -uid "02BD0197-4017-6C20-7516-1385CFB8A22E";
	setAttr -s 6 ".ip";
	setAttr -s 6 ".im";
createNode groupId -n "groupId2";
	rename -uid "C8410E44-4765-3E98-91FD-E0A6B745504E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "BA4D11BC-45DF-9ABD-F0A4-C7B79A8C95F5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:25]";
createNode groupId -n "groupId3";
	rename -uid "4BEEC334-4C93-F525-494B-A6BE7D1FAEB0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId4";
	rename -uid "BF517078-437E-82F6-A993-C480B623851E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "00CDDFF2-4F13-4415-9D6F-B6A82C72BBC6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:57]";
createNode groupId -n "groupId5";
	rename -uid "760C9772-4B80-EE9F-F6C7-DFAE64DD9EE2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6";
	rename -uid "E7D6B082-4828-547B-1579-32A5AA1AF983";
	setAttr ".ihi" 0;
createNode groupId -n "groupId7";
	rename -uid "A0907349-4AA4-9392-5E2F-B6BB6422C2BB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId8";
	rename -uid "9B3ACAC4-4E7F-7BEB-B14C-7F94966FB105";
	setAttr ".ihi" 0;
createNode groupId -n "groupId9";
	rename -uid "568D335D-47AE-D2E5-3EA8-D095662E2F59";
	setAttr ".ihi" 0;
createNode groupId -n "groupId10";
	rename -uid "26546901-4C92-6A47-554D-F0A794C174AC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId11";
	rename -uid "B45ECF10-4CE4-0510-C3D6-86A74E1EDCBD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId12";
	rename -uid "A01CC3BB-4462-7D7D-EFB5-8B9BA89B5487";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "572DB8E5-4C11-C4F4-D51E-70A8BEC4CF45";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:25]";
createNode groupId -n "groupId13";
	rename -uid "8CA9D18C-4E52-A2FB-C94D-6D9771934AD7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId14";
	rename -uid "A1957028-41E5-65A3-9CD8-4EA78381CB10";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "C83E8873-4CC9-14D4-2CA6-9F8C7DCF1963";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:25]";
createNode groupId -n "groupId15";
	rename -uid "2637F332-4769-A69E-811C-F697EC2A218B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "41D45A74-4E56-46AC-8014-01A6C93022ED";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:187]";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 13 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 13 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "groupParts3.og" "pCubeShape1.i";
connectAttr "polyTweakUV17.uvtk[0]" "pCubeShape1.uvst[0].uvtw";
connectAttr "groupId4.id" "pCubeShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape1.iog.og[0].gco";
connectAttr "groupId5.id" "pCubeShape1.ciog.cog[0].cgid";
connectAttr "groupId1.id" "pCubeShape2.iog.og[0].gid";
connectAttr "groupId2.id" "pCubeShape2.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape2.iog.og[1].gco";
connectAttr "groupParts2.og" "pCubeShape2.i";
connectAttr "polyTweakUV15.uvtk[0]" "pCubeShape2.uvst[0].uvtw";
connectAttr "groupId3.id" "pCubeShape2.ciog.cog[0].cgid";
connectAttr "groupParts4.og" "pCubeShape5.i";
connectAttr "polyTweakUV16.uvtk[0]" "pCubeShape5.uvst[0].uvtw";
connectAttr "groupId12.id" "pCubeShape5.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape5.iog.og[0].gco";
connectAttr "groupId13.id" "pCubeShape5.ciog.cog[0].cgid";
connectAttr "groupId10.id" "pCubeShape6.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape6.iog.og[0].gco";
connectAttr "groupId11.id" "pCubeShape6.ciog.cog[0].cgid";
connectAttr "groupId8.id" "pCubeShape7.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape7.iog.og[0].gco";
connectAttr "groupId9.id" "pCubeShape7.ciog.cog[0].cgid";
connectAttr "groupId6.id" "pCubeShape8.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape8.iog.og[0].gco";
connectAttr "groupId7.id" "pCubeShape8.ciog.cog[0].cgid";
connectAttr "groupParts6.og" "pCube9Shape.i";
connectAttr "groupId14.id" "pCube9Shape.iog.og[0].gid";
connectAttr "groupId15.id" "pCube9Shape.iog.og[1].gid";
connectAttr ":initialShadingGroup.mwc" "pCube9Shape.iog.og[1].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "|pCube5|polySurfaceShape1.o" "polyBevel1.ip";
connectAttr "pCubeShape5.wm" "polyBevel1.mp";
connectAttr "polyTweak1.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyCube1.out" "polyTweak1.ip";
connectAttr "polyTweak2.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak2.ip";
connectAttr "polySurfaceShape2.o" "polyBevel2.ip";
connectAttr "pCubeShape2.wm" "polyBevel2.mp";
connectAttr "polyExtrudeFace2.out" "polyBevel3.ip";
connectAttr "pCubeShape1.wm" "polyBevel3.mp";
connectAttr "polyBevel3.out" "polyMapDel1.ip";
connectAttr "polyMapDel1.out" "polyPlanarProj1.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj1.mp";
connectAttr "polyPlanarProj1.out" "polyPlanarProj2.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj2.mp";
connectAttr "polyPlanarProj2.out" "polyPlanarProj3.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj3.mp";
connectAttr "polyPlanarProj3.out" "polyTweakUV1.ip";
connectAttr "polyTweakUV1.out" "polyMapSewMove1.ip";
connectAttr "polyMapSewMove1.out" "polyTweakUV2.ip";
connectAttr "polyTweakUV2.out" "polyMapSewMove2.ip";
connectAttr "polyMapSewMove2.out" "polyFlipUV1.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV1.mp";
connectAttr "polyFlipUV1.out" "polyFlipUV2.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV2.mp";
connectAttr "polyFlipUV2.out" "polyTweakUV3.ip";
connectAttr "polyTweakUV3.out" "polyMapSewMove3.ip";
connectAttr "polyMapSewMove3.out" "polyTweakUV4.ip";
connectAttr "polyTweakUV4.out" "polyMapSewMove4.ip";
connectAttr "polyMapSewMove4.out" "polyTweakUV5.ip";
connectAttr "polyTweakUV5.out" "polyFlipUV3.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV3.mp";
connectAttr "polyFlipUV3.out" "polyTweakUV6.ip";
connectAttr "polyTweakUV6.out" "polyMapSewMove5.ip";
connectAttr "polyMapSewMove5.out" "polyTweakUV7.ip";
connectAttr "polyTweakUV7.out" "polyMapSewMove6.ip";
connectAttr "polyMapSewMove6.out" "polyTweakUV8.ip";
connectAttr "polyTweakUV8.out" "polyMapSewMove7.ip";
connectAttr "polyMapSewMove7.out" "polyFlipUV4.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV4.mp";
connectAttr "polyFlipUV4.out" "polyTweakUV9.ip";
connectAttr "polyTweakUV9.out" "polyMapSewMove8.ip";
connectAttr "polyMapSewMove8.out" "polyTweakUV10.ip";
connectAttr "polyTweakUV10.out" "polyMapSewMove9.ip";
connectAttr "polyBevel2.out" "polyMapDel2.ip";
connectAttr "polyMapDel2.out" "polyPlanarProj4.ip";
connectAttr "pCubeShape2.wm" "polyPlanarProj4.mp";
connectAttr "polyPlanarProj4.out" "polyPlanarProj5.ip";
connectAttr "pCubeShape2.wm" "polyPlanarProj5.mp";
connectAttr "polyPlanarProj5.out" "polyPlanarProj6.ip";
connectAttr "pCubeShape2.wm" "polyPlanarProj6.mp";
connectAttr "polyPlanarProj6.out" "polyTweakUV11.ip";
connectAttr "polyTweakUV11.out" "polyMapSewMove10.ip";
connectAttr "polyMapSewMove10.out" "polyTweakUV12.ip";
connectAttr "polyTweakUV12.out" "polyMapSewMove11.ip";
connectAttr "polyMapSewMove11.out" "polyFlipUV5.ip";
connectAttr "pCubeShape2.wm" "polyFlipUV5.mp";
connectAttr "polyFlipUV5.out" "polyTweakUV13.ip";
connectAttr "polyTweakUV13.out" "polyMapSewMove12.ip";
connectAttr "polyMapSewMove12.out" "polyTweakUV14.ip";
connectAttr "polyTweakUV14.out" "polyMapSewMove13.ip";
connectAttr "polyMapSewMove13.out" "polyTweakUV15.ip";
connectAttr "polyTweakUV15.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "polyBevel1.out" "polyTweakUV16.ip";
connectAttr "polyMapSewMove9.out" "polyTweakUV17.ip";
connectAttr "pCubeShape2.o" "polyUnite1.ip[0]";
connectAttr "pCubeShape1.o" "polyUnite1.ip[1]";
connectAttr "pCubeShape8.o" "polyUnite1.ip[2]";
connectAttr "pCubeShape7.o" "polyUnite1.ip[3]";
connectAttr "pCubeShape6.o" "polyUnite1.ip[4]";
connectAttr "pCubeShape5.o" "polyUnite1.ip[5]";
connectAttr "pCubeShape2.wm" "polyUnite1.im[0]";
connectAttr "pCubeShape1.wm" "polyUnite1.im[1]";
connectAttr "pCubeShape8.wm" "polyUnite1.im[2]";
connectAttr "pCubeShape7.wm" "polyUnite1.im[3]";
connectAttr "pCubeShape6.wm" "polyUnite1.im[4]";
connectAttr "pCubeShape5.wm" "polyUnite1.im[5]";
connectAttr "groupParts1.og" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "polyTweakUV17.out" "groupParts3.ig";
connectAttr "groupId4.id" "groupParts3.gi";
connectAttr "polyTweakUV16.out" "groupParts4.ig";
connectAttr "groupId12.id" "groupParts4.gi";
connectAttr "polyUnite1.out" "groupParts5.ig";
connectAttr "groupId14.id" "groupParts5.gi";
connectAttr "groupParts5.og" "groupParts6.ig";
connectAttr "groupId15.id" "groupParts6.gi";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape2.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape8.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape8.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape7.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape7.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape6.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape6.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape5.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape5.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCube9Shape.iog.og[1]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId2.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId3.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId4.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId5.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId6.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId7.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId8.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId9.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId10.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId11.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId12.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId13.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId15.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId1.msg" ":defaultLastHiddenSet.gn" -na;
connectAttr "groupId14.msg" ":defaultLastHiddenSet.gn" -na;
connectAttr "pCubeShape2.iog.og[0]" ":defaultLastHiddenSet.dsm" -na;
connectAttr "pCube9Shape.iog.og[0]" ":defaultLastHiddenSet.dsm" -na;
// End of entranceFirePit_model_002.ma
