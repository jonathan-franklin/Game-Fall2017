//Maya ASCII 2017ff05 scene
//Name: wetlandThree_model_003.ma
//Last modified: Mon, Sep 25, 2017 01:58:45 PM
//Codeset: 1252
requires maya "2017ff05";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2017";
fileInfo "version" "2017";
fileInfo "cutIdentifier" "201706020738-1017329";
fileInfo "osv" "Microsoft Windows 8 Enterprise Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "education";
createNode transform -s -n "persp";
	rename -uid "2E4A49DF-4275-368D-2AA3-A6AFC4D0CF37";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -15.203030457053233 41.531766043864046 32.146545522353442 ;
	setAttr ".r" -type "double3" -50.138352726069428 -1466.199999999742 -1.7723739275961773e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "D166449D-47C5-B3EC-461E-9281E30BEE95";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 62.364201867252518;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "3DEF197B-4CFD-58CB-4977-7A939FD3C31D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -2.1857917363219279 1000.2415998664521 -17.731393937930417 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
	setAttr ".rpt" -type "double3" 0 3.5032461608120427e-046 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "34D82AEF-45A8-6EB7-361B-B98E9CF9D410";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1007.7819566980151;
	setAttr ".ow" 77.29652291213705;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" -16.893298388414312 -7.5403568315631606 0 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "9E7D0114-442E-0E4B-B05D-25BDD8C71C47";
	setAttr ".t" -type "double3" -15.020601089068592 -5.1186407634279814 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "73ED0A1B-4C72-C516-5ABE-7B9254693DD9";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 22.380877542977576;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "36A679BB-4CC6-B7BB-A436-A9B3E2C5B461";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 -7.4335083517111737 -10.6122771699082 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "234790D1-4CC0-258B-27F2-7AB889DC1B29";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 39.101379010450891;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pCube1";
	rename -uid "038CD857-48C3-FEED-0E55-AA8823590CB4";
	setAttr ".t" -type "double3" -16.893298388414312 -7.5403568315631606 0 ;
	setAttr ".s" -type "double3" 1 9.7311524693841456 1 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "B2CD1D9D-4997-5DCD-9EF1-58B5AD84D001";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.31552261114120483 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "24C7F7B1-489A-42B7-70F5-73860170C14F";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "D59C9E05-4ED3-29A9-5F83-03949414357D";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "22751EBF-44A6-C402-7042-4C912CD40B4D";
createNode displayLayerManager -n "layerManager";
	rename -uid "64A1553A-4742-4122-1826-899B8AC75D6D";
createNode displayLayer -n "defaultLayer";
	rename -uid "2E809B99-402F-8B93-5F54-E3A19EB360D4";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "16C1C333-446C-D8D6-7FF0-3F8D9861475F";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "9EF38CDE-4083-8F99-E5C8-3DB920619D77";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "C1A73289-4C57-2FB6-AB84-C9A12019044A";
	setAttr ".cuv" 4;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "D6427121-4E82-C0DE-69F0-0AB25ED85720";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n"
		+ "            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 778\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 778\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n"
		+ "            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n"
		+ "            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 778\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 778\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n"
		+ "            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n"
		+ "            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n"
		+ "            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n"
		+ "                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n"
		+ "                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n"
		+ "                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n"
		+ "                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n"
		+ "                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n"
		+ "                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n"
		+ "\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 778\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "711C7206-420A-E30C-A5AF-7EB47606152D";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "D199B074-47F4-E519-79D6-909F1B97D036";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -16.393299 -7.5403566 0 ;
	setAttr ".rs" 61940;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -16.393298388414312 -12.405933066255233 -0.5 ;
	setAttr ".cbx" -type "double3" -16.393298388414312 -2.6747805968710878 0.5 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "353FBA5E-41C9-FA62-FB9C-10ADA706BB75";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -15.16249 -7.5403571 0 ;
	setAttr ".rs" 60015;
	setAttr ".lt" -type "double3" -5.6025362052208483e-015 3.0899773642991175e-015 25.231579966162325 ;
	setAttr ".d" 16;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -15.162489415101934 -12.40593364627712 -0.5 ;
	setAttr ".cbx" -type "double3" -15.162489415101934 -2.6747805968710878 0.5 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "6B725159-4D3D-9981-B919-13A38C1DED39";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[8:11]" -type "float3"  1.23080897 0 0 1.23080897
		 0 0 1.23080897 0 0 1.23080897 0 0;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "14BA31CF-4D92-5FEF-BEA1-BDA3B7431CFC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[34]" "e[50]" "e[82]" "e[114]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".wt" 0.45700722932815552;
	setAttr ".re" 114;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak2";
	rename -uid "81E6C42E-4980-EDFF-5229-1B80E3B140B1";
	setAttr ".uopa" yes;
	setAttr -s 36 ".tk";
	setAttr ".tk[3]" -type "float3" 0 0.014572343 0 ;
	setAttr ".tk[5]" -type "float3" 0 0.014572343 0 ;
	setAttr ".tk[10]" -type "float3" 0 -0.072861739 0 ;
	setAttr ".tk[11]" -type "float3" 0 -0.072861739 0 ;
	setAttr ".tk[44]" -type "float3" 0 -0.31816262 0 ;
	setAttr ".tk[45]" -type "float3" 0 -0.40802544 0 ;
	setAttr ".tk[46]" -type "float3" 0 -0.50274575 0 ;
	setAttr ".tk[47]" -type "float3" 0 -0.57803619 0 ;
	setAttr ".tk[48]" -type "float3" 0 -0.60475194 0 ;
	setAttr ".tk[49]" -type "float3" 0 -0.61203808 0 ;
	setAttr ".tk[50]" -type "float3" 0 -0.61446691 0 ;
	setAttr ".tk[51]" -type "float3" 0 -0.59989482 0 ;
	setAttr ".tk[52]" -type "float3" 0 -0.58532238 0 ;
	setAttr ".tk[53]" -type "float3" 0 -0.56103486 0 ;
	setAttr ".tk[54]" -type "float3" 0 -0.55374885 0 ;
	setAttr ".tk[55]" -type "float3" 0 -0.544034 0 ;
	setAttr ".tk[56]" -type "float3" 0 -0.49303058 0 ;
	setAttr ".tk[57]" -type "float3" 0 -0.1821543 0 ;
	setAttr ".tk[58]" -type "float3" 0 0.038859583 0 ;
	setAttr ".tk[59]" -type "float3" 0 0.070432998 0 ;
	setAttr ".tk[60]" -type "float3" 0 -0.31816262 0 ;
	setAttr ".tk[61]" -type "float3" 0 -0.40802544 0 ;
	setAttr ".tk[62]" -type "float3" 0 -0.50274575 0 ;
	setAttr ".tk[63]" -type "float3" 0 -0.57803619 0 ;
	setAttr ".tk[64]" -type "float3" 0 -0.60475194 0 ;
	setAttr ".tk[65]" -type "float3" 0 -0.61203808 0 ;
	setAttr ".tk[66]" -type "float3" 0 -0.61446691 0 ;
	setAttr ".tk[67]" -type "float3" 0 -0.59989482 0 ;
	setAttr ".tk[68]" -type "float3" 0 -0.58532238 0 ;
	setAttr ".tk[69]" -type "float3" 0 -0.56103486 0 ;
	setAttr ".tk[70]" -type "float3" 0 -0.55374885 0 ;
	setAttr ".tk[71]" -type "float3" 0 -0.544034 0 ;
	setAttr ".tk[72]" -type "float3" 0 -0.49303058 0 ;
	setAttr ".tk[73]" -type "float3" 0 -0.1821543 0 ;
	setAttr ".tk[74]" -type "float3" 0 0.038859583 0 ;
	setAttr ".tk[75]" -type "float3" 0 0.070432998 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "C43C7B8D-4281-7C62-69DB-F9A48B78B217";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 10.069097 -7.19766 0 ;
	setAttr ".rs" 63000;
	setAttr ".lt" -type "double3" -2.0830146713125263e-015 -6.2750782743657796e-016 
		9.3810640975303734 ;
	setAttr ".d" 4;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 10.069096325941157 -12.40593364627712 -0.5 ;
	setAttr ".cbx" -type "double3" 10.069096325941157 -1.9893861346136337 0.5 ;
createNode polyTweak -n "polyTweak3";
	rename -uid "DC08CA9D-45B1-81BF-E745-10BCB88878AF";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[76:77]" -type "float3"  0 0.070433006 0 0 0.070433006
		 0;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "3D0903C4-4FBE-2E86-6520-1DAE30BFAD0C";
	setAttr ".ics" -type "componentList" 1 "f[5]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -17.393299 -7.5403571 0 ;
	setAttr ".rs" 60029;
	setAttr ".lt" -type "double3" 0 0 1.5387900215012316 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -17.393298388414312 -12.40593364627712 -0.5 ;
	setAttr ".cbx" -type "double3" -17.393298388414312 -2.6747805968710878 0.5 ;
createNode polyTweak -n "polyTweak4";
	rename -uid "B15C0776-40A1-0CE0-8563-9DAF702DDD67";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[88]" -type "float3" -0.025854222 0.0053137019 0 ;
	setAttr ".tk[89]" -type "float3" 0 -0.013284255 0 ;
	setAttr ".tk[90]" -type "float3" 0 0.010627404 0 ;
	setAttr ".tk[92]" -type "float3" -0.025854222 0.0053137019 0 ;
	setAttr ".tk[93]" -type "float3" 0 -0.013284255 0 ;
	setAttr ".tk[94]" -type "float3" 0 0.010627404 0 ;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "A2DE699C-4C88-EB1A-68ED-9FBE45A2DDC7";
	setAttr ".dc" -type "componentList" 6 "f[2]" "f[7]" "f[26:41]" "f[75]" "f[82:85]" "f[97]";
createNode deleteComponent -n "deleteComponent2";
	rename -uid "87259EC2-4F60-49E1-9DA4-CEAEF62894C0";
	setAttr ".dc" -type "componentList" 6 "f[2]" "f[5]" "f[8:23]" "f[57]" "f[59:62]" "f[71]";
createNode polyExtrudeEdge -n "polyExtrudeEdge1";
	rename -uid "58682BBE-4A19-733F-79E1-4890293F63D0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[2]" "e[8]" "e[28:43]" "e[93]" "e[102:105]" "e[123]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.25903583 -5.2701101 -0.49999976 ;
	setAttr ".rs" 49851;
	setAttr ".lt" -type "double3" -1.022363548882881e-016 2.8740418785744697 -4.3969032018496988e-016 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -18.932090044908453 -8.6542511625738445 -0.5 ;
	setAttr ".cbx" -type "double3" 19.450161694593501 -1.8859688123273415 -0.4999995231628418 ;
createNode polyExtrudeEdge -n "polyExtrudeEdge2";
	rename -uid "A9AFCE0D-497C-0FB5-4398-AF8977D6C71F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 23 "e[128]" "e[130]" "e[132]" "e[134]" "e[136]" "e[138]" "e[140]" "e[142]" "e[144]" "e[146]" "e[148]" "e[150]" "e[152]" "e[154]" "e[156]" "e[158]" "e[160]" "e[163:164]" "e[166]" "e[168]" "e[170]" "e[172]" "e[174]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.25903583 -5.2701097 -3.374042 ;
	setAttr ".rs" 64444;
	setAttr ".lt" -type "double3" -3.5798735394086112e-015 4.4609001524075769 5.9119807877781491e-017 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -18.932090044908453 -8.6542511625738445 -3.3740420341491699 ;
	setAttr ".cbx" -type "double3" 19.450161694593501 -1.8859682323054558 -3.3740420341491699 ;
createNode polyExtrudeEdge -n "polyExtrudeEdge3";
	rename -uid "7354D307-427F-BF73-53DE-D9B3EF235DF6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 23 "e[177]" "e[179]" "e[181]" "e[183]" "e[185]" "e[187]" "e[189]" "e[191]" "e[193]" "e[195]" "e[197]" "e[199]" "e[201]" "e[203]" "e[205]" "e[207]" "e[209]" "e[212:213]" "e[215]" "e[217]" "e[219]" "e[221]" "e[223]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.25903583 -5.2701097 -7.8349423 ;
	setAttr ".rs" 64979;
	setAttr ".lt" -type "double3" -3.5798735394086112e-015 6.3001231666884872 5.9119807877781491e-017 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -18.932090044908453 -8.6542511625738445 -7.8349428176879883 ;
	setAttr ".cbx" -type "double3" 19.450161694593501 -1.8859682323054558 -7.8349423408508301 ;
createNode polyExtrudeEdge -n "polyExtrudeEdge4";
	rename -uid "CFC3F9BF-4EBA-5533-193B-3E88820CABE0";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 23 "e[226]" "e[228]" "e[230]" "e[232]" "e[234]" "e[236]" "e[238]" "e[240]" "e[242]" "e[244]" "e[246]" "e[248]" "e[250]" "e[252]" "e[254]" "e[256]" "e[258]" "e[261:262]" "e[264]" "e[266]" "e[268]" "e[270]" "e[272]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.25903583 -5.2701097 -14.135067 ;
	setAttr ".rs" 57998;
	setAttr ".lt" -type "double3" -3.5798735393732276e-015 7.997667185508277 -1.7167190405077304e-015 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -18.932090044908453 -8.6542511625738445 -14.135066986083984 ;
	setAttr ".cbx" -type "double3" 19.450161694593501 -1.8859682323054558 -14.135066032409668 ;
createNode polyMapDel -n "polyMapDel1";
	rename -uid "DB28560C-4413-2B98-BF95-27BEA5010485";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyTweak -n "polyTweak5";
	rename -uid "2C99EE40-48B5-5CCE-C7B3-BAA1B372DEE1";
	setAttr ".uopa" yes;
	setAttr -s 142 ".tk";
	setAttr ".tk[2]" -type "float3" 2.0116568e-007 0 0 ;
	setAttr ".tk[3]" -type "float3" -2.6077032e-007 0 0 ;
	setAttr ".tk[4]" -type "float3" -1.1399388e-006 0 4.7683716e-007 ;
	setAttr ".tk[5]" -type "float3" 5.8114529e-007 0 4.7683716e-007 ;
	setAttr ".tk[7]" -type "float3" 8.7916851e-007 0 4.7683716e-007 ;
	setAttr ".tk[8]" -type "float3" -5.4761767e-007 0 0 ;
	setAttr ".tk[25]" -type "float3" -1.4901161e-007 0 4.7683716e-007 ;
	setAttr ".tk[26]" -type "float3" -1.2293458e-006 0 4.7683716e-007 ;
	setAttr ".tk[27]" -type "float3" -3.5972334e-008 0 4.7683716e-007 ;
	setAttr ".tk[28]" -type "float3" 0 0 4.7683716e-007 ;
	setAttr ".tk[29]" -type "float3" 0 0 4.7683716e-007 ;
	setAttr ".tk[30]" -type "float3" 0 0 4.7683716e-007 ;
	setAttr ".tk[31]" -type "float3" 0 0 4.7683716e-007 ;
	setAttr ".tk[32]" -type "float3" 0 0 4.7683716e-007 ;
	setAttr ".tk[33]" -type "float3" 0 0 4.7683716e-007 ;
	setAttr ".tk[34]" -type "float3" 0 0 4.7683716e-007 ;
	setAttr ".tk[35]" -type "float3" -1.9837171e-007 0 4.7683716e-007 ;
	setAttr ".tk[36]" -type "float3" -2.9057264e-007 0 4.7683716e-007 ;
	setAttr ".tk[37]" -type "float3" -9.0152025e-007 0 4.7683716e-007 ;
	setAttr ".tk[38]" -type "float3" 1.0281801e-006 0 4.7683716e-007 ;
	setAttr ".tk[39]" -type "float3" 1.5646219e-007 0 4.7683716e-007 ;
	setAttr ".tk[40]" -type "float3" 9.8347664e-007 0 4.7683716e-007 ;
	setAttr ".tk[41]" -type "float3" -1.4901161e-008 0 0 ;
	setAttr ".tk[42]" -type "float3" -1.4808029e-007 0 0 ;
	setAttr ".tk[52]" -type "float3" -3.054738e-007 0 0 ;
	setAttr ".tk[53]" -type "float3" 4.0046871e-007 0 0 ;
	setAttr ".tk[55]" -type "float3" -2.8312206e-007 0 0 ;
	setAttr ".tk[56]" -type "float3" -1.3038516e-008 0 0 ;
	setAttr ".tk[57]" -type "float3" -8.6054206e-007 0 0 ;
	setAttr ".tk[58]" -type "float3" 6.7800283e-007 0 4.7683716e-007 ;
	setAttr ".tk[60]" -type "float3" 7.3019838 0 0 ;
	setAttr ".tk[64]" -type "float3" 7.3019838 0 0 ;
	setAttr ".tk[65]" -type "float3" -2.9685907e-009 0 4.7683716e-007 ;
	setAttr ".tk[68]" -type "float3" 7.3019838 0 0 ;
	setAttr ".tk[72]" -type "float3" 7.3019838 0 0 ;
	setAttr ".tk[73]" -type "float3" -8.0820942 0 0 ;
	setAttr ".tk[74]" -type "float3" -8.0820942 0 0 ;
	setAttr ".tk[75]" -type "float3" -8.0820942 0 0 ;
	setAttr ".tk[76]" -type "float3" -8.0820942 0 0 ;
	setAttr ".tk[77]" -type "float3" -4.6248322 0 0 ;
	setAttr ".tk[78]" -type "float3" -4.3253913 0 0 ;
	setAttr ".tk[79]" -type "float3" -3.9568233 0 0 ;
	setAttr ".tk[80]" -type "float3" -3.4845986 0 0 ;
	setAttr ".tk[81]" -type "float3" -2.1527648 0 0 ;
	setAttr ".tk[82]" -type "float3" -1.8594501 0 0 ;
	setAttr ".tk[83]" -type "float3" -1.5661336 0 0 ;
	setAttr ".tk[84]" -type "float3" -1.2728173 0 0 ;
	setAttr ".tk[85]" -type "float3" -0.97950077 0 0 ;
	setAttr ".tk[86]" -type "float3" -0.68618411 0 0 ;
	setAttr ".tk[87]" -type "float3" -0.3928673 0 0 ;
	setAttr ".tk[88]" -type "float3" -0.099550672 0 0 ;
	setAttr ".tk[89]" -type "float3" 0.19376597 0 0 ;
	setAttr ".tk[90]" -type "float3" 0.48708218 0 0 ;
	setAttr ".tk[91]" -type "float3" 0.78039932 0 0 ;
	setAttr ".tk[92]" -type "float3" 2.18208 0 0 ;
	setAttr ".tk[93]" -type "float3" 2.6543007 0 0 ;
	setAttr ".tk[94]" -type "float3" 2.8701169 0 0 ;
	setAttr ".tk[95]" -type "float3" 3.1265223 0 0 ;
	setAttr ".tk[96]" -type "float3" 3.598743 0 0 ;
	setAttr ".tk[97]" -type "float3" 2.3850758 0 0 ;
	setAttr ".tk[98]" -type "float3" 2.8261032 0 0 ;
	setAttr ".tk[99]" -type "float3" 3.2623215 0 0 ;
	setAttr ".tk[100]" -type "float3" 7.3020225 0 0 ;
	setAttr ".tk[101]" -type "float3" -8.0820742 0 0 ;
	setAttr ".tk[102]" -type "float3" -6.7453794 0 0 ;
	setAttr ".tk[103]" -type "float3" -6.3632588 0 0 ;
	setAttr ".tk[104]" -type "float3" -5.8929372 0 0 ;
	setAttr ".tk[105]" -type "float3" -5.2903357 0 0 ;
	setAttr ".tk[106]" -type "float3" -4.6877375 0 0 ;
	setAttr ".tk[107]" -type "float3" -4.0851364 0 0 ;
	setAttr ".tk[108]" -type "float3" -3.4825368 0 0 ;
	setAttr ".tk[109]" -type "float3" -2.8799376 0 0 ;
	setAttr ".tk[110]" -type "float3" -2.2773376 0 0 ;
	setAttr ".tk[111]" -type "float3" -1.6747382 0 0 ;
	setAttr ".tk[112]" -type "float3" -1.0721384 0 0 ;
	setAttr ".tk[113]" -type "float3" -0.4695383 0 0 ;
	setAttr ".tk[114]" -type "float3" 0.13306114 0 0 ;
	setAttr ".tk[115]" -type "float3" 0.7356602 0 0 ;
	setAttr ".tk[116]" -type "float3" 1.3382598 0 0 ;
	setAttr ".tk[117]" -type "float3" 1.9408616 0 0 ;
	setAttr ".tk[118]" -type "float3" 2.5434611 0 0 ;
	setAttr ".tk[119]" -type "float3" 2.8188517 0 0 ;
	setAttr ".tk[120]" -type "float3" 3.1460567 0 0 ;
	setAttr ".tk[121]" -type "float3" 3.7486625 0 0 ;
	setAttr ".tk[122]" -type "float3" 4.6349621 0 0 ;
	setAttr ".tk[123]" -type "float3" 5.5410247 0 0 ;
	setAttr ".tk[124]" -type "float3" 6.4372053 0 0 ;
	setAttr ".tk[125]" -type "float3" 7.3020582 0 0 ;
	setAttr ".tk[126]" -type "float3" -8.0820961 0 0 ;
	setAttr ".tk[127]" -type "float3" -6.9901886 0 0 ;
	setAttr ".tk[128]" -type "float3" -6.5941958 0 0 ;
	setAttr ".tk[129]" -type "float3" -6.1068091 0 0 ;
	setAttr ".tk[130]" -type "float3" -5.4823351 0 0 ;
	setAttr ".tk[131]" -type "float3" -4.8578658 0 0 ;
	setAttr ".tk[132]" -type "float3" -4.2333999 0 0 ;
	setAttr ".tk[133]" -type "float3" -3.6089292 0 0 ;
	setAttr ".tk[134]" -type "float3" -2.9844568 0 0 ;
	setAttr ".tk[135]" -type "float3" -2.3599882 0 0 ;
	setAttr ".tk[136]" -type "float3" -1.7355195 0 0 ;
	setAttr ".tk[137]" -type "float3" -1.1110488 0 0 ;
	setAttr ".tk[138]" -type "float3" -0.48657918 0 0 ;
	setAttr ".tk[139]" -type "float3" 0.13789055 0 0 ;
	setAttr ".tk[140]" -type "float3" 0.76235992 0 0 ;
	setAttr ".tk[141]" -type "float3" 1.3868285 0 0 ;
	setAttr ".tk[142]" -type "float3" 2.0112994 0 0 ;
	setAttr ".tk[143]" -type "float3" 2.6357713 0 0 ;
	setAttr ".tk[144]" -type "float3" 2.9211576 0 0 ;
	setAttr ".tk[145]" -type "float3" 3.2602389 0 0 ;
	setAttr ".tk[146]" -type "float3" 3.8847096 0 0 ;
	setAttr ".tk[147]" -type "float3" 4.8031821 0 0 ;
	setAttr ".tk[148]" -type "float3" 5.742125 0 0 ;
	setAttr ".tk[149]" -type "float3" 6.6708317 0 0 ;
	setAttr ".tk[150]" -type "float3" 7.3020606 0 0 ;
	setAttr ".tk[151]" -type "float3" -8.082099 0 0 ;
	setAttr ".tk[152]" -type "float3" -3.079392 0 0 ;
	setAttr ".tk[153]" -type "float3" -2.9049459 0 0 ;
	setAttr ".tk[154]" -type "float3" -2.6902356 0 0 ;
	setAttr ".tk[155]" -type "float3" -2.4151371 0 0 ;
	setAttr ".tk[156]" -type "float3" -2.140039 0 0 ;
	setAttr ".tk[157]" -type "float3" -1.8649415 0 0 ;
	setAttr ".tk[158]" -type "float3" -1.5898432 0 0 ;
	setAttr ".tk[159]" -type "float3" -1.3147454 0 0 ;
	setAttr ".tk[160]" -type "float3" -1.0396475 0 0 ;
	setAttr ".tk[161]" -type "float3" -0.76454937 0 0 ;
	setAttr ".tk[162]" -type "float3" -0.48945126 0 0 ;
	setAttr ".tk[163]" -type "float3" -0.21435316 0 0 ;
	setAttr ".tk[164]" -type "float3" 0.060744833 0 0 ;
	setAttr ".tk[165]" -type "float3" 0.33584267 0 0 ;
	setAttr ".tk[166]" -type "float3" 0.61094058 0 0 ;
	setAttr ".tk[167]" -type "float3" 0.88603896 0 0 ;
	setAttr ".tk[168]" -type "float3" 1.1611367 0 0 ;
	setAttr ".tk[169]" -type "float3" 1.2868586 0 0 ;
	setAttr ".tk[170]" -type "float3" 1.4362347 0 0 ;
	setAttr ".tk[171]" -type "float3" 1.711333 0 0 ;
	setAttr ".tk[172]" -type "float3" 2.115947 0 0 ;
	setAttr ".tk[173]" -type "float3" 2.5295815 0 0 ;
	setAttr ".tk[174]" -type "float3" 2.938705 0 0 ;
	setAttr ".tk[175]" -type "float3" 7.3020234 0 0 ;
	setAttr ".tk[176]" -type "float3" -8.0820618 0 0 ;
createNode polyPlanarProj -n "polyPlanarProj1";
	rename -uid "8B12AF00-4894-6C2A-1348-7CAEA0674F7D";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "f[0]" "f[5]" "f[22:37]" "f[39]" "f[44:48]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.13102054595947266 -7.1459507942199707 0.5 ;
	setAttr ".ps" -type "double2" 53.766328811645508 52.677358566281683 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj2";
	rename -uid "17618DC4-45FB-14AC-D54A-2290CA30742B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[2:3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.13102054595947266 -7.1976597309112549 0 ;
	setAttr ".ro" -type "double3" 0 90 0 ;
	setAttr ".ps" -type "double2" 10.313507692138423 10.416547298431396 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "767A22AD-4E4B-E342-A602-D790338E3B19";
	setAttr ".uopa" yes;
	setAttr -s 58 ".uvtk[0:57]" -type "float2" 0 -0.40014714 0 -0.40014714
		 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714
		 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014717 -2.9802322e-008 -0.40014714
		 -2.9802322e-008 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714
		 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714
		 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714 -5.9604645e-008 -0.40014714
		 -5.9604645e-008 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714
		 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714
		 0 -0.40014714 0 -0.40014714 -5.9604645e-008 -0.40014714 -5.9604645e-008 -0.40014714
		 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714 0 -0.40014714
		 -0.056278467 0.071721926 -0.13435483 0.071721926 -0.13435483 -0.73351884 -0.056278467
		 -0.73351884 -0.15342373 0.072596997 -0.075347364 0.072596997 -0.075347364 -0.67966002
		 -0.15342373 -0.67966002;
createNode polyFlipUV -n "polyFlipUV1";
	rename -uid "75964AFC-4D0E-6532-F340-928F26BF6461";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[3]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.38561460380000001;
	setAttr ".pv" 0.1635692269;
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "AF881731-46C0-E8BC-2217-CFBFD913DEEF";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[54]" -type "float2" -0.39534581 -0.072597116 ;
	setAttr ".uvtk[55]" -type "float2" -0.39505655 -0.072597116 ;
	setAttr ".uvtk[56]" -type "float2" -0.39505655 -0.069810152 ;
	setAttr ".uvtk[57]" -type "float2" -0.39534581 -0.069810152 ;
createNode polyMapSewMove -n "polyMapSewMove1";
	rename -uid "7D1A0F58-460D-C4DA-A481-ACAA6D706AF6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[122]";
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "27385716-4708-A5E5-1D49-31A7C87DD7ED";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[50]" -type "float2" 0.60475868 -0.071722046 ;
	setAttr ".uvtk[51]" -type "float2" 0.60504788 -0.071722046 ;
	setAttr ".uvtk[52]" -type "float2" 0.60504788 -0.068738766 ;
	setAttr ".uvtk[53]" -type "float2" 0.60475868 -0.068738766 ;
createNode polyMapSewMove -n "polyMapSewMove2";
	rename -uid "1D654DF8-4EDA-01DA-2AEC-89B4CDBD351F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[118]";
createNode polyPlanarProj -n "polyPlanarProj3";
	rename -uid "CDF49369-49A1-C155-E726-B58C363CB644";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "f[1:4]" "f[6:21]" "f[38]" "f[40:43]" "f[49:145]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.13098526000976563 -7.1459507942199707 -10.816367149353027 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 53.766410827636719 54.528841203358525 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweak -n "polyTweak6";
	rename -uid "1FAC40B7-41DE-9378-EACC-8AA826FBA2AE";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[120]" -type "float3" 1.0646853 0 0 ;
	setAttr ".tk[121]" -type "float3" 1.0646853 0 0 ;
	setAttr ".tk[145]" -type "float3" 0.58746487 0 0 ;
	setAttr ".tk[146]" -type "float3" 0.58746487 0 0 ;
createNode polyTweakUV -n "polyTweakUV4";
	rename -uid "E1E767D1-4111-FD61-B32F-70A348E8342F";
	setAttr ".uopa" yes;
	setAttr -s 155 ".uvtk";
	setAttr ".uvtk[50]" -type "float2" 0.0033490658 -0.0042360425 ;
	setAttr ".uvtk[51]" -type "float2" 0.003218621 -0.0040981174 ;
	setAttr ".uvtk[52]" -type "float2" 0.0030224919 -0.0039957762 ;
	setAttr ".uvtk[53]" -type "float2" 0.0031922907 -0.004134506 ;
	setAttr ".uvtk[54]" -type "float2" 0.017648458 0.0050926208 ;
	setAttr ".uvtk[55]" -type "float2" 0.017422676 0.0051917434 ;
	setAttr ".uvtk[56]" -type "float2" -0.0023143291 0.0049497485 ;
	setAttr ".uvtk[57]" -type "float2" -0.0020884275 0.0048511028 ;
	setAttr ".uvtk[58]" -type "float2" -0.013697055 -0.0056813955 ;
	setAttr ".uvtk[59]" -type "float2" -0.013550306 -0.0057817698 ;
	setAttr ".uvtk[60]" -type "float2" 0.0048888139 -0.0056353211 ;
	setAttr ".uvtk[61]" -type "float2" 0.0047419593 -0.0055344403 ;
	setAttr ".uvtk[62]" -type "float2" 0.0031328648 -0.0038227141 ;
	setAttr ".uvtk[63]" -type "float2" 0.0028578639 -0.0037530065 ;
	setAttr ".uvtk[64]" -type "float2" 0.0032483637 -0.003271997 ;
	setAttr ".uvtk[65]" -type "float2" 0.0029859841 -0.0031547546 ;
	setAttr ".uvtk[66]" -type "float2" 0.0030255318 -0.0029154718 ;
	setAttr ".uvtk[67]" -type "float2" 0.0027974546 -0.0028172135 ;
	setAttr ".uvtk[68]" -type "float2" 0.0028188825 -0.0025840104 ;
	setAttr ".uvtk[69]" -type "float2" 0.0026083887 -0.0024851859 ;
	setAttr ".uvtk[70]" -type "float2" 0.0025965869 -0.002266556 ;
	setAttr ".uvtk[71]" -type "float2" 0.0023966432 -0.0021669269 ;
	setAttr ".uvtk[72]" -type "float2" 0.0023469627 -0.0019607246 ;
	setAttr ".uvtk[73]" -type "float2" 0.002153039 -0.0018614531 ;
	setAttr ".uvtk[74]" -type "float2" 0.0020941496 -0.0016581118 ;
	setAttr ".uvtk[75]" -type "float2" 0.0019033253 -0.0015588701 ;
	setAttr ".uvtk[76]" -type "float2" 0.0018408 -0.0013563931 ;
	setAttr ".uvtk[77]" -type "float2" 0.0016520023 -0.0012571812 ;
	setAttr ".uvtk[78]" -type "float2" 0.0015882552 -0.0010551214 ;
	setAttr ".uvtk[79]" -type "float2" 0.0014013946 -0.00095590949 ;
	setAttr ".uvtk[80]" -type "float2" 0.0013353527 -0.00075447559 ;
	setAttr ".uvtk[81]" -type "float2" 0.0011506677 -0.00065565109 ;
	setAttr ".uvtk[82]" -type "float2" 0.0010845661 -0.00045403838 ;
	setAttr ".uvtk[83]" -type "float2" 0.00090366602 -0.00035586953 ;
	setAttr ".uvtk[84]" -type "float2" 0.00082981586 -0.00015610456 ;
	setAttr ".uvtk[85]" -type "float2" 0.00065732002 -5.9753656e-005 ;
	setAttr ".uvtk[86]" -type "float2" 0.00057077408 0.00013202429 ;
	setAttr ".uvtk[87]" -type "float2" 0.00042039156 0.00022593141 ;
	setAttr ".uvtk[88]" -type "float2" 0.00030821562 0.00036638975 ;
	setAttr ".uvtk[89]" -type "float2" 0.00021928549 0.00048562884 ;
	setAttr ".uvtk[90]" -type "float2" 0.00062662363 0.00040102005 ;
	setAttr ".uvtk[91]" -type "float2" 0.0005748868 0.00052270293 ;
	setAttr ".uvtk[92]" -type "float2" 0.00094097853 0.00041177869 ;
	setAttr ".uvtk[93]" -type "float2" 0.00083363056 0.00046232343 ;
	setAttr ".uvtk[94]" -type "float2" 0.00068235397 0.00068312883 ;
	setAttr ".uvtk[95]" -type "float2" 0.00087934732 0.00058797002 ;
	setAttr ".uvtk[96]" -type "float2" 0.00064980984 0.0010240078 ;
	setAttr ".uvtk[97]" -type "float2" 0.00040012598 0.0011274517 ;
	setAttr ".uvtk[98]" -type "float2" 0.00026136637 0.001637131 ;
	setAttr ".uvtk[99]" -type "float2" 8.3446503e-006 0.0017380714 ;
	setAttr ".uvtk[100]" -type "float2" -0.00014412403 0.0022183359 ;
	setAttr ".uvtk[101]" -type "float2" -0.00038707256 0.0023191571 ;
	setAttr ".uvtk[102]" -type "float2" -0.00053620338 0.0027463138 ;
	setAttr ".uvtk[103]" -type "float2" -0.00076633692 0.0028455257 ;
	setAttr ".uvtk[104]" -type "float2" 0.0033132955 -0.0042847097 ;
	setAttr ".uvtk[105]" -type "float2" 0.0035110116 -0.0044828057 ;
	setAttr ".uvtk[106]" -type "float2" 0.0031176358 -0.0039970577 ;
	setAttr ".uvtk[107]" -type "float2" 0.003073886 -0.0034031272 ;
	setAttr ".uvtk[108]" -type "float2" 0.0026269108 -0.0029086173 ;
	setAttr ".uvtk[109]" -type "float2" 0.0023720264 -0.0025449991 ;
	setAttr ".uvtk[110]" -type "float2" 0.0021032989 -0.0021807253 ;
	setAttr ".uvtk[111]" -type "float2" 0.0018109381 -0.0018236041 ;
	setAttr ".uvtk[112]" -type "float2" 0.0015155971 -0.0014656484 ;
	setAttr ".uvtk[113]" -type "float2" 0.0012201369 -0.0011077821 ;
	setAttr ".uvtk[114]" -type "float2" 0.00092563033 -0.00075054169 ;
	setAttr ".uvtk[115]" -type "float2" 0.00063127279 -0.00039452314 ;
	setAttr ".uvtk[116]" -type "float2" 0.00033837557 -4.0829182e-005 ;
	setAttr ".uvtk[117]" -type "float2" 4.2319298e-005 0.00031077862 ;
	setAttr ".uvtk[118]" -type "float2" -0.00025993586 0.00066441298 ;
	setAttr ".uvtk[119]" -type "float2" -0.00076675415 0.0012894273 ;
	setAttr ".uvtk[120]" -type "float2" -0.00065582991 0.0015702248 ;
	setAttr ".uvtk[121]" -type "float2" -0.00048804283 0.0016386807 ;
	setAttr ".uvtk[122]" -type "float2" -0.00091528893 0.0023813844 ;
	setAttr ".uvtk[123]" -type "float2" -0.00062245131 0.0019007623 ;
	setAttr ".uvtk[124]" -type "float2" -0.0010794997 0.0026293993 ;
	setAttr ".uvtk[125]" -type "float2" -0.0015040636 0.0032397211 ;
	setAttr ".uvtk[126]" -type "float2" -0.0019338131 0.0038379133 ;
	setAttr ".uvtk[127]" -type "float2" -0.0029515624 0.0052236617 ;
	setAttr ".uvtk[128]" -type "float2" 0.0042991308 -0.0052709281 ;
	setAttr ".uvtk[129]" -type "float2" 0.0028639734 -0.0041227043 ;
	setAttr ".uvtk[130]" -type "float2" 0.0030848905 -0.0043643713 ;
	setAttr ".uvtk[131]" -type "float2" 0.0026308149 -0.003813237 ;
	setAttr ".uvtk[132]" -type "float2" 0.0025336891 -0.0033563972 ;
	setAttr ".uvtk[133]" -type "float2" 0.0022219121 -0.0029304326 ;
	setAttr ".uvtk[134]" -type "float2" 0.0019102991 -0.0025180578 ;
	setAttr ".uvtk[135]" -type "float2" 0.0015827417 -0.0021051466 ;
	setAttr ".uvtk[136]" -type "float2" 0.0012346804 -0.0016928911 ;
	setAttr ".uvtk[137]" -type "float2" 0.00088533759 -0.0012786686 ;
	setAttr ".uvtk[138]" -type "float2" 0.00053733587 -0.00086325407 ;
	setAttr ".uvtk[139]" -type "float2" 0.00019136071 -0.00044754148 ;
	setAttr ".uvtk[140]" -type "float2" -0.00015386939 -3.2097101e-005 ;
	setAttr ".uvtk[141]" -type "float2" -0.00049710274 0.00038328767 ;
	setAttr ".uvtk[142]" -type "float2" -0.00084257126 0.00079897046 ;
	setAttr ".uvtk[143]" -type "float2" -0.0011870265 0.0012174249 ;
	setAttr ".uvtk[144]" -type "float2" -0.0015127063 0.0016276538 ;
	setAttr ".uvtk[145]" -type "float2" -0.0014368892 0.0020339191 ;
	setAttr ".uvtk[146]" -type "float2" -0.0013254881 0.0022188723 ;
	setAttr ".uvtk[147]" -type "float2" -0.0020246506 0.0031166673 ;
	setAttr ".uvtk[148]" -type "float2" -0.0016820431 0.0026640594 ;
	setAttr ".uvtk[149]" -type "float2" -0.0023622513 0.0035665035 ;
	setAttr ".uvtk[150]" -type "float2" -0.0028808117 0.0042583942 ;
	setAttr ".uvtk[151]" -type "float2" -0.0033963919 0.0049456358 ;
	setAttr ".uvtk[152]" -type "float2" -0.0039096475 0.0056286156 ;
	setAttr ".uvtk[153]" -type "float2" 0.0035442463 -0.0048643053 ;
	setAttr ".uvtk[154]" -type "float2" 0.0017209724 -0.0034914613 ;
	setAttr ".uvtk[155]" -type "float2" 0.0019439831 -0.0037617087 ;
	setAttr ".uvtk[156]" -type "float2" 0.0014863908 -0.0031645894 ;
	setAttr ".uvtk[157]" -type "float2" 0.0013829619 -0.0027825832 ;
	setAttr ".uvtk[158]" -type "float2" 0.0010599047 -0.0023609996 ;
	setAttr ".uvtk[159]" -type "float2" 0.00074134767 -0.0019413829 ;
	setAttr ".uvtk[160]" -type "float2" 0.00040876865 -0.0015218258 ;
	setAttr ".uvtk[161]" -type "float2" 5.5760145e-005 -0.0011011362 ;
	setAttr ".uvtk[162]" -type "float2" -0.00029966235 -0.00068080425 ;
	setAttr ".uvtk[163]" -type "float2" -0.00065472722 -0.00026071072 ;
	setAttr ".uvtk[164]" -type "float2" -0.0010084808 0.00015920401 ;
	setAttr ".uvtk[165]" -type "float2" -0.0013619661 0.00057893991 ;
	setAttr ".uvtk[166]" -type "float2" -0.0017136931 0.00099873543 ;
	setAttr ".uvtk[167]" -type "float2" -0.0020685196 0.0014197826 ;
	setAttr ".uvtk[168]" -type "float2" -0.0024231672 0.0018420219 ;
	setAttr ".uvtk[169]" -type "float2" -0.0027680397 0.0022666454 ;
	setAttr ".uvtk[170]" -type "float2" -0.0027208924 0.0027247071 ;
	setAttr ".uvtk[171]" -type "float2" -0.0026140213 0.0029444098 ;
	setAttr ".uvtk[172]" -type "float2" -0.0032365918 0.0037153363 ;
	setAttr ".uvtk[173]" -type "float2" -0.0028866529 0.0032860637 ;
	setAttr ".uvtk[174]" -type "float2" -0.0036636591 0.004244566 ;
	setAttr ".uvtk[175]" -type "float2" -0.0041942596 0.0049194098 ;
	setAttr ".uvtk[176]" -type "float2" -0.0047185421 0.0055915117 ;
	setAttr ".uvtk[177]" -type "float2" -0.005194366 0.0062028766 ;
	setAttr ".uvtk[178]" -type "float2" 0.0023630804 -0.0042617917 ;
	setAttr ".uvtk[179]" -type "float2" -0.00048908591 -0.0019258261 ;
	setAttr ".uvtk[180]" -type "float2" -0.00029783696 -0.0021649003 ;
	setAttr ".uvtk[181]" -type "float2" -0.000677526 -0.0016492605 ;
	setAttr ".uvtk[182]" -type "float2" -0.00067745149 -0.0013942719 ;
	setAttr ".uvtk[183]" -type "float2" -0.00093629956 -0.001044631 ;
	setAttr ".uvtk[184]" -type "float2" -0.0011906028 -0.00069802999 ;
	setAttr ".uvtk[185]" -type "float2" -0.0014616847 -0.00034826994 ;
	setAttr ".uvtk[186]" -type "float2" -0.0017580688 6.1988831e-006 ;
	setAttr ".uvtk[187]" -type "float2" -0.0020584464 0.00036042929 ;
	setAttr ".uvtk[188]" -type "float2" -0.0023596883 0.00071430206 ;
	setAttr ".uvtk[189]" -type "float2" -0.0026607215 0.0010679364 ;
	setAttr ".uvtk[190]" -type "float2" -0.0029626191 0.00142169 ;
	setAttr ".uvtk[191]" -type "float2" -0.0032635331 0.0017759204 ;
	setAttr ".uvtk[192]" -type "float2" -0.003567934 0.0021297932 ;
	setAttr ".uvtk[193]" -type "float2" -0.0038734674 0.0024845004 ;
	setAttr ".uvtk[194]" -type "float2" -0.0041677952 0.0028414726 ;
	setAttr ".uvtk[195]" -type "float2" -0.0039998889 0.0032699704 ;
	setAttr ".uvtk[196]" -type "float2" -0.0038269758 0.0034873486 ;
	setAttr ".uvtk[197]" -type "float2" -0.0042874217 0.0040129423 ;
	setAttr ".uvtk[198]" -type "float2" -0.0039761662 0.0036723614 ;
	setAttr ".uvtk[199]" -type "float2" -0.0047519207 0.0045285225 ;
	setAttr ".uvtk[200]" -type "float2" -0.0052223802 0.0050725937 ;
	setAttr ".uvtk[201]" -type "float2" -0.0056809187 0.0056235194 ;
	setAttr ".uvtk[202]" -type "float2" -0.0067777038 0.0069749355 ;
	setAttr ".uvtk[203]" -type "float2" 0.00076936703 -0.0034711361 ;
createNode polyMapDel -n "polyMapDel2";
	rename -uid "49A57572-4057-DC69-1249-6484186DBA73";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "f[1:4]" "f[6:21]" "f[38]" "f[40:43]" "f[49:145]";
createNode polyPlanarProj -n "polyPlanarProj4";
	rename -uid "61929682-4A35-DFF9-1854-25BDE3C37F09";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 14 "f[1]" "f[8:18]" "f[21]" "f[38]" "f[40:43]" "f[49:50]" "f[54:64]" "f[67:74]" "f[78:88]" "f[91:98]" "f[102:112]" "f[115:122]" "f[126:136]" "f[139:145]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -0.13098526000976563 -5.2701096534729004 -10.816367149353027 ;
	setAttr ".ro" -type "double3" -90 0 0 ;
	setAttr ".ps" -type "double2" 53.766410827636719 51.059163379743879 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj5";
	rename -uid "40561085-4F86-0350-8427-90837996B150";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "f[19:20]" "f[65:66]" "f[89:90]" "f[113:114]" "f[137:138]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 7.9475822448730469 -5.1257956027984619 -10.816367149353027 ;
	setAttr ".ro" -type "double3" 21.511272107342954 89.999999999999986 0 ;
	setAttr ".ps" -type "double2" 22.632734298706055 22.682348918056039 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj6";
	rename -uid "2F5C8C01-4DCC-4292-7B77-ADA024B5B446";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "f[4]" "f[6:7]" "f[51:53]" "f[75:77]" "f[99:101]" "f[123:125]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -17.498018741607666 -4.5891549587249756 -10.816367149353027 ;
	setAttr ".ro" -type "double3" -40.115354718649236 89.999999999999972 0 ;
	setAttr ".ps" -type "double2" 22.632734298706055 22.478162570651946 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV5";
	rename -uid "DB7D97CE-4A73-ED87-1950-F282050B1517";
	setAttr ".uopa" yes;
	setAttr -s 224 ".uvtk[0:223]" -type "float2" -0.11321981 0.68614036 -0.12498796
		 0.68614036 -0.12498796 0.56755215 -0.11321981 0.56925553 -0.13947225 0.68614036 -0.13947225
		 0.5777719 -0.1580303 0.68614036 -0.1580303 0.60644388 -0.17658833 0.68614036 -0.17658833
		 0.61694747 -0.19514635 0.68614036 -0.19514635 0.62801886 -0.21370438 0.68614036 -0.21370438
		 0.63681912 -0.23226245 0.68614036 -0.23226245 0.63994181 -0.25082046 0.68614036 -0.25082046
		 0.6407935 -0.26937848 0.68614036 -0.26937848 0.64107734 -0.28793654 0.68614036 -0.28793654
		 0.63937408 -0.30649456 0.68614036 -0.30649456 0.63767081 -0.32505259 0.68614036 -0.32505259
		 0.63483196 -0.34361061 0.68614036 -0.34361061 0.63398033 -0.36216861 0.68614036 -0.36216861
		 0.63284481 -0.38072667 0.68614036 -0.38072667 0.62688327 -0.39928472 0.68614036 -0.39928472
		 0.59054661 -0.40776587 0.68614036 -0.40776587 0.570508 -0.41784275 0.56471342 -0.41784275
		 0.68614036 -0.43640083 0.68614036 -0.43640083 0.561023 -0.46400023 0.68614036 -0.46369597
		 0.56040186 -0.49159953 0.68614036 -0.49159953 0.5625757 -0.51919901 0.68614036 -0.51919901
		 0.55978078 -0.63272905 0.68614036 -0.63272905 0.561023 0 0.56925553 0 0.68614036
		 0.51061726 0.2216419 0.51299739 0.2216419 0.51299739 0.22414827 0.51061726 0.22414827
		 -0.17716473 -0.27585876 -0.17716473 -0.27836752 -0.17340767 -0.27836752 -0.17340767
		 -0.27585876 -0.1696506 -0.27836752 -0.1696506 -0.27585876 -0.16589354 -0.27836752
		 -0.16589354 -0.27585876 -0.16213648 -0.27836752 -0.16213648 -0.27585876 -0.15837941
		 -0.27836752 -0.15837941 -0.27585876 -0.15462235 -0.27836752 -0.15462235 -0.27585876
		 -0.15086529 -0.27836752 -0.15086529 -0.27585876 -0.14710823 -0.27836752 -0.14710823
		 -0.27585876 -0.14335117 -0.27836752 -0.14335117 -0.27585876 -0.13959411 -0.27836752
		 -0.13959411 -0.27585876 -0.13583705 -0.27836752 -0.13583705 -0.27585876 -0.15469801
		 -0.27586034 -0.15469801 -0.27836752 -0.15094328 -0.27836752 -0.15094328 -0.27586034
		 -0.15673676 -0.27586034 -0.15673676 -0.27836752 -0.14542091 -0.27836752 -0.14542091
		 -0.27586034 -0.1397754 -0.27836752 -0.1397754 -0.27586034 -0.13419145 -0.27836752
		 -0.13419145 -0.27586034 -0.11122191 -0.27836752 -0.11122191 -0.27586034 0.48771811
		 0.22414827 0.48771811 0.2216419 0.50270236 0.23135161 0.49960947 0.23135161 -0.17783771
		 -0.26864845 -0.18229359 -0.26864845 -0.17338184 -0.26864845 -0.16892597 -0.26864845
		 -0.16447009 -0.26864845 -0.16001421 -0.26864845 -0.15555835 -0.26864845 -0.15110248
		 -0.26864845 -0.14664659 -0.26864845 -0.14219072 -0.26864845 -0.13773486 -0.26864845
		 -0.13063836 -0.26864845 -0.14237487 -0.26865458 -0.14725393 -0.26865458 -0.14990318
		 -0.26865458 -0.1397422 -0.26865458 -0.13304663 -0.26865458 -0.12642407 -0.26865458
		 -0.11122179 -0.26865458 0.48771814 0.23135161 0.49785191 0.24253225 0.49456227 0.24253225
		 -0.18314031 -0.25745705 -0.18833303 -0.25745705 -0.17794758 -0.25745705 -0.17275485
		 -0.25745705 -0.16756213 -0.25745705 -0.1623694 -0.25745705 -0.15717667 -0.25745705
		 -0.15198395 -0.25745705 -0.14679122 -0.25745705 -0.14159849 -0.25745705 -0.13640577
		 -0.25745705 -0.13121307 -0.25745705 -0.13948298 -0.25747025 -0.14467245 -0.25747025
		 -0.15002519 -0.25747025 -0.13438529 -0.25747025 -0.1265825 -0.25747025 -0.11886483
		 -0.25747025 -0.11122167 -0.25747025 0.48771811 0.24253225 0.49730223 0.25832254 0.49397957
		 0.25832254 -0.18349352 -0.24165148 -0.18873835 -0.24165148 -0.1782487 -0.24165148
		 -0.17300387 -0.24165148 -0.16775903 -0.24165148 -0.16251421 -0.24165148 -0.15726936
		 -0.24165148 -0.15202454 -0.24165148 -0.14677972 -0.24165148 -0.14153489 -0.24165148
		 -0.13629007 -0.24165148 -0.13104522 -0.24165148 -0.14029527 -0.24167469 -0.14553684
		 -0.24167469 -0.14978164 -0.24167469 -0.1339848 -0.24167469 -0.1261037 -0.24167469
		 -0.1183086 -0.24167469 -0.11122167 -0.24167469 0.48771808 0.25832254 0.50608319 0.27836752
		 0.50328785 0.27836752 -0.1778508 -0.22158712 -0.18226326 -0.22158712 -0.17343833
		 -0.22158712 -0.16902585 -0.22158712 -0.16461338 -0.22158712 -0.16020092 -0.22158712
		 -0.15578845 -0.22158712 -0.15137598 -0.22158712 -0.14696351 -0.22158712 -0.14255106
		 -0.22158712 -0.13813856 -0.22158712 -0.13372612 -0.22158712 -0.14686871 -0.22162306
		 -0.15127838 -0.22162306 -0.15367281 -0.22162306 -0.14038295 -0.22162306 -0.13375258
		 -0.22162306 -0.12719458 -0.22162306 -0.11122179 -0.22162306 0.48771819 0.27836752
		 0.60655051 0.21358997 0.63470161 0.21358997 0.63470161 0.11829007 0.60655051 0.11828995
		 0.63470161 0.067269444 0.60655051 0.067269444 0.52564293 0.090950727 0.52564293 0.1911146
		 0.52564293 0.03770721 0.40006346 0.092092335 0.40006346 0.19359916 0.40006346 0.038235247
		 0.22270775 0.091141522 0.22270775 0.1928736 0.22270775 0.037181497 -0.0024358034
		 0.10633028 -0.0024358034 0.20446384 -0.0024358034 0.054014802 -0.024430754 0.063347399
		 1.3323573e-008 0.063347399 2.9802322e-008 0.098861039 -0.024430722 0.098861098 2.9802322e-008
		 0.16876078 -0.024430722 0.16876078 2.9802322e-008 0.21020573 -0.024430722 0.2102057
		 -0.094645813 0.0361467 -0.094645813 -0.0052087307 -0.094645813 0.11353099 -0.094645783
		 0.17608505 -0.20362905 0.0054598451 -0.20362905 -0.037508309 -0.20362905 0.084910631
		 -0.20362905 0.13590658 -0.35754588 0.0020700693 -0.35754588 -0.041168571 -0.35754588
		 0.081867516 -0.35754588 0.13321012 -0.55293506 0.056221664 -0.55293506 0.017304897
		 -0.55293506 0.13048166 -0.55293506 0.17628676;
createNode polyFlipUV -n "polyFlipUV2";
	rename -uid "E4001176-46B9-D448-018C-689AA81D3269";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "f[19:20]" "f[65:66]" "f[89:90]" "f[113:114]" "f[137:138]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".up" yes;
	setAttr ".pu" 0.81613290309999997;
	setAttr ".pv" 0.62538573149999999;
createNode polyTweakUV -n "polyTweakUV6";
	rename -uid "D60D93ED-4AEF-74FC-9706-038DCCE9FF02";
	setAttr ".uopa" yes;
	setAttr -s 19 ".uvtk";
	setAttr ".uvtk[182]" -type "float2" -0.6086421 -0.55305934 ;
	setAttr ".uvtk[183]" -type "float2" -0.6246748 -0.57515162 ;
	setAttr ".uvtk[184]" -type "float2" -0.54988575 -0.62942684 ;
	setAttr ".uvtk[185]" -type "float2" -0.53385299 -0.60733467 ;
	setAttr ".uvtk[186]" -type "float2" -0.50984609 -0.6584841 ;
	setAttr ".uvtk[187]" -type "float2" -0.49381346 -0.63639182 ;
	setAttr ".uvtk[188]" -type "float2" -0.46631944 -0.55941057 ;
	setAttr ".uvtk[189]" -type "float2" -0.54492545 -0.50236529 ;
	setAttr ".uvtk[190]" -type "float2" -0.42453527 -0.58973384 ;
	setAttr ".uvtk[191]" -type "float2" -0.39569524 -0.46020877 ;
	setAttr ".uvtk[192]" -type "float2" -0.4753553 -0.40239859 ;
	setAttr ".uvtk[193]" -type "float2" -0.35342956 -0.49088144 ;
	setAttr ".uvtk[194]" -type "float2" -0.2939415 -0.32156587 ;
	setAttr ".uvtk[195]" -type "float2" -0.37377825 -0.26362747 ;
	setAttr ".uvtk[196]" -type "float2" -0.2515949 -0.35229725 ;
	setAttr ".uvtk[197]" -type "float2" -0.1776374 -0.13622856 ;
	setAttr ".uvtk[198]" -type "float2" -0.25465015 -0.080339551 ;
	setAttr ".uvtk[199]" -type "float2" -0.13658154 -0.16602325 ;
createNode polyMapSewMove -n "polyMapSewMove3";
	rename -uid "03217F5B-4127-71D8-05A0-3785C7B7EE8B";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[94]" "e[159]" "e[208]" "e[257]" "e[306]";
createNode polyTweakUV -n "polyTweakUV7";
	rename -uid "9D4B7006-4852-5A69-EBC2-459C03934284";
	setAttr ".uopa" yes;
	setAttr -s 127 ".uvtk";
	setAttr ".uvtk[54]" -type "float2" 0.032338113 0.022377804 ;
	setAttr ".uvtk[55]" -type "float2" 0.031343579 0.02240181 ;
	setAttr ".uvtk[56]" -type "float2" 0.031307608 0.020912383 ;
	setAttr ".uvtk[57]" -type "float2" 0.032302141 0.020888373 ;
	setAttr ".uvtk[58]" -type "float2" 0.031271681 0.01942295 ;
	setAttr ".uvtk[59]" -type "float2" 0.032266214 0.019398943 ;
	setAttr ".uvtk[60]" -type "float2" 0.03123571 0.017933525 ;
	setAttr ".uvtk[61]" -type "float2" 0.032230273 0.017909516 ;
	setAttr ".uvtk[62]" -type "float2" 0.031199723 0.016444098 ;
	setAttr ".uvtk[63]" -type "float2" 0.032194287 0.016420089 ;
	setAttr ".uvtk[64]" -type "float2" 0.031163812 0.014954666 ;
	setAttr ".uvtk[65]" -type "float2" 0.032158345 0.014930654 ;
	setAttr ".uvtk[66]" -type "float2" 0.03112781 0.01346524 ;
	setAttr ".uvtk[67]" -type "float2" 0.032122374 0.013441227 ;
	setAttr ".uvtk[68]" -type "float2" 0.031091869 0.011975814 ;
	setAttr ".uvtk[69]" -type "float2" 0.032086402 0.011951804 ;
	setAttr ".uvtk[70]" -type "float2" 0.031055897 0.010486383 ;
	setAttr ".uvtk[71]" -type "float2" 0.032050461 0.010462373 ;
	setAttr ".uvtk[72]" -type "float2" 0.031019956 0.0089969607 ;
	setAttr ".uvtk[73]" -type "float2" 0.032014549 0.0089729503 ;
	setAttr ".uvtk[74]" -type "float2" 0.030983984 0.00750753 ;
	setAttr ".uvtk[75]" -type "float2" 0.031978577 0.007483521 ;
	setAttr ".uvtk[76]" -type "float2" 0.030948043 0.0060181003 ;
	setAttr ".uvtk[77]" -type "float2" 0.031942606 0.0059940903 ;
	setAttr ".uvtk[78]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[79]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[80]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[81]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[82]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[83]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[84]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[85]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[86]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[87]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[88]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[89]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[90]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[91]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[96]" -type "float2" 0.035203025 0.022575591 ;
	setAttr ".uvtk[97]" -type "float2" 0.035245627 0.024342064 ;
	setAttr ".uvtk[98]" -type "float2" 0.035160363 0.020809133 ;
	setAttr ".uvtk[99]" -type "float2" 0.035117716 0.019042682 ;
	setAttr ".uvtk[100]" -type "float2" 0.035075054 0.017276209 ;
	setAttr ".uvtk[101]" -type "float2" 0.035032392 0.015509758 ;
	setAttr ".uvtk[102]" -type "float2" 0.034989774 0.0137433 ;
	setAttr ".uvtk[103]" -type "float2" 0.034947127 0.011976827 ;
	setAttr ".uvtk[104]" -type "float2" 0.03490448 0.010210369 ;
	setAttr ".uvtk[105]" -type "float2" 0.034861833 0.0084439181 ;
	setAttr ".uvtk[106]" -type "float2" 0.034819186 0.006677452 ;
	setAttr ".uvtk[107]" -type "float2" 0.034751296 0.0038641575 ;
	setAttr ".uvtk[108]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[109]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[110]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[111]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[112]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[113]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[114]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[118]" -type "float2" 0.039690375 0.024570618 ;
	setAttr ".uvtk[119]" -type "float2" 0.039740086 0.026629183 ;
	setAttr ".uvtk[120]" -type "float2" 0.039640665 0.022512037 ;
	setAttr ".uvtk[121]" -type "float2" 0.039590985 0.020453457 ;
	setAttr ".uvtk[122]" -type "float2" 0.039541304 0.018394906 ;
	setAttr ".uvtk[123]" -type "float2" 0.039491594 0.016336326 ;
	setAttr ".uvtk[124]" -type "float2" 0.039441884 0.014277745 ;
	setAttr ".uvtk[125]" -type "float2" 0.039392233 0.012219179 ;
	setAttr ".uvtk[126]" -type "float2" 0.039342523 0.010160599 ;
	setAttr ".uvtk[127]" -type "float2" 0.039292812 0.0081020184 ;
	setAttr ".uvtk[128]" -type "float2" 0.039243162 0.0060434523 ;
	setAttr ".uvtk[129]" -type "float2" 0.039193392 0.0039848718 ;
	setAttr ".uvtk[130]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[131]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[132]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[133]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[134]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[135]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[136]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[140]" -type "float2" 0.045959622 0.024559412 ;
	setAttr ".uvtk[141]" -type "float2" 0.046009809 0.026638631 ;
	setAttr ".uvtk[142]" -type "float2" 0.045909435 0.022480194 ;
	setAttr ".uvtk[143]" -type "float2" 0.045859203 0.020400945 ;
	setAttr ".uvtk[144]" -type "float2" 0.04580906 0.018321697 ;
	setAttr ".uvtk[145]" -type "float2" 0.045758814 0.016242478 ;
	setAttr ".uvtk[146]" -type "float2" 0.045708656 0.01416323 ;
	setAttr ".uvtk[147]" -type "float2" 0.045658469 0.012084011 ;
	setAttr ".uvtk[148]" -type "float2" 0.045608252 0.010004792 ;
	setAttr ".uvtk[149]" -type "float2" 0.045558065 0.0079255439 ;
	setAttr ".uvtk[150]" -type "float2" 0.045507908 0.0058463248 ;
	setAttr ".uvtk[151]" -type "float2" 0.045457721 0.0037671062 ;
	setAttr ".uvtk[152]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[153]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[154]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[155]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[156]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[157]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[158]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[162]" -type "float2" 0.053859785 0.022130404 ;
	setAttr ".uvtk[163]" -type "float2" 0.05390203 0.023879621 ;
	setAttr ".uvtk[164]" -type "float2" 0.053817555 0.020381127 ;
	setAttr ".uvtk[165]" -type "float2" 0.053775325 0.018631849 ;
	setAttr ".uvtk[166]" -type "float2" 0.053733096 0.016882632 ;
	setAttr ".uvtk[167]" -type "float2" 0.05369091 0.015133414 ;
	setAttr ".uvtk[168]" -type "float2" 0.053648651 0.013384137 ;
	setAttr ".uvtk[169]" -type "float2" 0.053606451 0.01163486 ;
	setAttr ".uvtk[170]" -type "float2" 0.053564191 0.0098855831 ;
	setAttr ".uvtk[171]" -type "float2" 0.053521991 0.0081364252 ;
	setAttr ".uvtk[172]" -type "float2" 0.053479761 0.0063871476 ;
	setAttr ".uvtk[173]" -type "float2" 0.053437591 0.0046378705 ;
	setAttr ".uvtk[174]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[175]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[176]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[177]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[178]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[179]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[180]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[182]" -type "float2" 0.10773195 0 ;
	setAttr ".uvtk[183]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[184]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[185]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[186]" -type "float2" 0.10773195 0 ;
	setAttr ".uvtk[187]" -type "float2" 0.10773195 0 ;
	setAttr ".uvtk[188]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[189]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[190]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[191]" -type "float2" 0.10773195 0 ;
	setAttr ".uvtk[192]" -type "float2" 0.10773198 0 ;
	setAttr ".uvtk[193]" -type "float2" 0.10773198 0 ;
createNode polyMapSewMove -n "polyMapSewMove4";
	rename -uid "E4E49DBB-4B97-6585-DEBB-9DBA4C9CFCD2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[72]" "e[155]" "e[204]" "e[253]" "e[302]";
createNode polyTweakUV -n "polyTweakUV8";
	rename -uid "EC652F92-4A56-2AF1-2B50-C4A915BF4EF7";
	setAttr ".uopa" yes;
	setAttr -s 25 ".uvtk";
	setAttr ".uvtk[188]" -type "float2" -0.0181567 -0.55118626 ;
	setAttr ".uvtk[189]" -type "float2" 0.0006018132 -0.57325619 ;
	setAttr ".uvtk[190]" -type "float2" 0.032683559 -0.54598802 ;
	setAttr ".uvtk[191]" -type "float2" 0.013925113 -0.52391815 ;
	setAttr ".uvtk[192]" -type "float2" 0.09582857 -0.49231738 ;
	setAttr ".uvtk[193]" -type "float2" 0.077070072 -0.47024751 ;
	setAttr ".uvtk[194]" -type "float2" 0.13326848 -0.460495 ;
	setAttr ".uvtk[195]" -type "float2" 0.11450993 -0.43842512 ;
	setAttr ".uvtk[196]" -type "float2" -0.096641734 -0.5086419 ;
	setAttr ".uvtk[197]" -type "float2" -0.13400072 -0.5403955 ;
	setAttr ".uvtk[198]" -type "float2" -0.026735373 -0.44922435 ;
	setAttr ".uvtk[199]" -type "float2" 0.029773727 -0.40119398 ;
	setAttr ".uvtk[200]" -type "float2" -0.20804305 -0.43375239 ;
	setAttr ".uvtk[201]" -type "float2" -0.24685894 -0.4667443 ;
	setAttr ".uvtk[202]" -type "float2" -0.13626985 -0.37274811 ;
	setAttr ".uvtk[203]" -type "float2" -0.090202019 -0.3335923 ;
	setAttr ".uvtk[204]" -type "float2" -0.3292861 -0.29731205 ;
	setAttr ".uvtk[205]" -type "float2" -0.36834645 -0.33051172 ;
	setAttr ".uvtk[206]" -type "float2" -0.25719994 -0.23604175 ;
	setAttr ".uvtk[207]" -type "float2" -0.21081883 -0.19661966 ;
	setAttr ".uvtk[208]" -type "float2" -0.43039182 -0.079225302 ;
	setAttr ".uvtk[209]" -type "float2" -0.46554795 -0.1091066 ;
	setAttr ".uvtk[210]" -type "float2" -0.36330798 -0.022206783 ;
	setAttr ".uvtk[211]" -type "float2" -0.32192928 0.012963414 ;
createNode polyMapSewMove -n "polyMapSewMove5";
	rename -uid "91AC24F2-462F-7087-EF75-3C84CE0CD976";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[61]" "e[133]" "e[182]" "e[231]" "e[280]";
createNode polyTweakUV -n "polyTweakUV9";
	rename -uid "931648A9-4E88-161A-35AC-51BA6372F2ED";
	setAttr ".uopa" yes;
	setAttr -s 19 ".uvtk";
	setAttr ".uvtk[50]" -type "float2" -0.71853262 -0.47576562 ;
	setAttr ".uvtk[51]" -type "float2" -0.71853185 -0.47576562 ;
	setAttr ".uvtk[52]" -type "float2" -0.71853185 -0.47576478 ;
	setAttr ".uvtk[53]" -type "float2" -0.71853262 -0.47576478 ;
	setAttr ".uvtk[92]" -type "float2" -0.71854007 -0.47576478 ;
	setAttr ".uvtk[93]" -type "float2" -0.71854007 -0.47576562 ;
	setAttr ".uvtk[94]" -type "float2" -0.71853518 -0.47576243 ;
	setAttr ".uvtk[95]" -type "float2" -0.71853626 -0.47576243 ;
	setAttr ".uvtk[115]" -type "float2" -0.71854007 -0.47576243 ;
	setAttr ".uvtk[116]" -type "float2" -0.71853679 -0.47575876 ;
	setAttr ".uvtk[117]" -type "float2" -0.71853787 -0.47575876 ;
	setAttr ".uvtk[137]" -type "float2" -0.71854007 -0.47575876 ;
	setAttr ".uvtk[138]" -type "float2" -0.71853697 -0.47575364 ;
	setAttr ".uvtk[139]" -type "float2" -0.71853805 -0.47575364 ;
	setAttr ".uvtk[159]" -type "float2" -0.71854007 -0.47575364 ;
	setAttr ".uvtk[160]" -type "float2" -0.71853411 -0.47574708 ;
	setAttr ".uvtk[161]" -type "float2" -0.71853501 -0.47574708 ;
	setAttr ".uvtk[181]" -type "float2" -0.71854007 -0.47574708 ;
createNode polyMapSewMove -n "polyMapSewMove6";
	rename -uid "EFDA631E-4ADC-AD8A-AA0A-458F3A411B7C";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[6]" "e[127]" "e[176]" "e[225]" "e[274]";
createNode polyTweakUV -n "polyTweakUV10";
	rename -uid "A93B2B74-4684-60DA-0069-07A5616B7FBA";
	setAttr ".uopa" yes;
	setAttr -s 200 ".uvtk[0:199]" -type "float2" 0.10457094 -0.24514857 0.11544019
		 -0.24514857 0.11544019 -0.13561934 0.10457094 -0.13719243 0.12881793 -0.24514857
		 0.12881793 -0.1450583 0.14595841 -0.24514857 0.14595841 -0.17153978 0.16309856 -0.24514857
		 0.16309856 -0.1812411 0.18023908 -0.24514857 0.18023908 -0.19146675 0.19737962 -0.24514857
		 0.19737962 -0.19959509 0.21452004 -0.24514857 0.21452004 -0.2024793 0.23166028 -0.24514857
		 0.23166028 -0.20326567 0.24880072 -0.24514857 0.24880072 -0.20352796 0.2659412 -0.24514857
		 0.2659412 -0.2019549 0.28308132 -0.24514857 0.28308132 -0.20038146 0.30022177 -0.24514857
		 0.30022177 -0.19775963 0.31736231 -0.24514857 0.31736231 -0.19697285 0.3345027 -0.24514857
		 0.3345027 -0.19592404 0.35164294 -0.24514857 0.35164294 -0.190418 0.3687835 -0.24514857
		 0.3687835 -0.15685713 0.37661675 -0.24514857 0.37661675 -0.13834929 0.38592377 -0.13299721
		 0.38592377 -0.24514857 0.40306422 -0.24514857 0.40306422 -0.12958863 0.4285554 -0.24514857
		 0.42827412 -0.129015 0.4540464 -0.24514857 0.4540464 -0.13102305 0.4795374 -0.24514857
		 0.4795374 -0.12844124 0.58439499 -0.24514857 0.58439499 -0.12958863 0 -0.13719243
		 0 -0.24514857 0.17689593 0.0078650918 0.1742842 0.0076778382 0.17417844 0.0034285523
		 0.17666689 0.0036188737 0.1492459 0.0031475462 0.14933285 0.007397756 0.14216074
		 0.007914314 0.14196932 0.0036662444 0.13579896 0.0082010031 0.13560751 0.0039529353
		 0.12943718 0.0084876921 0.12924573 0.0042396225 0.1230754 0.0087743811 0.12288398
		 0.0045263097 0.11671361 0.009061072 0.11652216 0.0048130006 0.11035183 0.0093477601
		 0.11016038 0.0050996915 0.10399005 0.0096344482 0.10379863 0.0053863786 0.097628266
		 0.0099211391 0.097436816 0.0056730695 0.091266513 0.010207825 0.091075033 0.0059597567
		 0.084904701 0.010494516 0.08471328 0.0062464457 0.077397048 0.010795709 0.077143431
		 0.0065513514 0.053729892 0.0083748903 0.053854465 0.012625453 0.047488928 0.012812003
		 0.047364354 0.0085614417 0.055540085 0.0082422756 0.055760264 0.012487876 0.038126528
		 0.013086381 0.038001955 0.0088358186 0.028555453 0.013366876 0.028430879 0.0091163134
		 0.019088686 0.013644312 0.018964112 0.0093937488 -0.019852817 0.014785547 -0.019977391
		 0.010534985 0.21546216 0.0015260354 0.21569122 0.0057722554 0.1885616 -0.0092299581
		 0.19465773 -0.0095909536 0.14892045 -0.0088809356 0.15630771 -0.0092511997 0.14137536
		 -0.0085409209 0.13383031 -0.0082009137 0.12628523 -0.007860899 0.11874017 -0.0075208843
		 0.11119509 -0.0071808696 0.10365 -0.0068408549 0.09610495 -0.0065008402 0.088559866
		 -0.0061608329 0.081014842 -0.0058208182 0.070320249 -0.0053227022 0.032479882 -0.0032291338
		 0.040751576 -0.0034715459 0.046887994 -0.0036242008 0.028016508 -0.0030983314 0.016665161
		 -0.002765663 0.0054376125 -0.002436623 -0.020335555 -0.0016813055 0.21480374 -0.010677747
		 0.19500944 -0.028395653 0.20218678 -0.028994173 0.15704529 -0.028235778 0.16452968
		 -0.028427005 0.14825252 -0.027839541 0.13945973 -0.027443305 0.13066697 -0.027047068
		 0.12187418 -0.026650816 0.11308143 -0.026254579 0.10428864 -0.025858343 0.09549588
		 -0.025462106 0.086703092 -0.025065869 0.077910304 -0.024669617 0.069862604 -0.024292231
		 0.027021408 -0.02204679 0.035819352 -0.022304624 0.046049058 -0.022569418 0.018379092
		 -0.021793514 0.0051506162 -0.021405846 -0.0079336166 -0.021022394 -0.020891368 -0.020642638
		 0.213782 -0.029619679 0.19512716 -0.055190414 0.20173082 -0.055799037 0.15643734
		 -0.055026114 0.16455959 -0.055218339 0.14755633 -0.054625899 0.13867533 -0.054225653
		 0.1297943 -0.053825438 0.12091333 -0.053425223 0.11203229 -0.053025007 0.10315129
		 -0.052624792 0.094270319 -0.052224576 0.085389316 -0.051824361 0.076508343 -0.051424146
		 0.068068027 -0.051021755 0.027613759 -0.048866212 0.036500037 -0.049126625 0.044376016
		 -0.04930529 0.016915262 -0.048552662 0.0035540462 -0.048161089 -0.0096614361 -0.047773808
		 -0.021676183 -0.047421694 0.21233888 -0.056371301 0.18175399 -0.08879444 0.18412891
		 -0.088908106 0.14535153 -0.088570207 0.15463305 -0.088978499 0.13787994 -0.088233501
		 0.13040838 -0.087896794 0.12293679 -0.087560087 0.11546522 -0.087223411 0.10799363
		 -0.086886704 0.10052204 -0.086549997 0.09305051 -0.086213291 0.085578918 -0.085876614
		 0.078107297 -0.085539907 0.069182456 -0.085134029 0.037761748 -0.083187401 0.04523772
		 -0.083406478 0.047181427 -0.083451301 0.026766121 -0.082865149 0.015525341 -0.082535714
		 0.0044071674 -0.082209885 -0.022672296 -0.081416279 0.21050672 -0.090331078 0.061895311
		 0.01187926 0.061579466 0.0076386221 0.056553543 -0.0042424947 0.055316806 -0.023172408
		 0.053184032 -0.049878359 0.052946448 -0.08396402 0.16952351 0.0072476175 0.16954099
		 0.0029952861 0.15735695 0.0071975682 0.15737444 0.0029452257 0.18050717 -0.0091812387
		 0.16703786 -0.0092366561 0.18592648 -0.028128564 0.17209749 -0.028185472 0.1866267
		 -0.054916441 0.1727374 -0.054973572 0.17734115 -0.088964105 0.16441569 -0.089017302;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "C9082107-4E2E-3792-849C-71A82920876A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[2]" "e[8]" "e[28:43]" "e[93]" "e[102:105]" "e[123]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".a" 0;
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "B8167D39-4B49-C827-4715-968A403C5F5E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 12 "e[4]" "e[6]" "e[94]" "e[96]" "e[127]" "e[159]" "e[176]" "e[208]" "e[225]" "e[257]" "e[274]" "e[306]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".a" 180;
createNode polySoftEdge -n "polySoftEdge3";
	rename -uid "E326DABF-40C9-013A-998E-EEB96844B224";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[72]" "e[88]" "e[155]" "e[204]" "e[253]" "e[302]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".a" 180;
createNode polySoftEdge -n "polySoftEdge4";
	rename -uid "65231681-4903-C99B-8BA9-1DB61EBA9E2F";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 17 "e[3:6]" "e[74]" "e[90]" "e[94]" "e[96]" "e[126:127]" "e[159]" "e[161]" "e[175:176]" "e[208]" "e[210]" "e[224:225]" "e[257]" "e[259]" "e[273:274]" "e[306]" "e[308]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 9.7311524693841456 0 0 0 0 1 0 -16.893298388414312 -7.5403568315631606 0 1;
	setAttr ".a" 180;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "polySoftEdge4.out" "pCubeShape1.i";
connectAttr "polyTweakUV10.uvtk[0]" "pCubeShape1.uvst[0].uvtw";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube1.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyTweak1.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak1.ip";
connectAttr "polyTweak2.out" "polySplitRing1.ip";
connectAttr "pCubeShape1.wm" "polySplitRing1.mp";
connectAttr "polyExtrudeFace2.out" "polyTweak2.ip";
connectAttr "polyTweak3.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polySplitRing1.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polyExtrudeFace4.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak4.ip";
connectAttr "polyExtrudeFace4.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "polyExtrudeEdge1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeEdge1.mp";
connectAttr "polyExtrudeEdge1.out" "polyExtrudeEdge2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeEdge2.mp";
connectAttr "polyExtrudeEdge2.out" "polyExtrudeEdge3.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeEdge3.mp";
connectAttr "polyExtrudeEdge3.out" "polyExtrudeEdge4.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeEdge4.mp";
connectAttr "polyTweak5.out" "polyMapDel1.ip";
connectAttr "polyExtrudeEdge4.out" "polyTweak5.ip";
connectAttr "polyMapDel1.out" "polyPlanarProj1.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj1.mp";
connectAttr "polyPlanarProj1.out" "polyPlanarProj2.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj2.mp";
connectAttr "polyPlanarProj2.out" "polyTweakUV1.ip";
connectAttr "polyTweakUV1.out" "polyFlipUV1.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV1.mp";
connectAttr "polyFlipUV1.out" "polyTweakUV2.ip";
connectAttr "polyTweakUV2.out" "polyMapSewMove1.ip";
connectAttr "polyMapSewMove1.out" "polyTweakUV3.ip";
connectAttr "polyTweakUV3.out" "polyMapSewMove2.ip";
connectAttr "polyTweak6.out" "polyPlanarProj3.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj3.mp";
connectAttr "polyMapSewMove2.out" "polyTweak6.ip";
connectAttr "polyPlanarProj3.out" "polyTweakUV4.ip";
connectAttr "polyTweakUV4.out" "polyMapDel2.ip";
connectAttr "polyMapDel2.out" "polyPlanarProj4.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj4.mp";
connectAttr "polyPlanarProj4.out" "polyPlanarProj5.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj5.mp";
connectAttr "polyPlanarProj5.out" "polyPlanarProj6.ip";
connectAttr "pCubeShape1.wm" "polyPlanarProj6.mp";
connectAttr "polyPlanarProj6.out" "polyTweakUV5.ip";
connectAttr "polyTweakUV5.out" "polyFlipUV2.ip";
connectAttr "pCubeShape1.wm" "polyFlipUV2.mp";
connectAttr "polyFlipUV2.out" "polyTweakUV6.ip";
connectAttr "polyTweakUV6.out" "polyMapSewMove3.ip";
connectAttr "polyMapSewMove3.out" "polyTweakUV7.ip";
connectAttr "polyTweakUV7.out" "polyMapSewMove4.ip";
connectAttr "polyMapSewMove4.out" "polyTweakUV8.ip";
connectAttr "polyTweakUV8.out" "polyMapSewMove5.ip";
connectAttr "polyMapSewMove5.out" "polyTweakUV9.ip";
connectAttr "polyTweakUV9.out" "polyMapSewMove6.ip";
connectAttr "polyMapSewMove6.out" "polyTweakUV10.ip";
connectAttr "polyTweakUV10.out" "polySoftEdge1.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge1.mp";
connectAttr "polySoftEdge1.out" "polySoftEdge2.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge2.mp";
connectAttr "polySoftEdge2.out" "polySoftEdge3.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge3.mp";
connectAttr "polySoftEdge3.out" "polySoftEdge4.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge4.mp";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
// End of wetlandThree_model_003.ma
