﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSpeed : MonoBehaviour 
{
    public static System.Action<float, float, float> SendSpeed;

    public Data.GameSpeed speedType;

	// Use this for initialization
	void OnTriggerEnter () 
    {
        switch (speedType)
        {
            case (Data.GameSpeed.DRAG):
            {
                    SendSpeed(Data.Instance.dragSpeed, Data.Instance.dragGravity, Data.Instance.dragJumpSpeed);
                    break;
            }
            case (Data.GameSpeed.BOOST):
            {
                    SendSpeed(Data.Instance.boostSpeed, Data.Instance.boostGravity, Data.Instance.boostJumpSpeed);
                    break;
            }
            default:
            {
                    break;
            }
        }
	}
	
	// Update is called once per frame
	void OnTriggerExit () 
    {
        SendSpeed(Data.Instance.speed, Data.Instance.gravity, Data.Instance.jumpSpeed);
	}
}
