﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[System.Serializable]
[CreateAssetMenu]

//public class Data : ScriptableObject
public class Data : MonoBehaviour
{
    Data()
    {

    }

    public float speed = 7.0f;
    public float dragSpeed = 1.0f;
    public float boostSpeed = 14.0f;
    public float jumpSpeed = 7.0f;
    public float dragJumpSpeed = 2.0f;
    public float boostJumpSpeed = 10.0f;

    public float gravity = 0.58f;
    public float dragGravity = 0.19f;
    public float boostGravity = 1.16f;

    public bool holdingFan = false;
    public bool onRaft = false;

    public enum GameSpeed
    {
        DRAG,
        BOOST
    }

    private static Data _Instance;
    public static Data Instance
    {
        get
        {
            if (_Instance == null)
            {
                Data.GetData();
            }
            return _Instance;
        }
    }

    public static void GetData()
    {
        if (string.IsNullOrEmpty(PlayerPrefs.GetString("GameData")))
        {
            _Instance = new Data();
        }
        else
        {
            _Instance = JsonUtility.FromJson<Data>(PlayerPrefs.GetString("GameData"));
        }
    }

    public static void SetData()
    {
        PlayerPrefs.SetString("GameData", JsonUtility.ToJson(Instance));
    }

}
