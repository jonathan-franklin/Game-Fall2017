﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour 
{
    public static System.Action End;
    public UnityEngine.UI.Text berryCounter;
    public int berryCount;
    GameObject player;
    public GameObject WinPanel;

    void Start()
    {
        player = GameObject.Find("PlayerCapsule");
        berryCount = 0;
    }

    void OnTriggerEnter()
    {
        berryCounter = GameObject.FindWithTag("BerryCounter").GetComponent<UnityEngine.UI.Text>();
        berryCount = int.Parse(berryCounter.text);
        if (berryCount >= 20)
        {
            player.GetComponent<MovementCC>().Stop();
            WinPanel.SetActive(true);
        }
    }
}
