﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTrigger : MonoBehaviour 
{
    public Animator anim;

    public void Start()
    {
        anim = gameObject.GetComponentInParent<Animator>();
    }
    private void OnTriggerEnter(Collider other)
    {
        GetComponentInParent<EnemyMovement>().StartMoveCoroutine();
        anim.SetBool("IsAttacking", true);
    }
}
