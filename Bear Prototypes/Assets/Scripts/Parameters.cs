﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Parameters : MonoBehaviour 
{
    public float speed = 0.0f;
    public float dragSpeed = 2.0f;
    public float boostSpeed = 14.0f;

    public float gravity = 0.58f;
    public float dragGravity = 0.19f;
    public float boostGravity = 1.16f;

    public bool holdingFan = false;
    public bool onRaft = false;

    public float Speed
    {
        get { return speed; }
        set { speed = value; }
    }

    public float DragSpeed
    {
        get { return dragSpeed; }
        set { dragSpeed = value; }
    }
    public float BoostSpeed
    {
        get { return boostSpeed; }
        set { boostSpeed = value; }
    }
    public float Gravity
    {
        get { return gravity; }
        set { gravity = value; }
    }
    public float DragGravity
    {
        get { return dragGravity; }
        set { dragGravity = value; }
    }
    public float BoostGravity
    {
        get { return boostGravity; }
        set { boostGravity = value; }
    }
    public bool HoldingFan
    {
        get { return holdingFan; }
        set { holdingFan = value; }
    }
    public bool OnRaft
    {
        get { return onRaft; }
        set { onRaft = value; }
    }

}
