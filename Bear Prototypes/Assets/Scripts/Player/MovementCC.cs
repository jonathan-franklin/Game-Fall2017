﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]

public class MovementCC : MonoBehaviour 
{
    private float speed = 0;
    private float jumpSpeed = 0;
    private float fallVelocity = 0;
    private float gravity;

    private int jumpCounter = 0;
    private int airJumps = 1;

    private CharacterController playerController;
    private Vector3 moveValue;
    public GameObject player;

    public Animator anim;

    // Use this for initialization
    void Start ()
    {
        speed = Data.Instance.speed;
        gravity = Data.Instance.gravity;
        jumpSpeed = Data.Instance.jumpSpeed;
        player = GameObject.FindWithTag("Player");
        playerController = GetComponent<CharacterController>();
        anim = player.GetComponent<Animator>();

        ChangeSpeed.SendSpeed += SendSpeedHandler;
    }

    public void StartMoveAction ()
    {
        Actions.PlayerAction += Move;
    }

    private void SendSpeedHandler (float _speed, float _gravity, float _jumpSpeed)
    {
        speed = _speed;
        gravity = _gravity;
        jumpSpeed = _jumpSpeed;
    }
	
	public void Move (float input)
    {
        moveValue.x = input * speed * Time.deltaTime; // Handles left and right movement.

        if (Math.Sign(input) > 0)
        {
            transform.eulerAngles = Vector3.zero;
        }
        if (Math.Sign(input) < 0)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }

        // If you use up arrow, jump only if the player is grounded or 
        // hasn't already used their second jump.
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space))
        {
            if (playerController.isGrounded == true)
            {
                jumpCounter = 0; // Makes sure the jump counter is reset if the player isGrounded.
                jumpCounter++; // Adds one to the jump counter if player jumps off ground.
                moveValue.y = jumpSpeed * Time.deltaTime;
                fallVelocity = 0;
                anim.SetTrigger("Jump");
            }
            else
            {
                if (jumpCounter <= airJumps) // Jump again if player has only single-jumped.
                {
                    jumpCounter++;
                    fallVelocity = 0;
                    moveValue.y = jumpSpeed * Time.deltaTime;
                    anim.SetTrigger("Jump");
                }
            }
        }

        if (playerController.isGrounded == false) // Apply gravity whenever player is off the ground.
        {
            moveValue.y += fallVelocity * Time.deltaTime;
            fallVelocity -= gravity * Time.deltaTime;
        }
        else
        {
            fallVelocity = 0;
        }

        anim.SetFloat("Speed", Math.Abs(input));
        anim.SetTrigger("Run");
        playerController.Move(moveValue);
        player.transform.position = new Vector3(transform.position.x, transform.position.y, 53.42f);
    }

    public void Stop ()
    {
        Actions.PlayerAction -= player.GetComponent<MovementCC>().Move;
    }



    public void OnDisable()
    {
        Actions.PlayerAction -= Move;
        ChangeSpeed.SendSpeed -= SendSpeedHandler;
    }
}
