﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset : MonoBehaviour 
{
    private GameObject entity;

    private Vector3 entityStart;

    private void Start()
    {
        entity = gameObject;
        entityStart = entity.transform.position;
        Restart.RestartAction += RestartGame;
    }

    public void RestartGame()
    {
        entity.transform.position = entityStart;
    }
}
