﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaftParent : MonoBehaviour 
{
    GameObject raft;
    GameObject player;
    Rigidbody raftRB;

    public bool onRaft;
    public bool holdingFan;
    public float force = 0f;
    public float drag = 2.0f;

    private void Start()
    {
        raft = GameObject.FindWithTag("Raft");
        player = GameObject.FindWithTag("Player");

        onRaft = Data.Instance.onRaft;
        holdingFan = Data.Instance.holdingFan;

        RaftState.SendRaftStatus += RaftHandler;
    }

    private void RaftHandler(bool _onRaft, bool _holdingFan)
    {
        onRaft = _onRaft;
        holdingFan = _holdingFan;
    }

    private void OnTriggerEnter(Collider other)
    {
        onRaft = true;
        player.transform.parent = raft.transform;
        StartCoroutine(MoveRaft());
    }

    private void OnTriggerExit(Collider other)
    {
        player.transform.parent = null;
        onRaft = false;
        StopCoroutine(MoveRaft());
    }

    private void Move()
    {
        raft.transform.Translate(1, 0, 0);      
    }

    IEnumerator MoveRaft ()
    {
        while (onRaft == true)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                Move();
                print("PUUUSSSHHH");
            }
            yield return null;
        }
    }
}
