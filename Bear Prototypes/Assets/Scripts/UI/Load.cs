﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class Load : MonoBehaviour
{
    public GameObject startPanel;
    public GameObject aboutPanel;
    public GameObject healthPanel;
    public GameObject counterPanel;
    public GameObject pausePanel;
    public GameObject winPanel;

    public GameObject player;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    public void LoadLevel ()
    {
        SceneManager.LoadScene(0);
    }

    public void StartGame ()
    {
        startPanel.SetActive(false);
        healthPanel.SetActive(true);
        counterPanel.SetActive(true);
        aboutPanel.SetActive(false);
        pausePanel.SetActive(false);
        winPanel.SetActive(false);

        player.GetComponent<MovementCC>().StartMoveAction();
    }

    public void About ()
    {
        aboutPanel.SetActive(true);
        startPanel.SetActive(false);
        healthPanel.SetActive(false);
        counterPanel.SetActive(false);
        pausePanel.SetActive(false);
        winPanel.SetActive(false);
    }

    public void ReturnToMenu ()
    {
        startPanel.SetActive(true);
        aboutPanel.SetActive(false);
        healthPanel.SetActive(false);
        counterPanel.SetActive(false);
        pausePanel.SetActive(false);
        winPanel.SetActive(false);
    }

    public void Win ()
    {
        startPanel.SetActive(false);
        aboutPanel.SetActive(false);
        healthPanel.SetActive(false);
        counterPanel.SetActive(false);
        pausePanel.SetActive(false);
        winPanel.SetActive(true);
    }

    public void CloseGame ()
    {
        Application.Quit();
    }
}
