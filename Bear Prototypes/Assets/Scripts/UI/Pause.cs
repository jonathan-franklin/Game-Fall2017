﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour 
{
    public GameObject pauseScreen;

    // Update is called once per frame
    void Update () 
    {
		if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
        {
            if (pauseScreen.activeInHierarchy == false)
            {
                pauseScreen.SetActive(true);
                Time.timeScale = 0.0f;
            }
            else
            {
                Time.timeScale = 1.0f;
                pauseScreen.SetActive(false);
            }
        }
	}
}
