﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Restart : MonoBehaviour 
{
    public static System.Action RestartAction;

    public void CallRestart ()
    {
        RestartAction();
        Time.timeScale = 1.0f;
    }
}
